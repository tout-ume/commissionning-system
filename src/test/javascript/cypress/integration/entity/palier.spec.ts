import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Palier e2e test', () => {
  const palierPageUrl = '/palier';
  const palierPageUrlPattern = new RegExp('/palier(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const palierSample = {};

  let palier: any;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/paliers+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/paliers').as('postEntityRequest');
    cy.intercept('DELETE', '/api/paliers/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (palier) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/paliers/${palier.id}`,
      }).then(() => {
        palier = undefined;
      });
    }
  });

  it('Paliers menu should load Paliers page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('palier');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Palier').should('exist');
    cy.url().should('match', palierPageUrlPattern);
  });

  describe('Palier page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(palierPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Palier page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/palier/new$'));
        cy.getEntityCreateUpdateHeading('Palier');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', palierPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/paliers',
          body: palierSample,
        }).then(({ body }) => {
          palier = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/paliers+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/paliers?page=0&size=20>; rel="last",<http://localhost/api/paliers?page=0&size=20>; rel="first"',
              },
              body: [palier],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(palierPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Palier page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('palier');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', palierPageUrlPattern);
      });

      it('edit button click should load edit Palier page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Palier');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', palierPageUrlPattern);
      });

      it('last delete button click should delete instance of Palier', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('palier').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', palierPageUrlPattern);

        palier = undefined;
      });
    });
  });

  describe('new Palier page', () => {
    beforeEach(() => {
      cy.visit(`${palierPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('Palier');
    });

    it('should create an instance of Palier', () => {
      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        palier = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', palierPageUrlPattern);
    });
  });
});
