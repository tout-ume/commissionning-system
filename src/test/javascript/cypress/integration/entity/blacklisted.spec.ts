import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Blacklisted e2e test', () => {
  const blacklistedPageUrl = '/blacklisted';
  const blacklistedPageUrlPattern = new RegExp('/blacklisted(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const blacklistedSample = { createdBy: 'paradigms withdrawal' };

  let blacklisted: any;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/blacklisteds+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/blacklisteds').as('postEntityRequest');
    cy.intercept('DELETE', '/api/blacklisteds/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (blacklisted) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/blacklisteds/${blacklisted.id}`,
      }).then(() => {
        blacklisted = undefined;
      });
    }
  });

  it('Blacklisteds menu should load Blacklisteds page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('blacklisted');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Blacklisted').should('exist');
    cy.url().should('match', blacklistedPageUrlPattern);
  });

  describe('Blacklisted page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(blacklistedPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Blacklisted page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/blacklisted/new$'));
        cy.getEntityCreateUpdateHeading('Blacklisted');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', blacklistedPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/blacklisteds',
          body: blacklistedSample,
        }).then(({ body }) => {
          blacklisted = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/blacklisteds+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/blacklisteds?page=0&size=20>; rel="last",<http://localhost/api/blacklisteds?page=0&size=20>; rel="first"',
              },
              body: [blacklisted],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(blacklistedPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Blacklisted page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('blacklisted');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', blacklistedPageUrlPattern);
      });

      it('edit button click should load edit Blacklisted page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Blacklisted');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', blacklistedPageUrlPattern);
      });

      it('last delete button click should delete instance of Blacklisted', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('blacklisted').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', blacklistedPageUrlPattern);

        blacklisted = undefined;
      });
    });
  });

  describe('new Blacklisted page', () => {
    beforeEach(() => {
      cy.visit(`${blacklistedPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('Blacklisted');
    });

    it('should create an instance of Blacklisted', () => {
      cy.get(`[data-cy="reason"]`).type('multi-tasking regional Cross-group').should('have.value', 'multi-tasking regional Cross-group');

      cy.get(`[data-cy="blocked"]`).should('not.be.checked');
      cy.get(`[data-cy="blocked"]`).click().should('be.checked');

      cy.get(`[data-cy="createdBy"]`).type('redundant').should('have.value', 'redundant');

      cy.get(`[data-cy="createdDate"]`).type('2022-08-02T23:52').should('have.value', '2022-08-02T23:52');

      cy.get(`[data-cy="lastModifiedBy"]`).type('Stagiaire redefine').should('have.value', 'Stagiaire redefine');

      cy.get(`[data-cy="lastModifiedDate"]`).type('2022-08-02T15:32').should('have.value', '2022-08-02T15:32');

      cy.get(`[data-cy="spare1"]`).type('Jewelery').should('have.value', 'Jewelery');

      cy.get(`[data-cy="spare2"]`).type('Reactive Bedfordshire').should('have.value', 'Reactive Bedfordshire');

      cy.get(`[data-cy="spare3"]`).type('4th Wooden schemas').should('have.value', '4th Wooden schemas');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        blacklisted = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', blacklistedPageUrlPattern);
    });
  });
});
