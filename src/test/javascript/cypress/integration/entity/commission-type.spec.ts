import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('CommissionType e2e test', () => {
  const commissionTypePageUrl = '/commission-type';
  const commissionTypePageUrlPattern = new RegExp('/commission-type(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const commissionTypeSample = {};

  let commissionType: any;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/commission-types+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/commission-types').as('postEntityRequest');
    cy.intercept('DELETE', '/api/commission-types/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (commissionType) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/commission-types/${commissionType.id}`,
      }).then(() => {
        commissionType = undefined;
      });
    }
  });

  it('CommissionTypes menu should load CommissionTypes page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('commission-type');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('CommissionType').should('exist');
    cy.url().should('match', commissionTypePageUrlPattern);
  });

  describe('CommissionType page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(commissionTypePageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create CommissionType page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/commission-type/new$'));
        cy.getEntityCreateUpdateHeading('CommissionType');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', commissionTypePageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/commission-types',
          body: commissionTypeSample,
        }).then(({ body }) => {
          commissionType = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/commission-types+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/commission-types?page=0&size=20>; rel="last",<http://localhost/api/commission-types?page=0&size=20>; rel="first"',
              },
              body: [commissionType],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(commissionTypePageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details CommissionType page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('commissionType');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', commissionTypePageUrlPattern);
      });

      it('edit button click should load edit CommissionType page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('CommissionType');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', commissionTypePageUrlPattern);
      });

      it('last delete button click should delete instance of CommissionType', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('commissionType').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', commissionTypePageUrlPattern);

        commissionType = undefined;
      });
    });
  });

  describe('new CommissionType page', () => {
    beforeEach(() => {
      cy.visit(`${commissionTypePageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('CommissionType');
    });

    it('should create an instance of CommissionType', () => {
      cy.get(`[data-cy="tauxValue"]`).type('15085').should('have.value', '15085');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        commissionType = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', commissionTypePageUrlPattern);
    });
  });
});
