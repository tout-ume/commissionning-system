import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('ServiceType e2e test', () => {
  const serviceTypePageUrl = '/service-type';
  const serviceTypePageUrlPattern = new RegExp('/service-type(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const serviceTypeSample = {
    name: 'Checking',
    code: 'payment protocol 24/7',
    state: 'DEACTIVATED',
    isCosRelated: true,
    createdBy: 'b Multi-lateral',
  };

  let serviceType: any;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/service-types+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/service-types').as('postEntityRequest');
    cy.intercept('DELETE', '/api/service-types/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (serviceType) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/service-types/${serviceType.id}`,
      }).then(() => {
        serviceType = undefined;
      });
    }
  });

  it('ServiceTypes menu should load ServiceTypes page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('service-type');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('ServiceType').should('exist');
    cy.url().should('match', serviceTypePageUrlPattern);
  });

  describe('ServiceType page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(serviceTypePageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create ServiceType page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/service-type/new$'));
        cy.getEntityCreateUpdateHeading('ServiceType');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', serviceTypePageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/service-types',
          body: serviceTypeSample,
        }).then(({ body }) => {
          serviceType = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/service-types+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/service-types?page=0&size=20>; rel="last",<http://localhost/api/service-types?page=0&size=20>; rel="first"',
              },
              body: [serviceType],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(serviceTypePageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details ServiceType page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('serviceType');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', serviceTypePageUrlPattern);
      });

      it('edit button click should load edit ServiceType page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('ServiceType');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', serviceTypePageUrlPattern);
      });

      it('last delete button click should delete instance of ServiceType', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('serviceType').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', serviceTypePageUrlPattern);

        serviceType = undefined;
      });
    });
  });

  describe('new ServiceType page', () => {
    beforeEach(() => {
      cy.visit(`${serviceTypePageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('ServiceType');
    });

    it('should create an instance of ServiceType', () => {
      cy.get(`[data-cy="name"]`).type('de').should('have.value', 'de');

      cy.get(`[data-cy="code"]`).type('Marcadet global').should('have.value', 'Marcadet global');

      cy.get(`[data-cy="description"]`).type('Bourgogne non-volatile').should('have.value', 'Bourgogne non-volatile');

      cy.get(`[data-cy="state"]`).select('DEACTIVATED');

      cy.get(`[data-cy="isCosRelated"]`).should('not.be.checked');
      cy.get(`[data-cy="isCosRelated"]`).click().should('be.checked');

      cy.get(`[data-cy="commissioningPlanType"]`).select('MFS');

      cy.get(`[data-cy="createdBy"]`).type('cross-platform quantify a').should('have.value', 'cross-platform quantify a');

      cy.get(`[data-cy="createdDate"]`).type('2022-05-11T07:42').should('have.value', '2022-05-11T07:42');

      cy.get(`[data-cy="lastModifiedBy"]`).type('c calculating').should('have.value', 'c calculating');

      cy.get(`[data-cy="lastModifiedDate"]`).type('2022-05-11T07:58').should('have.value', '2022-05-11T07:58');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        serviceType = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', serviceTypePageUrlPattern);
    });
  });
});
