import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Frequency e2e test', () => {
  const frequencyPageUrl = '/frequency';
  const frequencyPageUrlPattern = new RegExp('/frequency(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const frequencySample = {
    name: 'Nord-Pas-de-Calais c',
    type: 'DAILY',
    operationType: 'PAYMENT',
    executionTime: 99919,
    createdBy: 'Re-engineered Shirt executive',
    createdDate: '2022-05-12T02:44:20.771Z',
  };

  let frequency: any;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/frequencies+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/frequencies').as('postEntityRequest');
    cy.intercept('DELETE', '/api/frequencies/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (frequency) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/frequencies/${frequency.id}`,
      }).then(() => {
        frequency = undefined;
      });
    }
  });

  it('Frequencies menu should load Frequencies page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('frequency');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Frequency').should('exist');
    cy.url().should('match', frequencyPageUrlPattern);
  });

  describe('Frequency page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(frequencyPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Frequency page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/frequency/new$'));
        cy.getEntityCreateUpdateHeading('Frequency');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', frequencyPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/frequencies',
          body: frequencySample,
        }).then(({ body }) => {
          frequency = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/frequencies+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/frequencies?page=0&size=20>; rel="last",<http://localhost/api/frequencies?page=0&size=20>; rel="first"',
              },
              body: [frequency],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(frequencyPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Frequency page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('frequency');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', frequencyPageUrlPattern);
      });

      it('edit button click should load edit Frequency page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Frequency');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', frequencyPageUrlPattern);
      });

      it('last delete button click should delete instance of Frequency', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('frequency').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', frequencyPageUrlPattern);

        frequency = undefined;
      });
    });
  });

  describe('new Frequency page', () => {
    beforeEach(() => {
      cy.visit(`${frequencyPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('Frequency');
    });

    it('should create an instance of Frequency', () => {
      cy.get(`[data-cy="name"]`).type('Bedfordshire').should('have.value', 'Bedfordshire');

      cy.get(`[data-cy="type"]`).select('WEEKLY');

      cy.get(`[data-cy="operationType"]`).select('CALCULUS');

      cy.get(`[data-cy="executionTime"]`).type('53409').should('have.value', '53409');

      cy.get(`[data-cy="executionWeekDay"]`).type('1').should('have.value', '1');

      cy.get(`[data-cy="executionMonthDay"]`).type('22').should('have.value', '22');

      cy.get(`[data-cy="daysAfter"]`).type('2').should('have.value', '2');

      cy.get(`[data-cy="createdBy"]`).type('Pérou').should('have.value', 'Pérou');

      cy.get(`[data-cy="createdDate"]`).type('2022-05-12T01:05').should('have.value', '2022-05-12T01:05');

      cy.get(`[data-cy="lastModifiedBy"]`).type('card').should('have.value', 'card');

      cy.get(`[data-cy="lastModifiedDate"]`).type('2022-05-11T22:02').should('have.value', '2022-05-11T22:02');

      cy.get(`[data-cy="cycle"]`).select('WEEKS');

      cy.get(`[data-cy="periodOfOccurrence"]`).select('MONTH');

      cy.get(`[data-cy="numberOfCycle"]`).type('55016').should('have.value', '55016');

      cy.get(`[data-cy="occurrenceByPeriod"]`).type('68304').should('have.value', '68304');

      cy.get(`[data-cy="datesOfOccurrence"]`).type('Concrete').should('have.value', 'Concrete');

      cy.get(`[data-cy="spare1"]`).type('Licensed extensible AGP').should('have.value', 'Licensed extensible AGP');

      cy.get(`[data-cy="spare2"]`).type('hack Awesome').should('have.value', 'hack Awesome');

      cy.get(`[data-cy="spare3"]`).type('Avon directional Metical').should('have.value', 'Avon directional Metical');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        frequency = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', frequencyPageUrlPattern);
    });
  });
});
