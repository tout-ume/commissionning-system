import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('CommissionAccount e2e test', () => {
  const commissionAccountPageUrl = '/commission-account';
  const commissionAccountPageUrlPattern = new RegExp('/commission-account(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const commissionAccountSample = {};

  let commissionAccount: any;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/commission-accounts+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/commission-accounts').as('postEntityRequest');
    cy.intercept('DELETE', '/api/commission-accounts/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (commissionAccount) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/commission-accounts/${commissionAccount.id}`,
      }).then(() => {
        commissionAccount = undefined;
      });
    }
  });

  it('CommissionAccounts menu should load CommissionAccounts page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('commission-account');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('CommissionAccount').should('exist');
    cy.url().should('match', commissionAccountPageUrlPattern);
  });

  describe('CommissionAccount page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(commissionAccountPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create CommissionAccount page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/commission-account/new$'));
        cy.getEntityCreateUpdateHeading('CommissionAccount');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', commissionAccountPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/commission-accounts',
          body: commissionAccountSample,
        }).then(({ body }) => {
          commissionAccount = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/commission-accounts+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/commission-accounts?page=0&size=20>; rel="last",<http://localhost/api/commission-accounts?page=0&size=20>; rel="first"',
              },
              body: [commissionAccount],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(commissionAccountPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details CommissionAccount page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('commissionAccount');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', commissionAccountPageUrlPattern);
      });

      it('edit button click should load edit CommissionAccount page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('CommissionAccount');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', commissionAccountPageUrlPattern);
      });

      it('last delete button click should delete instance of CommissionAccount', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('commissionAccount').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', commissionAccountPageUrlPattern);

        commissionAccount = undefined;
      });
    });
  });

  describe('new CommissionAccount page', () => {
    beforeEach(() => {
      cy.visit(`${commissionAccountPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('CommissionAccount');
    });

    it('should create an instance of CommissionAccount', () => {
      cy.get(`[data-cy="calculatedBalance"]`).type('6675').should('have.value', '6675');

      cy.get(`[data-cy="paidBalance"]`).type('79808').should('have.value', '79808');

      cy.get(`[data-cy="spare1"]`).type('withdrawal').should('have.value', 'withdrawal');

      cy.get(`[data-cy="spare2"]`).type('parsing').should('have.value', 'parsing');

      cy.get(`[data-cy="spare3"]`).type('generating hack').should('have.value', 'generating hack');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        commissionAccount = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', commissionAccountPageUrlPattern);
    });
  });
});
