import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('ConfigurationPlan e2e test', () => {
  const configurationPlanPageUrl = '/configuration-plan';
  const configurationPlanPageUrlPattern = new RegExp('/configuration-plan(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const configurationPlanSample = {};

  let configurationPlan: any;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/configuration-plans+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/configuration-plans').as('postEntityRequest');
    cy.intercept('DELETE', '/api/configuration-plans/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (configurationPlan) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/configuration-plans/${configurationPlan.id}`,
      }).then(() => {
        configurationPlan = undefined;
      });
    }
  });

  it('ConfigurationPlans menu should load ConfigurationPlans page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('configuration-plan');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('ConfigurationPlan').should('exist');
    cy.url().should('match', configurationPlanPageUrlPattern);
  });

  describe('ConfigurationPlan page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(configurationPlanPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create ConfigurationPlan page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/configuration-plan/new$'));
        cy.getEntityCreateUpdateHeading('ConfigurationPlan');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', configurationPlanPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/configuration-plans',
          body: configurationPlanSample,
        }).then(({ body }) => {
          configurationPlan = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/configuration-plans+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/configuration-plans?page=0&size=20>; rel="last",<http://localhost/api/configuration-plans?page=0&size=20>; rel="first"',
              },
              body: [configurationPlan],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(configurationPlanPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details ConfigurationPlan page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('configurationPlan');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', configurationPlanPageUrlPattern);
      });

      it('edit button click should load edit ConfigurationPlan page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('ConfigurationPlan');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', configurationPlanPageUrlPattern);
      });

      it('last delete button click should delete instance of ConfigurationPlan', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('configurationPlan').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', configurationPlanPageUrlPattern);

        configurationPlan = undefined;
      });
    });
  });

  describe('new ConfigurationPlan page', () => {
    beforeEach(() => {
      cy.visit(`${configurationPlanPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('ConfigurationPlan');
    });

    it('should create an instance of ConfigurationPlan', () => {
      cy.get(`[data-cy="spare1"]`).type('THX Nepalese SAS').should('have.value', 'THX Nepalese SAS');

      cy.get(`[data-cy="spare2"]`).type('Islands sticky').should('have.value', 'Islands sticky');

      cy.get(`[data-cy="spare3"]`).type('alarm Ball overriding').should('have.value', 'alarm Ball overriding');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        configurationPlan = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', configurationPlanPageUrlPattern);
    });
  });
});
