import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Partner e2e test', () => {
  const partnerPageUrl = '/partner';
  const partnerPageUrlPattern = new RegExp('/partner(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const partnerSample = { msisdn: 'Shoes orchestration', createdBy: 'Comores' };

  let partner: any;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/partners+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/partners').as('postEntityRequest');
    cy.intercept('DELETE', '/api/partners/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (partner) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/partners/${partner.id}`,
      }).then(() => {
        partner = undefined;
      });
    }
  });

  it('Partners menu should load Partners page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('partner');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Partner').should('exist');
    cy.url().should('match', partnerPageUrlPattern);
  });

  describe('Partner page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(partnerPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Partner page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/partner/new$'));
        cy.getEntityCreateUpdateHeading('Partner');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', partnerPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/partners',
          body: partnerSample,
        }).then(({ body }) => {
          partner = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/partners+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/partners?page=0&size=20>; rel="last",<http://localhost/api/partners?page=0&size=20>; rel="first"',
              },
              body: [partner],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(partnerPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Partner page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('partner');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', partnerPageUrlPattern);
      });

      it('edit button click should load edit Partner page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Partner');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', partnerPageUrlPattern);
      });

      it('last delete button click should delete instance of Partner', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('partner').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', partnerPageUrlPattern);

        partner = undefined;
      });
    });
  });

  describe('new Partner page', () => {
    beforeEach(() => {
      cy.visit(`${partnerPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('Partner');
    });

    it('should create an instance of Partner', () => {
      cy.get(`[data-cy="msisdn"]`).type('convergence orange bifurcated').should('have.value', 'convergence orange bifurcated');

      cy.get(`[data-cy="name"]`).type('Awesome').should('have.value', 'Awesome');

      cy.get(`[data-cy="surname"]`).type('Italie hacking').should('have.value', 'Italie hacking');

      cy.get(`[data-cy="state"]`).type('Unbranded Sleek').should('have.value', 'Unbranded Sleek');

      cy.get(`[data-cy="partnerProfileId"]`).type('42214').should('have.value', '42214');

      cy.get(`[data-cy="parentId"]`).type('65723').should('have.value', '65723');

      cy.get(`[data-cy="canResetPin"]`).should('not.be.checked');
      cy.get(`[data-cy="canResetPin"]`).click().should('be.checked');

      cy.get(`[data-cy="createdBy"]`).type('deploy c Franc').should('have.value', 'deploy c Franc');

      cy.get(`[data-cy="createdDate"]`).type('2022-05-09T23:07').should('have.value', '2022-05-09T23:07');

      cy.get(`[data-cy="lastModifiedBy"]`).type('Bedfordshire bandwidth').should('have.value', 'Bedfordshire bandwidth');

      cy.get(`[data-cy="lastModifiedDate"]`).type('2022-05-10T11:06').should('have.value', '2022-05-10T11:06');

      cy.get(`[data-cy="lastTransactionDate"]`).type('2022-05-10T10:10').should('have.value', '2022-05-10T10:10');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        partner = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', partnerPageUrlPattern);
    });
  });
});
