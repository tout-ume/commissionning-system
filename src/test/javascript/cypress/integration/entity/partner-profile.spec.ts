import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('PartnerProfile e2e test', () => {
  const partnerProfilePageUrl = '/partner-profile';
  const partnerProfilePageUrlPattern = new RegExp('/partner-profile(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const partnerProfileSample = { code: 'Kids', name: '6th bus Wooden', createdBy: 'invoice' };

  let partnerProfile: any;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/partner-profiles+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/partner-profiles').as('postEntityRequest');
    cy.intercept('DELETE', '/api/partner-profiles/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (partnerProfile) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/partner-profiles/${partnerProfile.id}`,
      }).then(() => {
        partnerProfile = undefined;
      });
    }
  });

  it('PartnerProfiles menu should load PartnerProfiles page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('partner-profile');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('PartnerProfile').should('exist');
    cy.url().should('match', partnerProfilePageUrlPattern);
  });

  describe('PartnerProfile page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(partnerProfilePageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create PartnerProfile page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/partner-profile/new$'));
        cy.getEntityCreateUpdateHeading('PartnerProfile');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', partnerProfilePageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/partner-profiles',
          body: partnerProfileSample,
        }).then(({ body }) => {
          partnerProfile = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/partner-profiles+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/partner-profiles?page=0&size=20>; rel="last",<http://localhost/api/partner-profiles?page=0&size=20>; rel="first"',
              },
              body: [partnerProfile],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(partnerProfilePageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details PartnerProfile page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('partnerProfile');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', partnerProfilePageUrlPattern);
      });

      it('edit button click should load edit PartnerProfile page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('PartnerProfile');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', partnerProfilePageUrlPattern);
      });

      it('last delete button click should delete instance of PartnerProfile', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('partnerProfile').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', partnerProfilePageUrlPattern);

        partnerProfile = undefined;
      });
    });
  });

  describe('new PartnerProfile page', () => {
    beforeEach(() => {
      cy.visit(`${partnerProfilePageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('PartnerProfile');
    });

    it('should create an instance of PartnerProfile', () => {
      cy.get(`[data-cy="code"]`).type('compress').should('have.value', 'compress');

      cy.get(`[data-cy="name"]`).type('Midi-Pyrénées').should('have.value', 'Midi-Pyrénées');

      cy.get(`[data-cy="description"]`).type('Paris software').should('have.value', 'Paris software');

      cy.get(`[data-cy="level"]`).type('64380').should('have.value', '64380');

      cy.get(`[data-cy="parentPartnerProfileId"]`).type('73929').should('have.value', '73929');

      cy.get(`[data-cy="createdBy"]`).type('PCI').should('have.value', 'PCI');

      cy.get(`[data-cy="createdDate"]`).type('2022-05-12T09:15').should('have.value', '2022-05-12T09:15');

      cy.get(`[data-cy="lastModifiedBy"]`).type('équatoriale a Credit').should('have.value', 'équatoriale a Credit');

      cy.get(`[data-cy="lastModifiedDate"]`).type('2022-05-12T01:30').should('have.value', '2022-05-12T01:30');

      cy.get(`[data-cy="canHaveMultipleZones"]`).should('not.be.checked');
      cy.get(`[data-cy="canHaveMultipleZones"]`).click().should('be.checked');

      cy.get(`[data-cy="zoneThreshold"]`).type('78411').should('have.value', '78411');

      cy.get(`[data-cy="simBackupThreshold"]`).type('66186').should('have.value', '66186');

      cy.get(`[data-cy="canDoProfileAssignment"]`).should('not.be.checked');
      cy.get(`[data-cy="canDoProfileAssignment"]`).click().should('be.checked');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        partnerProfile = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', partnerProfilePageUrlPattern);
    });
  });
});
