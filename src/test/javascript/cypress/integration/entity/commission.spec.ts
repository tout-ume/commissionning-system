import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Commission e2e test', () => {
  const commissionPageUrl = '/commission';
  const commissionPageUrlPattern = new RegExp('/commission(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const commissionSample = { amount: 76557 };

  let commission: any;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/commissions+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/commissions').as('postEntityRequest');
    cy.intercept('DELETE', '/api/commissions/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (commission) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/commissions/${commission.id}`,
      }).then(() => {
        commission = undefined;
      });
    }
  });

  it('Commissions menu should load Commissions page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('commission');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Commission').should('exist');
    cy.url().should('match', commissionPageUrlPattern);
  });

  describe('Commission page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(commissionPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Commission page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/commission/new$'));
        cy.getEntityCreateUpdateHeading('Commission');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', commissionPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/commissions',
          body: commissionSample,
        }).then(({ body }) => {
          commission = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/commissions+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/commissions?page=0&size=20>; rel="last",<http://localhost/api/commissions?page=0&size=20>; rel="first"',
              },
              body: [commission],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(commissionPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Commission page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('commission');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', commissionPageUrlPattern);
      });

      it('edit button click should load edit Commission page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Commission');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', commissionPageUrlPattern);
      });

      it('last delete button click should delete instance of Commission', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('commission').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', commissionPageUrlPattern);

        commission = undefined;
      });
    });
  });

  describe('new Commission page', () => {
    beforeEach(() => {
      cy.visit(`${commissionPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('Commission');
    });

    it('should create an instance of Commission', () => {
      cy.get(`[data-cy="amount"]`).type('2615').should('have.value', '2615');

      cy.get(`[data-cy="calculatedAt"]`).type('2022-05-24T19:46').should('have.value', '2022-05-24T19:46');

      cy.get(`[data-cy="calculationDate"]`).type('2022-05-25T03:30').should('have.value', '2022-05-25T03:30');

      cy.get(`[data-cy="calculationShortDate"]`).type('2022-05-24').should('have.value', '2022-05-24');

      cy.get(`[data-cy="senderMsisdn"]`)
        .type('synthesize Saint-Dominique Checking')
        .should('have.value', 'synthesize Saint-Dominique Checking');

      cy.get(`[data-cy="senderProfile"]`).type('Alsace').should('have.value', 'Alsace');

      cy.get(`[data-cy="serviceType"]`).type('impactful Borders').should('have.value', 'impactful Borders');

      cy.get(`[data-cy="commissionPaymentStatus"]`).select('TO_BE_PAID');

      cy.get(`[data-cy="commissioningPlanType"]`).select('MOBILE');

      cy.get(`[data-cy="globalNetworkCommissionAmount"]`).type('49966').should('have.value', '49966');

      cy.get(`[data-cy="transactionsAmount"]`).type('57920').should('have.value', '57920');

      cy.get(`[data-cy="fraudAmount"]`).type('85103').should('have.value', '85103');

      cy.get(`[data-cy="spare1"]`).type('Saint-Honoré bluetooth driver').should('have.value', 'Saint-Honoré bluetooth driver');

      cy.get(`[data-cy="spare2"]`).type('deposit la').should('have.value', 'deposit la');

      cy.get(`[data-cy="spare3"]`).type('Cambridgeshire Concrete coherent').should('have.value', 'Cambridgeshire Concrete coherent');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        commission = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', commissionPageUrlPattern);
    });
  });
});
