import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('CommissioningPlan e2e test', () => {
  const commissioningPlanPageUrl = '/commissioning-plan';
  const commissioningPlanPageUrlPattern = new RegExp('/commissioning-plan(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const commissioningPlanSample = {
    name: 'Bourgogne Money c',
    beginDate: '2022-05-11T10:34:01.016Z',
    endDate: '2022-05-10T15:20:59.571Z',
    creationDate: '2022-05-11T09:39:15.948Z',
    createdBy: 'Digitized parse Money',
    state: 'IN_USE',
  };

  let commissioningPlan: any;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/commissioning-plans+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/commissioning-plans').as('postEntityRequest');
    cy.intercept('DELETE', '/api/commissioning-plans/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (commissioningPlan) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/commissioning-plans/${commissioningPlan.id}`,
      }).then(() => {
        commissioningPlan = undefined;
      });
    }
  });

  it('CommissioningPlans menu should load CommissioningPlans page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('commissioning-plan');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('CommissioningPlan').should('exist');
    cy.url().should('match', commissioningPlanPageUrlPattern);
  });

  describe('CommissioningPlan page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(commissioningPlanPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create CommissioningPlan page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/commissioning-plan/new$'));
        cy.getEntityCreateUpdateHeading('CommissioningPlan');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', commissioningPlanPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/commissioning-plans',
          body: commissioningPlanSample,
        }).then(({ body }) => {
          commissioningPlan = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/commissioning-plans+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/commissioning-plans?page=0&size=20>; rel="last",<http://localhost/api/commissioning-plans?page=0&size=20>; rel="first"',
              },
              body: [commissioningPlan],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(commissioningPlanPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details CommissioningPlan page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('commissioningPlan');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', commissioningPlanPageUrlPattern);
      });

      it('edit button click should load edit CommissioningPlan page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('CommissioningPlan');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', commissioningPlanPageUrlPattern);
      });

      it('last delete button click should delete instance of CommissioningPlan', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('commissioningPlan').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', commissioningPlanPageUrlPattern);

        commissioningPlan = undefined;
      });
    });
  });

  describe('new CommissioningPlan page', () => {
    beforeEach(() => {
      cy.visit(`${commissioningPlanPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('CommissioningPlan');
    });

    it('should create an instance of CommissioningPlan', () => {
      cy.get(`[data-cy="name"]`).type('solid Loan executive').should('have.value', 'solid Loan executive');

      cy.get(`[data-cy="beginDate"]`).type('2022-05-11T04:27').should('have.value', '2022-05-11T04:27');

      cy.get(`[data-cy="endDate"]`).type('2022-05-11T12:44').should('have.value', '2022-05-11T12:44');

      cy.get(`[data-cy="creationDate"]`).type('2022-05-11T08:26').should('have.value', '2022-05-11T08:26');

      cy.get(`[data-cy="createdBy"]`).type('withdrawal').should('have.value', 'withdrawal');

      cy.get(`[data-cy="archiveDate"]`).type('2022-05-11T08:48').should('have.value', '2022-05-11T08:48');

      cy.get(`[data-cy="archivedBy"]`).type('deposit Tools Vision-oriented').should('have.value', 'deposit Tools Vision-oriented');

      cy.get(`[data-cy="paymentStrategy"]`).select('AUTOMATIC');

      cy.get(`[data-cy="state"]`).select('IN_USE');

      cy.get(`[data-cy="commissioningPlanType"]`).select('MFS');

      cy.get(`[data-cy="spare1"]`).type('Organized Industrial').should('have.value', 'Organized Industrial');

      cy.get(`[data-cy="spare2"]`).type('capacitor b Granite').should('have.value', 'capacitor b Granite');

      cy.get(`[data-cy="spare3"]`).type('a interfaces protocol').should('have.value', 'a interfaces protocol');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        commissioningPlan = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', commissioningPlanPageUrlPattern);
    });
  });
});
