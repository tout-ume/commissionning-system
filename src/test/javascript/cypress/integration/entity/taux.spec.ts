import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Taux e2e test', () => {
  const tauxPageUrl = '/taux';
  const tauxPageUrlPattern = new RegExp('/taux(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const tauxSample = { value: 18 };

  let taux: any;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/tauxes+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/tauxes').as('postEntityRequest');
    cy.intercept('DELETE', '/api/tauxes/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (taux) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/tauxes/${taux.id}`,
      }).then(() => {
        taux = undefined;
      });
    }
  });

  it('Tauxes menu should load Tauxes page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('taux');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Taux').should('exist');
    cy.url().should('match', tauxPageUrlPattern);
  });

  describe('Taux page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(tauxPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Taux page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/taux/new$'));
        cy.getEntityCreateUpdateHeading('Taux');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', tauxPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/tauxes',
          body: tauxSample,
        }).then(({ body }) => {
          taux = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/tauxes+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/tauxes?page=0&size=20>; rel="last",<http://localhost/api/tauxes?page=0&size=20>; rel="first"',
              },
              body: [taux],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(tauxPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Taux page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('taux');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', tauxPageUrlPattern);
      });

      it('edit button click should load edit Taux page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Taux');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', tauxPageUrlPattern);
      });

      it('last delete button click should delete instance of Taux', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('taux').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', tauxPageUrlPattern);

        taux = undefined;
      });
    });
  });

  describe('new Taux page', () => {
    beforeEach(() => {
      cy.visit(`${tauxPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('Taux');
    });

    it('should create an instance of Taux', () => {
      cy.get(`[data-cy="value"]`).type('7').should('have.value', '7');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        taux = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', tauxPageUrlPattern);
    });
  });
});
