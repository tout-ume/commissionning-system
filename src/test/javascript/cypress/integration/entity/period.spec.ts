import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Period e2e test', () => {
  const periodPageUrl = '/period';
  const periodPageUrlPattern = new RegExp('/period(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const periodSample = {
    periodType: 'DAILY',
    operationType: 'PAYMENT',
    createdBy: 'Cambridgeshire Games',
    createdDate: '2022-07-04T08:54:24.890Z',
  };

  let period: any;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/periods+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/periods').as('postEntityRequest');
    cy.intercept('DELETE', '/api/periods/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (period) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/periods/${period.id}`,
      }).then(() => {
        period = undefined;
      });
    }
  });

  it('Periods menu should load Periods page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('period');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Period').should('exist');
    cy.url().should('match', periodPageUrlPattern);
  });

  describe('Period page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(periodPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Period page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/period/new$'));
        cy.getEntityCreateUpdateHeading('Period');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', periodPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/periods',
          body: periodSample,
        }).then(({ body }) => {
          period = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/periods+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/periods?page=0&size=20>; rel="last",<http://localhost/api/periods?page=0&size=20>; rel="first"',
              },
              body: [period],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(periodPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Period page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('period');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', periodPageUrlPattern);
      });

      it('edit button click should load edit Period page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Period');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', periodPageUrlPattern);
      });

      it('last delete button click should delete instance of Period', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('period').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', periodPageUrlPattern);

        period = undefined;
      });
    });
  });

  describe('new Period page', () => {
    beforeEach(() => {
      cy.visit(`${periodPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('Period');
    });

    it('should create an instance of Period', () => {
      cy.get(`[data-cy="monthDayFrom"]`).type('41264').should('have.value', '41264');

      cy.get(`[data-cy="monthDayTo"]`).type('45623').should('have.value', '45623');

      cy.get(`[data-cy="weekDayFrom"]`).type('95477').should('have.value', '95477');

      cy.get(`[data-cy="weekDayTo"]`).type('89994').should('have.value', '89994');

      cy.get(`[data-cy="dayHourFrom"]`).type('25419').should('have.value', '25419');

      cy.get(`[data-cy="dayHourTo"]`).type('86412').should('have.value', '86412');

      cy.get(`[data-cy="isPreviousDayHourFrom"]`).should('not.be.checked');
      cy.get(`[data-cy="isPreviousDayHourFrom"]`).click().should('be.checked');

      cy.get(`[data-cy="isPreviousDayHourTo"]`).should('not.be.checked');
      cy.get(`[data-cy="isPreviousDayHourTo"]`).click().should('be.checked');

      cy.get(`[data-cy="isPreviousMonthDayFrom"]`).should('not.be.checked');
      cy.get(`[data-cy="isPreviousMonthDayFrom"]`).click().should('be.checked');

      cy.get(`[data-cy="isPreviousMonthDayTo"]`).should('not.be.checked');
      cy.get(`[data-cy="isPreviousMonthDayTo"]`).click().should('be.checked');

      cy.get(`[data-cy="isCompleteDay"]`).should('not.be.checked');
      cy.get(`[data-cy="isCompleteDay"]`).click().should('be.checked');

      cy.get(`[data-cy="isCompleteMonth"]`).should('not.be.checked');
      cy.get(`[data-cy="isCompleteMonth"]`).click().should('be.checked');

      cy.get(`[data-cy="periodType"]`).select('SEMI_ANNUALLY');

      cy.get(`[data-cy="operationType"]`).select('PAYMENT');

      cy.get(`[data-cy="createdBy"]`).type('Market Concrete Ergonomic').should('have.value', 'Market Concrete Ergonomic');

      cy.get(`[data-cy="createdDate"]`).type('2022-07-04T05:32').should('have.value', '2022-07-04T05:32');

      cy.get(`[data-cy="lastModifiedBy"]`).type('Intelligent sexy Plastic').should('have.value', 'Intelligent sexy Plastic');

      cy.get(`[data-cy="lastModifiedDate"]`).type('2022-07-04T08:38').should('have.value', '2022-07-04T08:38');

      cy.get(`[data-cy="daysOrDatesOfOccurrence"]`).type('strategize').should('have.value', 'strategize');

      cy.get(`[data-cy="spare1"]`).type('redefine Tasty copy').should('have.value', 'redefine Tasty copy');

      cy.get(`[data-cy="spare2"]`).type('Fresh Administrateur').should('have.value', 'Fresh Administrateur');

      cy.get(`[data-cy="spare3"]`).type('Presbourg').should('have.value', 'Presbourg');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        period = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', periodPageUrlPattern);
    });
  });
});
