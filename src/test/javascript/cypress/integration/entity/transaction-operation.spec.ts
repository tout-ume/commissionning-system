import { entityItemSelector } from '../../support/commands';
import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('TransactionOperation e2e test', () => {
  const transactionOperationPageUrl = '/transaction-operation';
  const transactionOperationPageUrlPattern = new RegExp('/transaction-operation(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const transactionOperationSample = {};

  let transactionOperation: any;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/transaction-operations+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/transaction-operations').as('postEntityRequest');
    cy.intercept('DELETE', '/api/transaction-operations/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (transactionOperation) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/transaction-operations/${transactionOperation.id}`,
      }).then(() => {
        transactionOperation = undefined;
      });
    }
  });

  it('TransactionOperations menu should load TransactionOperations page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('transaction-operation');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response!.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('TransactionOperation').should('exist');
    cy.url().should('match', transactionOperationPageUrlPattern);
  });

  describe('TransactionOperation page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(transactionOperationPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create TransactionOperation page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/transaction-operation/new$'));
        cy.getEntityCreateUpdateHeading('TransactionOperation');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', transactionOperationPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/transaction-operations',
          body: transactionOperationSample,
        }).then(({ body }) => {
          transactionOperation = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/transaction-operations+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              headers: {
                link: '<http://localhost/api/transaction-operations?page=0&size=20>; rel="last",<http://localhost/api/transaction-operations?page=0&size=20>; rel="first"',
              },
              body: [transactionOperation],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(transactionOperationPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details TransactionOperation page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('transactionOperation');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', transactionOperationPageUrlPattern);
      });

      it('edit button click should load edit TransactionOperation page', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('TransactionOperation');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', transactionOperationPageUrlPattern);
      });

      it('last delete button click should delete instance of TransactionOperation', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('transactionOperation').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response!.statusCode).to.equal(200);
        });
        cy.url().should('match', transactionOperationPageUrlPattern);

        transactionOperation = undefined;
      });
    });
  });

  describe('new TransactionOperation page', () => {
    beforeEach(() => {
      cy.visit(`${transactionOperationPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('TransactionOperation');
    });

    it('should create an instance of TransactionOperation', () => {
      cy.get(`[data-cy="amount"]`).type('66509').should('have.value', '66509');

      cy.get(`[data-cy="subsMsisdn"]`).type('Laffitte').should('have.value', 'Laffitte');

      cy.get(`[data-cy="agentMsisdn"]`).type('haptic Berkshire').should('have.value', 'haptic Berkshire');

      cy.get(`[data-cy="typeTransaction"]`).type('solution PNG Savings').should('have.value', 'solution PNG Savings');

      cy.get(`[data-cy="createdAt"]`).type('2022-05-24T23:19').should('have.value', '2022-05-24T23:19');

      cy.get(`[data-cy="transactionStatus"]`).type('invoice a magenta').should('have.value', 'invoice a magenta');

      cy.get(`[data-cy="senderZone"]`).type('Borders').should('have.value', 'Borders');

      cy.get(`[data-cy="senderProfile"]`).type('portal').should('have.value', 'portal');

      cy.get(`[data-cy="codeTerritory"]`).type('Assimilated Borders').should('have.value', 'Assimilated Borders');

      cy.get(`[data-cy="subType"]`).type('Credit context-sensitive Bourgogne').should('have.value', 'Credit context-sensitive Bourgogne');

      cy.get(`[data-cy="operationShortDate"]`).type('2022-05-25').should('have.value', '2022-05-25');

      cy.get(`[data-cy="operationDate"]`).type('2022-05-25T11:47').should('have.value', '2022-05-25T11:47');

      cy.get(`[data-cy="isFraud"]`).should('not.be.checked');
      cy.get(`[data-cy="isFraud"]`).click().should('be.checked');

      cy.get(`[data-cy="taggedAt"]`).type('2022-05-25T01:11').should('have.value', '2022-05-25T01:11');

      cy.get(`[data-cy="fraudSource"]`).type('Architecte').should('have.value', 'Architecte');

      cy.get(`[data-cy="comment"]`).type('invoice').should('have.value', 'invoice');

      cy.get(`[data-cy="falsePositiveDetectedAt"]`).type('2022-05-25T15:32').should('have.value', '2022-05-25T15:32');

      cy.get(`[data-cy="tid"]`).type('real-time Incredible Rustic').should('have.value', 'real-time Incredible Rustic');

      cy.get(`[data-cy="parentMsisdn"]`).type('Rubber').should('have.value', 'Rubber');

      cy.get(`[data-cy="fctDt"]`).type('wireless').should('have.value', 'wireless');

      cy.get(`[data-cy="parentId"]`).type('New Account Mozambique').should('have.value', 'New Account Mozambique');

      cy.get(`[data-cy="canceledAt"]`).type('2022-05-24T17:39').should('have.value', '2022-05-24T17:39');

      cy.get(`[data-cy="canceledId"]`).type('index').should('have.value', 'index');

      cy.get(`[data-cy="productId"]`).type('Montmorency').should('have.value', 'Montmorency');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(201);
        transactionOperation = response!.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response!.statusCode).to.equal(200);
      });
      cy.url().should('match', transactionOperationPageUrlPattern);
    });
  });
});
