package sn.free.commissioning.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class AuthorityMirrorTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AuthorityMirror.class);
        AuthorityMirror authorityMirror1 = new AuthorityMirror();
        authorityMirror1.setId(1L);
        AuthorityMirror authorityMirror2 = new AuthorityMirror();
        authorityMirror2.setId(authorityMirror1.getId());
        assertThat(authorityMirror1).isEqualTo(authorityMirror2);
        authorityMirror2.setId(2L);
        assertThat(authorityMirror1).isNotEqualTo(authorityMirror2);
        authorityMirror1.setId(null);
        assertThat(authorityMirror1).isNotEqualTo(authorityMirror2);
    }
}
