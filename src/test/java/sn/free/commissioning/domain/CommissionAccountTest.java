package sn.free.commissioning.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class CommissionAccountTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CommissionAccount.class);
        CommissionAccount commissionAccount1 = new CommissionAccount();
        commissionAccount1.setId(1L);
        CommissionAccount commissionAccount2 = new CommissionAccount();
        commissionAccount2.setId(commissionAccount1.getId());
        assertThat(commissionAccount1).isEqualTo(commissionAccount2);
        commissionAccount2.setId(2L);
        assertThat(commissionAccount1).isNotEqualTo(commissionAccount2);
        commissionAccount1.setId(null);
        assertThat(commissionAccount1).isNotEqualTo(commissionAccount2);
    }
}
