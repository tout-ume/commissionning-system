package sn.free.commissioning.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class ConfigurationPlanTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ConfigurationPlan.class);
        ConfigurationPlan configurationPlan1 = new ConfigurationPlan();
        configurationPlan1.setId(1L);
        ConfigurationPlan configurationPlan2 = new ConfigurationPlan();
        configurationPlan2.setId(configurationPlan1.getId());
        assertThat(configurationPlan1).isEqualTo(configurationPlan2);
        configurationPlan2.setId(2L);
        assertThat(configurationPlan1).isNotEqualTo(configurationPlan2);
        configurationPlan1.setId(null);
        assertThat(configurationPlan1).isNotEqualTo(configurationPlan2);
    }
}
