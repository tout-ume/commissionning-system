package sn.free.commissioning.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class TransactionOperationTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TransactionOperation.class);
        TransactionOperation transactionOperation1 = new TransactionOperation();
        transactionOperation1.setId(1L);
        TransactionOperation transactionOperation2 = new TransactionOperation();
        transactionOperation2.setId(transactionOperation1.getId());
        assertThat(transactionOperation1).isEqualTo(transactionOperation2);
        transactionOperation2.setId(2L);
        assertThat(transactionOperation1).isNotEqualTo(transactionOperation2);
        transactionOperation1.setId(null);
        assertThat(transactionOperation1).isNotEqualTo(transactionOperation2);
    }
}
