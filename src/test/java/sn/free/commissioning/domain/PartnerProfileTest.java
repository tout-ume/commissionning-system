package sn.free.commissioning.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class PartnerProfileTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PartnerProfile.class);
        PartnerProfile partnerProfile1 = new PartnerProfile();
        partnerProfile1.setId(1L);
        PartnerProfile partnerProfile2 = new PartnerProfile();
        partnerProfile2.setId(partnerProfile1.getId());
        assertThat(partnerProfile1).isEqualTo(partnerProfile2);
        partnerProfile2.setId(2L);
        assertThat(partnerProfile1).isNotEqualTo(partnerProfile2);
        partnerProfile1.setId(null);
        assertThat(partnerProfile1).isNotEqualTo(partnerProfile2);
    }
}
