package sn.free.commissioning.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class MissedTransactionOperationArchiveTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MissedTransactionOperationArchive.class);
        MissedTransactionOperationArchive missedTransactionOperationArchive1 = new MissedTransactionOperationArchive();
        missedTransactionOperationArchive1.setId(1L);
        MissedTransactionOperationArchive missedTransactionOperationArchive2 = new MissedTransactionOperationArchive();
        missedTransactionOperationArchive2.setId(missedTransactionOperationArchive1.getId());
        assertThat(missedTransactionOperationArchive1).isEqualTo(missedTransactionOperationArchive2);
        missedTransactionOperationArchive2.setId(2L);
        assertThat(missedTransactionOperationArchive1).isNotEqualTo(missedTransactionOperationArchive2);
        missedTransactionOperationArchive1.setId(null);
        assertThat(missedTransactionOperationArchive1).isNotEqualTo(missedTransactionOperationArchive2);
    }
}
