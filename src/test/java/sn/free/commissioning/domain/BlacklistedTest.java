package sn.free.commissioning.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class BlacklistedTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Blacklisted.class);
        Blacklisted blacklisted1 = new Blacklisted();
        blacklisted1.setId(1L);
        Blacklisted blacklisted2 = new Blacklisted();
        blacklisted2.setId(blacklisted1.getId());
        assertThat(blacklisted1).isEqualTo(blacklisted2);
        blacklisted2.setId(2L);
        assertThat(blacklisted1).isNotEqualTo(blacklisted2);
        blacklisted1.setId(null);
        assertThat(blacklisted1).isNotEqualTo(blacklisted2);
    }
}
