package sn.free.commissioning.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class MissedTransactionOperationTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MissedTransactionOperation.class);
        MissedTransactionOperation missedTransactionOperation1 = new MissedTransactionOperation();
        missedTransactionOperation1.setId(1L);
        MissedTransactionOperation missedTransactionOperation2 = new MissedTransactionOperation();
        missedTransactionOperation2.setId(missedTransactionOperation1.getId());
        assertThat(missedTransactionOperation1).isEqualTo(missedTransactionOperation2);
        missedTransactionOperation2.setId(2L);
        assertThat(missedTransactionOperation1).isNotEqualTo(missedTransactionOperation2);
        missedTransactionOperation1.setId(null);
        assertThat(missedTransactionOperation1).isNotEqualTo(missedTransactionOperation2);
    }
}
