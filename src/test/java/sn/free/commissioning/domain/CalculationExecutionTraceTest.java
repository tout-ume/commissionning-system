package sn.free.commissioning.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class CalculationExecutionTraceTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CalculationExecutionTrace.class);
        CalculationExecutionTrace calculationExecutionTrace1 = new CalculationExecutionTrace();
        calculationExecutionTrace1.setId(1L);
        CalculationExecutionTrace calculationExecutionTrace2 = new CalculationExecutionTrace();
        calculationExecutionTrace2.setId(calculationExecutionTrace1.getId());
        assertThat(calculationExecutionTrace1).isEqualTo(calculationExecutionTrace2);
        calculationExecutionTrace2.setId(2L);
        assertThat(calculationExecutionTrace1).isNotEqualTo(calculationExecutionTrace2);
        calculationExecutionTrace1.setId(null);
        assertThat(calculationExecutionTrace1).isNotEqualTo(calculationExecutionTrace2);
    }
}
