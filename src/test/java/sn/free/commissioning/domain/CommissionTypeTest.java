package sn.free.commissioning.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class CommissionTypeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CommissionType.class);
        CommissionType commissionType1 = new CommissionType();
        commissionType1.setId(1L);
        CommissionType commissionType2 = new CommissionType();
        commissionType2.setId(commissionType1.getId());
        assertThat(commissionType1).isEqualTo(commissionType2);
        commissionType2.setId(2L);
        assertThat(commissionType1).isNotEqualTo(commissionType2);
        commissionType1.setId(null);
        assertThat(commissionType1).isNotEqualTo(commissionType2);
    }
}
