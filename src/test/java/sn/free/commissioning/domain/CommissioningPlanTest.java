package sn.free.commissioning.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class CommissioningPlanTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CommissioningPlan.class);
        CommissioningPlan commissioningPlan1 = new CommissioningPlan();
        commissioningPlan1.setId(1L);
        CommissioningPlan commissioningPlan2 = new CommissioningPlan();
        commissioningPlan2.setId(commissioningPlan1.getId());
        assertThat(commissioningPlan1).isEqualTo(commissioningPlan2);
        commissioningPlan2.setId(2L);
        assertThat(commissioningPlan1).isNotEqualTo(commissioningPlan2);
        commissioningPlan1.setId(null);
        assertThat(commissioningPlan1).isNotEqualTo(commissioningPlan2);
    }
}
