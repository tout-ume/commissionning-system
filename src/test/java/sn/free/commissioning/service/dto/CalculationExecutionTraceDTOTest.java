package sn.free.commissioning.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class CalculationExecutionTraceDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CalculationExecutionTraceDTO.class);
        CalculationExecutionTraceDTO calculationExecutionTraceDTO1 = new CalculationExecutionTraceDTO();
        calculationExecutionTraceDTO1.setId(1L);
        CalculationExecutionTraceDTO calculationExecutionTraceDTO2 = new CalculationExecutionTraceDTO();
        assertThat(calculationExecutionTraceDTO1).isNotEqualTo(calculationExecutionTraceDTO2);
        calculationExecutionTraceDTO2.setId(calculationExecutionTraceDTO1.getId());
        assertThat(calculationExecutionTraceDTO1).isEqualTo(calculationExecutionTraceDTO2);
        calculationExecutionTraceDTO2.setId(2L);
        assertThat(calculationExecutionTraceDTO1).isNotEqualTo(calculationExecutionTraceDTO2);
        calculationExecutionTraceDTO1.setId(null);
        assertThat(calculationExecutionTraceDTO1).isNotEqualTo(calculationExecutionTraceDTO2);
    }
}
