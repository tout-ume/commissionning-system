package sn.free.commissioning.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class PartnerProfileDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PartnerProfileDTO.class);
        PartnerProfileDTO partnerProfileDTO1 = new PartnerProfileDTO();
        partnerProfileDTO1.setId(1L);
        PartnerProfileDTO partnerProfileDTO2 = new PartnerProfileDTO();
        assertThat(partnerProfileDTO1).isNotEqualTo(partnerProfileDTO2);
        partnerProfileDTO2.setId(partnerProfileDTO1.getId());
        assertThat(partnerProfileDTO1).isEqualTo(partnerProfileDTO2);
        partnerProfileDTO2.setId(2L);
        assertThat(partnerProfileDTO1).isNotEqualTo(partnerProfileDTO2);
        partnerProfileDTO1.setId(null);
        assertThat(partnerProfileDTO1).isNotEqualTo(partnerProfileDTO2);
    }
}
