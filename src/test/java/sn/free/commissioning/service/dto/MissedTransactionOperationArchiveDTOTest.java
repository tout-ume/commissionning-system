package sn.free.commissioning.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class MissedTransactionOperationArchiveDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MissedTransactionOperationArchiveDTO.class);
        MissedTransactionOperationArchiveDTO missedTransactionOperationArchiveDTO1 = new MissedTransactionOperationArchiveDTO();
        missedTransactionOperationArchiveDTO1.setId(1L);
        MissedTransactionOperationArchiveDTO missedTransactionOperationArchiveDTO2 = new MissedTransactionOperationArchiveDTO();
        assertThat(missedTransactionOperationArchiveDTO1).isNotEqualTo(missedTransactionOperationArchiveDTO2);
        missedTransactionOperationArchiveDTO2.setId(missedTransactionOperationArchiveDTO1.getId());
        assertThat(missedTransactionOperationArchiveDTO1).isEqualTo(missedTransactionOperationArchiveDTO2);
        missedTransactionOperationArchiveDTO2.setId(2L);
        assertThat(missedTransactionOperationArchiveDTO1).isNotEqualTo(missedTransactionOperationArchiveDTO2);
        missedTransactionOperationArchiveDTO1.setId(null);
        assertThat(missedTransactionOperationArchiveDTO1).isNotEqualTo(missedTransactionOperationArchiveDTO2);
    }
}
