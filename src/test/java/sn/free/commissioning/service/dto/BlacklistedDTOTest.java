package sn.free.commissioning.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class BlacklistedDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BlacklistedDTO.class);
        BlacklistedDTO blacklistedDTO1 = new BlacklistedDTO();
        blacklistedDTO1.setId(1L);
        BlacklistedDTO blacklistedDTO2 = new BlacklistedDTO();
        assertThat(blacklistedDTO1).isNotEqualTo(blacklistedDTO2);
        blacklistedDTO2.setId(blacklistedDTO1.getId());
        assertThat(blacklistedDTO1).isEqualTo(blacklistedDTO2);
        blacklistedDTO2.setId(2L);
        assertThat(blacklistedDTO1).isNotEqualTo(blacklistedDTO2);
        blacklistedDTO1.setId(null);
        assertThat(blacklistedDTO1).isNotEqualTo(blacklistedDTO2);
    }
}
