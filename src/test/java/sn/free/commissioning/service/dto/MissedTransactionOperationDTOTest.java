package sn.free.commissioning.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class MissedTransactionOperationDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(MissedTransactionOperationDTO.class);
        MissedTransactionOperationDTO missedTransactionOperationDTO1 = new MissedTransactionOperationDTO();
        missedTransactionOperationDTO1.setId(1L);
        MissedTransactionOperationDTO missedTransactionOperationDTO2 = new MissedTransactionOperationDTO();
        assertThat(missedTransactionOperationDTO1).isNotEqualTo(missedTransactionOperationDTO2);
        missedTransactionOperationDTO2.setId(missedTransactionOperationDTO1.getId());
        assertThat(missedTransactionOperationDTO1).isEqualTo(missedTransactionOperationDTO2);
        missedTransactionOperationDTO2.setId(2L);
        assertThat(missedTransactionOperationDTO1).isNotEqualTo(missedTransactionOperationDTO2);
        missedTransactionOperationDTO1.setId(null);
        assertThat(missedTransactionOperationDTO1).isNotEqualTo(missedTransactionOperationDTO2);
    }
}
