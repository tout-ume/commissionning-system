package sn.free.commissioning.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class CommissionTypeDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CommissionTypeDTO.class);
        CommissionTypeDTO commissionTypeDTO1 = new CommissionTypeDTO();
        commissionTypeDTO1.setId(1L);
        CommissionTypeDTO commissionTypeDTO2 = new CommissionTypeDTO();
        assertThat(commissionTypeDTO1).isNotEqualTo(commissionTypeDTO2);
        commissionTypeDTO2.setId(commissionTypeDTO1.getId());
        assertThat(commissionTypeDTO1).isEqualTo(commissionTypeDTO2);
        commissionTypeDTO2.setId(2L);
        assertThat(commissionTypeDTO1).isNotEqualTo(commissionTypeDTO2);
        commissionTypeDTO1.setId(null);
        assertThat(commissionTypeDTO1).isNotEqualTo(commissionTypeDTO2);
    }
}
