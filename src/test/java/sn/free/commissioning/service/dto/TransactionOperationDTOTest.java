package sn.free.commissioning.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class TransactionOperationDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TransactionOperationDTO.class);
        TransactionOperationDTO transactionOperationDTO1 = new TransactionOperationDTO();
        transactionOperationDTO1.setId(1L);
        TransactionOperationDTO transactionOperationDTO2 = new TransactionOperationDTO();
        assertThat(transactionOperationDTO1).isNotEqualTo(transactionOperationDTO2);
        transactionOperationDTO2.setId(transactionOperationDTO1.getId());
        assertThat(transactionOperationDTO1).isEqualTo(transactionOperationDTO2);
        transactionOperationDTO2.setId(2L);
        assertThat(transactionOperationDTO1).isNotEqualTo(transactionOperationDTO2);
        transactionOperationDTO1.setId(null);
        assertThat(transactionOperationDTO1).isNotEqualTo(transactionOperationDTO2);
    }
}
