package sn.free.commissioning.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class AuthorityMirrorDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AuthorityMirrorDTO.class);
        AuthorityMirrorDTO authorityMirrorDTO1 = new AuthorityMirrorDTO();
        authorityMirrorDTO1.setId(1L);
        AuthorityMirrorDTO authorityMirrorDTO2 = new AuthorityMirrorDTO();
        assertThat(authorityMirrorDTO1).isNotEqualTo(authorityMirrorDTO2);
        authorityMirrorDTO2.setId(authorityMirrorDTO1.getId());
        assertThat(authorityMirrorDTO1).isEqualTo(authorityMirrorDTO2);
        authorityMirrorDTO2.setId(2L);
        assertThat(authorityMirrorDTO1).isNotEqualTo(authorityMirrorDTO2);
        authorityMirrorDTO1.setId(null);
        assertThat(authorityMirrorDTO1).isNotEqualTo(authorityMirrorDTO2);
    }
}
