package sn.free.commissioning.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class ConfigurationPlanDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ConfigurationPlanDTO.class);
        ConfigurationPlanDTO configurationPlanDTO1 = new ConfigurationPlanDTO();
        configurationPlanDTO1.setId(1L);
        ConfigurationPlanDTO configurationPlanDTO2 = new ConfigurationPlanDTO();
        assertThat(configurationPlanDTO1).isNotEqualTo(configurationPlanDTO2);
        configurationPlanDTO2.setId(configurationPlanDTO1.getId());
        assertThat(configurationPlanDTO1).isEqualTo(configurationPlanDTO2);
        configurationPlanDTO2.setId(2L);
        assertThat(configurationPlanDTO1).isNotEqualTo(configurationPlanDTO2);
        configurationPlanDTO1.setId(null);
        assertThat(configurationPlanDTO1).isNotEqualTo(configurationPlanDTO2);
    }
}
