package sn.free.commissioning.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class CommissionAccountDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CommissionAccountDTO.class);
        CommissionAccountDTO commissionAccountDTO1 = new CommissionAccountDTO();
        commissionAccountDTO1.setId(1L);
        CommissionAccountDTO commissionAccountDTO2 = new CommissionAccountDTO();
        assertThat(commissionAccountDTO1).isNotEqualTo(commissionAccountDTO2);
        commissionAccountDTO2.setId(commissionAccountDTO1.getId());
        assertThat(commissionAccountDTO1).isEqualTo(commissionAccountDTO2);
        commissionAccountDTO2.setId(2L);
        assertThat(commissionAccountDTO1).isNotEqualTo(commissionAccountDTO2);
        commissionAccountDTO1.setId(null);
        assertThat(commissionAccountDTO1).isNotEqualTo(commissionAccountDTO2);
    }
}
