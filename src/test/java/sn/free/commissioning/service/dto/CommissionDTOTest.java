package sn.free.commissioning.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class CommissionDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CommissionDTO.class);
        CommissionDTO commissionDTO1 = new CommissionDTO();
        commissionDTO1.setId(1L);
        CommissionDTO commissionDTO2 = new CommissionDTO();
        assertThat(commissionDTO1).isNotEqualTo(commissionDTO2);
        commissionDTO2.setId(commissionDTO1.getId());
        assertThat(commissionDTO1).isEqualTo(commissionDTO2);
        commissionDTO2.setId(2L);
        assertThat(commissionDTO1).isNotEqualTo(commissionDTO2);
        commissionDTO1.setId(null);
        assertThat(commissionDTO1).isNotEqualTo(commissionDTO2);
    }
}
