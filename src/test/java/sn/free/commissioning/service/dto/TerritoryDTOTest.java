package sn.free.commissioning.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class TerritoryDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(TerritoryDTO.class);
        TerritoryDTO territoryDTO1 = new TerritoryDTO();
        territoryDTO1.setId(1L);
        TerritoryDTO territoryDTO2 = new TerritoryDTO();
        assertThat(territoryDTO1).isNotEqualTo(territoryDTO2);
        territoryDTO2.setId(territoryDTO1.getId());
        assertThat(territoryDTO1).isEqualTo(territoryDTO2);
        territoryDTO2.setId(2L);
        assertThat(territoryDTO1).isNotEqualTo(territoryDTO2);
        territoryDTO1.setId(null);
        assertThat(territoryDTO1).isNotEqualTo(territoryDTO2);
    }
}
