package sn.free.commissioning.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import sn.free.commissioning.web.rest.TestUtil;

class CommissioningPlanDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CommissioningPlanDTO.class);
        CommissioningPlanDTO commissioningPlanDTO1 = new CommissioningPlanDTO();
        commissioningPlanDTO1.setId(1L);
        CommissioningPlanDTO commissioningPlanDTO2 = new CommissioningPlanDTO();
        assertThat(commissioningPlanDTO1).isNotEqualTo(commissioningPlanDTO2);
        commissioningPlanDTO2.setId(commissioningPlanDTO1.getId());
        assertThat(commissioningPlanDTO1).isEqualTo(commissioningPlanDTO2);
        commissioningPlanDTO2.setId(2L);
        assertThat(commissioningPlanDTO1).isNotEqualTo(commissioningPlanDTO2);
        commissioningPlanDTO1.setId(null);
        assertThat(commissioningPlanDTO1).isNotEqualTo(commissioningPlanDTO2);
    }
}
