package sn.free.commissioning.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.IntegrationTest;
import sn.free.commissioning.domain.Blacklisted;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.domain.Zone;
import sn.free.commissioning.repository.PartnerRepository;
import sn.free.commissioning.service.PartnerService;
import sn.free.commissioning.service.criteria.PartnerCriteria;
import sn.free.commissioning.service.dto.PartnerDTO;
import sn.free.commissioning.service.mapper.PartnerMapper;

/**
 * Integration tests for the {@link PartnerResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class PartnerResourceIT {

    private static final String DEFAULT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SURNAME = "AAAAAAAAAA";
    private static final String UPDATED_SURNAME = "BBBBBBBBBB";

    private static final String DEFAULT_STATE = "AAAAAAAAAA";
    private static final String UPDATED_STATE = "BBBBBBBBBB";

    private static final Long DEFAULT_PARTNER_PROFILE_ID = 1L;
    private static final Long UPDATED_PARTNER_PROFILE_ID = 2L;
    private static final Long SMALLER_PARTNER_PROFILE_ID = 1L - 1L;

    private static final Long DEFAULT_PARENT_ID = 1L;
    private static final Long UPDATED_PARENT_ID = 2L;
    private static final Long SMALLER_PARENT_ID = 1L - 1L;

    private static final Boolean DEFAULT_CAN_RESET_PIN = false;
    private static final Boolean UPDATED_CAN_RESET_PIN = true;

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_LAST_TRANSACTION_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_TRANSACTION_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/partners";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PartnerRepository partnerRepository;

    @Mock
    private PartnerRepository partnerRepositoryMock;

    @Autowired
    private PartnerMapper partnerMapper;

    @Mock
    private PartnerService partnerServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPartnerMockMvc;

    private Partner partner;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Partner createEntity(EntityManager em) {
        Partner partner = new Partner()
            .msisdn(DEFAULT_MSISDN)
            .name(DEFAULT_NAME)
            .surname(DEFAULT_SURNAME)
            .state(DEFAULT_STATE)
            .partnerProfileId(DEFAULT_PARTNER_PROFILE_ID)
            //            .parentId(DEFAULT_PARENT_ID)
            .canResetPin(DEFAULT_CAN_RESET_PIN)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .lastTransactionDate(DEFAULT_LAST_TRANSACTION_DATE);
        return partner;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Partner createUpdatedEntity(EntityManager em) {
        Partner partner = new Partner()
            .msisdn(UPDATED_MSISDN)
            .name(UPDATED_NAME)
            .surname(UPDATED_SURNAME)
            .state(UPDATED_STATE)
            .partnerProfileId(UPDATED_PARTNER_PROFILE_ID)
            //            .parentId(UPDATED_PARENT_ID)
            .canResetPin(UPDATED_CAN_RESET_PIN)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastTransactionDate(UPDATED_LAST_TRANSACTION_DATE);
        return partner;
    }

    @BeforeEach
    public void initTest() {
        partner = createEntity(em);
    }

    @Test
    @Transactional
    void createPartner() throws Exception {
        int databaseSizeBeforeCreate = partnerRepository.findAll().size();
        // Create the Partner
        PartnerDTO partnerDTO = partnerMapper.toDto(partner);
        restPartnerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(partnerDTO)))
            .andExpect(status().isCreated());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeCreate + 1);
        Partner testPartner = partnerList.get(partnerList.size() - 1);
        assertThat(testPartner.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
        assertThat(testPartner.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPartner.getSurname()).isEqualTo(DEFAULT_SURNAME);
        assertThat(testPartner.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testPartner.getPartnerProfileId()).isEqualTo(DEFAULT_PARTNER_PROFILE_ID);
        //        assertThat(testPartner.getParentId()).isEqualTo(DEFAULT_PARENT_ID);
        assertThat(testPartner.getCanResetPin()).isEqualTo(DEFAULT_CAN_RESET_PIN);
        assertThat(testPartner.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testPartner.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testPartner.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testPartner.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testPartner.getLastTransactionDate()).isEqualTo(DEFAULT_LAST_TRANSACTION_DATE);
    }

    @Test
    @Transactional
    void createPartnerWithExistingId() throws Exception {
        // Create the Partner with an existing ID
        partner.setId(1L);
        PartnerDTO partnerDTO = partnerMapper.toDto(partner);

        int databaseSizeBeforeCreate = partnerRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPartnerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(partnerDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkMsisdnIsRequired() throws Exception {
        int databaseSizeBeforeTest = partnerRepository.findAll().size();
        // set the field null
        partner.setMsisdn(null);

        // Create the Partner, which fails.
        PartnerDTO partnerDTO = partnerMapper.toDto(partner);

        restPartnerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(partnerDTO)))
            .andExpect(status().isBadRequest());

        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCreatedByIsRequired() throws Exception {
        int databaseSizeBeforeTest = partnerRepository.findAll().size();
        // set the field null
        partner.setCreatedBy(null);

        // Create the Partner, which fails.
        PartnerDTO partnerDTO = partnerMapper.toDto(partner);

        restPartnerMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(partnerDTO)))
            .andExpect(status().isBadRequest());

        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPartners() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList
        restPartnerMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(partner.getId().intValue())))
            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].surname").value(hasItem(DEFAULT_SURNAME)))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE)))
            .andExpect(jsonPath("$.[*].partnerProfileId").value(hasItem(DEFAULT_PARTNER_PROFILE_ID.intValue())))
            .andExpect(jsonPath("$.[*].parentId").value(hasItem(DEFAULT_PARENT_ID.intValue())))
            .andExpect(jsonPath("$.[*].canResetPin").value(hasItem(DEFAULT_CAN_RESET_PIN.booleanValue())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastTransactionDate").value(hasItem(DEFAULT_LAST_TRANSACTION_DATE.toString())));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllPartnersWithEagerRelationshipsIsEnabled() throws Exception {
        when(partnerServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restPartnerMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(partnerServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllPartnersWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(partnerServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restPartnerMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(partnerServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    void getPartner() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get the partner
        restPartnerMockMvc
            .perform(get(ENTITY_API_URL_ID, partner.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(partner.getId().intValue()))
            .andExpect(jsonPath("$.msisdn").value(DEFAULT_MSISDN))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.surname").value(DEFAULT_SURNAME))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE))
            .andExpect(jsonPath("$.partnerProfileId").value(DEFAULT_PARTNER_PROFILE_ID.intValue()))
            .andExpect(jsonPath("$.parentId").value(DEFAULT_PARENT_ID.intValue()))
            .andExpect(jsonPath("$.canResetPin").value(DEFAULT_CAN_RESET_PIN.booleanValue()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.lastTransactionDate").value(DEFAULT_LAST_TRANSACTION_DATE.toString()));
    }

    @Test
    @Transactional
    void getPartnersByIdFiltering() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        Long id = partner.getId();

        defaultPartnerShouldBeFound("id.equals=" + id);
        defaultPartnerShouldNotBeFound("id.notEquals=" + id);

        defaultPartnerShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPartnerShouldNotBeFound("id.greaterThan=" + id);

        defaultPartnerShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPartnerShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllPartnersByMsisdnIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where msisdn equals to DEFAULT_MSISDN
        defaultPartnerShouldBeFound("msisdn.equals=" + DEFAULT_MSISDN);

        // Get all the partnerList where msisdn equals to UPDATED_MSISDN
        defaultPartnerShouldNotBeFound("msisdn.equals=" + UPDATED_MSISDN);
    }

    @Test
    @Transactional
    void getAllPartnersByMsisdnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where msisdn not equals to DEFAULT_MSISDN
        defaultPartnerShouldNotBeFound("msisdn.notEquals=" + DEFAULT_MSISDN);

        // Get all the partnerList where msisdn not equals to UPDATED_MSISDN
        defaultPartnerShouldBeFound("msisdn.notEquals=" + UPDATED_MSISDN);
    }

    @Test
    @Transactional
    void getAllPartnersByMsisdnIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where msisdn in DEFAULT_MSISDN or UPDATED_MSISDN
        defaultPartnerShouldBeFound("msisdn.in=" + DEFAULT_MSISDN + "," + UPDATED_MSISDN);

        // Get all the partnerList where msisdn equals to UPDATED_MSISDN
        defaultPartnerShouldNotBeFound("msisdn.in=" + UPDATED_MSISDN);
    }

    @Test
    @Transactional
    void getAllPartnersByMsisdnIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where msisdn is not null
        defaultPartnerShouldBeFound("msisdn.specified=true");

        // Get all the partnerList where msisdn is null
        defaultPartnerShouldNotBeFound("msisdn.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersByMsisdnContainsSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where msisdn contains DEFAULT_MSISDN
        defaultPartnerShouldBeFound("msisdn.contains=" + DEFAULT_MSISDN);

        // Get all the partnerList where msisdn contains UPDATED_MSISDN
        defaultPartnerShouldNotBeFound("msisdn.contains=" + UPDATED_MSISDN);
    }

    @Test
    @Transactional
    void getAllPartnersByMsisdnNotContainsSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where msisdn does not contain DEFAULT_MSISDN
        defaultPartnerShouldNotBeFound("msisdn.doesNotContain=" + DEFAULT_MSISDN);

        // Get all the partnerList where msisdn does not contain UPDATED_MSISDN
        defaultPartnerShouldBeFound("msisdn.doesNotContain=" + UPDATED_MSISDN);
    }

    @Test
    @Transactional
    void getAllPartnersByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where name equals to DEFAULT_NAME
        defaultPartnerShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the partnerList where name equals to UPDATED_NAME
        defaultPartnerShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllPartnersByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where name not equals to DEFAULT_NAME
        defaultPartnerShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the partnerList where name not equals to UPDATED_NAME
        defaultPartnerShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllPartnersByNameIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where name in DEFAULT_NAME or UPDATED_NAME
        defaultPartnerShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the partnerList where name equals to UPDATED_NAME
        defaultPartnerShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllPartnersByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where name is not null
        defaultPartnerShouldBeFound("name.specified=true");

        // Get all the partnerList where name is null
        defaultPartnerShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersByNameContainsSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where name contains DEFAULT_NAME
        defaultPartnerShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the partnerList where name contains UPDATED_NAME
        defaultPartnerShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllPartnersByNameNotContainsSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where name does not contain DEFAULT_NAME
        defaultPartnerShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the partnerList where name does not contain UPDATED_NAME
        defaultPartnerShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllPartnersBySurnameIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where surname equals to DEFAULT_SURNAME
        defaultPartnerShouldBeFound("surname.equals=" + DEFAULT_SURNAME);

        // Get all the partnerList where surname equals to UPDATED_SURNAME
        defaultPartnerShouldNotBeFound("surname.equals=" + UPDATED_SURNAME);
    }

    @Test
    @Transactional
    void getAllPartnersBySurnameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where surname not equals to DEFAULT_SURNAME
        defaultPartnerShouldNotBeFound("surname.notEquals=" + DEFAULT_SURNAME);

        // Get all the partnerList where surname not equals to UPDATED_SURNAME
        defaultPartnerShouldBeFound("surname.notEquals=" + UPDATED_SURNAME);
    }

    @Test
    @Transactional
    void getAllPartnersBySurnameIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where surname in DEFAULT_SURNAME or UPDATED_SURNAME
        defaultPartnerShouldBeFound("surname.in=" + DEFAULT_SURNAME + "," + UPDATED_SURNAME);

        // Get all the partnerList where surname equals to UPDATED_SURNAME
        defaultPartnerShouldNotBeFound("surname.in=" + UPDATED_SURNAME);
    }

    @Test
    @Transactional
    void getAllPartnersBySurnameIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where surname is not null
        defaultPartnerShouldBeFound("surname.specified=true");

        // Get all the partnerList where surname is null
        defaultPartnerShouldNotBeFound("surname.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersBySurnameContainsSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where surname contains DEFAULT_SURNAME
        defaultPartnerShouldBeFound("surname.contains=" + DEFAULT_SURNAME);

        // Get all the partnerList where surname contains UPDATED_SURNAME
        defaultPartnerShouldNotBeFound("surname.contains=" + UPDATED_SURNAME);
    }

    @Test
    @Transactional
    void getAllPartnersBySurnameNotContainsSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where surname does not contain DEFAULT_SURNAME
        defaultPartnerShouldNotBeFound("surname.doesNotContain=" + DEFAULT_SURNAME);

        // Get all the partnerList where surname does not contain UPDATED_SURNAME
        defaultPartnerShouldBeFound("surname.doesNotContain=" + UPDATED_SURNAME);
    }

    @Test
    @Transactional
    void getAllPartnersByStateIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where state equals to DEFAULT_STATE
        defaultPartnerShouldBeFound("state.equals=" + DEFAULT_STATE);

        // Get all the partnerList where state equals to UPDATED_STATE
        defaultPartnerShouldNotBeFound("state.equals=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    void getAllPartnersByStateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where state not equals to DEFAULT_STATE
        defaultPartnerShouldNotBeFound("state.notEquals=" + DEFAULT_STATE);

        // Get all the partnerList where state not equals to UPDATED_STATE
        defaultPartnerShouldBeFound("state.notEquals=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    void getAllPartnersByStateIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where state in DEFAULT_STATE or UPDATED_STATE
        defaultPartnerShouldBeFound("state.in=" + DEFAULT_STATE + "," + UPDATED_STATE);

        // Get all the partnerList where state equals to UPDATED_STATE
        defaultPartnerShouldNotBeFound("state.in=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    void getAllPartnersByStateIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where state is not null
        defaultPartnerShouldBeFound("state.specified=true");

        // Get all the partnerList where state is null
        defaultPartnerShouldNotBeFound("state.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersByStateContainsSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where state contains DEFAULT_STATE
        defaultPartnerShouldBeFound("state.contains=" + DEFAULT_STATE);

        // Get all the partnerList where state contains UPDATED_STATE
        defaultPartnerShouldNotBeFound("state.contains=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    void getAllPartnersByStateNotContainsSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where state does not contain DEFAULT_STATE
        defaultPartnerShouldNotBeFound("state.doesNotContain=" + DEFAULT_STATE);

        // Get all the partnerList where state does not contain UPDATED_STATE
        defaultPartnerShouldBeFound("state.doesNotContain=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    void getAllPartnersByPartnerProfileIdIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where partnerProfileId equals to DEFAULT_PARTNER_PROFILE_ID
        defaultPartnerShouldBeFound("partnerProfileId.equals=" + DEFAULT_PARTNER_PROFILE_ID);

        // Get all the partnerList where partnerProfileId equals to UPDATED_PARTNER_PROFILE_ID
        defaultPartnerShouldNotBeFound("partnerProfileId.equals=" + UPDATED_PARTNER_PROFILE_ID);
    }

    @Test
    @Transactional
    void getAllPartnersByPartnerProfileIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where partnerProfileId not equals to DEFAULT_PARTNER_PROFILE_ID
        defaultPartnerShouldNotBeFound("partnerProfileId.notEquals=" + DEFAULT_PARTNER_PROFILE_ID);

        // Get all the partnerList where partnerProfileId not equals to UPDATED_PARTNER_PROFILE_ID
        defaultPartnerShouldBeFound("partnerProfileId.notEquals=" + UPDATED_PARTNER_PROFILE_ID);
    }

    @Test
    @Transactional
    void getAllPartnersByPartnerProfileIdIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where partnerProfileId in DEFAULT_PARTNER_PROFILE_ID or UPDATED_PARTNER_PROFILE_ID
        defaultPartnerShouldBeFound("partnerProfileId.in=" + DEFAULT_PARTNER_PROFILE_ID + "," + UPDATED_PARTNER_PROFILE_ID);

        // Get all the partnerList where partnerProfileId equals to UPDATED_PARTNER_PROFILE_ID
        defaultPartnerShouldNotBeFound("partnerProfileId.in=" + UPDATED_PARTNER_PROFILE_ID);
    }

    @Test
    @Transactional
    void getAllPartnersByPartnerProfileIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where partnerProfileId is not null
        defaultPartnerShouldBeFound("partnerProfileId.specified=true");

        // Get all the partnerList where partnerProfileId is null
        defaultPartnerShouldNotBeFound("partnerProfileId.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersByPartnerProfileIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where partnerProfileId is greater than or equal to DEFAULT_PARTNER_PROFILE_ID
        defaultPartnerShouldBeFound("partnerProfileId.greaterThanOrEqual=" + DEFAULT_PARTNER_PROFILE_ID);

        // Get all the partnerList where partnerProfileId is greater than or equal to UPDATED_PARTNER_PROFILE_ID
        defaultPartnerShouldNotBeFound("partnerProfileId.greaterThanOrEqual=" + UPDATED_PARTNER_PROFILE_ID);
    }

    @Test
    @Transactional
    void getAllPartnersByPartnerProfileIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where partnerProfileId is less than or equal to DEFAULT_PARTNER_PROFILE_ID
        defaultPartnerShouldBeFound("partnerProfileId.lessThanOrEqual=" + DEFAULT_PARTNER_PROFILE_ID);

        // Get all the partnerList where partnerProfileId is less than or equal to SMALLER_PARTNER_PROFILE_ID
        defaultPartnerShouldNotBeFound("partnerProfileId.lessThanOrEqual=" + SMALLER_PARTNER_PROFILE_ID);
    }

    @Test
    @Transactional
    void getAllPartnersByPartnerProfileIdIsLessThanSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where partnerProfileId is less than DEFAULT_PARTNER_PROFILE_ID
        defaultPartnerShouldNotBeFound("partnerProfileId.lessThan=" + DEFAULT_PARTNER_PROFILE_ID);

        // Get all the partnerList where partnerProfileId is less than UPDATED_PARTNER_PROFILE_ID
        defaultPartnerShouldBeFound("partnerProfileId.lessThan=" + UPDATED_PARTNER_PROFILE_ID);
    }

    @Test
    @Transactional
    void getAllPartnersByPartnerProfileIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where partnerProfileId is greater than DEFAULT_PARTNER_PROFILE_ID
        defaultPartnerShouldNotBeFound("partnerProfileId.greaterThan=" + DEFAULT_PARTNER_PROFILE_ID);

        // Get all the partnerList where partnerProfileId is greater than SMALLER_PARTNER_PROFILE_ID
        defaultPartnerShouldBeFound("partnerProfileId.greaterThan=" + SMALLER_PARTNER_PROFILE_ID);
    }

    @Test
    @Transactional
    void getAllPartnersByParentIdIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where parentId equals to DEFAULT_PARENT_ID
        defaultPartnerShouldBeFound("parentId.equals=" + DEFAULT_PARENT_ID);

        // Get all the partnerList where parentId equals to UPDATED_PARENT_ID
        defaultPartnerShouldNotBeFound("parentId.equals=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllPartnersByParentIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where parentId not equals to DEFAULT_PARENT_ID
        defaultPartnerShouldNotBeFound("parentId.notEquals=" + DEFAULT_PARENT_ID);

        // Get all the partnerList where parentId not equals to UPDATED_PARENT_ID
        defaultPartnerShouldBeFound("parentId.notEquals=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllPartnersByParentIdIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where parentId in DEFAULT_PARENT_ID or UPDATED_PARENT_ID
        defaultPartnerShouldBeFound("parentId.in=" + DEFAULT_PARENT_ID + "," + UPDATED_PARENT_ID);

        // Get all the partnerList where parentId equals to UPDATED_PARENT_ID
        defaultPartnerShouldNotBeFound("parentId.in=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllPartnersByParentIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where parentId is not null
        defaultPartnerShouldBeFound("parentId.specified=true");

        // Get all the partnerList where parentId is null
        defaultPartnerShouldNotBeFound("parentId.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersByParentIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where parentId is greater than or equal to DEFAULT_PARENT_ID
        defaultPartnerShouldBeFound("parentId.greaterThanOrEqual=" + DEFAULT_PARENT_ID);

        // Get all the partnerList where parentId is greater than or equal to UPDATED_PARENT_ID
        defaultPartnerShouldNotBeFound("parentId.greaterThanOrEqual=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllPartnersByParentIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where parentId is less than or equal to DEFAULT_PARENT_ID
        defaultPartnerShouldBeFound("parentId.lessThanOrEqual=" + DEFAULT_PARENT_ID);

        // Get all the partnerList where parentId is less than or equal to SMALLER_PARENT_ID
        defaultPartnerShouldNotBeFound("parentId.lessThanOrEqual=" + SMALLER_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllPartnersByParentIdIsLessThanSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where parentId is less than DEFAULT_PARENT_ID
        defaultPartnerShouldNotBeFound("parentId.lessThan=" + DEFAULT_PARENT_ID);

        // Get all the partnerList where parentId is less than UPDATED_PARENT_ID
        defaultPartnerShouldBeFound("parentId.lessThan=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllPartnersByParentIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where parentId is greater than DEFAULT_PARENT_ID
        defaultPartnerShouldNotBeFound("parentId.greaterThan=" + DEFAULT_PARENT_ID);

        // Get all the partnerList where parentId is greater than SMALLER_PARENT_ID
        defaultPartnerShouldBeFound("parentId.greaterThan=" + SMALLER_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllPartnersByCanResetPinIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where canResetPin equals to DEFAULT_CAN_RESET_PIN
        defaultPartnerShouldBeFound("canResetPin.equals=" + DEFAULT_CAN_RESET_PIN);

        // Get all the partnerList where canResetPin equals to UPDATED_CAN_RESET_PIN
        defaultPartnerShouldNotBeFound("canResetPin.equals=" + UPDATED_CAN_RESET_PIN);
    }

    @Test
    @Transactional
    void getAllPartnersByCanResetPinIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where canResetPin not equals to DEFAULT_CAN_RESET_PIN
        defaultPartnerShouldNotBeFound("canResetPin.notEquals=" + DEFAULT_CAN_RESET_PIN);

        // Get all the partnerList where canResetPin not equals to UPDATED_CAN_RESET_PIN
        defaultPartnerShouldBeFound("canResetPin.notEquals=" + UPDATED_CAN_RESET_PIN);
    }

    @Test
    @Transactional
    void getAllPartnersByCanResetPinIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where canResetPin in DEFAULT_CAN_RESET_PIN or UPDATED_CAN_RESET_PIN
        defaultPartnerShouldBeFound("canResetPin.in=" + DEFAULT_CAN_RESET_PIN + "," + UPDATED_CAN_RESET_PIN);

        // Get all the partnerList where canResetPin equals to UPDATED_CAN_RESET_PIN
        defaultPartnerShouldNotBeFound("canResetPin.in=" + UPDATED_CAN_RESET_PIN);
    }

    @Test
    @Transactional
    void getAllPartnersByCanResetPinIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where canResetPin is not null
        defaultPartnerShouldBeFound("canResetPin.specified=true");

        // Get all the partnerList where canResetPin is null
        defaultPartnerShouldNotBeFound("canResetPin.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where createdBy equals to DEFAULT_CREATED_BY
        defaultPartnerShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the partnerList where createdBy equals to UPDATED_CREATED_BY
        defaultPartnerShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where createdBy not equals to DEFAULT_CREATED_BY
        defaultPartnerShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the partnerList where createdBy not equals to UPDATED_CREATED_BY
        defaultPartnerShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultPartnerShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the partnerList where createdBy equals to UPDATED_CREATED_BY
        defaultPartnerShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where createdBy is not null
        defaultPartnerShouldBeFound("createdBy.specified=true");

        // Get all the partnerList where createdBy is null
        defaultPartnerShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where createdBy contains DEFAULT_CREATED_BY
        defaultPartnerShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the partnerList where createdBy contains UPDATED_CREATED_BY
        defaultPartnerShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where createdBy does not contain DEFAULT_CREATED_BY
        defaultPartnerShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the partnerList where createdBy does not contain UPDATED_CREATED_BY
        defaultPartnerShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where createdDate equals to DEFAULT_CREATED_DATE
        defaultPartnerShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the partnerList where createdDate equals to UPDATED_CREATED_DATE
        defaultPartnerShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllPartnersByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultPartnerShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the partnerList where createdDate not equals to UPDATED_CREATED_DATE
        defaultPartnerShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllPartnersByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultPartnerShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the partnerList where createdDate equals to UPDATED_CREATED_DATE
        defaultPartnerShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllPartnersByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where createdDate is not null
        defaultPartnerShouldBeFound("createdDate.specified=true");

        // Get all the partnerList where createdDate is null
        defaultPartnerShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultPartnerShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the partnerList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultPartnerShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultPartnerShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the partnerList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultPartnerShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultPartnerShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the partnerList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultPartnerShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where lastModifiedBy is not null
        defaultPartnerShouldBeFound("lastModifiedBy.specified=true");

        // Get all the partnerList where lastModifiedBy is null
        defaultPartnerShouldNotBeFound("lastModifiedBy.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultPartnerShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the partnerList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultPartnerShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultPartnerShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the partnerList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultPartnerShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllPartnersByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultPartnerShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the partnerList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultPartnerShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllPartnersByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultPartnerShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the partnerList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultPartnerShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllPartnersByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultPartnerShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the partnerList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultPartnerShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllPartnersByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where lastModifiedDate is not null
        defaultPartnerShouldBeFound("lastModifiedDate.specified=true");

        // Get all the partnerList where lastModifiedDate is null
        defaultPartnerShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersByLastTransactionDateIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where lastTransactionDate equals to DEFAULT_LAST_TRANSACTION_DATE
        defaultPartnerShouldBeFound("lastTransactionDate.equals=" + DEFAULT_LAST_TRANSACTION_DATE);

        // Get all the partnerList where lastTransactionDate equals to UPDATED_LAST_TRANSACTION_DATE
        defaultPartnerShouldNotBeFound("lastTransactionDate.equals=" + UPDATED_LAST_TRANSACTION_DATE);
    }

    @Test
    @Transactional
    void getAllPartnersByLastTransactionDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where lastTransactionDate not equals to DEFAULT_LAST_TRANSACTION_DATE
        defaultPartnerShouldNotBeFound("lastTransactionDate.notEquals=" + DEFAULT_LAST_TRANSACTION_DATE);

        // Get all the partnerList where lastTransactionDate not equals to UPDATED_LAST_TRANSACTION_DATE
        defaultPartnerShouldBeFound("lastTransactionDate.notEquals=" + UPDATED_LAST_TRANSACTION_DATE);
    }

    @Test
    @Transactional
    void getAllPartnersByLastTransactionDateIsInShouldWork() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where lastTransactionDate in DEFAULT_LAST_TRANSACTION_DATE or UPDATED_LAST_TRANSACTION_DATE
        defaultPartnerShouldBeFound("lastTransactionDate.in=" + DEFAULT_LAST_TRANSACTION_DATE + "," + UPDATED_LAST_TRANSACTION_DATE);

        // Get all the partnerList where lastTransactionDate equals to UPDATED_LAST_TRANSACTION_DATE
        defaultPartnerShouldNotBeFound("lastTransactionDate.in=" + UPDATED_LAST_TRANSACTION_DATE);
    }

    @Test
    @Transactional
    void getAllPartnersByLastTransactionDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        // Get all the partnerList where lastTransactionDate is not null
        defaultPartnerShouldBeFound("lastTransactionDate.specified=true");

        // Get all the partnerList where lastTransactionDate is null
        defaultPartnerShouldNotBeFound("lastTransactionDate.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnersByBlacklistedIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);
        Blacklisted blacklisted;
        if (TestUtil.findAll(em, Blacklisted.class).isEmpty()) {
            blacklisted = BlacklistedResourceIT.createEntity(em);
            em.persist(blacklisted);
            em.flush();
        } else {
            blacklisted = TestUtil.findAll(em, Blacklisted.class).get(0);
        }
        em.persist(blacklisted);
        em.flush();
        partner.addBlacklisted(blacklisted);
        partnerRepository.saveAndFlush(partner);
        Long blacklistedId = blacklisted.getId();

        // Get all the partnerList where blacklisted equals to blacklistedId
        defaultPartnerShouldBeFound("blacklistedId.equals=" + blacklistedId);

        // Get all the partnerList where blacklisted equals to (blacklistedId + 1)
        defaultPartnerShouldNotBeFound("blacklistedId.equals=" + (blacklistedId + 1));
    }

    @Test
    @Transactional
    void getAllPartnersByChildrenIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);
        Partner children;
        if (TestUtil.findAll(em, Partner.class).isEmpty()) {
            children = PartnerResourceIT.createEntity(em);
            em.persist(children);
            em.flush();
        } else {
            children = TestUtil.findAll(em, Partner.class).get(0);
        }
        em.persist(children);
        em.flush();
        partner.addChildren(children);
        partnerRepository.saveAndFlush(partner);
        Long childrenId = children.getId();

        // Get all the partnerList where children equals to childrenId
        defaultPartnerShouldBeFound("childrenId.equals=" + childrenId);

        // Get all the partnerList where children equals to (childrenId + 1)
        defaultPartnerShouldNotBeFound("childrenId.equals=" + (childrenId + 1));
    }

    @Test
    @Transactional
    void getAllPartnersByZoneIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);
        Zone zone;
        if (TestUtil.findAll(em, Zone.class).isEmpty()) {
            zone = ZoneResourceIT.createEntity(em);
            em.persist(zone);
            em.flush();
        } else {
            zone = TestUtil.findAll(em, Zone.class).get(0);
        }
        em.persist(zone);
        em.flush();
        partner.addZone(zone);
        partnerRepository.saveAndFlush(partner);
        Long zoneId = zone.getId();

        // Get all the partnerList where zone equals to zoneId
        defaultPartnerShouldBeFound("zoneId.equals=" + zoneId);

        // Get all the partnerList where zone equals to (zoneId + 1)
        defaultPartnerShouldNotBeFound("zoneId.equals=" + (zoneId + 1));
    }

    @Test
    @Transactional
    void getAllPartnersByPartnerIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);
        Partner partner;
        if (TestUtil.findAll(em, Partner.class).isEmpty()) {
            partner = PartnerResourceIT.createEntity(em);
            em.persist(partner);
            em.flush();
        } else {
            partner = TestUtil.findAll(em, Partner.class).get(0);
        }
        em.persist(partner);
        em.flush();
        partner.setParent(partner);
        partnerRepository.saveAndFlush(partner);
        Long partnerId = partner.getId();

        // Get all the partnerList where partner equals to partnerId
        defaultPartnerShouldBeFound("partnerId.equals=" + partnerId);

        // Get all the partnerList where partner equals to (partnerId + 1)
        defaultPartnerShouldNotBeFound("partnerId.equals=" + (partnerId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPartnerShouldBeFound(String filter) throws Exception {
        restPartnerMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(partner.getId().intValue())))
            .andExpect(jsonPath("$.[*].msisdn").value(hasItem(DEFAULT_MSISDN)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].surname").value(hasItem(DEFAULT_SURNAME)))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE)))
            .andExpect(jsonPath("$.[*].partnerProfileId").value(hasItem(DEFAULT_PARTNER_PROFILE_ID.intValue())))
            .andExpect(jsonPath("$.[*].parentId").value(hasItem(DEFAULT_PARENT_ID.intValue())))
            .andExpect(jsonPath("$.[*].canResetPin").value(hasItem(DEFAULT_CAN_RESET_PIN.booleanValue())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastTransactionDate").value(hasItem(DEFAULT_LAST_TRANSACTION_DATE.toString())));

        // Check, that the count call also returns 1
        restPartnerMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPartnerShouldNotBeFound(String filter) throws Exception {
        restPartnerMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPartnerMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingPartner() throws Exception {
        // Get the partner
        restPartnerMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPartner() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        int databaseSizeBeforeUpdate = partnerRepository.findAll().size();

        // Update the partner
        Partner updatedPartner = partnerRepository.findById(partner.getId()).get();
        // Disconnect from session so that the updates on updatedPartner are not directly saved in db
        em.detach(updatedPartner);
        updatedPartner
            .msisdn(UPDATED_MSISDN)
            .name(UPDATED_NAME)
            .surname(UPDATED_SURNAME)
            .state(UPDATED_STATE)
            .partnerProfileId(UPDATED_PARTNER_PROFILE_ID)
            //            .parentId(UPDATED_PARENT_ID)
            .canResetPin(UPDATED_CAN_RESET_PIN)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastTransactionDate(UPDATED_LAST_TRANSACTION_DATE);
        PartnerDTO partnerDTO = partnerMapper.toDto(updatedPartner);

        restPartnerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, partnerDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(partnerDTO))
            )
            .andExpect(status().isOk());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);
        Partner testPartner = partnerList.get(partnerList.size() - 1);
        assertThat(testPartner.getMsisdn()).isEqualTo(UPDATED_MSISDN);
        assertThat(testPartner.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPartner.getSurname()).isEqualTo(UPDATED_SURNAME);
        assertThat(testPartner.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testPartner.getPartnerProfileId()).isEqualTo(UPDATED_PARTNER_PROFILE_ID);
        //        assertThat(testPartner.getParentId()).isEqualTo(UPDATED_PARENT_ID);
        assertThat(testPartner.getCanResetPin()).isEqualTo(UPDATED_CAN_RESET_PIN);
        assertThat(testPartner.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testPartner.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testPartner.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testPartner.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testPartner.getLastTransactionDate()).isEqualTo(UPDATED_LAST_TRANSACTION_DATE);
    }

    @Test
    @Transactional
    void putNonExistingPartner() throws Exception {
        int databaseSizeBeforeUpdate = partnerRepository.findAll().size();
        partner.setId(count.incrementAndGet());

        // Create the Partner
        PartnerDTO partnerDTO = partnerMapper.toDto(partner);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPartnerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, partnerDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(partnerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPartner() throws Exception {
        int databaseSizeBeforeUpdate = partnerRepository.findAll().size();
        partner.setId(count.incrementAndGet());

        // Create the Partner
        PartnerDTO partnerDTO = partnerMapper.toDto(partner);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPartnerMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(partnerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPartner() throws Exception {
        int databaseSizeBeforeUpdate = partnerRepository.findAll().size();
        partner.setId(count.incrementAndGet());

        // Create the Partner
        PartnerDTO partnerDTO = partnerMapper.toDto(partner);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPartnerMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(partnerDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePartnerWithPatch() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        int databaseSizeBeforeUpdate = partnerRepository.findAll().size();

        // Update the partner using partial update
        Partner partialUpdatedPartner = new Partner();
        partialUpdatedPartner.setId(partner.getId());

        partialUpdatedPartner
            .name(UPDATED_NAME)
            .surname(UPDATED_SURNAME)
            .state(UPDATED_STATE)
            .canResetPin(UPDATED_CAN_RESET_PIN)
            .createdDate(UPDATED_CREATED_DATE);

        restPartnerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPartner.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPartner))
            )
            .andExpect(status().isOk());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);
        Partner testPartner = partnerList.get(partnerList.size() - 1);
        assertThat(testPartner.getMsisdn()).isEqualTo(DEFAULT_MSISDN);
        assertThat(testPartner.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPartner.getSurname()).isEqualTo(UPDATED_SURNAME);
        assertThat(testPartner.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testPartner.getPartnerProfileId()).isEqualTo(DEFAULT_PARTNER_PROFILE_ID);
        //        assertThat(testPartner.getParentId()).isEqualTo(DEFAULT_PARENT_ID);
        assertThat(testPartner.getCanResetPin()).isEqualTo(UPDATED_CAN_RESET_PIN);
        assertThat(testPartner.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testPartner.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testPartner.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testPartner.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testPartner.getLastTransactionDate()).isEqualTo(DEFAULT_LAST_TRANSACTION_DATE);
    }

    @Test
    @Transactional
    void fullUpdatePartnerWithPatch() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        int databaseSizeBeforeUpdate = partnerRepository.findAll().size();

        // Update the partner using partial update
        Partner partialUpdatedPartner = new Partner();
        partialUpdatedPartner.setId(partner.getId());

        partialUpdatedPartner
            .msisdn(UPDATED_MSISDN)
            .name(UPDATED_NAME)
            .surname(UPDATED_SURNAME)
            .state(UPDATED_STATE)
            .partnerProfileId(UPDATED_PARTNER_PROFILE_ID)
            //            .parentId(UPDATED_PARENT_ID)
            .canResetPin(UPDATED_CAN_RESET_PIN)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .lastTransactionDate(UPDATED_LAST_TRANSACTION_DATE);

        restPartnerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPartner.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPartner))
            )
            .andExpect(status().isOk());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);
        Partner testPartner = partnerList.get(partnerList.size() - 1);
        assertThat(testPartner.getMsisdn()).isEqualTo(UPDATED_MSISDN);
        assertThat(testPartner.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPartner.getSurname()).isEqualTo(UPDATED_SURNAME);
        assertThat(testPartner.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testPartner.getPartnerProfileId()).isEqualTo(UPDATED_PARTNER_PROFILE_ID);
        //        assertThat(testPartner.getParentId()).isEqualTo(UPDATED_PARENT_ID);
        assertThat(testPartner.getCanResetPin()).isEqualTo(UPDATED_CAN_RESET_PIN);
        assertThat(testPartner.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testPartner.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testPartner.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testPartner.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testPartner.getLastTransactionDate()).isEqualTo(UPDATED_LAST_TRANSACTION_DATE);
    }

    @Test
    @Transactional
    void patchNonExistingPartner() throws Exception {
        int databaseSizeBeforeUpdate = partnerRepository.findAll().size();
        partner.setId(count.incrementAndGet());

        // Create the Partner
        PartnerDTO partnerDTO = partnerMapper.toDto(partner);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPartnerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partnerDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partnerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPartner() throws Exception {
        int databaseSizeBeforeUpdate = partnerRepository.findAll().size();
        partner.setId(count.incrementAndGet());

        // Create the Partner
        PartnerDTO partnerDTO = partnerMapper.toDto(partner);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPartnerMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partnerDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPartner() throws Exception {
        int databaseSizeBeforeUpdate = partnerRepository.findAll().size();
        partner.setId(count.incrementAndGet());

        // Create the Partner
        PartnerDTO partnerDTO = partnerMapper.toDto(partner);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPartnerMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(partnerDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Partner in the database
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePartner() throws Exception {
        // Initialize the database
        partnerRepository.saveAndFlush(partner);

        int databaseSizeBeforeDelete = partnerRepository.findAll().size();

        // Delete the partner
        restPartnerMockMvc
            .perform(delete(ENTITY_API_URL_ID, partner.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Partner> partnerList = partnerRepository.findAll();
        assertThat(partnerList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
