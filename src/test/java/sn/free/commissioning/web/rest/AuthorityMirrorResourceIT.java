package sn.free.commissioning.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.IntegrationTest;
import sn.free.commissioning.domain.AuthorityMirror;
import sn.free.commissioning.domain.Role;
import sn.free.commissioning.repository.AuthorityMirrorRepository;
import sn.free.commissioning.service.criteria.AuthorityMirrorCriteria;
import sn.free.commissioning.service.dto.AuthorityMirrorDTO;
import sn.free.commissioning.service.mapper.AuthorityMirrorMapper;

/**
 * Integration tests for the {@link AuthorityMirrorResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class AuthorityMirrorResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/authority-mirrors";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private AuthorityMirrorRepository authorityMirrorRepository;

    @Autowired
    private AuthorityMirrorMapper authorityMirrorMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAuthorityMirrorMockMvc;

    private AuthorityMirror authorityMirror;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AuthorityMirror createEntity(EntityManager em) {
        AuthorityMirror authorityMirror = new AuthorityMirror().name(DEFAULT_NAME);
        return authorityMirror;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AuthorityMirror createUpdatedEntity(EntityManager em) {
        AuthorityMirror authorityMirror = new AuthorityMirror().name(UPDATED_NAME);
        return authorityMirror;
    }

    @BeforeEach
    public void initTest() {
        authorityMirror = createEntity(em);
    }

    @Test
    @Transactional
    void createAuthorityMirror() throws Exception {
        int databaseSizeBeforeCreate = authorityMirrorRepository.findAll().size();
        // Create the AuthorityMirror
        AuthorityMirrorDTO authorityMirrorDTO = authorityMirrorMapper.toDto(authorityMirror);
        restAuthorityMirrorMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(authorityMirrorDTO))
            )
            .andExpect(status().isCreated());

        // Validate the AuthorityMirror in the database
        List<AuthorityMirror> authorityMirrorList = authorityMirrorRepository.findAll();
        assertThat(authorityMirrorList).hasSize(databaseSizeBeforeCreate + 1);
        AuthorityMirror testAuthorityMirror = authorityMirrorList.get(authorityMirrorList.size() - 1);
        assertThat(testAuthorityMirror.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    void createAuthorityMirrorWithExistingId() throws Exception {
        // Create the AuthorityMirror with an existing ID
        authorityMirror.setId(1L);
        AuthorityMirrorDTO authorityMirrorDTO = authorityMirrorMapper.toDto(authorityMirror);

        int databaseSizeBeforeCreate = authorityMirrorRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restAuthorityMirrorMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(authorityMirrorDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the AuthorityMirror in the database
        List<AuthorityMirror> authorityMirrorList = authorityMirrorRepository.findAll();
        assertThat(authorityMirrorList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = authorityMirrorRepository.findAll().size();
        // set the field null
        authorityMirror.setName(null);

        // Create the AuthorityMirror, which fails.
        AuthorityMirrorDTO authorityMirrorDTO = authorityMirrorMapper.toDto(authorityMirror);

        restAuthorityMirrorMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(authorityMirrorDTO))
            )
            .andExpect(status().isBadRequest());

        List<AuthorityMirror> authorityMirrorList = authorityMirrorRepository.findAll();
        assertThat(authorityMirrorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllAuthorityMirrors() throws Exception {
        // Initialize the database
        authorityMirrorRepository.saveAndFlush(authorityMirror);

        // Get all the authorityMirrorList
        restAuthorityMirrorMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorityMirror.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }

    @Test
    @Transactional
    void getAuthorityMirror() throws Exception {
        // Initialize the database
        authorityMirrorRepository.saveAndFlush(authorityMirror);

        // Get the authorityMirror
        restAuthorityMirrorMockMvc
            .perform(get(ENTITY_API_URL_ID, authorityMirror.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(authorityMirror.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }

    @Test
    @Transactional
    void getAuthorityMirrorsByIdFiltering() throws Exception {
        // Initialize the database
        authorityMirrorRepository.saveAndFlush(authorityMirror);

        Long id = authorityMirror.getId();

        defaultAuthorityMirrorShouldBeFound("id.equals=" + id);
        defaultAuthorityMirrorShouldNotBeFound("id.notEquals=" + id);

        defaultAuthorityMirrorShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultAuthorityMirrorShouldNotBeFound("id.greaterThan=" + id);

        defaultAuthorityMirrorShouldBeFound("id.lessThanOrEqual=" + id);
        defaultAuthorityMirrorShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllAuthorityMirrorsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        authorityMirrorRepository.saveAndFlush(authorityMirror);

        // Get all the authorityMirrorList where name equals to DEFAULT_NAME
        defaultAuthorityMirrorShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the authorityMirrorList where name equals to UPDATED_NAME
        defaultAuthorityMirrorShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllAuthorityMirrorsByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        authorityMirrorRepository.saveAndFlush(authorityMirror);

        // Get all the authorityMirrorList where name not equals to DEFAULT_NAME
        defaultAuthorityMirrorShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the authorityMirrorList where name not equals to UPDATED_NAME
        defaultAuthorityMirrorShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllAuthorityMirrorsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        authorityMirrorRepository.saveAndFlush(authorityMirror);

        // Get all the authorityMirrorList where name in DEFAULT_NAME or UPDATED_NAME
        defaultAuthorityMirrorShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the authorityMirrorList where name equals to UPDATED_NAME
        defaultAuthorityMirrorShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllAuthorityMirrorsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        authorityMirrorRepository.saveAndFlush(authorityMirror);

        // Get all the authorityMirrorList where name is not null
        defaultAuthorityMirrorShouldBeFound("name.specified=true");

        // Get all the authorityMirrorList where name is null
        defaultAuthorityMirrorShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllAuthorityMirrorsByNameContainsSomething() throws Exception {
        // Initialize the database
        authorityMirrorRepository.saveAndFlush(authorityMirror);

        // Get all the authorityMirrorList where name contains DEFAULT_NAME
        defaultAuthorityMirrorShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the authorityMirrorList where name contains UPDATED_NAME
        defaultAuthorityMirrorShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllAuthorityMirrorsByNameNotContainsSomething() throws Exception {
        // Initialize the database
        authorityMirrorRepository.saveAndFlush(authorityMirror);

        // Get all the authorityMirrorList where name does not contain DEFAULT_NAME
        defaultAuthorityMirrorShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the authorityMirrorList where name does not contain UPDATED_NAME
        defaultAuthorityMirrorShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllAuthorityMirrorsByRolesIsEqualToSomething() throws Exception {
        // Initialize the database
        authorityMirrorRepository.saveAndFlush(authorityMirror);
        Role roles;
        if (TestUtil.findAll(em, Role.class).isEmpty()) {
            roles = RoleResourceIT.createEntity(em);
            em.persist(roles);
            em.flush();
        } else {
            roles = TestUtil.findAll(em, Role.class).get(0);
        }
        em.persist(roles);
        em.flush();
        authorityMirror.addRoles(roles);
        authorityMirrorRepository.saveAndFlush(authorityMirror);
        Long rolesId = roles.getId();

        // Get all the authorityMirrorList where roles equals to rolesId
        defaultAuthorityMirrorShouldBeFound("rolesId.equals=" + rolesId);

        // Get all the authorityMirrorList where roles equals to (rolesId + 1)
        defaultAuthorityMirrorShouldNotBeFound("rolesId.equals=" + (rolesId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultAuthorityMirrorShouldBeFound(String filter) throws Exception {
        restAuthorityMirrorMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authorityMirror.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));

        // Check, that the count call also returns 1
        restAuthorityMirrorMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultAuthorityMirrorShouldNotBeFound(String filter) throws Exception {
        restAuthorityMirrorMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restAuthorityMirrorMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingAuthorityMirror() throws Exception {
        // Get the authorityMirror
        restAuthorityMirrorMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewAuthorityMirror() throws Exception {
        // Initialize the database
        authorityMirrorRepository.saveAndFlush(authorityMirror);

        int databaseSizeBeforeUpdate = authorityMirrorRepository.findAll().size();

        // Update the authorityMirror
        AuthorityMirror updatedAuthorityMirror = authorityMirrorRepository.findById(authorityMirror.getId()).get();
        // Disconnect from session so that the updates on updatedAuthorityMirror are not directly saved in db
        em.detach(updatedAuthorityMirror);
        updatedAuthorityMirror.name(UPDATED_NAME);
        AuthorityMirrorDTO authorityMirrorDTO = authorityMirrorMapper.toDto(updatedAuthorityMirror);

        restAuthorityMirrorMockMvc
            .perform(
                put(ENTITY_API_URL_ID, authorityMirrorDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(authorityMirrorDTO))
            )
            .andExpect(status().isOk());

        // Validate the AuthorityMirror in the database
        List<AuthorityMirror> authorityMirrorList = authorityMirrorRepository.findAll();
        assertThat(authorityMirrorList).hasSize(databaseSizeBeforeUpdate);
        AuthorityMirror testAuthorityMirror = authorityMirrorList.get(authorityMirrorList.size() - 1);
        assertThat(testAuthorityMirror.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void putNonExistingAuthorityMirror() throws Exception {
        int databaseSizeBeforeUpdate = authorityMirrorRepository.findAll().size();
        authorityMirror.setId(count.incrementAndGet());

        // Create the AuthorityMirror
        AuthorityMirrorDTO authorityMirrorDTO = authorityMirrorMapper.toDto(authorityMirror);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAuthorityMirrorMockMvc
            .perform(
                put(ENTITY_API_URL_ID, authorityMirrorDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(authorityMirrorDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the AuthorityMirror in the database
        List<AuthorityMirror> authorityMirrorList = authorityMirrorRepository.findAll();
        assertThat(authorityMirrorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchAuthorityMirror() throws Exception {
        int databaseSizeBeforeUpdate = authorityMirrorRepository.findAll().size();
        authorityMirror.setId(count.incrementAndGet());

        // Create the AuthorityMirror
        AuthorityMirrorDTO authorityMirrorDTO = authorityMirrorMapper.toDto(authorityMirror);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAuthorityMirrorMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(authorityMirrorDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the AuthorityMirror in the database
        List<AuthorityMirror> authorityMirrorList = authorityMirrorRepository.findAll();
        assertThat(authorityMirrorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamAuthorityMirror() throws Exception {
        int databaseSizeBeforeUpdate = authorityMirrorRepository.findAll().size();
        authorityMirror.setId(count.incrementAndGet());

        // Create the AuthorityMirror
        AuthorityMirrorDTO authorityMirrorDTO = authorityMirrorMapper.toDto(authorityMirror);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAuthorityMirrorMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(authorityMirrorDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the AuthorityMirror in the database
        List<AuthorityMirror> authorityMirrorList = authorityMirrorRepository.findAll();
        assertThat(authorityMirrorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateAuthorityMirrorWithPatch() throws Exception {
        // Initialize the database
        authorityMirrorRepository.saveAndFlush(authorityMirror);

        int databaseSizeBeforeUpdate = authorityMirrorRepository.findAll().size();

        // Update the authorityMirror using partial update
        AuthorityMirror partialUpdatedAuthorityMirror = new AuthorityMirror();
        partialUpdatedAuthorityMirror.setId(authorityMirror.getId());

        restAuthorityMirrorMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAuthorityMirror.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAuthorityMirror))
            )
            .andExpect(status().isOk());

        // Validate the AuthorityMirror in the database
        List<AuthorityMirror> authorityMirrorList = authorityMirrorRepository.findAll();
        assertThat(authorityMirrorList).hasSize(databaseSizeBeforeUpdate);
        AuthorityMirror testAuthorityMirror = authorityMirrorList.get(authorityMirrorList.size() - 1);
        assertThat(testAuthorityMirror.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    void fullUpdateAuthorityMirrorWithPatch() throws Exception {
        // Initialize the database
        authorityMirrorRepository.saveAndFlush(authorityMirror);

        int databaseSizeBeforeUpdate = authorityMirrorRepository.findAll().size();

        // Update the authorityMirror using partial update
        AuthorityMirror partialUpdatedAuthorityMirror = new AuthorityMirror();
        partialUpdatedAuthorityMirror.setId(authorityMirror.getId());

        partialUpdatedAuthorityMirror.name(UPDATED_NAME);

        restAuthorityMirrorMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedAuthorityMirror.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedAuthorityMirror))
            )
            .andExpect(status().isOk());

        // Validate the AuthorityMirror in the database
        List<AuthorityMirror> authorityMirrorList = authorityMirrorRepository.findAll();
        assertThat(authorityMirrorList).hasSize(databaseSizeBeforeUpdate);
        AuthorityMirror testAuthorityMirror = authorityMirrorList.get(authorityMirrorList.size() - 1);
        assertThat(testAuthorityMirror.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    void patchNonExistingAuthorityMirror() throws Exception {
        int databaseSizeBeforeUpdate = authorityMirrorRepository.findAll().size();
        authorityMirror.setId(count.incrementAndGet());

        // Create the AuthorityMirror
        AuthorityMirrorDTO authorityMirrorDTO = authorityMirrorMapper.toDto(authorityMirror);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAuthorityMirrorMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, authorityMirrorDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(authorityMirrorDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the AuthorityMirror in the database
        List<AuthorityMirror> authorityMirrorList = authorityMirrorRepository.findAll();
        assertThat(authorityMirrorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchAuthorityMirror() throws Exception {
        int databaseSizeBeforeUpdate = authorityMirrorRepository.findAll().size();
        authorityMirror.setId(count.incrementAndGet());

        // Create the AuthorityMirror
        AuthorityMirrorDTO authorityMirrorDTO = authorityMirrorMapper.toDto(authorityMirror);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAuthorityMirrorMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(authorityMirrorDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the AuthorityMirror in the database
        List<AuthorityMirror> authorityMirrorList = authorityMirrorRepository.findAll();
        assertThat(authorityMirrorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamAuthorityMirror() throws Exception {
        int databaseSizeBeforeUpdate = authorityMirrorRepository.findAll().size();
        authorityMirror.setId(count.incrementAndGet());

        // Create the AuthorityMirror
        AuthorityMirrorDTO authorityMirrorDTO = authorityMirrorMapper.toDto(authorityMirror);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAuthorityMirrorMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(authorityMirrorDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the AuthorityMirror in the database
        List<AuthorityMirror> authorityMirrorList = authorityMirrorRepository.findAll();
        assertThat(authorityMirrorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteAuthorityMirror() throws Exception {
        // Initialize the database
        authorityMirrorRepository.saveAndFlush(authorityMirror);

        int databaseSizeBeforeDelete = authorityMirrorRepository.findAll().size();

        // Delete the authorityMirror
        restAuthorityMirrorMockMvc
            .perform(delete(ENTITY_API_URL_ID, authorityMirror.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AuthorityMirror> authorityMirrorList = authorityMirrorRepository.findAll();
        assertThat(authorityMirrorList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
