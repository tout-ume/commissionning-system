package sn.free.commissioning.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.IntegrationTest;
import sn.free.commissioning.domain.Commission;
import sn.free.commissioning.domain.CommissionAccount;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.repository.CommissionAccountRepository;
import sn.free.commissioning.service.criteria.CommissionAccountCriteria;
import sn.free.commissioning.service.dto.CommissionAccountDTO;
import sn.free.commissioning.service.mapper.CommissionAccountMapper;

/**
 * Integration tests for the {@link CommissionAccountResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CommissionAccountResourceIT {

    private static final Double DEFAULT_CALCULATED_BALANCE = 1D;
    private static final Double UPDATED_CALCULATED_BALANCE = 2D;
    private static final Double SMALLER_CALCULATED_BALANCE = 1D - 1D;

    private static final Double DEFAULT_PAID_BALANCE = 1D;
    private static final Double UPDATED_PAID_BALANCE = 2D;
    private static final Double SMALLER_PAID_BALANCE = 1D - 1D;

    private static final String DEFAULT_SPARE_1 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_2 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_3 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_3 = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/commission-accounts";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CommissionAccountRepository commissionAccountRepository;

    @Autowired
    private CommissionAccountMapper commissionAccountMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCommissionAccountMockMvc;

    private CommissionAccount commissionAccount;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CommissionAccount createEntity(EntityManager em) {
        CommissionAccount commissionAccount = new CommissionAccount()
            .calculatedBalance(DEFAULT_CALCULATED_BALANCE)
            .paidBalance(DEFAULT_PAID_BALANCE)
            .spare1(DEFAULT_SPARE_1)
            .spare2(DEFAULT_SPARE_2)
            .spare3(DEFAULT_SPARE_3);
        return commissionAccount;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CommissionAccount createUpdatedEntity(EntityManager em) {
        CommissionAccount commissionAccount = new CommissionAccount()
            .calculatedBalance(UPDATED_CALCULATED_BALANCE)
            .paidBalance(UPDATED_PAID_BALANCE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);
        return commissionAccount;
    }

    @BeforeEach
    public void initTest() {
        commissionAccount = createEntity(em);
    }

    @Test
    @Transactional
    void createCommissionAccount() throws Exception {
        int databaseSizeBeforeCreate = commissionAccountRepository.findAll().size();
        // Create the CommissionAccount
        CommissionAccountDTO commissionAccountDTO = commissionAccountMapper.toDto(commissionAccount);
        restCommissionAccountMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissionAccountDTO))
            )
            .andExpect(status().isCreated());

        // Validate the CommissionAccount in the database
        List<CommissionAccount> commissionAccountList = commissionAccountRepository.findAll();
        assertThat(commissionAccountList).hasSize(databaseSizeBeforeCreate + 1);
        CommissionAccount testCommissionAccount = commissionAccountList.get(commissionAccountList.size() - 1);
        assertThat(testCommissionAccount.getCalculatedBalance()).isEqualTo(DEFAULT_CALCULATED_BALANCE);
        assertThat(testCommissionAccount.getPaidBalance()).isEqualTo(DEFAULT_PAID_BALANCE);
        assertThat(testCommissionAccount.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testCommissionAccount.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
        assertThat(testCommissionAccount.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
    }

    @Test
    @Transactional
    void createCommissionAccountWithExistingId() throws Exception {
        // Create the CommissionAccount with an existing ID
        commissionAccount.setId(1L);
        CommissionAccountDTO commissionAccountDTO = commissionAccountMapper.toDto(commissionAccount);

        int databaseSizeBeforeCreate = commissionAccountRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCommissionAccountMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissionAccountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommissionAccount in the database
        List<CommissionAccount> commissionAccountList = commissionAccountRepository.findAll();
        assertThat(commissionAccountList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCommissionAccounts() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList
        restCommissionAccountMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(commissionAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].calculatedBalance").value(hasItem(DEFAULT_CALCULATED_BALANCE.doubleValue())))
            .andExpect(jsonPath("$.[*].paidBalance").value(hasItem(DEFAULT_PAID_BALANCE.doubleValue())))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)));
    }

    @Test
    @Transactional
    void getCommissionAccount() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get the commissionAccount
        restCommissionAccountMockMvc
            .perform(get(ENTITY_API_URL_ID, commissionAccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(commissionAccount.getId().intValue()))
            .andExpect(jsonPath("$.calculatedBalance").value(DEFAULT_CALCULATED_BALANCE.doubleValue()))
            .andExpect(jsonPath("$.paidBalance").value(DEFAULT_PAID_BALANCE.doubleValue()))
            .andExpect(jsonPath("$.spare1").value(DEFAULT_SPARE_1))
            .andExpect(jsonPath("$.spare2").value(DEFAULT_SPARE_2))
            .andExpect(jsonPath("$.spare3").value(DEFAULT_SPARE_3));
    }

    @Test
    @Transactional
    void getCommissionAccountsByIdFiltering() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        Long id = commissionAccount.getId();

        defaultCommissionAccountShouldBeFound("id.equals=" + id);
        defaultCommissionAccountShouldNotBeFound("id.notEquals=" + id);

        defaultCommissionAccountShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCommissionAccountShouldNotBeFound("id.greaterThan=" + id);

        defaultCommissionAccountShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCommissionAccountShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsByCalculatedBalanceIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where calculatedBalance equals to DEFAULT_CALCULATED_BALANCE
        defaultCommissionAccountShouldBeFound("calculatedBalance.equals=" + DEFAULT_CALCULATED_BALANCE);

        // Get all the commissionAccountList where calculatedBalance equals to UPDATED_CALCULATED_BALANCE
        defaultCommissionAccountShouldNotBeFound("calculatedBalance.equals=" + UPDATED_CALCULATED_BALANCE);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsByCalculatedBalanceIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where calculatedBalance not equals to DEFAULT_CALCULATED_BALANCE
        defaultCommissionAccountShouldNotBeFound("calculatedBalance.notEquals=" + DEFAULT_CALCULATED_BALANCE);

        // Get all the commissionAccountList where calculatedBalance not equals to UPDATED_CALCULATED_BALANCE
        defaultCommissionAccountShouldBeFound("calculatedBalance.notEquals=" + UPDATED_CALCULATED_BALANCE);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsByCalculatedBalanceIsInShouldWork() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where calculatedBalance in DEFAULT_CALCULATED_BALANCE or UPDATED_CALCULATED_BALANCE
        defaultCommissionAccountShouldBeFound("calculatedBalance.in=" + DEFAULT_CALCULATED_BALANCE + "," + UPDATED_CALCULATED_BALANCE);

        // Get all the commissionAccountList where calculatedBalance equals to UPDATED_CALCULATED_BALANCE
        defaultCommissionAccountShouldNotBeFound("calculatedBalance.in=" + UPDATED_CALCULATED_BALANCE);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsByCalculatedBalanceIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where calculatedBalance is not null
        defaultCommissionAccountShouldBeFound("calculatedBalance.specified=true");

        // Get all the commissionAccountList where calculatedBalance is null
        defaultCommissionAccountShouldNotBeFound("calculatedBalance.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionAccountsByCalculatedBalanceIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where calculatedBalance is greater than or equal to DEFAULT_CALCULATED_BALANCE
        defaultCommissionAccountShouldBeFound("calculatedBalance.greaterThanOrEqual=" + DEFAULT_CALCULATED_BALANCE);

        // Get all the commissionAccountList where calculatedBalance is greater than or equal to UPDATED_CALCULATED_BALANCE
        defaultCommissionAccountShouldNotBeFound("calculatedBalance.greaterThanOrEqual=" + UPDATED_CALCULATED_BALANCE);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsByCalculatedBalanceIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where calculatedBalance is less than or equal to DEFAULT_CALCULATED_BALANCE
        defaultCommissionAccountShouldBeFound("calculatedBalance.lessThanOrEqual=" + DEFAULT_CALCULATED_BALANCE);

        // Get all the commissionAccountList where calculatedBalance is less than or equal to SMALLER_CALCULATED_BALANCE
        defaultCommissionAccountShouldNotBeFound("calculatedBalance.lessThanOrEqual=" + SMALLER_CALCULATED_BALANCE);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsByCalculatedBalanceIsLessThanSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where calculatedBalance is less than DEFAULT_CALCULATED_BALANCE
        defaultCommissionAccountShouldNotBeFound("calculatedBalance.lessThan=" + DEFAULT_CALCULATED_BALANCE);

        // Get all the commissionAccountList where calculatedBalance is less than UPDATED_CALCULATED_BALANCE
        defaultCommissionAccountShouldBeFound("calculatedBalance.lessThan=" + UPDATED_CALCULATED_BALANCE);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsByCalculatedBalanceIsGreaterThanSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where calculatedBalance is greater than DEFAULT_CALCULATED_BALANCE
        defaultCommissionAccountShouldNotBeFound("calculatedBalance.greaterThan=" + DEFAULT_CALCULATED_BALANCE);

        // Get all the commissionAccountList where calculatedBalance is greater than SMALLER_CALCULATED_BALANCE
        defaultCommissionAccountShouldBeFound("calculatedBalance.greaterThan=" + SMALLER_CALCULATED_BALANCE);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsByPaidBalanceIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where paidBalance equals to DEFAULT_PAID_BALANCE
        defaultCommissionAccountShouldBeFound("paidBalance.equals=" + DEFAULT_PAID_BALANCE);

        // Get all the commissionAccountList where paidBalance equals to UPDATED_PAID_BALANCE
        defaultCommissionAccountShouldNotBeFound("paidBalance.equals=" + UPDATED_PAID_BALANCE);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsByPaidBalanceIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where paidBalance not equals to DEFAULT_PAID_BALANCE
        defaultCommissionAccountShouldNotBeFound("paidBalance.notEquals=" + DEFAULT_PAID_BALANCE);

        // Get all the commissionAccountList where paidBalance not equals to UPDATED_PAID_BALANCE
        defaultCommissionAccountShouldBeFound("paidBalance.notEquals=" + UPDATED_PAID_BALANCE);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsByPaidBalanceIsInShouldWork() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where paidBalance in DEFAULT_PAID_BALANCE or UPDATED_PAID_BALANCE
        defaultCommissionAccountShouldBeFound("paidBalance.in=" + DEFAULT_PAID_BALANCE + "," + UPDATED_PAID_BALANCE);

        // Get all the commissionAccountList where paidBalance equals to UPDATED_PAID_BALANCE
        defaultCommissionAccountShouldNotBeFound("paidBalance.in=" + UPDATED_PAID_BALANCE);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsByPaidBalanceIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where paidBalance is not null
        defaultCommissionAccountShouldBeFound("paidBalance.specified=true");

        // Get all the commissionAccountList where paidBalance is null
        defaultCommissionAccountShouldNotBeFound("paidBalance.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionAccountsByPaidBalanceIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where paidBalance is greater than or equal to DEFAULT_PAID_BALANCE
        defaultCommissionAccountShouldBeFound("paidBalance.greaterThanOrEqual=" + DEFAULT_PAID_BALANCE);

        // Get all the commissionAccountList where paidBalance is greater than or equal to UPDATED_PAID_BALANCE
        defaultCommissionAccountShouldNotBeFound("paidBalance.greaterThanOrEqual=" + UPDATED_PAID_BALANCE);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsByPaidBalanceIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where paidBalance is less than or equal to DEFAULT_PAID_BALANCE
        defaultCommissionAccountShouldBeFound("paidBalance.lessThanOrEqual=" + DEFAULT_PAID_BALANCE);

        // Get all the commissionAccountList where paidBalance is less than or equal to SMALLER_PAID_BALANCE
        defaultCommissionAccountShouldNotBeFound("paidBalance.lessThanOrEqual=" + SMALLER_PAID_BALANCE);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsByPaidBalanceIsLessThanSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where paidBalance is less than DEFAULT_PAID_BALANCE
        defaultCommissionAccountShouldNotBeFound("paidBalance.lessThan=" + DEFAULT_PAID_BALANCE);

        // Get all the commissionAccountList where paidBalance is less than UPDATED_PAID_BALANCE
        defaultCommissionAccountShouldBeFound("paidBalance.lessThan=" + UPDATED_PAID_BALANCE);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsByPaidBalanceIsGreaterThanSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where paidBalance is greater than DEFAULT_PAID_BALANCE
        defaultCommissionAccountShouldNotBeFound("paidBalance.greaterThan=" + DEFAULT_PAID_BALANCE);

        // Get all the commissionAccountList where paidBalance is greater than SMALLER_PAID_BALANCE
        defaultCommissionAccountShouldBeFound("paidBalance.greaterThan=" + SMALLER_PAID_BALANCE);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsBySpare1IsEqualToSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where spare1 equals to DEFAULT_SPARE_1
        defaultCommissionAccountShouldBeFound("spare1.equals=" + DEFAULT_SPARE_1);

        // Get all the commissionAccountList where spare1 equals to UPDATED_SPARE_1
        defaultCommissionAccountShouldNotBeFound("spare1.equals=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsBySpare1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where spare1 not equals to DEFAULT_SPARE_1
        defaultCommissionAccountShouldNotBeFound("spare1.notEquals=" + DEFAULT_SPARE_1);

        // Get all the commissionAccountList where spare1 not equals to UPDATED_SPARE_1
        defaultCommissionAccountShouldBeFound("spare1.notEquals=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsBySpare1IsInShouldWork() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where spare1 in DEFAULT_SPARE_1 or UPDATED_SPARE_1
        defaultCommissionAccountShouldBeFound("spare1.in=" + DEFAULT_SPARE_1 + "," + UPDATED_SPARE_1);

        // Get all the commissionAccountList where spare1 equals to UPDATED_SPARE_1
        defaultCommissionAccountShouldNotBeFound("spare1.in=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsBySpare1IsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where spare1 is not null
        defaultCommissionAccountShouldBeFound("spare1.specified=true");

        // Get all the commissionAccountList where spare1 is null
        defaultCommissionAccountShouldNotBeFound("spare1.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionAccountsBySpare1ContainsSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where spare1 contains DEFAULT_SPARE_1
        defaultCommissionAccountShouldBeFound("spare1.contains=" + DEFAULT_SPARE_1);

        // Get all the commissionAccountList where spare1 contains UPDATED_SPARE_1
        defaultCommissionAccountShouldNotBeFound("spare1.contains=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsBySpare1NotContainsSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where spare1 does not contain DEFAULT_SPARE_1
        defaultCommissionAccountShouldNotBeFound("spare1.doesNotContain=" + DEFAULT_SPARE_1);

        // Get all the commissionAccountList where spare1 does not contain UPDATED_SPARE_1
        defaultCommissionAccountShouldBeFound("spare1.doesNotContain=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsBySpare2IsEqualToSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where spare2 equals to DEFAULT_SPARE_2
        defaultCommissionAccountShouldBeFound("spare2.equals=" + DEFAULT_SPARE_2);

        // Get all the commissionAccountList where spare2 equals to UPDATED_SPARE_2
        defaultCommissionAccountShouldNotBeFound("spare2.equals=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsBySpare2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where spare2 not equals to DEFAULT_SPARE_2
        defaultCommissionAccountShouldNotBeFound("spare2.notEquals=" + DEFAULT_SPARE_2);

        // Get all the commissionAccountList where spare2 not equals to UPDATED_SPARE_2
        defaultCommissionAccountShouldBeFound("spare2.notEquals=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsBySpare2IsInShouldWork() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where spare2 in DEFAULT_SPARE_2 or UPDATED_SPARE_2
        defaultCommissionAccountShouldBeFound("spare2.in=" + DEFAULT_SPARE_2 + "," + UPDATED_SPARE_2);

        // Get all the commissionAccountList where spare2 equals to UPDATED_SPARE_2
        defaultCommissionAccountShouldNotBeFound("spare2.in=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsBySpare2IsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where spare2 is not null
        defaultCommissionAccountShouldBeFound("spare2.specified=true");

        // Get all the commissionAccountList where spare2 is null
        defaultCommissionAccountShouldNotBeFound("spare2.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionAccountsBySpare2ContainsSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where spare2 contains DEFAULT_SPARE_2
        defaultCommissionAccountShouldBeFound("spare2.contains=" + DEFAULT_SPARE_2);

        // Get all the commissionAccountList where spare2 contains UPDATED_SPARE_2
        defaultCommissionAccountShouldNotBeFound("spare2.contains=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsBySpare2NotContainsSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where spare2 does not contain DEFAULT_SPARE_2
        defaultCommissionAccountShouldNotBeFound("spare2.doesNotContain=" + DEFAULT_SPARE_2);

        // Get all the commissionAccountList where spare2 does not contain UPDATED_SPARE_2
        defaultCommissionAccountShouldBeFound("spare2.doesNotContain=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsBySpare3IsEqualToSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where spare3 equals to DEFAULT_SPARE_3
        defaultCommissionAccountShouldBeFound("spare3.equals=" + DEFAULT_SPARE_3);

        // Get all the commissionAccountList where spare3 equals to UPDATED_SPARE_3
        defaultCommissionAccountShouldNotBeFound("spare3.equals=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsBySpare3IsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where spare3 not equals to DEFAULT_SPARE_3
        defaultCommissionAccountShouldNotBeFound("spare3.notEquals=" + DEFAULT_SPARE_3);

        // Get all the commissionAccountList where spare3 not equals to UPDATED_SPARE_3
        defaultCommissionAccountShouldBeFound("spare3.notEquals=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsBySpare3IsInShouldWork() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where spare3 in DEFAULT_SPARE_3 or UPDATED_SPARE_3
        defaultCommissionAccountShouldBeFound("spare3.in=" + DEFAULT_SPARE_3 + "," + UPDATED_SPARE_3);

        // Get all the commissionAccountList where spare3 equals to UPDATED_SPARE_3
        defaultCommissionAccountShouldNotBeFound("spare3.in=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsBySpare3IsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where spare3 is not null
        defaultCommissionAccountShouldBeFound("spare3.specified=true");

        // Get all the commissionAccountList where spare3 is null
        defaultCommissionAccountShouldNotBeFound("spare3.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionAccountsBySpare3ContainsSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where spare3 contains DEFAULT_SPARE_3
        defaultCommissionAccountShouldBeFound("spare3.contains=" + DEFAULT_SPARE_3);

        // Get all the commissionAccountList where spare3 contains UPDATED_SPARE_3
        defaultCommissionAccountShouldNotBeFound("spare3.contains=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsBySpare3NotContainsSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        // Get all the commissionAccountList where spare3 does not contain DEFAULT_SPARE_3
        defaultCommissionAccountShouldNotBeFound("spare3.doesNotContain=" + DEFAULT_SPARE_3);

        // Get all the commissionAccountList where spare3 does not contain UPDATED_SPARE_3
        defaultCommissionAccountShouldBeFound("spare3.doesNotContain=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCommissionAccountsByPartnerIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);
        Partner partner;
        if (TestUtil.findAll(em, Partner.class).isEmpty()) {
            partner = PartnerResourceIT.createEntity(em);
            em.persist(partner);
            em.flush();
        } else {
            partner = TestUtil.findAll(em, Partner.class).get(0);
        }
        em.persist(partner);
        em.flush();
        commissionAccount.setPartner(partner);
        commissionAccountRepository.saveAndFlush(commissionAccount);
        Long partnerId = partner.getId();

        // Get all the commissionAccountList where partner equals to partnerId
        defaultCommissionAccountShouldBeFound("partnerId.equals=" + partnerId);

        // Get all the commissionAccountList where partner equals to (partnerId + 1)
        defaultCommissionAccountShouldNotBeFound("partnerId.equals=" + (partnerId + 1));
    }

    @Test
    @Transactional
    void getAllCommissionAccountsByCommissionIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);
        Commission commission;
        if (TestUtil.findAll(em, Commission.class).isEmpty()) {
            commission = CommissionResourceIT.createEntity(em);
            em.persist(commission);
            em.flush();
        } else {
            commission = TestUtil.findAll(em, Commission.class).get(0);
        }
        em.persist(commission);
        em.flush();
        commissionAccount.addCommission(commission);
        commissionAccountRepository.saveAndFlush(commissionAccount);
        Long commissionId = commission.getId();

        // Get all the commissionAccountList where commission equals to commissionId
        defaultCommissionAccountShouldBeFound("commissionId.equals=" + commissionId);

        // Get all the commissionAccountList where commission equals to (commissionId + 1)
        defaultCommissionAccountShouldNotBeFound("commissionId.equals=" + (commissionId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCommissionAccountShouldBeFound(String filter) throws Exception {
        restCommissionAccountMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(commissionAccount.getId().intValue())))
            .andExpect(jsonPath("$.[*].calculatedBalance").value(hasItem(DEFAULT_CALCULATED_BALANCE.doubleValue())))
            .andExpect(jsonPath("$.[*].paidBalance").value(hasItem(DEFAULT_PAID_BALANCE.doubleValue())))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)));

        // Check, that the count call also returns 1
        restCommissionAccountMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCommissionAccountShouldNotBeFound(String filter) throws Exception {
        restCommissionAccountMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCommissionAccountMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingCommissionAccount() throws Exception {
        // Get the commissionAccount
        restCommissionAccountMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCommissionAccount() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        int databaseSizeBeforeUpdate = commissionAccountRepository.findAll().size();

        // Update the commissionAccount
        CommissionAccount updatedCommissionAccount = commissionAccountRepository.findById(commissionAccount.getId()).get();
        // Disconnect from session so that the updates on updatedCommissionAccount are not directly saved in db
        em.detach(updatedCommissionAccount);
        updatedCommissionAccount
            .calculatedBalance(UPDATED_CALCULATED_BALANCE)
            .paidBalance(UPDATED_PAID_BALANCE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);
        CommissionAccountDTO commissionAccountDTO = commissionAccountMapper.toDto(updatedCommissionAccount);

        restCommissionAccountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, commissionAccountDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissionAccountDTO))
            )
            .andExpect(status().isOk());

        // Validate the CommissionAccount in the database
        List<CommissionAccount> commissionAccountList = commissionAccountRepository.findAll();
        assertThat(commissionAccountList).hasSize(databaseSizeBeforeUpdate);
        CommissionAccount testCommissionAccount = commissionAccountList.get(commissionAccountList.size() - 1);
        assertThat(testCommissionAccount.getCalculatedBalance()).isEqualTo(UPDATED_CALCULATED_BALANCE);
        assertThat(testCommissionAccount.getPaidBalance()).isEqualTo(UPDATED_PAID_BALANCE);
        assertThat(testCommissionAccount.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testCommissionAccount.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testCommissionAccount.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void putNonExistingCommissionAccount() throws Exception {
        int databaseSizeBeforeUpdate = commissionAccountRepository.findAll().size();
        commissionAccount.setId(count.incrementAndGet());

        // Create the CommissionAccount
        CommissionAccountDTO commissionAccountDTO = commissionAccountMapper.toDto(commissionAccount);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCommissionAccountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, commissionAccountDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissionAccountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommissionAccount in the database
        List<CommissionAccount> commissionAccountList = commissionAccountRepository.findAll();
        assertThat(commissionAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCommissionAccount() throws Exception {
        int databaseSizeBeforeUpdate = commissionAccountRepository.findAll().size();
        commissionAccount.setId(count.incrementAndGet());

        // Create the CommissionAccount
        CommissionAccountDTO commissionAccountDTO = commissionAccountMapper.toDto(commissionAccount);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommissionAccountMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissionAccountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommissionAccount in the database
        List<CommissionAccount> commissionAccountList = commissionAccountRepository.findAll();
        assertThat(commissionAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCommissionAccount() throws Exception {
        int databaseSizeBeforeUpdate = commissionAccountRepository.findAll().size();
        commissionAccount.setId(count.incrementAndGet());

        // Create the CommissionAccount
        CommissionAccountDTO commissionAccountDTO = commissionAccountMapper.toDto(commissionAccount);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommissionAccountMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(commissionAccountDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CommissionAccount in the database
        List<CommissionAccount> commissionAccountList = commissionAccountRepository.findAll();
        assertThat(commissionAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCommissionAccountWithPatch() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        int databaseSizeBeforeUpdate = commissionAccountRepository.findAll().size();

        // Update the commissionAccount using partial update
        CommissionAccount partialUpdatedCommissionAccount = new CommissionAccount();
        partialUpdatedCommissionAccount.setId(commissionAccount.getId());

        partialUpdatedCommissionAccount
            .calculatedBalance(UPDATED_CALCULATED_BALANCE)
            .paidBalance(UPDATED_PAID_BALANCE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);

        restCommissionAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCommissionAccount.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCommissionAccount))
            )
            .andExpect(status().isOk());

        // Validate the CommissionAccount in the database
        List<CommissionAccount> commissionAccountList = commissionAccountRepository.findAll();
        assertThat(commissionAccountList).hasSize(databaseSizeBeforeUpdate);
        CommissionAccount testCommissionAccount = commissionAccountList.get(commissionAccountList.size() - 1);
        assertThat(testCommissionAccount.getCalculatedBalance()).isEqualTo(UPDATED_CALCULATED_BALANCE);
        assertThat(testCommissionAccount.getPaidBalance()).isEqualTo(UPDATED_PAID_BALANCE);
        assertThat(testCommissionAccount.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testCommissionAccount.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testCommissionAccount.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void fullUpdateCommissionAccountWithPatch() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        int databaseSizeBeforeUpdate = commissionAccountRepository.findAll().size();

        // Update the commissionAccount using partial update
        CommissionAccount partialUpdatedCommissionAccount = new CommissionAccount();
        partialUpdatedCommissionAccount.setId(commissionAccount.getId());

        partialUpdatedCommissionAccount
            .calculatedBalance(UPDATED_CALCULATED_BALANCE)
            .paidBalance(UPDATED_PAID_BALANCE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);

        restCommissionAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCommissionAccount.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCommissionAccount))
            )
            .andExpect(status().isOk());

        // Validate the CommissionAccount in the database
        List<CommissionAccount> commissionAccountList = commissionAccountRepository.findAll();
        assertThat(commissionAccountList).hasSize(databaseSizeBeforeUpdate);
        CommissionAccount testCommissionAccount = commissionAccountList.get(commissionAccountList.size() - 1);
        assertThat(testCommissionAccount.getCalculatedBalance()).isEqualTo(UPDATED_CALCULATED_BALANCE);
        assertThat(testCommissionAccount.getPaidBalance()).isEqualTo(UPDATED_PAID_BALANCE);
        assertThat(testCommissionAccount.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testCommissionAccount.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testCommissionAccount.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void patchNonExistingCommissionAccount() throws Exception {
        int databaseSizeBeforeUpdate = commissionAccountRepository.findAll().size();
        commissionAccount.setId(count.incrementAndGet());

        // Create the CommissionAccount
        CommissionAccountDTO commissionAccountDTO = commissionAccountMapper.toDto(commissionAccount);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCommissionAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, commissionAccountDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(commissionAccountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommissionAccount in the database
        List<CommissionAccount> commissionAccountList = commissionAccountRepository.findAll();
        assertThat(commissionAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCommissionAccount() throws Exception {
        int databaseSizeBeforeUpdate = commissionAccountRepository.findAll().size();
        commissionAccount.setId(count.incrementAndGet());

        // Create the CommissionAccount
        CommissionAccountDTO commissionAccountDTO = commissionAccountMapper.toDto(commissionAccount);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommissionAccountMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(commissionAccountDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommissionAccount in the database
        List<CommissionAccount> commissionAccountList = commissionAccountRepository.findAll();
        assertThat(commissionAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCommissionAccount() throws Exception {
        int databaseSizeBeforeUpdate = commissionAccountRepository.findAll().size();
        commissionAccount.setId(count.incrementAndGet());

        // Create the CommissionAccount
        CommissionAccountDTO commissionAccountDTO = commissionAccountMapper.toDto(commissionAccount);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommissionAccountMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(commissionAccountDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CommissionAccount in the database
        List<CommissionAccount> commissionAccountList = commissionAccountRepository.findAll();
        assertThat(commissionAccountList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCommissionAccount() throws Exception {
        // Initialize the database
        commissionAccountRepository.saveAndFlush(commissionAccount);

        int databaseSizeBeforeDelete = commissionAccountRepository.findAll().size();

        // Delete the commissionAccount
        restCommissionAccountMockMvc
            .perform(delete(ENTITY_API_URL_ID, commissionAccount.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CommissionAccount> commissionAccountList = commissionAccountRepository.findAll();
        assertThat(commissionAccountList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
