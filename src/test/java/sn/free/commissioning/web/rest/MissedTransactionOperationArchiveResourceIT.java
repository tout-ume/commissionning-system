package sn.free.commissioning.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.IntegrationTest;
import sn.free.commissioning.domain.MissedTransactionOperationArchive;
import sn.free.commissioning.repository.MissedTransactionOperationArchiveRepository;
import sn.free.commissioning.service.criteria.MissedTransactionOperationArchiveCriteria;
import sn.free.commissioning.service.dto.MissedTransactionOperationArchiveDTO;
import sn.free.commissioning.service.mapper.MissedTransactionOperationArchiveMapper;

/**
 * Integration tests for the {@link MissedTransactionOperationArchiveResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MissedTransactionOperationArchiveResourceIT {

    private static final Double DEFAULT_AMOUNT = 1D;
    private static final Double UPDATED_AMOUNT = 2D;
    private static final Double SMALLER_AMOUNT = 1D - 1D;

    private static final String DEFAULT_SUBS_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_SUBS_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_AGENT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_AGENT_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE_TRANSACTION = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_TRANSACTION = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_TRANSACTION_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_TRANSACTION_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_SENDER_ZONE = "AAAAAAAAAA";
    private static final String UPDATED_SENDER_ZONE = "BBBBBBBBBB";

    private static final String DEFAULT_SENDER_PROFILE = "AAAAAAAAAA";
    private static final String UPDATED_SENDER_PROFILE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE_TERRITORY = "AAAAAAAAAA";
    private static final String UPDATED_CODE_TERRITORY = "BBBBBBBBBB";

    private static final String DEFAULT_SUB_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_SUB_TYPE = "BBBBBBBBBB";

    private static final Instant DEFAULT_OPERATION_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_OPERATION_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_IS_FRAUD = false;
    private static final Boolean UPDATED_IS_FRAUD = true;

    private static final Instant DEFAULT_TAGGED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TAGGED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_FRAUD_SOURCE = "AAAAAAAAAA";
    private static final String UPDATED_FRAUD_SOURCE = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENT = "BBBBBBBBBB";

    private static final Instant DEFAULT_FALSE_POSITIVE_DETECTED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FALSE_POSITIVE_DETECTED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_TID = "AAAAAAAAAA";
    private static final String UPDATED_TID = "BBBBBBBBBB";

    private static final String DEFAULT_PARENT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_PARENT_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_FCT_DT = "AAAAAAAAAA";
    private static final String UPDATED_FCT_DT = "BBBBBBBBBB";

    private static final String DEFAULT_PARENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_PARENT_ID = "BBBBBBBBBB";

    private static final Instant DEFAULT_CANCELED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CANCELED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CANCELED_ID = "AAAAAAAAAA";
    private static final String UPDATED_CANCELED_ID = "BBBBBBBBBB";

    private static final String DEFAULT_PRODUCT_ID = "AAAAAAAAAA";
    private static final String UPDATED_PRODUCT_ID = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/missed-transaction-operation-archives";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MissedTransactionOperationArchiveRepository missedTransactionOperationArchiveRepository;

    @Autowired
    private MissedTransactionOperationArchiveMapper missedTransactionOperationArchiveMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMissedTransactionOperationArchiveMockMvc;

    private MissedTransactionOperationArchive missedTransactionOperationArchive;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MissedTransactionOperationArchive createEntity(EntityManager em) {
        MissedTransactionOperationArchive missedTransactionOperationArchive = new MissedTransactionOperationArchive()
            .amount(DEFAULT_AMOUNT)
            .subsMsisdn(DEFAULT_SUBS_MSISDN)
            .agentMsisdn(DEFAULT_AGENT_MSISDN)
            .typeTransaction(DEFAULT_TYPE_TRANSACTION)
            .createdAt(DEFAULT_CREATED_AT)
            .transactionStatus(DEFAULT_TRANSACTION_STATUS)
            .senderZone(DEFAULT_SENDER_ZONE)
            .senderProfile(DEFAULT_SENDER_PROFILE)
            .codeTerritory(DEFAULT_CODE_TERRITORY)
            .subType(DEFAULT_SUB_TYPE)
            .operationDate(DEFAULT_OPERATION_DATE)
            .isFraud(DEFAULT_IS_FRAUD)
            .taggedAt(DEFAULT_TAGGED_AT)
            .fraudSource(DEFAULT_FRAUD_SOURCE)
            .comment(DEFAULT_COMMENT)
            .falsePositiveDetectedAt(DEFAULT_FALSE_POSITIVE_DETECTED_AT)
            .tid(DEFAULT_TID)
            .parentMsisdn(DEFAULT_PARENT_MSISDN)
            .fctDt(DEFAULT_FCT_DT)
            .parentId(DEFAULT_PARENT_ID)
            .canceledAt(DEFAULT_CANCELED_AT)
            .canceledId(DEFAULT_CANCELED_ID)
            .productId(DEFAULT_PRODUCT_ID);
        return missedTransactionOperationArchive;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MissedTransactionOperationArchive createUpdatedEntity(EntityManager em) {
        MissedTransactionOperationArchive missedTransactionOperationArchive = new MissedTransactionOperationArchive()
            .amount(UPDATED_AMOUNT)
            .subsMsisdn(UPDATED_SUBS_MSISDN)
            .agentMsisdn(UPDATED_AGENT_MSISDN)
            .typeTransaction(UPDATED_TYPE_TRANSACTION)
            .createdAt(UPDATED_CREATED_AT)
            .transactionStatus(UPDATED_TRANSACTION_STATUS)
            .senderZone(UPDATED_SENDER_ZONE)
            .senderProfile(UPDATED_SENDER_PROFILE)
            .codeTerritory(UPDATED_CODE_TERRITORY)
            .subType(UPDATED_SUB_TYPE)
            .operationDate(UPDATED_OPERATION_DATE)
            .isFraud(UPDATED_IS_FRAUD)
            .taggedAt(UPDATED_TAGGED_AT)
            .fraudSource(UPDATED_FRAUD_SOURCE)
            .comment(UPDATED_COMMENT)
            .falsePositiveDetectedAt(UPDATED_FALSE_POSITIVE_DETECTED_AT)
            .tid(UPDATED_TID)
            .parentMsisdn(UPDATED_PARENT_MSISDN)
            .fctDt(UPDATED_FCT_DT)
            .parentId(UPDATED_PARENT_ID)
            .canceledAt(UPDATED_CANCELED_AT)
            .canceledId(UPDATED_CANCELED_ID)
            .productId(UPDATED_PRODUCT_ID);
        return missedTransactionOperationArchive;
    }

    @BeforeEach
    public void initTest() {
        missedTransactionOperationArchive = createEntity(em);
    }

    @Test
    @Transactional
    void createMissedTransactionOperationArchive() throws Exception {
        int databaseSizeBeforeCreate = missedTransactionOperationArchiveRepository.findAll().size();
        // Create the MissedTransactionOperationArchive
        MissedTransactionOperationArchiveDTO missedTransactionOperationArchiveDTO = missedTransactionOperationArchiveMapper.toDto(
            missedTransactionOperationArchive
        );
        restMissedTransactionOperationArchiveMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(missedTransactionOperationArchiveDTO))
            )
            .andExpect(status().isCreated());

        // Validate the MissedTransactionOperationArchive in the database
        List<MissedTransactionOperationArchive> missedTransactionOperationArchiveList = missedTransactionOperationArchiveRepository.findAll();
        assertThat(missedTransactionOperationArchiveList).hasSize(databaseSizeBeforeCreate + 1);
        MissedTransactionOperationArchive testMissedTransactionOperationArchive = missedTransactionOperationArchiveList.get(
            missedTransactionOperationArchiveList.size() - 1
        );
        assertThat(testMissedTransactionOperationArchive.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testMissedTransactionOperationArchive.getSubsMsisdn()).isEqualTo(DEFAULT_SUBS_MSISDN);
        assertThat(testMissedTransactionOperationArchive.getAgentMsisdn()).isEqualTo(DEFAULT_AGENT_MSISDN);
        assertThat(testMissedTransactionOperationArchive.getTypeTransaction()).isEqualTo(DEFAULT_TYPE_TRANSACTION);
        assertThat(testMissedTransactionOperationArchive.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testMissedTransactionOperationArchive.getTransactionStatus()).isEqualTo(DEFAULT_TRANSACTION_STATUS);
        assertThat(testMissedTransactionOperationArchive.getSenderZone()).isEqualTo(DEFAULT_SENDER_ZONE);
        assertThat(testMissedTransactionOperationArchive.getSenderProfile()).isEqualTo(DEFAULT_SENDER_PROFILE);
        assertThat(testMissedTransactionOperationArchive.getCodeTerritory()).isEqualTo(DEFAULT_CODE_TERRITORY);
        assertThat(testMissedTransactionOperationArchive.getSubType()).isEqualTo(DEFAULT_SUB_TYPE);
        assertThat(testMissedTransactionOperationArchive.getOperationDate()).isEqualTo(DEFAULT_OPERATION_DATE);
        assertThat(testMissedTransactionOperationArchive.getIsFraud()).isEqualTo(DEFAULT_IS_FRAUD);
        assertThat(testMissedTransactionOperationArchive.getTaggedAt()).isEqualTo(DEFAULT_TAGGED_AT);
        assertThat(testMissedTransactionOperationArchive.getFraudSource()).isEqualTo(DEFAULT_FRAUD_SOURCE);
        assertThat(testMissedTransactionOperationArchive.getComment()).isEqualTo(DEFAULT_COMMENT);
        assertThat(testMissedTransactionOperationArchive.getFalsePositiveDetectedAt()).isEqualTo(DEFAULT_FALSE_POSITIVE_DETECTED_AT);
        assertThat(testMissedTransactionOperationArchive.getTid()).isEqualTo(DEFAULT_TID);
        assertThat(testMissedTransactionOperationArchive.getParentMsisdn()).isEqualTo(DEFAULT_PARENT_MSISDN);
        assertThat(testMissedTransactionOperationArchive.getFctDt()).isEqualTo(DEFAULT_FCT_DT);
        assertThat(testMissedTransactionOperationArchive.getParentId()).isEqualTo(DEFAULT_PARENT_ID);
        assertThat(testMissedTransactionOperationArchive.getCanceledAt()).isEqualTo(DEFAULT_CANCELED_AT);
        assertThat(testMissedTransactionOperationArchive.getCanceledId()).isEqualTo(DEFAULT_CANCELED_ID);
        assertThat(testMissedTransactionOperationArchive.getProductId()).isEqualTo(DEFAULT_PRODUCT_ID);
    }

    @Test
    @Transactional
    void createMissedTransactionOperationArchiveWithExistingId() throws Exception {
        // Create the MissedTransactionOperationArchive with an existing ID
        missedTransactionOperationArchive.setId(1L);
        MissedTransactionOperationArchiveDTO missedTransactionOperationArchiveDTO = missedTransactionOperationArchiveMapper.toDto(
            missedTransactionOperationArchive
        );

        int databaseSizeBeforeCreate = missedTransactionOperationArchiveRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMissedTransactionOperationArchiveMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(missedTransactionOperationArchiveDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MissedTransactionOperationArchive in the database
        List<MissedTransactionOperationArchive> missedTransactionOperationArchiveList = missedTransactionOperationArchiveRepository.findAll();
        assertThat(missedTransactionOperationArchiveList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchives() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList
        restMissedTransactionOperationArchiveMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(missedTransactionOperationArchive.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].subsMsisdn").value(hasItem(DEFAULT_SUBS_MSISDN)))
            .andExpect(jsonPath("$.[*].agentMsisdn").value(hasItem(DEFAULT_AGENT_MSISDN)))
            .andExpect(jsonPath("$.[*].typeTransaction").value(hasItem(DEFAULT_TYPE_TRANSACTION)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].transactionStatus").value(hasItem(DEFAULT_TRANSACTION_STATUS)))
            .andExpect(jsonPath("$.[*].senderZone").value(hasItem(DEFAULT_SENDER_ZONE)))
            .andExpect(jsonPath("$.[*].senderProfile").value(hasItem(DEFAULT_SENDER_PROFILE)))
            .andExpect(jsonPath("$.[*].codeTerritory").value(hasItem(DEFAULT_CODE_TERRITORY)))
            .andExpect(jsonPath("$.[*].subType").value(hasItem(DEFAULT_SUB_TYPE)))
            .andExpect(jsonPath("$.[*].operationDate").value(hasItem(DEFAULT_OPERATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].isFraud").value(hasItem(DEFAULT_IS_FRAUD.booleanValue())))
            .andExpect(jsonPath("$.[*].taggedAt").value(hasItem(DEFAULT_TAGGED_AT.toString())))
            .andExpect(jsonPath("$.[*].fraudSource").value(hasItem(DEFAULT_FRAUD_SOURCE)))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT)))
            .andExpect(jsonPath("$.[*].falsePositiveDetectedAt").value(hasItem(DEFAULT_FALSE_POSITIVE_DETECTED_AT.toString())))
            .andExpect(jsonPath("$.[*].tid").value(hasItem(DEFAULT_TID)))
            .andExpect(jsonPath("$.[*].parentMsisdn").value(hasItem(DEFAULT_PARENT_MSISDN)))
            .andExpect(jsonPath("$.[*].fctDt").value(hasItem(DEFAULT_FCT_DT)))
            .andExpect(jsonPath("$.[*].parentId").value(hasItem(DEFAULT_PARENT_ID)))
            .andExpect(jsonPath("$.[*].canceledAt").value(hasItem(DEFAULT_CANCELED_AT.toString())))
            .andExpect(jsonPath("$.[*].canceledId").value(hasItem(DEFAULT_CANCELED_ID)))
            .andExpect(jsonPath("$.[*].productId").value(hasItem(DEFAULT_PRODUCT_ID)));
    }

    @Test
    @Transactional
    void getMissedTransactionOperationArchive() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get the missedTransactionOperationArchive
        restMissedTransactionOperationArchiveMockMvc
            .perform(get(ENTITY_API_URL_ID, missedTransactionOperationArchive.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(missedTransactionOperationArchive.getId().intValue()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.subsMsisdn").value(DEFAULT_SUBS_MSISDN))
            .andExpect(jsonPath("$.agentMsisdn").value(DEFAULT_AGENT_MSISDN))
            .andExpect(jsonPath("$.typeTransaction").value(DEFAULT_TYPE_TRANSACTION))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.transactionStatus").value(DEFAULT_TRANSACTION_STATUS))
            .andExpect(jsonPath("$.senderZone").value(DEFAULT_SENDER_ZONE))
            .andExpect(jsonPath("$.senderProfile").value(DEFAULT_SENDER_PROFILE))
            .andExpect(jsonPath("$.codeTerritory").value(DEFAULT_CODE_TERRITORY))
            .andExpect(jsonPath("$.subType").value(DEFAULT_SUB_TYPE))
            .andExpect(jsonPath("$.operationDate").value(DEFAULT_OPERATION_DATE.toString()))
            .andExpect(jsonPath("$.isFraud").value(DEFAULT_IS_FRAUD.booleanValue()))
            .andExpect(jsonPath("$.taggedAt").value(DEFAULT_TAGGED_AT.toString()))
            .andExpect(jsonPath("$.fraudSource").value(DEFAULT_FRAUD_SOURCE))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT))
            .andExpect(jsonPath("$.falsePositiveDetectedAt").value(DEFAULT_FALSE_POSITIVE_DETECTED_AT.toString()))
            .andExpect(jsonPath("$.tid").value(DEFAULT_TID))
            .andExpect(jsonPath("$.parentMsisdn").value(DEFAULT_PARENT_MSISDN))
            .andExpect(jsonPath("$.fctDt").value(DEFAULT_FCT_DT))
            .andExpect(jsonPath("$.parentId").value(DEFAULT_PARENT_ID))
            .andExpect(jsonPath("$.canceledAt").value(DEFAULT_CANCELED_AT.toString()))
            .andExpect(jsonPath("$.canceledId").value(DEFAULT_CANCELED_ID))
            .andExpect(jsonPath("$.productId").value(DEFAULT_PRODUCT_ID));
    }

    @Test
    @Transactional
    void getMissedTransactionOperationArchivesByIdFiltering() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        Long id = missedTransactionOperationArchive.getId();

        defaultMissedTransactionOperationArchiveShouldBeFound("id.equals=" + id);
        defaultMissedTransactionOperationArchiveShouldNotBeFound("id.notEquals=" + id);

        defaultMissedTransactionOperationArchiveShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultMissedTransactionOperationArchiveShouldNotBeFound("id.greaterThan=" + id);

        defaultMissedTransactionOperationArchiveShouldBeFound("id.lessThanOrEqual=" + id);
        defaultMissedTransactionOperationArchiveShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where amount equals to DEFAULT_AMOUNT
        defaultMissedTransactionOperationArchiveShouldBeFound("amount.equals=" + DEFAULT_AMOUNT);

        // Get all the missedTransactionOperationArchiveList where amount equals to UPDATED_AMOUNT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("amount.equals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByAmountIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where amount not equals to DEFAULT_AMOUNT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("amount.notEquals=" + DEFAULT_AMOUNT);

        // Get all the missedTransactionOperationArchiveList where amount not equals to UPDATED_AMOUNT
        defaultMissedTransactionOperationArchiveShouldBeFound("amount.notEquals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByAmountIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where amount in DEFAULT_AMOUNT or UPDATED_AMOUNT
        defaultMissedTransactionOperationArchiveShouldBeFound("amount.in=" + DEFAULT_AMOUNT + "," + UPDATED_AMOUNT);

        // Get all the missedTransactionOperationArchiveList where amount equals to UPDATED_AMOUNT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("amount.in=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where amount is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("amount.specified=true");

        // Get all the missedTransactionOperationArchiveList where amount is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("amount.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByAmountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where amount is greater than or equal to DEFAULT_AMOUNT
        defaultMissedTransactionOperationArchiveShouldBeFound("amount.greaterThanOrEqual=" + DEFAULT_AMOUNT);

        // Get all the missedTransactionOperationArchiveList where amount is greater than or equal to UPDATED_AMOUNT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("amount.greaterThanOrEqual=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByAmountIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where amount is less than or equal to DEFAULT_AMOUNT
        defaultMissedTransactionOperationArchiveShouldBeFound("amount.lessThanOrEqual=" + DEFAULT_AMOUNT);

        // Get all the missedTransactionOperationArchiveList where amount is less than or equal to SMALLER_AMOUNT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("amount.lessThanOrEqual=" + SMALLER_AMOUNT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByAmountIsLessThanSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where amount is less than DEFAULT_AMOUNT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("amount.lessThan=" + DEFAULT_AMOUNT);

        // Get all the missedTransactionOperationArchiveList where amount is less than UPDATED_AMOUNT
        defaultMissedTransactionOperationArchiveShouldBeFound("amount.lessThan=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByAmountIsGreaterThanSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where amount is greater than DEFAULT_AMOUNT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("amount.greaterThan=" + DEFAULT_AMOUNT);

        // Get all the missedTransactionOperationArchiveList where amount is greater than SMALLER_AMOUNT
        defaultMissedTransactionOperationArchiveShouldBeFound("amount.greaterThan=" + SMALLER_AMOUNT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySubsMsisdnIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where subsMsisdn equals to DEFAULT_SUBS_MSISDN
        defaultMissedTransactionOperationArchiveShouldBeFound("subsMsisdn.equals=" + DEFAULT_SUBS_MSISDN);

        // Get all the missedTransactionOperationArchiveList where subsMsisdn equals to UPDATED_SUBS_MSISDN
        defaultMissedTransactionOperationArchiveShouldNotBeFound("subsMsisdn.equals=" + UPDATED_SUBS_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySubsMsisdnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where subsMsisdn not equals to DEFAULT_SUBS_MSISDN
        defaultMissedTransactionOperationArchiveShouldNotBeFound("subsMsisdn.notEquals=" + DEFAULT_SUBS_MSISDN);

        // Get all the missedTransactionOperationArchiveList where subsMsisdn not equals to UPDATED_SUBS_MSISDN
        defaultMissedTransactionOperationArchiveShouldBeFound("subsMsisdn.notEquals=" + UPDATED_SUBS_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySubsMsisdnIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where subsMsisdn in DEFAULT_SUBS_MSISDN or UPDATED_SUBS_MSISDN
        defaultMissedTransactionOperationArchiveShouldBeFound("subsMsisdn.in=" + DEFAULT_SUBS_MSISDN + "," + UPDATED_SUBS_MSISDN);

        // Get all the missedTransactionOperationArchiveList where subsMsisdn equals to UPDATED_SUBS_MSISDN
        defaultMissedTransactionOperationArchiveShouldNotBeFound("subsMsisdn.in=" + UPDATED_SUBS_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySubsMsisdnIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where subsMsisdn is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("subsMsisdn.specified=true");

        // Get all the missedTransactionOperationArchiveList where subsMsisdn is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("subsMsisdn.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySubsMsisdnContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where subsMsisdn contains DEFAULT_SUBS_MSISDN
        defaultMissedTransactionOperationArchiveShouldBeFound("subsMsisdn.contains=" + DEFAULT_SUBS_MSISDN);

        // Get all the missedTransactionOperationArchiveList where subsMsisdn contains UPDATED_SUBS_MSISDN
        defaultMissedTransactionOperationArchiveShouldNotBeFound("subsMsisdn.contains=" + UPDATED_SUBS_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySubsMsisdnNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where subsMsisdn does not contain DEFAULT_SUBS_MSISDN
        defaultMissedTransactionOperationArchiveShouldNotBeFound("subsMsisdn.doesNotContain=" + DEFAULT_SUBS_MSISDN);

        // Get all the missedTransactionOperationArchiveList where subsMsisdn does not contain UPDATED_SUBS_MSISDN
        defaultMissedTransactionOperationArchiveShouldBeFound("subsMsisdn.doesNotContain=" + UPDATED_SUBS_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByAgentMsisdnIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where agentMsisdn equals to DEFAULT_AGENT_MSISDN
        defaultMissedTransactionOperationArchiveShouldBeFound("agentMsisdn.equals=" + DEFAULT_AGENT_MSISDN);

        // Get all the missedTransactionOperationArchiveList where agentMsisdn equals to UPDATED_AGENT_MSISDN
        defaultMissedTransactionOperationArchiveShouldNotBeFound("agentMsisdn.equals=" + UPDATED_AGENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByAgentMsisdnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where agentMsisdn not equals to DEFAULT_AGENT_MSISDN
        defaultMissedTransactionOperationArchiveShouldNotBeFound("agentMsisdn.notEquals=" + DEFAULT_AGENT_MSISDN);

        // Get all the missedTransactionOperationArchiveList where agentMsisdn not equals to UPDATED_AGENT_MSISDN
        defaultMissedTransactionOperationArchiveShouldBeFound("agentMsisdn.notEquals=" + UPDATED_AGENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByAgentMsisdnIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where agentMsisdn in DEFAULT_AGENT_MSISDN or UPDATED_AGENT_MSISDN
        defaultMissedTransactionOperationArchiveShouldBeFound("agentMsisdn.in=" + DEFAULT_AGENT_MSISDN + "," + UPDATED_AGENT_MSISDN);

        // Get all the missedTransactionOperationArchiveList where agentMsisdn equals to UPDATED_AGENT_MSISDN
        defaultMissedTransactionOperationArchiveShouldNotBeFound("agentMsisdn.in=" + UPDATED_AGENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByAgentMsisdnIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where agentMsisdn is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("agentMsisdn.specified=true");

        // Get all the missedTransactionOperationArchiveList where agentMsisdn is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("agentMsisdn.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByAgentMsisdnContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where agentMsisdn contains DEFAULT_AGENT_MSISDN
        defaultMissedTransactionOperationArchiveShouldBeFound("agentMsisdn.contains=" + DEFAULT_AGENT_MSISDN);

        // Get all the missedTransactionOperationArchiveList where agentMsisdn contains UPDATED_AGENT_MSISDN
        defaultMissedTransactionOperationArchiveShouldNotBeFound("agentMsisdn.contains=" + UPDATED_AGENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByAgentMsisdnNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where agentMsisdn does not contain DEFAULT_AGENT_MSISDN
        defaultMissedTransactionOperationArchiveShouldNotBeFound("agentMsisdn.doesNotContain=" + DEFAULT_AGENT_MSISDN);

        // Get all the missedTransactionOperationArchiveList where agentMsisdn does not contain UPDATED_AGENT_MSISDN
        defaultMissedTransactionOperationArchiveShouldBeFound("agentMsisdn.doesNotContain=" + UPDATED_AGENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTypeTransactionIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where typeTransaction equals to DEFAULT_TYPE_TRANSACTION
        defaultMissedTransactionOperationArchiveShouldBeFound("typeTransaction.equals=" + DEFAULT_TYPE_TRANSACTION);

        // Get all the missedTransactionOperationArchiveList where typeTransaction equals to UPDATED_TYPE_TRANSACTION
        defaultMissedTransactionOperationArchiveShouldNotBeFound("typeTransaction.equals=" + UPDATED_TYPE_TRANSACTION);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTypeTransactionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where typeTransaction not equals to DEFAULT_TYPE_TRANSACTION
        defaultMissedTransactionOperationArchiveShouldNotBeFound("typeTransaction.notEquals=" + DEFAULT_TYPE_TRANSACTION);

        // Get all the missedTransactionOperationArchiveList where typeTransaction not equals to UPDATED_TYPE_TRANSACTION
        defaultMissedTransactionOperationArchiveShouldBeFound("typeTransaction.notEquals=" + UPDATED_TYPE_TRANSACTION);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTypeTransactionIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where typeTransaction in DEFAULT_TYPE_TRANSACTION or UPDATED_TYPE_TRANSACTION
        defaultMissedTransactionOperationArchiveShouldBeFound(
            "typeTransaction.in=" + DEFAULT_TYPE_TRANSACTION + "," + UPDATED_TYPE_TRANSACTION
        );

        // Get all the missedTransactionOperationArchiveList where typeTransaction equals to UPDATED_TYPE_TRANSACTION
        defaultMissedTransactionOperationArchiveShouldNotBeFound("typeTransaction.in=" + UPDATED_TYPE_TRANSACTION);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTypeTransactionIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where typeTransaction is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("typeTransaction.specified=true");

        // Get all the missedTransactionOperationArchiveList where typeTransaction is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("typeTransaction.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTypeTransactionContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where typeTransaction contains DEFAULT_TYPE_TRANSACTION
        defaultMissedTransactionOperationArchiveShouldBeFound("typeTransaction.contains=" + DEFAULT_TYPE_TRANSACTION);

        // Get all the missedTransactionOperationArchiveList where typeTransaction contains UPDATED_TYPE_TRANSACTION
        defaultMissedTransactionOperationArchiveShouldNotBeFound("typeTransaction.contains=" + UPDATED_TYPE_TRANSACTION);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTypeTransactionNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where typeTransaction does not contain DEFAULT_TYPE_TRANSACTION
        defaultMissedTransactionOperationArchiveShouldNotBeFound("typeTransaction.doesNotContain=" + DEFAULT_TYPE_TRANSACTION);

        // Get all the missedTransactionOperationArchiveList where typeTransaction does not contain UPDATED_TYPE_TRANSACTION
        defaultMissedTransactionOperationArchiveShouldBeFound("typeTransaction.doesNotContain=" + UPDATED_TYPE_TRANSACTION);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCreatedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where createdAt equals to DEFAULT_CREATED_AT
        defaultMissedTransactionOperationArchiveShouldBeFound("createdAt.equals=" + DEFAULT_CREATED_AT);

        // Get all the missedTransactionOperationArchiveList where createdAt equals to UPDATED_CREATED_AT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("createdAt.equals=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCreatedAtIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where createdAt not equals to DEFAULT_CREATED_AT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("createdAt.notEquals=" + DEFAULT_CREATED_AT);

        // Get all the missedTransactionOperationArchiveList where createdAt not equals to UPDATED_CREATED_AT
        defaultMissedTransactionOperationArchiveShouldBeFound("createdAt.notEquals=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCreatedAtIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where createdAt in DEFAULT_CREATED_AT or UPDATED_CREATED_AT
        defaultMissedTransactionOperationArchiveShouldBeFound("createdAt.in=" + DEFAULT_CREATED_AT + "," + UPDATED_CREATED_AT);

        // Get all the missedTransactionOperationArchiveList where createdAt equals to UPDATED_CREATED_AT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("createdAt.in=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCreatedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where createdAt is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("createdAt.specified=true");

        // Get all the missedTransactionOperationArchiveList where createdAt is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("createdAt.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTransactionStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where transactionStatus equals to DEFAULT_TRANSACTION_STATUS
        defaultMissedTransactionOperationArchiveShouldBeFound("transactionStatus.equals=" + DEFAULT_TRANSACTION_STATUS);

        // Get all the missedTransactionOperationArchiveList where transactionStatus equals to UPDATED_TRANSACTION_STATUS
        defaultMissedTransactionOperationArchiveShouldNotBeFound("transactionStatus.equals=" + UPDATED_TRANSACTION_STATUS);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTransactionStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where transactionStatus not equals to DEFAULT_TRANSACTION_STATUS
        defaultMissedTransactionOperationArchiveShouldNotBeFound("transactionStatus.notEquals=" + DEFAULT_TRANSACTION_STATUS);

        // Get all the missedTransactionOperationArchiveList where transactionStatus not equals to UPDATED_TRANSACTION_STATUS
        defaultMissedTransactionOperationArchiveShouldBeFound("transactionStatus.notEquals=" + UPDATED_TRANSACTION_STATUS);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTransactionStatusIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where transactionStatus in DEFAULT_TRANSACTION_STATUS or UPDATED_TRANSACTION_STATUS
        defaultMissedTransactionOperationArchiveShouldBeFound(
            "transactionStatus.in=" + DEFAULT_TRANSACTION_STATUS + "," + UPDATED_TRANSACTION_STATUS
        );

        // Get all the missedTransactionOperationArchiveList where transactionStatus equals to UPDATED_TRANSACTION_STATUS
        defaultMissedTransactionOperationArchiveShouldNotBeFound("transactionStatus.in=" + UPDATED_TRANSACTION_STATUS);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTransactionStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where transactionStatus is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("transactionStatus.specified=true");

        // Get all the missedTransactionOperationArchiveList where transactionStatus is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("transactionStatus.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTransactionStatusContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where transactionStatus contains DEFAULT_TRANSACTION_STATUS
        defaultMissedTransactionOperationArchiveShouldBeFound("transactionStatus.contains=" + DEFAULT_TRANSACTION_STATUS);

        // Get all the missedTransactionOperationArchiveList where transactionStatus contains UPDATED_TRANSACTION_STATUS
        defaultMissedTransactionOperationArchiveShouldNotBeFound("transactionStatus.contains=" + UPDATED_TRANSACTION_STATUS);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTransactionStatusNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where transactionStatus does not contain DEFAULT_TRANSACTION_STATUS
        defaultMissedTransactionOperationArchiveShouldNotBeFound("transactionStatus.doesNotContain=" + DEFAULT_TRANSACTION_STATUS);

        // Get all the missedTransactionOperationArchiveList where transactionStatus does not contain UPDATED_TRANSACTION_STATUS
        defaultMissedTransactionOperationArchiveShouldBeFound("transactionStatus.doesNotContain=" + UPDATED_TRANSACTION_STATUS);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySenderZoneIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where senderZone equals to DEFAULT_SENDER_ZONE
        defaultMissedTransactionOperationArchiveShouldBeFound("senderZone.equals=" + DEFAULT_SENDER_ZONE);

        // Get all the missedTransactionOperationArchiveList where senderZone equals to UPDATED_SENDER_ZONE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("senderZone.equals=" + UPDATED_SENDER_ZONE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySenderZoneIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where senderZone not equals to DEFAULT_SENDER_ZONE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("senderZone.notEquals=" + DEFAULT_SENDER_ZONE);

        // Get all the missedTransactionOperationArchiveList where senderZone not equals to UPDATED_SENDER_ZONE
        defaultMissedTransactionOperationArchiveShouldBeFound("senderZone.notEquals=" + UPDATED_SENDER_ZONE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySenderZoneIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where senderZone in DEFAULT_SENDER_ZONE or UPDATED_SENDER_ZONE
        defaultMissedTransactionOperationArchiveShouldBeFound("senderZone.in=" + DEFAULT_SENDER_ZONE + "," + UPDATED_SENDER_ZONE);

        // Get all the missedTransactionOperationArchiveList where senderZone equals to UPDATED_SENDER_ZONE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("senderZone.in=" + UPDATED_SENDER_ZONE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySenderZoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where senderZone is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("senderZone.specified=true");

        // Get all the missedTransactionOperationArchiveList where senderZone is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("senderZone.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySenderZoneContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where senderZone contains DEFAULT_SENDER_ZONE
        defaultMissedTransactionOperationArchiveShouldBeFound("senderZone.contains=" + DEFAULT_SENDER_ZONE);

        // Get all the missedTransactionOperationArchiveList where senderZone contains UPDATED_SENDER_ZONE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("senderZone.contains=" + UPDATED_SENDER_ZONE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySenderZoneNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where senderZone does not contain DEFAULT_SENDER_ZONE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("senderZone.doesNotContain=" + DEFAULT_SENDER_ZONE);

        // Get all the missedTransactionOperationArchiveList where senderZone does not contain UPDATED_SENDER_ZONE
        defaultMissedTransactionOperationArchiveShouldBeFound("senderZone.doesNotContain=" + UPDATED_SENDER_ZONE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySenderProfileIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where senderProfile equals to DEFAULT_SENDER_PROFILE
        defaultMissedTransactionOperationArchiveShouldBeFound("senderProfile.equals=" + DEFAULT_SENDER_PROFILE);

        // Get all the missedTransactionOperationArchiveList where senderProfile equals to UPDATED_SENDER_PROFILE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("senderProfile.equals=" + UPDATED_SENDER_PROFILE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySenderProfileIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where senderProfile not equals to DEFAULT_SENDER_PROFILE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("senderProfile.notEquals=" + DEFAULT_SENDER_PROFILE);

        // Get all the missedTransactionOperationArchiveList where senderProfile not equals to UPDATED_SENDER_PROFILE
        defaultMissedTransactionOperationArchiveShouldBeFound("senderProfile.notEquals=" + UPDATED_SENDER_PROFILE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySenderProfileIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where senderProfile in DEFAULT_SENDER_PROFILE or UPDATED_SENDER_PROFILE
        defaultMissedTransactionOperationArchiveShouldBeFound("senderProfile.in=" + DEFAULT_SENDER_PROFILE + "," + UPDATED_SENDER_PROFILE);

        // Get all the missedTransactionOperationArchiveList where senderProfile equals to UPDATED_SENDER_PROFILE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("senderProfile.in=" + UPDATED_SENDER_PROFILE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySenderProfileIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where senderProfile is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("senderProfile.specified=true");

        // Get all the missedTransactionOperationArchiveList where senderProfile is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("senderProfile.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySenderProfileContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where senderProfile contains DEFAULT_SENDER_PROFILE
        defaultMissedTransactionOperationArchiveShouldBeFound("senderProfile.contains=" + DEFAULT_SENDER_PROFILE);

        // Get all the missedTransactionOperationArchiveList where senderProfile contains UPDATED_SENDER_PROFILE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("senderProfile.contains=" + UPDATED_SENDER_PROFILE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySenderProfileNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where senderProfile does not contain DEFAULT_SENDER_PROFILE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("senderProfile.doesNotContain=" + DEFAULT_SENDER_PROFILE);

        // Get all the missedTransactionOperationArchiveList where senderProfile does not contain UPDATED_SENDER_PROFILE
        defaultMissedTransactionOperationArchiveShouldBeFound("senderProfile.doesNotContain=" + UPDATED_SENDER_PROFILE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCodeTerritoryIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where codeTerritory equals to DEFAULT_CODE_TERRITORY
        defaultMissedTransactionOperationArchiveShouldBeFound("codeTerritory.equals=" + DEFAULT_CODE_TERRITORY);

        // Get all the missedTransactionOperationArchiveList where codeTerritory equals to UPDATED_CODE_TERRITORY
        defaultMissedTransactionOperationArchiveShouldNotBeFound("codeTerritory.equals=" + UPDATED_CODE_TERRITORY);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCodeTerritoryIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where codeTerritory not equals to DEFAULT_CODE_TERRITORY
        defaultMissedTransactionOperationArchiveShouldNotBeFound("codeTerritory.notEquals=" + DEFAULT_CODE_TERRITORY);

        // Get all the missedTransactionOperationArchiveList where codeTerritory not equals to UPDATED_CODE_TERRITORY
        defaultMissedTransactionOperationArchiveShouldBeFound("codeTerritory.notEquals=" + UPDATED_CODE_TERRITORY);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCodeTerritoryIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where codeTerritory in DEFAULT_CODE_TERRITORY or UPDATED_CODE_TERRITORY
        defaultMissedTransactionOperationArchiveShouldBeFound("codeTerritory.in=" + DEFAULT_CODE_TERRITORY + "," + UPDATED_CODE_TERRITORY);

        // Get all the missedTransactionOperationArchiveList where codeTerritory equals to UPDATED_CODE_TERRITORY
        defaultMissedTransactionOperationArchiveShouldNotBeFound("codeTerritory.in=" + UPDATED_CODE_TERRITORY);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCodeTerritoryIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where codeTerritory is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("codeTerritory.specified=true");

        // Get all the missedTransactionOperationArchiveList where codeTerritory is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("codeTerritory.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCodeTerritoryContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where codeTerritory contains DEFAULT_CODE_TERRITORY
        defaultMissedTransactionOperationArchiveShouldBeFound("codeTerritory.contains=" + DEFAULT_CODE_TERRITORY);

        // Get all the missedTransactionOperationArchiveList where codeTerritory contains UPDATED_CODE_TERRITORY
        defaultMissedTransactionOperationArchiveShouldNotBeFound("codeTerritory.contains=" + UPDATED_CODE_TERRITORY);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCodeTerritoryNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where codeTerritory does not contain DEFAULT_CODE_TERRITORY
        defaultMissedTransactionOperationArchiveShouldNotBeFound("codeTerritory.doesNotContain=" + DEFAULT_CODE_TERRITORY);

        // Get all the missedTransactionOperationArchiveList where codeTerritory does not contain UPDATED_CODE_TERRITORY
        defaultMissedTransactionOperationArchiveShouldBeFound("codeTerritory.doesNotContain=" + UPDATED_CODE_TERRITORY);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySubTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where subType equals to DEFAULT_SUB_TYPE
        defaultMissedTransactionOperationArchiveShouldBeFound("subType.equals=" + DEFAULT_SUB_TYPE);

        // Get all the missedTransactionOperationArchiveList where subType equals to UPDATED_SUB_TYPE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("subType.equals=" + UPDATED_SUB_TYPE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySubTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where subType not equals to DEFAULT_SUB_TYPE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("subType.notEquals=" + DEFAULT_SUB_TYPE);

        // Get all the missedTransactionOperationArchiveList where subType not equals to UPDATED_SUB_TYPE
        defaultMissedTransactionOperationArchiveShouldBeFound("subType.notEquals=" + UPDATED_SUB_TYPE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySubTypeIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where subType in DEFAULT_SUB_TYPE or UPDATED_SUB_TYPE
        defaultMissedTransactionOperationArchiveShouldBeFound("subType.in=" + DEFAULT_SUB_TYPE + "," + UPDATED_SUB_TYPE);

        // Get all the missedTransactionOperationArchiveList where subType equals to UPDATED_SUB_TYPE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("subType.in=" + UPDATED_SUB_TYPE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySubTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where subType is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("subType.specified=true");

        // Get all the missedTransactionOperationArchiveList where subType is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("subType.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySubTypeContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where subType contains DEFAULT_SUB_TYPE
        defaultMissedTransactionOperationArchiveShouldBeFound("subType.contains=" + DEFAULT_SUB_TYPE);

        // Get all the missedTransactionOperationArchiveList where subType contains UPDATED_SUB_TYPE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("subType.contains=" + UPDATED_SUB_TYPE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesBySubTypeNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where subType does not contain DEFAULT_SUB_TYPE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("subType.doesNotContain=" + DEFAULT_SUB_TYPE);

        // Get all the missedTransactionOperationArchiveList where subType does not contain UPDATED_SUB_TYPE
        defaultMissedTransactionOperationArchiveShouldBeFound("subType.doesNotContain=" + UPDATED_SUB_TYPE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByOperationDateIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where operationDate equals to DEFAULT_OPERATION_DATE
        defaultMissedTransactionOperationArchiveShouldBeFound("operationDate.equals=" + DEFAULT_OPERATION_DATE);

        // Get all the missedTransactionOperationArchiveList where operationDate equals to UPDATED_OPERATION_DATE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("operationDate.equals=" + UPDATED_OPERATION_DATE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByOperationDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where operationDate not equals to DEFAULT_OPERATION_DATE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("operationDate.notEquals=" + DEFAULT_OPERATION_DATE);

        // Get all the missedTransactionOperationArchiveList where operationDate not equals to UPDATED_OPERATION_DATE
        defaultMissedTransactionOperationArchiveShouldBeFound("operationDate.notEquals=" + UPDATED_OPERATION_DATE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByOperationDateIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where operationDate in DEFAULT_OPERATION_DATE or UPDATED_OPERATION_DATE
        defaultMissedTransactionOperationArchiveShouldBeFound("operationDate.in=" + DEFAULT_OPERATION_DATE + "," + UPDATED_OPERATION_DATE);

        // Get all the missedTransactionOperationArchiveList where operationDate equals to UPDATED_OPERATION_DATE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("operationDate.in=" + UPDATED_OPERATION_DATE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByOperationDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where operationDate is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("operationDate.specified=true");

        // Get all the missedTransactionOperationArchiveList where operationDate is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("operationDate.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByIsFraudIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where isFraud equals to DEFAULT_IS_FRAUD
        defaultMissedTransactionOperationArchiveShouldBeFound("isFraud.equals=" + DEFAULT_IS_FRAUD);

        // Get all the missedTransactionOperationArchiveList where isFraud equals to UPDATED_IS_FRAUD
        defaultMissedTransactionOperationArchiveShouldNotBeFound("isFraud.equals=" + UPDATED_IS_FRAUD);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByIsFraudIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where isFraud not equals to DEFAULT_IS_FRAUD
        defaultMissedTransactionOperationArchiveShouldNotBeFound("isFraud.notEquals=" + DEFAULT_IS_FRAUD);

        // Get all the missedTransactionOperationArchiveList where isFraud not equals to UPDATED_IS_FRAUD
        defaultMissedTransactionOperationArchiveShouldBeFound("isFraud.notEquals=" + UPDATED_IS_FRAUD);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByIsFraudIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where isFraud in DEFAULT_IS_FRAUD or UPDATED_IS_FRAUD
        defaultMissedTransactionOperationArchiveShouldBeFound("isFraud.in=" + DEFAULT_IS_FRAUD + "," + UPDATED_IS_FRAUD);

        // Get all the missedTransactionOperationArchiveList where isFraud equals to UPDATED_IS_FRAUD
        defaultMissedTransactionOperationArchiveShouldNotBeFound("isFraud.in=" + UPDATED_IS_FRAUD);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByIsFraudIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where isFraud is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("isFraud.specified=true");

        // Get all the missedTransactionOperationArchiveList where isFraud is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("isFraud.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTaggedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where taggedAt equals to DEFAULT_TAGGED_AT
        defaultMissedTransactionOperationArchiveShouldBeFound("taggedAt.equals=" + DEFAULT_TAGGED_AT);

        // Get all the missedTransactionOperationArchiveList where taggedAt equals to UPDATED_TAGGED_AT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("taggedAt.equals=" + UPDATED_TAGGED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTaggedAtIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where taggedAt not equals to DEFAULT_TAGGED_AT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("taggedAt.notEquals=" + DEFAULT_TAGGED_AT);

        // Get all the missedTransactionOperationArchiveList where taggedAt not equals to UPDATED_TAGGED_AT
        defaultMissedTransactionOperationArchiveShouldBeFound("taggedAt.notEquals=" + UPDATED_TAGGED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTaggedAtIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where taggedAt in DEFAULT_TAGGED_AT or UPDATED_TAGGED_AT
        defaultMissedTransactionOperationArchiveShouldBeFound("taggedAt.in=" + DEFAULT_TAGGED_AT + "," + UPDATED_TAGGED_AT);

        // Get all the missedTransactionOperationArchiveList where taggedAt equals to UPDATED_TAGGED_AT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("taggedAt.in=" + UPDATED_TAGGED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTaggedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where taggedAt is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("taggedAt.specified=true");

        // Get all the missedTransactionOperationArchiveList where taggedAt is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("taggedAt.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByFraudSourceIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where fraudSource equals to DEFAULT_FRAUD_SOURCE
        defaultMissedTransactionOperationArchiveShouldBeFound("fraudSource.equals=" + DEFAULT_FRAUD_SOURCE);

        // Get all the missedTransactionOperationArchiveList where fraudSource equals to UPDATED_FRAUD_SOURCE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("fraudSource.equals=" + UPDATED_FRAUD_SOURCE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByFraudSourceIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where fraudSource not equals to DEFAULT_FRAUD_SOURCE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("fraudSource.notEquals=" + DEFAULT_FRAUD_SOURCE);

        // Get all the missedTransactionOperationArchiveList where fraudSource not equals to UPDATED_FRAUD_SOURCE
        defaultMissedTransactionOperationArchiveShouldBeFound("fraudSource.notEquals=" + UPDATED_FRAUD_SOURCE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByFraudSourceIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where fraudSource in DEFAULT_FRAUD_SOURCE or UPDATED_FRAUD_SOURCE
        defaultMissedTransactionOperationArchiveShouldBeFound("fraudSource.in=" + DEFAULT_FRAUD_SOURCE + "," + UPDATED_FRAUD_SOURCE);

        // Get all the missedTransactionOperationArchiveList where fraudSource equals to UPDATED_FRAUD_SOURCE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("fraudSource.in=" + UPDATED_FRAUD_SOURCE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByFraudSourceIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where fraudSource is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("fraudSource.specified=true");

        // Get all the missedTransactionOperationArchiveList where fraudSource is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("fraudSource.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByFraudSourceContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where fraudSource contains DEFAULT_FRAUD_SOURCE
        defaultMissedTransactionOperationArchiveShouldBeFound("fraudSource.contains=" + DEFAULT_FRAUD_SOURCE);

        // Get all the missedTransactionOperationArchiveList where fraudSource contains UPDATED_FRAUD_SOURCE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("fraudSource.contains=" + UPDATED_FRAUD_SOURCE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByFraudSourceNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where fraudSource does not contain DEFAULT_FRAUD_SOURCE
        defaultMissedTransactionOperationArchiveShouldNotBeFound("fraudSource.doesNotContain=" + DEFAULT_FRAUD_SOURCE);

        // Get all the missedTransactionOperationArchiveList where fraudSource does not contain UPDATED_FRAUD_SOURCE
        defaultMissedTransactionOperationArchiveShouldBeFound("fraudSource.doesNotContain=" + UPDATED_FRAUD_SOURCE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCommentIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where comment equals to DEFAULT_COMMENT
        defaultMissedTransactionOperationArchiveShouldBeFound("comment.equals=" + DEFAULT_COMMENT);

        // Get all the missedTransactionOperationArchiveList where comment equals to UPDATED_COMMENT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("comment.equals=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCommentIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where comment not equals to DEFAULT_COMMENT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("comment.notEquals=" + DEFAULT_COMMENT);

        // Get all the missedTransactionOperationArchiveList where comment not equals to UPDATED_COMMENT
        defaultMissedTransactionOperationArchiveShouldBeFound("comment.notEquals=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCommentIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where comment in DEFAULT_COMMENT or UPDATED_COMMENT
        defaultMissedTransactionOperationArchiveShouldBeFound("comment.in=" + DEFAULT_COMMENT + "," + UPDATED_COMMENT);

        // Get all the missedTransactionOperationArchiveList where comment equals to UPDATED_COMMENT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("comment.in=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCommentIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where comment is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("comment.specified=true");

        // Get all the missedTransactionOperationArchiveList where comment is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("comment.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCommentContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where comment contains DEFAULT_COMMENT
        defaultMissedTransactionOperationArchiveShouldBeFound("comment.contains=" + DEFAULT_COMMENT);

        // Get all the missedTransactionOperationArchiveList where comment contains UPDATED_COMMENT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("comment.contains=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCommentNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where comment does not contain DEFAULT_COMMENT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("comment.doesNotContain=" + DEFAULT_COMMENT);

        // Get all the missedTransactionOperationArchiveList where comment does not contain UPDATED_COMMENT
        defaultMissedTransactionOperationArchiveShouldBeFound("comment.doesNotContain=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByFalsePositiveDetectedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where falsePositiveDetectedAt equals to DEFAULT_FALSE_POSITIVE_DETECTED_AT
        defaultMissedTransactionOperationArchiveShouldBeFound("falsePositiveDetectedAt.equals=" + DEFAULT_FALSE_POSITIVE_DETECTED_AT);

        // Get all the missedTransactionOperationArchiveList where falsePositiveDetectedAt equals to UPDATED_FALSE_POSITIVE_DETECTED_AT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("falsePositiveDetectedAt.equals=" + UPDATED_FALSE_POSITIVE_DETECTED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByFalsePositiveDetectedAtIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where falsePositiveDetectedAt not equals to DEFAULT_FALSE_POSITIVE_DETECTED_AT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("falsePositiveDetectedAt.notEquals=" + DEFAULT_FALSE_POSITIVE_DETECTED_AT);

        // Get all the missedTransactionOperationArchiveList where falsePositiveDetectedAt not equals to UPDATED_FALSE_POSITIVE_DETECTED_AT
        defaultMissedTransactionOperationArchiveShouldBeFound("falsePositiveDetectedAt.notEquals=" + UPDATED_FALSE_POSITIVE_DETECTED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByFalsePositiveDetectedAtIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where falsePositiveDetectedAt in DEFAULT_FALSE_POSITIVE_DETECTED_AT or UPDATED_FALSE_POSITIVE_DETECTED_AT
        defaultMissedTransactionOperationArchiveShouldBeFound(
            "falsePositiveDetectedAt.in=" + DEFAULT_FALSE_POSITIVE_DETECTED_AT + "," + UPDATED_FALSE_POSITIVE_DETECTED_AT
        );

        // Get all the missedTransactionOperationArchiveList where falsePositiveDetectedAt equals to UPDATED_FALSE_POSITIVE_DETECTED_AT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("falsePositiveDetectedAt.in=" + UPDATED_FALSE_POSITIVE_DETECTED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByFalsePositiveDetectedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where falsePositiveDetectedAt is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("falsePositiveDetectedAt.specified=true");

        // Get all the missedTransactionOperationArchiveList where falsePositiveDetectedAt is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("falsePositiveDetectedAt.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTidIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where tid equals to DEFAULT_TID
        defaultMissedTransactionOperationArchiveShouldBeFound("tid.equals=" + DEFAULT_TID);

        // Get all the missedTransactionOperationArchiveList where tid equals to UPDATED_TID
        defaultMissedTransactionOperationArchiveShouldNotBeFound("tid.equals=" + UPDATED_TID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTidIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where tid not equals to DEFAULT_TID
        defaultMissedTransactionOperationArchiveShouldNotBeFound("tid.notEquals=" + DEFAULT_TID);

        // Get all the missedTransactionOperationArchiveList where tid not equals to UPDATED_TID
        defaultMissedTransactionOperationArchiveShouldBeFound("tid.notEquals=" + UPDATED_TID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTidIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where tid in DEFAULT_TID or UPDATED_TID
        defaultMissedTransactionOperationArchiveShouldBeFound("tid.in=" + DEFAULT_TID + "," + UPDATED_TID);

        // Get all the missedTransactionOperationArchiveList where tid equals to UPDATED_TID
        defaultMissedTransactionOperationArchiveShouldNotBeFound("tid.in=" + UPDATED_TID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTidIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where tid is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("tid.specified=true");

        // Get all the missedTransactionOperationArchiveList where tid is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("tid.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTidContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where tid contains DEFAULT_TID
        defaultMissedTransactionOperationArchiveShouldBeFound("tid.contains=" + DEFAULT_TID);

        // Get all the missedTransactionOperationArchiveList where tid contains UPDATED_TID
        defaultMissedTransactionOperationArchiveShouldNotBeFound("tid.contains=" + UPDATED_TID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByTidNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where tid does not contain DEFAULT_TID
        defaultMissedTransactionOperationArchiveShouldNotBeFound("tid.doesNotContain=" + DEFAULT_TID);

        // Get all the missedTransactionOperationArchiveList where tid does not contain UPDATED_TID
        defaultMissedTransactionOperationArchiveShouldBeFound("tid.doesNotContain=" + UPDATED_TID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByParentMsisdnIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where parentMsisdn equals to DEFAULT_PARENT_MSISDN
        defaultMissedTransactionOperationArchiveShouldBeFound("parentMsisdn.equals=" + DEFAULT_PARENT_MSISDN);

        // Get all the missedTransactionOperationArchiveList where parentMsisdn equals to UPDATED_PARENT_MSISDN
        defaultMissedTransactionOperationArchiveShouldNotBeFound("parentMsisdn.equals=" + UPDATED_PARENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByParentMsisdnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where parentMsisdn not equals to DEFAULT_PARENT_MSISDN
        defaultMissedTransactionOperationArchiveShouldNotBeFound("parentMsisdn.notEquals=" + DEFAULT_PARENT_MSISDN);

        // Get all the missedTransactionOperationArchiveList where parentMsisdn not equals to UPDATED_PARENT_MSISDN
        defaultMissedTransactionOperationArchiveShouldBeFound("parentMsisdn.notEquals=" + UPDATED_PARENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByParentMsisdnIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where parentMsisdn in DEFAULT_PARENT_MSISDN or UPDATED_PARENT_MSISDN
        defaultMissedTransactionOperationArchiveShouldBeFound("parentMsisdn.in=" + DEFAULT_PARENT_MSISDN + "," + UPDATED_PARENT_MSISDN);

        // Get all the missedTransactionOperationArchiveList where parentMsisdn equals to UPDATED_PARENT_MSISDN
        defaultMissedTransactionOperationArchiveShouldNotBeFound("parentMsisdn.in=" + UPDATED_PARENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByParentMsisdnIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where parentMsisdn is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("parentMsisdn.specified=true");

        // Get all the missedTransactionOperationArchiveList where parentMsisdn is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("parentMsisdn.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByParentMsisdnContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where parentMsisdn contains DEFAULT_PARENT_MSISDN
        defaultMissedTransactionOperationArchiveShouldBeFound("parentMsisdn.contains=" + DEFAULT_PARENT_MSISDN);

        // Get all the missedTransactionOperationArchiveList where parentMsisdn contains UPDATED_PARENT_MSISDN
        defaultMissedTransactionOperationArchiveShouldNotBeFound("parentMsisdn.contains=" + UPDATED_PARENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByParentMsisdnNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where parentMsisdn does not contain DEFAULT_PARENT_MSISDN
        defaultMissedTransactionOperationArchiveShouldNotBeFound("parentMsisdn.doesNotContain=" + DEFAULT_PARENT_MSISDN);

        // Get all the missedTransactionOperationArchiveList where parentMsisdn does not contain UPDATED_PARENT_MSISDN
        defaultMissedTransactionOperationArchiveShouldBeFound("parentMsisdn.doesNotContain=" + UPDATED_PARENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByFctDtIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where fctDt equals to DEFAULT_FCT_DT
        defaultMissedTransactionOperationArchiveShouldBeFound("fctDt.equals=" + DEFAULT_FCT_DT);

        // Get all the missedTransactionOperationArchiveList where fctDt equals to UPDATED_FCT_DT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("fctDt.equals=" + UPDATED_FCT_DT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByFctDtIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where fctDt not equals to DEFAULT_FCT_DT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("fctDt.notEquals=" + DEFAULT_FCT_DT);

        // Get all the missedTransactionOperationArchiveList where fctDt not equals to UPDATED_FCT_DT
        defaultMissedTransactionOperationArchiveShouldBeFound("fctDt.notEquals=" + UPDATED_FCT_DT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByFctDtIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where fctDt in DEFAULT_FCT_DT or UPDATED_FCT_DT
        defaultMissedTransactionOperationArchiveShouldBeFound("fctDt.in=" + DEFAULT_FCT_DT + "," + UPDATED_FCT_DT);

        // Get all the missedTransactionOperationArchiveList where fctDt equals to UPDATED_FCT_DT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("fctDt.in=" + UPDATED_FCT_DT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByFctDtIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where fctDt is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("fctDt.specified=true");

        // Get all the missedTransactionOperationArchiveList where fctDt is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("fctDt.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByFctDtContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where fctDt contains DEFAULT_FCT_DT
        defaultMissedTransactionOperationArchiveShouldBeFound("fctDt.contains=" + DEFAULT_FCT_DT);

        // Get all the missedTransactionOperationArchiveList where fctDt contains UPDATED_FCT_DT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("fctDt.contains=" + UPDATED_FCT_DT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByFctDtNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where fctDt does not contain DEFAULT_FCT_DT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("fctDt.doesNotContain=" + DEFAULT_FCT_DT);

        // Get all the missedTransactionOperationArchiveList where fctDt does not contain UPDATED_FCT_DT
        defaultMissedTransactionOperationArchiveShouldBeFound("fctDt.doesNotContain=" + UPDATED_FCT_DT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByParentIdIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where parentId equals to DEFAULT_PARENT_ID
        defaultMissedTransactionOperationArchiveShouldBeFound("parentId.equals=" + DEFAULT_PARENT_ID);

        // Get all the missedTransactionOperationArchiveList where parentId equals to UPDATED_PARENT_ID
        defaultMissedTransactionOperationArchiveShouldNotBeFound("parentId.equals=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByParentIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where parentId not equals to DEFAULT_PARENT_ID
        defaultMissedTransactionOperationArchiveShouldNotBeFound("parentId.notEquals=" + DEFAULT_PARENT_ID);

        // Get all the missedTransactionOperationArchiveList where parentId not equals to UPDATED_PARENT_ID
        defaultMissedTransactionOperationArchiveShouldBeFound("parentId.notEquals=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByParentIdIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where parentId in DEFAULT_PARENT_ID or UPDATED_PARENT_ID
        defaultMissedTransactionOperationArchiveShouldBeFound("parentId.in=" + DEFAULT_PARENT_ID + "," + UPDATED_PARENT_ID);

        // Get all the missedTransactionOperationArchiveList where parentId equals to UPDATED_PARENT_ID
        defaultMissedTransactionOperationArchiveShouldNotBeFound("parentId.in=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByParentIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where parentId is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("parentId.specified=true");

        // Get all the missedTransactionOperationArchiveList where parentId is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("parentId.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByParentIdContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where parentId contains DEFAULT_PARENT_ID
        defaultMissedTransactionOperationArchiveShouldBeFound("parentId.contains=" + DEFAULT_PARENT_ID);

        // Get all the missedTransactionOperationArchiveList where parentId contains UPDATED_PARENT_ID
        defaultMissedTransactionOperationArchiveShouldNotBeFound("parentId.contains=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByParentIdNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where parentId does not contain DEFAULT_PARENT_ID
        defaultMissedTransactionOperationArchiveShouldNotBeFound("parentId.doesNotContain=" + DEFAULT_PARENT_ID);

        // Get all the missedTransactionOperationArchiveList where parentId does not contain UPDATED_PARENT_ID
        defaultMissedTransactionOperationArchiveShouldBeFound("parentId.doesNotContain=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCanceledAtIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where canceledAt equals to DEFAULT_CANCELED_AT
        defaultMissedTransactionOperationArchiveShouldBeFound("canceledAt.equals=" + DEFAULT_CANCELED_AT);

        // Get all the missedTransactionOperationArchiveList where canceledAt equals to UPDATED_CANCELED_AT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("canceledAt.equals=" + UPDATED_CANCELED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCanceledAtIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where canceledAt not equals to DEFAULT_CANCELED_AT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("canceledAt.notEquals=" + DEFAULT_CANCELED_AT);

        // Get all the missedTransactionOperationArchiveList where canceledAt not equals to UPDATED_CANCELED_AT
        defaultMissedTransactionOperationArchiveShouldBeFound("canceledAt.notEquals=" + UPDATED_CANCELED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCanceledAtIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where canceledAt in DEFAULT_CANCELED_AT or UPDATED_CANCELED_AT
        defaultMissedTransactionOperationArchiveShouldBeFound("canceledAt.in=" + DEFAULT_CANCELED_AT + "," + UPDATED_CANCELED_AT);

        // Get all the missedTransactionOperationArchiveList where canceledAt equals to UPDATED_CANCELED_AT
        defaultMissedTransactionOperationArchiveShouldNotBeFound("canceledAt.in=" + UPDATED_CANCELED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCanceledAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where canceledAt is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("canceledAt.specified=true");

        // Get all the missedTransactionOperationArchiveList where canceledAt is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("canceledAt.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCanceledIdIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where canceledId equals to DEFAULT_CANCELED_ID
        defaultMissedTransactionOperationArchiveShouldBeFound("canceledId.equals=" + DEFAULT_CANCELED_ID);

        // Get all the missedTransactionOperationArchiveList where canceledId equals to UPDATED_CANCELED_ID
        defaultMissedTransactionOperationArchiveShouldNotBeFound("canceledId.equals=" + UPDATED_CANCELED_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCanceledIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where canceledId not equals to DEFAULT_CANCELED_ID
        defaultMissedTransactionOperationArchiveShouldNotBeFound("canceledId.notEquals=" + DEFAULT_CANCELED_ID);

        // Get all the missedTransactionOperationArchiveList where canceledId not equals to UPDATED_CANCELED_ID
        defaultMissedTransactionOperationArchiveShouldBeFound("canceledId.notEquals=" + UPDATED_CANCELED_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCanceledIdIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where canceledId in DEFAULT_CANCELED_ID or UPDATED_CANCELED_ID
        defaultMissedTransactionOperationArchiveShouldBeFound("canceledId.in=" + DEFAULT_CANCELED_ID + "," + UPDATED_CANCELED_ID);

        // Get all the missedTransactionOperationArchiveList where canceledId equals to UPDATED_CANCELED_ID
        defaultMissedTransactionOperationArchiveShouldNotBeFound("canceledId.in=" + UPDATED_CANCELED_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCanceledIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where canceledId is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("canceledId.specified=true");

        // Get all the missedTransactionOperationArchiveList where canceledId is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("canceledId.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCanceledIdContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where canceledId contains DEFAULT_CANCELED_ID
        defaultMissedTransactionOperationArchiveShouldBeFound("canceledId.contains=" + DEFAULT_CANCELED_ID);

        // Get all the missedTransactionOperationArchiveList where canceledId contains UPDATED_CANCELED_ID
        defaultMissedTransactionOperationArchiveShouldNotBeFound("canceledId.contains=" + UPDATED_CANCELED_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByCanceledIdNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where canceledId does not contain DEFAULT_CANCELED_ID
        defaultMissedTransactionOperationArchiveShouldNotBeFound("canceledId.doesNotContain=" + DEFAULT_CANCELED_ID);

        // Get all the missedTransactionOperationArchiveList where canceledId does not contain UPDATED_CANCELED_ID
        defaultMissedTransactionOperationArchiveShouldBeFound("canceledId.doesNotContain=" + UPDATED_CANCELED_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByProductIdIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where productId equals to DEFAULT_PRODUCT_ID
        defaultMissedTransactionOperationArchiveShouldBeFound("productId.equals=" + DEFAULT_PRODUCT_ID);

        // Get all the missedTransactionOperationArchiveList where productId equals to UPDATED_PRODUCT_ID
        defaultMissedTransactionOperationArchiveShouldNotBeFound("productId.equals=" + UPDATED_PRODUCT_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByProductIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where productId not equals to DEFAULT_PRODUCT_ID
        defaultMissedTransactionOperationArchiveShouldNotBeFound("productId.notEquals=" + DEFAULT_PRODUCT_ID);

        // Get all the missedTransactionOperationArchiveList where productId not equals to UPDATED_PRODUCT_ID
        defaultMissedTransactionOperationArchiveShouldBeFound("productId.notEquals=" + UPDATED_PRODUCT_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByProductIdIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where productId in DEFAULT_PRODUCT_ID or UPDATED_PRODUCT_ID
        defaultMissedTransactionOperationArchiveShouldBeFound("productId.in=" + DEFAULT_PRODUCT_ID + "," + UPDATED_PRODUCT_ID);

        // Get all the missedTransactionOperationArchiveList where productId equals to UPDATED_PRODUCT_ID
        defaultMissedTransactionOperationArchiveShouldNotBeFound("productId.in=" + UPDATED_PRODUCT_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByProductIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where productId is not null
        defaultMissedTransactionOperationArchiveShouldBeFound("productId.specified=true");

        // Get all the missedTransactionOperationArchiveList where productId is null
        defaultMissedTransactionOperationArchiveShouldNotBeFound("productId.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByProductIdContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where productId contains DEFAULT_PRODUCT_ID
        defaultMissedTransactionOperationArchiveShouldBeFound("productId.contains=" + DEFAULT_PRODUCT_ID);

        // Get all the missedTransactionOperationArchiveList where productId contains UPDATED_PRODUCT_ID
        defaultMissedTransactionOperationArchiveShouldNotBeFound("productId.contains=" + UPDATED_PRODUCT_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationArchivesByProductIdNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        // Get all the missedTransactionOperationArchiveList where productId does not contain DEFAULT_PRODUCT_ID
        defaultMissedTransactionOperationArchiveShouldNotBeFound("productId.doesNotContain=" + DEFAULT_PRODUCT_ID);

        // Get all the missedTransactionOperationArchiveList where productId does not contain UPDATED_PRODUCT_ID
        defaultMissedTransactionOperationArchiveShouldBeFound("productId.doesNotContain=" + UPDATED_PRODUCT_ID);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMissedTransactionOperationArchiveShouldBeFound(String filter) throws Exception {
        restMissedTransactionOperationArchiveMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(missedTransactionOperationArchive.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].subsMsisdn").value(hasItem(DEFAULT_SUBS_MSISDN)))
            .andExpect(jsonPath("$.[*].agentMsisdn").value(hasItem(DEFAULT_AGENT_MSISDN)))
            .andExpect(jsonPath("$.[*].typeTransaction").value(hasItem(DEFAULT_TYPE_TRANSACTION)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].transactionStatus").value(hasItem(DEFAULT_TRANSACTION_STATUS)))
            .andExpect(jsonPath("$.[*].senderZone").value(hasItem(DEFAULT_SENDER_ZONE)))
            .andExpect(jsonPath("$.[*].senderProfile").value(hasItem(DEFAULT_SENDER_PROFILE)))
            .andExpect(jsonPath("$.[*].codeTerritory").value(hasItem(DEFAULT_CODE_TERRITORY)))
            .andExpect(jsonPath("$.[*].subType").value(hasItem(DEFAULT_SUB_TYPE)))
            .andExpect(jsonPath("$.[*].operationDate").value(hasItem(DEFAULT_OPERATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].isFraud").value(hasItem(DEFAULT_IS_FRAUD.booleanValue())))
            .andExpect(jsonPath("$.[*].taggedAt").value(hasItem(DEFAULT_TAGGED_AT.toString())))
            .andExpect(jsonPath("$.[*].fraudSource").value(hasItem(DEFAULT_FRAUD_SOURCE)))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT)))
            .andExpect(jsonPath("$.[*].falsePositiveDetectedAt").value(hasItem(DEFAULT_FALSE_POSITIVE_DETECTED_AT.toString())))
            .andExpect(jsonPath("$.[*].tid").value(hasItem(DEFAULT_TID)))
            .andExpect(jsonPath("$.[*].parentMsisdn").value(hasItem(DEFAULT_PARENT_MSISDN)))
            .andExpect(jsonPath("$.[*].fctDt").value(hasItem(DEFAULT_FCT_DT)))
            .andExpect(jsonPath("$.[*].parentId").value(hasItem(DEFAULT_PARENT_ID)))
            .andExpect(jsonPath("$.[*].canceledAt").value(hasItem(DEFAULT_CANCELED_AT.toString())))
            .andExpect(jsonPath("$.[*].canceledId").value(hasItem(DEFAULT_CANCELED_ID)))
            .andExpect(jsonPath("$.[*].productId").value(hasItem(DEFAULT_PRODUCT_ID)));

        // Check, that the count call also returns 1
        restMissedTransactionOperationArchiveMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMissedTransactionOperationArchiveShouldNotBeFound(String filter) throws Exception {
        restMissedTransactionOperationArchiveMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMissedTransactionOperationArchiveMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingMissedTransactionOperationArchive() throws Exception {
        // Get the missedTransactionOperationArchive
        restMissedTransactionOperationArchiveMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMissedTransactionOperationArchive() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        int databaseSizeBeforeUpdate = missedTransactionOperationArchiveRepository.findAll().size();

        // Update the missedTransactionOperationArchive
        MissedTransactionOperationArchive updatedMissedTransactionOperationArchive = missedTransactionOperationArchiveRepository
            .findById(missedTransactionOperationArchive.getId())
            .get();
        // Disconnect from session so that the updates on updatedMissedTransactionOperationArchive are not directly saved in db
        em.detach(updatedMissedTransactionOperationArchive);
        updatedMissedTransactionOperationArchive
            .amount(UPDATED_AMOUNT)
            .subsMsisdn(UPDATED_SUBS_MSISDN)
            .agentMsisdn(UPDATED_AGENT_MSISDN)
            .typeTransaction(UPDATED_TYPE_TRANSACTION)
            .createdAt(UPDATED_CREATED_AT)
            .transactionStatus(UPDATED_TRANSACTION_STATUS)
            .senderZone(UPDATED_SENDER_ZONE)
            .senderProfile(UPDATED_SENDER_PROFILE)
            .codeTerritory(UPDATED_CODE_TERRITORY)
            .subType(UPDATED_SUB_TYPE)
            .operationDate(UPDATED_OPERATION_DATE)
            .isFraud(UPDATED_IS_FRAUD)
            .taggedAt(UPDATED_TAGGED_AT)
            .fraudSource(UPDATED_FRAUD_SOURCE)
            .comment(UPDATED_COMMENT)
            .falsePositiveDetectedAt(UPDATED_FALSE_POSITIVE_DETECTED_AT)
            .tid(UPDATED_TID)
            .parentMsisdn(UPDATED_PARENT_MSISDN)
            .fctDt(UPDATED_FCT_DT)
            .parentId(UPDATED_PARENT_ID)
            .canceledAt(UPDATED_CANCELED_AT)
            .canceledId(UPDATED_CANCELED_ID)
            .productId(UPDATED_PRODUCT_ID);
        MissedTransactionOperationArchiveDTO missedTransactionOperationArchiveDTO = missedTransactionOperationArchiveMapper.toDto(
            updatedMissedTransactionOperationArchive
        );

        restMissedTransactionOperationArchiveMockMvc
            .perform(
                put(ENTITY_API_URL_ID, missedTransactionOperationArchiveDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(missedTransactionOperationArchiveDTO))
            )
            .andExpect(status().isOk());

        // Validate the MissedTransactionOperationArchive in the database
        List<MissedTransactionOperationArchive> missedTransactionOperationArchiveList = missedTransactionOperationArchiveRepository.findAll();
        assertThat(missedTransactionOperationArchiveList).hasSize(databaseSizeBeforeUpdate);
        MissedTransactionOperationArchive testMissedTransactionOperationArchive = missedTransactionOperationArchiveList.get(
            missedTransactionOperationArchiveList.size() - 1
        );
        assertThat(testMissedTransactionOperationArchive.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testMissedTransactionOperationArchive.getSubsMsisdn()).isEqualTo(UPDATED_SUBS_MSISDN);
        assertThat(testMissedTransactionOperationArchive.getAgentMsisdn()).isEqualTo(UPDATED_AGENT_MSISDN);
        assertThat(testMissedTransactionOperationArchive.getTypeTransaction()).isEqualTo(UPDATED_TYPE_TRANSACTION);
        assertThat(testMissedTransactionOperationArchive.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testMissedTransactionOperationArchive.getTransactionStatus()).isEqualTo(UPDATED_TRANSACTION_STATUS);
        assertThat(testMissedTransactionOperationArchive.getSenderZone()).isEqualTo(UPDATED_SENDER_ZONE);
        assertThat(testMissedTransactionOperationArchive.getSenderProfile()).isEqualTo(UPDATED_SENDER_PROFILE);
        assertThat(testMissedTransactionOperationArchive.getCodeTerritory()).isEqualTo(UPDATED_CODE_TERRITORY);
        assertThat(testMissedTransactionOperationArchive.getSubType()).isEqualTo(UPDATED_SUB_TYPE);
        assertThat(testMissedTransactionOperationArchive.getOperationDate()).isEqualTo(UPDATED_OPERATION_DATE);
        assertThat(testMissedTransactionOperationArchive.getIsFraud()).isEqualTo(UPDATED_IS_FRAUD);
        assertThat(testMissedTransactionOperationArchive.getTaggedAt()).isEqualTo(UPDATED_TAGGED_AT);
        assertThat(testMissedTransactionOperationArchive.getFraudSource()).isEqualTo(UPDATED_FRAUD_SOURCE);
        assertThat(testMissedTransactionOperationArchive.getComment()).isEqualTo(UPDATED_COMMENT);
        assertThat(testMissedTransactionOperationArchive.getFalsePositiveDetectedAt()).isEqualTo(UPDATED_FALSE_POSITIVE_DETECTED_AT);
        assertThat(testMissedTransactionOperationArchive.getTid()).isEqualTo(UPDATED_TID);
        assertThat(testMissedTransactionOperationArchive.getParentMsisdn()).isEqualTo(UPDATED_PARENT_MSISDN);
        assertThat(testMissedTransactionOperationArchive.getFctDt()).isEqualTo(UPDATED_FCT_DT);
        assertThat(testMissedTransactionOperationArchive.getParentId()).isEqualTo(UPDATED_PARENT_ID);
        assertThat(testMissedTransactionOperationArchive.getCanceledAt()).isEqualTo(UPDATED_CANCELED_AT);
        assertThat(testMissedTransactionOperationArchive.getCanceledId()).isEqualTo(UPDATED_CANCELED_ID);
        assertThat(testMissedTransactionOperationArchive.getProductId()).isEqualTo(UPDATED_PRODUCT_ID);
    }

    @Test
    @Transactional
    void putNonExistingMissedTransactionOperationArchive() throws Exception {
        int databaseSizeBeforeUpdate = missedTransactionOperationArchiveRepository.findAll().size();
        missedTransactionOperationArchive.setId(count.incrementAndGet());

        // Create the MissedTransactionOperationArchive
        MissedTransactionOperationArchiveDTO missedTransactionOperationArchiveDTO = missedTransactionOperationArchiveMapper.toDto(
            missedTransactionOperationArchive
        );

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMissedTransactionOperationArchiveMockMvc
            .perform(
                put(ENTITY_API_URL_ID, missedTransactionOperationArchiveDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(missedTransactionOperationArchiveDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MissedTransactionOperationArchive in the database
        List<MissedTransactionOperationArchive> missedTransactionOperationArchiveList = missedTransactionOperationArchiveRepository.findAll();
        assertThat(missedTransactionOperationArchiveList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMissedTransactionOperationArchive() throws Exception {
        int databaseSizeBeforeUpdate = missedTransactionOperationArchiveRepository.findAll().size();
        missedTransactionOperationArchive.setId(count.incrementAndGet());

        // Create the MissedTransactionOperationArchive
        MissedTransactionOperationArchiveDTO missedTransactionOperationArchiveDTO = missedTransactionOperationArchiveMapper.toDto(
            missedTransactionOperationArchive
        );

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMissedTransactionOperationArchiveMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(missedTransactionOperationArchiveDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MissedTransactionOperationArchive in the database
        List<MissedTransactionOperationArchive> missedTransactionOperationArchiveList = missedTransactionOperationArchiveRepository.findAll();
        assertThat(missedTransactionOperationArchiveList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMissedTransactionOperationArchive() throws Exception {
        int databaseSizeBeforeUpdate = missedTransactionOperationArchiveRepository.findAll().size();
        missedTransactionOperationArchive.setId(count.incrementAndGet());

        // Create the MissedTransactionOperationArchive
        MissedTransactionOperationArchiveDTO missedTransactionOperationArchiveDTO = missedTransactionOperationArchiveMapper.toDto(
            missedTransactionOperationArchive
        );

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMissedTransactionOperationArchiveMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(missedTransactionOperationArchiveDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MissedTransactionOperationArchive in the database
        List<MissedTransactionOperationArchive> missedTransactionOperationArchiveList = missedTransactionOperationArchiveRepository.findAll();
        assertThat(missedTransactionOperationArchiveList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMissedTransactionOperationArchiveWithPatch() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        int databaseSizeBeforeUpdate = missedTransactionOperationArchiveRepository.findAll().size();

        // Update the missedTransactionOperationArchive using partial update
        MissedTransactionOperationArchive partialUpdatedMissedTransactionOperationArchive = new MissedTransactionOperationArchive();
        partialUpdatedMissedTransactionOperationArchive.setId(missedTransactionOperationArchive.getId());

        partialUpdatedMissedTransactionOperationArchive
            .amount(UPDATED_AMOUNT)
            .agentMsisdn(UPDATED_AGENT_MSISDN)
            .typeTransaction(UPDATED_TYPE_TRANSACTION)
            .transactionStatus(UPDATED_TRANSACTION_STATUS)
            .subType(UPDATED_SUB_TYPE)
            .taggedAt(UPDATED_TAGGED_AT)
            .fraudSource(UPDATED_FRAUD_SOURCE)
            .comment(UPDATED_COMMENT)
            .falsePositiveDetectedAt(UPDATED_FALSE_POSITIVE_DETECTED_AT)
            .tid(UPDATED_TID)
            .parentId(UPDATED_PARENT_ID)
            .canceledAt(UPDATED_CANCELED_AT);

        restMissedTransactionOperationArchiveMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMissedTransactionOperationArchive.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMissedTransactionOperationArchive))
            )
            .andExpect(status().isOk());

        // Validate the MissedTransactionOperationArchive in the database
        List<MissedTransactionOperationArchive> missedTransactionOperationArchiveList = missedTransactionOperationArchiveRepository.findAll();
        assertThat(missedTransactionOperationArchiveList).hasSize(databaseSizeBeforeUpdate);
        MissedTransactionOperationArchive testMissedTransactionOperationArchive = missedTransactionOperationArchiveList.get(
            missedTransactionOperationArchiveList.size() - 1
        );
        assertThat(testMissedTransactionOperationArchive.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testMissedTransactionOperationArchive.getSubsMsisdn()).isEqualTo(DEFAULT_SUBS_MSISDN);
        assertThat(testMissedTransactionOperationArchive.getAgentMsisdn()).isEqualTo(UPDATED_AGENT_MSISDN);
        assertThat(testMissedTransactionOperationArchive.getTypeTransaction()).isEqualTo(UPDATED_TYPE_TRANSACTION);
        assertThat(testMissedTransactionOperationArchive.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testMissedTransactionOperationArchive.getTransactionStatus()).isEqualTo(UPDATED_TRANSACTION_STATUS);
        assertThat(testMissedTransactionOperationArchive.getSenderZone()).isEqualTo(DEFAULT_SENDER_ZONE);
        assertThat(testMissedTransactionOperationArchive.getSenderProfile()).isEqualTo(DEFAULT_SENDER_PROFILE);
        assertThat(testMissedTransactionOperationArchive.getCodeTerritory()).isEqualTo(DEFAULT_CODE_TERRITORY);
        assertThat(testMissedTransactionOperationArchive.getSubType()).isEqualTo(UPDATED_SUB_TYPE);
        assertThat(testMissedTransactionOperationArchive.getOperationDate()).isEqualTo(DEFAULT_OPERATION_DATE);
        assertThat(testMissedTransactionOperationArchive.getIsFraud()).isEqualTo(DEFAULT_IS_FRAUD);
        assertThat(testMissedTransactionOperationArchive.getTaggedAt()).isEqualTo(UPDATED_TAGGED_AT);
        assertThat(testMissedTransactionOperationArchive.getFraudSource()).isEqualTo(UPDATED_FRAUD_SOURCE);
        assertThat(testMissedTransactionOperationArchive.getComment()).isEqualTo(UPDATED_COMMENT);
        assertThat(testMissedTransactionOperationArchive.getFalsePositiveDetectedAt()).isEqualTo(UPDATED_FALSE_POSITIVE_DETECTED_AT);
        assertThat(testMissedTransactionOperationArchive.getTid()).isEqualTo(UPDATED_TID);
        assertThat(testMissedTransactionOperationArchive.getParentMsisdn()).isEqualTo(DEFAULT_PARENT_MSISDN);
        assertThat(testMissedTransactionOperationArchive.getFctDt()).isEqualTo(DEFAULT_FCT_DT);
        assertThat(testMissedTransactionOperationArchive.getParentId()).isEqualTo(UPDATED_PARENT_ID);
        assertThat(testMissedTransactionOperationArchive.getCanceledAt()).isEqualTo(UPDATED_CANCELED_AT);
        assertThat(testMissedTransactionOperationArchive.getCanceledId()).isEqualTo(DEFAULT_CANCELED_ID);
        assertThat(testMissedTransactionOperationArchive.getProductId()).isEqualTo(DEFAULT_PRODUCT_ID);
    }

    @Test
    @Transactional
    void fullUpdateMissedTransactionOperationArchiveWithPatch() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        int databaseSizeBeforeUpdate = missedTransactionOperationArchiveRepository.findAll().size();

        // Update the missedTransactionOperationArchive using partial update
        MissedTransactionOperationArchive partialUpdatedMissedTransactionOperationArchive = new MissedTransactionOperationArchive();
        partialUpdatedMissedTransactionOperationArchive.setId(missedTransactionOperationArchive.getId());

        partialUpdatedMissedTransactionOperationArchive
            .amount(UPDATED_AMOUNT)
            .subsMsisdn(UPDATED_SUBS_MSISDN)
            .agentMsisdn(UPDATED_AGENT_MSISDN)
            .typeTransaction(UPDATED_TYPE_TRANSACTION)
            .createdAt(UPDATED_CREATED_AT)
            .transactionStatus(UPDATED_TRANSACTION_STATUS)
            .senderZone(UPDATED_SENDER_ZONE)
            .senderProfile(UPDATED_SENDER_PROFILE)
            .codeTerritory(UPDATED_CODE_TERRITORY)
            .subType(UPDATED_SUB_TYPE)
            .operationDate(UPDATED_OPERATION_DATE)
            .isFraud(UPDATED_IS_FRAUD)
            .taggedAt(UPDATED_TAGGED_AT)
            .fraudSource(UPDATED_FRAUD_SOURCE)
            .comment(UPDATED_COMMENT)
            .falsePositiveDetectedAt(UPDATED_FALSE_POSITIVE_DETECTED_AT)
            .tid(UPDATED_TID)
            .parentMsisdn(UPDATED_PARENT_MSISDN)
            .fctDt(UPDATED_FCT_DT)
            .parentId(UPDATED_PARENT_ID)
            .canceledAt(UPDATED_CANCELED_AT)
            .canceledId(UPDATED_CANCELED_ID)
            .productId(UPDATED_PRODUCT_ID);

        restMissedTransactionOperationArchiveMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMissedTransactionOperationArchive.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMissedTransactionOperationArchive))
            )
            .andExpect(status().isOk());

        // Validate the MissedTransactionOperationArchive in the database
        List<MissedTransactionOperationArchive> missedTransactionOperationArchiveList = missedTransactionOperationArchiveRepository.findAll();
        assertThat(missedTransactionOperationArchiveList).hasSize(databaseSizeBeforeUpdate);
        MissedTransactionOperationArchive testMissedTransactionOperationArchive = missedTransactionOperationArchiveList.get(
            missedTransactionOperationArchiveList.size() - 1
        );
        assertThat(testMissedTransactionOperationArchive.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testMissedTransactionOperationArchive.getSubsMsisdn()).isEqualTo(UPDATED_SUBS_MSISDN);
        assertThat(testMissedTransactionOperationArchive.getAgentMsisdn()).isEqualTo(UPDATED_AGENT_MSISDN);
        assertThat(testMissedTransactionOperationArchive.getTypeTransaction()).isEqualTo(UPDATED_TYPE_TRANSACTION);
        assertThat(testMissedTransactionOperationArchive.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testMissedTransactionOperationArchive.getTransactionStatus()).isEqualTo(UPDATED_TRANSACTION_STATUS);
        assertThat(testMissedTransactionOperationArchive.getSenderZone()).isEqualTo(UPDATED_SENDER_ZONE);
        assertThat(testMissedTransactionOperationArchive.getSenderProfile()).isEqualTo(UPDATED_SENDER_PROFILE);
        assertThat(testMissedTransactionOperationArchive.getCodeTerritory()).isEqualTo(UPDATED_CODE_TERRITORY);
        assertThat(testMissedTransactionOperationArchive.getSubType()).isEqualTo(UPDATED_SUB_TYPE);
        assertThat(testMissedTransactionOperationArchive.getOperationDate()).isEqualTo(UPDATED_OPERATION_DATE);
        assertThat(testMissedTransactionOperationArchive.getIsFraud()).isEqualTo(UPDATED_IS_FRAUD);
        assertThat(testMissedTransactionOperationArchive.getTaggedAt()).isEqualTo(UPDATED_TAGGED_AT);
        assertThat(testMissedTransactionOperationArchive.getFraudSource()).isEqualTo(UPDATED_FRAUD_SOURCE);
        assertThat(testMissedTransactionOperationArchive.getComment()).isEqualTo(UPDATED_COMMENT);
        assertThat(testMissedTransactionOperationArchive.getFalsePositiveDetectedAt()).isEqualTo(UPDATED_FALSE_POSITIVE_DETECTED_AT);
        assertThat(testMissedTransactionOperationArchive.getTid()).isEqualTo(UPDATED_TID);
        assertThat(testMissedTransactionOperationArchive.getParentMsisdn()).isEqualTo(UPDATED_PARENT_MSISDN);
        assertThat(testMissedTransactionOperationArchive.getFctDt()).isEqualTo(UPDATED_FCT_DT);
        assertThat(testMissedTransactionOperationArchive.getParentId()).isEqualTo(UPDATED_PARENT_ID);
        assertThat(testMissedTransactionOperationArchive.getCanceledAt()).isEqualTo(UPDATED_CANCELED_AT);
        assertThat(testMissedTransactionOperationArchive.getCanceledId()).isEqualTo(UPDATED_CANCELED_ID);
        assertThat(testMissedTransactionOperationArchive.getProductId()).isEqualTo(UPDATED_PRODUCT_ID);
    }

    @Test
    @Transactional
    void patchNonExistingMissedTransactionOperationArchive() throws Exception {
        int databaseSizeBeforeUpdate = missedTransactionOperationArchiveRepository.findAll().size();
        missedTransactionOperationArchive.setId(count.incrementAndGet());

        // Create the MissedTransactionOperationArchive
        MissedTransactionOperationArchiveDTO missedTransactionOperationArchiveDTO = missedTransactionOperationArchiveMapper.toDto(
            missedTransactionOperationArchive
        );

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMissedTransactionOperationArchiveMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, missedTransactionOperationArchiveDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(missedTransactionOperationArchiveDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MissedTransactionOperationArchive in the database
        List<MissedTransactionOperationArchive> missedTransactionOperationArchiveList = missedTransactionOperationArchiveRepository.findAll();
        assertThat(missedTransactionOperationArchiveList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMissedTransactionOperationArchive() throws Exception {
        int databaseSizeBeforeUpdate = missedTransactionOperationArchiveRepository.findAll().size();
        missedTransactionOperationArchive.setId(count.incrementAndGet());

        // Create the MissedTransactionOperationArchive
        MissedTransactionOperationArchiveDTO missedTransactionOperationArchiveDTO = missedTransactionOperationArchiveMapper.toDto(
            missedTransactionOperationArchive
        );

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMissedTransactionOperationArchiveMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(missedTransactionOperationArchiveDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MissedTransactionOperationArchive in the database
        List<MissedTransactionOperationArchive> missedTransactionOperationArchiveList = missedTransactionOperationArchiveRepository.findAll();
        assertThat(missedTransactionOperationArchiveList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMissedTransactionOperationArchive() throws Exception {
        int databaseSizeBeforeUpdate = missedTransactionOperationArchiveRepository.findAll().size();
        missedTransactionOperationArchive.setId(count.incrementAndGet());

        // Create the MissedTransactionOperationArchive
        MissedTransactionOperationArchiveDTO missedTransactionOperationArchiveDTO = missedTransactionOperationArchiveMapper.toDto(
            missedTransactionOperationArchive
        );

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMissedTransactionOperationArchiveMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(missedTransactionOperationArchiveDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MissedTransactionOperationArchive in the database
        List<MissedTransactionOperationArchive> missedTransactionOperationArchiveList = missedTransactionOperationArchiveRepository.findAll();
        assertThat(missedTransactionOperationArchiveList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMissedTransactionOperationArchive() throws Exception {
        // Initialize the database
        missedTransactionOperationArchiveRepository.saveAndFlush(missedTransactionOperationArchive);

        int databaseSizeBeforeDelete = missedTransactionOperationArchiveRepository.findAll().size();

        // Delete the missedTransactionOperationArchive
        restMissedTransactionOperationArchiveMockMvc
            .perform(delete(ENTITY_API_URL_ID, missedTransactionOperationArchive.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MissedTransactionOperationArchive> missedTransactionOperationArchiveList = missedTransactionOperationArchiveRepository.findAll();
        assertThat(missedTransactionOperationArchiveList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
