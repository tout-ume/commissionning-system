package sn.free.commissioning.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.IntegrationTest;
import sn.free.commissioning.domain.MissedTransactionOperation;
import sn.free.commissioning.repository.MissedTransactionOperationRepository;
import sn.free.commissioning.service.criteria.MissedTransactionOperationCriteria;
import sn.free.commissioning.service.dto.MissedTransactionOperationDTO;
import sn.free.commissioning.service.mapper.MissedTransactionOperationMapper;

/**
 * Integration tests for the {@link MissedTransactionOperationResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MissedTransactionOperationResourceIT {

    private static final Double DEFAULT_AMOUNT = 1D;
    private static final Double UPDATED_AMOUNT = 2D;
    private static final Double SMALLER_AMOUNT = 1D - 1D;

    private static final String DEFAULT_SUBS_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_SUBS_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_AGENT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_AGENT_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE_TRANSACTION = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_TRANSACTION = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_TRANSACTION_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_TRANSACTION_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_SENDER_ZONE = "AAAAAAAAAA";
    private static final String UPDATED_SENDER_ZONE = "BBBBBBBBBB";

    private static final String DEFAULT_SENDER_PROFILE = "AAAAAAAAAA";
    private static final String UPDATED_SENDER_PROFILE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE_TERRITORY = "AAAAAAAAAA";
    private static final String UPDATED_CODE_TERRITORY = "BBBBBBBBBB";

    private static final String DEFAULT_SUB_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_SUB_TYPE = "BBBBBBBBBB";

    private static final Instant DEFAULT_OPERATION_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_OPERATION_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_IS_FRAUD = false;
    private static final Boolean UPDATED_IS_FRAUD = true;

    private static final Instant DEFAULT_TAGGED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TAGGED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_FRAUD_SOURCE = "AAAAAAAAAA";
    private static final String UPDATED_FRAUD_SOURCE = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENT = "BBBBBBBBBB";

    private static final Instant DEFAULT_FALSE_POSITIVE_DETECTED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FALSE_POSITIVE_DETECTED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_TID = "AAAAAAAAAA";
    private static final String UPDATED_TID = "BBBBBBBBBB";

    private static final String DEFAULT_PARENT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_PARENT_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_FCT_DT = "AAAAAAAAAA";
    private static final String UPDATED_FCT_DT = "BBBBBBBBBB";

    private static final String DEFAULT_PARENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_PARENT_ID = "BBBBBBBBBB";

    private static final Instant DEFAULT_CANCELED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CANCELED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CANCELED_ID = "AAAAAAAAAA";
    private static final String UPDATED_CANCELED_ID = "BBBBBBBBBB";

    private static final String DEFAULT_PRODUCT_ID = "AAAAAAAAAA";
    private static final String UPDATED_PRODUCT_ID = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/missed-transaction-operations";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MissedTransactionOperationRepository missedTransactionOperationRepository;

    @Autowired
    private MissedTransactionOperationMapper missedTransactionOperationMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMissedTransactionOperationMockMvc;

    private MissedTransactionOperation missedTransactionOperation;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MissedTransactionOperation createEntity(EntityManager em) {
        MissedTransactionOperation missedTransactionOperation = new MissedTransactionOperation()
            .amount(DEFAULT_AMOUNT)
            .subsMsisdn(DEFAULT_SUBS_MSISDN)
            .agentMsisdn(DEFAULT_AGENT_MSISDN)
            .typeTransaction(DEFAULT_TYPE_TRANSACTION)
            .createdAt(DEFAULT_CREATED_AT)
            .transactionStatus(DEFAULT_TRANSACTION_STATUS)
            .senderZone(DEFAULT_SENDER_ZONE)
            .senderProfile(DEFAULT_SENDER_PROFILE)
            .codeTerritory(DEFAULT_CODE_TERRITORY)
            .subType(DEFAULT_SUB_TYPE)
            .operationDate(DEFAULT_OPERATION_DATE)
            .isFraud(DEFAULT_IS_FRAUD)
            .taggedAt(DEFAULT_TAGGED_AT)
            .fraudSource(DEFAULT_FRAUD_SOURCE)
            .comment(DEFAULT_COMMENT)
            .falsePositiveDetectedAt(DEFAULT_FALSE_POSITIVE_DETECTED_AT)
            .tid(DEFAULT_TID)
            .parentMsisdn(DEFAULT_PARENT_MSISDN)
            .fctDt(DEFAULT_FCT_DT)
            .parentId(DEFAULT_PARENT_ID)
            .canceledAt(DEFAULT_CANCELED_AT)
            .canceledId(DEFAULT_CANCELED_ID)
            .productId(DEFAULT_PRODUCT_ID);
        return missedTransactionOperation;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MissedTransactionOperation createUpdatedEntity(EntityManager em) {
        MissedTransactionOperation missedTransactionOperation = new MissedTransactionOperation()
            .amount(UPDATED_AMOUNT)
            .subsMsisdn(UPDATED_SUBS_MSISDN)
            .agentMsisdn(UPDATED_AGENT_MSISDN)
            .typeTransaction(UPDATED_TYPE_TRANSACTION)
            .createdAt(UPDATED_CREATED_AT)
            .transactionStatus(UPDATED_TRANSACTION_STATUS)
            .senderZone(UPDATED_SENDER_ZONE)
            .senderProfile(UPDATED_SENDER_PROFILE)
            .codeTerritory(UPDATED_CODE_TERRITORY)
            .subType(UPDATED_SUB_TYPE)
            .operationDate(UPDATED_OPERATION_DATE)
            .isFraud(UPDATED_IS_FRAUD)
            .taggedAt(UPDATED_TAGGED_AT)
            .fraudSource(UPDATED_FRAUD_SOURCE)
            .comment(UPDATED_COMMENT)
            .falsePositiveDetectedAt(UPDATED_FALSE_POSITIVE_DETECTED_AT)
            .tid(UPDATED_TID)
            .parentMsisdn(UPDATED_PARENT_MSISDN)
            .fctDt(UPDATED_FCT_DT)
            .parentId(UPDATED_PARENT_ID)
            .canceledAt(UPDATED_CANCELED_AT)
            .canceledId(UPDATED_CANCELED_ID)
            .productId(UPDATED_PRODUCT_ID);
        return missedTransactionOperation;
    }

    @BeforeEach
    public void initTest() {
        missedTransactionOperation = createEntity(em);
    }

    @Test
    @Transactional
    void createMissedTransactionOperation() throws Exception {
        int databaseSizeBeforeCreate = missedTransactionOperationRepository.findAll().size();
        // Create the MissedTransactionOperation
        MissedTransactionOperationDTO missedTransactionOperationDTO = missedTransactionOperationMapper.toDto(missedTransactionOperation);
        restMissedTransactionOperationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(missedTransactionOperationDTO))
            )
            .andExpect(status().isCreated());

        // Validate the MissedTransactionOperation in the database
        List<MissedTransactionOperation> missedTransactionOperationList = missedTransactionOperationRepository.findAll();
        assertThat(missedTransactionOperationList).hasSize(databaseSizeBeforeCreate + 1);
        MissedTransactionOperation testMissedTransactionOperation = missedTransactionOperationList.get(
            missedTransactionOperationList.size() - 1
        );
        assertThat(testMissedTransactionOperation.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testMissedTransactionOperation.getSubsMsisdn()).isEqualTo(DEFAULT_SUBS_MSISDN);
        assertThat(testMissedTransactionOperation.getAgentMsisdn()).isEqualTo(DEFAULT_AGENT_MSISDN);
        assertThat(testMissedTransactionOperation.getTypeTransaction()).isEqualTo(DEFAULT_TYPE_TRANSACTION);
        assertThat(testMissedTransactionOperation.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testMissedTransactionOperation.getTransactionStatus()).isEqualTo(DEFAULT_TRANSACTION_STATUS);
        assertThat(testMissedTransactionOperation.getSenderZone()).isEqualTo(DEFAULT_SENDER_ZONE);
        assertThat(testMissedTransactionOperation.getSenderProfile()).isEqualTo(DEFAULT_SENDER_PROFILE);
        assertThat(testMissedTransactionOperation.getCodeTerritory()).isEqualTo(DEFAULT_CODE_TERRITORY);
        assertThat(testMissedTransactionOperation.getSubType()).isEqualTo(DEFAULT_SUB_TYPE);
        assertThat(testMissedTransactionOperation.getOperationDate()).isEqualTo(DEFAULT_OPERATION_DATE);
        assertThat(testMissedTransactionOperation.getIsFraud()).isEqualTo(DEFAULT_IS_FRAUD);
        assertThat(testMissedTransactionOperation.getTaggedAt()).isEqualTo(DEFAULT_TAGGED_AT);
        assertThat(testMissedTransactionOperation.getFraudSource()).isEqualTo(DEFAULT_FRAUD_SOURCE);
        assertThat(testMissedTransactionOperation.getComment()).isEqualTo(DEFAULT_COMMENT);
        assertThat(testMissedTransactionOperation.getFalsePositiveDetectedAt()).isEqualTo(DEFAULT_FALSE_POSITIVE_DETECTED_AT);
        assertThat(testMissedTransactionOperation.getTid()).isEqualTo(DEFAULT_TID);
        assertThat(testMissedTransactionOperation.getParentMsisdn()).isEqualTo(DEFAULT_PARENT_MSISDN);
        assertThat(testMissedTransactionOperation.getFctDt()).isEqualTo(DEFAULT_FCT_DT);
        assertThat(testMissedTransactionOperation.getParentId()).isEqualTo(DEFAULT_PARENT_ID);
        assertThat(testMissedTransactionOperation.getCanceledAt()).isEqualTo(DEFAULT_CANCELED_AT);
        assertThat(testMissedTransactionOperation.getCanceledId()).isEqualTo(DEFAULT_CANCELED_ID);
        assertThat(testMissedTransactionOperation.getProductId()).isEqualTo(DEFAULT_PRODUCT_ID);
    }

    @Test
    @Transactional
    void createMissedTransactionOperationWithExistingId() throws Exception {
        // Create the MissedTransactionOperation with an existing ID
        missedTransactionOperation.setId(1L);
        MissedTransactionOperationDTO missedTransactionOperationDTO = missedTransactionOperationMapper.toDto(missedTransactionOperation);

        int databaseSizeBeforeCreate = missedTransactionOperationRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMissedTransactionOperationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(missedTransactionOperationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MissedTransactionOperation in the database
        List<MissedTransactionOperation> missedTransactionOperationList = missedTransactionOperationRepository.findAll();
        assertThat(missedTransactionOperationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperations() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList
        restMissedTransactionOperationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(missedTransactionOperation.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].subsMsisdn").value(hasItem(DEFAULT_SUBS_MSISDN)))
            .andExpect(jsonPath("$.[*].agentMsisdn").value(hasItem(DEFAULT_AGENT_MSISDN)))
            .andExpect(jsonPath("$.[*].typeTransaction").value(hasItem(DEFAULT_TYPE_TRANSACTION)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].transactionStatus").value(hasItem(DEFAULT_TRANSACTION_STATUS)))
            .andExpect(jsonPath("$.[*].senderZone").value(hasItem(DEFAULT_SENDER_ZONE)))
            .andExpect(jsonPath("$.[*].senderProfile").value(hasItem(DEFAULT_SENDER_PROFILE)))
            .andExpect(jsonPath("$.[*].codeTerritory").value(hasItem(DEFAULT_CODE_TERRITORY)))
            .andExpect(jsonPath("$.[*].subType").value(hasItem(DEFAULT_SUB_TYPE)))
            .andExpect(jsonPath("$.[*].operationDate").value(hasItem(DEFAULT_OPERATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].isFraud").value(hasItem(DEFAULT_IS_FRAUD.booleanValue())))
            .andExpect(jsonPath("$.[*].taggedAt").value(hasItem(DEFAULT_TAGGED_AT.toString())))
            .andExpect(jsonPath("$.[*].fraudSource").value(hasItem(DEFAULT_FRAUD_SOURCE)))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT)))
            .andExpect(jsonPath("$.[*].falsePositiveDetectedAt").value(hasItem(DEFAULT_FALSE_POSITIVE_DETECTED_AT.toString())))
            .andExpect(jsonPath("$.[*].tid").value(hasItem(DEFAULT_TID)))
            .andExpect(jsonPath("$.[*].parentMsisdn").value(hasItem(DEFAULT_PARENT_MSISDN)))
            .andExpect(jsonPath("$.[*].fctDt").value(hasItem(DEFAULT_FCT_DT)))
            .andExpect(jsonPath("$.[*].parentId").value(hasItem(DEFAULT_PARENT_ID)))
            .andExpect(jsonPath("$.[*].canceledAt").value(hasItem(DEFAULT_CANCELED_AT.toString())))
            .andExpect(jsonPath("$.[*].canceledId").value(hasItem(DEFAULT_CANCELED_ID)))
            .andExpect(jsonPath("$.[*].productId").value(hasItem(DEFAULT_PRODUCT_ID)));
    }

    @Test
    @Transactional
    void getMissedTransactionOperation() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get the missedTransactionOperation
        restMissedTransactionOperationMockMvc
            .perform(get(ENTITY_API_URL_ID, missedTransactionOperation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(missedTransactionOperation.getId().intValue()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.subsMsisdn").value(DEFAULT_SUBS_MSISDN))
            .andExpect(jsonPath("$.agentMsisdn").value(DEFAULT_AGENT_MSISDN))
            .andExpect(jsonPath("$.typeTransaction").value(DEFAULT_TYPE_TRANSACTION))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.transactionStatus").value(DEFAULT_TRANSACTION_STATUS))
            .andExpect(jsonPath("$.senderZone").value(DEFAULT_SENDER_ZONE))
            .andExpect(jsonPath("$.senderProfile").value(DEFAULT_SENDER_PROFILE))
            .andExpect(jsonPath("$.codeTerritory").value(DEFAULT_CODE_TERRITORY))
            .andExpect(jsonPath("$.subType").value(DEFAULT_SUB_TYPE))
            .andExpect(jsonPath("$.operationDate").value(DEFAULT_OPERATION_DATE.toString()))
            .andExpect(jsonPath("$.isFraud").value(DEFAULT_IS_FRAUD.booleanValue()))
            .andExpect(jsonPath("$.taggedAt").value(DEFAULT_TAGGED_AT.toString()))
            .andExpect(jsonPath("$.fraudSource").value(DEFAULT_FRAUD_SOURCE))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT))
            .andExpect(jsonPath("$.falsePositiveDetectedAt").value(DEFAULT_FALSE_POSITIVE_DETECTED_AT.toString()))
            .andExpect(jsonPath("$.tid").value(DEFAULT_TID))
            .andExpect(jsonPath("$.parentMsisdn").value(DEFAULT_PARENT_MSISDN))
            .andExpect(jsonPath("$.fctDt").value(DEFAULT_FCT_DT))
            .andExpect(jsonPath("$.parentId").value(DEFAULT_PARENT_ID))
            .andExpect(jsonPath("$.canceledAt").value(DEFAULT_CANCELED_AT.toString()))
            .andExpect(jsonPath("$.canceledId").value(DEFAULT_CANCELED_ID))
            .andExpect(jsonPath("$.productId").value(DEFAULT_PRODUCT_ID));
    }

    @Test
    @Transactional
    void getMissedTransactionOperationsByIdFiltering() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        Long id = missedTransactionOperation.getId();

        defaultMissedTransactionOperationShouldBeFound("id.equals=" + id);
        defaultMissedTransactionOperationShouldNotBeFound("id.notEquals=" + id);

        defaultMissedTransactionOperationShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultMissedTransactionOperationShouldNotBeFound("id.greaterThan=" + id);

        defaultMissedTransactionOperationShouldBeFound("id.lessThanOrEqual=" + id);
        defaultMissedTransactionOperationShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where amount equals to DEFAULT_AMOUNT
        defaultMissedTransactionOperationShouldBeFound("amount.equals=" + DEFAULT_AMOUNT);

        // Get all the missedTransactionOperationList where amount equals to UPDATED_AMOUNT
        defaultMissedTransactionOperationShouldNotBeFound("amount.equals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByAmountIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where amount not equals to DEFAULT_AMOUNT
        defaultMissedTransactionOperationShouldNotBeFound("amount.notEquals=" + DEFAULT_AMOUNT);

        // Get all the missedTransactionOperationList where amount not equals to UPDATED_AMOUNT
        defaultMissedTransactionOperationShouldBeFound("amount.notEquals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByAmountIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where amount in DEFAULT_AMOUNT or UPDATED_AMOUNT
        defaultMissedTransactionOperationShouldBeFound("amount.in=" + DEFAULT_AMOUNT + "," + UPDATED_AMOUNT);

        // Get all the missedTransactionOperationList where amount equals to UPDATED_AMOUNT
        defaultMissedTransactionOperationShouldNotBeFound("amount.in=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where amount is not null
        defaultMissedTransactionOperationShouldBeFound("amount.specified=true");

        // Get all the missedTransactionOperationList where amount is null
        defaultMissedTransactionOperationShouldNotBeFound("amount.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByAmountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where amount is greater than or equal to DEFAULT_AMOUNT
        defaultMissedTransactionOperationShouldBeFound("amount.greaterThanOrEqual=" + DEFAULT_AMOUNT);

        // Get all the missedTransactionOperationList where amount is greater than or equal to UPDATED_AMOUNT
        defaultMissedTransactionOperationShouldNotBeFound("amount.greaterThanOrEqual=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByAmountIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where amount is less than or equal to DEFAULT_AMOUNT
        defaultMissedTransactionOperationShouldBeFound("amount.lessThanOrEqual=" + DEFAULT_AMOUNT);

        // Get all the missedTransactionOperationList where amount is less than or equal to SMALLER_AMOUNT
        defaultMissedTransactionOperationShouldNotBeFound("amount.lessThanOrEqual=" + SMALLER_AMOUNT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByAmountIsLessThanSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where amount is less than DEFAULT_AMOUNT
        defaultMissedTransactionOperationShouldNotBeFound("amount.lessThan=" + DEFAULT_AMOUNT);

        // Get all the missedTransactionOperationList where amount is less than UPDATED_AMOUNT
        defaultMissedTransactionOperationShouldBeFound("amount.lessThan=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByAmountIsGreaterThanSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where amount is greater than DEFAULT_AMOUNT
        defaultMissedTransactionOperationShouldNotBeFound("amount.greaterThan=" + DEFAULT_AMOUNT);

        // Get all the missedTransactionOperationList where amount is greater than SMALLER_AMOUNT
        defaultMissedTransactionOperationShouldBeFound("amount.greaterThan=" + SMALLER_AMOUNT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySubsMsisdnIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where subsMsisdn equals to DEFAULT_SUBS_MSISDN
        defaultMissedTransactionOperationShouldBeFound("subsMsisdn.equals=" + DEFAULT_SUBS_MSISDN);

        // Get all the missedTransactionOperationList where subsMsisdn equals to UPDATED_SUBS_MSISDN
        defaultMissedTransactionOperationShouldNotBeFound("subsMsisdn.equals=" + UPDATED_SUBS_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySubsMsisdnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where subsMsisdn not equals to DEFAULT_SUBS_MSISDN
        defaultMissedTransactionOperationShouldNotBeFound("subsMsisdn.notEquals=" + DEFAULT_SUBS_MSISDN);

        // Get all the missedTransactionOperationList where subsMsisdn not equals to UPDATED_SUBS_MSISDN
        defaultMissedTransactionOperationShouldBeFound("subsMsisdn.notEquals=" + UPDATED_SUBS_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySubsMsisdnIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where subsMsisdn in DEFAULT_SUBS_MSISDN or UPDATED_SUBS_MSISDN
        defaultMissedTransactionOperationShouldBeFound("subsMsisdn.in=" + DEFAULT_SUBS_MSISDN + "," + UPDATED_SUBS_MSISDN);

        // Get all the missedTransactionOperationList where subsMsisdn equals to UPDATED_SUBS_MSISDN
        defaultMissedTransactionOperationShouldNotBeFound("subsMsisdn.in=" + UPDATED_SUBS_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySubsMsisdnIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where subsMsisdn is not null
        defaultMissedTransactionOperationShouldBeFound("subsMsisdn.specified=true");

        // Get all the missedTransactionOperationList where subsMsisdn is null
        defaultMissedTransactionOperationShouldNotBeFound("subsMsisdn.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySubsMsisdnContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where subsMsisdn contains DEFAULT_SUBS_MSISDN
        defaultMissedTransactionOperationShouldBeFound("subsMsisdn.contains=" + DEFAULT_SUBS_MSISDN);

        // Get all the missedTransactionOperationList where subsMsisdn contains UPDATED_SUBS_MSISDN
        defaultMissedTransactionOperationShouldNotBeFound("subsMsisdn.contains=" + UPDATED_SUBS_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySubsMsisdnNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where subsMsisdn does not contain DEFAULT_SUBS_MSISDN
        defaultMissedTransactionOperationShouldNotBeFound("subsMsisdn.doesNotContain=" + DEFAULT_SUBS_MSISDN);

        // Get all the missedTransactionOperationList where subsMsisdn does not contain UPDATED_SUBS_MSISDN
        defaultMissedTransactionOperationShouldBeFound("subsMsisdn.doesNotContain=" + UPDATED_SUBS_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByAgentMsisdnIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where agentMsisdn equals to DEFAULT_AGENT_MSISDN
        defaultMissedTransactionOperationShouldBeFound("agentMsisdn.equals=" + DEFAULT_AGENT_MSISDN);

        // Get all the missedTransactionOperationList where agentMsisdn equals to UPDATED_AGENT_MSISDN
        defaultMissedTransactionOperationShouldNotBeFound("agentMsisdn.equals=" + UPDATED_AGENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByAgentMsisdnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where agentMsisdn not equals to DEFAULT_AGENT_MSISDN
        defaultMissedTransactionOperationShouldNotBeFound("agentMsisdn.notEquals=" + DEFAULT_AGENT_MSISDN);

        // Get all the missedTransactionOperationList where agentMsisdn not equals to UPDATED_AGENT_MSISDN
        defaultMissedTransactionOperationShouldBeFound("agentMsisdn.notEquals=" + UPDATED_AGENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByAgentMsisdnIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where agentMsisdn in DEFAULT_AGENT_MSISDN or UPDATED_AGENT_MSISDN
        defaultMissedTransactionOperationShouldBeFound("agentMsisdn.in=" + DEFAULT_AGENT_MSISDN + "," + UPDATED_AGENT_MSISDN);

        // Get all the missedTransactionOperationList where agentMsisdn equals to UPDATED_AGENT_MSISDN
        defaultMissedTransactionOperationShouldNotBeFound("agentMsisdn.in=" + UPDATED_AGENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByAgentMsisdnIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where agentMsisdn is not null
        defaultMissedTransactionOperationShouldBeFound("agentMsisdn.specified=true");

        // Get all the missedTransactionOperationList where agentMsisdn is null
        defaultMissedTransactionOperationShouldNotBeFound("agentMsisdn.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByAgentMsisdnContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where agentMsisdn contains DEFAULT_AGENT_MSISDN
        defaultMissedTransactionOperationShouldBeFound("agentMsisdn.contains=" + DEFAULT_AGENT_MSISDN);

        // Get all the missedTransactionOperationList where agentMsisdn contains UPDATED_AGENT_MSISDN
        defaultMissedTransactionOperationShouldNotBeFound("agentMsisdn.contains=" + UPDATED_AGENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByAgentMsisdnNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where agentMsisdn does not contain DEFAULT_AGENT_MSISDN
        defaultMissedTransactionOperationShouldNotBeFound("agentMsisdn.doesNotContain=" + DEFAULT_AGENT_MSISDN);

        // Get all the missedTransactionOperationList where agentMsisdn does not contain UPDATED_AGENT_MSISDN
        defaultMissedTransactionOperationShouldBeFound("agentMsisdn.doesNotContain=" + UPDATED_AGENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTypeTransactionIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where typeTransaction equals to DEFAULT_TYPE_TRANSACTION
        defaultMissedTransactionOperationShouldBeFound("typeTransaction.equals=" + DEFAULT_TYPE_TRANSACTION);

        // Get all the missedTransactionOperationList where typeTransaction equals to UPDATED_TYPE_TRANSACTION
        defaultMissedTransactionOperationShouldNotBeFound("typeTransaction.equals=" + UPDATED_TYPE_TRANSACTION);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTypeTransactionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where typeTransaction not equals to DEFAULT_TYPE_TRANSACTION
        defaultMissedTransactionOperationShouldNotBeFound("typeTransaction.notEquals=" + DEFAULT_TYPE_TRANSACTION);

        // Get all the missedTransactionOperationList where typeTransaction not equals to UPDATED_TYPE_TRANSACTION
        defaultMissedTransactionOperationShouldBeFound("typeTransaction.notEquals=" + UPDATED_TYPE_TRANSACTION);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTypeTransactionIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where typeTransaction in DEFAULT_TYPE_TRANSACTION or UPDATED_TYPE_TRANSACTION
        defaultMissedTransactionOperationShouldBeFound("typeTransaction.in=" + DEFAULT_TYPE_TRANSACTION + "," + UPDATED_TYPE_TRANSACTION);

        // Get all the missedTransactionOperationList where typeTransaction equals to UPDATED_TYPE_TRANSACTION
        defaultMissedTransactionOperationShouldNotBeFound("typeTransaction.in=" + UPDATED_TYPE_TRANSACTION);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTypeTransactionIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where typeTransaction is not null
        defaultMissedTransactionOperationShouldBeFound("typeTransaction.specified=true");

        // Get all the missedTransactionOperationList where typeTransaction is null
        defaultMissedTransactionOperationShouldNotBeFound("typeTransaction.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTypeTransactionContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where typeTransaction contains DEFAULT_TYPE_TRANSACTION
        defaultMissedTransactionOperationShouldBeFound("typeTransaction.contains=" + DEFAULT_TYPE_TRANSACTION);

        // Get all the missedTransactionOperationList where typeTransaction contains UPDATED_TYPE_TRANSACTION
        defaultMissedTransactionOperationShouldNotBeFound("typeTransaction.contains=" + UPDATED_TYPE_TRANSACTION);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTypeTransactionNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where typeTransaction does not contain DEFAULT_TYPE_TRANSACTION
        defaultMissedTransactionOperationShouldNotBeFound("typeTransaction.doesNotContain=" + DEFAULT_TYPE_TRANSACTION);

        // Get all the missedTransactionOperationList where typeTransaction does not contain UPDATED_TYPE_TRANSACTION
        defaultMissedTransactionOperationShouldBeFound("typeTransaction.doesNotContain=" + UPDATED_TYPE_TRANSACTION);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCreatedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where createdAt equals to DEFAULT_CREATED_AT
        defaultMissedTransactionOperationShouldBeFound("createdAt.equals=" + DEFAULT_CREATED_AT);

        // Get all the missedTransactionOperationList where createdAt equals to UPDATED_CREATED_AT
        defaultMissedTransactionOperationShouldNotBeFound("createdAt.equals=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCreatedAtIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where createdAt not equals to DEFAULT_CREATED_AT
        defaultMissedTransactionOperationShouldNotBeFound("createdAt.notEquals=" + DEFAULT_CREATED_AT);

        // Get all the missedTransactionOperationList where createdAt not equals to UPDATED_CREATED_AT
        defaultMissedTransactionOperationShouldBeFound("createdAt.notEquals=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCreatedAtIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where createdAt in DEFAULT_CREATED_AT or UPDATED_CREATED_AT
        defaultMissedTransactionOperationShouldBeFound("createdAt.in=" + DEFAULT_CREATED_AT + "," + UPDATED_CREATED_AT);

        // Get all the missedTransactionOperationList where createdAt equals to UPDATED_CREATED_AT
        defaultMissedTransactionOperationShouldNotBeFound("createdAt.in=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCreatedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where createdAt is not null
        defaultMissedTransactionOperationShouldBeFound("createdAt.specified=true");

        // Get all the missedTransactionOperationList where createdAt is null
        defaultMissedTransactionOperationShouldNotBeFound("createdAt.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTransactionStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where transactionStatus equals to DEFAULT_TRANSACTION_STATUS
        defaultMissedTransactionOperationShouldBeFound("transactionStatus.equals=" + DEFAULT_TRANSACTION_STATUS);

        // Get all the missedTransactionOperationList where transactionStatus equals to UPDATED_TRANSACTION_STATUS
        defaultMissedTransactionOperationShouldNotBeFound("transactionStatus.equals=" + UPDATED_TRANSACTION_STATUS);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTransactionStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where transactionStatus not equals to DEFAULT_TRANSACTION_STATUS
        defaultMissedTransactionOperationShouldNotBeFound("transactionStatus.notEquals=" + DEFAULT_TRANSACTION_STATUS);

        // Get all the missedTransactionOperationList where transactionStatus not equals to UPDATED_TRANSACTION_STATUS
        defaultMissedTransactionOperationShouldBeFound("transactionStatus.notEquals=" + UPDATED_TRANSACTION_STATUS);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTransactionStatusIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where transactionStatus in DEFAULT_TRANSACTION_STATUS or UPDATED_TRANSACTION_STATUS
        defaultMissedTransactionOperationShouldBeFound(
            "transactionStatus.in=" + DEFAULT_TRANSACTION_STATUS + "," + UPDATED_TRANSACTION_STATUS
        );

        // Get all the missedTransactionOperationList where transactionStatus equals to UPDATED_TRANSACTION_STATUS
        defaultMissedTransactionOperationShouldNotBeFound("transactionStatus.in=" + UPDATED_TRANSACTION_STATUS);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTransactionStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where transactionStatus is not null
        defaultMissedTransactionOperationShouldBeFound("transactionStatus.specified=true");

        // Get all the missedTransactionOperationList where transactionStatus is null
        defaultMissedTransactionOperationShouldNotBeFound("transactionStatus.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTransactionStatusContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where transactionStatus contains DEFAULT_TRANSACTION_STATUS
        defaultMissedTransactionOperationShouldBeFound("transactionStatus.contains=" + DEFAULT_TRANSACTION_STATUS);

        // Get all the missedTransactionOperationList where transactionStatus contains UPDATED_TRANSACTION_STATUS
        defaultMissedTransactionOperationShouldNotBeFound("transactionStatus.contains=" + UPDATED_TRANSACTION_STATUS);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTransactionStatusNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where transactionStatus does not contain DEFAULT_TRANSACTION_STATUS
        defaultMissedTransactionOperationShouldNotBeFound("transactionStatus.doesNotContain=" + DEFAULT_TRANSACTION_STATUS);

        // Get all the missedTransactionOperationList where transactionStatus does not contain UPDATED_TRANSACTION_STATUS
        defaultMissedTransactionOperationShouldBeFound("transactionStatus.doesNotContain=" + UPDATED_TRANSACTION_STATUS);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySenderZoneIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where senderZone equals to DEFAULT_SENDER_ZONE
        defaultMissedTransactionOperationShouldBeFound("senderZone.equals=" + DEFAULT_SENDER_ZONE);

        // Get all the missedTransactionOperationList where senderZone equals to UPDATED_SENDER_ZONE
        defaultMissedTransactionOperationShouldNotBeFound("senderZone.equals=" + UPDATED_SENDER_ZONE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySenderZoneIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where senderZone not equals to DEFAULT_SENDER_ZONE
        defaultMissedTransactionOperationShouldNotBeFound("senderZone.notEquals=" + DEFAULT_SENDER_ZONE);

        // Get all the missedTransactionOperationList where senderZone not equals to UPDATED_SENDER_ZONE
        defaultMissedTransactionOperationShouldBeFound("senderZone.notEquals=" + UPDATED_SENDER_ZONE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySenderZoneIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where senderZone in DEFAULT_SENDER_ZONE or UPDATED_SENDER_ZONE
        defaultMissedTransactionOperationShouldBeFound("senderZone.in=" + DEFAULT_SENDER_ZONE + "," + UPDATED_SENDER_ZONE);

        // Get all the missedTransactionOperationList where senderZone equals to UPDATED_SENDER_ZONE
        defaultMissedTransactionOperationShouldNotBeFound("senderZone.in=" + UPDATED_SENDER_ZONE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySenderZoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where senderZone is not null
        defaultMissedTransactionOperationShouldBeFound("senderZone.specified=true");

        // Get all the missedTransactionOperationList where senderZone is null
        defaultMissedTransactionOperationShouldNotBeFound("senderZone.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySenderZoneContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where senderZone contains DEFAULT_SENDER_ZONE
        defaultMissedTransactionOperationShouldBeFound("senderZone.contains=" + DEFAULT_SENDER_ZONE);

        // Get all the missedTransactionOperationList where senderZone contains UPDATED_SENDER_ZONE
        defaultMissedTransactionOperationShouldNotBeFound("senderZone.contains=" + UPDATED_SENDER_ZONE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySenderZoneNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where senderZone does not contain DEFAULT_SENDER_ZONE
        defaultMissedTransactionOperationShouldNotBeFound("senderZone.doesNotContain=" + DEFAULT_SENDER_ZONE);

        // Get all the missedTransactionOperationList where senderZone does not contain UPDATED_SENDER_ZONE
        defaultMissedTransactionOperationShouldBeFound("senderZone.doesNotContain=" + UPDATED_SENDER_ZONE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySenderProfileIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where senderProfile equals to DEFAULT_SENDER_PROFILE
        defaultMissedTransactionOperationShouldBeFound("senderProfile.equals=" + DEFAULT_SENDER_PROFILE);

        // Get all the missedTransactionOperationList where senderProfile equals to UPDATED_SENDER_PROFILE
        defaultMissedTransactionOperationShouldNotBeFound("senderProfile.equals=" + UPDATED_SENDER_PROFILE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySenderProfileIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where senderProfile not equals to DEFAULT_SENDER_PROFILE
        defaultMissedTransactionOperationShouldNotBeFound("senderProfile.notEquals=" + DEFAULT_SENDER_PROFILE);

        // Get all the missedTransactionOperationList where senderProfile not equals to UPDATED_SENDER_PROFILE
        defaultMissedTransactionOperationShouldBeFound("senderProfile.notEquals=" + UPDATED_SENDER_PROFILE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySenderProfileIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where senderProfile in DEFAULT_SENDER_PROFILE or UPDATED_SENDER_PROFILE
        defaultMissedTransactionOperationShouldBeFound("senderProfile.in=" + DEFAULT_SENDER_PROFILE + "," + UPDATED_SENDER_PROFILE);

        // Get all the missedTransactionOperationList where senderProfile equals to UPDATED_SENDER_PROFILE
        defaultMissedTransactionOperationShouldNotBeFound("senderProfile.in=" + UPDATED_SENDER_PROFILE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySenderProfileIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where senderProfile is not null
        defaultMissedTransactionOperationShouldBeFound("senderProfile.specified=true");

        // Get all the missedTransactionOperationList where senderProfile is null
        defaultMissedTransactionOperationShouldNotBeFound("senderProfile.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySenderProfileContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where senderProfile contains DEFAULT_SENDER_PROFILE
        defaultMissedTransactionOperationShouldBeFound("senderProfile.contains=" + DEFAULT_SENDER_PROFILE);

        // Get all the missedTransactionOperationList where senderProfile contains UPDATED_SENDER_PROFILE
        defaultMissedTransactionOperationShouldNotBeFound("senderProfile.contains=" + UPDATED_SENDER_PROFILE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySenderProfileNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where senderProfile does not contain DEFAULT_SENDER_PROFILE
        defaultMissedTransactionOperationShouldNotBeFound("senderProfile.doesNotContain=" + DEFAULT_SENDER_PROFILE);

        // Get all the missedTransactionOperationList where senderProfile does not contain UPDATED_SENDER_PROFILE
        defaultMissedTransactionOperationShouldBeFound("senderProfile.doesNotContain=" + UPDATED_SENDER_PROFILE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCodeTerritoryIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where codeTerritory equals to DEFAULT_CODE_TERRITORY
        defaultMissedTransactionOperationShouldBeFound("codeTerritory.equals=" + DEFAULT_CODE_TERRITORY);

        // Get all the missedTransactionOperationList where codeTerritory equals to UPDATED_CODE_TERRITORY
        defaultMissedTransactionOperationShouldNotBeFound("codeTerritory.equals=" + UPDATED_CODE_TERRITORY);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCodeTerritoryIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where codeTerritory not equals to DEFAULT_CODE_TERRITORY
        defaultMissedTransactionOperationShouldNotBeFound("codeTerritory.notEquals=" + DEFAULT_CODE_TERRITORY);

        // Get all the missedTransactionOperationList where codeTerritory not equals to UPDATED_CODE_TERRITORY
        defaultMissedTransactionOperationShouldBeFound("codeTerritory.notEquals=" + UPDATED_CODE_TERRITORY);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCodeTerritoryIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where codeTerritory in DEFAULT_CODE_TERRITORY or UPDATED_CODE_TERRITORY
        defaultMissedTransactionOperationShouldBeFound("codeTerritory.in=" + DEFAULT_CODE_TERRITORY + "," + UPDATED_CODE_TERRITORY);

        // Get all the missedTransactionOperationList where codeTerritory equals to UPDATED_CODE_TERRITORY
        defaultMissedTransactionOperationShouldNotBeFound("codeTerritory.in=" + UPDATED_CODE_TERRITORY);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCodeTerritoryIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where codeTerritory is not null
        defaultMissedTransactionOperationShouldBeFound("codeTerritory.specified=true");

        // Get all the missedTransactionOperationList where codeTerritory is null
        defaultMissedTransactionOperationShouldNotBeFound("codeTerritory.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCodeTerritoryContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where codeTerritory contains DEFAULT_CODE_TERRITORY
        defaultMissedTransactionOperationShouldBeFound("codeTerritory.contains=" + DEFAULT_CODE_TERRITORY);

        // Get all the missedTransactionOperationList where codeTerritory contains UPDATED_CODE_TERRITORY
        defaultMissedTransactionOperationShouldNotBeFound("codeTerritory.contains=" + UPDATED_CODE_TERRITORY);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCodeTerritoryNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where codeTerritory does not contain DEFAULT_CODE_TERRITORY
        defaultMissedTransactionOperationShouldNotBeFound("codeTerritory.doesNotContain=" + DEFAULT_CODE_TERRITORY);

        // Get all the missedTransactionOperationList where codeTerritory does not contain UPDATED_CODE_TERRITORY
        defaultMissedTransactionOperationShouldBeFound("codeTerritory.doesNotContain=" + UPDATED_CODE_TERRITORY);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySubTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where subType equals to DEFAULT_SUB_TYPE
        defaultMissedTransactionOperationShouldBeFound("subType.equals=" + DEFAULT_SUB_TYPE);

        // Get all the missedTransactionOperationList where subType equals to UPDATED_SUB_TYPE
        defaultMissedTransactionOperationShouldNotBeFound("subType.equals=" + UPDATED_SUB_TYPE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySubTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where subType not equals to DEFAULT_SUB_TYPE
        defaultMissedTransactionOperationShouldNotBeFound("subType.notEquals=" + DEFAULT_SUB_TYPE);

        // Get all the missedTransactionOperationList where subType not equals to UPDATED_SUB_TYPE
        defaultMissedTransactionOperationShouldBeFound("subType.notEquals=" + UPDATED_SUB_TYPE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySubTypeIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where subType in DEFAULT_SUB_TYPE or UPDATED_SUB_TYPE
        defaultMissedTransactionOperationShouldBeFound("subType.in=" + DEFAULT_SUB_TYPE + "," + UPDATED_SUB_TYPE);

        // Get all the missedTransactionOperationList where subType equals to UPDATED_SUB_TYPE
        defaultMissedTransactionOperationShouldNotBeFound("subType.in=" + UPDATED_SUB_TYPE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySubTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where subType is not null
        defaultMissedTransactionOperationShouldBeFound("subType.specified=true");

        // Get all the missedTransactionOperationList where subType is null
        defaultMissedTransactionOperationShouldNotBeFound("subType.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySubTypeContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where subType contains DEFAULT_SUB_TYPE
        defaultMissedTransactionOperationShouldBeFound("subType.contains=" + DEFAULT_SUB_TYPE);

        // Get all the missedTransactionOperationList where subType contains UPDATED_SUB_TYPE
        defaultMissedTransactionOperationShouldNotBeFound("subType.contains=" + UPDATED_SUB_TYPE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsBySubTypeNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where subType does not contain DEFAULT_SUB_TYPE
        defaultMissedTransactionOperationShouldNotBeFound("subType.doesNotContain=" + DEFAULT_SUB_TYPE);

        // Get all the missedTransactionOperationList where subType does not contain UPDATED_SUB_TYPE
        defaultMissedTransactionOperationShouldBeFound("subType.doesNotContain=" + UPDATED_SUB_TYPE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByOperationDateIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where operationDate equals to DEFAULT_OPERATION_DATE
        defaultMissedTransactionOperationShouldBeFound("operationDate.equals=" + DEFAULT_OPERATION_DATE);

        // Get all the missedTransactionOperationList where operationDate equals to UPDATED_OPERATION_DATE
        defaultMissedTransactionOperationShouldNotBeFound("operationDate.equals=" + UPDATED_OPERATION_DATE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByOperationDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where operationDate not equals to DEFAULT_OPERATION_DATE
        defaultMissedTransactionOperationShouldNotBeFound("operationDate.notEquals=" + DEFAULT_OPERATION_DATE);

        // Get all the missedTransactionOperationList where operationDate not equals to UPDATED_OPERATION_DATE
        defaultMissedTransactionOperationShouldBeFound("operationDate.notEquals=" + UPDATED_OPERATION_DATE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByOperationDateIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where operationDate in DEFAULT_OPERATION_DATE or UPDATED_OPERATION_DATE
        defaultMissedTransactionOperationShouldBeFound("operationDate.in=" + DEFAULT_OPERATION_DATE + "," + UPDATED_OPERATION_DATE);

        // Get all the missedTransactionOperationList where operationDate equals to UPDATED_OPERATION_DATE
        defaultMissedTransactionOperationShouldNotBeFound("operationDate.in=" + UPDATED_OPERATION_DATE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByOperationDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where operationDate is not null
        defaultMissedTransactionOperationShouldBeFound("operationDate.specified=true");

        // Get all the missedTransactionOperationList where operationDate is null
        defaultMissedTransactionOperationShouldNotBeFound("operationDate.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByIsFraudIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where isFraud equals to DEFAULT_IS_FRAUD
        defaultMissedTransactionOperationShouldBeFound("isFraud.equals=" + DEFAULT_IS_FRAUD);

        // Get all the missedTransactionOperationList where isFraud equals to UPDATED_IS_FRAUD
        defaultMissedTransactionOperationShouldNotBeFound("isFraud.equals=" + UPDATED_IS_FRAUD);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByIsFraudIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where isFraud not equals to DEFAULT_IS_FRAUD
        defaultMissedTransactionOperationShouldNotBeFound("isFraud.notEquals=" + DEFAULT_IS_FRAUD);

        // Get all the missedTransactionOperationList where isFraud not equals to UPDATED_IS_FRAUD
        defaultMissedTransactionOperationShouldBeFound("isFraud.notEquals=" + UPDATED_IS_FRAUD);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByIsFraudIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where isFraud in DEFAULT_IS_FRAUD or UPDATED_IS_FRAUD
        defaultMissedTransactionOperationShouldBeFound("isFraud.in=" + DEFAULT_IS_FRAUD + "," + UPDATED_IS_FRAUD);

        // Get all the missedTransactionOperationList where isFraud equals to UPDATED_IS_FRAUD
        defaultMissedTransactionOperationShouldNotBeFound("isFraud.in=" + UPDATED_IS_FRAUD);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByIsFraudIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where isFraud is not null
        defaultMissedTransactionOperationShouldBeFound("isFraud.specified=true");

        // Get all the missedTransactionOperationList where isFraud is null
        defaultMissedTransactionOperationShouldNotBeFound("isFraud.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTaggedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where taggedAt equals to DEFAULT_TAGGED_AT
        defaultMissedTransactionOperationShouldBeFound("taggedAt.equals=" + DEFAULT_TAGGED_AT);

        // Get all the missedTransactionOperationList where taggedAt equals to UPDATED_TAGGED_AT
        defaultMissedTransactionOperationShouldNotBeFound("taggedAt.equals=" + UPDATED_TAGGED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTaggedAtIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where taggedAt not equals to DEFAULT_TAGGED_AT
        defaultMissedTransactionOperationShouldNotBeFound("taggedAt.notEquals=" + DEFAULT_TAGGED_AT);

        // Get all the missedTransactionOperationList where taggedAt not equals to UPDATED_TAGGED_AT
        defaultMissedTransactionOperationShouldBeFound("taggedAt.notEquals=" + UPDATED_TAGGED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTaggedAtIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where taggedAt in DEFAULT_TAGGED_AT or UPDATED_TAGGED_AT
        defaultMissedTransactionOperationShouldBeFound("taggedAt.in=" + DEFAULT_TAGGED_AT + "," + UPDATED_TAGGED_AT);

        // Get all the missedTransactionOperationList where taggedAt equals to UPDATED_TAGGED_AT
        defaultMissedTransactionOperationShouldNotBeFound("taggedAt.in=" + UPDATED_TAGGED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTaggedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where taggedAt is not null
        defaultMissedTransactionOperationShouldBeFound("taggedAt.specified=true");

        // Get all the missedTransactionOperationList where taggedAt is null
        defaultMissedTransactionOperationShouldNotBeFound("taggedAt.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByFraudSourceIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where fraudSource equals to DEFAULT_FRAUD_SOURCE
        defaultMissedTransactionOperationShouldBeFound("fraudSource.equals=" + DEFAULT_FRAUD_SOURCE);

        // Get all the missedTransactionOperationList where fraudSource equals to UPDATED_FRAUD_SOURCE
        defaultMissedTransactionOperationShouldNotBeFound("fraudSource.equals=" + UPDATED_FRAUD_SOURCE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByFraudSourceIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where fraudSource not equals to DEFAULT_FRAUD_SOURCE
        defaultMissedTransactionOperationShouldNotBeFound("fraudSource.notEquals=" + DEFAULT_FRAUD_SOURCE);

        // Get all the missedTransactionOperationList where fraudSource not equals to UPDATED_FRAUD_SOURCE
        defaultMissedTransactionOperationShouldBeFound("fraudSource.notEquals=" + UPDATED_FRAUD_SOURCE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByFraudSourceIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where fraudSource in DEFAULT_FRAUD_SOURCE or UPDATED_FRAUD_SOURCE
        defaultMissedTransactionOperationShouldBeFound("fraudSource.in=" + DEFAULT_FRAUD_SOURCE + "," + UPDATED_FRAUD_SOURCE);

        // Get all the missedTransactionOperationList where fraudSource equals to UPDATED_FRAUD_SOURCE
        defaultMissedTransactionOperationShouldNotBeFound("fraudSource.in=" + UPDATED_FRAUD_SOURCE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByFraudSourceIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where fraudSource is not null
        defaultMissedTransactionOperationShouldBeFound("fraudSource.specified=true");

        // Get all the missedTransactionOperationList where fraudSource is null
        defaultMissedTransactionOperationShouldNotBeFound("fraudSource.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByFraudSourceContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where fraudSource contains DEFAULT_FRAUD_SOURCE
        defaultMissedTransactionOperationShouldBeFound("fraudSource.contains=" + DEFAULT_FRAUD_SOURCE);

        // Get all the missedTransactionOperationList where fraudSource contains UPDATED_FRAUD_SOURCE
        defaultMissedTransactionOperationShouldNotBeFound("fraudSource.contains=" + UPDATED_FRAUD_SOURCE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByFraudSourceNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where fraudSource does not contain DEFAULT_FRAUD_SOURCE
        defaultMissedTransactionOperationShouldNotBeFound("fraudSource.doesNotContain=" + DEFAULT_FRAUD_SOURCE);

        // Get all the missedTransactionOperationList where fraudSource does not contain UPDATED_FRAUD_SOURCE
        defaultMissedTransactionOperationShouldBeFound("fraudSource.doesNotContain=" + UPDATED_FRAUD_SOURCE);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCommentIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where comment equals to DEFAULT_COMMENT
        defaultMissedTransactionOperationShouldBeFound("comment.equals=" + DEFAULT_COMMENT);

        // Get all the missedTransactionOperationList where comment equals to UPDATED_COMMENT
        defaultMissedTransactionOperationShouldNotBeFound("comment.equals=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCommentIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where comment not equals to DEFAULT_COMMENT
        defaultMissedTransactionOperationShouldNotBeFound("comment.notEquals=" + DEFAULT_COMMENT);

        // Get all the missedTransactionOperationList where comment not equals to UPDATED_COMMENT
        defaultMissedTransactionOperationShouldBeFound("comment.notEquals=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCommentIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where comment in DEFAULT_COMMENT or UPDATED_COMMENT
        defaultMissedTransactionOperationShouldBeFound("comment.in=" + DEFAULT_COMMENT + "," + UPDATED_COMMENT);

        // Get all the missedTransactionOperationList where comment equals to UPDATED_COMMENT
        defaultMissedTransactionOperationShouldNotBeFound("comment.in=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCommentIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where comment is not null
        defaultMissedTransactionOperationShouldBeFound("comment.specified=true");

        // Get all the missedTransactionOperationList where comment is null
        defaultMissedTransactionOperationShouldNotBeFound("comment.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCommentContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where comment contains DEFAULT_COMMENT
        defaultMissedTransactionOperationShouldBeFound("comment.contains=" + DEFAULT_COMMENT);

        // Get all the missedTransactionOperationList where comment contains UPDATED_COMMENT
        defaultMissedTransactionOperationShouldNotBeFound("comment.contains=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCommentNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where comment does not contain DEFAULT_COMMENT
        defaultMissedTransactionOperationShouldNotBeFound("comment.doesNotContain=" + DEFAULT_COMMENT);

        // Get all the missedTransactionOperationList where comment does not contain UPDATED_COMMENT
        defaultMissedTransactionOperationShouldBeFound("comment.doesNotContain=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByFalsePositiveDetectedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where falsePositiveDetectedAt equals to DEFAULT_FALSE_POSITIVE_DETECTED_AT
        defaultMissedTransactionOperationShouldBeFound("falsePositiveDetectedAt.equals=" + DEFAULT_FALSE_POSITIVE_DETECTED_AT);

        // Get all the missedTransactionOperationList where falsePositiveDetectedAt equals to UPDATED_FALSE_POSITIVE_DETECTED_AT
        defaultMissedTransactionOperationShouldNotBeFound("falsePositiveDetectedAt.equals=" + UPDATED_FALSE_POSITIVE_DETECTED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByFalsePositiveDetectedAtIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where falsePositiveDetectedAt not equals to DEFAULT_FALSE_POSITIVE_DETECTED_AT
        defaultMissedTransactionOperationShouldNotBeFound("falsePositiveDetectedAt.notEquals=" + DEFAULT_FALSE_POSITIVE_DETECTED_AT);

        // Get all the missedTransactionOperationList where falsePositiveDetectedAt not equals to UPDATED_FALSE_POSITIVE_DETECTED_AT
        defaultMissedTransactionOperationShouldBeFound("falsePositiveDetectedAt.notEquals=" + UPDATED_FALSE_POSITIVE_DETECTED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByFalsePositiveDetectedAtIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where falsePositiveDetectedAt in DEFAULT_FALSE_POSITIVE_DETECTED_AT or UPDATED_FALSE_POSITIVE_DETECTED_AT
        defaultMissedTransactionOperationShouldBeFound(
            "falsePositiveDetectedAt.in=" + DEFAULT_FALSE_POSITIVE_DETECTED_AT + "," + UPDATED_FALSE_POSITIVE_DETECTED_AT
        );

        // Get all the missedTransactionOperationList where falsePositiveDetectedAt equals to UPDATED_FALSE_POSITIVE_DETECTED_AT
        defaultMissedTransactionOperationShouldNotBeFound("falsePositiveDetectedAt.in=" + UPDATED_FALSE_POSITIVE_DETECTED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByFalsePositiveDetectedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where falsePositiveDetectedAt is not null
        defaultMissedTransactionOperationShouldBeFound("falsePositiveDetectedAt.specified=true");

        // Get all the missedTransactionOperationList where falsePositiveDetectedAt is null
        defaultMissedTransactionOperationShouldNotBeFound("falsePositiveDetectedAt.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTidIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where tid equals to DEFAULT_TID
        defaultMissedTransactionOperationShouldBeFound("tid.equals=" + DEFAULT_TID);

        // Get all the missedTransactionOperationList where tid equals to UPDATED_TID
        defaultMissedTransactionOperationShouldNotBeFound("tid.equals=" + UPDATED_TID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTidIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where tid not equals to DEFAULT_TID
        defaultMissedTransactionOperationShouldNotBeFound("tid.notEquals=" + DEFAULT_TID);

        // Get all the missedTransactionOperationList where tid not equals to UPDATED_TID
        defaultMissedTransactionOperationShouldBeFound("tid.notEquals=" + UPDATED_TID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTidIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where tid in DEFAULT_TID or UPDATED_TID
        defaultMissedTransactionOperationShouldBeFound("tid.in=" + DEFAULT_TID + "," + UPDATED_TID);

        // Get all the missedTransactionOperationList where tid equals to UPDATED_TID
        defaultMissedTransactionOperationShouldNotBeFound("tid.in=" + UPDATED_TID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTidIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where tid is not null
        defaultMissedTransactionOperationShouldBeFound("tid.specified=true");

        // Get all the missedTransactionOperationList where tid is null
        defaultMissedTransactionOperationShouldNotBeFound("tid.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTidContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where tid contains DEFAULT_TID
        defaultMissedTransactionOperationShouldBeFound("tid.contains=" + DEFAULT_TID);

        // Get all the missedTransactionOperationList where tid contains UPDATED_TID
        defaultMissedTransactionOperationShouldNotBeFound("tid.contains=" + UPDATED_TID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByTidNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where tid does not contain DEFAULT_TID
        defaultMissedTransactionOperationShouldNotBeFound("tid.doesNotContain=" + DEFAULT_TID);

        // Get all the missedTransactionOperationList where tid does not contain UPDATED_TID
        defaultMissedTransactionOperationShouldBeFound("tid.doesNotContain=" + UPDATED_TID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByParentMsisdnIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where parentMsisdn equals to DEFAULT_PARENT_MSISDN
        defaultMissedTransactionOperationShouldBeFound("parentMsisdn.equals=" + DEFAULT_PARENT_MSISDN);

        // Get all the missedTransactionOperationList where parentMsisdn equals to UPDATED_PARENT_MSISDN
        defaultMissedTransactionOperationShouldNotBeFound("parentMsisdn.equals=" + UPDATED_PARENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByParentMsisdnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where parentMsisdn not equals to DEFAULT_PARENT_MSISDN
        defaultMissedTransactionOperationShouldNotBeFound("parentMsisdn.notEquals=" + DEFAULT_PARENT_MSISDN);

        // Get all the missedTransactionOperationList where parentMsisdn not equals to UPDATED_PARENT_MSISDN
        defaultMissedTransactionOperationShouldBeFound("parentMsisdn.notEquals=" + UPDATED_PARENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByParentMsisdnIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where parentMsisdn in DEFAULT_PARENT_MSISDN or UPDATED_PARENT_MSISDN
        defaultMissedTransactionOperationShouldBeFound("parentMsisdn.in=" + DEFAULT_PARENT_MSISDN + "," + UPDATED_PARENT_MSISDN);

        // Get all the missedTransactionOperationList where parentMsisdn equals to UPDATED_PARENT_MSISDN
        defaultMissedTransactionOperationShouldNotBeFound("parentMsisdn.in=" + UPDATED_PARENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByParentMsisdnIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where parentMsisdn is not null
        defaultMissedTransactionOperationShouldBeFound("parentMsisdn.specified=true");

        // Get all the missedTransactionOperationList where parentMsisdn is null
        defaultMissedTransactionOperationShouldNotBeFound("parentMsisdn.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByParentMsisdnContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where parentMsisdn contains DEFAULT_PARENT_MSISDN
        defaultMissedTransactionOperationShouldBeFound("parentMsisdn.contains=" + DEFAULT_PARENT_MSISDN);

        // Get all the missedTransactionOperationList where parentMsisdn contains UPDATED_PARENT_MSISDN
        defaultMissedTransactionOperationShouldNotBeFound("parentMsisdn.contains=" + UPDATED_PARENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByParentMsisdnNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where parentMsisdn does not contain DEFAULT_PARENT_MSISDN
        defaultMissedTransactionOperationShouldNotBeFound("parentMsisdn.doesNotContain=" + DEFAULT_PARENT_MSISDN);

        // Get all the missedTransactionOperationList where parentMsisdn does not contain UPDATED_PARENT_MSISDN
        defaultMissedTransactionOperationShouldBeFound("parentMsisdn.doesNotContain=" + UPDATED_PARENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByFctDtIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where fctDt equals to DEFAULT_FCT_DT
        defaultMissedTransactionOperationShouldBeFound("fctDt.equals=" + DEFAULT_FCT_DT);

        // Get all the missedTransactionOperationList where fctDt equals to UPDATED_FCT_DT
        defaultMissedTransactionOperationShouldNotBeFound("fctDt.equals=" + UPDATED_FCT_DT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByFctDtIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where fctDt not equals to DEFAULT_FCT_DT
        defaultMissedTransactionOperationShouldNotBeFound("fctDt.notEquals=" + DEFAULT_FCT_DT);

        // Get all the missedTransactionOperationList where fctDt not equals to UPDATED_FCT_DT
        defaultMissedTransactionOperationShouldBeFound("fctDt.notEquals=" + UPDATED_FCT_DT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByFctDtIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where fctDt in DEFAULT_FCT_DT or UPDATED_FCT_DT
        defaultMissedTransactionOperationShouldBeFound("fctDt.in=" + DEFAULT_FCT_DT + "," + UPDATED_FCT_DT);

        // Get all the missedTransactionOperationList where fctDt equals to UPDATED_FCT_DT
        defaultMissedTransactionOperationShouldNotBeFound("fctDt.in=" + UPDATED_FCT_DT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByFctDtIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where fctDt is not null
        defaultMissedTransactionOperationShouldBeFound("fctDt.specified=true");

        // Get all the missedTransactionOperationList where fctDt is null
        defaultMissedTransactionOperationShouldNotBeFound("fctDt.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByFctDtContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where fctDt contains DEFAULT_FCT_DT
        defaultMissedTransactionOperationShouldBeFound("fctDt.contains=" + DEFAULT_FCT_DT);

        // Get all the missedTransactionOperationList where fctDt contains UPDATED_FCT_DT
        defaultMissedTransactionOperationShouldNotBeFound("fctDt.contains=" + UPDATED_FCT_DT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByFctDtNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where fctDt does not contain DEFAULT_FCT_DT
        defaultMissedTransactionOperationShouldNotBeFound("fctDt.doesNotContain=" + DEFAULT_FCT_DT);

        // Get all the missedTransactionOperationList where fctDt does not contain UPDATED_FCT_DT
        defaultMissedTransactionOperationShouldBeFound("fctDt.doesNotContain=" + UPDATED_FCT_DT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByParentIdIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where parentId equals to DEFAULT_PARENT_ID
        defaultMissedTransactionOperationShouldBeFound("parentId.equals=" + DEFAULT_PARENT_ID);

        // Get all the missedTransactionOperationList where parentId equals to UPDATED_PARENT_ID
        defaultMissedTransactionOperationShouldNotBeFound("parentId.equals=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByParentIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where parentId not equals to DEFAULT_PARENT_ID
        defaultMissedTransactionOperationShouldNotBeFound("parentId.notEquals=" + DEFAULT_PARENT_ID);

        // Get all the missedTransactionOperationList where parentId not equals to UPDATED_PARENT_ID
        defaultMissedTransactionOperationShouldBeFound("parentId.notEquals=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByParentIdIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where parentId in DEFAULT_PARENT_ID or UPDATED_PARENT_ID
        defaultMissedTransactionOperationShouldBeFound("parentId.in=" + DEFAULT_PARENT_ID + "," + UPDATED_PARENT_ID);

        // Get all the missedTransactionOperationList where parentId equals to UPDATED_PARENT_ID
        defaultMissedTransactionOperationShouldNotBeFound("parentId.in=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByParentIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where parentId is not null
        defaultMissedTransactionOperationShouldBeFound("parentId.specified=true");

        // Get all the missedTransactionOperationList where parentId is null
        defaultMissedTransactionOperationShouldNotBeFound("parentId.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByParentIdContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where parentId contains DEFAULT_PARENT_ID
        defaultMissedTransactionOperationShouldBeFound("parentId.contains=" + DEFAULT_PARENT_ID);

        // Get all the missedTransactionOperationList where parentId contains UPDATED_PARENT_ID
        defaultMissedTransactionOperationShouldNotBeFound("parentId.contains=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByParentIdNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where parentId does not contain DEFAULT_PARENT_ID
        defaultMissedTransactionOperationShouldNotBeFound("parentId.doesNotContain=" + DEFAULT_PARENT_ID);

        // Get all the missedTransactionOperationList where parentId does not contain UPDATED_PARENT_ID
        defaultMissedTransactionOperationShouldBeFound("parentId.doesNotContain=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCanceledAtIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where canceledAt equals to DEFAULT_CANCELED_AT
        defaultMissedTransactionOperationShouldBeFound("canceledAt.equals=" + DEFAULT_CANCELED_AT);

        // Get all the missedTransactionOperationList where canceledAt equals to UPDATED_CANCELED_AT
        defaultMissedTransactionOperationShouldNotBeFound("canceledAt.equals=" + UPDATED_CANCELED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCanceledAtIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where canceledAt not equals to DEFAULT_CANCELED_AT
        defaultMissedTransactionOperationShouldNotBeFound("canceledAt.notEquals=" + DEFAULT_CANCELED_AT);

        // Get all the missedTransactionOperationList where canceledAt not equals to UPDATED_CANCELED_AT
        defaultMissedTransactionOperationShouldBeFound("canceledAt.notEquals=" + UPDATED_CANCELED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCanceledAtIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where canceledAt in DEFAULT_CANCELED_AT or UPDATED_CANCELED_AT
        defaultMissedTransactionOperationShouldBeFound("canceledAt.in=" + DEFAULT_CANCELED_AT + "," + UPDATED_CANCELED_AT);

        // Get all the missedTransactionOperationList where canceledAt equals to UPDATED_CANCELED_AT
        defaultMissedTransactionOperationShouldNotBeFound("canceledAt.in=" + UPDATED_CANCELED_AT);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCanceledAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where canceledAt is not null
        defaultMissedTransactionOperationShouldBeFound("canceledAt.specified=true");

        // Get all the missedTransactionOperationList where canceledAt is null
        defaultMissedTransactionOperationShouldNotBeFound("canceledAt.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCanceledIdIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where canceledId equals to DEFAULT_CANCELED_ID
        defaultMissedTransactionOperationShouldBeFound("canceledId.equals=" + DEFAULT_CANCELED_ID);

        // Get all the missedTransactionOperationList where canceledId equals to UPDATED_CANCELED_ID
        defaultMissedTransactionOperationShouldNotBeFound("canceledId.equals=" + UPDATED_CANCELED_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCanceledIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where canceledId not equals to DEFAULT_CANCELED_ID
        defaultMissedTransactionOperationShouldNotBeFound("canceledId.notEquals=" + DEFAULT_CANCELED_ID);

        // Get all the missedTransactionOperationList where canceledId not equals to UPDATED_CANCELED_ID
        defaultMissedTransactionOperationShouldBeFound("canceledId.notEquals=" + UPDATED_CANCELED_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCanceledIdIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where canceledId in DEFAULT_CANCELED_ID or UPDATED_CANCELED_ID
        defaultMissedTransactionOperationShouldBeFound("canceledId.in=" + DEFAULT_CANCELED_ID + "," + UPDATED_CANCELED_ID);

        // Get all the missedTransactionOperationList where canceledId equals to UPDATED_CANCELED_ID
        defaultMissedTransactionOperationShouldNotBeFound("canceledId.in=" + UPDATED_CANCELED_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCanceledIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where canceledId is not null
        defaultMissedTransactionOperationShouldBeFound("canceledId.specified=true");

        // Get all the missedTransactionOperationList where canceledId is null
        defaultMissedTransactionOperationShouldNotBeFound("canceledId.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCanceledIdContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where canceledId contains DEFAULT_CANCELED_ID
        defaultMissedTransactionOperationShouldBeFound("canceledId.contains=" + DEFAULT_CANCELED_ID);

        // Get all the missedTransactionOperationList where canceledId contains UPDATED_CANCELED_ID
        defaultMissedTransactionOperationShouldNotBeFound("canceledId.contains=" + UPDATED_CANCELED_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByCanceledIdNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where canceledId does not contain DEFAULT_CANCELED_ID
        defaultMissedTransactionOperationShouldNotBeFound("canceledId.doesNotContain=" + DEFAULT_CANCELED_ID);

        // Get all the missedTransactionOperationList where canceledId does not contain UPDATED_CANCELED_ID
        defaultMissedTransactionOperationShouldBeFound("canceledId.doesNotContain=" + UPDATED_CANCELED_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByProductIdIsEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where productId equals to DEFAULT_PRODUCT_ID
        defaultMissedTransactionOperationShouldBeFound("productId.equals=" + DEFAULT_PRODUCT_ID);

        // Get all the missedTransactionOperationList where productId equals to UPDATED_PRODUCT_ID
        defaultMissedTransactionOperationShouldNotBeFound("productId.equals=" + UPDATED_PRODUCT_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByProductIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where productId not equals to DEFAULT_PRODUCT_ID
        defaultMissedTransactionOperationShouldNotBeFound("productId.notEquals=" + DEFAULT_PRODUCT_ID);

        // Get all the missedTransactionOperationList where productId not equals to UPDATED_PRODUCT_ID
        defaultMissedTransactionOperationShouldBeFound("productId.notEquals=" + UPDATED_PRODUCT_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByProductIdIsInShouldWork() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where productId in DEFAULT_PRODUCT_ID or UPDATED_PRODUCT_ID
        defaultMissedTransactionOperationShouldBeFound("productId.in=" + DEFAULT_PRODUCT_ID + "," + UPDATED_PRODUCT_ID);

        // Get all the missedTransactionOperationList where productId equals to UPDATED_PRODUCT_ID
        defaultMissedTransactionOperationShouldNotBeFound("productId.in=" + UPDATED_PRODUCT_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByProductIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where productId is not null
        defaultMissedTransactionOperationShouldBeFound("productId.specified=true");

        // Get all the missedTransactionOperationList where productId is null
        defaultMissedTransactionOperationShouldNotBeFound("productId.specified=false");
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByProductIdContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where productId contains DEFAULT_PRODUCT_ID
        defaultMissedTransactionOperationShouldBeFound("productId.contains=" + DEFAULT_PRODUCT_ID);

        // Get all the missedTransactionOperationList where productId contains UPDATED_PRODUCT_ID
        defaultMissedTransactionOperationShouldNotBeFound("productId.contains=" + UPDATED_PRODUCT_ID);
    }

    @Test
    @Transactional
    void getAllMissedTransactionOperationsByProductIdNotContainsSomething() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        // Get all the missedTransactionOperationList where productId does not contain DEFAULT_PRODUCT_ID
        defaultMissedTransactionOperationShouldNotBeFound("productId.doesNotContain=" + DEFAULT_PRODUCT_ID);

        // Get all the missedTransactionOperationList where productId does not contain UPDATED_PRODUCT_ID
        defaultMissedTransactionOperationShouldBeFound("productId.doesNotContain=" + UPDATED_PRODUCT_ID);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultMissedTransactionOperationShouldBeFound(String filter) throws Exception {
        restMissedTransactionOperationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(missedTransactionOperation.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].subsMsisdn").value(hasItem(DEFAULT_SUBS_MSISDN)))
            .andExpect(jsonPath("$.[*].agentMsisdn").value(hasItem(DEFAULT_AGENT_MSISDN)))
            .andExpect(jsonPath("$.[*].typeTransaction").value(hasItem(DEFAULT_TYPE_TRANSACTION)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].transactionStatus").value(hasItem(DEFAULT_TRANSACTION_STATUS)))
            .andExpect(jsonPath("$.[*].senderZone").value(hasItem(DEFAULT_SENDER_ZONE)))
            .andExpect(jsonPath("$.[*].senderProfile").value(hasItem(DEFAULT_SENDER_PROFILE)))
            .andExpect(jsonPath("$.[*].codeTerritory").value(hasItem(DEFAULT_CODE_TERRITORY)))
            .andExpect(jsonPath("$.[*].subType").value(hasItem(DEFAULT_SUB_TYPE)))
            .andExpect(jsonPath("$.[*].operationDate").value(hasItem(DEFAULT_OPERATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].isFraud").value(hasItem(DEFAULT_IS_FRAUD.booleanValue())))
            .andExpect(jsonPath("$.[*].taggedAt").value(hasItem(DEFAULT_TAGGED_AT.toString())))
            .andExpect(jsonPath("$.[*].fraudSource").value(hasItem(DEFAULT_FRAUD_SOURCE)))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT)))
            .andExpect(jsonPath("$.[*].falsePositiveDetectedAt").value(hasItem(DEFAULT_FALSE_POSITIVE_DETECTED_AT.toString())))
            .andExpect(jsonPath("$.[*].tid").value(hasItem(DEFAULT_TID)))
            .andExpect(jsonPath("$.[*].parentMsisdn").value(hasItem(DEFAULT_PARENT_MSISDN)))
            .andExpect(jsonPath("$.[*].fctDt").value(hasItem(DEFAULT_FCT_DT)))
            .andExpect(jsonPath("$.[*].parentId").value(hasItem(DEFAULT_PARENT_ID)))
            .andExpect(jsonPath("$.[*].canceledAt").value(hasItem(DEFAULT_CANCELED_AT.toString())))
            .andExpect(jsonPath("$.[*].canceledId").value(hasItem(DEFAULT_CANCELED_ID)))
            .andExpect(jsonPath("$.[*].productId").value(hasItem(DEFAULT_PRODUCT_ID)));

        // Check, that the count call also returns 1
        restMissedTransactionOperationMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultMissedTransactionOperationShouldNotBeFound(String filter) throws Exception {
        restMissedTransactionOperationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restMissedTransactionOperationMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingMissedTransactionOperation() throws Exception {
        // Get the missedTransactionOperation
        restMissedTransactionOperationMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMissedTransactionOperation() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        int databaseSizeBeforeUpdate = missedTransactionOperationRepository.findAll().size();

        // Update the missedTransactionOperation
        MissedTransactionOperation updatedMissedTransactionOperation = missedTransactionOperationRepository
            .findById(missedTransactionOperation.getId())
            .get();
        // Disconnect from session so that the updates on updatedMissedTransactionOperation are not directly saved in db
        em.detach(updatedMissedTransactionOperation);
        updatedMissedTransactionOperation
            .amount(UPDATED_AMOUNT)
            .subsMsisdn(UPDATED_SUBS_MSISDN)
            .agentMsisdn(UPDATED_AGENT_MSISDN)
            .typeTransaction(UPDATED_TYPE_TRANSACTION)
            .createdAt(UPDATED_CREATED_AT)
            .transactionStatus(UPDATED_TRANSACTION_STATUS)
            .senderZone(UPDATED_SENDER_ZONE)
            .senderProfile(UPDATED_SENDER_PROFILE)
            .codeTerritory(UPDATED_CODE_TERRITORY)
            .subType(UPDATED_SUB_TYPE)
            .operationDate(UPDATED_OPERATION_DATE)
            .isFraud(UPDATED_IS_FRAUD)
            .taggedAt(UPDATED_TAGGED_AT)
            .fraudSource(UPDATED_FRAUD_SOURCE)
            .comment(UPDATED_COMMENT)
            .falsePositiveDetectedAt(UPDATED_FALSE_POSITIVE_DETECTED_AT)
            .tid(UPDATED_TID)
            .parentMsisdn(UPDATED_PARENT_MSISDN)
            .fctDt(UPDATED_FCT_DT)
            .parentId(UPDATED_PARENT_ID)
            .canceledAt(UPDATED_CANCELED_AT)
            .canceledId(UPDATED_CANCELED_ID)
            .productId(UPDATED_PRODUCT_ID);
        MissedTransactionOperationDTO missedTransactionOperationDTO = missedTransactionOperationMapper.toDto(
            updatedMissedTransactionOperation
        );

        restMissedTransactionOperationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, missedTransactionOperationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(missedTransactionOperationDTO))
            )
            .andExpect(status().isOk());

        // Validate the MissedTransactionOperation in the database
        List<MissedTransactionOperation> missedTransactionOperationList = missedTransactionOperationRepository.findAll();
        assertThat(missedTransactionOperationList).hasSize(databaseSizeBeforeUpdate);
        MissedTransactionOperation testMissedTransactionOperation = missedTransactionOperationList.get(
            missedTransactionOperationList.size() - 1
        );
        assertThat(testMissedTransactionOperation.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testMissedTransactionOperation.getSubsMsisdn()).isEqualTo(UPDATED_SUBS_MSISDN);
        assertThat(testMissedTransactionOperation.getAgentMsisdn()).isEqualTo(UPDATED_AGENT_MSISDN);
        assertThat(testMissedTransactionOperation.getTypeTransaction()).isEqualTo(UPDATED_TYPE_TRANSACTION);
        assertThat(testMissedTransactionOperation.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testMissedTransactionOperation.getTransactionStatus()).isEqualTo(UPDATED_TRANSACTION_STATUS);
        assertThat(testMissedTransactionOperation.getSenderZone()).isEqualTo(UPDATED_SENDER_ZONE);
        assertThat(testMissedTransactionOperation.getSenderProfile()).isEqualTo(UPDATED_SENDER_PROFILE);
        assertThat(testMissedTransactionOperation.getCodeTerritory()).isEqualTo(UPDATED_CODE_TERRITORY);
        assertThat(testMissedTransactionOperation.getSubType()).isEqualTo(UPDATED_SUB_TYPE);
        assertThat(testMissedTransactionOperation.getOperationDate()).isEqualTo(UPDATED_OPERATION_DATE);
        assertThat(testMissedTransactionOperation.getIsFraud()).isEqualTo(UPDATED_IS_FRAUD);
        assertThat(testMissedTransactionOperation.getTaggedAt()).isEqualTo(UPDATED_TAGGED_AT);
        assertThat(testMissedTransactionOperation.getFraudSource()).isEqualTo(UPDATED_FRAUD_SOURCE);
        assertThat(testMissedTransactionOperation.getComment()).isEqualTo(UPDATED_COMMENT);
        assertThat(testMissedTransactionOperation.getFalsePositiveDetectedAt()).isEqualTo(UPDATED_FALSE_POSITIVE_DETECTED_AT);
        assertThat(testMissedTransactionOperation.getTid()).isEqualTo(UPDATED_TID);
        assertThat(testMissedTransactionOperation.getParentMsisdn()).isEqualTo(UPDATED_PARENT_MSISDN);
        assertThat(testMissedTransactionOperation.getFctDt()).isEqualTo(UPDATED_FCT_DT);
        assertThat(testMissedTransactionOperation.getParentId()).isEqualTo(UPDATED_PARENT_ID);
        assertThat(testMissedTransactionOperation.getCanceledAt()).isEqualTo(UPDATED_CANCELED_AT);
        assertThat(testMissedTransactionOperation.getCanceledId()).isEqualTo(UPDATED_CANCELED_ID);
        assertThat(testMissedTransactionOperation.getProductId()).isEqualTo(UPDATED_PRODUCT_ID);
    }

    @Test
    @Transactional
    void putNonExistingMissedTransactionOperation() throws Exception {
        int databaseSizeBeforeUpdate = missedTransactionOperationRepository.findAll().size();
        missedTransactionOperation.setId(count.incrementAndGet());

        // Create the MissedTransactionOperation
        MissedTransactionOperationDTO missedTransactionOperationDTO = missedTransactionOperationMapper.toDto(missedTransactionOperation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMissedTransactionOperationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, missedTransactionOperationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(missedTransactionOperationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MissedTransactionOperation in the database
        List<MissedTransactionOperation> missedTransactionOperationList = missedTransactionOperationRepository.findAll();
        assertThat(missedTransactionOperationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMissedTransactionOperation() throws Exception {
        int databaseSizeBeforeUpdate = missedTransactionOperationRepository.findAll().size();
        missedTransactionOperation.setId(count.incrementAndGet());

        // Create the MissedTransactionOperation
        MissedTransactionOperationDTO missedTransactionOperationDTO = missedTransactionOperationMapper.toDto(missedTransactionOperation);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMissedTransactionOperationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(missedTransactionOperationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MissedTransactionOperation in the database
        List<MissedTransactionOperation> missedTransactionOperationList = missedTransactionOperationRepository.findAll();
        assertThat(missedTransactionOperationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMissedTransactionOperation() throws Exception {
        int databaseSizeBeforeUpdate = missedTransactionOperationRepository.findAll().size();
        missedTransactionOperation.setId(count.incrementAndGet());

        // Create the MissedTransactionOperation
        MissedTransactionOperationDTO missedTransactionOperationDTO = missedTransactionOperationMapper.toDto(missedTransactionOperation);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMissedTransactionOperationMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(missedTransactionOperationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MissedTransactionOperation in the database
        List<MissedTransactionOperation> missedTransactionOperationList = missedTransactionOperationRepository.findAll();
        assertThat(missedTransactionOperationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMissedTransactionOperationWithPatch() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        int databaseSizeBeforeUpdate = missedTransactionOperationRepository.findAll().size();

        // Update the missedTransactionOperation using partial update
        MissedTransactionOperation partialUpdatedMissedTransactionOperation = new MissedTransactionOperation();
        partialUpdatedMissedTransactionOperation.setId(missedTransactionOperation.getId());

        partialUpdatedMissedTransactionOperation
            .subsMsisdn(UPDATED_SUBS_MSISDN)
            .agentMsisdn(UPDATED_AGENT_MSISDN)
            .typeTransaction(UPDATED_TYPE_TRANSACTION)
            .createdAt(UPDATED_CREATED_AT)
            .transactionStatus(UPDATED_TRANSACTION_STATUS)
            .senderProfile(UPDATED_SENDER_PROFILE)
            .codeTerritory(UPDATED_CODE_TERRITORY)
            .isFraud(UPDATED_IS_FRAUD)
            .tid(UPDATED_TID)
            .fctDt(UPDATED_FCT_DT)
            .parentId(UPDATED_PARENT_ID)
            .canceledId(UPDATED_CANCELED_ID)
            .productId(UPDATED_PRODUCT_ID);

        restMissedTransactionOperationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMissedTransactionOperation.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMissedTransactionOperation))
            )
            .andExpect(status().isOk());

        // Validate the MissedTransactionOperation in the database
        List<MissedTransactionOperation> missedTransactionOperationList = missedTransactionOperationRepository.findAll();
        assertThat(missedTransactionOperationList).hasSize(databaseSizeBeforeUpdate);
        MissedTransactionOperation testMissedTransactionOperation = missedTransactionOperationList.get(
            missedTransactionOperationList.size() - 1
        );
        assertThat(testMissedTransactionOperation.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testMissedTransactionOperation.getSubsMsisdn()).isEqualTo(UPDATED_SUBS_MSISDN);
        assertThat(testMissedTransactionOperation.getAgentMsisdn()).isEqualTo(UPDATED_AGENT_MSISDN);
        assertThat(testMissedTransactionOperation.getTypeTransaction()).isEqualTo(UPDATED_TYPE_TRANSACTION);
        assertThat(testMissedTransactionOperation.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testMissedTransactionOperation.getTransactionStatus()).isEqualTo(UPDATED_TRANSACTION_STATUS);
        assertThat(testMissedTransactionOperation.getSenderZone()).isEqualTo(DEFAULT_SENDER_ZONE);
        assertThat(testMissedTransactionOperation.getSenderProfile()).isEqualTo(UPDATED_SENDER_PROFILE);
        assertThat(testMissedTransactionOperation.getCodeTerritory()).isEqualTo(UPDATED_CODE_TERRITORY);
        assertThat(testMissedTransactionOperation.getSubType()).isEqualTo(DEFAULT_SUB_TYPE);
        assertThat(testMissedTransactionOperation.getOperationDate()).isEqualTo(DEFAULT_OPERATION_DATE);
        assertThat(testMissedTransactionOperation.getIsFraud()).isEqualTo(UPDATED_IS_FRAUD);
        assertThat(testMissedTransactionOperation.getTaggedAt()).isEqualTo(DEFAULT_TAGGED_AT);
        assertThat(testMissedTransactionOperation.getFraudSource()).isEqualTo(DEFAULT_FRAUD_SOURCE);
        assertThat(testMissedTransactionOperation.getComment()).isEqualTo(DEFAULT_COMMENT);
        assertThat(testMissedTransactionOperation.getFalsePositiveDetectedAt()).isEqualTo(DEFAULT_FALSE_POSITIVE_DETECTED_AT);
        assertThat(testMissedTransactionOperation.getTid()).isEqualTo(UPDATED_TID);
        assertThat(testMissedTransactionOperation.getParentMsisdn()).isEqualTo(DEFAULT_PARENT_MSISDN);
        assertThat(testMissedTransactionOperation.getFctDt()).isEqualTo(UPDATED_FCT_DT);
        assertThat(testMissedTransactionOperation.getParentId()).isEqualTo(UPDATED_PARENT_ID);
        assertThat(testMissedTransactionOperation.getCanceledAt()).isEqualTo(DEFAULT_CANCELED_AT);
        assertThat(testMissedTransactionOperation.getCanceledId()).isEqualTo(UPDATED_CANCELED_ID);
        assertThat(testMissedTransactionOperation.getProductId()).isEqualTo(UPDATED_PRODUCT_ID);
    }

    @Test
    @Transactional
    void fullUpdateMissedTransactionOperationWithPatch() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        int databaseSizeBeforeUpdate = missedTransactionOperationRepository.findAll().size();

        // Update the missedTransactionOperation using partial update
        MissedTransactionOperation partialUpdatedMissedTransactionOperation = new MissedTransactionOperation();
        partialUpdatedMissedTransactionOperation.setId(missedTransactionOperation.getId());

        partialUpdatedMissedTransactionOperation
            .amount(UPDATED_AMOUNT)
            .subsMsisdn(UPDATED_SUBS_MSISDN)
            .agentMsisdn(UPDATED_AGENT_MSISDN)
            .typeTransaction(UPDATED_TYPE_TRANSACTION)
            .createdAt(UPDATED_CREATED_AT)
            .transactionStatus(UPDATED_TRANSACTION_STATUS)
            .senderZone(UPDATED_SENDER_ZONE)
            .senderProfile(UPDATED_SENDER_PROFILE)
            .codeTerritory(UPDATED_CODE_TERRITORY)
            .subType(UPDATED_SUB_TYPE)
            .operationDate(UPDATED_OPERATION_DATE)
            .isFraud(UPDATED_IS_FRAUD)
            .taggedAt(UPDATED_TAGGED_AT)
            .fraudSource(UPDATED_FRAUD_SOURCE)
            .comment(UPDATED_COMMENT)
            .falsePositiveDetectedAt(UPDATED_FALSE_POSITIVE_DETECTED_AT)
            .tid(UPDATED_TID)
            .parentMsisdn(UPDATED_PARENT_MSISDN)
            .fctDt(UPDATED_FCT_DT)
            .parentId(UPDATED_PARENT_ID)
            .canceledAt(UPDATED_CANCELED_AT)
            .canceledId(UPDATED_CANCELED_ID)
            .productId(UPDATED_PRODUCT_ID);

        restMissedTransactionOperationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMissedTransactionOperation.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMissedTransactionOperation))
            )
            .andExpect(status().isOk());

        // Validate the MissedTransactionOperation in the database
        List<MissedTransactionOperation> missedTransactionOperationList = missedTransactionOperationRepository.findAll();
        assertThat(missedTransactionOperationList).hasSize(databaseSizeBeforeUpdate);
        MissedTransactionOperation testMissedTransactionOperation = missedTransactionOperationList.get(
            missedTransactionOperationList.size() - 1
        );
        assertThat(testMissedTransactionOperation.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testMissedTransactionOperation.getSubsMsisdn()).isEqualTo(UPDATED_SUBS_MSISDN);
        assertThat(testMissedTransactionOperation.getAgentMsisdn()).isEqualTo(UPDATED_AGENT_MSISDN);
        assertThat(testMissedTransactionOperation.getTypeTransaction()).isEqualTo(UPDATED_TYPE_TRANSACTION);
        assertThat(testMissedTransactionOperation.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testMissedTransactionOperation.getTransactionStatus()).isEqualTo(UPDATED_TRANSACTION_STATUS);
        assertThat(testMissedTransactionOperation.getSenderZone()).isEqualTo(UPDATED_SENDER_ZONE);
        assertThat(testMissedTransactionOperation.getSenderProfile()).isEqualTo(UPDATED_SENDER_PROFILE);
        assertThat(testMissedTransactionOperation.getCodeTerritory()).isEqualTo(UPDATED_CODE_TERRITORY);
        assertThat(testMissedTransactionOperation.getSubType()).isEqualTo(UPDATED_SUB_TYPE);
        assertThat(testMissedTransactionOperation.getOperationDate()).isEqualTo(UPDATED_OPERATION_DATE);
        assertThat(testMissedTransactionOperation.getIsFraud()).isEqualTo(UPDATED_IS_FRAUD);
        assertThat(testMissedTransactionOperation.getTaggedAt()).isEqualTo(UPDATED_TAGGED_AT);
        assertThat(testMissedTransactionOperation.getFraudSource()).isEqualTo(UPDATED_FRAUD_SOURCE);
        assertThat(testMissedTransactionOperation.getComment()).isEqualTo(UPDATED_COMMENT);
        assertThat(testMissedTransactionOperation.getFalsePositiveDetectedAt()).isEqualTo(UPDATED_FALSE_POSITIVE_DETECTED_AT);
        assertThat(testMissedTransactionOperation.getTid()).isEqualTo(UPDATED_TID);
        assertThat(testMissedTransactionOperation.getParentMsisdn()).isEqualTo(UPDATED_PARENT_MSISDN);
        assertThat(testMissedTransactionOperation.getFctDt()).isEqualTo(UPDATED_FCT_DT);
        assertThat(testMissedTransactionOperation.getParentId()).isEqualTo(UPDATED_PARENT_ID);
        assertThat(testMissedTransactionOperation.getCanceledAt()).isEqualTo(UPDATED_CANCELED_AT);
        assertThat(testMissedTransactionOperation.getCanceledId()).isEqualTo(UPDATED_CANCELED_ID);
        assertThat(testMissedTransactionOperation.getProductId()).isEqualTo(UPDATED_PRODUCT_ID);
    }

    @Test
    @Transactional
    void patchNonExistingMissedTransactionOperation() throws Exception {
        int databaseSizeBeforeUpdate = missedTransactionOperationRepository.findAll().size();
        missedTransactionOperation.setId(count.incrementAndGet());

        // Create the MissedTransactionOperation
        MissedTransactionOperationDTO missedTransactionOperationDTO = missedTransactionOperationMapper.toDto(missedTransactionOperation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMissedTransactionOperationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, missedTransactionOperationDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(missedTransactionOperationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MissedTransactionOperation in the database
        List<MissedTransactionOperation> missedTransactionOperationList = missedTransactionOperationRepository.findAll();
        assertThat(missedTransactionOperationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMissedTransactionOperation() throws Exception {
        int databaseSizeBeforeUpdate = missedTransactionOperationRepository.findAll().size();
        missedTransactionOperation.setId(count.incrementAndGet());

        // Create the MissedTransactionOperation
        MissedTransactionOperationDTO missedTransactionOperationDTO = missedTransactionOperationMapper.toDto(missedTransactionOperation);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMissedTransactionOperationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(missedTransactionOperationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the MissedTransactionOperation in the database
        List<MissedTransactionOperation> missedTransactionOperationList = missedTransactionOperationRepository.findAll();
        assertThat(missedTransactionOperationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMissedTransactionOperation() throws Exception {
        int databaseSizeBeforeUpdate = missedTransactionOperationRepository.findAll().size();
        missedTransactionOperation.setId(count.incrementAndGet());

        // Create the MissedTransactionOperation
        MissedTransactionOperationDTO missedTransactionOperationDTO = missedTransactionOperationMapper.toDto(missedTransactionOperation);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMissedTransactionOperationMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(missedTransactionOperationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MissedTransactionOperation in the database
        List<MissedTransactionOperation> missedTransactionOperationList = missedTransactionOperationRepository.findAll();
        assertThat(missedTransactionOperationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMissedTransactionOperation() throws Exception {
        // Initialize the database
        missedTransactionOperationRepository.saveAndFlush(missedTransactionOperation);

        int databaseSizeBeforeDelete = missedTransactionOperationRepository.findAll().size();

        // Delete the missedTransactionOperation
        restMissedTransactionOperationMockMvc
            .perform(delete(ENTITY_API_URL_ID, missedTransactionOperation.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MissedTransactionOperation> missedTransactionOperationList = missedTransactionOperationRepository.findAll();
        assertThat(missedTransactionOperationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
