package sn.free.commissioning.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.IntegrationTest;
import sn.free.commissioning.domain.CommissioningPlan;
import sn.free.commissioning.domain.ConfigurationPlan;
import sn.free.commissioning.domain.Frequency;
import sn.free.commissioning.domain.Period;
import sn.free.commissioning.domain.ServiceType;
import sn.free.commissioning.domain.enumeration.CommissioningPlanState;
import sn.free.commissioning.domain.enumeration.CommissioningPlanType;
import sn.free.commissioning.domain.enumeration.PaymentStrategy;
import sn.free.commissioning.repository.CommissioningPlanRepository;
import sn.free.commissioning.service.CommissioningPlanService;
import sn.free.commissioning.service.criteria.CommissioningPlanCriteria;
import sn.free.commissioning.service.dto.CommissioningPlanDTO;
import sn.free.commissioning.service.mapper.CommissioningPlanMapper;

/**
 * Integration tests for the {@link CommissioningPlanResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class CommissioningPlanResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Instant DEFAULT_BEGIN_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_BEGIN_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_END_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_END_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_CREATION_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATION_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_ARCHIVE_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_ARCHIVE_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_ARCHIVED_BY = "AAAAAAAAAA";
    private static final String UPDATED_ARCHIVED_BY = "BBBBBBBBBB";

    private static final PaymentStrategy DEFAULT_PAYMENT_STRATEGY = PaymentStrategy.AUTOMATIC;
    private static final PaymentStrategy UPDATED_PAYMENT_STRATEGY = PaymentStrategy.MANUAL;

    private static final CommissioningPlanState DEFAULT_STATE = CommissioningPlanState.CREATED;
    private static final CommissioningPlanState UPDATED_STATE = CommissioningPlanState.ARCHIVED;

    private static final CommissioningPlanType DEFAULT_COMMISSIONING_PLAN_TYPE = CommissioningPlanType.MFS;
    private static final CommissioningPlanType UPDATED_COMMISSIONING_PLAN_TYPE = CommissioningPlanType.MOBILE;

    private static final String DEFAULT_SPARE_1 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_2 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_3 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_3 = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/commissioning-plans";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CommissioningPlanRepository commissioningPlanRepository;

    @Mock
    private CommissioningPlanRepository commissioningPlanRepositoryMock;

    @Autowired
    private CommissioningPlanMapper commissioningPlanMapper;

    @Mock
    private CommissioningPlanService commissioningPlanServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCommissioningPlanMockMvc;

    private CommissioningPlan commissioningPlan;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CommissioningPlan createEntity(EntityManager em) {
        CommissioningPlan commissioningPlan = new CommissioningPlan()
            .name(DEFAULT_NAME)
            .beginDate(DEFAULT_BEGIN_DATE)
            .endDate(DEFAULT_END_DATE)
            .creationDate(DEFAULT_CREATION_DATE)
            .createdBy(DEFAULT_CREATED_BY)
            .archiveDate(DEFAULT_ARCHIVE_DATE)
            .archivedBy(DEFAULT_ARCHIVED_BY)
            .paymentStrategy(DEFAULT_PAYMENT_STRATEGY)
            .state(DEFAULT_STATE)
            .commissioningPlanType(DEFAULT_COMMISSIONING_PLAN_TYPE)
            .spare1(DEFAULT_SPARE_1)
            .spare2(DEFAULT_SPARE_2)
            .spare3(DEFAULT_SPARE_3);
        return commissioningPlan;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CommissioningPlan createUpdatedEntity(EntityManager em) {
        CommissioningPlan commissioningPlan = new CommissioningPlan()
            .name(UPDATED_NAME)
            .beginDate(UPDATED_BEGIN_DATE)
            .endDate(UPDATED_END_DATE)
            .creationDate(UPDATED_CREATION_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .archiveDate(UPDATED_ARCHIVE_DATE)
            .archivedBy(UPDATED_ARCHIVED_BY)
            .paymentStrategy(UPDATED_PAYMENT_STRATEGY)
            .state(UPDATED_STATE)
            .commissioningPlanType(UPDATED_COMMISSIONING_PLAN_TYPE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);
        return commissioningPlan;
    }

    @BeforeEach
    public void initTest() {
        commissioningPlan = createEntity(em);
    }

    @Test
    @Transactional
    void createCommissioningPlan() throws Exception {
        int databaseSizeBeforeCreate = commissioningPlanRepository.findAll().size();
        // Create the CommissioningPlan
        CommissioningPlanDTO commissioningPlanDTO = commissioningPlanMapper.toDto(commissioningPlan);
        restCommissioningPlanMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissioningPlanDTO))
            )
            .andExpect(status().isCreated());

        // Validate the CommissioningPlan in the database
        List<CommissioningPlan> commissioningPlanList = commissioningPlanRepository.findAll();
        assertThat(commissioningPlanList).hasSize(databaseSizeBeforeCreate + 1);
        CommissioningPlan testCommissioningPlan = commissioningPlanList.get(commissioningPlanList.size() - 1);
        assertThat(testCommissioningPlan.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCommissioningPlan.getBeginDate()).isEqualTo(DEFAULT_BEGIN_DATE);
        assertThat(testCommissioningPlan.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testCommissioningPlan.getCreationDate()).isEqualTo(DEFAULT_CREATION_DATE);
        assertThat(testCommissioningPlan.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testCommissioningPlan.getArchiveDate()).isEqualTo(DEFAULT_ARCHIVE_DATE);
        assertThat(testCommissioningPlan.getArchivedBy()).isEqualTo(DEFAULT_ARCHIVED_BY);
        assertThat(testCommissioningPlan.getPaymentStrategy()).isEqualTo(DEFAULT_PAYMENT_STRATEGY);
        assertThat(testCommissioningPlan.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testCommissioningPlan.getCommissioningPlanType()).isEqualTo(DEFAULT_COMMISSIONING_PLAN_TYPE);
        assertThat(testCommissioningPlan.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testCommissioningPlan.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
        assertThat(testCommissioningPlan.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
    }

    @Test
    @Transactional
    void createCommissioningPlanWithExistingId() throws Exception {
        // Create the CommissioningPlan with an existing ID
        commissioningPlan.setId(1L);
        CommissioningPlanDTO commissioningPlanDTO = commissioningPlanMapper.toDto(commissioningPlan);

        int databaseSizeBeforeCreate = commissioningPlanRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCommissioningPlanMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissioningPlanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommissioningPlan in the database
        List<CommissioningPlan> commissioningPlanList = commissioningPlanRepository.findAll();
        assertThat(commissioningPlanList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = commissioningPlanRepository.findAll().size();
        // set the field null
        commissioningPlan.setName(null);

        // Create the CommissioningPlan, which fails.
        CommissioningPlanDTO commissioningPlanDTO = commissioningPlanMapper.toDto(commissioningPlan);

        restCommissioningPlanMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissioningPlanDTO))
            )
            .andExpect(status().isBadRequest());

        List<CommissioningPlan> commissioningPlanList = commissioningPlanRepository.findAll();
        assertThat(commissioningPlanList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkBeginDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = commissioningPlanRepository.findAll().size();
        // set the field null
        commissioningPlan.setBeginDate(null);

        // Create the CommissioningPlan, which fails.
        CommissioningPlanDTO commissioningPlanDTO = commissioningPlanMapper.toDto(commissioningPlan);

        restCommissioningPlanMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissioningPlanDTO))
            )
            .andExpect(status().isBadRequest());

        List<CommissioningPlan> commissioningPlanList = commissioningPlanRepository.findAll();
        assertThat(commissioningPlanList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkEndDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = commissioningPlanRepository.findAll().size();
        // set the field null
        commissioningPlan.setEndDate(null);

        // Create the CommissioningPlan, which fails.
        CommissioningPlanDTO commissioningPlanDTO = commissioningPlanMapper.toDto(commissioningPlan);

        restCommissioningPlanMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissioningPlanDTO))
            )
            .andExpect(status().isBadRequest());

        List<CommissioningPlan> commissioningPlanList = commissioningPlanRepository.findAll();
        assertThat(commissioningPlanList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCreationDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = commissioningPlanRepository.findAll().size();
        // set the field null
        commissioningPlan.setCreationDate(null);

        // Create the CommissioningPlan, which fails.
        CommissioningPlanDTO commissioningPlanDTO = commissioningPlanMapper.toDto(commissioningPlan);

        restCommissioningPlanMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissioningPlanDTO))
            )
            .andExpect(status().isBadRequest());

        List<CommissioningPlan> commissioningPlanList = commissioningPlanRepository.findAll();
        assertThat(commissioningPlanList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCreatedByIsRequired() throws Exception {
        int databaseSizeBeforeTest = commissioningPlanRepository.findAll().size();
        // set the field null
        commissioningPlan.setCreatedBy(null);

        // Create the CommissioningPlan, which fails.
        CommissioningPlanDTO commissioningPlanDTO = commissioningPlanMapper.toDto(commissioningPlan);

        restCommissioningPlanMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissioningPlanDTO))
            )
            .andExpect(status().isBadRequest());

        List<CommissioningPlan> commissioningPlanList = commissioningPlanRepository.findAll();
        assertThat(commissioningPlanList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkStateIsRequired() throws Exception {
        int databaseSizeBeforeTest = commissioningPlanRepository.findAll().size();
        // set the field null
        commissioningPlan.setState(null);

        // Create the CommissioningPlan, which fails.
        CommissioningPlanDTO commissioningPlanDTO = commissioningPlanMapper.toDto(commissioningPlan);

        restCommissioningPlanMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissioningPlanDTO))
            )
            .andExpect(status().isBadRequest());

        List<CommissioningPlan> commissioningPlanList = commissioningPlanRepository.findAll();
        assertThat(commissioningPlanList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCommissioningPlans() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList
        restCommissioningPlanMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(commissioningPlan.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].beginDate").value(hasItem(DEFAULT_BEGIN_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].archiveDate").value(hasItem(DEFAULT_ARCHIVE_DATE.toString())))
            .andExpect(jsonPath("$.[*].archivedBy").value(hasItem(DEFAULT_ARCHIVED_BY)))
            .andExpect(jsonPath("$.[*].paymentStrategy").value(hasItem(DEFAULT_PAYMENT_STRATEGY.toString())))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
            .andExpect(jsonPath("$.[*].commissioningPlanType").value(hasItem(DEFAULT_COMMISSIONING_PLAN_TYPE.toString())))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllCommissioningPlansWithEagerRelationshipsIsEnabled() throws Exception {
        when(commissioningPlanServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restCommissioningPlanMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(commissioningPlanServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllCommissioningPlansWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(commissioningPlanServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restCommissioningPlanMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(commissioningPlanServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    void getCommissioningPlan() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get the commissioningPlan
        restCommissioningPlanMockMvc
            .perform(get(ENTITY_API_URL_ID, commissioningPlan.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(commissioningPlan.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.beginDate").value(DEFAULT_BEGIN_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()))
            .andExpect(jsonPath("$.creationDate").value(DEFAULT_CREATION_DATE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.archiveDate").value(DEFAULT_ARCHIVE_DATE.toString()))
            .andExpect(jsonPath("$.archivedBy").value(DEFAULT_ARCHIVED_BY))
            .andExpect(jsonPath("$.paymentStrategy").value(DEFAULT_PAYMENT_STRATEGY.toString()))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()))
            .andExpect(jsonPath("$.commissioningPlanType").value(DEFAULT_COMMISSIONING_PLAN_TYPE.toString()))
            .andExpect(jsonPath("$.spare1").value(DEFAULT_SPARE_1))
            .andExpect(jsonPath("$.spare2").value(DEFAULT_SPARE_2))
            .andExpect(jsonPath("$.spare3").value(DEFAULT_SPARE_3));
    }

    @Test
    @Transactional
    void getCommissioningPlansByIdFiltering() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        Long id = commissioningPlan.getId();

        defaultCommissioningPlanShouldBeFound("id.equals=" + id);
        defaultCommissioningPlanShouldNotBeFound("id.notEquals=" + id);

        defaultCommissioningPlanShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCommissioningPlanShouldNotBeFound("id.greaterThan=" + id);

        defaultCommissioningPlanShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCommissioningPlanShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where name equals to DEFAULT_NAME
        defaultCommissioningPlanShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the commissioningPlanList where name equals to UPDATED_NAME
        defaultCommissioningPlanShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where name not equals to DEFAULT_NAME
        defaultCommissioningPlanShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the commissioningPlanList where name not equals to UPDATED_NAME
        defaultCommissioningPlanShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByNameIsInShouldWork() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where name in DEFAULT_NAME or UPDATED_NAME
        defaultCommissioningPlanShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the commissioningPlanList where name equals to UPDATED_NAME
        defaultCommissioningPlanShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where name is not null
        defaultCommissioningPlanShouldBeFound("name.specified=true");

        // Get all the commissioningPlanList where name is null
        defaultCommissioningPlanShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByNameContainsSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where name contains DEFAULT_NAME
        defaultCommissioningPlanShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the commissioningPlanList where name contains UPDATED_NAME
        defaultCommissioningPlanShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByNameNotContainsSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where name does not contain DEFAULT_NAME
        defaultCommissioningPlanShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the commissioningPlanList where name does not contain UPDATED_NAME
        defaultCommissioningPlanShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByBeginDateIsEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where beginDate equals to DEFAULT_BEGIN_DATE
        defaultCommissioningPlanShouldBeFound("beginDate.equals=" + DEFAULT_BEGIN_DATE);

        // Get all the commissioningPlanList where beginDate equals to UPDATED_BEGIN_DATE
        defaultCommissioningPlanShouldNotBeFound("beginDate.equals=" + UPDATED_BEGIN_DATE);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByBeginDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where beginDate not equals to DEFAULT_BEGIN_DATE
        defaultCommissioningPlanShouldNotBeFound("beginDate.notEquals=" + DEFAULT_BEGIN_DATE);

        // Get all the commissioningPlanList where beginDate not equals to UPDATED_BEGIN_DATE
        defaultCommissioningPlanShouldBeFound("beginDate.notEquals=" + UPDATED_BEGIN_DATE);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByBeginDateIsInShouldWork() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where beginDate in DEFAULT_BEGIN_DATE or UPDATED_BEGIN_DATE
        defaultCommissioningPlanShouldBeFound("beginDate.in=" + DEFAULT_BEGIN_DATE + "," + UPDATED_BEGIN_DATE);

        // Get all the commissioningPlanList where beginDate equals to UPDATED_BEGIN_DATE
        defaultCommissioningPlanShouldNotBeFound("beginDate.in=" + UPDATED_BEGIN_DATE);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByBeginDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where beginDate is not null
        defaultCommissioningPlanShouldBeFound("beginDate.specified=true");

        // Get all the commissioningPlanList where beginDate is null
        defaultCommissioningPlanShouldNotBeFound("beginDate.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByEndDateIsEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where endDate equals to DEFAULT_END_DATE
        defaultCommissioningPlanShouldBeFound("endDate.equals=" + DEFAULT_END_DATE);

        // Get all the commissioningPlanList where endDate equals to UPDATED_END_DATE
        defaultCommissioningPlanShouldNotBeFound("endDate.equals=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByEndDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where endDate not equals to DEFAULT_END_DATE
        defaultCommissioningPlanShouldNotBeFound("endDate.notEquals=" + DEFAULT_END_DATE);

        // Get all the commissioningPlanList where endDate not equals to UPDATED_END_DATE
        defaultCommissioningPlanShouldBeFound("endDate.notEquals=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByEndDateIsInShouldWork() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where endDate in DEFAULT_END_DATE or UPDATED_END_DATE
        defaultCommissioningPlanShouldBeFound("endDate.in=" + DEFAULT_END_DATE + "," + UPDATED_END_DATE);

        // Get all the commissioningPlanList where endDate equals to UPDATED_END_DATE
        defaultCommissioningPlanShouldNotBeFound("endDate.in=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByEndDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where endDate is not null
        defaultCommissioningPlanShouldBeFound("endDate.specified=true");

        // Get all the commissioningPlanList where endDate is null
        defaultCommissioningPlanShouldNotBeFound("endDate.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByCreationDateIsEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where creationDate equals to DEFAULT_CREATION_DATE
        defaultCommissioningPlanShouldBeFound("creationDate.equals=" + DEFAULT_CREATION_DATE);

        // Get all the commissioningPlanList where creationDate equals to UPDATED_CREATION_DATE
        defaultCommissioningPlanShouldNotBeFound("creationDate.equals=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByCreationDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where creationDate not equals to DEFAULT_CREATION_DATE
        defaultCommissioningPlanShouldNotBeFound("creationDate.notEquals=" + DEFAULT_CREATION_DATE);

        // Get all the commissioningPlanList where creationDate not equals to UPDATED_CREATION_DATE
        defaultCommissioningPlanShouldBeFound("creationDate.notEquals=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByCreationDateIsInShouldWork() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where creationDate in DEFAULT_CREATION_DATE or UPDATED_CREATION_DATE
        defaultCommissioningPlanShouldBeFound("creationDate.in=" + DEFAULT_CREATION_DATE + "," + UPDATED_CREATION_DATE);

        // Get all the commissioningPlanList where creationDate equals to UPDATED_CREATION_DATE
        defaultCommissioningPlanShouldNotBeFound("creationDate.in=" + UPDATED_CREATION_DATE);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByCreationDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where creationDate is not null
        defaultCommissioningPlanShouldBeFound("creationDate.specified=true");

        // Get all the commissioningPlanList where creationDate is null
        defaultCommissioningPlanShouldNotBeFound("creationDate.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where createdBy equals to DEFAULT_CREATED_BY
        defaultCommissioningPlanShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the commissioningPlanList where createdBy equals to UPDATED_CREATED_BY
        defaultCommissioningPlanShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where createdBy not equals to DEFAULT_CREATED_BY
        defaultCommissioningPlanShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the commissioningPlanList where createdBy not equals to UPDATED_CREATED_BY
        defaultCommissioningPlanShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultCommissioningPlanShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the commissioningPlanList where createdBy equals to UPDATED_CREATED_BY
        defaultCommissioningPlanShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where createdBy is not null
        defaultCommissioningPlanShouldBeFound("createdBy.specified=true");

        // Get all the commissioningPlanList where createdBy is null
        defaultCommissioningPlanShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where createdBy contains DEFAULT_CREATED_BY
        defaultCommissioningPlanShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the commissioningPlanList where createdBy contains UPDATED_CREATED_BY
        defaultCommissioningPlanShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where createdBy does not contain DEFAULT_CREATED_BY
        defaultCommissioningPlanShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the commissioningPlanList where createdBy does not contain UPDATED_CREATED_BY
        defaultCommissioningPlanShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByArchiveDateIsEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where archiveDate equals to DEFAULT_ARCHIVE_DATE
        defaultCommissioningPlanShouldBeFound("archiveDate.equals=" + DEFAULT_ARCHIVE_DATE);

        // Get all the commissioningPlanList where archiveDate equals to UPDATED_ARCHIVE_DATE
        defaultCommissioningPlanShouldNotBeFound("archiveDate.equals=" + UPDATED_ARCHIVE_DATE);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByArchiveDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where archiveDate not equals to DEFAULT_ARCHIVE_DATE
        defaultCommissioningPlanShouldNotBeFound("archiveDate.notEquals=" + DEFAULT_ARCHIVE_DATE);

        // Get all the commissioningPlanList where archiveDate not equals to UPDATED_ARCHIVE_DATE
        defaultCommissioningPlanShouldBeFound("archiveDate.notEquals=" + UPDATED_ARCHIVE_DATE);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByArchiveDateIsInShouldWork() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where archiveDate in DEFAULT_ARCHIVE_DATE or UPDATED_ARCHIVE_DATE
        defaultCommissioningPlanShouldBeFound("archiveDate.in=" + DEFAULT_ARCHIVE_DATE + "," + UPDATED_ARCHIVE_DATE);

        // Get all the commissioningPlanList where archiveDate equals to UPDATED_ARCHIVE_DATE
        defaultCommissioningPlanShouldNotBeFound("archiveDate.in=" + UPDATED_ARCHIVE_DATE);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByArchiveDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where archiveDate is not null
        defaultCommissioningPlanShouldBeFound("archiveDate.specified=true");

        // Get all the commissioningPlanList where archiveDate is null
        defaultCommissioningPlanShouldNotBeFound("archiveDate.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByArchivedByIsEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where archivedBy equals to DEFAULT_ARCHIVED_BY
        defaultCommissioningPlanShouldBeFound("archivedBy.equals=" + DEFAULT_ARCHIVED_BY);

        // Get all the commissioningPlanList where archivedBy equals to UPDATED_ARCHIVED_BY
        defaultCommissioningPlanShouldNotBeFound("archivedBy.equals=" + UPDATED_ARCHIVED_BY);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByArchivedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where archivedBy not equals to DEFAULT_ARCHIVED_BY
        defaultCommissioningPlanShouldNotBeFound("archivedBy.notEquals=" + DEFAULT_ARCHIVED_BY);

        // Get all the commissioningPlanList where archivedBy not equals to UPDATED_ARCHIVED_BY
        defaultCommissioningPlanShouldBeFound("archivedBy.notEquals=" + UPDATED_ARCHIVED_BY);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByArchivedByIsInShouldWork() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where archivedBy in DEFAULT_ARCHIVED_BY or UPDATED_ARCHIVED_BY
        defaultCommissioningPlanShouldBeFound("archivedBy.in=" + DEFAULT_ARCHIVED_BY + "," + UPDATED_ARCHIVED_BY);

        // Get all the commissioningPlanList where archivedBy equals to UPDATED_ARCHIVED_BY
        defaultCommissioningPlanShouldNotBeFound("archivedBy.in=" + UPDATED_ARCHIVED_BY);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByArchivedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where archivedBy is not null
        defaultCommissioningPlanShouldBeFound("archivedBy.specified=true");

        // Get all the commissioningPlanList where archivedBy is null
        defaultCommissioningPlanShouldNotBeFound("archivedBy.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByArchivedByContainsSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where archivedBy contains DEFAULT_ARCHIVED_BY
        defaultCommissioningPlanShouldBeFound("archivedBy.contains=" + DEFAULT_ARCHIVED_BY);

        // Get all the commissioningPlanList where archivedBy contains UPDATED_ARCHIVED_BY
        defaultCommissioningPlanShouldNotBeFound("archivedBy.contains=" + UPDATED_ARCHIVED_BY);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByArchivedByNotContainsSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where archivedBy does not contain DEFAULT_ARCHIVED_BY
        defaultCommissioningPlanShouldNotBeFound("archivedBy.doesNotContain=" + DEFAULT_ARCHIVED_BY);

        // Get all the commissioningPlanList where archivedBy does not contain UPDATED_ARCHIVED_BY
        defaultCommissioningPlanShouldBeFound("archivedBy.doesNotContain=" + UPDATED_ARCHIVED_BY);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByPaymentStrategyIsEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where paymentStrategy equals to DEFAULT_PAYMENT_STRATEGY
        defaultCommissioningPlanShouldBeFound("paymentStrategy.equals=" + DEFAULT_PAYMENT_STRATEGY);

        // Get all the commissioningPlanList where paymentStrategy equals to UPDATED_PAYMENT_STRATEGY
        defaultCommissioningPlanShouldNotBeFound("paymentStrategy.equals=" + UPDATED_PAYMENT_STRATEGY);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByPaymentStrategyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where paymentStrategy not equals to DEFAULT_PAYMENT_STRATEGY
        defaultCommissioningPlanShouldNotBeFound("paymentStrategy.notEquals=" + DEFAULT_PAYMENT_STRATEGY);

        // Get all the commissioningPlanList where paymentStrategy not equals to UPDATED_PAYMENT_STRATEGY
        defaultCommissioningPlanShouldBeFound("paymentStrategy.notEquals=" + UPDATED_PAYMENT_STRATEGY);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByPaymentStrategyIsInShouldWork() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where paymentStrategy in DEFAULT_PAYMENT_STRATEGY or UPDATED_PAYMENT_STRATEGY
        defaultCommissioningPlanShouldBeFound("paymentStrategy.in=" + DEFAULT_PAYMENT_STRATEGY + "," + UPDATED_PAYMENT_STRATEGY);

        // Get all the commissioningPlanList where paymentStrategy equals to UPDATED_PAYMENT_STRATEGY
        defaultCommissioningPlanShouldNotBeFound("paymentStrategy.in=" + UPDATED_PAYMENT_STRATEGY);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByPaymentStrategyIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where paymentStrategy is not null
        defaultCommissioningPlanShouldBeFound("paymentStrategy.specified=true");

        // Get all the commissioningPlanList where paymentStrategy is null
        defaultCommissioningPlanShouldNotBeFound("paymentStrategy.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByStateIsEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where state equals to DEFAULT_STATE
        defaultCommissioningPlanShouldBeFound("state.equals=" + DEFAULT_STATE);

        // Get all the commissioningPlanList where state equals to UPDATED_STATE
        defaultCommissioningPlanShouldNotBeFound("state.equals=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByStateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where state not equals to DEFAULT_STATE
        defaultCommissioningPlanShouldNotBeFound("state.notEquals=" + DEFAULT_STATE);

        // Get all the commissioningPlanList where state not equals to UPDATED_STATE
        defaultCommissioningPlanShouldBeFound("state.notEquals=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByStateIsInShouldWork() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where state in DEFAULT_STATE or UPDATED_STATE
        defaultCommissioningPlanShouldBeFound("state.in=" + DEFAULT_STATE + "," + UPDATED_STATE);

        // Get all the commissioningPlanList where state equals to UPDATED_STATE
        defaultCommissioningPlanShouldNotBeFound("state.in=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByStateIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where state is not null
        defaultCommissioningPlanShouldBeFound("state.specified=true");

        // Get all the commissioningPlanList where state is null
        defaultCommissioningPlanShouldNotBeFound("state.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByCommissioningPlanTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where commissioningPlanType equals to DEFAULT_COMMISSIONING_PLAN_TYPE
        defaultCommissioningPlanShouldBeFound("commissioningPlanType.equals=" + DEFAULT_COMMISSIONING_PLAN_TYPE);

        // Get all the commissioningPlanList where commissioningPlanType equals to UPDATED_COMMISSIONING_PLAN_TYPE
        defaultCommissioningPlanShouldNotBeFound("commissioningPlanType.equals=" + UPDATED_COMMISSIONING_PLAN_TYPE);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByCommissioningPlanTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where commissioningPlanType not equals to DEFAULT_COMMISSIONING_PLAN_TYPE
        defaultCommissioningPlanShouldNotBeFound("commissioningPlanType.notEquals=" + DEFAULT_COMMISSIONING_PLAN_TYPE);

        // Get all the commissioningPlanList where commissioningPlanType not equals to UPDATED_COMMISSIONING_PLAN_TYPE
        defaultCommissioningPlanShouldBeFound("commissioningPlanType.notEquals=" + UPDATED_COMMISSIONING_PLAN_TYPE);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByCommissioningPlanTypeIsInShouldWork() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where commissioningPlanType in DEFAULT_COMMISSIONING_PLAN_TYPE or UPDATED_COMMISSIONING_PLAN_TYPE
        defaultCommissioningPlanShouldBeFound(
            "commissioningPlanType.in=" + DEFAULT_COMMISSIONING_PLAN_TYPE + "," + UPDATED_COMMISSIONING_PLAN_TYPE
        );

        // Get all the commissioningPlanList where commissioningPlanType equals to UPDATED_COMMISSIONING_PLAN_TYPE
        defaultCommissioningPlanShouldNotBeFound("commissioningPlanType.in=" + UPDATED_COMMISSIONING_PLAN_TYPE);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByCommissioningPlanTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where commissioningPlanType is not null
        defaultCommissioningPlanShouldBeFound("commissioningPlanType.specified=true");

        // Get all the commissioningPlanList where commissioningPlanType is null
        defaultCommissioningPlanShouldNotBeFound("commissioningPlanType.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissioningPlansBySpare1IsEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where spare1 equals to DEFAULT_SPARE_1
        defaultCommissioningPlanShouldBeFound("spare1.equals=" + DEFAULT_SPARE_1);

        // Get all the commissioningPlanList where spare1 equals to UPDATED_SPARE_1
        defaultCommissioningPlanShouldNotBeFound("spare1.equals=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansBySpare1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where spare1 not equals to DEFAULT_SPARE_1
        defaultCommissioningPlanShouldNotBeFound("spare1.notEquals=" + DEFAULT_SPARE_1);

        // Get all the commissioningPlanList where spare1 not equals to UPDATED_SPARE_1
        defaultCommissioningPlanShouldBeFound("spare1.notEquals=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansBySpare1IsInShouldWork() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where spare1 in DEFAULT_SPARE_1 or UPDATED_SPARE_1
        defaultCommissioningPlanShouldBeFound("spare1.in=" + DEFAULT_SPARE_1 + "," + UPDATED_SPARE_1);

        // Get all the commissioningPlanList where spare1 equals to UPDATED_SPARE_1
        defaultCommissioningPlanShouldNotBeFound("spare1.in=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansBySpare1IsNullOrNotNull() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where spare1 is not null
        defaultCommissioningPlanShouldBeFound("spare1.specified=true");

        // Get all the commissioningPlanList where spare1 is null
        defaultCommissioningPlanShouldNotBeFound("spare1.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissioningPlansBySpare1ContainsSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where spare1 contains DEFAULT_SPARE_1
        defaultCommissioningPlanShouldBeFound("spare1.contains=" + DEFAULT_SPARE_1);

        // Get all the commissioningPlanList where spare1 contains UPDATED_SPARE_1
        defaultCommissioningPlanShouldNotBeFound("spare1.contains=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansBySpare1NotContainsSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where spare1 does not contain DEFAULT_SPARE_1
        defaultCommissioningPlanShouldNotBeFound("spare1.doesNotContain=" + DEFAULT_SPARE_1);

        // Get all the commissioningPlanList where spare1 does not contain UPDATED_SPARE_1
        defaultCommissioningPlanShouldBeFound("spare1.doesNotContain=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansBySpare2IsEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where spare2 equals to DEFAULT_SPARE_2
        defaultCommissioningPlanShouldBeFound("spare2.equals=" + DEFAULT_SPARE_2);

        // Get all the commissioningPlanList where spare2 equals to UPDATED_SPARE_2
        defaultCommissioningPlanShouldNotBeFound("spare2.equals=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansBySpare2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where spare2 not equals to DEFAULT_SPARE_2
        defaultCommissioningPlanShouldNotBeFound("spare2.notEquals=" + DEFAULT_SPARE_2);

        // Get all the commissioningPlanList where spare2 not equals to UPDATED_SPARE_2
        defaultCommissioningPlanShouldBeFound("spare2.notEquals=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansBySpare2IsInShouldWork() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where spare2 in DEFAULT_SPARE_2 or UPDATED_SPARE_2
        defaultCommissioningPlanShouldBeFound("spare2.in=" + DEFAULT_SPARE_2 + "," + UPDATED_SPARE_2);

        // Get all the commissioningPlanList where spare2 equals to UPDATED_SPARE_2
        defaultCommissioningPlanShouldNotBeFound("spare2.in=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansBySpare2IsNullOrNotNull() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where spare2 is not null
        defaultCommissioningPlanShouldBeFound("spare2.specified=true");

        // Get all the commissioningPlanList where spare2 is null
        defaultCommissioningPlanShouldNotBeFound("spare2.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissioningPlansBySpare2ContainsSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where spare2 contains DEFAULT_SPARE_2
        defaultCommissioningPlanShouldBeFound("spare2.contains=" + DEFAULT_SPARE_2);

        // Get all the commissioningPlanList where spare2 contains UPDATED_SPARE_2
        defaultCommissioningPlanShouldNotBeFound("spare2.contains=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansBySpare2NotContainsSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where spare2 does not contain DEFAULT_SPARE_2
        defaultCommissioningPlanShouldNotBeFound("spare2.doesNotContain=" + DEFAULT_SPARE_2);

        // Get all the commissioningPlanList where spare2 does not contain UPDATED_SPARE_2
        defaultCommissioningPlanShouldBeFound("spare2.doesNotContain=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansBySpare3IsEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where spare3 equals to DEFAULT_SPARE_3
        defaultCommissioningPlanShouldBeFound("spare3.equals=" + DEFAULT_SPARE_3);

        // Get all the commissioningPlanList where spare3 equals to UPDATED_SPARE_3
        defaultCommissioningPlanShouldNotBeFound("spare3.equals=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansBySpare3IsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where spare3 not equals to DEFAULT_SPARE_3
        defaultCommissioningPlanShouldNotBeFound("spare3.notEquals=" + DEFAULT_SPARE_3);

        // Get all the commissioningPlanList where spare3 not equals to UPDATED_SPARE_3
        defaultCommissioningPlanShouldBeFound("spare3.notEquals=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansBySpare3IsInShouldWork() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where spare3 in DEFAULT_SPARE_3 or UPDATED_SPARE_3
        defaultCommissioningPlanShouldBeFound("spare3.in=" + DEFAULT_SPARE_3 + "," + UPDATED_SPARE_3);

        // Get all the commissioningPlanList where spare3 equals to UPDATED_SPARE_3
        defaultCommissioningPlanShouldNotBeFound("spare3.in=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansBySpare3IsNullOrNotNull() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where spare3 is not null
        defaultCommissioningPlanShouldBeFound("spare3.specified=true");

        // Get all the commissioningPlanList where spare3 is null
        defaultCommissioningPlanShouldNotBeFound("spare3.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissioningPlansBySpare3ContainsSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where spare3 contains DEFAULT_SPARE_3
        defaultCommissioningPlanShouldBeFound("spare3.contains=" + DEFAULT_SPARE_3);

        // Get all the commissioningPlanList where spare3 contains UPDATED_SPARE_3
        defaultCommissioningPlanShouldNotBeFound("spare3.contains=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansBySpare3NotContainsSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        // Get all the commissioningPlanList where spare3 does not contain DEFAULT_SPARE_3
        defaultCommissioningPlanShouldNotBeFound("spare3.doesNotContain=" + DEFAULT_SPARE_3);

        // Get all the commissioningPlanList where spare3 does not contain UPDATED_SPARE_3
        defaultCommissioningPlanShouldBeFound("spare3.doesNotContain=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByPaymentFrequencyIsEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);
        Frequency paymentFrequency;
        if (TestUtil.findAll(em, Frequency.class).isEmpty()) {
            paymentFrequency = FrequencyResourceIT.createEntity(em);
            em.persist(paymentFrequency);
            em.flush();
        } else {
            paymentFrequency = TestUtil.findAll(em, Frequency.class).get(0);
        }
        em.persist(paymentFrequency);
        em.flush();
        commissioningPlan.setPaymentFrequency(paymentFrequency);
        commissioningPlanRepository.saveAndFlush(commissioningPlan);
        Long paymentFrequencyId = paymentFrequency.getId();

        // Get all the commissioningPlanList where paymentFrequency equals to paymentFrequencyId
        defaultCommissioningPlanShouldBeFound("paymentFrequencyId.equals=" + paymentFrequencyId);

        // Get all the commissioningPlanList where paymentFrequency equals to (paymentFrequencyId + 1)
        defaultCommissioningPlanShouldNotBeFound("paymentFrequencyId.equals=" + (paymentFrequencyId + 1));
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByCalculusFrequencyIsEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);
        Frequency calculusFrequency;
        if (TestUtil.findAll(em, Frequency.class).isEmpty()) {
            calculusFrequency = FrequencyResourceIT.createEntity(em);
            em.persist(calculusFrequency);
            em.flush();
        } else {
            calculusFrequency = TestUtil.findAll(em, Frequency.class).get(0);
        }
        em.persist(calculusFrequency);
        em.flush();
        commissioningPlan.setCalculusFrequency(calculusFrequency);
        commissioningPlanRepository.saveAndFlush(commissioningPlan);
        Long calculusFrequencyId = calculusFrequency.getId();

        // Get all the commissioningPlanList where calculusFrequency equals to calculusFrequencyId
        defaultCommissioningPlanShouldBeFound("calculusFrequencyId.equals=" + calculusFrequencyId);

        // Get all the commissioningPlanList where calculusFrequency equals to (calculusFrequencyId + 1)
        defaultCommissioningPlanShouldNotBeFound("calculusFrequencyId.equals=" + (calculusFrequencyId + 1));
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByCalculusPeriodIsEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);
        Period calculusPeriod;
        if (TestUtil.findAll(em, Period.class).isEmpty()) {
            calculusPeriod = PeriodResourceIT.createEntity(em);
            em.persist(calculusPeriod);
            em.flush();
        } else {
            calculusPeriod = TestUtil.findAll(em, Period.class).get(0);
        }
        em.persist(calculusPeriod);
        em.flush();
        commissioningPlan.setCalculusPeriod(calculusPeriod);
        commissioningPlanRepository.saveAndFlush(commissioningPlan);
        Long calculusPeriodId = calculusPeriod.getId();

        // Get all the commissioningPlanList where calculusPeriod equals to calculusPeriodId
        defaultCommissioningPlanShouldBeFound("calculusPeriodId.equals=" + calculusPeriodId);

        // Get all the commissioningPlanList where calculusPeriod equals to (calculusPeriodId + 1)
        defaultCommissioningPlanShouldNotBeFound("calculusPeriodId.equals=" + (calculusPeriodId + 1));
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByPaymentPeriodIsEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);
        Period paymentPeriod;
        if (TestUtil.findAll(em, Period.class).isEmpty()) {
            paymentPeriod = PeriodResourceIT.createEntity(em);
            em.persist(paymentPeriod);
            em.flush();
        } else {
            paymentPeriod = TestUtil.findAll(em, Period.class).get(0);
        }
        em.persist(paymentPeriod);
        em.flush();
        commissioningPlan.setPaymentPeriod(paymentPeriod);
        commissioningPlanRepository.saveAndFlush(commissioningPlan);
        Long paymentPeriodId = paymentPeriod.getId();

        // Get all the commissioningPlanList where paymentPeriod equals to paymentPeriodId
        defaultCommissioningPlanShouldBeFound("paymentPeriodId.equals=" + paymentPeriodId);

        // Get all the commissioningPlanList where paymentPeriod equals to (paymentPeriodId + 1)
        defaultCommissioningPlanShouldNotBeFound("paymentPeriodId.equals=" + (paymentPeriodId + 1));
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByServicesIsEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);
        ServiceType services;
        if (TestUtil.findAll(em, ServiceType.class).isEmpty()) {
            services = ServiceTypeResourceIT.createEntity(em);
            em.persist(services);
            em.flush();
        } else {
            services = TestUtil.findAll(em, ServiceType.class).get(0);
        }
        em.persist(services);
        em.flush();
        commissioningPlan.addServices(services);
        commissioningPlanRepository.saveAndFlush(commissioningPlan);
        Long servicesId = services.getId();

        // Get all the commissioningPlanList where services equals to servicesId
        defaultCommissioningPlanShouldBeFound("servicesId.equals=" + servicesId);

        // Get all the commissioningPlanList where services equals to (servicesId + 1)
        defaultCommissioningPlanShouldNotBeFound("servicesId.equals=" + (servicesId + 1));
    }

    @Test
    @Transactional
    void getAllCommissioningPlansByConfigurationPlanIsEqualToSomething() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);
        ConfigurationPlan configurationPlan;
        if (TestUtil.findAll(em, ConfigurationPlan.class).isEmpty()) {
            configurationPlan = ConfigurationPlanResourceIT.createEntity(em);
            em.persist(configurationPlan);
            em.flush();
        } else {
            configurationPlan = TestUtil.findAll(em, ConfigurationPlan.class).get(0);
        }
        em.persist(configurationPlan);
        em.flush();
        commissioningPlan.addConfigurationPlan(configurationPlan);
        commissioningPlanRepository.saveAndFlush(commissioningPlan);
        Long configurationPlanId = configurationPlan.getId();

        // Get all the commissioningPlanList where configurationPlan equals to configurationPlanId
        defaultCommissioningPlanShouldBeFound("configurationPlanId.equals=" + configurationPlanId);

        // Get all the commissioningPlanList where configurationPlan equals to (configurationPlanId + 1)
        defaultCommissioningPlanShouldNotBeFound("configurationPlanId.equals=" + (configurationPlanId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCommissioningPlanShouldBeFound(String filter) throws Exception {
        restCommissioningPlanMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(commissioningPlan.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].beginDate").value(hasItem(DEFAULT_BEGIN_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].creationDate").value(hasItem(DEFAULT_CREATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].archiveDate").value(hasItem(DEFAULT_ARCHIVE_DATE.toString())))
            .andExpect(jsonPath("$.[*].archivedBy").value(hasItem(DEFAULT_ARCHIVED_BY)))
            .andExpect(jsonPath("$.[*].paymentStrategy").value(hasItem(DEFAULT_PAYMENT_STRATEGY.toString())))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
            .andExpect(jsonPath("$.[*].commissioningPlanType").value(hasItem(DEFAULT_COMMISSIONING_PLAN_TYPE.toString())))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)));

        // Check, that the count call also returns 1
        restCommissioningPlanMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCommissioningPlanShouldNotBeFound(String filter) throws Exception {
        restCommissioningPlanMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCommissioningPlanMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingCommissioningPlan() throws Exception {
        // Get the commissioningPlan
        restCommissioningPlanMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCommissioningPlan() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        int databaseSizeBeforeUpdate = commissioningPlanRepository.findAll().size();

        // Update the commissioningPlan
        CommissioningPlan updatedCommissioningPlan = commissioningPlanRepository.findById(commissioningPlan.getId()).get();
        // Disconnect from session so that the updates on updatedCommissioningPlan are not directly saved in db
        em.detach(updatedCommissioningPlan);
        updatedCommissioningPlan
            .name(UPDATED_NAME)
            .beginDate(UPDATED_BEGIN_DATE)
            .endDate(UPDATED_END_DATE)
            .creationDate(UPDATED_CREATION_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .archiveDate(UPDATED_ARCHIVE_DATE)
            .archivedBy(UPDATED_ARCHIVED_BY)
            .paymentStrategy(UPDATED_PAYMENT_STRATEGY)
            .state(UPDATED_STATE)
            .commissioningPlanType(UPDATED_COMMISSIONING_PLAN_TYPE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);
        CommissioningPlanDTO commissioningPlanDTO = commissioningPlanMapper.toDto(updatedCommissioningPlan);

        restCommissioningPlanMockMvc
            .perform(
                put(ENTITY_API_URL_ID, commissioningPlanDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissioningPlanDTO))
            )
            .andExpect(status().isOk());

        // Validate the CommissioningPlan in the database
        List<CommissioningPlan> commissioningPlanList = commissioningPlanRepository.findAll();
        assertThat(commissioningPlanList).hasSize(databaseSizeBeforeUpdate);
        CommissioningPlan testCommissioningPlan = commissioningPlanList.get(commissioningPlanList.size() - 1);
        assertThat(testCommissioningPlan.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCommissioningPlan.getBeginDate()).isEqualTo(UPDATED_BEGIN_DATE);
        assertThat(testCommissioningPlan.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testCommissioningPlan.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testCommissioningPlan.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCommissioningPlan.getArchiveDate()).isEqualTo(UPDATED_ARCHIVE_DATE);
        assertThat(testCommissioningPlan.getArchivedBy()).isEqualTo(UPDATED_ARCHIVED_BY);
        assertThat(testCommissioningPlan.getPaymentStrategy()).isEqualTo(UPDATED_PAYMENT_STRATEGY);
        assertThat(testCommissioningPlan.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testCommissioningPlan.getCommissioningPlanType()).isEqualTo(UPDATED_COMMISSIONING_PLAN_TYPE);
        assertThat(testCommissioningPlan.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testCommissioningPlan.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testCommissioningPlan.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void putNonExistingCommissioningPlan() throws Exception {
        int databaseSizeBeforeUpdate = commissioningPlanRepository.findAll().size();
        commissioningPlan.setId(count.incrementAndGet());

        // Create the CommissioningPlan
        CommissioningPlanDTO commissioningPlanDTO = commissioningPlanMapper.toDto(commissioningPlan);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCommissioningPlanMockMvc
            .perform(
                put(ENTITY_API_URL_ID, commissioningPlanDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissioningPlanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommissioningPlan in the database
        List<CommissioningPlan> commissioningPlanList = commissioningPlanRepository.findAll();
        assertThat(commissioningPlanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCommissioningPlan() throws Exception {
        int databaseSizeBeforeUpdate = commissioningPlanRepository.findAll().size();
        commissioningPlan.setId(count.incrementAndGet());

        // Create the CommissioningPlan
        CommissioningPlanDTO commissioningPlanDTO = commissioningPlanMapper.toDto(commissioningPlan);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommissioningPlanMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissioningPlanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommissioningPlan in the database
        List<CommissioningPlan> commissioningPlanList = commissioningPlanRepository.findAll();
        assertThat(commissioningPlanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCommissioningPlan() throws Exception {
        int databaseSizeBeforeUpdate = commissioningPlanRepository.findAll().size();
        commissioningPlan.setId(count.incrementAndGet());

        // Create the CommissioningPlan
        CommissioningPlanDTO commissioningPlanDTO = commissioningPlanMapper.toDto(commissioningPlan);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommissioningPlanMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(commissioningPlanDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CommissioningPlan in the database
        List<CommissioningPlan> commissioningPlanList = commissioningPlanRepository.findAll();
        assertThat(commissioningPlanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCommissioningPlanWithPatch() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        int databaseSizeBeforeUpdate = commissioningPlanRepository.findAll().size();

        // Update the commissioningPlan using partial update
        CommissioningPlan partialUpdatedCommissioningPlan = new CommissioningPlan();
        partialUpdatedCommissioningPlan.setId(commissioningPlan.getId());

        partialUpdatedCommissioningPlan
            .endDate(UPDATED_END_DATE)
            .creationDate(UPDATED_CREATION_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .archivedBy(UPDATED_ARCHIVED_BY)
            .paymentStrategy(UPDATED_PAYMENT_STRATEGY)
            .state(UPDATED_STATE)
            .spare1(UPDATED_SPARE_1)
            .spare3(UPDATED_SPARE_3);

        restCommissioningPlanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCommissioningPlan.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCommissioningPlan))
            )
            .andExpect(status().isOk());

        // Validate the CommissioningPlan in the database
        List<CommissioningPlan> commissioningPlanList = commissioningPlanRepository.findAll();
        assertThat(commissioningPlanList).hasSize(databaseSizeBeforeUpdate);
        CommissioningPlan testCommissioningPlan = commissioningPlanList.get(commissioningPlanList.size() - 1);
        assertThat(testCommissioningPlan.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCommissioningPlan.getBeginDate()).isEqualTo(DEFAULT_BEGIN_DATE);
        assertThat(testCommissioningPlan.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testCommissioningPlan.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testCommissioningPlan.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCommissioningPlan.getArchiveDate()).isEqualTo(DEFAULT_ARCHIVE_DATE);
        assertThat(testCommissioningPlan.getArchivedBy()).isEqualTo(UPDATED_ARCHIVED_BY);
        assertThat(testCommissioningPlan.getPaymentStrategy()).isEqualTo(UPDATED_PAYMENT_STRATEGY);
        assertThat(testCommissioningPlan.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testCommissioningPlan.getCommissioningPlanType()).isEqualTo(DEFAULT_COMMISSIONING_PLAN_TYPE);
        assertThat(testCommissioningPlan.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testCommissioningPlan.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
        assertThat(testCommissioningPlan.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void fullUpdateCommissioningPlanWithPatch() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        int databaseSizeBeforeUpdate = commissioningPlanRepository.findAll().size();

        // Update the commissioningPlan using partial update
        CommissioningPlan partialUpdatedCommissioningPlan = new CommissioningPlan();
        partialUpdatedCommissioningPlan.setId(commissioningPlan.getId());

        partialUpdatedCommissioningPlan
            .name(UPDATED_NAME)
            .beginDate(UPDATED_BEGIN_DATE)
            .endDate(UPDATED_END_DATE)
            .creationDate(UPDATED_CREATION_DATE)
            .createdBy(UPDATED_CREATED_BY)
            .archiveDate(UPDATED_ARCHIVE_DATE)
            .archivedBy(UPDATED_ARCHIVED_BY)
            .paymentStrategy(UPDATED_PAYMENT_STRATEGY)
            .state(UPDATED_STATE)
            .commissioningPlanType(UPDATED_COMMISSIONING_PLAN_TYPE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);

        restCommissioningPlanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCommissioningPlan.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCommissioningPlan))
            )
            .andExpect(status().isOk());

        // Validate the CommissioningPlan in the database
        List<CommissioningPlan> commissioningPlanList = commissioningPlanRepository.findAll();
        assertThat(commissioningPlanList).hasSize(databaseSizeBeforeUpdate);
        CommissioningPlan testCommissioningPlan = commissioningPlanList.get(commissioningPlanList.size() - 1);
        assertThat(testCommissioningPlan.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCommissioningPlan.getBeginDate()).isEqualTo(UPDATED_BEGIN_DATE);
        assertThat(testCommissioningPlan.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testCommissioningPlan.getCreationDate()).isEqualTo(UPDATED_CREATION_DATE);
        assertThat(testCommissioningPlan.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testCommissioningPlan.getArchiveDate()).isEqualTo(UPDATED_ARCHIVE_DATE);
        assertThat(testCommissioningPlan.getArchivedBy()).isEqualTo(UPDATED_ARCHIVED_BY);
        assertThat(testCommissioningPlan.getPaymentStrategy()).isEqualTo(UPDATED_PAYMENT_STRATEGY);
        assertThat(testCommissioningPlan.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testCommissioningPlan.getCommissioningPlanType()).isEqualTo(UPDATED_COMMISSIONING_PLAN_TYPE);
        assertThat(testCommissioningPlan.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testCommissioningPlan.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testCommissioningPlan.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void patchNonExistingCommissioningPlan() throws Exception {
        int databaseSizeBeforeUpdate = commissioningPlanRepository.findAll().size();
        commissioningPlan.setId(count.incrementAndGet());

        // Create the CommissioningPlan
        CommissioningPlanDTO commissioningPlanDTO = commissioningPlanMapper.toDto(commissioningPlan);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCommissioningPlanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, commissioningPlanDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(commissioningPlanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommissioningPlan in the database
        List<CommissioningPlan> commissioningPlanList = commissioningPlanRepository.findAll();
        assertThat(commissioningPlanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCommissioningPlan() throws Exception {
        int databaseSizeBeforeUpdate = commissioningPlanRepository.findAll().size();
        commissioningPlan.setId(count.incrementAndGet());

        // Create the CommissioningPlan
        CommissioningPlanDTO commissioningPlanDTO = commissioningPlanMapper.toDto(commissioningPlan);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommissioningPlanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(commissioningPlanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommissioningPlan in the database
        List<CommissioningPlan> commissioningPlanList = commissioningPlanRepository.findAll();
        assertThat(commissioningPlanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCommissioningPlan() throws Exception {
        int databaseSizeBeforeUpdate = commissioningPlanRepository.findAll().size();
        commissioningPlan.setId(count.incrementAndGet());

        // Create the CommissioningPlan
        CommissioningPlanDTO commissioningPlanDTO = commissioningPlanMapper.toDto(commissioningPlan);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommissioningPlanMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(commissioningPlanDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CommissioningPlan in the database
        List<CommissioningPlan> commissioningPlanList = commissioningPlanRepository.findAll();
        assertThat(commissioningPlanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCommissioningPlan() throws Exception {
        // Initialize the database
        commissioningPlanRepository.saveAndFlush(commissioningPlan);

        int databaseSizeBeforeDelete = commissioningPlanRepository.findAll().size();

        // Delete the commissioningPlan
        restCommissioningPlanMockMvc
            .perform(delete(ENTITY_API_URL_ID, commissioningPlan.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CommissioningPlan> commissioningPlanList = commissioningPlanRepository.findAll();
        assertThat(commissioningPlanList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
