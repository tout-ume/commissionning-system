package sn.free.commissioning.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.IntegrationTest;
import sn.free.commissioning.domain.ConfigurationPlan;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.domain.Territory;
import sn.free.commissioning.domain.Zone;
import sn.free.commissioning.repository.ZoneRepository;
import sn.free.commissioning.service.criteria.ZoneCriteria;
import sn.free.commissioning.service.dto.ZoneDTO;
import sn.free.commissioning.service.mapper.ZoneMapper;

/**
 * Integration tests for the {@link ZoneResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ZoneResourceIT {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_STATE = "AAAAAAAAAA";
    private static final String UPDATED_STATE = "BBBBBBBBBB";

    private static final Long DEFAULT_ZONE_TYPE_ID = 1L;
    private static final Long UPDATED_ZONE_TYPE_ID = 2L;
    private static final Long SMALLER_ZONE_TYPE_ID = 1L - 1L;

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/zones";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ZoneRepository zoneRepository;

    @Autowired
    private ZoneMapper zoneMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restZoneMockMvc;

    private Zone zone;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Zone createEntity(EntityManager em) {
        Zone zone = new Zone()
            .code(DEFAULT_CODE)
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .state(DEFAULT_STATE)
            .zoneTypeId(DEFAULT_ZONE_TYPE_ID)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE);
        return zone;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Zone createUpdatedEntity(EntityManager em) {
        Zone zone = new Zone()
            .code(UPDATED_CODE)
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .state(UPDATED_STATE)
            .zoneTypeId(UPDATED_ZONE_TYPE_ID)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE);
        return zone;
    }

    @BeforeEach
    public void initTest() {
        zone = createEntity(em);
    }

    @Test
    @Transactional
    void createZone() throws Exception {
        int databaseSizeBeforeCreate = zoneRepository.findAll().size();
        // Create the Zone
        ZoneDTO zoneDTO = zoneMapper.toDto(zone);
        restZoneMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(zoneDTO)))
            .andExpect(status().isCreated());

        // Validate the Zone in the database
        List<Zone> zoneList = zoneRepository.findAll();
        assertThat(zoneList).hasSize(databaseSizeBeforeCreate + 1);
        Zone testZone = zoneList.get(zoneList.size() - 1);
        assertThat(testZone.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testZone.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testZone.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testZone.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testZone.getZoneTypeId()).isEqualTo(DEFAULT_ZONE_TYPE_ID);
        assertThat(testZone.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testZone.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testZone.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testZone.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void createZoneWithExistingId() throws Exception {
        // Create the Zone with an existing ID
        zone.setId(1L);
        ZoneDTO zoneDTO = zoneMapper.toDto(zone);

        int databaseSizeBeforeCreate = zoneRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restZoneMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(zoneDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Zone in the database
        List<Zone> zoneList = zoneRepository.findAll();
        assertThat(zoneList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = zoneRepository.findAll().size();
        // set the field null
        zone.setCode(null);

        // Create the Zone, which fails.
        ZoneDTO zoneDTO = zoneMapper.toDto(zone);

        restZoneMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(zoneDTO)))
            .andExpect(status().isBadRequest());

        List<Zone> zoneList = zoneRepository.findAll();
        assertThat(zoneList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = zoneRepository.findAll().size();
        // set the field null
        zone.setName(null);

        // Create the Zone, which fails.
        ZoneDTO zoneDTO = zoneMapper.toDto(zone);

        restZoneMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(zoneDTO)))
            .andExpect(status().isBadRequest());

        List<Zone> zoneList = zoneRepository.findAll();
        assertThat(zoneList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCreatedByIsRequired() throws Exception {
        int databaseSizeBeforeTest = zoneRepository.findAll().size();
        // set the field null
        zone.setCreatedBy(null);

        // Create the Zone, which fails.
        ZoneDTO zoneDTO = zoneMapper.toDto(zone);

        restZoneMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(zoneDTO)))
            .andExpect(status().isBadRequest());

        List<Zone> zoneList = zoneRepository.findAll();
        assertThat(zoneList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllZones() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList
        restZoneMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(zone.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE)))
            .andExpect(jsonPath("$.[*].zoneTypeId").value(hasItem(DEFAULT_ZONE_TYPE_ID.intValue())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())));
    }

    @Test
    @Transactional
    void getZone() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get the zone
        restZoneMockMvc
            .perform(get(ENTITY_API_URL_ID, zone.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(zone.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE))
            .andExpect(jsonPath("$.zoneTypeId").value(DEFAULT_ZONE_TYPE_ID.intValue()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()));
    }

    @Test
    @Transactional
    void getZonesByIdFiltering() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        Long id = zone.getId();

        defaultZoneShouldBeFound("id.equals=" + id);
        defaultZoneShouldNotBeFound("id.notEquals=" + id);

        defaultZoneShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultZoneShouldNotBeFound("id.greaterThan=" + id);

        defaultZoneShouldBeFound("id.lessThanOrEqual=" + id);
        defaultZoneShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllZonesByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where code equals to DEFAULT_CODE
        defaultZoneShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the zoneList where code equals to UPDATED_CODE
        defaultZoneShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllZonesByCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where code not equals to DEFAULT_CODE
        defaultZoneShouldNotBeFound("code.notEquals=" + DEFAULT_CODE);

        // Get all the zoneList where code not equals to UPDATED_CODE
        defaultZoneShouldBeFound("code.notEquals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllZonesByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where code in DEFAULT_CODE or UPDATED_CODE
        defaultZoneShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the zoneList where code equals to UPDATED_CODE
        defaultZoneShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllZonesByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where code is not null
        defaultZoneShouldBeFound("code.specified=true");

        // Get all the zoneList where code is null
        defaultZoneShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    void getAllZonesByCodeContainsSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where code contains DEFAULT_CODE
        defaultZoneShouldBeFound("code.contains=" + DEFAULT_CODE);

        // Get all the zoneList where code contains UPDATED_CODE
        defaultZoneShouldNotBeFound("code.contains=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllZonesByCodeNotContainsSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where code does not contain DEFAULT_CODE
        defaultZoneShouldNotBeFound("code.doesNotContain=" + DEFAULT_CODE);

        // Get all the zoneList where code does not contain UPDATED_CODE
        defaultZoneShouldBeFound("code.doesNotContain=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllZonesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where name equals to DEFAULT_NAME
        defaultZoneShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the zoneList where name equals to UPDATED_NAME
        defaultZoneShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllZonesByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where name not equals to DEFAULT_NAME
        defaultZoneShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the zoneList where name not equals to UPDATED_NAME
        defaultZoneShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllZonesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where name in DEFAULT_NAME or UPDATED_NAME
        defaultZoneShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the zoneList where name equals to UPDATED_NAME
        defaultZoneShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllZonesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where name is not null
        defaultZoneShouldBeFound("name.specified=true");

        // Get all the zoneList where name is null
        defaultZoneShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllZonesByNameContainsSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where name contains DEFAULT_NAME
        defaultZoneShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the zoneList where name contains UPDATED_NAME
        defaultZoneShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllZonesByNameNotContainsSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where name does not contain DEFAULT_NAME
        defaultZoneShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the zoneList where name does not contain UPDATED_NAME
        defaultZoneShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllZonesByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where description equals to DEFAULT_DESCRIPTION
        defaultZoneShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the zoneList where description equals to UPDATED_DESCRIPTION
        defaultZoneShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllZonesByDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where description not equals to DEFAULT_DESCRIPTION
        defaultZoneShouldNotBeFound("description.notEquals=" + DEFAULT_DESCRIPTION);

        // Get all the zoneList where description not equals to UPDATED_DESCRIPTION
        defaultZoneShouldBeFound("description.notEquals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllZonesByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultZoneShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the zoneList where description equals to UPDATED_DESCRIPTION
        defaultZoneShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllZonesByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where description is not null
        defaultZoneShouldBeFound("description.specified=true");

        // Get all the zoneList where description is null
        defaultZoneShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    void getAllZonesByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where description contains DEFAULT_DESCRIPTION
        defaultZoneShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the zoneList where description contains UPDATED_DESCRIPTION
        defaultZoneShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllZonesByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where description does not contain DEFAULT_DESCRIPTION
        defaultZoneShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the zoneList where description does not contain UPDATED_DESCRIPTION
        defaultZoneShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllZonesByStateIsEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where state equals to DEFAULT_STATE
        defaultZoneShouldBeFound("state.equals=" + DEFAULT_STATE);

        // Get all the zoneList where state equals to UPDATED_STATE
        defaultZoneShouldNotBeFound("state.equals=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    void getAllZonesByStateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where state not equals to DEFAULT_STATE
        defaultZoneShouldNotBeFound("state.notEquals=" + DEFAULT_STATE);

        // Get all the zoneList where state not equals to UPDATED_STATE
        defaultZoneShouldBeFound("state.notEquals=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    void getAllZonesByStateIsInShouldWork() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where state in DEFAULT_STATE or UPDATED_STATE
        defaultZoneShouldBeFound("state.in=" + DEFAULT_STATE + "," + UPDATED_STATE);

        // Get all the zoneList where state equals to UPDATED_STATE
        defaultZoneShouldNotBeFound("state.in=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    void getAllZonesByStateIsNullOrNotNull() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where state is not null
        defaultZoneShouldBeFound("state.specified=true");

        // Get all the zoneList where state is null
        defaultZoneShouldNotBeFound("state.specified=false");
    }

    @Test
    @Transactional
    void getAllZonesByStateContainsSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where state contains DEFAULT_STATE
        defaultZoneShouldBeFound("state.contains=" + DEFAULT_STATE);

        // Get all the zoneList where state contains UPDATED_STATE
        defaultZoneShouldNotBeFound("state.contains=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    void getAllZonesByStateNotContainsSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where state does not contain DEFAULT_STATE
        defaultZoneShouldNotBeFound("state.doesNotContain=" + DEFAULT_STATE);

        // Get all the zoneList where state does not contain UPDATED_STATE
        defaultZoneShouldBeFound("state.doesNotContain=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    void getAllZonesByZoneTypeIdIsEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where zoneTypeId equals to DEFAULT_ZONE_TYPE_ID
        defaultZoneShouldBeFound("zoneTypeId.equals=" + DEFAULT_ZONE_TYPE_ID);

        // Get all the zoneList where zoneTypeId equals to UPDATED_ZONE_TYPE_ID
        defaultZoneShouldNotBeFound("zoneTypeId.equals=" + UPDATED_ZONE_TYPE_ID);
    }

    @Test
    @Transactional
    void getAllZonesByZoneTypeIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where zoneTypeId not equals to DEFAULT_ZONE_TYPE_ID
        defaultZoneShouldNotBeFound("zoneTypeId.notEquals=" + DEFAULT_ZONE_TYPE_ID);

        // Get all the zoneList where zoneTypeId not equals to UPDATED_ZONE_TYPE_ID
        defaultZoneShouldBeFound("zoneTypeId.notEquals=" + UPDATED_ZONE_TYPE_ID);
    }

    @Test
    @Transactional
    void getAllZonesByZoneTypeIdIsInShouldWork() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where zoneTypeId in DEFAULT_ZONE_TYPE_ID or UPDATED_ZONE_TYPE_ID
        defaultZoneShouldBeFound("zoneTypeId.in=" + DEFAULT_ZONE_TYPE_ID + "," + UPDATED_ZONE_TYPE_ID);

        // Get all the zoneList where zoneTypeId equals to UPDATED_ZONE_TYPE_ID
        defaultZoneShouldNotBeFound("zoneTypeId.in=" + UPDATED_ZONE_TYPE_ID);
    }

    @Test
    @Transactional
    void getAllZonesByZoneTypeIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where zoneTypeId is not null
        defaultZoneShouldBeFound("zoneTypeId.specified=true");

        // Get all the zoneList where zoneTypeId is null
        defaultZoneShouldNotBeFound("zoneTypeId.specified=false");
    }

    @Test
    @Transactional
    void getAllZonesByZoneTypeIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where zoneTypeId is greater than or equal to DEFAULT_ZONE_TYPE_ID
        defaultZoneShouldBeFound("zoneTypeId.greaterThanOrEqual=" + DEFAULT_ZONE_TYPE_ID);

        // Get all the zoneList where zoneTypeId is greater than or equal to UPDATED_ZONE_TYPE_ID
        defaultZoneShouldNotBeFound("zoneTypeId.greaterThanOrEqual=" + UPDATED_ZONE_TYPE_ID);
    }

    @Test
    @Transactional
    void getAllZonesByZoneTypeIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where zoneTypeId is less than or equal to DEFAULT_ZONE_TYPE_ID
        defaultZoneShouldBeFound("zoneTypeId.lessThanOrEqual=" + DEFAULT_ZONE_TYPE_ID);

        // Get all the zoneList where zoneTypeId is less than or equal to SMALLER_ZONE_TYPE_ID
        defaultZoneShouldNotBeFound("zoneTypeId.lessThanOrEqual=" + SMALLER_ZONE_TYPE_ID);
    }

    @Test
    @Transactional
    void getAllZonesByZoneTypeIdIsLessThanSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where zoneTypeId is less than DEFAULT_ZONE_TYPE_ID
        defaultZoneShouldNotBeFound("zoneTypeId.lessThan=" + DEFAULT_ZONE_TYPE_ID);

        // Get all the zoneList where zoneTypeId is less than UPDATED_ZONE_TYPE_ID
        defaultZoneShouldBeFound("zoneTypeId.lessThan=" + UPDATED_ZONE_TYPE_ID);
    }

    @Test
    @Transactional
    void getAllZonesByZoneTypeIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where zoneTypeId is greater than DEFAULT_ZONE_TYPE_ID
        defaultZoneShouldNotBeFound("zoneTypeId.greaterThan=" + DEFAULT_ZONE_TYPE_ID);

        // Get all the zoneList where zoneTypeId is greater than SMALLER_ZONE_TYPE_ID
        defaultZoneShouldBeFound("zoneTypeId.greaterThan=" + SMALLER_ZONE_TYPE_ID);
    }

    @Test
    @Transactional
    void getAllZonesByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where createdBy equals to DEFAULT_CREATED_BY
        defaultZoneShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the zoneList where createdBy equals to UPDATED_CREATED_BY
        defaultZoneShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllZonesByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where createdBy not equals to DEFAULT_CREATED_BY
        defaultZoneShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the zoneList where createdBy not equals to UPDATED_CREATED_BY
        defaultZoneShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllZonesByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultZoneShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the zoneList where createdBy equals to UPDATED_CREATED_BY
        defaultZoneShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllZonesByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where createdBy is not null
        defaultZoneShouldBeFound("createdBy.specified=true");

        // Get all the zoneList where createdBy is null
        defaultZoneShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    void getAllZonesByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where createdBy contains DEFAULT_CREATED_BY
        defaultZoneShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the zoneList where createdBy contains UPDATED_CREATED_BY
        defaultZoneShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllZonesByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where createdBy does not contain DEFAULT_CREATED_BY
        defaultZoneShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the zoneList where createdBy does not contain UPDATED_CREATED_BY
        defaultZoneShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllZonesByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where createdDate equals to DEFAULT_CREATED_DATE
        defaultZoneShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the zoneList where createdDate equals to UPDATED_CREATED_DATE
        defaultZoneShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllZonesByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultZoneShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the zoneList where createdDate not equals to UPDATED_CREATED_DATE
        defaultZoneShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllZonesByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultZoneShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the zoneList where createdDate equals to UPDATED_CREATED_DATE
        defaultZoneShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllZonesByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where createdDate is not null
        defaultZoneShouldBeFound("createdDate.specified=true");

        // Get all the zoneList where createdDate is null
        defaultZoneShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    void getAllZonesByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultZoneShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the zoneList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultZoneShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllZonesByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultZoneShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the zoneList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultZoneShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllZonesByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultZoneShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the zoneList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultZoneShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllZonesByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where lastModifiedBy is not null
        defaultZoneShouldBeFound("lastModifiedBy.specified=true");

        // Get all the zoneList where lastModifiedBy is null
        defaultZoneShouldNotBeFound("lastModifiedBy.specified=false");
    }

    @Test
    @Transactional
    void getAllZonesByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultZoneShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the zoneList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultZoneShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllZonesByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultZoneShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the zoneList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultZoneShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllZonesByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultZoneShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the zoneList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultZoneShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllZonesByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultZoneShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the zoneList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultZoneShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllZonesByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultZoneShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the zoneList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultZoneShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllZonesByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        // Get all the zoneList where lastModifiedDate is not null
        defaultZoneShouldBeFound("lastModifiedDate.specified=true");

        // Get all the zoneList where lastModifiedDate is null
        defaultZoneShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    void getAllZonesByTerritoryIsEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);
        Territory territory;
        if (TestUtil.findAll(em, Territory.class).isEmpty()) {
            territory = TerritoryResourceIT.createEntity(em);
            em.persist(territory);
            em.flush();
        } else {
            territory = TestUtil.findAll(em, Territory.class).get(0);
        }
        em.persist(territory);
        em.flush();
        zone.setTerritory(territory);
        zoneRepository.saveAndFlush(zone);
        Long territoryId = territory.getId();

        // Get all the zoneList where territory equals to territoryId
        defaultZoneShouldBeFound("territoryId.equals=" + territoryId);

        // Get all the zoneList where territory equals to (territoryId + 1)
        defaultZoneShouldNotBeFound("territoryId.equals=" + (territoryId + 1));
    }

    @Test
    @Transactional
    void getAllZonesByConfigurationPlansIsEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);
        ConfigurationPlan configurationPlans;
        if (TestUtil.findAll(em, ConfigurationPlan.class).isEmpty()) {
            configurationPlans = ConfigurationPlanResourceIT.createEntity(em);
            em.persist(configurationPlans);
            em.flush();
        } else {
            configurationPlans = TestUtil.findAll(em, ConfigurationPlan.class).get(0);
        }
        em.persist(configurationPlans);
        em.flush();
        zone.addConfigurationPlans(configurationPlans);
        zoneRepository.saveAndFlush(zone);
        Long configurationPlansId = configurationPlans.getId();

        // Get all the zoneList where configurationPlans equals to configurationPlansId
        defaultZoneShouldBeFound("configurationPlansId.equals=" + configurationPlansId);

        // Get all the zoneList where configurationPlans equals to (configurationPlansId + 1)
        defaultZoneShouldNotBeFound("configurationPlansId.equals=" + (configurationPlansId + 1));
    }

    @Test
    @Transactional
    void getAllZonesByPartnerIsEqualToSomething() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);
        Partner partner;
        if (TestUtil.findAll(em, Partner.class).isEmpty()) {
            partner = PartnerResourceIT.createEntity(em);
            em.persist(partner);
            em.flush();
        } else {
            partner = TestUtil.findAll(em, Partner.class).get(0);
        }
        em.persist(partner);
        em.flush();
        zone.addPartner(partner);
        zoneRepository.saveAndFlush(zone);
        Long partnerId = partner.getId();

        // Get all the zoneList where partner equals to partnerId
        defaultZoneShouldBeFound("partnerId.equals=" + partnerId);

        // Get all the zoneList where partner equals to (partnerId + 1)
        defaultZoneShouldNotBeFound("partnerId.equals=" + (partnerId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultZoneShouldBeFound(String filter) throws Exception {
        restZoneMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(zone.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE)))
            .andExpect(jsonPath("$.[*].zoneTypeId").value(hasItem(DEFAULT_ZONE_TYPE_ID.intValue())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())));

        // Check, that the count call also returns 1
        restZoneMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultZoneShouldNotBeFound(String filter) throws Exception {
        restZoneMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restZoneMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingZone() throws Exception {
        // Get the zone
        restZoneMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewZone() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        int databaseSizeBeforeUpdate = zoneRepository.findAll().size();

        // Update the zone
        Zone updatedZone = zoneRepository.findById(zone.getId()).get();
        // Disconnect from session so that the updates on updatedZone are not directly saved in db
        em.detach(updatedZone);
        updatedZone
            .code(UPDATED_CODE)
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .state(UPDATED_STATE)
            .zoneTypeId(UPDATED_ZONE_TYPE_ID)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE);
        ZoneDTO zoneDTO = zoneMapper.toDto(updatedZone);

        restZoneMockMvc
            .perform(
                put(ENTITY_API_URL_ID, zoneDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(zoneDTO))
            )
            .andExpect(status().isOk());

        // Validate the Zone in the database
        List<Zone> zoneList = zoneRepository.findAll();
        assertThat(zoneList).hasSize(databaseSizeBeforeUpdate);
        Zone testZone = zoneList.get(zoneList.size() - 1);
        assertThat(testZone.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testZone.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testZone.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testZone.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testZone.getZoneTypeId()).isEqualTo(UPDATED_ZONE_TYPE_ID);
        assertThat(testZone.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testZone.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testZone.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testZone.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void putNonExistingZone() throws Exception {
        int databaseSizeBeforeUpdate = zoneRepository.findAll().size();
        zone.setId(count.incrementAndGet());

        // Create the Zone
        ZoneDTO zoneDTO = zoneMapper.toDto(zone);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restZoneMockMvc
            .perform(
                put(ENTITY_API_URL_ID, zoneDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(zoneDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Zone in the database
        List<Zone> zoneList = zoneRepository.findAll();
        assertThat(zoneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchZone() throws Exception {
        int databaseSizeBeforeUpdate = zoneRepository.findAll().size();
        zone.setId(count.incrementAndGet());

        // Create the Zone
        ZoneDTO zoneDTO = zoneMapper.toDto(zone);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restZoneMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(zoneDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Zone in the database
        List<Zone> zoneList = zoneRepository.findAll();
        assertThat(zoneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamZone() throws Exception {
        int databaseSizeBeforeUpdate = zoneRepository.findAll().size();
        zone.setId(count.incrementAndGet());

        // Create the Zone
        ZoneDTO zoneDTO = zoneMapper.toDto(zone);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restZoneMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(zoneDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Zone in the database
        List<Zone> zoneList = zoneRepository.findAll();
        assertThat(zoneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateZoneWithPatch() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        int databaseSizeBeforeUpdate = zoneRepository.findAll().size();

        // Update the zone using partial update
        Zone partialUpdatedZone = new Zone();
        partialUpdatedZone.setId(zone.getId());

        partialUpdatedZone.state(UPDATED_STATE).createdBy(UPDATED_CREATED_BY).lastModifiedDate(UPDATED_LAST_MODIFIED_DATE);

        restZoneMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedZone.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedZone))
            )
            .andExpect(status().isOk());

        // Validate the Zone in the database
        List<Zone> zoneList = zoneRepository.findAll();
        assertThat(zoneList).hasSize(databaseSizeBeforeUpdate);
        Zone testZone = zoneList.get(zoneList.size() - 1);
        assertThat(testZone.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testZone.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testZone.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testZone.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testZone.getZoneTypeId()).isEqualTo(DEFAULT_ZONE_TYPE_ID);
        assertThat(testZone.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testZone.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testZone.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testZone.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void fullUpdateZoneWithPatch() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        int databaseSizeBeforeUpdate = zoneRepository.findAll().size();

        // Update the zone using partial update
        Zone partialUpdatedZone = new Zone();
        partialUpdatedZone.setId(zone.getId());

        partialUpdatedZone
            .code(UPDATED_CODE)
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .state(UPDATED_STATE)
            .zoneTypeId(UPDATED_ZONE_TYPE_ID)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE);

        restZoneMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedZone.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedZone))
            )
            .andExpect(status().isOk());

        // Validate the Zone in the database
        List<Zone> zoneList = zoneRepository.findAll();
        assertThat(zoneList).hasSize(databaseSizeBeforeUpdate);
        Zone testZone = zoneList.get(zoneList.size() - 1);
        assertThat(testZone.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testZone.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testZone.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testZone.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testZone.getZoneTypeId()).isEqualTo(UPDATED_ZONE_TYPE_ID);
        assertThat(testZone.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testZone.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testZone.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testZone.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void patchNonExistingZone() throws Exception {
        int databaseSizeBeforeUpdate = zoneRepository.findAll().size();
        zone.setId(count.incrementAndGet());

        // Create the Zone
        ZoneDTO zoneDTO = zoneMapper.toDto(zone);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restZoneMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, zoneDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(zoneDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Zone in the database
        List<Zone> zoneList = zoneRepository.findAll();
        assertThat(zoneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchZone() throws Exception {
        int databaseSizeBeforeUpdate = zoneRepository.findAll().size();
        zone.setId(count.incrementAndGet());

        // Create the Zone
        ZoneDTO zoneDTO = zoneMapper.toDto(zone);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restZoneMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(zoneDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Zone in the database
        List<Zone> zoneList = zoneRepository.findAll();
        assertThat(zoneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamZone() throws Exception {
        int databaseSizeBeforeUpdate = zoneRepository.findAll().size();
        zone.setId(count.incrementAndGet());

        // Create the Zone
        ZoneDTO zoneDTO = zoneMapper.toDto(zone);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restZoneMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(zoneDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Zone in the database
        List<Zone> zoneList = zoneRepository.findAll();
        assertThat(zoneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteZone() throws Exception {
        // Initialize the database
        zoneRepository.saveAndFlush(zone);

        int databaseSizeBeforeDelete = zoneRepository.findAll().size();

        // Delete the zone
        restZoneMockMvc
            .perform(delete(ENTITY_API_URL_ID, zone.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Zone> zoneList = zoneRepository.findAll();
        assertThat(zoneList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
