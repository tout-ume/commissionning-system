package sn.free.commissioning.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.IntegrationTest;
import sn.free.commissioning.domain.CommissioningPlan;
import sn.free.commissioning.domain.ServiceType;
import sn.free.commissioning.domain.enumeration.CommissioningPlanType;
import sn.free.commissioning.domain.enumeration.ServiceTypeStatus;
import sn.free.commissioning.repository.ServiceTypeRepository;
import sn.free.commissioning.service.criteria.ServiceTypeCriteria;
import sn.free.commissioning.service.dto.ServiceTypeDTO;
import sn.free.commissioning.service.mapper.ServiceTypeMapper;

/**
 * Integration tests for the {@link ServiceTypeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ServiceTypeResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final ServiceTypeStatus DEFAULT_STATE = ServiceTypeStatus.ACTIVATED;
    private static final ServiceTypeStatus UPDATED_STATE = ServiceTypeStatus.DEACTIVATED;

    private static final Boolean DEFAULT_IS_COS_RELATED = false;
    private static final Boolean UPDATED_IS_COS_RELATED = true;

    private static final CommissioningPlanType DEFAULT_COMMISSIONING_PLAN_TYPE = CommissioningPlanType.MFS;
    private static final CommissioningPlanType UPDATED_COMMISSIONING_PLAN_TYPE = CommissioningPlanType.MOBILE;

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/service-types";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ServiceTypeRepository serviceTypeRepository;

    @Autowired
    private ServiceTypeMapper serviceTypeMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restServiceTypeMockMvc;

    private ServiceType serviceType;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ServiceType createEntity(EntityManager em) {
        ServiceType serviceType = new ServiceType()
            .name(DEFAULT_NAME)
            .code(DEFAULT_CODE)
            .description(DEFAULT_DESCRIPTION)
            .state(DEFAULT_STATE)
            .isCosRelated(DEFAULT_IS_COS_RELATED)
            .commissioningPlanType(DEFAULT_COMMISSIONING_PLAN_TYPE)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE);
        return serviceType;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ServiceType createUpdatedEntity(EntityManager em) {
        ServiceType serviceType = new ServiceType()
            .name(UPDATED_NAME)
            .code(UPDATED_CODE)
            .description(UPDATED_DESCRIPTION)
            .state(UPDATED_STATE)
            .isCosRelated(UPDATED_IS_COS_RELATED)
            .commissioningPlanType(UPDATED_COMMISSIONING_PLAN_TYPE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE);
        return serviceType;
    }

    @BeforeEach
    public void initTest() {
        serviceType = createEntity(em);
    }

    @Test
    @Transactional
    void createServiceType() throws Exception {
        int databaseSizeBeforeCreate = serviceTypeRepository.findAll().size();
        // Create the ServiceType
        ServiceTypeDTO serviceTypeDTO = serviceTypeMapper.toDto(serviceType);
        restServiceTypeMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(serviceTypeDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ServiceType in the database
        List<ServiceType> serviceTypeList = serviceTypeRepository.findAll();
        assertThat(serviceTypeList).hasSize(databaseSizeBeforeCreate + 1);
        ServiceType testServiceType = serviceTypeList.get(serviceTypeList.size() - 1);
        assertThat(testServiceType.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testServiceType.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testServiceType.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testServiceType.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testServiceType.getIsCosRelated()).isEqualTo(DEFAULT_IS_COS_RELATED);
        assertThat(testServiceType.getCommissioningPlanType()).isEqualTo(DEFAULT_COMMISSIONING_PLAN_TYPE);
        assertThat(testServiceType.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testServiceType.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testServiceType.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testServiceType.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void createServiceTypeWithExistingId() throws Exception {
        // Create the ServiceType with an existing ID
        serviceType.setId(1L);
        ServiceTypeDTO serviceTypeDTO = serviceTypeMapper.toDto(serviceType);

        int databaseSizeBeforeCreate = serviceTypeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restServiceTypeMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(serviceTypeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ServiceType in the database
        List<ServiceType> serviceTypeList = serviceTypeRepository.findAll();
        assertThat(serviceTypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = serviceTypeRepository.findAll().size();
        // set the field null
        serviceType.setName(null);

        // Create the ServiceType, which fails.
        ServiceTypeDTO serviceTypeDTO = serviceTypeMapper.toDto(serviceType);

        restServiceTypeMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(serviceTypeDTO))
            )
            .andExpect(status().isBadRequest());

        List<ServiceType> serviceTypeList = serviceTypeRepository.findAll();
        assertThat(serviceTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = serviceTypeRepository.findAll().size();
        // set the field null
        serviceType.setCode(null);

        // Create the ServiceType, which fails.
        ServiceTypeDTO serviceTypeDTO = serviceTypeMapper.toDto(serviceType);

        restServiceTypeMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(serviceTypeDTO))
            )
            .andExpect(status().isBadRequest());

        List<ServiceType> serviceTypeList = serviceTypeRepository.findAll();
        assertThat(serviceTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkStateIsRequired() throws Exception {
        int databaseSizeBeforeTest = serviceTypeRepository.findAll().size();
        // set the field null
        serviceType.setState(null);

        // Create the ServiceType, which fails.
        ServiceTypeDTO serviceTypeDTO = serviceTypeMapper.toDto(serviceType);

        restServiceTypeMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(serviceTypeDTO))
            )
            .andExpect(status().isBadRequest());

        List<ServiceType> serviceTypeList = serviceTypeRepository.findAll();
        assertThat(serviceTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkIsCosRelatedIsRequired() throws Exception {
        int databaseSizeBeforeTest = serviceTypeRepository.findAll().size();
        // set the field null
        serviceType.setIsCosRelated(null);

        // Create the ServiceType, which fails.
        ServiceTypeDTO serviceTypeDTO = serviceTypeMapper.toDto(serviceType);

        restServiceTypeMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(serviceTypeDTO))
            )
            .andExpect(status().isBadRequest());

        List<ServiceType> serviceTypeList = serviceTypeRepository.findAll();
        assertThat(serviceTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCreatedByIsRequired() throws Exception {
        int databaseSizeBeforeTest = serviceTypeRepository.findAll().size();
        // set the field null
        serviceType.setCreatedBy(null);

        // Create the ServiceType, which fails.
        ServiceTypeDTO serviceTypeDTO = serviceTypeMapper.toDto(serviceType);

        restServiceTypeMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(serviceTypeDTO))
            )
            .andExpect(status().isBadRequest());

        List<ServiceType> serviceTypeList = serviceTypeRepository.findAll();
        assertThat(serviceTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllServiceTypes() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList
        restServiceTypeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(serviceType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
            .andExpect(jsonPath("$.[*].isCosRelated").value(hasItem(DEFAULT_IS_COS_RELATED.booleanValue())))
            .andExpect(jsonPath("$.[*].commissioningPlanType").value(hasItem(DEFAULT_COMMISSIONING_PLAN_TYPE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())));
    }

    @Test
    @Transactional
    void getServiceType() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get the serviceType
        restServiceTypeMockMvc
            .perform(get(ENTITY_API_URL_ID, serviceType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(serviceType.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE.toString()))
            .andExpect(jsonPath("$.isCosRelated").value(DEFAULT_IS_COS_RELATED.booleanValue()))
            .andExpect(jsonPath("$.commissioningPlanType").value(DEFAULT_COMMISSIONING_PLAN_TYPE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()));
    }

    @Test
    @Transactional
    void getServiceTypesByIdFiltering() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        Long id = serviceType.getId();

        defaultServiceTypeShouldBeFound("id.equals=" + id);
        defaultServiceTypeShouldNotBeFound("id.notEquals=" + id);

        defaultServiceTypeShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultServiceTypeShouldNotBeFound("id.greaterThan=" + id);

        defaultServiceTypeShouldBeFound("id.lessThanOrEqual=" + id);
        defaultServiceTypeShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllServiceTypesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where name equals to DEFAULT_NAME
        defaultServiceTypeShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the serviceTypeList where name equals to UPDATED_NAME
        defaultServiceTypeShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllServiceTypesByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where name not equals to DEFAULT_NAME
        defaultServiceTypeShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the serviceTypeList where name not equals to UPDATED_NAME
        defaultServiceTypeShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllServiceTypesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where name in DEFAULT_NAME or UPDATED_NAME
        defaultServiceTypeShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the serviceTypeList where name equals to UPDATED_NAME
        defaultServiceTypeShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllServiceTypesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where name is not null
        defaultServiceTypeShouldBeFound("name.specified=true");

        // Get all the serviceTypeList where name is null
        defaultServiceTypeShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllServiceTypesByNameContainsSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where name contains DEFAULT_NAME
        defaultServiceTypeShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the serviceTypeList where name contains UPDATED_NAME
        defaultServiceTypeShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllServiceTypesByNameNotContainsSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where name does not contain DEFAULT_NAME
        defaultServiceTypeShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the serviceTypeList where name does not contain UPDATED_NAME
        defaultServiceTypeShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllServiceTypesByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where code equals to DEFAULT_CODE
        defaultServiceTypeShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the serviceTypeList where code equals to UPDATED_CODE
        defaultServiceTypeShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllServiceTypesByCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where code not equals to DEFAULT_CODE
        defaultServiceTypeShouldNotBeFound("code.notEquals=" + DEFAULT_CODE);

        // Get all the serviceTypeList where code not equals to UPDATED_CODE
        defaultServiceTypeShouldBeFound("code.notEquals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllServiceTypesByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where code in DEFAULT_CODE or UPDATED_CODE
        defaultServiceTypeShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the serviceTypeList where code equals to UPDATED_CODE
        defaultServiceTypeShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllServiceTypesByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where code is not null
        defaultServiceTypeShouldBeFound("code.specified=true");

        // Get all the serviceTypeList where code is null
        defaultServiceTypeShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    void getAllServiceTypesByCodeContainsSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where code contains DEFAULT_CODE
        defaultServiceTypeShouldBeFound("code.contains=" + DEFAULT_CODE);

        // Get all the serviceTypeList where code contains UPDATED_CODE
        defaultServiceTypeShouldNotBeFound("code.contains=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllServiceTypesByCodeNotContainsSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where code does not contain DEFAULT_CODE
        defaultServiceTypeShouldNotBeFound("code.doesNotContain=" + DEFAULT_CODE);

        // Get all the serviceTypeList where code does not contain UPDATED_CODE
        defaultServiceTypeShouldBeFound("code.doesNotContain=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllServiceTypesByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where description equals to DEFAULT_DESCRIPTION
        defaultServiceTypeShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the serviceTypeList where description equals to UPDATED_DESCRIPTION
        defaultServiceTypeShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllServiceTypesByDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where description not equals to DEFAULT_DESCRIPTION
        defaultServiceTypeShouldNotBeFound("description.notEquals=" + DEFAULT_DESCRIPTION);

        // Get all the serviceTypeList where description not equals to UPDATED_DESCRIPTION
        defaultServiceTypeShouldBeFound("description.notEquals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllServiceTypesByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultServiceTypeShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the serviceTypeList where description equals to UPDATED_DESCRIPTION
        defaultServiceTypeShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllServiceTypesByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where description is not null
        defaultServiceTypeShouldBeFound("description.specified=true");

        // Get all the serviceTypeList where description is null
        defaultServiceTypeShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    void getAllServiceTypesByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where description contains DEFAULT_DESCRIPTION
        defaultServiceTypeShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the serviceTypeList where description contains UPDATED_DESCRIPTION
        defaultServiceTypeShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllServiceTypesByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where description does not contain DEFAULT_DESCRIPTION
        defaultServiceTypeShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the serviceTypeList where description does not contain UPDATED_DESCRIPTION
        defaultServiceTypeShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllServiceTypesByStateIsEqualToSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where state equals to DEFAULT_STATE
        defaultServiceTypeShouldBeFound("state.equals=" + DEFAULT_STATE);

        // Get all the serviceTypeList where state equals to UPDATED_STATE
        defaultServiceTypeShouldNotBeFound("state.equals=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    void getAllServiceTypesByStateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where state not equals to DEFAULT_STATE
        defaultServiceTypeShouldNotBeFound("state.notEquals=" + DEFAULT_STATE);

        // Get all the serviceTypeList where state not equals to UPDATED_STATE
        defaultServiceTypeShouldBeFound("state.notEquals=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    void getAllServiceTypesByStateIsInShouldWork() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where state in DEFAULT_STATE or UPDATED_STATE
        defaultServiceTypeShouldBeFound("state.in=" + DEFAULT_STATE + "," + UPDATED_STATE);

        // Get all the serviceTypeList where state equals to UPDATED_STATE
        defaultServiceTypeShouldNotBeFound("state.in=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    void getAllServiceTypesByStateIsNullOrNotNull() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where state is not null
        defaultServiceTypeShouldBeFound("state.specified=true");

        // Get all the serviceTypeList where state is null
        defaultServiceTypeShouldNotBeFound("state.specified=false");
    }

    @Test
    @Transactional
    void getAllServiceTypesByIsCosRelatedIsEqualToSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where isCosRelated equals to DEFAULT_IS_COS_RELATED
        defaultServiceTypeShouldBeFound("isCosRelated.equals=" + DEFAULT_IS_COS_RELATED);

        // Get all the serviceTypeList where isCosRelated equals to UPDATED_IS_COS_RELATED
        defaultServiceTypeShouldNotBeFound("isCosRelated.equals=" + UPDATED_IS_COS_RELATED);
    }

    @Test
    @Transactional
    void getAllServiceTypesByIsCosRelatedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where isCosRelated not equals to DEFAULT_IS_COS_RELATED
        defaultServiceTypeShouldNotBeFound("isCosRelated.notEquals=" + DEFAULT_IS_COS_RELATED);

        // Get all the serviceTypeList where isCosRelated not equals to UPDATED_IS_COS_RELATED
        defaultServiceTypeShouldBeFound("isCosRelated.notEquals=" + UPDATED_IS_COS_RELATED);
    }

    @Test
    @Transactional
    void getAllServiceTypesByIsCosRelatedIsInShouldWork() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where isCosRelated in DEFAULT_IS_COS_RELATED or UPDATED_IS_COS_RELATED
        defaultServiceTypeShouldBeFound("isCosRelated.in=" + DEFAULT_IS_COS_RELATED + "," + UPDATED_IS_COS_RELATED);

        // Get all the serviceTypeList where isCosRelated equals to UPDATED_IS_COS_RELATED
        defaultServiceTypeShouldNotBeFound("isCosRelated.in=" + UPDATED_IS_COS_RELATED);
    }

    @Test
    @Transactional
    void getAllServiceTypesByIsCosRelatedIsNullOrNotNull() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where isCosRelated is not null
        defaultServiceTypeShouldBeFound("isCosRelated.specified=true");

        // Get all the serviceTypeList where isCosRelated is null
        defaultServiceTypeShouldNotBeFound("isCosRelated.specified=false");
    }

    @Test
    @Transactional
    void getAllServiceTypesByCommissioningPlanTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where commissioningPlanType equals to DEFAULT_COMMISSIONING_PLAN_TYPE
        defaultServiceTypeShouldBeFound("commissioningPlanType.equals=" + DEFAULT_COMMISSIONING_PLAN_TYPE);

        // Get all the serviceTypeList where commissioningPlanType equals to UPDATED_COMMISSIONING_PLAN_TYPE
        defaultServiceTypeShouldNotBeFound("commissioningPlanType.equals=" + UPDATED_COMMISSIONING_PLAN_TYPE);
    }

    @Test
    @Transactional
    void getAllServiceTypesByCommissioningPlanTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where commissioningPlanType not equals to DEFAULT_COMMISSIONING_PLAN_TYPE
        defaultServiceTypeShouldNotBeFound("commissioningPlanType.notEquals=" + DEFAULT_COMMISSIONING_PLAN_TYPE);

        // Get all the serviceTypeList where commissioningPlanType not equals to UPDATED_COMMISSIONING_PLAN_TYPE
        defaultServiceTypeShouldBeFound("commissioningPlanType.notEquals=" + UPDATED_COMMISSIONING_PLAN_TYPE);
    }

    @Test
    @Transactional
    void getAllServiceTypesByCommissioningPlanTypeIsInShouldWork() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where commissioningPlanType in DEFAULT_COMMISSIONING_PLAN_TYPE or UPDATED_COMMISSIONING_PLAN_TYPE
        defaultServiceTypeShouldBeFound(
            "commissioningPlanType.in=" + DEFAULT_COMMISSIONING_PLAN_TYPE + "," + UPDATED_COMMISSIONING_PLAN_TYPE
        );

        // Get all the serviceTypeList where commissioningPlanType equals to UPDATED_COMMISSIONING_PLAN_TYPE
        defaultServiceTypeShouldNotBeFound("commissioningPlanType.in=" + UPDATED_COMMISSIONING_PLAN_TYPE);
    }

    @Test
    @Transactional
    void getAllServiceTypesByCommissioningPlanTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where commissioningPlanType is not null
        defaultServiceTypeShouldBeFound("commissioningPlanType.specified=true");

        // Get all the serviceTypeList where commissioningPlanType is null
        defaultServiceTypeShouldNotBeFound("commissioningPlanType.specified=false");
    }

    @Test
    @Transactional
    void getAllServiceTypesByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where createdBy equals to DEFAULT_CREATED_BY
        defaultServiceTypeShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the serviceTypeList where createdBy equals to UPDATED_CREATED_BY
        defaultServiceTypeShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllServiceTypesByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where createdBy not equals to DEFAULT_CREATED_BY
        defaultServiceTypeShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the serviceTypeList where createdBy not equals to UPDATED_CREATED_BY
        defaultServiceTypeShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllServiceTypesByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultServiceTypeShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the serviceTypeList where createdBy equals to UPDATED_CREATED_BY
        defaultServiceTypeShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllServiceTypesByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where createdBy is not null
        defaultServiceTypeShouldBeFound("createdBy.specified=true");

        // Get all the serviceTypeList where createdBy is null
        defaultServiceTypeShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    void getAllServiceTypesByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where createdBy contains DEFAULT_CREATED_BY
        defaultServiceTypeShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the serviceTypeList where createdBy contains UPDATED_CREATED_BY
        defaultServiceTypeShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllServiceTypesByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where createdBy does not contain DEFAULT_CREATED_BY
        defaultServiceTypeShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the serviceTypeList where createdBy does not contain UPDATED_CREATED_BY
        defaultServiceTypeShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllServiceTypesByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where createdDate equals to DEFAULT_CREATED_DATE
        defaultServiceTypeShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the serviceTypeList where createdDate equals to UPDATED_CREATED_DATE
        defaultServiceTypeShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllServiceTypesByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultServiceTypeShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the serviceTypeList where createdDate not equals to UPDATED_CREATED_DATE
        defaultServiceTypeShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllServiceTypesByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultServiceTypeShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the serviceTypeList where createdDate equals to UPDATED_CREATED_DATE
        defaultServiceTypeShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllServiceTypesByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where createdDate is not null
        defaultServiceTypeShouldBeFound("createdDate.specified=true");

        // Get all the serviceTypeList where createdDate is null
        defaultServiceTypeShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    void getAllServiceTypesByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultServiceTypeShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the serviceTypeList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultServiceTypeShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllServiceTypesByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultServiceTypeShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the serviceTypeList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultServiceTypeShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllServiceTypesByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultServiceTypeShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the serviceTypeList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultServiceTypeShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllServiceTypesByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where lastModifiedBy is not null
        defaultServiceTypeShouldBeFound("lastModifiedBy.specified=true");

        // Get all the serviceTypeList where lastModifiedBy is null
        defaultServiceTypeShouldNotBeFound("lastModifiedBy.specified=false");
    }

    @Test
    @Transactional
    void getAllServiceTypesByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultServiceTypeShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the serviceTypeList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultServiceTypeShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllServiceTypesByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultServiceTypeShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the serviceTypeList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultServiceTypeShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllServiceTypesByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultServiceTypeShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the serviceTypeList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultServiceTypeShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllServiceTypesByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultServiceTypeShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the serviceTypeList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultServiceTypeShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllServiceTypesByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultServiceTypeShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the serviceTypeList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultServiceTypeShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllServiceTypesByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        // Get all the serviceTypeList where lastModifiedDate is not null
        defaultServiceTypeShouldBeFound("lastModifiedDate.specified=true");

        // Get all the serviceTypeList where lastModifiedDate is null
        defaultServiceTypeShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    void getAllServiceTypesByCommissionPlanIsEqualToSomething() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);
        CommissioningPlan commissionPlan;
        if (TestUtil.findAll(em, CommissioningPlan.class).isEmpty()) {
            commissionPlan = CommissioningPlanResourceIT.createEntity(em);
            em.persist(commissionPlan);
            em.flush();
        } else {
            commissionPlan = TestUtil.findAll(em, CommissioningPlan.class).get(0);
        }
        em.persist(commissionPlan);
        em.flush();
        serviceType.addCommissionPlan(commissionPlan);
        serviceTypeRepository.saveAndFlush(serviceType);
        Long commissionPlanId = commissionPlan.getId();

        // Get all the serviceTypeList where commissionPlan equals to commissionPlanId
        defaultServiceTypeShouldBeFound("commissionPlanId.equals=" + commissionPlanId);

        // Get all the serviceTypeList where commissionPlan equals to (commissionPlanId + 1)
        defaultServiceTypeShouldNotBeFound("commissionPlanId.equals=" + (commissionPlanId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultServiceTypeShouldBeFound(String filter) throws Exception {
        restServiceTypeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(serviceType.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE.toString())))
            .andExpect(jsonPath("$.[*].isCosRelated").value(hasItem(DEFAULT_IS_COS_RELATED.booleanValue())))
            .andExpect(jsonPath("$.[*].commissioningPlanType").value(hasItem(DEFAULT_COMMISSIONING_PLAN_TYPE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())));

        // Check, that the count call also returns 1
        restServiceTypeMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultServiceTypeShouldNotBeFound(String filter) throws Exception {
        restServiceTypeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restServiceTypeMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingServiceType() throws Exception {
        // Get the serviceType
        restServiceTypeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewServiceType() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        int databaseSizeBeforeUpdate = serviceTypeRepository.findAll().size();

        // Update the serviceType
        ServiceType updatedServiceType = serviceTypeRepository.findById(serviceType.getId()).get();
        // Disconnect from session so that the updates on updatedServiceType are not directly saved in db
        em.detach(updatedServiceType);
        updatedServiceType
            .name(UPDATED_NAME)
            .code(UPDATED_CODE)
            .description(UPDATED_DESCRIPTION)
            .state(UPDATED_STATE)
            .isCosRelated(UPDATED_IS_COS_RELATED)
            .commissioningPlanType(UPDATED_COMMISSIONING_PLAN_TYPE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE);
        ServiceTypeDTO serviceTypeDTO = serviceTypeMapper.toDto(updatedServiceType);

        restServiceTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, serviceTypeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(serviceTypeDTO))
            )
            .andExpect(status().isOk());

        // Validate the ServiceType in the database
        List<ServiceType> serviceTypeList = serviceTypeRepository.findAll();
        assertThat(serviceTypeList).hasSize(databaseSizeBeforeUpdate);
        ServiceType testServiceType = serviceTypeList.get(serviceTypeList.size() - 1);
        assertThat(testServiceType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testServiceType.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testServiceType.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testServiceType.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testServiceType.getIsCosRelated()).isEqualTo(UPDATED_IS_COS_RELATED);
        assertThat(testServiceType.getCommissioningPlanType()).isEqualTo(UPDATED_COMMISSIONING_PLAN_TYPE);
        assertThat(testServiceType.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testServiceType.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testServiceType.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testServiceType.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void putNonExistingServiceType() throws Exception {
        int databaseSizeBeforeUpdate = serviceTypeRepository.findAll().size();
        serviceType.setId(count.incrementAndGet());

        // Create the ServiceType
        ServiceTypeDTO serviceTypeDTO = serviceTypeMapper.toDto(serviceType);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restServiceTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, serviceTypeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(serviceTypeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ServiceType in the database
        List<ServiceType> serviceTypeList = serviceTypeRepository.findAll();
        assertThat(serviceTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchServiceType() throws Exception {
        int databaseSizeBeforeUpdate = serviceTypeRepository.findAll().size();
        serviceType.setId(count.incrementAndGet());

        // Create the ServiceType
        ServiceTypeDTO serviceTypeDTO = serviceTypeMapper.toDto(serviceType);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restServiceTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(serviceTypeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ServiceType in the database
        List<ServiceType> serviceTypeList = serviceTypeRepository.findAll();
        assertThat(serviceTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamServiceType() throws Exception {
        int databaseSizeBeforeUpdate = serviceTypeRepository.findAll().size();
        serviceType.setId(count.incrementAndGet());

        // Create the ServiceType
        ServiceTypeDTO serviceTypeDTO = serviceTypeMapper.toDto(serviceType);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restServiceTypeMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(serviceTypeDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the ServiceType in the database
        List<ServiceType> serviceTypeList = serviceTypeRepository.findAll();
        assertThat(serviceTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateServiceTypeWithPatch() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        int databaseSizeBeforeUpdate = serviceTypeRepository.findAll().size();

        // Update the serviceType using partial update
        ServiceType partialUpdatedServiceType = new ServiceType();
        partialUpdatedServiceType.setId(serviceType.getId());

        partialUpdatedServiceType
            .code(UPDATED_CODE)
            .description(UPDATED_DESCRIPTION)
            .state(UPDATED_STATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE);

        restServiceTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedServiceType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedServiceType))
            )
            .andExpect(status().isOk());

        // Validate the ServiceType in the database
        List<ServiceType> serviceTypeList = serviceTypeRepository.findAll();
        assertThat(serviceTypeList).hasSize(databaseSizeBeforeUpdate);
        ServiceType testServiceType = serviceTypeList.get(serviceTypeList.size() - 1);
        assertThat(testServiceType.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testServiceType.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testServiceType.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testServiceType.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testServiceType.getIsCosRelated()).isEqualTo(DEFAULT_IS_COS_RELATED);
        assertThat(testServiceType.getCommissioningPlanType()).isEqualTo(DEFAULT_COMMISSIONING_PLAN_TYPE);
        assertThat(testServiceType.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testServiceType.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testServiceType.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testServiceType.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void fullUpdateServiceTypeWithPatch() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        int databaseSizeBeforeUpdate = serviceTypeRepository.findAll().size();

        // Update the serviceType using partial update
        ServiceType partialUpdatedServiceType = new ServiceType();
        partialUpdatedServiceType.setId(serviceType.getId());

        partialUpdatedServiceType
            .name(UPDATED_NAME)
            .code(UPDATED_CODE)
            .description(UPDATED_DESCRIPTION)
            .state(UPDATED_STATE)
            .isCosRelated(UPDATED_IS_COS_RELATED)
            .commissioningPlanType(UPDATED_COMMISSIONING_PLAN_TYPE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE);

        restServiceTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedServiceType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedServiceType))
            )
            .andExpect(status().isOk());

        // Validate the ServiceType in the database
        List<ServiceType> serviceTypeList = serviceTypeRepository.findAll();
        assertThat(serviceTypeList).hasSize(databaseSizeBeforeUpdate);
        ServiceType testServiceType = serviceTypeList.get(serviceTypeList.size() - 1);
        assertThat(testServiceType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testServiceType.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testServiceType.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testServiceType.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testServiceType.getIsCosRelated()).isEqualTo(UPDATED_IS_COS_RELATED);
        assertThat(testServiceType.getCommissioningPlanType()).isEqualTo(UPDATED_COMMISSIONING_PLAN_TYPE);
        assertThat(testServiceType.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testServiceType.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testServiceType.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testServiceType.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void patchNonExistingServiceType() throws Exception {
        int databaseSizeBeforeUpdate = serviceTypeRepository.findAll().size();
        serviceType.setId(count.incrementAndGet());

        // Create the ServiceType
        ServiceTypeDTO serviceTypeDTO = serviceTypeMapper.toDto(serviceType);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restServiceTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, serviceTypeDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(serviceTypeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ServiceType in the database
        List<ServiceType> serviceTypeList = serviceTypeRepository.findAll();
        assertThat(serviceTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchServiceType() throws Exception {
        int databaseSizeBeforeUpdate = serviceTypeRepository.findAll().size();
        serviceType.setId(count.incrementAndGet());

        // Create the ServiceType
        ServiceTypeDTO serviceTypeDTO = serviceTypeMapper.toDto(serviceType);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restServiceTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(serviceTypeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ServiceType in the database
        List<ServiceType> serviceTypeList = serviceTypeRepository.findAll();
        assertThat(serviceTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamServiceType() throws Exception {
        int databaseSizeBeforeUpdate = serviceTypeRepository.findAll().size();
        serviceType.setId(count.incrementAndGet());

        // Create the ServiceType
        ServiceTypeDTO serviceTypeDTO = serviceTypeMapper.toDto(serviceType);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restServiceTypeMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(serviceTypeDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ServiceType in the database
        List<ServiceType> serviceTypeList = serviceTypeRepository.findAll();
        assertThat(serviceTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteServiceType() throws Exception {
        // Initialize the database
        serviceTypeRepository.saveAndFlush(serviceType);

        int databaseSizeBeforeDelete = serviceTypeRepository.findAll().size();

        // Delete the serviceType
        restServiceTypeMockMvc
            .perform(delete(ENTITY_API_URL_ID, serviceType.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ServiceType> serviceTypeList = serviceTypeRepository.findAll();
        assertThat(serviceTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
