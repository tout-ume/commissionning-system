package sn.free.commissioning.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.IntegrationTest;
import sn.free.commissioning.domain.Commission;
import sn.free.commissioning.domain.CommissionType;
import sn.free.commissioning.domain.CommissioningPlan;
import sn.free.commissioning.domain.ConfigurationPlan;
import sn.free.commissioning.domain.PartnerProfile;
import sn.free.commissioning.domain.Zone;
import sn.free.commissioning.repository.ConfigurationPlanRepository;
import sn.free.commissioning.service.ConfigurationPlanService;
import sn.free.commissioning.service.criteria.ConfigurationPlanCriteria;
import sn.free.commissioning.service.dto.ConfigurationPlanDTO;
import sn.free.commissioning.service.mapper.ConfigurationPlanMapper;

/**
 * Integration tests for the {@link ConfigurationPlanResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class ConfigurationPlanResourceIT {

    private static final String DEFAULT_SPARE_1 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_2 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_3 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_3 = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/configuration-plans";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ConfigurationPlanRepository configurationPlanRepository;

    @Mock
    private ConfigurationPlanRepository configurationPlanRepositoryMock;

    @Autowired
    private ConfigurationPlanMapper configurationPlanMapper;

    @Mock
    private ConfigurationPlanService configurationPlanServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restConfigurationPlanMockMvc;

    private ConfigurationPlan configurationPlan;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ConfigurationPlan createEntity(EntityManager em) {
        ConfigurationPlan configurationPlan = new ConfigurationPlan()
            .spare1(DEFAULT_SPARE_1)
            .spare2(DEFAULT_SPARE_2)
            .spare3(DEFAULT_SPARE_3);
        return configurationPlan;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ConfigurationPlan createUpdatedEntity(EntityManager em) {
        ConfigurationPlan configurationPlan = new ConfigurationPlan()
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);
        return configurationPlan;
    }

    @BeforeEach
    public void initTest() {
        configurationPlan = createEntity(em);
    }

    @Test
    @Transactional
    void createConfigurationPlan() throws Exception {
        int databaseSizeBeforeCreate = configurationPlanRepository.findAll().size();
        // Create the ConfigurationPlan
        ConfigurationPlanDTO configurationPlanDTO = configurationPlanMapper.toDto(configurationPlan);
        restConfigurationPlanMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(configurationPlanDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ConfigurationPlan in the database
        List<ConfigurationPlan> configurationPlanList = configurationPlanRepository.findAll();
        assertThat(configurationPlanList).hasSize(databaseSizeBeforeCreate + 1);
        ConfigurationPlan testConfigurationPlan = configurationPlanList.get(configurationPlanList.size() - 1);
        assertThat(testConfigurationPlan.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testConfigurationPlan.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
        assertThat(testConfigurationPlan.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
    }

    @Test
    @Transactional
    void createConfigurationPlanWithExistingId() throws Exception {
        // Create the ConfigurationPlan with an existing ID
        configurationPlan.setId(1L);
        ConfigurationPlanDTO configurationPlanDTO = configurationPlanMapper.toDto(configurationPlan);

        int databaseSizeBeforeCreate = configurationPlanRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restConfigurationPlanMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(configurationPlanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ConfigurationPlan in the database
        List<ConfigurationPlan> configurationPlanList = configurationPlanRepository.findAll();
        assertThat(configurationPlanList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllConfigurationPlans() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        // Get all the configurationPlanList
        restConfigurationPlanMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(configurationPlan.getId().intValue())))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllConfigurationPlansWithEagerRelationshipsIsEnabled() throws Exception {
        when(configurationPlanServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restConfigurationPlanMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(configurationPlanServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllConfigurationPlansWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(configurationPlanServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restConfigurationPlanMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(configurationPlanServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    void getConfigurationPlan() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        // Get the configurationPlan
        restConfigurationPlanMockMvc
            .perform(get(ENTITY_API_URL_ID, configurationPlan.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(configurationPlan.getId().intValue()))
            .andExpect(jsonPath("$.spare1").value(DEFAULT_SPARE_1))
            .andExpect(jsonPath("$.spare2").value(DEFAULT_SPARE_2))
            .andExpect(jsonPath("$.spare3").value(DEFAULT_SPARE_3));
    }

    @Test
    @Transactional
    void getConfigurationPlansByIdFiltering() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        Long id = configurationPlan.getId();

        defaultConfigurationPlanShouldBeFound("id.equals=" + id);
        defaultConfigurationPlanShouldNotBeFound("id.notEquals=" + id);

        defaultConfigurationPlanShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultConfigurationPlanShouldNotBeFound("id.greaterThan=" + id);

        defaultConfigurationPlanShouldBeFound("id.lessThanOrEqual=" + id);
        defaultConfigurationPlanShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllConfigurationPlansBySpare1IsEqualToSomething() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        // Get all the configurationPlanList where spare1 equals to DEFAULT_SPARE_1
        defaultConfigurationPlanShouldBeFound("spare1.equals=" + DEFAULT_SPARE_1);

        // Get all the configurationPlanList where spare1 equals to UPDATED_SPARE_1
        defaultConfigurationPlanShouldNotBeFound("spare1.equals=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllConfigurationPlansBySpare1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        // Get all the configurationPlanList where spare1 not equals to DEFAULT_SPARE_1
        defaultConfigurationPlanShouldNotBeFound("spare1.notEquals=" + DEFAULT_SPARE_1);

        // Get all the configurationPlanList where spare1 not equals to UPDATED_SPARE_1
        defaultConfigurationPlanShouldBeFound("spare1.notEquals=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllConfigurationPlansBySpare1IsInShouldWork() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        // Get all the configurationPlanList where spare1 in DEFAULT_SPARE_1 or UPDATED_SPARE_1
        defaultConfigurationPlanShouldBeFound("spare1.in=" + DEFAULT_SPARE_1 + "," + UPDATED_SPARE_1);

        // Get all the configurationPlanList where spare1 equals to UPDATED_SPARE_1
        defaultConfigurationPlanShouldNotBeFound("spare1.in=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllConfigurationPlansBySpare1IsNullOrNotNull() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        // Get all the configurationPlanList where spare1 is not null
        defaultConfigurationPlanShouldBeFound("spare1.specified=true");

        // Get all the configurationPlanList where spare1 is null
        defaultConfigurationPlanShouldNotBeFound("spare1.specified=false");
    }

    @Test
    @Transactional
    void getAllConfigurationPlansBySpare1ContainsSomething() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        // Get all the configurationPlanList where spare1 contains DEFAULT_SPARE_1
        defaultConfigurationPlanShouldBeFound("spare1.contains=" + DEFAULT_SPARE_1);

        // Get all the configurationPlanList where spare1 contains UPDATED_SPARE_1
        defaultConfigurationPlanShouldNotBeFound("spare1.contains=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllConfigurationPlansBySpare1NotContainsSomething() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        // Get all the configurationPlanList where spare1 does not contain DEFAULT_SPARE_1
        defaultConfigurationPlanShouldNotBeFound("spare1.doesNotContain=" + DEFAULT_SPARE_1);

        // Get all the configurationPlanList where spare1 does not contain UPDATED_SPARE_1
        defaultConfigurationPlanShouldBeFound("spare1.doesNotContain=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllConfigurationPlansBySpare2IsEqualToSomething() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        // Get all the configurationPlanList where spare2 equals to DEFAULT_SPARE_2
        defaultConfigurationPlanShouldBeFound("spare2.equals=" + DEFAULT_SPARE_2);

        // Get all the configurationPlanList where spare2 equals to UPDATED_SPARE_2
        defaultConfigurationPlanShouldNotBeFound("spare2.equals=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllConfigurationPlansBySpare2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        // Get all the configurationPlanList where spare2 not equals to DEFAULT_SPARE_2
        defaultConfigurationPlanShouldNotBeFound("spare2.notEquals=" + DEFAULT_SPARE_2);

        // Get all the configurationPlanList where spare2 not equals to UPDATED_SPARE_2
        defaultConfigurationPlanShouldBeFound("spare2.notEquals=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllConfigurationPlansBySpare2IsInShouldWork() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        // Get all the configurationPlanList where spare2 in DEFAULT_SPARE_2 or UPDATED_SPARE_2
        defaultConfigurationPlanShouldBeFound("spare2.in=" + DEFAULT_SPARE_2 + "," + UPDATED_SPARE_2);

        // Get all the configurationPlanList where spare2 equals to UPDATED_SPARE_2
        defaultConfigurationPlanShouldNotBeFound("spare2.in=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllConfigurationPlansBySpare2IsNullOrNotNull() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        // Get all the configurationPlanList where spare2 is not null
        defaultConfigurationPlanShouldBeFound("spare2.specified=true");

        // Get all the configurationPlanList where spare2 is null
        defaultConfigurationPlanShouldNotBeFound("spare2.specified=false");
    }

    @Test
    @Transactional
    void getAllConfigurationPlansBySpare2ContainsSomething() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        // Get all the configurationPlanList where spare2 contains DEFAULT_SPARE_2
        defaultConfigurationPlanShouldBeFound("spare2.contains=" + DEFAULT_SPARE_2);

        // Get all the configurationPlanList where spare2 contains UPDATED_SPARE_2
        defaultConfigurationPlanShouldNotBeFound("spare2.contains=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllConfigurationPlansBySpare2NotContainsSomething() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        // Get all the configurationPlanList where spare2 does not contain DEFAULT_SPARE_2
        defaultConfigurationPlanShouldNotBeFound("spare2.doesNotContain=" + DEFAULT_SPARE_2);

        // Get all the configurationPlanList where spare2 does not contain UPDATED_SPARE_2
        defaultConfigurationPlanShouldBeFound("spare2.doesNotContain=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllConfigurationPlansBySpare3IsEqualToSomething() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        // Get all the configurationPlanList where spare3 equals to DEFAULT_SPARE_3
        defaultConfigurationPlanShouldBeFound("spare3.equals=" + DEFAULT_SPARE_3);

        // Get all the configurationPlanList where spare3 equals to UPDATED_SPARE_3
        defaultConfigurationPlanShouldNotBeFound("spare3.equals=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllConfigurationPlansBySpare3IsNotEqualToSomething() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        // Get all the configurationPlanList where spare3 not equals to DEFAULT_SPARE_3
        defaultConfigurationPlanShouldNotBeFound("spare3.notEquals=" + DEFAULT_SPARE_3);

        // Get all the configurationPlanList where spare3 not equals to UPDATED_SPARE_3
        defaultConfigurationPlanShouldBeFound("spare3.notEquals=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllConfigurationPlansBySpare3IsInShouldWork() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        // Get all the configurationPlanList where spare3 in DEFAULT_SPARE_3 or UPDATED_SPARE_3
        defaultConfigurationPlanShouldBeFound("spare3.in=" + DEFAULT_SPARE_3 + "," + UPDATED_SPARE_3);

        // Get all the configurationPlanList where spare3 equals to UPDATED_SPARE_3
        defaultConfigurationPlanShouldNotBeFound("spare3.in=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllConfigurationPlansBySpare3IsNullOrNotNull() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        // Get all the configurationPlanList where spare3 is not null
        defaultConfigurationPlanShouldBeFound("spare3.specified=true");

        // Get all the configurationPlanList where spare3 is null
        defaultConfigurationPlanShouldNotBeFound("spare3.specified=false");
    }

    @Test
    @Transactional
    void getAllConfigurationPlansBySpare3ContainsSomething() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        // Get all the configurationPlanList where spare3 contains DEFAULT_SPARE_3
        defaultConfigurationPlanShouldBeFound("spare3.contains=" + DEFAULT_SPARE_3);

        // Get all the configurationPlanList where spare3 contains UPDATED_SPARE_3
        defaultConfigurationPlanShouldNotBeFound("spare3.contains=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllConfigurationPlansBySpare3NotContainsSomething() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        // Get all the configurationPlanList where spare3 does not contain DEFAULT_SPARE_3
        defaultConfigurationPlanShouldNotBeFound("spare3.doesNotContain=" + DEFAULT_SPARE_3);

        // Get all the configurationPlanList where spare3 does not contain UPDATED_SPARE_3
        defaultConfigurationPlanShouldBeFound("spare3.doesNotContain=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllConfigurationPlansByCommissionTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);
        CommissionType commissionType;
        if (TestUtil.findAll(em, CommissionType.class).isEmpty()) {
            commissionType = CommissionTypeResourceIT.createEntity(em);
            em.persist(commissionType);
            em.flush();
        } else {
            commissionType = TestUtil.findAll(em, CommissionType.class).get(0);
        }
        em.persist(commissionType);
        em.flush();
        configurationPlan.addCommissionType(commissionType);
        configurationPlanRepository.saveAndFlush(configurationPlan);
        Long commissionTypeId = commissionType.getId();

        // Get all the configurationPlanList where commissionType equals to commissionTypeId
        defaultConfigurationPlanShouldBeFound("commissionTypeId.equals=" + commissionTypeId);

        // Get all the configurationPlanList where commissionType equals to (commissionTypeId + 1)
        defaultConfigurationPlanShouldNotBeFound("commissionTypeId.equals=" + (commissionTypeId + 1));
    }

    @Test
    @Transactional
    void getAllConfigurationPlansByCommissionIsEqualToSomething() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);
        Commission commission;
        if (TestUtil.findAll(em, Commission.class).isEmpty()) {
            commission = CommissionResourceIT.createEntity(em);
            em.persist(commission);
            em.flush();
        } else {
            commission = TestUtil.findAll(em, Commission.class).get(0);
        }
        em.persist(commission);
        em.flush();
        configurationPlan.addCommission(commission);
        configurationPlanRepository.saveAndFlush(configurationPlan);
        Long commissionId = commission.getId();

        // Get all the configurationPlanList where commission equals to commissionId
        defaultConfigurationPlanShouldBeFound("commissionId.equals=" + commissionId);

        // Get all the configurationPlanList where commission equals to (commissionId + 1)
        defaultConfigurationPlanShouldNotBeFound("commissionId.equals=" + (commissionId + 1));
    }

    @Test
    @Transactional
    void getAllConfigurationPlansByPartnerProfileIsEqualToSomething() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);
        PartnerProfile partnerProfile;
        if (TestUtil.findAll(em, PartnerProfile.class).isEmpty()) {
            partnerProfile = PartnerProfileResourceIT.createEntity(em);
            em.persist(partnerProfile);
            em.flush();
        } else {
            partnerProfile = TestUtil.findAll(em, PartnerProfile.class).get(0);
        }
        em.persist(partnerProfile);
        em.flush();
        configurationPlan.setPartnerProfile(partnerProfile);
        configurationPlanRepository.saveAndFlush(configurationPlan);
        Long partnerProfileId = partnerProfile.getId();

        // Get all the configurationPlanList where partnerProfile equals to partnerProfileId
        defaultConfigurationPlanShouldBeFound("partnerProfileId.equals=" + partnerProfileId);

        // Get all the configurationPlanList where partnerProfile equals to (partnerProfileId + 1)
        defaultConfigurationPlanShouldNotBeFound("partnerProfileId.equals=" + (partnerProfileId + 1));
    }

    @Test
    @Transactional
    void getAllConfigurationPlansByCommissioningPlanIsEqualToSomething() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);
        CommissioningPlan commissioningPlan;
        if (TestUtil.findAll(em, CommissioningPlan.class).isEmpty()) {
            commissioningPlan = CommissioningPlanResourceIT.createEntity(em);
            em.persist(commissioningPlan);
            em.flush();
        } else {
            commissioningPlan = TestUtil.findAll(em, CommissioningPlan.class).get(0);
        }
        em.persist(commissioningPlan);
        em.flush();
        configurationPlan.setCommissioningPlan(commissioningPlan);
        configurationPlanRepository.saveAndFlush(configurationPlan);
        Long commissioningPlanId = commissioningPlan.getId();

        // Get all the configurationPlanList where commissioningPlan equals to commissioningPlanId
        defaultConfigurationPlanShouldBeFound("commissioningPlanId.equals=" + commissioningPlanId);

        // Get all the configurationPlanList where commissioningPlan equals to (commissioningPlanId + 1)
        defaultConfigurationPlanShouldNotBeFound("commissioningPlanId.equals=" + (commissioningPlanId + 1));
    }

    @Test
    @Transactional
    void getAllConfigurationPlansByZonesIsEqualToSomething() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);
        Zone zones;
        if (TestUtil.findAll(em, Zone.class).isEmpty()) {
            zones = ZoneResourceIT.createEntity(em);
            em.persist(zones);
            em.flush();
        } else {
            zones = TestUtil.findAll(em, Zone.class).get(0);
        }
        em.persist(zones);
        em.flush();
        configurationPlan.addZones(zones);
        configurationPlanRepository.saveAndFlush(configurationPlan);
        Long zonesId = zones.getId();

        // Get all the configurationPlanList where zones equals to zonesId
        defaultConfigurationPlanShouldBeFound("zonesId.equals=" + zonesId);

        // Get all the configurationPlanList where zones equals to (zonesId + 1)
        defaultConfigurationPlanShouldNotBeFound("zonesId.equals=" + (zonesId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultConfigurationPlanShouldBeFound(String filter) throws Exception {
        restConfigurationPlanMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(configurationPlan.getId().intValue())))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)));

        // Check, that the count call also returns 1
        restConfigurationPlanMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultConfigurationPlanShouldNotBeFound(String filter) throws Exception {
        restConfigurationPlanMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restConfigurationPlanMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingConfigurationPlan() throws Exception {
        // Get the configurationPlan
        restConfigurationPlanMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewConfigurationPlan() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        int databaseSizeBeforeUpdate = configurationPlanRepository.findAll().size();

        // Update the configurationPlan
        ConfigurationPlan updatedConfigurationPlan = configurationPlanRepository.findById(configurationPlan.getId()).get();
        // Disconnect from session so that the updates on updatedConfigurationPlan are not directly saved in db
        em.detach(updatedConfigurationPlan);
        updatedConfigurationPlan.spare1(UPDATED_SPARE_1).spare2(UPDATED_SPARE_2).spare3(UPDATED_SPARE_3);
        ConfigurationPlanDTO configurationPlanDTO = configurationPlanMapper.toDto(updatedConfigurationPlan);

        restConfigurationPlanMockMvc
            .perform(
                put(ENTITY_API_URL_ID, configurationPlanDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(configurationPlanDTO))
            )
            .andExpect(status().isOk());

        // Validate the ConfigurationPlan in the database
        List<ConfigurationPlan> configurationPlanList = configurationPlanRepository.findAll();
        assertThat(configurationPlanList).hasSize(databaseSizeBeforeUpdate);
        ConfigurationPlan testConfigurationPlan = configurationPlanList.get(configurationPlanList.size() - 1);
        assertThat(testConfigurationPlan.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testConfigurationPlan.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testConfigurationPlan.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void putNonExistingConfigurationPlan() throws Exception {
        int databaseSizeBeforeUpdate = configurationPlanRepository.findAll().size();
        configurationPlan.setId(count.incrementAndGet());

        // Create the ConfigurationPlan
        ConfigurationPlanDTO configurationPlanDTO = configurationPlanMapper.toDto(configurationPlan);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restConfigurationPlanMockMvc
            .perform(
                put(ENTITY_API_URL_ID, configurationPlanDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(configurationPlanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ConfigurationPlan in the database
        List<ConfigurationPlan> configurationPlanList = configurationPlanRepository.findAll();
        assertThat(configurationPlanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchConfigurationPlan() throws Exception {
        int databaseSizeBeforeUpdate = configurationPlanRepository.findAll().size();
        configurationPlan.setId(count.incrementAndGet());

        // Create the ConfigurationPlan
        ConfigurationPlanDTO configurationPlanDTO = configurationPlanMapper.toDto(configurationPlan);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restConfigurationPlanMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(configurationPlanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ConfigurationPlan in the database
        List<ConfigurationPlan> configurationPlanList = configurationPlanRepository.findAll();
        assertThat(configurationPlanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamConfigurationPlan() throws Exception {
        int databaseSizeBeforeUpdate = configurationPlanRepository.findAll().size();
        configurationPlan.setId(count.incrementAndGet());

        // Create the ConfigurationPlan
        ConfigurationPlanDTO configurationPlanDTO = configurationPlanMapper.toDto(configurationPlan);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restConfigurationPlanMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(configurationPlanDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ConfigurationPlan in the database
        List<ConfigurationPlan> configurationPlanList = configurationPlanRepository.findAll();
        assertThat(configurationPlanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateConfigurationPlanWithPatch() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        int databaseSizeBeforeUpdate = configurationPlanRepository.findAll().size();

        // Update the configurationPlan using partial update
        ConfigurationPlan partialUpdatedConfigurationPlan = new ConfigurationPlan();
        partialUpdatedConfigurationPlan.setId(configurationPlan.getId());

        partialUpdatedConfigurationPlan.spare1(UPDATED_SPARE_1).spare3(UPDATED_SPARE_3);

        restConfigurationPlanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedConfigurationPlan.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedConfigurationPlan))
            )
            .andExpect(status().isOk());

        // Validate the ConfigurationPlan in the database
        List<ConfigurationPlan> configurationPlanList = configurationPlanRepository.findAll();
        assertThat(configurationPlanList).hasSize(databaseSizeBeforeUpdate);
        ConfigurationPlan testConfigurationPlan = configurationPlanList.get(configurationPlanList.size() - 1);
        assertThat(testConfigurationPlan.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testConfigurationPlan.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
        assertThat(testConfigurationPlan.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void fullUpdateConfigurationPlanWithPatch() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        int databaseSizeBeforeUpdate = configurationPlanRepository.findAll().size();

        // Update the configurationPlan using partial update
        ConfigurationPlan partialUpdatedConfigurationPlan = new ConfigurationPlan();
        partialUpdatedConfigurationPlan.setId(configurationPlan.getId());

        partialUpdatedConfigurationPlan.spare1(UPDATED_SPARE_1).spare2(UPDATED_SPARE_2).spare3(UPDATED_SPARE_3);

        restConfigurationPlanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedConfigurationPlan.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedConfigurationPlan))
            )
            .andExpect(status().isOk());

        // Validate the ConfigurationPlan in the database
        List<ConfigurationPlan> configurationPlanList = configurationPlanRepository.findAll();
        assertThat(configurationPlanList).hasSize(databaseSizeBeforeUpdate);
        ConfigurationPlan testConfigurationPlan = configurationPlanList.get(configurationPlanList.size() - 1);
        assertThat(testConfigurationPlan.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testConfigurationPlan.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testConfigurationPlan.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void patchNonExistingConfigurationPlan() throws Exception {
        int databaseSizeBeforeUpdate = configurationPlanRepository.findAll().size();
        configurationPlan.setId(count.incrementAndGet());

        // Create the ConfigurationPlan
        ConfigurationPlanDTO configurationPlanDTO = configurationPlanMapper.toDto(configurationPlan);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restConfigurationPlanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, configurationPlanDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(configurationPlanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ConfigurationPlan in the database
        List<ConfigurationPlan> configurationPlanList = configurationPlanRepository.findAll();
        assertThat(configurationPlanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchConfigurationPlan() throws Exception {
        int databaseSizeBeforeUpdate = configurationPlanRepository.findAll().size();
        configurationPlan.setId(count.incrementAndGet());

        // Create the ConfigurationPlan
        ConfigurationPlanDTO configurationPlanDTO = configurationPlanMapper.toDto(configurationPlan);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restConfigurationPlanMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(configurationPlanDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ConfigurationPlan in the database
        List<ConfigurationPlan> configurationPlanList = configurationPlanRepository.findAll();
        assertThat(configurationPlanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamConfigurationPlan() throws Exception {
        int databaseSizeBeforeUpdate = configurationPlanRepository.findAll().size();
        configurationPlan.setId(count.incrementAndGet());

        // Create the ConfigurationPlan
        ConfigurationPlanDTO configurationPlanDTO = configurationPlanMapper.toDto(configurationPlan);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restConfigurationPlanMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(configurationPlanDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ConfigurationPlan in the database
        List<ConfigurationPlan> configurationPlanList = configurationPlanRepository.findAll();
        assertThat(configurationPlanList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteConfigurationPlan() throws Exception {
        // Initialize the database
        configurationPlanRepository.saveAndFlush(configurationPlan);

        int databaseSizeBeforeDelete = configurationPlanRepository.findAll().size();

        // Delete the configurationPlan
        restConfigurationPlanMockMvc
            .perform(delete(ENTITY_API_URL_ID, configurationPlan.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ConfigurationPlan> configurationPlanList = configurationPlanRepository.findAll();
        assertThat(configurationPlanList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
