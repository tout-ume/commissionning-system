package sn.free.commissioning.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.IntegrationTest;
import sn.free.commissioning.domain.Period;
import sn.free.commissioning.domain.enumeration.FrequencyType;
import sn.free.commissioning.domain.enumeration.OperationType;
import sn.free.commissioning.repository.PeriodRepository;
import sn.free.commissioning.service.criteria.PeriodCriteria;
import sn.free.commissioning.service.dto.PeriodDTO;
import sn.free.commissioning.service.mapper.PeriodMapper;

/**
 * Integration tests for the {@link PeriodResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PeriodResourceIT {

    private static final Integer DEFAULT_MONTH_DAY_FROM = 1;
    private static final Integer UPDATED_MONTH_DAY_FROM = 2;
    private static final Integer SMALLER_MONTH_DAY_FROM = 1 - 1;

    private static final Integer DEFAULT_MONTH_DAY_TO = 1;
    private static final Integer UPDATED_MONTH_DAY_TO = 2;
    private static final Integer SMALLER_MONTH_DAY_TO = 1 - 1;

    private static final Integer DEFAULT_WEEK_DAY_FROM = 1;
    private static final Integer UPDATED_WEEK_DAY_FROM = 2;
    private static final Integer SMALLER_WEEK_DAY_FROM = 1 - 1;

    private static final Integer DEFAULT_WEEK_DAY_TO = 1;
    private static final Integer UPDATED_WEEK_DAY_TO = 2;
    private static final Integer SMALLER_WEEK_DAY_TO = 1 - 1;

    private static final Integer DEFAULT_DAY_HOUR_FROM = 1;
    private static final Integer UPDATED_DAY_HOUR_FROM = 2;
    private static final Integer SMALLER_DAY_HOUR_FROM = 1 - 1;

    private static final Integer DEFAULT_DAY_HOUR_TO = 1;
    private static final Integer UPDATED_DAY_HOUR_TO = 2;
    private static final Integer SMALLER_DAY_HOUR_TO = 1 - 1;

    private static final Boolean DEFAULT_IS_PREVIOUS_DAY_HOUR_FROM = false;
    private static final Boolean UPDATED_IS_PREVIOUS_DAY_HOUR_FROM = true;

    private static final Boolean DEFAULT_IS_PREVIOUS_DAY_HOUR_TO = false;
    private static final Boolean UPDATED_IS_PREVIOUS_DAY_HOUR_TO = true;

    private static final Boolean DEFAULT_IS_PREVIOUS_MONTH_DAY_FROM = false;
    private static final Boolean UPDATED_IS_PREVIOUS_MONTH_DAY_FROM = true;

    private static final Boolean DEFAULT_IS_PREVIOUS_MONTH_DAY_TO = false;
    private static final Boolean UPDATED_IS_PREVIOUS_MONTH_DAY_TO = true;

    private static final Boolean DEFAULT_IS_COMPLETE_DAY = false;
    private static final Boolean UPDATED_IS_COMPLETE_DAY = true;

    private static final Boolean DEFAULT_IS_COMPLETE_MONTH = false;
    private static final Boolean UPDATED_IS_COMPLETE_MONTH = true;

    private static final FrequencyType DEFAULT_PERIOD_TYPE = FrequencyType.INSTANTLY;
    private static final FrequencyType UPDATED_PERIOD_TYPE = FrequencyType.DAILY;

    private static final OperationType DEFAULT_OPERATION_TYPE = OperationType.CALCULUS;
    private static final OperationType UPDATED_OPERATION_TYPE = OperationType.PAYMENT;

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_DAYS_OR_DATES_OF_OCCURRENCE = "AAAAAAAAAA";
    private static final String UPDATED_DAYS_OR_DATES_OF_OCCURRENCE = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_1 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_2 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_3 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_3 = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/periods";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PeriodRepository periodRepository;

    @Autowired
    private PeriodMapper periodMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPeriodMockMvc;

    private Period period;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Period createEntity(EntityManager em) {
        Period period = new Period()
            .monthDayFrom(DEFAULT_MONTH_DAY_FROM)
            .monthDayTo(DEFAULT_MONTH_DAY_TO)
            .weekDayFrom(DEFAULT_WEEK_DAY_FROM)
            .weekDayTo(DEFAULT_WEEK_DAY_TO)
            .dayHourFrom(DEFAULT_DAY_HOUR_FROM)
            .dayHourTo(DEFAULT_DAY_HOUR_TO)
            .isPreviousDayHourFrom(DEFAULT_IS_PREVIOUS_DAY_HOUR_FROM)
            .isPreviousDayHourTo(DEFAULT_IS_PREVIOUS_DAY_HOUR_TO)
            .isPreviousMonthDayFrom(DEFAULT_IS_PREVIOUS_MONTH_DAY_FROM)
            .isPreviousMonthDayTo(DEFAULT_IS_PREVIOUS_MONTH_DAY_TO)
            .isCompleteDay(DEFAULT_IS_COMPLETE_DAY)
            .isCompleteMonth(DEFAULT_IS_COMPLETE_MONTH)
            .periodType(DEFAULT_PERIOD_TYPE)
            .operationType(DEFAULT_OPERATION_TYPE)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .daysOrDatesOfOccurrence(DEFAULT_DAYS_OR_DATES_OF_OCCURRENCE)
            .spare1(DEFAULT_SPARE_1)
            .spare2(DEFAULT_SPARE_2)
            .spare3(DEFAULT_SPARE_3);
        return period;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Period createUpdatedEntity(EntityManager em) {
        Period period = new Period()
            .monthDayFrom(UPDATED_MONTH_DAY_FROM)
            .monthDayTo(UPDATED_MONTH_DAY_TO)
            .weekDayFrom(UPDATED_WEEK_DAY_FROM)
            .weekDayTo(UPDATED_WEEK_DAY_TO)
            .dayHourFrom(UPDATED_DAY_HOUR_FROM)
            .dayHourTo(UPDATED_DAY_HOUR_TO)
            .isPreviousDayHourFrom(UPDATED_IS_PREVIOUS_DAY_HOUR_FROM)
            .isPreviousDayHourTo(UPDATED_IS_PREVIOUS_DAY_HOUR_TO)
            .isPreviousMonthDayFrom(UPDATED_IS_PREVIOUS_MONTH_DAY_FROM)
            .isPreviousMonthDayTo(UPDATED_IS_PREVIOUS_MONTH_DAY_TO)
            .isCompleteDay(UPDATED_IS_COMPLETE_DAY)
            .isCompleteMonth(UPDATED_IS_COMPLETE_MONTH)
            .periodType(UPDATED_PERIOD_TYPE)
            .operationType(UPDATED_OPERATION_TYPE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .daysOrDatesOfOccurrence(UPDATED_DAYS_OR_DATES_OF_OCCURRENCE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);
        return period;
    }

    @BeforeEach
    public void initTest() {
        period = createEntity(em);
    }

    @Test
    @Transactional
    void createPeriod() throws Exception {
        int databaseSizeBeforeCreate = periodRepository.findAll().size();
        // Create the Period
        PeriodDTO periodDTO = periodMapper.toDto(period);
        restPeriodMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(periodDTO)))
            .andExpect(status().isCreated());

        // Validate the Period in the database
        List<Period> periodList = periodRepository.findAll();
        assertThat(periodList).hasSize(databaseSizeBeforeCreate + 1);
        Period testPeriod = periodList.get(periodList.size() - 1);
        assertThat(testPeriod.getMonthDayFrom()).isEqualTo(DEFAULT_MONTH_DAY_FROM);
        assertThat(testPeriod.getMonthDayTo()).isEqualTo(DEFAULT_MONTH_DAY_TO);
        assertThat(testPeriod.getWeekDayFrom()).isEqualTo(DEFAULT_WEEK_DAY_FROM);
        assertThat(testPeriod.getWeekDayTo()).isEqualTo(DEFAULT_WEEK_DAY_TO);
        assertThat(testPeriod.getDayHourFrom()).isEqualTo(DEFAULT_DAY_HOUR_FROM);
        assertThat(testPeriod.getDayHourTo()).isEqualTo(DEFAULT_DAY_HOUR_TO);
        assertThat(testPeriod.getIsPreviousDayHourFrom()).isEqualTo(DEFAULT_IS_PREVIOUS_DAY_HOUR_FROM);
        assertThat(testPeriod.getIsPreviousDayHourTo()).isEqualTo(DEFAULT_IS_PREVIOUS_DAY_HOUR_TO);
        assertThat(testPeriod.getIsPreviousMonthDayFrom()).isEqualTo(DEFAULT_IS_PREVIOUS_MONTH_DAY_FROM);
        assertThat(testPeriod.getIsPreviousMonthDayTo()).isEqualTo(DEFAULT_IS_PREVIOUS_MONTH_DAY_TO);
        assertThat(testPeriod.getIsCompleteDay()).isEqualTo(DEFAULT_IS_COMPLETE_DAY);
        assertThat(testPeriod.getIsCompleteMonth()).isEqualTo(DEFAULT_IS_COMPLETE_MONTH);
        assertThat(testPeriod.getPeriodType()).isEqualTo(DEFAULT_PERIOD_TYPE);
        assertThat(testPeriod.getOperationType()).isEqualTo(DEFAULT_OPERATION_TYPE);
        assertThat(testPeriod.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testPeriod.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testPeriod.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testPeriod.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testPeriod.getDaysOrDatesOfOccurrence()).isEqualTo(DEFAULT_DAYS_OR_DATES_OF_OCCURRENCE);
        assertThat(testPeriod.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testPeriod.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
        assertThat(testPeriod.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
    }

    @Test
    @Transactional
    void createPeriodWithExistingId() throws Exception {
        // Create the Period with an existing ID
        period.setId(1L);
        PeriodDTO periodDTO = periodMapper.toDto(period);

        int databaseSizeBeforeCreate = periodRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPeriodMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(periodDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Period in the database
        List<Period> periodList = periodRepository.findAll();
        assertThat(periodList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkPeriodTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = periodRepository.findAll().size();
        // set the field null
        period.setPeriodType(null);

        // Create the Period, which fails.
        PeriodDTO periodDTO = periodMapper.toDto(period);

        restPeriodMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(periodDTO)))
            .andExpect(status().isBadRequest());

        List<Period> periodList = periodRepository.findAll();
        assertThat(periodList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkOperationTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = periodRepository.findAll().size();
        // set the field null
        period.setOperationType(null);

        // Create the Period, which fails.
        PeriodDTO periodDTO = periodMapper.toDto(period);

        restPeriodMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(periodDTO)))
            .andExpect(status().isBadRequest());

        List<Period> periodList = periodRepository.findAll();
        assertThat(periodList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCreatedByIsRequired() throws Exception {
        int databaseSizeBeforeTest = periodRepository.findAll().size();
        // set the field null
        period.setCreatedBy(null);

        // Create the Period, which fails.
        PeriodDTO periodDTO = periodMapper.toDto(period);

        restPeriodMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(periodDTO)))
            .andExpect(status().isBadRequest());

        List<Period> periodList = periodRepository.findAll();
        assertThat(periodList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCreatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = periodRepository.findAll().size();
        // set the field null
        period.setCreatedDate(null);

        // Create the Period, which fails.
        PeriodDTO periodDTO = periodMapper.toDto(period);

        restPeriodMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(periodDTO)))
            .andExpect(status().isBadRequest());

        List<Period> periodList = periodRepository.findAll();
        assertThat(periodList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPeriods() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList
        restPeriodMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(period.getId().intValue())))
            .andExpect(jsonPath("$.[*].monthDayFrom").value(hasItem(DEFAULT_MONTH_DAY_FROM)))
            .andExpect(jsonPath("$.[*].monthDayTo").value(hasItem(DEFAULT_MONTH_DAY_TO)))
            .andExpect(jsonPath("$.[*].weekDayFrom").value(hasItem(DEFAULT_WEEK_DAY_FROM)))
            .andExpect(jsonPath("$.[*].weekDayTo").value(hasItem(DEFAULT_WEEK_DAY_TO)))
            .andExpect(jsonPath("$.[*].dayHourFrom").value(hasItem(DEFAULT_DAY_HOUR_FROM)))
            .andExpect(jsonPath("$.[*].dayHourTo").value(hasItem(DEFAULT_DAY_HOUR_TO)))
            .andExpect(jsonPath("$.[*].isPreviousDayHourFrom").value(hasItem(DEFAULT_IS_PREVIOUS_DAY_HOUR_FROM.booleanValue())))
            .andExpect(jsonPath("$.[*].isPreviousDayHourTo").value(hasItem(DEFAULT_IS_PREVIOUS_DAY_HOUR_TO.booleanValue())))
            .andExpect(jsonPath("$.[*].isPreviousMonthDayFrom").value(hasItem(DEFAULT_IS_PREVIOUS_MONTH_DAY_FROM.booleanValue())))
            .andExpect(jsonPath("$.[*].isPreviousMonthDayTo").value(hasItem(DEFAULT_IS_PREVIOUS_MONTH_DAY_TO.booleanValue())))
            .andExpect(jsonPath("$.[*].isCompleteDay").value(hasItem(DEFAULT_IS_COMPLETE_DAY.booleanValue())))
            .andExpect(jsonPath("$.[*].isCompleteMonth").value(hasItem(DEFAULT_IS_COMPLETE_MONTH.booleanValue())))
            .andExpect(jsonPath("$.[*].periodType").value(hasItem(DEFAULT_PERIOD_TYPE.toString())))
            .andExpect(jsonPath("$.[*].operationType").value(hasItem(DEFAULT_OPERATION_TYPE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].daysOrDatesOfOccurrence").value(hasItem(DEFAULT_DAYS_OR_DATES_OF_OCCURRENCE)))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)));
    }

    @Test
    @Transactional
    void getPeriod() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get the period
        restPeriodMockMvc
            .perform(get(ENTITY_API_URL_ID, period.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(period.getId().intValue()))
            .andExpect(jsonPath("$.monthDayFrom").value(DEFAULT_MONTH_DAY_FROM))
            .andExpect(jsonPath("$.monthDayTo").value(DEFAULT_MONTH_DAY_TO))
            .andExpect(jsonPath("$.weekDayFrom").value(DEFAULT_WEEK_DAY_FROM))
            .andExpect(jsonPath("$.weekDayTo").value(DEFAULT_WEEK_DAY_TO))
            .andExpect(jsonPath("$.dayHourFrom").value(DEFAULT_DAY_HOUR_FROM))
            .andExpect(jsonPath("$.dayHourTo").value(DEFAULT_DAY_HOUR_TO))
            .andExpect(jsonPath("$.isPreviousDayHourFrom").value(DEFAULT_IS_PREVIOUS_DAY_HOUR_FROM.booleanValue()))
            .andExpect(jsonPath("$.isPreviousDayHourTo").value(DEFAULT_IS_PREVIOUS_DAY_HOUR_TO.booleanValue()))
            .andExpect(jsonPath("$.isPreviousMonthDayFrom").value(DEFAULT_IS_PREVIOUS_MONTH_DAY_FROM.booleanValue()))
            .andExpect(jsonPath("$.isPreviousMonthDayTo").value(DEFAULT_IS_PREVIOUS_MONTH_DAY_TO.booleanValue()))
            .andExpect(jsonPath("$.isCompleteDay").value(DEFAULT_IS_COMPLETE_DAY.booleanValue()))
            .andExpect(jsonPath("$.isCompleteMonth").value(DEFAULT_IS_COMPLETE_MONTH.booleanValue()))
            .andExpect(jsonPath("$.periodType").value(DEFAULT_PERIOD_TYPE.toString()))
            .andExpect(jsonPath("$.operationType").value(DEFAULT_OPERATION_TYPE.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.daysOrDatesOfOccurrence").value(DEFAULT_DAYS_OR_DATES_OF_OCCURRENCE))
            .andExpect(jsonPath("$.spare1").value(DEFAULT_SPARE_1))
            .andExpect(jsonPath("$.spare2").value(DEFAULT_SPARE_2))
            .andExpect(jsonPath("$.spare3").value(DEFAULT_SPARE_3));
    }

    @Test
    @Transactional
    void getPeriodsByIdFiltering() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        Long id = period.getId();

        defaultPeriodShouldBeFound("id.equals=" + id);
        defaultPeriodShouldNotBeFound("id.notEquals=" + id);

        defaultPeriodShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPeriodShouldNotBeFound("id.greaterThan=" + id);

        defaultPeriodShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPeriodShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllPeriodsByMonthDayFromIsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where monthDayFrom equals to DEFAULT_MONTH_DAY_FROM
        defaultPeriodShouldBeFound("monthDayFrom.equals=" + DEFAULT_MONTH_DAY_FROM);

        // Get all the periodList where monthDayFrom equals to UPDATED_MONTH_DAY_FROM
        defaultPeriodShouldNotBeFound("monthDayFrom.equals=" + UPDATED_MONTH_DAY_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByMonthDayFromIsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where monthDayFrom not equals to DEFAULT_MONTH_DAY_FROM
        defaultPeriodShouldNotBeFound("monthDayFrom.notEquals=" + DEFAULT_MONTH_DAY_FROM);

        // Get all the periodList where monthDayFrom not equals to UPDATED_MONTH_DAY_FROM
        defaultPeriodShouldBeFound("monthDayFrom.notEquals=" + UPDATED_MONTH_DAY_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByMonthDayFromIsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where monthDayFrom in DEFAULT_MONTH_DAY_FROM or UPDATED_MONTH_DAY_FROM
        defaultPeriodShouldBeFound("monthDayFrom.in=" + DEFAULT_MONTH_DAY_FROM + "," + UPDATED_MONTH_DAY_FROM);

        // Get all the periodList where monthDayFrom equals to UPDATED_MONTH_DAY_FROM
        defaultPeriodShouldNotBeFound("monthDayFrom.in=" + UPDATED_MONTH_DAY_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByMonthDayFromIsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where monthDayFrom is not null
        defaultPeriodShouldBeFound("monthDayFrom.specified=true");

        // Get all the periodList where monthDayFrom is null
        defaultPeriodShouldNotBeFound("monthDayFrom.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsByMonthDayFromIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where monthDayFrom is greater than or equal to DEFAULT_MONTH_DAY_FROM
        defaultPeriodShouldBeFound("monthDayFrom.greaterThanOrEqual=" + DEFAULT_MONTH_DAY_FROM);

        // Get all the periodList where monthDayFrom is greater than or equal to UPDATED_MONTH_DAY_FROM
        defaultPeriodShouldNotBeFound("monthDayFrom.greaterThanOrEqual=" + UPDATED_MONTH_DAY_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByMonthDayFromIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where monthDayFrom is less than or equal to DEFAULT_MONTH_DAY_FROM
        defaultPeriodShouldBeFound("monthDayFrom.lessThanOrEqual=" + DEFAULT_MONTH_DAY_FROM);

        // Get all the periodList where monthDayFrom is less than or equal to SMALLER_MONTH_DAY_FROM
        defaultPeriodShouldNotBeFound("monthDayFrom.lessThanOrEqual=" + SMALLER_MONTH_DAY_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByMonthDayFromIsLessThanSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where monthDayFrom is less than DEFAULT_MONTH_DAY_FROM
        defaultPeriodShouldNotBeFound("monthDayFrom.lessThan=" + DEFAULT_MONTH_DAY_FROM);

        // Get all the periodList where monthDayFrom is less than UPDATED_MONTH_DAY_FROM
        defaultPeriodShouldBeFound("monthDayFrom.lessThan=" + UPDATED_MONTH_DAY_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByMonthDayFromIsGreaterThanSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where monthDayFrom is greater than DEFAULT_MONTH_DAY_FROM
        defaultPeriodShouldNotBeFound("monthDayFrom.greaterThan=" + DEFAULT_MONTH_DAY_FROM);

        // Get all the periodList where monthDayFrom is greater than SMALLER_MONTH_DAY_FROM
        defaultPeriodShouldBeFound("monthDayFrom.greaterThan=" + SMALLER_MONTH_DAY_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByMonthDayToIsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where monthDayTo equals to DEFAULT_MONTH_DAY_TO
        defaultPeriodShouldBeFound("monthDayTo.equals=" + DEFAULT_MONTH_DAY_TO);

        // Get all the periodList where monthDayTo equals to UPDATED_MONTH_DAY_TO
        defaultPeriodShouldNotBeFound("monthDayTo.equals=" + UPDATED_MONTH_DAY_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByMonthDayToIsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where monthDayTo not equals to DEFAULT_MONTH_DAY_TO
        defaultPeriodShouldNotBeFound("monthDayTo.notEquals=" + DEFAULT_MONTH_DAY_TO);

        // Get all the periodList where monthDayTo not equals to UPDATED_MONTH_DAY_TO
        defaultPeriodShouldBeFound("monthDayTo.notEquals=" + UPDATED_MONTH_DAY_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByMonthDayToIsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where monthDayTo in DEFAULT_MONTH_DAY_TO or UPDATED_MONTH_DAY_TO
        defaultPeriodShouldBeFound("monthDayTo.in=" + DEFAULT_MONTH_DAY_TO + "," + UPDATED_MONTH_DAY_TO);

        // Get all the periodList where monthDayTo equals to UPDATED_MONTH_DAY_TO
        defaultPeriodShouldNotBeFound("monthDayTo.in=" + UPDATED_MONTH_DAY_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByMonthDayToIsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where monthDayTo is not null
        defaultPeriodShouldBeFound("monthDayTo.specified=true");

        // Get all the periodList where monthDayTo is null
        defaultPeriodShouldNotBeFound("monthDayTo.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsByMonthDayToIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where monthDayTo is greater than or equal to DEFAULT_MONTH_DAY_TO
        defaultPeriodShouldBeFound("monthDayTo.greaterThanOrEqual=" + DEFAULT_MONTH_DAY_TO);

        // Get all the periodList where monthDayTo is greater than or equal to UPDATED_MONTH_DAY_TO
        defaultPeriodShouldNotBeFound("monthDayTo.greaterThanOrEqual=" + UPDATED_MONTH_DAY_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByMonthDayToIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where monthDayTo is less than or equal to DEFAULT_MONTH_DAY_TO
        defaultPeriodShouldBeFound("monthDayTo.lessThanOrEqual=" + DEFAULT_MONTH_DAY_TO);

        // Get all the periodList where monthDayTo is less than or equal to SMALLER_MONTH_DAY_TO
        defaultPeriodShouldNotBeFound("monthDayTo.lessThanOrEqual=" + SMALLER_MONTH_DAY_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByMonthDayToIsLessThanSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where monthDayTo is less than DEFAULT_MONTH_DAY_TO
        defaultPeriodShouldNotBeFound("monthDayTo.lessThan=" + DEFAULT_MONTH_DAY_TO);

        // Get all the periodList where monthDayTo is less than UPDATED_MONTH_DAY_TO
        defaultPeriodShouldBeFound("monthDayTo.lessThan=" + UPDATED_MONTH_DAY_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByMonthDayToIsGreaterThanSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where monthDayTo is greater than DEFAULT_MONTH_DAY_TO
        defaultPeriodShouldNotBeFound("monthDayTo.greaterThan=" + DEFAULT_MONTH_DAY_TO);

        // Get all the periodList where monthDayTo is greater than SMALLER_MONTH_DAY_TO
        defaultPeriodShouldBeFound("monthDayTo.greaterThan=" + SMALLER_MONTH_DAY_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByWeekDayFromIsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where weekDayFrom equals to DEFAULT_WEEK_DAY_FROM
        defaultPeriodShouldBeFound("weekDayFrom.equals=" + DEFAULT_WEEK_DAY_FROM);

        // Get all the periodList where weekDayFrom equals to UPDATED_WEEK_DAY_FROM
        defaultPeriodShouldNotBeFound("weekDayFrom.equals=" + UPDATED_WEEK_DAY_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByWeekDayFromIsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where weekDayFrom not equals to DEFAULT_WEEK_DAY_FROM
        defaultPeriodShouldNotBeFound("weekDayFrom.notEquals=" + DEFAULT_WEEK_DAY_FROM);

        // Get all the periodList where weekDayFrom not equals to UPDATED_WEEK_DAY_FROM
        defaultPeriodShouldBeFound("weekDayFrom.notEquals=" + UPDATED_WEEK_DAY_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByWeekDayFromIsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where weekDayFrom in DEFAULT_WEEK_DAY_FROM or UPDATED_WEEK_DAY_FROM
        defaultPeriodShouldBeFound("weekDayFrom.in=" + DEFAULT_WEEK_DAY_FROM + "," + UPDATED_WEEK_DAY_FROM);

        // Get all the periodList where weekDayFrom equals to UPDATED_WEEK_DAY_FROM
        defaultPeriodShouldNotBeFound("weekDayFrom.in=" + UPDATED_WEEK_DAY_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByWeekDayFromIsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where weekDayFrom is not null
        defaultPeriodShouldBeFound("weekDayFrom.specified=true");

        // Get all the periodList where weekDayFrom is null
        defaultPeriodShouldNotBeFound("weekDayFrom.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsByWeekDayFromIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where weekDayFrom is greater than or equal to DEFAULT_WEEK_DAY_FROM
        defaultPeriodShouldBeFound("weekDayFrom.greaterThanOrEqual=" + DEFAULT_WEEK_DAY_FROM);

        // Get all the periodList where weekDayFrom is greater than or equal to UPDATED_WEEK_DAY_FROM
        defaultPeriodShouldNotBeFound("weekDayFrom.greaterThanOrEqual=" + UPDATED_WEEK_DAY_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByWeekDayFromIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where weekDayFrom is less than or equal to DEFAULT_WEEK_DAY_FROM
        defaultPeriodShouldBeFound("weekDayFrom.lessThanOrEqual=" + DEFAULT_WEEK_DAY_FROM);

        // Get all the periodList where weekDayFrom is less than or equal to SMALLER_WEEK_DAY_FROM
        defaultPeriodShouldNotBeFound("weekDayFrom.lessThanOrEqual=" + SMALLER_WEEK_DAY_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByWeekDayFromIsLessThanSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where weekDayFrom is less than DEFAULT_WEEK_DAY_FROM
        defaultPeriodShouldNotBeFound("weekDayFrom.lessThan=" + DEFAULT_WEEK_DAY_FROM);

        // Get all the periodList where weekDayFrom is less than UPDATED_WEEK_DAY_FROM
        defaultPeriodShouldBeFound("weekDayFrom.lessThan=" + UPDATED_WEEK_DAY_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByWeekDayFromIsGreaterThanSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where weekDayFrom is greater than DEFAULT_WEEK_DAY_FROM
        defaultPeriodShouldNotBeFound("weekDayFrom.greaterThan=" + DEFAULT_WEEK_DAY_FROM);

        // Get all the periodList where weekDayFrom is greater than SMALLER_WEEK_DAY_FROM
        defaultPeriodShouldBeFound("weekDayFrom.greaterThan=" + SMALLER_WEEK_DAY_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByWeekDayToIsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where weekDayTo equals to DEFAULT_WEEK_DAY_TO
        defaultPeriodShouldBeFound("weekDayTo.equals=" + DEFAULT_WEEK_DAY_TO);

        // Get all the periodList where weekDayTo equals to UPDATED_WEEK_DAY_TO
        defaultPeriodShouldNotBeFound("weekDayTo.equals=" + UPDATED_WEEK_DAY_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByWeekDayToIsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where weekDayTo not equals to DEFAULT_WEEK_DAY_TO
        defaultPeriodShouldNotBeFound("weekDayTo.notEquals=" + DEFAULT_WEEK_DAY_TO);

        // Get all the periodList where weekDayTo not equals to UPDATED_WEEK_DAY_TO
        defaultPeriodShouldBeFound("weekDayTo.notEquals=" + UPDATED_WEEK_DAY_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByWeekDayToIsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where weekDayTo in DEFAULT_WEEK_DAY_TO or UPDATED_WEEK_DAY_TO
        defaultPeriodShouldBeFound("weekDayTo.in=" + DEFAULT_WEEK_DAY_TO + "," + UPDATED_WEEK_DAY_TO);

        // Get all the periodList where weekDayTo equals to UPDATED_WEEK_DAY_TO
        defaultPeriodShouldNotBeFound("weekDayTo.in=" + UPDATED_WEEK_DAY_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByWeekDayToIsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where weekDayTo is not null
        defaultPeriodShouldBeFound("weekDayTo.specified=true");

        // Get all the periodList where weekDayTo is null
        defaultPeriodShouldNotBeFound("weekDayTo.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsByWeekDayToIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where weekDayTo is greater than or equal to DEFAULT_WEEK_DAY_TO
        defaultPeriodShouldBeFound("weekDayTo.greaterThanOrEqual=" + DEFAULT_WEEK_DAY_TO);

        // Get all the periodList where weekDayTo is greater than or equal to UPDATED_WEEK_DAY_TO
        defaultPeriodShouldNotBeFound("weekDayTo.greaterThanOrEqual=" + UPDATED_WEEK_DAY_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByWeekDayToIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where weekDayTo is less than or equal to DEFAULT_WEEK_DAY_TO
        defaultPeriodShouldBeFound("weekDayTo.lessThanOrEqual=" + DEFAULT_WEEK_DAY_TO);

        // Get all the periodList where weekDayTo is less than or equal to SMALLER_WEEK_DAY_TO
        defaultPeriodShouldNotBeFound("weekDayTo.lessThanOrEqual=" + SMALLER_WEEK_DAY_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByWeekDayToIsLessThanSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where weekDayTo is less than DEFAULT_WEEK_DAY_TO
        defaultPeriodShouldNotBeFound("weekDayTo.lessThan=" + DEFAULT_WEEK_DAY_TO);

        // Get all the periodList where weekDayTo is less than UPDATED_WEEK_DAY_TO
        defaultPeriodShouldBeFound("weekDayTo.lessThan=" + UPDATED_WEEK_DAY_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByWeekDayToIsGreaterThanSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where weekDayTo is greater than DEFAULT_WEEK_DAY_TO
        defaultPeriodShouldNotBeFound("weekDayTo.greaterThan=" + DEFAULT_WEEK_DAY_TO);

        // Get all the periodList where weekDayTo is greater than SMALLER_WEEK_DAY_TO
        defaultPeriodShouldBeFound("weekDayTo.greaterThan=" + SMALLER_WEEK_DAY_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByDayHourFromIsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where dayHourFrom equals to DEFAULT_DAY_HOUR_FROM
        defaultPeriodShouldBeFound("dayHourFrom.equals=" + DEFAULT_DAY_HOUR_FROM);

        // Get all the periodList where dayHourFrom equals to UPDATED_DAY_HOUR_FROM
        defaultPeriodShouldNotBeFound("dayHourFrom.equals=" + UPDATED_DAY_HOUR_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByDayHourFromIsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where dayHourFrom not equals to DEFAULT_DAY_HOUR_FROM
        defaultPeriodShouldNotBeFound("dayHourFrom.notEquals=" + DEFAULT_DAY_HOUR_FROM);

        // Get all the periodList where dayHourFrom not equals to UPDATED_DAY_HOUR_FROM
        defaultPeriodShouldBeFound("dayHourFrom.notEquals=" + UPDATED_DAY_HOUR_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByDayHourFromIsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where dayHourFrom in DEFAULT_DAY_HOUR_FROM or UPDATED_DAY_HOUR_FROM
        defaultPeriodShouldBeFound("dayHourFrom.in=" + DEFAULT_DAY_HOUR_FROM + "," + UPDATED_DAY_HOUR_FROM);

        // Get all the periodList where dayHourFrom equals to UPDATED_DAY_HOUR_FROM
        defaultPeriodShouldNotBeFound("dayHourFrom.in=" + UPDATED_DAY_HOUR_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByDayHourFromIsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where dayHourFrom is not null
        defaultPeriodShouldBeFound("dayHourFrom.specified=true");

        // Get all the periodList where dayHourFrom is null
        defaultPeriodShouldNotBeFound("dayHourFrom.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsByDayHourFromIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where dayHourFrom is greater than or equal to DEFAULT_DAY_HOUR_FROM
        defaultPeriodShouldBeFound("dayHourFrom.greaterThanOrEqual=" + DEFAULT_DAY_HOUR_FROM);

        // Get all the periodList where dayHourFrom is greater than or equal to UPDATED_DAY_HOUR_FROM
        defaultPeriodShouldNotBeFound("dayHourFrom.greaterThanOrEqual=" + UPDATED_DAY_HOUR_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByDayHourFromIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where dayHourFrom is less than or equal to DEFAULT_DAY_HOUR_FROM
        defaultPeriodShouldBeFound("dayHourFrom.lessThanOrEqual=" + DEFAULT_DAY_HOUR_FROM);

        // Get all the periodList where dayHourFrom is less than or equal to SMALLER_DAY_HOUR_FROM
        defaultPeriodShouldNotBeFound("dayHourFrom.lessThanOrEqual=" + SMALLER_DAY_HOUR_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByDayHourFromIsLessThanSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where dayHourFrom is less than DEFAULT_DAY_HOUR_FROM
        defaultPeriodShouldNotBeFound("dayHourFrom.lessThan=" + DEFAULT_DAY_HOUR_FROM);

        // Get all the periodList where dayHourFrom is less than UPDATED_DAY_HOUR_FROM
        defaultPeriodShouldBeFound("dayHourFrom.lessThan=" + UPDATED_DAY_HOUR_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByDayHourFromIsGreaterThanSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where dayHourFrom is greater than DEFAULT_DAY_HOUR_FROM
        defaultPeriodShouldNotBeFound("dayHourFrom.greaterThan=" + DEFAULT_DAY_HOUR_FROM);

        // Get all the periodList where dayHourFrom is greater than SMALLER_DAY_HOUR_FROM
        defaultPeriodShouldBeFound("dayHourFrom.greaterThan=" + SMALLER_DAY_HOUR_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByDayHourToIsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where dayHourTo equals to DEFAULT_DAY_HOUR_TO
        defaultPeriodShouldBeFound("dayHourTo.equals=" + DEFAULT_DAY_HOUR_TO);

        // Get all the periodList where dayHourTo equals to UPDATED_DAY_HOUR_TO
        defaultPeriodShouldNotBeFound("dayHourTo.equals=" + UPDATED_DAY_HOUR_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByDayHourToIsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where dayHourTo not equals to DEFAULT_DAY_HOUR_TO
        defaultPeriodShouldNotBeFound("dayHourTo.notEquals=" + DEFAULT_DAY_HOUR_TO);

        // Get all the periodList where dayHourTo not equals to UPDATED_DAY_HOUR_TO
        defaultPeriodShouldBeFound("dayHourTo.notEquals=" + UPDATED_DAY_HOUR_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByDayHourToIsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where dayHourTo in DEFAULT_DAY_HOUR_TO or UPDATED_DAY_HOUR_TO
        defaultPeriodShouldBeFound("dayHourTo.in=" + DEFAULT_DAY_HOUR_TO + "," + UPDATED_DAY_HOUR_TO);

        // Get all the periodList where dayHourTo equals to UPDATED_DAY_HOUR_TO
        defaultPeriodShouldNotBeFound("dayHourTo.in=" + UPDATED_DAY_HOUR_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByDayHourToIsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where dayHourTo is not null
        defaultPeriodShouldBeFound("dayHourTo.specified=true");

        // Get all the periodList where dayHourTo is null
        defaultPeriodShouldNotBeFound("dayHourTo.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsByDayHourToIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where dayHourTo is greater than or equal to DEFAULT_DAY_HOUR_TO
        defaultPeriodShouldBeFound("dayHourTo.greaterThanOrEqual=" + DEFAULT_DAY_HOUR_TO);

        // Get all the periodList where dayHourTo is greater than or equal to UPDATED_DAY_HOUR_TO
        defaultPeriodShouldNotBeFound("dayHourTo.greaterThanOrEqual=" + UPDATED_DAY_HOUR_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByDayHourToIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where dayHourTo is less than or equal to DEFAULT_DAY_HOUR_TO
        defaultPeriodShouldBeFound("dayHourTo.lessThanOrEqual=" + DEFAULT_DAY_HOUR_TO);

        // Get all the periodList where dayHourTo is less than or equal to SMALLER_DAY_HOUR_TO
        defaultPeriodShouldNotBeFound("dayHourTo.lessThanOrEqual=" + SMALLER_DAY_HOUR_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByDayHourToIsLessThanSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where dayHourTo is less than DEFAULT_DAY_HOUR_TO
        defaultPeriodShouldNotBeFound("dayHourTo.lessThan=" + DEFAULT_DAY_HOUR_TO);

        // Get all the periodList where dayHourTo is less than UPDATED_DAY_HOUR_TO
        defaultPeriodShouldBeFound("dayHourTo.lessThan=" + UPDATED_DAY_HOUR_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByDayHourToIsGreaterThanSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where dayHourTo is greater than DEFAULT_DAY_HOUR_TO
        defaultPeriodShouldNotBeFound("dayHourTo.greaterThan=" + DEFAULT_DAY_HOUR_TO);

        // Get all the periodList where dayHourTo is greater than SMALLER_DAY_HOUR_TO
        defaultPeriodShouldBeFound("dayHourTo.greaterThan=" + SMALLER_DAY_HOUR_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByIsPreviousDayHourFromIsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isPreviousDayHourFrom equals to DEFAULT_IS_PREVIOUS_DAY_HOUR_FROM
        defaultPeriodShouldBeFound("isPreviousDayHourFrom.equals=" + DEFAULT_IS_PREVIOUS_DAY_HOUR_FROM);

        // Get all the periodList where isPreviousDayHourFrom equals to UPDATED_IS_PREVIOUS_DAY_HOUR_FROM
        defaultPeriodShouldNotBeFound("isPreviousDayHourFrom.equals=" + UPDATED_IS_PREVIOUS_DAY_HOUR_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByIsPreviousDayHourFromIsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isPreviousDayHourFrom not equals to DEFAULT_IS_PREVIOUS_DAY_HOUR_FROM
        defaultPeriodShouldNotBeFound("isPreviousDayHourFrom.notEquals=" + DEFAULT_IS_PREVIOUS_DAY_HOUR_FROM);

        // Get all the periodList where isPreviousDayHourFrom not equals to UPDATED_IS_PREVIOUS_DAY_HOUR_FROM
        defaultPeriodShouldBeFound("isPreviousDayHourFrom.notEquals=" + UPDATED_IS_PREVIOUS_DAY_HOUR_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByIsPreviousDayHourFromIsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isPreviousDayHourFrom in DEFAULT_IS_PREVIOUS_DAY_HOUR_FROM or UPDATED_IS_PREVIOUS_DAY_HOUR_FROM
        defaultPeriodShouldBeFound(
            "isPreviousDayHourFrom.in=" + DEFAULT_IS_PREVIOUS_DAY_HOUR_FROM + "," + UPDATED_IS_PREVIOUS_DAY_HOUR_FROM
        );

        // Get all the periodList where isPreviousDayHourFrom equals to UPDATED_IS_PREVIOUS_DAY_HOUR_FROM
        defaultPeriodShouldNotBeFound("isPreviousDayHourFrom.in=" + UPDATED_IS_PREVIOUS_DAY_HOUR_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByIsPreviousDayHourFromIsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isPreviousDayHourFrom is not null
        defaultPeriodShouldBeFound("isPreviousDayHourFrom.specified=true");

        // Get all the periodList where isPreviousDayHourFrom is null
        defaultPeriodShouldNotBeFound("isPreviousDayHourFrom.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsByIsPreviousDayHourToIsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isPreviousDayHourTo equals to DEFAULT_IS_PREVIOUS_DAY_HOUR_TO
        defaultPeriodShouldBeFound("isPreviousDayHourTo.equals=" + DEFAULT_IS_PREVIOUS_DAY_HOUR_TO);

        // Get all the periodList where isPreviousDayHourTo equals to UPDATED_IS_PREVIOUS_DAY_HOUR_TO
        defaultPeriodShouldNotBeFound("isPreviousDayHourTo.equals=" + UPDATED_IS_PREVIOUS_DAY_HOUR_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByIsPreviousDayHourToIsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isPreviousDayHourTo not equals to DEFAULT_IS_PREVIOUS_DAY_HOUR_TO
        defaultPeriodShouldNotBeFound("isPreviousDayHourTo.notEquals=" + DEFAULT_IS_PREVIOUS_DAY_HOUR_TO);

        // Get all the periodList where isPreviousDayHourTo not equals to UPDATED_IS_PREVIOUS_DAY_HOUR_TO
        defaultPeriodShouldBeFound("isPreviousDayHourTo.notEquals=" + UPDATED_IS_PREVIOUS_DAY_HOUR_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByIsPreviousDayHourToIsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isPreviousDayHourTo in DEFAULT_IS_PREVIOUS_DAY_HOUR_TO or UPDATED_IS_PREVIOUS_DAY_HOUR_TO
        defaultPeriodShouldBeFound("isPreviousDayHourTo.in=" + DEFAULT_IS_PREVIOUS_DAY_HOUR_TO + "," + UPDATED_IS_PREVIOUS_DAY_HOUR_TO);

        // Get all the periodList where isPreviousDayHourTo equals to UPDATED_IS_PREVIOUS_DAY_HOUR_TO
        defaultPeriodShouldNotBeFound("isPreviousDayHourTo.in=" + UPDATED_IS_PREVIOUS_DAY_HOUR_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByIsPreviousDayHourToIsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isPreviousDayHourTo is not null
        defaultPeriodShouldBeFound("isPreviousDayHourTo.specified=true");

        // Get all the periodList where isPreviousDayHourTo is null
        defaultPeriodShouldNotBeFound("isPreviousDayHourTo.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsByIsPreviousMonthDayFromIsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isPreviousMonthDayFrom equals to DEFAULT_IS_PREVIOUS_MONTH_DAY_FROM
        defaultPeriodShouldBeFound("isPreviousMonthDayFrom.equals=" + DEFAULT_IS_PREVIOUS_MONTH_DAY_FROM);

        // Get all the periodList where isPreviousMonthDayFrom equals to UPDATED_IS_PREVIOUS_MONTH_DAY_FROM
        defaultPeriodShouldNotBeFound("isPreviousMonthDayFrom.equals=" + UPDATED_IS_PREVIOUS_MONTH_DAY_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByIsPreviousMonthDayFromIsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isPreviousMonthDayFrom not equals to DEFAULT_IS_PREVIOUS_MONTH_DAY_FROM
        defaultPeriodShouldNotBeFound("isPreviousMonthDayFrom.notEquals=" + DEFAULT_IS_PREVIOUS_MONTH_DAY_FROM);

        // Get all the periodList where isPreviousMonthDayFrom not equals to UPDATED_IS_PREVIOUS_MONTH_DAY_FROM
        defaultPeriodShouldBeFound("isPreviousMonthDayFrom.notEquals=" + UPDATED_IS_PREVIOUS_MONTH_DAY_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByIsPreviousMonthDayFromIsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isPreviousMonthDayFrom in DEFAULT_IS_PREVIOUS_MONTH_DAY_FROM or UPDATED_IS_PREVIOUS_MONTH_DAY_FROM
        defaultPeriodShouldBeFound(
            "isPreviousMonthDayFrom.in=" + DEFAULT_IS_PREVIOUS_MONTH_DAY_FROM + "," + UPDATED_IS_PREVIOUS_MONTH_DAY_FROM
        );

        // Get all the periodList where isPreviousMonthDayFrom equals to UPDATED_IS_PREVIOUS_MONTH_DAY_FROM
        defaultPeriodShouldNotBeFound("isPreviousMonthDayFrom.in=" + UPDATED_IS_PREVIOUS_MONTH_DAY_FROM);
    }

    @Test
    @Transactional
    void getAllPeriodsByIsPreviousMonthDayFromIsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isPreviousMonthDayFrom is not null
        defaultPeriodShouldBeFound("isPreviousMonthDayFrom.specified=true");

        // Get all the periodList where isPreviousMonthDayFrom is null
        defaultPeriodShouldNotBeFound("isPreviousMonthDayFrom.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsByIsPreviousMonthDayToIsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isPreviousMonthDayTo equals to DEFAULT_IS_PREVIOUS_MONTH_DAY_TO
        defaultPeriodShouldBeFound("isPreviousMonthDayTo.equals=" + DEFAULT_IS_PREVIOUS_MONTH_DAY_TO);

        // Get all the periodList where isPreviousMonthDayTo equals to UPDATED_IS_PREVIOUS_MONTH_DAY_TO
        defaultPeriodShouldNotBeFound("isPreviousMonthDayTo.equals=" + UPDATED_IS_PREVIOUS_MONTH_DAY_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByIsPreviousMonthDayToIsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isPreviousMonthDayTo not equals to DEFAULT_IS_PREVIOUS_MONTH_DAY_TO
        defaultPeriodShouldNotBeFound("isPreviousMonthDayTo.notEquals=" + DEFAULT_IS_PREVIOUS_MONTH_DAY_TO);

        // Get all the periodList where isPreviousMonthDayTo not equals to UPDATED_IS_PREVIOUS_MONTH_DAY_TO
        defaultPeriodShouldBeFound("isPreviousMonthDayTo.notEquals=" + UPDATED_IS_PREVIOUS_MONTH_DAY_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByIsPreviousMonthDayToIsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isPreviousMonthDayTo in DEFAULT_IS_PREVIOUS_MONTH_DAY_TO or UPDATED_IS_PREVIOUS_MONTH_DAY_TO
        defaultPeriodShouldBeFound("isPreviousMonthDayTo.in=" + DEFAULT_IS_PREVIOUS_MONTH_DAY_TO + "," + UPDATED_IS_PREVIOUS_MONTH_DAY_TO);

        // Get all the periodList where isPreviousMonthDayTo equals to UPDATED_IS_PREVIOUS_MONTH_DAY_TO
        defaultPeriodShouldNotBeFound("isPreviousMonthDayTo.in=" + UPDATED_IS_PREVIOUS_MONTH_DAY_TO);
    }

    @Test
    @Transactional
    void getAllPeriodsByIsPreviousMonthDayToIsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isPreviousMonthDayTo is not null
        defaultPeriodShouldBeFound("isPreviousMonthDayTo.specified=true");

        // Get all the periodList where isPreviousMonthDayTo is null
        defaultPeriodShouldNotBeFound("isPreviousMonthDayTo.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsByIsCompleteDayIsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isCompleteDay equals to DEFAULT_IS_COMPLETE_DAY
        defaultPeriodShouldBeFound("isCompleteDay.equals=" + DEFAULT_IS_COMPLETE_DAY);

        // Get all the periodList where isCompleteDay equals to UPDATED_IS_COMPLETE_DAY
        defaultPeriodShouldNotBeFound("isCompleteDay.equals=" + UPDATED_IS_COMPLETE_DAY);
    }

    @Test
    @Transactional
    void getAllPeriodsByIsCompleteDayIsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isCompleteDay not equals to DEFAULT_IS_COMPLETE_DAY
        defaultPeriodShouldNotBeFound("isCompleteDay.notEquals=" + DEFAULT_IS_COMPLETE_DAY);

        // Get all the periodList where isCompleteDay not equals to UPDATED_IS_COMPLETE_DAY
        defaultPeriodShouldBeFound("isCompleteDay.notEquals=" + UPDATED_IS_COMPLETE_DAY);
    }

    @Test
    @Transactional
    void getAllPeriodsByIsCompleteDayIsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isCompleteDay in DEFAULT_IS_COMPLETE_DAY or UPDATED_IS_COMPLETE_DAY
        defaultPeriodShouldBeFound("isCompleteDay.in=" + DEFAULT_IS_COMPLETE_DAY + "," + UPDATED_IS_COMPLETE_DAY);

        // Get all the periodList where isCompleteDay equals to UPDATED_IS_COMPLETE_DAY
        defaultPeriodShouldNotBeFound("isCompleteDay.in=" + UPDATED_IS_COMPLETE_DAY);
    }

    @Test
    @Transactional
    void getAllPeriodsByIsCompleteDayIsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isCompleteDay is not null
        defaultPeriodShouldBeFound("isCompleteDay.specified=true");

        // Get all the periodList where isCompleteDay is null
        defaultPeriodShouldNotBeFound("isCompleteDay.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsByIsCompleteMonthIsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isCompleteMonth equals to DEFAULT_IS_COMPLETE_MONTH
        defaultPeriodShouldBeFound("isCompleteMonth.equals=" + DEFAULT_IS_COMPLETE_MONTH);

        // Get all the periodList where isCompleteMonth equals to UPDATED_IS_COMPLETE_MONTH
        defaultPeriodShouldNotBeFound("isCompleteMonth.equals=" + UPDATED_IS_COMPLETE_MONTH);
    }

    @Test
    @Transactional
    void getAllPeriodsByIsCompleteMonthIsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isCompleteMonth not equals to DEFAULT_IS_COMPLETE_MONTH
        defaultPeriodShouldNotBeFound("isCompleteMonth.notEquals=" + DEFAULT_IS_COMPLETE_MONTH);

        // Get all the periodList where isCompleteMonth not equals to UPDATED_IS_COMPLETE_MONTH
        defaultPeriodShouldBeFound("isCompleteMonth.notEquals=" + UPDATED_IS_COMPLETE_MONTH);
    }

    @Test
    @Transactional
    void getAllPeriodsByIsCompleteMonthIsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isCompleteMonth in DEFAULT_IS_COMPLETE_MONTH or UPDATED_IS_COMPLETE_MONTH
        defaultPeriodShouldBeFound("isCompleteMonth.in=" + DEFAULT_IS_COMPLETE_MONTH + "," + UPDATED_IS_COMPLETE_MONTH);

        // Get all the periodList where isCompleteMonth equals to UPDATED_IS_COMPLETE_MONTH
        defaultPeriodShouldNotBeFound("isCompleteMonth.in=" + UPDATED_IS_COMPLETE_MONTH);
    }

    @Test
    @Transactional
    void getAllPeriodsByIsCompleteMonthIsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where isCompleteMonth is not null
        defaultPeriodShouldBeFound("isCompleteMonth.specified=true");

        // Get all the periodList where isCompleteMonth is null
        defaultPeriodShouldNotBeFound("isCompleteMonth.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsByPeriodTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where periodType equals to DEFAULT_PERIOD_TYPE
        defaultPeriodShouldBeFound("periodType.equals=" + DEFAULT_PERIOD_TYPE);

        // Get all the periodList where periodType equals to UPDATED_PERIOD_TYPE
        defaultPeriodShouldNotBeFound("periodType.equals=" + UPDATED_PERIOD_TYPE);
    }

    @Test
    @Transactional
    void getAllPeriodsByPeriodTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where periodType not equals to DEFAULT_PERIOD_TYPE
        defaultPeriodShouldNotBeFound("periodType.notEquals=" + DEFAULT_PERIOD_TYPE);

        // Get all the periodList where periodType not equals to UPDATED_PERIOD_TYPE
        defaultPeriodShouldBeFound("periodType.notEquals=" + UPDATED_PERIOD_TYPE);
    }

    @Test
    @Transactional
    void getAllPeriodsByPeriodTypeIsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where periodType in DEFAULT_PERIOD_TYPE or UPDATED_PERIOD_TYPE
        defaultPeriodShouldBeFound("periodType.in=" + DEFAULT_PERIOD_TYPE + "," + UPDATED_PERIOD_TYPE);

        // Get all the periodList where periodType equals to UPDATED_PERIOD_TYPE
        defaultPeriodShouldNotBeFound("periodType.in=" + UPDATED_PERIOD_TYPE);
    }

    @Test
    @Transactional
    void getAllPeriodsByPeriodTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where periodType is not null
        defaultPeriodShouldBeFound("periodType.specified=true");

        // Get all the periodList where periodType is null
        defaultPeriodShouldNotBeFound("periodType.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsByOperationTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where operationType equals to DEFAULT_OPERATION_TYPE
        defaultPeriodShouldBeFound("operationType.equals=" + DEFAULT_OPERATION_TYPE);

        // Get all the periodList where operationType equals to UPDATED_OPERATION_TYPE
        defaultPeriodShouldNotBeFound("operationType.equals=" + UPDATED_OPERATION_TYPE);
    }

    @Test
    @Transactional
    void getAllPeriodsByOperationTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where operationType not equals to DEFAULT_OPERATION_TYPE
        defaultPeriodShouldNotBeFound("operationType.notEquals=" + DEFAULT_OPERATION_TYPE);

        // Get all the periodList where operationType not equals to UPDATED_OPERATION_TYPE
        defaultPeriodShouldBeFound("operationType.notEquals=" + UPDATED_OPERATION_TYPE);
    }

    @Test
    @Transactional
    void getAllPeriodsByOperationTypeIsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where operationType in DEFAULT_OPERATION_TYPE or UPDATED_OPERATION_TYPE
        defaultPeriodShouldBeFound("operationType.in=" + DEFAULT_OPERATION_TYPE + "," + UPDATED_OPERATION_TYPE);

        // Get all the periodList where operationType equals to UPDATED_OPERATION_TYPE
        defaultPeriodShouldNotBeFound("operationType.in=" + UPDATED_OPERATION_TYPE);
    }

    @Test
    @Transactional
    void getAllPeriodsByOperationTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where operationType is not null
        defaultPeriodShouldBeFound("operationType.specified=true");

        // Get all the periodList where operationType is null
        defaultPeriodShouldNotBeFound("operationType.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where createdBy equals to DEFAULT_CREATED_BY
        defaultPeriodShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the periodList where createdBy equals to UPDATED_CREATED_BY
        defaultPeriodShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPeriodsByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where createdBy not equals to DEFAULT_CREATED_BY
        defaultPeriodShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the periodList where createdBy not equals to UPDATED_CREATED_BY
        defaultPeriodShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPeriodsByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultPeriodShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the periodList where createdBy equals to UPDATED_CREATED_BY
        defaultPeriodShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPeriodsByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where createdBy is not null
        defaultPeriodShouldBeFound("createdBy.specified=true");

        // Get all the periodList where createdBy is null
        defaultPeriodShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where createdBy contains DEFAULT_CREATED_BY
        defaultPeriodShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the periodList where createdBy contains UPDATED_CREATED_BY
        defaultPeriodShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPeriodsByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where createdBy does not contain DEFAULT_CREATED_BY
        defaultPeriodShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the periodList where createdBy does not contain UPDATED_CREATED_BY
        defaultPeriodShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPeriodsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where createdDate equals to DEFAULT_CREATED_DATE
        defaultPeriodShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the periodList where createdDate equals to UPDATED_CREATED_DATE
        defaultPeriodShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllPeriodsByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultPeriodShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the periodList where createdDate not equals to UPDATED_CREATED_DATE
        defaultPeriodShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllPeriodsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultPeriodShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the periodList where createdDate equals to UPDATED_CREATED_DATE
        defaultPeriodShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllPeriodsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where createdDate is not null
        defaultPeriodShouldBeFound("createdDate.specified=true");

        // Get all the periodList where createdDate is null
        defaultPeriodShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultPeriodShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the periodList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultPeriodShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllPeriodsByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultPeriodShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the periodList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultPeriodShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllPeriodsByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultPeriodShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the periodList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultPeriodShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllPeriodsByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where lastModifiedBy is not null
        defaultPeriodShouldBeFound("lastModifiedBy.specified=true");

        // Get all the periodList where lastModifiedBy is null
        defaultPeriodShouldNotBeFound("lastModifiedBy.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultPeriodShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the periodList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultPeriodShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllPeriodsByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultPeriodShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the periodList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultPeriodShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllPeriodsByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultPeriodShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the periodList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultPeriodShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllPeriodsByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultPeriodShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the periodList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultPeriodShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllPeriodsByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultPeriodShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the periodList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultPeriodShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllPeriodsByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where lastModifiedDate is not null
        defaultPeriodShouldBeFound("lastModifiedDate.specified=true");

        // Get all the periodList where lastModifiedDate is null
        defaultPeriodShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsByDaysOrDatesOfOccurrenceIsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where daysOrDatesOfOccurrence equals to DEFAULT_DAYS_OR_DATES_OF_OCCURRENCE
        defaultPeriodShouldBeFound("daysOrDatesOfOccurrence.equals=" + DEFAULT_DAYS_OR_DATES_OF_OCCURRENCE);

        // Get all the periodList where daysOrDatesOfOccurrence equals to UPDATED_DAYS_OR_DATES_OF_OCCURRENCE
        defaultPeriodShouldNotBeFound("daysOrDatesOfOccurrence.equals=" + UPDATED_DAYS_OR_DATES_OF_OCCURRENCE);
    }

    @Test
    @Transactional
    void getAllPeriodsByDaysOrDatesOfOccurrenceIsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where daysOrDatesOfOccurrence not equals to DEFAULT_DAYS_OR_DATES_OF_OCCURRENCE
        defaultPeriodShouldNotBeFound("daysOrDatesOfOccurrence.notEquals=" + DEFAULT_DAYS_OR_DATES_OF_OCCURRENCE);

        // Get all the periodList where daysOrDatesOfOccurrence not equals to UPDATED_DAYS_OR_DATES_OF_OCCURRENCE
        defaultPeriodShouldBeFound("daysOrDatesOfOccurrence.notEquals=" + UPDATED_DAYS_OR_DATES_OF_OCCURRENCE);
    }

    @Test
    @Transactional
    void getAllPeriodsByDaysOrDatesOfOccurrenceIsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where daysOrDatesOfOccurrence in DEFAULT_DAYS_OR_DATES_OF_OCCURRENCE or UPDATED_DAYS_OR_DATES_OF_OCCURRENCE
        defaultPeriodShouldBeFound(
            "daysOrDatesOfOccurrence.in=" + DEFAULT_DAYS_OR_DATES_OF_OCCURRENCE + "," + UPDATED_DAYS_OR_DATES_OF_OCCURRENCE
        );

        // Get all the periodList where daysOrDatesOfOccurrence equals to UPDATED_DAYS_OR_DATES_OF_OCCURRENCE
        defaultPeriodShouldNotBeFound("daysOrDatesOfOccurrence.in=" + UPDATED_DAYS_OR_DATES_OF_OCCURRENCE);
    }

    @Test
    @Transactional
    void getAllPeriodsByDaysOrDatesOfOccurrenceIsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where daysOrDatesOfOccurrence is not null
        defaultPeriodShouldBeFound("daysOrDatesOfOccurrence.specified=true");

        // Get all the periodList where daysOrDatesOfOccurrence is null
        defaultPeriodShouldNotBeFound("daysOrDatesOfOccurrence.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsByDaysOrDatesOfOccurrenceContainsSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where daysOrDatesOfOccurrence contains DEFAULT_DAYS_OR_DATES_OF_OCCURRENCE
        defaultPeriodShouldBeFound("daysOrDatesOfOccurrence.contains=" + DEFAULT_DAYS_OR_DATES_OF_OCCURRENCE);

        // Get all the periodList where daysOrDatesOfOccurrence contains UPDATED_DAYS_OR_DATES_OF_OCCURRENCE
        defaultPeriodShouldNotBeFound("daysOrDatesOfOccurrence.contains=" + UPDATED_DAYS_OR_DATES_OF_OCCURRENCE);
    }

    @Test
    @Transactional
    void getAllPeriodsByDaysOrDatesOfOccurrenceNotContainsSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where daysOrDatesOfOccurrence does not contain DEFAULT_DAYS_OR_DATES_OF_OCCURRENCE
        defaultPeriodShouldNotBeFound("daysOrDatesOfOccurrence.doesNotContain=" + DEFAULT_DAYS_OR_DATES_OF_OCCURRENCE);

        // Get all the periodList where daysOrDatesOfOccurrence does not contain UPDATED_DAYS_OR_DATES_OF_OCCURRENCE
        defaultPeriodShouldBeFound("daysOrDatesOfOccurrence.doesNotContain=" + UPDATED_DAYS_OR_DATES_OF_OCCURRENCE);
    }

    @Test
    @Transactional
    void getAllPeriodsBySpare1IsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where spare1 equals to DEFAULT_SPARE_1
        defaultPeriodShouldBeFound("spare1.equals=" + DEFAULT_SPARE_1);

        // Get all the periodList where spare1 equals to UPDATED_SPARE_1
        defaultPeriodShouldNotBeFound("spare1.equals=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllPeriodsBySpare1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where spare1 not equals to DEFAULT_SPARE_1
        defaultPeriodShouldNotBeFound("spare1.notEquals=" + DEFAULT_SPARE_1);

        // Get all the periodList where spare1 not equals to UPDATED_SPARE_1
        defaultPeriodShouldBeFound("spare1.notEquals=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllPeriodsBySpare1IsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where spare1 in DEFAULT_SPARE_1 or UPDATED_SPARE_1
        defaultPeriodShouldBeFound("spare1.in=" + DEFAULT_SPARE_1 + "," + UPDATED_SPARE_1);

        // Get all the periodList where spare1 equals to UPDATED_SPARE_1
        defaultPeriodShouldNotBeFound("spare1.in=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllPeriodsBySpare1IsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where spare1 is not null
        defaultPeriodShouldBeFound("spare1.specified=true");

        // Get all the periodList where spare1 is null
        defaultPeriodShouldNotBeFound("spare1.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsBySpare1ContainsSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where spare1 contains DEFAULT_SPARE_1
        defaultPeriodShouldBeFound("spare1.contains=" + DEFAULT_SPARE_1);

        // Get all the periodList where spare1 contains UPDATED_SPARE_1
        defaultPeriodShouldNotBeFound("spare1.contains=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllPeriodsBySpare1NotContainsSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where spare1 does not contain DEFAULT_SPARE_1
        defaultPeriodShouldNotBeFound("spare1.doesNotContain=" + DEFAULT_SPARE_1);

        // Get all the periodList where spare1 does not contain UPDATED_SPARE_1
        defaultPeriodShouldBeFound("spare1.doesNotContain=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllPeriodsBySpare2IsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where spare2 equals to DEFAULT_SPARE_2
        defaultPeriodShouldBeFound("spare2.equals=" + DEFAULT_SPARE_2);

        // Get all the periodList where spare2 equals to UPDATED_SPARE_2
        defaultPeriodShouldNotBeFound("spare2.equals=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllPeriodsBySpare2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where spare2 not equals to DEFAULT_SPARE_2
        defaultPeriodShouldNotBeFound("spare2.notEquals=" + DEFAULT_SPARE_2);

        // Get all the periodList where spare2 not equals to UPDATED_SPARE_2
        defaultPeriodShouldBeFound("spare2.notEquals=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllPeriodsBySpare2IsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where spare2 in DEFAULT_SPARE_2 or UPDATED_SPARE_2
        defaultPeriodShouldBeFound("spare2.in=" + DEFAULT_SPARE_2 + "," + UPDATED_SPARE_2);

        // Get all the periodList where spare2 equals to UPDATED_SPARE_2
        defaultPeriodShouldNotBeFound("spare2.in=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllPeriodsBySpare2IsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where spare2 is not null
        defaultPeriodShouldBeFound("spare2.specified=true");

        // Get all the periodList where spare2 is null
        defaultPeriodShouldNotBeFound("spare2.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsBySpare2ContainsSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where spare2 contains DEFAULT_SPARE_2
        defaultPeriodShouldBeFound("spare2.contains=" + DEFAULT_SPARE_2);

        // Get all the periodList where spare2 contains UPDATED_SPARE_2
        defaultPeriodShouldNotBeFound("spare2.contains=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllPeriodsBySpare2NotContainsSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where spare2 does not contain DEFAULT_SPARE_2
        defaultPeriodShouldNotBeFound("spare2.doesNotContain=" + DEFAULT_SPARE_2);

        // Get all the periodList where spare2 does not contain UPDATED_SPARE_2
        defaultPeriodShouldBeFound("spare2.doesNotContain=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllPeriodsBySpare3IsEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where spare3 equals to DEFAULT_SPARE_3
        defaultPeriodShouldBeFound("spare3.equals=" + DEFAULT_SPARE_3);

        // Get all the periodList where spare3 equals to UPDATED_SPARE_3
        defaultPeriodShouldNotBeFound("spare3.equals=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllPeriodsBySpare3IsNotEqualToSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where spare3 not equals to DEFAULT_SPARE_3
        defaultPeriodShouldNotBeFound("spare3.notEquals=" + DEFAULT_SPARE_3);

        // Get all the periodList where spare3 not equals to UPDATED_SPARE_3
        defaultPeriodShouldBeFound("spare3.notEquals=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllPeriodsBySpare3IsInShouldWork() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where spare3 in DEFAULT_SPARE_3 or UPDATED_SPARE_3
        defaultPeriodShouldBeFound("spare3.in=" + DEFAULT_SPARE_3 + "," + UPDATED_SPARE_3);

        // Get all the periodList where spare3 equals to UPDATED_SPARE_3
        defaultPeriodShouldNotBeFound("spare3.in=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllPeriodsBySpare3IsNullOrNotNull() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where spare3 is not null
        defaultPeriodShouldBeFound("spare3.specified=true");

        // Get all the periodList where spare3 is null
        defaultPeriodShouldNotBeFound("spare3.specified=false");
    }

    @Test
    @Transactional
    void getAllPeriodsBySpare3ContainsSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where spare3 contains DEFAULT_SPARE_3
        defaultPeriodShouldBeFound("spare3.contains=" + DEFAULT_SPARE_3);

        // Get all the periodList where spare3 contains UPDATED_SPARE_3
        defaultPeriodShouldNotBeFound("spare3.contains=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllPeriodsBySpare3NotContainsSomething() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        // Get all the periodList where spare3 does not contain DEFAULT_SPARE_3
        defaultPeriodShouldNotBeFound("spare3.doesNotContain=" + DEFAULT_SPARE_3);

        // Get all the periodList where spare3 does not contain UPDATED_SPARE_3
        defaultPeriodShouldBeFound("spare3.doesNotContain=" + UPDATED_SPARE_3);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPeriodShouldBeFound(String filter) throws Exception {
        restPeriodMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(period.getId().intValue())))
            .andExpect(jsonPath("$.[*].monthDayFrom").value(hasItem(DEFAULT_MONTH_DAY_FROM)))
            .andExpect(jsonPath("$.[*].monthDayTo").value(hasItem(DEFAULT_MONTH_DAY_TO)))
            .andExpect(jsonPath("$.[*].weekDayFrom").value(hasItem(DEFAULT_WEEK_DAY_FROM)))
            .andExpect(jsonPath("$.[*].weekDayTo").value(hasItem(DEFAULT_WEEK_DAY_TO)))
            .andExpect(jsonPath("$.[*].dayHourFrom").value(hasItem(DEFAULT_DAY_HOUR_FROM)))
            .andExpect(jsonPath("$.[*].dayHourTo").value(hasItem(DEFAULT_DAY_HOUR_TO)))
            .andExpect(jsonPath("$.[*].isPreviousDayHourFrom").value(hasItem(DEFAULT_IS_PREVIOUS_DAY_HOUR_FROM.booleanValue())))
            .andExpect(jsonPath("$.[*].isPreviousDayHourTo").value(hasItem(DEFAULT_IS_PREVIOUS_DAY_HOUR_TO.booleanValue())))
            .andExpect(jsonPath("$.[*].isPreviousMonthDayFrom").value(hasItem(DEFAULT_IS_PREVIOUS_MONTH_DAY_FROM.booleanValue())))
            .andExpect(jsonPath("$.[*].isPreviousMonthDayTo").value(hasItem(DEFAULT_IS_PREVIOUS_MONTH_DAY_TO.booleanValue())))
            .andExpect(jsonPath("$.[*].isCompleteDay").value(hasItem(DEFAULT_IS_COMPLETE_DAY.booleanValue())))
            .andExpect(jsonPath("$.[*].isCompleteMonth").value(hasItem(DEFAULT_IS_COMPLETE_MONTH.booleanValue())))
            .andExpect(jsonPath("$.[*].periodType").value(hasItem(DEFAULT_PERIOD_TYPE.toString())))
            .andExpect(jsonPath("$.[*].operationType").value(hasItem(DEFAULT_OPERATION_TYPE.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].daysOrDatesOfOccurrence").value(hasItem(DEFAULT_DAYS_OR_DATES_OF_OCCURRENCE)))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)));

        // Check, that the count call also returns 1
        restPeriodMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPeriodShouldNotBeFound(String filter) throws Exception {
        restPeriodMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPeriodMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingPeriod() throws Exception {
        // Get the period
        restPeriodMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPeriod() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        int databaseSizeBeforeUpdate = periodRepository.findAll().size();

        // Update the period
        Period updatedPeriod = periodRepository.findById(period.getId()).get();
        // Disconnect from session so that the updates on updatedPeriod are not directly saved in db
        em.detach(updatedPeriod);
        updatedPeriod
            .monthDayFrom(UPDATED_MONTH_DAY_FROM)
            .monthDayTo(UPDATED_MONTH_DAY_TO)
            .weekDayFrom(UPDATED_WEEK_DAY_FROM)
            .weekDayTo(UPDATED_WEEK_DAY_TO)
            .dayHourFrom(UPDATED_DAY_HOUR_FROM)
            .dayHourTo(UPDATED_DAY_HOUR_TO)
            .isPreviousDayHourFrom(UPDATED_IS_PREVIOUS_DAY_HOUR_FROM)
            .isPreviousDayHourTo(UPDATED_IS_PREVIOUS_DAY_HOUR_TO)
            .isPreviousMonthDayFrom(UPDATED_IS_PREVIOUS_MONTH_DAY_FROM)
            .isPreviousMonthDayTo(UPDATED_IS_PREVIOUS_MONTH_DAY_TO)
            .isCompleteDay(UPDATED_IS_COMPLETE_DAY)
            .isCompleteMonth(UPDATED_IS_COMPLETE_MONTH)
            .periodType(UPDATED_PERIOD_TYPE)
            .operationType(UPDATED_OPERATION_TYPE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .daysOrDatesOfOccurrence(UPDATED_DAYS_OR_DATES_OF_OCCURRENCE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);
        PeriodDTO periodDTO = periodMapper.toDto(updatedPeriod);

        restPeriodMockMvc
            .perform(
                put(ENTITY_API_URL_ID, periodDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(periodDTO))
            )
            .andExpect(status().isOk());

        // Validate the Period in the database
        List<Period> periodList = periodRepository.findAll();
        assertThat(periodList).hasSize(databaseSizeBeforeUpdate);
        Period testPeriod = periodList.get(periodList.size() - 1);
        assertThat(testPeriod.getMonthDayFrom()).isEqualTo(UPDATED_MONTH_DAY_FROM);
        assertThat(testPeriod.getMonthDayTo()).isEqualTo(UPDATED_MONTH_DAY_TO);
        assertThat(testPeriod.getWeekDayFrom()).isEqualTo(UPDATED_WEEK_DAY_FROM);
        assertThat(testPeriod.getWeekDayTo()).isEqualTo(UPDATED_WEEK_DAY_TO);
        assertThat(testPeriod.getDayHourFrom()).isEqualTo(UPDATED_DAY_HOUR_FROM);
        assertThat(testPeriod.getDayHourTo()).isEqualTo(UPDATED_DAY_HOUR_TO);
        assertThat(testPeriod.getIsPreviousDayHourFrom()).isEqualTo(UPDATED_IS_PREVIOUS_DAY_HOUR_FROM);
        assertThat(testPeriod.getIsPreviousDayHourTo()).isEqualTo(UPDATED_IS_PREVIOUS_DAY_HOUR_TO);
        assertThat(testPeriod.getIsPreviousMonthDayFrom()).isEqualTo(UPDATED_IS_PREVIOUS_MONTH_DAY_FROM);
        assertThat(testPeriod.getIsPreviousMonthDayTo()).isEqualTo(UPDATED_IS_PREVIOUS_MONTH_DAY_TO);
        assertThat(testPeriod.getIsCompleteDay()).isEqualTo(UPDATED_IS_COMPLETE_DAY);
        assertThat(testPeriod.getIsCompleteMonth()).isEqualTo(UPDATED_IS_COMPLETE_MONTH);
        assertThat(testPeriod.getPeriodType()).isEqualTo(UPDATED_PERIOD_TYPE);
        assertThat(testPeriod.getOperationType()).isEqualTo(UPDATED_OPERATION_TYPE);
        assertThat(testPeriod.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testPeriod.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testPeriod.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testPeriod.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testPeriod.getDaysOrDatesOfOccurrence()).isEqualTo(UPDATED_DAYS_OR_DATES_OF_OCCURRENCE);
        assertThat(testPeriod.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testPeriod.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testPeriod.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void putNonExistingPeriod() throws Exception {
        int databaseSizeBeforeUpdate = periodRepository.findAll().size();
        period.setId(count.incrementAndGet());

        // Create the Period
        PeriodDTO periodDTO = periodMapper.toDto(period);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPeriodMockMvc
            .perform(
                put(ENTITY_API_URL_ID, periodDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(periodDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Period in the database
        List<Period> periodList = periodRepository.findAll();
        assertThat(periodList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPeriod() throws Exception {
        int databaseSizeBeforeUpdate = periodRepository.findAll().size();
        period.setId(count.incrementAndGet());

        // Create the Period
        PeriodDTO periodDTO = periodMapper.toDto(period);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPeriodMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(periodDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Period in the database
        List<Period> periodList = periodRepository.findAll();
        assertThat(periodList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPeriod() throws Exception {
        int databaseSizeBeforeUpdate = periodRepository.findAll().size();
        period.setId(count.incrementAndGet());

        // Create the Period
        PeriodDTO periodDTO = periodMapper.toDto(period);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPeriodMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(periodDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Period in the database
        List<Period> periodList = periodRepository.findAll();
        assertThat(periodList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePeriodWithPatch() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        int databaseSizeBeforeUpdate = periodRepository.findAll().size();

        // Update the period using partial update
        Period partialUpdatedPeriod = new Period();
        partialUpdatedPeriod.setId(period.getId());

        partialUpdatedPeriod
            .isPreviousDayHourFrom(UPDATED_IS_PREVIOUS_DAY_HOUR_FROM)
            .isPreviousMonthDayFrom(UPDATED_IS_PREVIOUS_MONTH_DAY_FROM)
            .isPreviousMonthDayTo(UPDATED_IS_PREVIOUS_MONTH_DAY_TO)
            .isCompleteDay(UPDATED_IS_COMPLETE_DAY)
            .operationType(UPDATED_OPERATION_TYPE)
            .createdDate(UPDATED_CREATED_DATE)
            .spare2(UPDATED_SPARE_2);

        restPeriodMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPeriod.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPeriod))
            )
            .andExpect(status().isOk());

        // Validate the Period in the database
        List<Period> periodList = periodRepository.findAll();
        assertThat(periodList).hasSize(databaseSizeBeforeUpdate);
        Period testPeriod = periodList.get(periodList.size() - 1);
        assertThat(testPeriod.getMonthDayFrom()).isEqualTo(DEFAULT_MONTH_DAY_FROM);
        assertThat(testPeriod.getMonthDayTo()).isEqualTo(DEFAULT_MONTH_DAY_TO);
        assertThat(testPeriod.getWeekDayFrom()).isEqualTo(DEFAULT_WEEK_DAY_FROM);
        assertThat(testPeriod.getWeekDayTo()).isEqualTo(DEFAULT_WEEK_DAY_TO);
        assertThat(testPeriod.getDayHourFrom()).isEqualTo(DEFAULT_DAY_HOUR_FROM);
        assertThat(testPeriod.getDayHourTo()).isEqualTo(DEFAULT_DAY_HOUR_TO);
        assertThat(testPeriod.getIsPreviousDayHourFrom()).isEqualTo(UPDATED_IS_PREVIOUS_DAY_HOUR_FROM);
        assertThat(testPeriod.getIsPreviousDayHourTo()).isEqualTo(DEFAULT_IS_PREVIOUS_DAY_HOUR_TO);
        assertThat(testPeriod.getIsPreviousMonthDayFrom()).isEqualTo(UPDATED_IS_PREVIOUS_MONTH_DAY_FROM);
        assertThat(testPeriod.getIsPreviousMonthDayTo()).isEqualTo(UPDATED_IS_PREVIOUS_MONTH_DAY_TO);
        assertThat(testPeriod.getIsCompleteDay()).isEqualTo(UPDATED_IS_COMPLETE_DAY);
        assertThat(testPeriod.getIsCompleteMonth()).isEqualTo(DEFAULT_IS_COMPLETE_MONTH);
        assertThat(testPeriod.getPeriodType()).isEqualTo(DEFAULT_PERIOD_TYPE);
        assertThat(testPeriod.getOperationType()).isEqualTo(UPDATED_OPERATION_TYPE);
        assertThat(testPeriod.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testPeriod.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testPeriod.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testPeriod.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testPeriod.getDaysOrDatesOfOccurrence()).isEqualTo(DEFAULT_DAYS_OR_DATES_OF_OCCURRENCE);
        assertThat(testPeriod.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testPeriod.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testPeriod.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
    }

    @Test
    @Transactional
    void fullUpdatePeriodWithPatch() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        int databaseSizeBeforeUpdate = periodRepository.findAll().size();

        // Update the period using partial update
        Period partialUpdatedPeriod = new Period();
        partialUpdatedPeriod.setId(period.getId());

        partialUpdatedPeriod
            .monthDayFrom(UPDATED_MONTH_DAY_FROM)
            .monthDayTo(UPDATED_MONTH_DAY_TO)
            .weekDayFrom(UPDATED_WEEK_DAY_FROM)
            .weekDayTo(UPDATED_WEEK_DAY_TO)
            .dayHourFrom(UPDATED_DAY_HOUR_FROM)
            .dayHourTo(UPDATED_DAY_HOUR_TO)
            .isPreviousDayHourFrom(UPDATED_IS_PREVIOUS_DAY_HOUR_FROM)
            .isPreviousDayHourTo(UPDATED_IS_PREVIOUS_DAY_HOUR_TO)
            .isPreviousMonthDayFrom(UPDATED_IS_PREVIOUS_MONTH_DAY_FROM)
            .isPreviousMonthDayTo(UPDATED_IS_PREVIOUS_MONTH_DAY_TO)
            .isCompleteDay(UPDATED_IS_COMPLETE_DAY)
            .isCompleteMonth(UPDATED_IS_COMPLETE_MONTH)
            .periodType(UPDATED_PERIOD_TYPE)
            .operationType(UPDATED_OPERATION_TYPE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .daysOrDatesOfOccurrence(UPDATED_DAYS_OR_DATES_OF_OCCURRENCE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);

        restPeriodMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPeriod.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPeriod))
            )
            .andExpect(status().isOk());

        // Validate the Period in the database
        List<Period> periodList = periodRepository.findAll();
        assertThat(periodList).hasSize(databaseSizeBeforeUpdate);
        Period testPeriod = periodList.get(periodList.size() - 1);
        assertThat(testPeriod.getMonthDayFrom()).isEqualTo(UPDATED_MONTH_DAY_FROM);
        assertThat(testPeriod.getMonthDayTo()).isEqualTo(UPDATED_MONTH_DAY_TO);
        assertThat(testPeriod.getWeekDayFrom()).isEqualTo(UPDATED_WEEK_DAY_FROM);
        assertThat(testPeriod.getWeekDayTo()).isEqualTo(UPDATED_WEEK_DAY_TO);
        assertThat(testPeriod.getDayHourFrom()).isEqualTo(UPDATED_DAY_HOUR_FROM);
        assertThat(testPeriod.getDayHourTo()).isEqualTo(UPDATED_DAY_HOUR_TO);
        assertThat(testPeriod.getIsPreviousDayHourFrom()).isEqualTo(UPDATED_IS_PREVIOUS_DAY_HOUR_FROM);
        assertThat(testPeriod.getIsPreviousDayHourTo()).isEqualTo(UPDATED_IS_PREVIOUS_DAY_HOUR_TO);
        assertThat(testPeriod.getIsPreviousMonthDayFrom()).isEqualTo(UPDATED_IS_PREVIOUS_MONTH_DAY_FROM);
        assertThat(testPeriod.getIsPreviousMonthDayTo()).isEqualTo(UPDATED_IS_PREVIOUS_MONTH_DAY_TO);
        assertThat(testPeriod.getIsCompleteDay()).isEqualTo(UPDATED_IS_COMPLETE_DAY);
        assertThat(testPeriod.getIsCompleteMonth()).isEqualTo(UPDATED_IS_COMPLETE_MONTH);
        assertThat(testPeriod.getPeriodType()).isEqualTo(UPDATED_PERIOD_TYPE);
        assertThat(testPeriod.getOperationType()).isEqualTo(UPDATED_OPERATION_TYPE);
        assertThat(testPeriod.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testPeriod.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testPeriod.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testPeriod.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testPeriod.getDaysOrDatesOfOccurrence()).isEqualTo(UPDATED_DAYS_OR_DATES_OF_OCCURRENCE);
        assertThat(testPeriod.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testPeriod.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testPeriod.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void patchNonExistingPeriod() throws Exception {
        int databaseSizeBeforeUpdate = periodRepository.findAll().size();
        period.setId(count.incrementAndGet());

        // Create the Period
        PeriodDTO periodDTO = periodMapper.toDto(period);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPeriodMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, periodDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(periodDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Period in the database
        List<Period> periodList = periodRepository.findAll();
        assertThat(periodList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPeriod() throws Exception {
        int databaseSizeBeforeUpdate = periodRepository.findAll().size();
        period.setId(count.incrementAndGet());

        // Create the Period
        PeriodDTO periodDTO = periodMapper.toDto(period);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPeriodMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(periodDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Period in the database
        List<Period> periodList = periodRepository.findAll();
        assertThat(periodList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPeriod() throws Exception {
        int databaseSizeBeforeUpdate = periodRepository.findAll().size();
        period.setId(count.incrementAndGet());

        // Create the Period
        PeriodDTO periodDTO = periodMapper.toDto(period);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPeriodMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(periodDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Period in the database
        List<Period> periodList = periodRepository.findAll();
        assertThat(periodList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePeriod() throws Exception {
        // Initialize the database
        periodRepository.saveAndFlush(period);

        int databaseSizeBeforeDelete = periodRepository.findAll().size();

        // Delete the period
        restPeriodMockMvc
            .perform(delete(ENTITY_API_URL_ID, period.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Period> periodList = periodRepository.findAll();
        assertThat(periodList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
