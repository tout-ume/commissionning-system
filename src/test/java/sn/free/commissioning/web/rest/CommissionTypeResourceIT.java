package sn.free.commissioning.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.IntegrationTest;
import sn.free.commissioning.domain.CommissionType;
import sn.free.commissioning.domain.ConfigurationPlan;
import sn.free.commissioning.domain.enumeration.CommissionTypeType;
import sn.free.commissioning.domain.enumeration.PalierValueType;
import sn.free.commissioning.repository.CommissionTypeRepository;
import sn.free.commissioning.service.criteria.CommissionTypeCriteria;
import sn.free.commissioning.service.dto.CommissionTypeDTO;
import sn.free.commissioning.service.mapper.CommissionTypeMapper;

/**
 * Integration tests for the {@link CommissionTypeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CommissionTypeResourceIT {

    private static final Double DEFAULT_MIN_VALUE = 1D;
    private static final Double UPDATED_MIN_VALUE = 2D;
    private static final Double SMALLER_MIN_VALUE = 1D - 1D;

    private static final Double DEFAULT_MAX_VALUE = 1D;
    private static final Double UPDATED_MAX_VALUE = 2D;
    private static final Double SMALLER_MAX_VALUE = 1D - 1D;

    private static final Double DEFAULT_TAUX_VALUE = 1D;
    private static final Double UPDATED_TAUX_VALUE = 2D;
    private static final Double SMALLER_TAUX_VALUE = 1D - 1D;

    private static final Double DEFAULT_PALIER_VALUE = 1D;
    private static final Double UPDATED_PALIER_VALUE = 2D;
    private static final Double SMALLER_PALIER_VALUE = 1D - 1D;

    private static final CommissionTypeType DEFAULT_COMMISSION_TYPE_TYPE = CommissionTypeType.PALIER;
    private static final CommissionTypeType UPDATED_COMMISSION_TYPE_TYPE = CommissionTypeType.TAUX;

    private static final PalierValueType DEFAULT_PALIER_VALUE_TYPE = PalierValueType.TAUX;
    private static final PalierValueType UPDATED_PALIER_VALUE_TYPE = PalierValueType.FIXE;

    private static final Boolean DEFAULT_IS_INFINITY = false;
    private static final Boolean UPDATED_IS_INFINITY = true;

    private static final String DEFAULT_SPARE_1 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_2 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_3 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_3 = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/commission-types";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CommissionTypeRepository commissionTypeRepository;

    @Autowired
    private CommissionTypeMapper commissionTypeMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCommissionTypeMockMvc;

    private CommissionType commissionType;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CommissionType createEntity(EntityManager em) {
        CommissionType commissionType = new CommissionType()
            .minValue(DEFAULT_MIN_VALUE)
            .maxValue(DEFAULT_MAX_VALUE)
            .tauxValue(DEFAULT_TAUX_VALUE)
            .palierValue(DEFAULT_PALIER_VALUE)
            .commissionTypeType(DEFAULT_COMMISSION_TYPE_TYPE)
            .palierValueType(DEFAULT_PALIER_VALUE_TYPE)
            .isInfinity(DEFAULT_IS_INFINITY)
            .spare1(DEFAULT_SPARE_1)
            .spare2(DEFAULT_SPARE_2)
            .spare3(DEFAULT_SPARE_3);
        return commissionType;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CommissionType createUpdatedEntity(EntityManager em) {
        CommissionType commissionType = new CommissionType()
            .minValue(UPDATED_MIN_VALUE)
            .maxValue(UPDATED_MAX_VALUE)
            .tauxValue(UPDATED_TAUX_VALUE)
            .palierValue(UPDATED_PALIER_VALUE)
            .commissionTypeType(UPDATED_COMMISSION_TYPE_TYPE)
            .palierValueType(UPDATED_PALIER_VALUE_TYPE)
            .isInfinity(UPDATED_IS_INFINITY)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);
        return commissionType;
    }

    @BeforeEach
    public void initTest() {
        commissionType = createEntity(em);
    }

    @Test
    @Transactional
    void createCommissionType() throws Exception {
        int databaseSizeBeforeCreate = commissionTypeRepository.findAll().size();
        // Create the CommissionType
        CommissionTypeDTO commissionTypeDTO = commissionTypeMapper.toDto(commissionType);
        restCommissionTypeMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(commissionTypeDTO))
            )
            .andExpect(status().isCreated());

        // Validate the CommissionType in the database
        List<CommissionType> commissionTypeList = commissionTypeRepository.findAll();
        assertThat(commissionTypeList).hasSize(databaseSizeBeforeCreate + 1);
        CommissionType testCommissionType = commissionTypeList.get(commissionTypeList.size() - 1);
        assertThat(testCommissionType.getMinValue()).isEqualTo(DEFAULT_MIN_VALUE);
        assertThat(testCommissionType.getMaxValue()).isEqualTo(DEFAULT_MAX_VALUE);
        assertThat(testCommissionType.getTauxValue()).isEqualTo(DEFAULT_TAUX_VALUE);
        assertThat(testCommissionType.getPalierValue()).isEqualTo(DEFAULT_PALIER_VALUE);
        assertThat(testCommissionType.getCommissionTypeType()).isEqualTo(DEFAULT_COMMISSION_TYPE_TYPE);
        assertThat(testCommissionType.getPalierValueType()).isEqualTo(DEFAULT_PALIER_VALUE_TYPE);
        assertThat(testCommissionType.getIsInfinity()).isEqualTo(DEFAULT_IS_INFINITY);
        assertThat(testCommissionType.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testCommissionType.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
        assertThat(testCommissionType.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
    }

    @Test
    @Transactional
    void createCommissionTypeWithExistingId() throws Exception {
        // Create the CommissionType with an existing ID
        commissionType.setId(1L);
        CommissionTypeDTO commissionTypeDTO = commissionTypeMapper.toDto(commissionType);

        int databaseSizeBeforeCreate = commissionTypeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCommissionTypeMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(commissionTypeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommissionType in the database
        List<CommissionType> commissionTypeList = commissionTypeRepository.findAll();
        assertThat(commissionTypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllCommissionTypes() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList
        restCommissionTypeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(commissionType.getId().intValue())))
            .andExpect(jsonPath("$.[*].minValue").value(hasItem(DEFAULT_MIN_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].maxValue").value(hasItem(DEFAULT_MAX_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].tauxValue").value(hasItem(DEFAULT_TAUX_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].palierValue").value(hasItem(DEFAULT_PALIER_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].commissionTypeType").value(hasItem(DEFAULT_COMMISSION_TYPE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].palierValueType").value(hasItem(DEFAULT_PALIER_VALUE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].isInfinity").value(hasItem(DEFAULT_IS_INFINITY.booleanValue())))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)));
    }

    @Test
    @Transactional
    void getCommissionType() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get the commissionType
        restCommissionTypeMockMvc
            .perform(get(ENTITY_API_URL_ID, commissionType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(commissionType.getId().intValue()))
            .andExpect(jsonPath("$.minValue").value(DEFAULT_MIN_VALUE.doubleValue()))
            .andExpect(jsonPath("$.maxValue").value(DEFAULT_MAX_VALUE.doubleValue()))
            .andExpect(jsonPath("$.tauxValue").value(DEFAULT_TAUX_VALUE.doubleValue()))
            .andExpect(jsonPath("$.palierValue").value(DEFAULT_PALIER_VALUE.doubleValue()))
            .andExpect(jsonPath("$.commissionTypeType").value(DEFAULT_COMMISSION_TYPE_TYPE.toString()))
            .andExpect(jsonPath("$.palierValueType").value(DEFAULT_PALIER_VALUE_TYPE.toString()))
            .andExpect(jsonPath("$.isInfinity").value(DEFAULT_IS_INFINITY.booleanValue()))
            .andExpect(jsonPath("$.spare1").value(DEFAULT_SPARE_1))
            .andExpect(jsonPath("$.spare2").value(DEFAULT_SPARE_2))
            .andExpect(jsonPath("$.spare3").value(DEFAULT_SPARE_3));
    }

    @Test
    @Transactional
    void getCommissionTypesByIdFiltering() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        Long id = commissionType.getId();

        defaultCommissionTypeShouldBeFound("id.equals=" + id);
        defaultCommissionTypeShouldNotBeFound("id.notEquals=" + id);

        defaultCommissionTypeShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCommissionTypeShouldNotBeFound("id.greaterThan=" + id);

        defaultCommissionTypeShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCommissionTypeShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByMinValueIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where minValue equals to DEFAULT_MIN_VALUE
        defaultCommissionTypeShouldBeFound("minValue.equals=" + DEFAULT_MIN_VALUE);

        // Get all the commissionTypeList where minValue equals to UPDATED_MIN_VALUE
        defaultCommissionTypeShouldNotBeFound("minValue.equals=" + UPDATED_MIN_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByMinValueIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where minValue not equals to DEFAULT_MIN_VALUE
        defaultCommissionTypeShouldNotBeFound("minValue.notEquals=" + DEFAULT_MIN_VALUE);

        // Get all the commissionTypeList where minValue not equals to UPDATED_MIN_VALUE
        defaultCommissionTypeShouldBeFound("minValue.notEquals=" + UPDATED_MIN_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByMinValueIsInShouldWork() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where minValue in DEFAULT_MIN_VALUE or UPDATED_MIN_VALUE
        defaultCommissionTypeShouldBeFound("minValue.in=" + DEFAULT_MIN_VALUE + "," + UPDATED_MIN_VALUE);

        // Get all the commissionTypeList where minValue equals to UPDATED_MIN_VALUE
        defaultCommissionTypeShouldNotBeFound("minValue.in=" + UPDATED_MIN_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByMinValueIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where minValue is not null
        defaultCommissionTypeShouldBeFound("minValue.specified=true");

        // Get all the commissionTypeList where minValue is null
        defaultCommissionTypeShouldNotBeFound("minValue.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionTypesByMinValueIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where minValue is greater than or equal to DEFAULT_MIN_VALUE
        defaultCommissionTypeShouldBeFound("minValue.greaterThanOrEqual=" + DEFAULT_MIN_VALUE);

        // Get all the commissionTypeList where minValue is greater than or equal to UPDATED_MIN_VALUE
        defaultCommissionTypeShouldNotBeFound("minValue.greaterThanOrEqual=" + UPDATED_MIN_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByMinValueIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where minValue is less than or equal to DEFAULT_MIN_VALUE
        defaultCommissionTypeShouldBeFound("minValue.lessThanOrEqual=" + DEFAULT_MIN_VALUE);

        // Get all the commissionTypeList where minValue is less than or equal to SMALLER_MIN_VALUE
        defaultCommissionTypeShouldNotBeFound("minValue.lessThanOrEqual=" + SMALLER_MIN_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByMinValueIsLessThanSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where minValue is less than DEFAULT_MIN_VALUE
        defaultCommissionTypeShouldNotBeFound("minValue.lessThan=" + DEFAULT_MIN_VALUE);

        // Get all the commissionTypeList where minValue is less than UPDATED_MIN_VALUE
        defaultCommissionTypeShouldBeFound("minValue.lessThan=" + UPDATED_MIN_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByMinValueIsGreaterThanSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where minValue is greater than DEFAULT_MIN_VALUE
        defaultCommissionTypeShouldNotBeFound("minValue.greaterThan=" + DEFAULT_MIN_VALUE);

        // Get all the commissionTypeList where minValue is greater than SMALLER_MIN_VALUE
        defaultCommissionTypeShouldBeFound("minValue.greaterThan=" + SMALLER_MIN_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByMaxValueIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where maxValue equals to DEFAULT_MAX_VALUE
        defaultCommissionTypeShouldBeFound("maxValue.equals=" + DEFAULT_MAX_VALUE);

        // Get all the commissionTypeList where maxValue equals to UPDATED_MAX_VALUE
        defaultCommissionTypeShouldNotBeFound("maxValue.equals=" + UPDATED_MAX_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByMaxValueIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where maxValue not equals to DEFAULT_MAX_VALUE
        defaultCommissionTypeShouldNotBeFound("maxValue.notEquals=" + DEFAULT_MAX_VALUE);

        // Get all the commissionTypeList where maxValue not equals to UPDATED_MAX_VALUE
        defaultCommissionTypeShouldBeFound("maxValue.notEquals=" + UPDATED_MAX_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByMaxValueIsInShouldWork() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where maxValue in DEFAULT_MAX_VALUE or UPDATED_MAX_VALUE
        defaultCommissionTypeShouldBeFound("maxValue.in=" + DEFAULT_MAX_VALUE + "," + UPDATED_MAX_VALUE);

        // Get all the commissionTypeList where maxValue equals to UPDATED_MAX_VALUE
        defaultCommissionTypeShouldNotBeFound("maxValue.in=" + UPDATED_MAX_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByMaxValueIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where maxValue is not null
        defaultCommissionTypeShouldBeFound("maxValue.specified=true");

        // Get all the commissionTypeList where maxValue is null
        defaultCommissionTypeShouldNotBeFound("maxValue.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionTypesByMaxValueIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where maxValue is greater than or equal to DEFAULT_MAX_VALUE
        defaultCommissionTypeShouldBeFound("maxValue.greaterThanOrEqual=" + DEFAULT_MAX_VALUE);

        // Get all the commissionTypeList where maxValue is greater than or equal to UPDATED_MAX_VALUE
        defaultCommissionTypeShouldNotBeFound("maxValue.greaterThanOrEqual=" + UPDATED_MAX_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByMaxValueIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where maxValue is less than or equal to DEFAULT_MAX_VALUE
        defaultCommissionTypeShouldBeFound("maxValue.lessThanOrEqual=" + DEFAULT_MAX_VALUE);

        // Get all the commissionTypeList where maxValue is less than or equal to SMALLER_MAX_VALUE
        defaultCommissionTypeShouldNotBeFound("maxValue.lessThanOrEqual=" + SMALLER_MAX_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByMaxValueIsLessThanSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where maxValue is less than DEFAULT_MAX_VALUE
        defaultCommissionTypeShouldNotBeFound("maxValue.lessThan=" + DEFAULT_MAX_VALUE);

        // Get all the commissionTypeList where maxValue is less than UPDATED_MAX_VALUE
        defaultCommissionTypeShouldBeFound("maxValue.lessThan=" + UPDATED_MAX_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByMaxValueIsGreaterThanSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where maxValue is greater than DEFAULT_MAX_VALUE
        defaultCommissionTypeShouldNotBeFound("maxValue.greaterThan=" + DEFAULT_MAX_VALUE);

        // Get all the commissionTypeList where maxValue is greater than SMALLER_MAX_VALUE
        defaultCommissionTypeShouldBeFound("maxValue.greaterThan=" + SMALLER_MAX_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByTauxValueIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where tauxValue equals to DEFAULT_TAUX_VALUE
        defaultCommissionTypeShouldBeFound("tauxValue.equals=" + DEFAULT_TAUX_VALUE);

        // Get all the commissionTypeList where tauxValue equals to UPDATED_TAUX_VALUE
        defaultCommissionTypeShouldNotBeFound("tauxValue.equals=" + UPDATED_TAUX_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByTauxValueIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where tauxValue not equals to DEFAULT_TAUX_VALUE
        defaultCommissionTypeShouldNotBeFound("tauxValue.notEquals=" + DEFAULT_TAUX_VALUE);

        // Get all the commissionTypeList where tauxValue not equals to UPDATED_TAUX_VALUE
        defaultCommissionTypeShouldBeFound("tauxValue.notEquals=" + UPDATED_TAUX_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByTauxValueIsInShouldWork() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where tauxValue in DEFAULT_TAUX_VALUE or UPDATED_TAUX_VALUE
        defaultCommissionTypeShouldBeFound("tauxValue.in=" + DEFAULT_TAUX_VALUE + "," + UPDATED_TAUX_VALUE);

        // Get all the commissionTypeList where tauxValue equals to UPDATED_TAUX_VALUE
        defaultCommissionTypeShouldNotBeFound("tauxValue.in=" + UPDATED_TAUX_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByTauxValueIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where tauxValue is not null
        defaultCommissionTypeShouldBeFound("tauxValue.specified=true");

        // Get all the commissionTypeList where tauxValue is null
        defaultCommissionTypeShouldNotBeFound("tauxValue.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionTypesByTauxValueIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where tauxValue is greater than or equal to DEFAULT_TAUX_VALUE
        defaultCommissionTypeShouldBeFound("tauxValue.greaterThanOrEqual=" + DEFAULT_TAUX_VALUE);

        // Get all the commissionTypeList where tauxValue is greater than or equal to UPDATED_TAUX_VALUE
        defaultCommissionTypeShouldNotBeFound("tauxValue.greaterThanOrEqual=" + UPDATED_TAUX_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByTauxValueIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where tauxValue is less than or equal to DEFAULT_TAUX_VALUE
        defaultCommissionTypeShouldBeFound("tauxValue.lessThanOrEqual=" + DEFAULT_TAUX_VALUE);

        // Get all the commissionTypeList where tauxValue is less than or equal to SMALLER_TAUX_VALUE
        defaultCommissionTypeShouldNotBeFound("tauxValue.lessThanOrEqual=" + SMALLER_TAUX_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByTauxValueIsLessThanSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where tauxValue is less than DEFAULT_TAUX_VALUE
        defaultCommissionTypeShouldNotBeFound("tauxValue.lessThan=" + DEFAULT_TAUX_VALUE);

        // Get all the commissionTypeList where tauxValue is less than UPDATED_TAUX_VALUE
        defaultCommissionTypeShouldBeFound("tauxValue.lessThan=" + UPDATED_TAUX_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByTauxValueIsGreaterThanSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where tauxValue is greater than DEFAULT_TAUX_VALUE
        defaultCommissionTypeShouldNotBeFound("tauxValue.greaterThan=" + DEFAULT_TAUX_VALUE);

        // Get all the commissionTypeList where tauxValue is greater than SMALLER_TAUX_VALUE
        defaultCommissionTypeShouldBeFound("tauxValue.greaterThan=" + SMALLER_TAUX_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByPalierValueIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where palierValue equals to DEFAULT_PALIER_VALUE
        defaultCommissionTypeShouldBeFound("palierValue.equals=" + DEFAULT_PALIER_VALUE);

        // Get all the commissionTypeList where palierValue equals to UPDATED_PALIER_VALUE
        defaultCommissionTypeShouldNotBeFound("palierValue.equals=" + UPDATED_PALIER_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByPalierValueIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where palierValue not equals to DEFAULT_PALIER_VALUE
        defaultCommissionTypeShouldNotBeFound("palierValue.notEquals=" + DEFAULT_PALIER_VALUE);

        // Get all the commissionTypeList where palierValue not equals to UPDATED_PALIER_VALUE
        defaultCommissionTypeShouldBeFound("palierValue.notEquals=" + UPDATED_PALIER_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByPalierValueIsInShouldWork() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where palierValue in DEFAULT_PALIER_VALUE or UPDATED_PALIER_VALUE
        defaultCommissionTypeShouldBeFound("palierValue.in=" + DEFAULT_PALIER_VALUE + "," + UPDATED_PALIER_VALUE);

        // Get all the commissionTypeList where palierValue equals to UPDATED_PALIER_VALUE
        defaultCommissionTypeShouldNotBeFound("palierValue.in=" + UPDATED_PALIER_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByPalierValueIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where palierValue is not null
        defaultCommissionTypeShouldBeFound("palierValue.specified=true");

        // Get all the commissionTypeList where palierValue is null
        defaultCommissionTypeShouldNotBeFound("palierValue.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionTypesByPalierValueIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where palierValue is greater than or equal to DEFAULT_PALIER_VALUE
        defaultCommissionTypeShouldBeFound("palierValue.greaterThanOrEqual=" + DEFAULT_PALIER_VALUE);

        // Get all the commissionTypeList where palierValue is greater than or equal to UPDATED_PALIER_VALUE
        defaultCommissionTypeShouldNotBeFound("palierValue.greaterThanOrEqual=" + UPDATED_PALIER_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByPalierValueIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where palierValue is less than or equal to DEFAULT_PALIER_VALUE
        defaultCommissionTypeShouldBeFound("palierValue.lessThanOrEqual=" + DEFAULT_PALIER_VALUE);

        // Get all the commissionTypeList where palierValue is less than or equal to SMALLER_PALIER_VALUE
        defaultCommissionTypeShouldNotBeFound("palierValue.lessThanOrEqual=" + SMALLER_PALIER_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByPalierValueIsLessThanSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where palierValue is less than DEFAULT_PALIER_VALUE
        defaultCommissionTypeShouldNotBeFound("palierValue.lessThan=" + DEFAULT_PALIER_VALUE);

        // Get all the commissionTypeList where palierValue is less than UPDATED_PALIER_VALUE
        defaultCommissionTypeShouldBeFound("palierValue.lessThan=" + UPDATED_PALIER_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByPalierValueIsGreaterThanSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where palierValue is greater than DEFAULT_PALIER_VALUE
        defaultCommissionTypeShouldNotBeFound("palierValue.greaterThan=" + DEFAULT_PALIER_VALUE);

        // Get all the commissionTypeList where palierValue is greater than SMALLER_PALIER_VALUE
        defaultCommissionTypeShouldBeFound("palierValue.greaterThan=" + SMALLER_PALIER_VALUE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByCommissionTypeTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where commissionTypeType equals to DEFAULT_COMMISSION_TYPE_TYPE
        defaultCommissionTypeShouldBeFound("commissionTypeType.equals=" + DEFAULT_COMMISSION_TYPE_TYPE);

        // Get all the commissionTypeList where commissionTypeType equals to UPDATED_COMMISSION_TYPE_TYPE
        defaultCommissionTypeShouldNotBeFound("commissionTypeType.equals=" + UPDATED_COMMISSION_TYPE_TYPE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByCommissionTypeTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where commissionTypeType not equals to DEFAULT_COMMISSION_TYPE_TYPE
        defaultCommissionTypeShouldNotBeFound("commissionTypeType.notEquals=" + DEFAULT_COMMISSION_TYPE_TYPE);

        // Get all the commissionTypeList where commissionTypeType not equals to UPDATED_COMMISSION_TYPE_TYPE
        defaultCommissionTypeShouldBeFound("commissionTypeType.notEquals=" + UPDATED_COMMISSION_TYPE_TYPE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByCommissionTypeTypeIsInShouldWork() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where commissionTypeType in DEFAULT_COMMISSION_TYPE_TYPE or UPDATED_COMMISSION_TYPE_TYPE
        defaultCommissionTypeShouldBeFound("commissionTypeType.in=" + DEFAULT_COMMISSION_TYPE_TYPE + "," + UPDATED_COMMISSION_TYPE_TYPE);

        // Get all the commissionTypeList where commissionTypeType equals to UPDATED_COMMISSION_TYPE_TYPE
        defaultCommissionTypeShouldNotBeFound("commissionTypeType.in=" + UPDATED_COMMISSION_TYPE_TYPE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByCommissionTypeTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where commissionTypeType is not null
        defaultCommissionTypeShouldBeFound("commissionTypeType.specified=true");

        // Get all the commissionTypeList where commissionTypeType is null
        defaultCommissionTypeShouldNotBeFound("commissionTypeType.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionTypesByPalierValueTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where palierValueType equals to DEFAULT_PALIER_VALUE_TYPE
        defaultCommissionTypeShouldBeFound("palierValueType.equals=" + DEFAULT_PALIER_VALUE_TYPE);

        // Get all the commissionTypeList where palierValueType equals to UPDATED_PALIER_VALUE_TYPE
        defaultCommissionTypeShouldNotBeFound("palierValueType.equals=" + UPDATED_PALIER_VALUE_TYPE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByPalierValueTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where palierValueType not equals to DEFAULT_PALIER_VALUE_TYPE
        defaultCommissionTypeShouldNotBeFound("palierValueType.notEquals=" + DEFAULT_PALIER_VALUE_TYPE);

        // Get all the commissionTypeList where palierValueType not equals to UPDATED_PALIER_VALUE_TYPE
        defaultCommissionTypeShouldBeFound("palierValueType.notEquals=" + UPDATED_PALIER_VALUE_TYPE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByPalierValueTypeIsInShouldWork() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where palierValueType in DEFAULT_PALIER_VALUE_TYPE or UPDATED_PALIER_VALUE_TYPE
        defaultCommissionTypeShouldBeFound("palierValueType.in=" + DEFAULT_PALIER_VALUE_TYPE + "," + UPDATED_PALIER_VALUE_TYPE);

        // Get all the commissionTypeList where palierValueType equals to UPDATED_PALIER_VALUE_TYPE
        defaultCommissionTypeShouldNotBeFound("palierValueType.in=" + UPDATED_PALIER_VALUE_TYPE);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByPalierValueTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where palierValueType is not null
        defaultCommissionTypeShouldBeFound("palierValueType.specified=true");

        // Get all the commissionTypeList where palierValueType is null
        defaultCommissionTypeShouldNotBeFound("palierValueType.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionTypesByIsInfinityIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where isInfinity equals to DEFAULT_IS_INFINITY
        defaultCommissionTypeShouldBeFound("isInfinity.equals=" + DEFAULT_IS_INFINITY);

        // Get all the commissionTypeList where isInfinity equals to UPDATED_IS_INFINITY
        defaultCommissionTypeShouldNotBeFound("isInfinity.equals=" + UPDATED_IS_INFINITY);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByIsInfinityIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where isInfinity not equals to DEFAULT_IS_INFINITY
        defaultCommissionTypeShouldNotBeFound("isInfinity.notEquals=" + DEFAULT_IS_INFINITY);

        // Get all the commissionTypeList where isInfinity not equals to UPDATED_IS_INFINITY
        defaultCommissionTypeShouldBeFound("isInfinity.notEquals=" + UPDATED_IS_INFINITY);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByIsInfinityIsInShouldWork() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where isInfinity in DEFAULT_IS_INFINITY or UPDATED_IS_INFINITY
        defaultCommissionTypeShouldBeFound("isInfinity.in=" + DEFAULT_IS_INFINITY + "," + UPDATED_IS_INFINITY);

        // Get all the commissionTypeList where isInfinity equals to UPDATED_IS_INFINITY
        defaultCommissionTypeShouldNotBeFound("isInfinity.in=" + UPDATED_IS_INFINITY);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByIsInfinityIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where isInfinity is not null
        defaultCommissionTypeShouldBeFound("isInfinity.specified=true");

        // Get all the commissionTypeList where isInfinity is null
        defaultCommissionTypeShouldNotBeFound("isInfinity.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionTypesBySpare1IsEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where spare1 equals to DEFAULT_SPARE_1
        defaultCommissionTypeShouldBeFound("spare1.equals=" + DEFAULT_SPARE_1);

        // Get all the commissionTypeList where spare1 equals to UPDATED_SPARE_1
        defaultCommissionTypeShouldNotBeFound("spare1.equals=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCommissionTypesBySpare1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where spare1 not equals to DEFAULT_SPARE_1
        defaultCommissionTypeShouldNotBeFound("spare1.notEquals=" + DEFAULT_SPARE_1);

        // Get all the commissionTypeList where spare1 not equals to UPDATED_SPARE_1
        defaultCommissionTypeShouldBeFound("spare1.notEquals=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCommissionTypesBySpare1IsInShouldWork() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where spare1 in DEFAULT_SPARE_1 or UPDATED_SPARE_1
        defaultCommissionTypeShouldBeFound("spare1.in=" + DEFAULT_SPARE_1 + "," + UPDATED_SPARE_1);

        // Get all the commissionTypeList where spare1 equals to UPDATED_SPARE_1
        defaultCommissionTypeShouldNotBeFound("spare1.in=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCommissionTypesBySpare1IsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where spare1 is not null
        defaultCommissionTypeShouldBeFound("spare1.specified=true");

        // Get all the commissionTypeList where spare1 is null
        defaultCommissionTypeShouldNotBeFound("spare1.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionTypesBySpare1ContainsSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where spare1 contains DEFAULT_SPARE_1
        defaultCommissionTypeShouldBeFound("spare1.contains=" + DEFAULT_SPARE_1);

        // Get all the commissionTypeList where spare1 contains UPDATED_SPARE_1
        defaultCommissionTypeShouldNotBeFound("spare1.contains=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCommissionTypesBySpare1NotContainsSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where spare1 does not contain DEFAULT_SPARE_1
        defaultCommissionTypeShouldNotBeFound("spare1.doesNotContain=" + DEFAULT_SPARE_1);

        // Get all the commissionTypeList where spare1 does not contain UPDATED_SPARE_1
        defaultCommissionTypeShouldBeFound("spare1.doesNotContain=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCommissionTypesBySpare2IsEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where spare2 equals to DEFAULT_SPARE_2
        defaultCommissionTypeShouldBeFound("spare2.equals=" + DEFAULT_SPARE_2);

        // Get all the commissionTypeList where spare2 equals to UPDATED_SPARE_2
        defaultCommissionTypeShouldNotBeFound("spare2.equals=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCommissionTypesBySpare2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where spare2 not equals to DEFAULT_SPARE_2
        defaultCommissionTypeShouldNotBeFound("spare2.notEquals=" + DEFAULT_SPARE_2);

        // Get all the commissionTypeList where spare2 not equals to UPDATED_SPARE_2
        defaultCommissionTypeShouldBeFound("spare2.notEquals=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCommissionTypesBySpare2IsInShouldWork() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where spare2 in DEFAULT_SPARE_2 or UPDATED_SPARE_2
        defaultCommissionTypeShouldBeFound("spare2.in=" + DEFAULT_SPARE_2 + "," + UPDATED_SPARE_2);

        // Get all the commissionTypeList where spare2 equals to UPDATED_SPARE_2
        defaultCommissionTypeShouldNotBeFound("spare2.in=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCommissionTypesBySpare2IsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where spare2 is not null
        defaultCommissionTypeShouldBeFound("spare2.specified=true");

        // Get all the commissionTypeList where spare2 is null
        defaultCommissionTypeShouldNotBeFound("spare2.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionTypesBySpare2ContainsSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where spare2 contains DEFAULT_SPARE_2
        defaultCommissionTypeShouldBeFound("spare2.contains=" + DEFAULT_SPARE_2);

        // Get all the commissionTypeList where spare2 contains UPDATED_SPARE_2
        defaultCommissionTypeShouldNotBeFound("spare2.contains=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCommissionTypesBySpare2NotContainsSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where spare2 does not contain DEFAULT_SPARE_2
        defaultCommissionTypeShouldNotBeFound("spare2.doesNotContain=" + DEFAULT_SPARE_2);

        // Get all the commissionTypeList where spare2 does not contain UPDATED_SPARE_2
        defaultCommissionTypeShouldBeFound("spare2.doesNotContain=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCommissionTypesBySpare3IsEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where spare3 equals to DEFAULT_SPARE_3
        defaultCommissionTypeShouldBeFound("spare3.equals=" + DEFAULT_SPARE_3);

        // Get all the commissionTypeList where spare3 equals to UPDATED_SPARE_3
        defaultCommissionTypeShouldNotBeFound("spare3.equals=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCommissionTypesBySpare3IsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where spare3 not equals to DEFAULT_SPARE_3
        defaultCommissionTypeShouldNotBeFound("spare3.notEquals=" + DEFAULT_SPARE_3);

        // Get all the commissionTypeList where spare3 not equals to UPDATED_SPARE_3
        defaultCommissionTypeShouldBeFound("spare3.notEquals=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCommissionTypesBySpare3IsInShouldWork() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where spare3 in DEFAULT_SPARE_3 or UPDATED_SPARE_3
        defaultCommissionTypeShouldBeFound("spare3.in=" + DEFAULT_SPARE_3 + "," + UPDATED_SPARE_3);

        // Get all the commissionTypeList where spare3 equals to UPDATED_SPARE_3
        defaultCommissionTypeShouldNotBeFound("spare3.in=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCommissionTypesBySpare3IsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where spare3 is not null
        defaultCommissionTypeShouldBeFound("spare3.specified=true");

        // Get all the commissionTypeList where spare3 is null
        defaultCommissionTypeShouldNotBeFound("spare3.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionTypesBySpare3ContainsSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where spare3 contains DEFAULT_SPARE_3
        defaultCommissionTypeShouldBeFound("spare3.contains=" + DEFAULT_SPARE_3);

        // Get all the commissionTypeList where spare3 contains UPDATED_SPARE_3
        defaultCommissionTypeShouldNotBeFound("spare3.contains=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCommissionTypesBySpare3NotContainsSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        // Get all the commissionTypeList where spare3 does not contain DEFAULT_SPARE_3
        defaultCommissionTypeShouldNotBeFound("spare3.doesNotContain=" + DEFAULT_SPARE_3);

        // Get all the commissionTypeList where spare3 does not contain UPDATED_SPARE_3
        defaultCommissionTypeShouldBeFound("spare3.doesNotContain=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCommissionTypesByConfigurationPlanIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);
        ConfigurationPlan configurationPlan;
        if (TestUtil.findAll(em, ConfigurationPlan.class).isEmpty()) {
            configurationPlan = ConfigurationPlanResourceIT.createEntity(em);
            em.persist(configurationPlan);
            em.flush();
        } else {
            configurationPlan = TestUtil.findAll(em, ConfigurationPlan.class).get(0);
        }
        em.persist(configurationPlan);
        em.flush();
        commissionType.setConfigurationPlan(configurationPlan);
        commissionTypeRepository.saveAndFlush(commissionType);
        Long configurationPlanId = configurationPlan.getId();

        // Get all the commissionTypeList where configurationPlan equals to configurationPlanId
        defaultCommissionTypeShouldBeFound("configurationPlanId.equals=" + configurationPlanId);

        // Get all the commissionTypeList where configurationPlan equals to (configurationPlanId + 1)
        defaultCommissionTypeShouldNotBeFound("configurationPlanId.equals=" + (configurationPlanId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCommissionTypeShouldBeFound(String filter) throws Exception {
        restCommissionTypeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(commissionType.getId().intValue())))
            .andExpect(jsonPath("$.[*].minValue").value(hasItem(DEFAULT_MIN_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].maxValue").value(hasItem(DEFAULT_MAX_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].tauxValue").value(hasItem(DEFAULT_TAUX_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].palierValue").value(hasItem(DEFAULT_PALIER_VALUE.doubleValue())))
            .andExpect(jsonPath("$.[*].commissionTypeType").value(hasItem(DEFAULT_COMMISSION_TYPE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].palierValueType").value(hasItem(DEFAULT_PALIER_VALUE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].isInfinity").value(hasItem(DEFAULT_IS_INFINITY.booleanValue())))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)));

        // Check, that the count call also returns 1
        restCommissionTypeMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCommissionTypeShouldNotBeFound(String filter) throws Exception {
        restCommissionTypeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCommissionTypeMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingCommissionType() throws Exception {
        // Get the commissionType
        restCommissionTypeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCommissionType() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        int databaseSizeBeforeUpdate = commissionTypeRepository.findAll().size();

        // Update the commissionType
        CommissionType updatedCommissionType = commissionTypeRepository.findById(commissionType.getId()).get();
        // Disconnect from session so that the updates on updatedCommissionType are not directly saved in db
        em.detach(updatedCommissionType);
        updatedCommissionType
            .minValue(UPDATED_MIN_VALUE)
            .maxValue(UPDATED_MAX_VALUE)
            .tauxValue(UPDATED_TAUX_VALUE)
            .palierValue(UPDATED_PALIER_VALUE)
            .commissionTypeType(UPDATED_COMMISSION_TYPE_TYPE)
            .palierValueType(UPDATED_PALIER_VALUE_TYPE)
            .isInfinity(UPDATED_IS_INFINITY)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);
        CommissionTypeDTO commissionTypeDTO = commissionTypeMapper.toDto(updatedCommissionType);

        restCommissionTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, commissionTypeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissionTypeDTO))
            )
            .andExpect(status().isOk());

        // Validate the CommissionType in the database
        List<CommissionType> commissionTypeList = commissionTypeRepository.findAll();
        assertThat(commissionTypeList).hasSize(databaseSizeBeforeUpdate);
        CommissionType testCommissionType = commissionTypeList.get(commissionTypeList.size() - 1);
        assertThat(testCommissionType.getMinValue()).isEqualTo(UPDATED_MIN_VALUE);
        assertThat(testCommissionType.getMaxValue()).isEqualTo(UPDATED_MAX_VALUE);
        assertThat(testCommissionType.getTauxValue()).isEqualTo(UPDATED_TAUX_VALUE);
        assertThat(testCommissionType.getPalierValue()).isEqualTo(UPDATED_PALIER_VALUE);
        assertThat(testCommissionType.getCommissionTypeType()).isEqualTo(UPDATED_COMMISSION_TYPE_TYPE);
        assertThat(testCommissionType.getPalierValueType()).isEqualTo(UPDATED_PALIER_VALUE_TYPE);
        assertThat(testCommissionType.getIsInfinity()).isEqualTo(UPDATED_IS_INFINITY);
        assertThat(testCommissionType.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testCommissionType.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testCommissionType.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void putNonExistingCommissionType() throws Exception {
        int databaseSizeBeforeUpdate = commissionTypeRepository.findAll().size();
        commissionType.setId(count.incrementAndGet());

        // Create the CommissionType
        CommissionTypeDTO commissionTypeDTO = commissionTypeMapper.toDto(commissionType);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCommissionTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, commissionTypeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissionTypeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommissionType in the database
        List<CommissionType> commissionTypeList = commissionTypeRepository.findAll();
        assertThat(commissionTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCommissionType() throws Exception {
        int databaseSizeBeforeUpdate = commissionTypeRepository.findAll().size();
        commissionType.setId(count.incrementAndGet());

        // Create the CommissionType
        CommissionTypeDTO commissionTypeDTO = commissionTypeMapper.toDto(commissionType);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommissionTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissionTypeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommissionType in the database
        List<CommissionType> commissionTypeList = commissionTypeRepository.findAll();
        assertThat(commissionTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCommissionType() throws Exception {
        int databaseSizeBeforeUpdate = commissionTypeRepository.findAll().size();
        commissionType.setId(count.incrementAndGet());

        // Create the CommissionType
        CommissionTypeDTO commissionTypeDTO = commissionTypeMapper.toDto(commissionType);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommissionTypeMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(commissionTypeDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CommissionType in the database
        List<CommissionType> commissionTypeList = commissionTypeRepository.findAll();
        assertThat(commissionTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCommissionTypeWithPatch() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        int databaseSizeBeforeUpdate = commissionTypeRepository.findAll().size();

        // Update the commissionType using partial update
        CommissionType partialUpdatedCommissionType = new CommissionType();
        partialUpdatedCommissionType.setId(commissionType.getId());

        partialUpdatedCommissionType
            .minValue(UPDATED_MIN_VALUE)
            .maxValue(UPDATED_MAX_VALUE)
            .palierValue(UPDATED_PALIER_VALUE)
            .commissionTypeType(UPDATED_COMMISSION_TYPE_TYPE)
            .isInfinity(UPDATED_IS_INFINITY)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2);

        restCommissionTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCommissionType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCommissionType))
            )
            .andExpect(status().isOk());

        // Validate the CommissionType in the database
        List<CommissionType> commissionTypeList = commissionTypeRepository.findAll();
        assertThat(commissionTypeList).hasSize(databaseSizeBeforeUpdate);
        CommissionType testCommissionType = commissionTypeList.get(commissionTypeList.size() - 1);
        assertThat(testCommissionType.getMinValue()).isEqualTo(UPDATED_MIN_VALUE);
        assertThat(testCommissionType.getMaxValue()).isEqualTo(UPDATED_MAX_VALUE);
        assertThat(testCommissionType.getTauxValue()).isEqualTo(DEFAULT_TAUX_VALUE);
        assertThat(testCommissionType.getPalierValue()).isEqualTo(UPDATED_PALIER_VALUE);
        assertThat(testCommissionType.getCommissionTypeType()).isEqualTo(UPDATED_COMMISSION_TYPE_TYPE);
        assertThat(testCommissionType.getPalierValueType()).isEqualTo(DEFAULT_PALIER_VALUE_TYPE);
        assertThat(testCommissionType.getIsInfinity()).isEqualTo(UPDATED_IS_INFINITY);
        assertThat(testCommissionType.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testCommissionType.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testCommissionType.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
    }

    @Test
    @Transactional
    void fullUpdateCommissionTypeWithPatch() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        int databaseSizeBeforeUpdate = commissionTypeRepository.findAll().size();

        // Update the commissionType using partial update
        CommissionType partialUpdatedCommissionType = new CommissionType();
        partialUpdatedCommissionType.setId(commissionType.getId());

        partialUpdatedCommissionType
            .minValue(UPDATED_MIN_VALUE)
            .maxValue(UPDATED_MAX_VALUE)
            .tauxValue(UPDATED_TAUX_VALUE)
            .palierValue(UPDATED_PALIER_VALUE)
            .commissionTypeType(UPDATED_COMMISSION_TYPE_TYPE)
            .palierValueType(UPDATED_PALIER_VALUE_TYPE)
            .isInfinity(UPDATED_IS_INFINITY)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);

        restCommissionTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCommissionType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCommissionType))
            )
            .andExpect(status().isOk());

        // Validate the CommissionType in the database
        List<CommissionType> commissionTypeList = commissionTypeRepository.findAll();
        assertThat(commissionTypeList).hasSize(databaseSizeBeforeUpdate);
        CommissionType testCommissionType = commissionTypeList.get(commissionTypeList.size() - 1);
        assertThat(testCommissionType.getMinValue()).isEqualTo(UPDATED_MIN_VALUE);
        assertThat(testCommissionType.getMaxValue()).isEqualTo(UPDATED_MAX_VALUE);
        assertThat(testCommissionType.getTauxValue()).isEqualTo(UPDATED_TAUX_VALUE);
        assertThat(testCommissionType.getPalierValue()).isEqualTo(UPDATED_PALIER_VALUE);
        assertThat(testCommissionType.getCommissionTypeType()).isEqualTo(UPDATED_COMMISSION_TYPE_TYPE);
        assertThat(testCommissionType.getPalierValueType()).isEqualTo(UPDATED_PALIER_VALUE_TYPE);
        assertThat(testCommissionType.getIsInfinity()).isEqualTo(UPDATED_IS_INFINITY);
        assertThat(testCommissionType.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testCommissionType.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testCommissionType.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void patchNonExistingCommissionType() throws Exception {
        int databaseSizeBeforeUpdate = commissionTypeRepository.findAll().size();
        commissionType.setId(count.incrementAndGet());

        // Create the CommissionType
        CommissionTypeDTO commissionTypeDTO = commissionTypeMapper.toDto(commissionType);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCommissionTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, commissionTypeDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(commissionTypeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommissionType in the database
        List<CommissionType> commissionTypeList = commissionTypeRepository.findAll();
        assertThat(commissionTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCommissionType() throws Exception {
        int databaseSizeBeforeUpdate = commissionTypeRepository.findAll().size();
        commissionType.setId(count.incrementAndGet());

        // Create the CommissionType
        CommissionTypeDTO commissionTypeDTO = commissionTypeMapper.toDto(commissionType);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommissionTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(commissionTypeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CommissionType in the database
        List<CommissionType> commissionTypeList = commissionTypeRepository.findAll();
        assertThat(commissionTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCommissionType() throws Exception {
        int databaseSizeBeforeUpdate = commissionTypeRepository.findAll().size();
        commissionType.setId(count.incrementAndGet());

        // Create the CommissionType
        CommissionTypeDTO commissionTypeDTO = commissionTypeMapper.toDto(commissionType);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommissionTypeMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(commissionTypeDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CommissionType in the database
        List<CommissionType> commissionTypeList = commissionTypeRepository.findAll();
        assertThat(commissionTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCommissionType() throws Exception {
        // Initialize the database
        commissionTypeRepository.saveAndFlush(commissionType);

        int databaseSizeBeforeDelete = commissionTypeRepository.findAll().size();

        // Delete the commissionType
        restCommissionTypeMockMvc
            .perform(delete(ENTITY_API_URL_ID, commissionType.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CommissionType> commissionTypeList = commissionTypeRepository.findAll();
        assertThat(commissionTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
