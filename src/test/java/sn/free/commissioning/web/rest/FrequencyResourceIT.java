package sn.free.commissioning.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.IntegrationTest;
import sn.free.commissioning.domain.Frequency;
import sn.free.commissioning.domain.enumeration.Cycle;
import sn.free.commissioning.domain.enumeration.FrequencyType;
import sn.free.commissioning.domain.enumeration.OperationType;
import sn.free.commissioning.domain.enumeration.PeriodOfOccurrence;
import sn.free.commissioning.repository.FrequencyRepository;
import sn.free.commissioning.service.criteria.FrequencyCriteria;
import sn.free.commissioning.service.dto.FrequencyDTO;
import sn.free.commissioning.service.mapper.FrequencyMapper;

/**
 * Integration tests for the {@link FrequencyResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class FrequencyResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final FrequencyType DEFAULT_TYPE = FrequencyType.INSTANTLY;
    private static final FrequencyType UPDATED_TYPE = FrequencyType.DAILY;

    private static final OperationType DEFAULT_OPERATION_TYPE = OperationType.CALCULUS;
    private static final OperationType UPDATED_OPERATION_TYPE = OperationType.PAYMENT;

    private static final Integer DEFAULT_EXECUTION_TIME = 1;
    private static final Integer UPDATED_EXECUTION_TIME = 2;
    private static final Integer SMALLER_EXECUTION_TIME = 1 - 1;

    private static final Integer DEFAULT_EXECUTION_WEEK_DAY = 1;
    private static final Integer UPDATED_EXECUTION_WEEK_DAY = 2;
    private static final Integer SMALLER_EXECUTION_WEEK_DAY = 1 - 1;

    private static final Integer DEFAULT_EXECUTION_MONTH_DAY = 1;
    private static final Integer UPDATED_EXECUTION_MONTH_DAY = 2;
    private static final Integer SMALLER_EXECUTION_MONTH_DAY = 1 - 1;

    private static final Integer DEFAULT_DAYS_AFTER = 0;
    private static final Integer UPDATED_DAYS_AFTER = 1;
    private static final Integer SMALLER_DAYS_AFTER = 0 - 1;

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Cycle DEFAULT_CYCLE = Cycle.DAYS;
    private static final Cycle UPDATED_CYCLE = Cycle.WEEKS;

    private static final PeriodOfOccurrence DEFAULT_PERIOD_OF_OCCURRENCE = PeriodOfOccurrence.WEEK;
    private static final PeriodOfOccurrence UPDATED_PERIOD_OF_OCCURRENCE = PeriodOfOccurrence.MONTH;

    private static final Integer DEFAULT_NUMBER_OF_CYCLE = 1;
    private static final Integer UPDATED_NUMBER_OF_CYCLE = 2;
    private static final Integer SMALLER_NUMBER_OF_CYCLE = 1 - 1;

    private static final Integer DEFAULT_OCCURRENCE_BY_PERIOD = 1;
    private static final Integer UPDATED_OCCURRENCE_BY_PERIOD = 2;
    private static final Integer SMALLER_OCCURRENCE_BY_PERIOD = 1 - 1;

    private static final String DEFAULT_DATES_OF_OCCURRENCE = "AAAAAAAAAA";
    private static final String UPDATED_DATES_OF_OCCURRENCE = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_1 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_2 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_3 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_3 = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/frequencies";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private FrequencyRepository frequencyRepository;

    @Autowired
    private FrequencyMapper frequencyMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFrequencyMockMvc;

    private Frequency frequency;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Frequency createEntity(EntityManager em) {
        Frequency frequency = new Frequency()
            .name(DEFAULT_NAME)
            .type(DEFAULT_TYPE)
            .operationType(DEFAULT_OPERATION_TYPE)
            .executionTime(DEFAULT_EXECUTION_TIME)
            .executionWeekDay(DEFAULT_EXECUTION_WEEK_DAY)
            .executionMonthDay(DEFAULT_EXECUTION_MONTH_DAY)
            .daysAfter(DEFAULT_DAYS_AFTER)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .cycle(DEFAULT_CYCLE)
            .periodOfOccurrence(DEFAULT_PERIOD_OF_OCCURRENCE)
            .numberOfCycle(DEFAULT_NUMBER_OF_CYCLE)
            .occurrenceByPeriod(DEFAULT_OCCURRENCE_BY_PERIOD)
            .datesOfOccurrence(DEFAULT_DATES_OF_OCCURRENCE)
            .spare1(DEFAULT_SPARE_1)
            .spare2(DEFAULT_SPARE_2)
            .spare3(DEFAULT_SPARE_3);
        return frequency;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Frequency createUpdatedEntity(EntityManager em) {
        Frequency frequency = new Frequency()
            .name(UPDATED_NAME)
            .type(UPDATED_TYPE)
            .operationType(UPDATED_OPERATION_TYPE)
            .executionTime(UPDATED_EXECUTION_TIME)
            .executionWeekDay(UPDATED_EXECUTION_WEEK_DAY)
            .executionMonthDay(UPDATED_EXECUTION_MONTH_DAY)
            .daysAfter(UPDATED_DAYS_AFTER)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .cycle(UPDATED_CYCLE)
            .periodOfOccurrence(UPDATED_PERIOD_OF_OCCURRENCE)
            .numberOfCycle(UPDATED_NUMBER_OF_CYCLE)
            .occurrenceByPeriod(UPDATED_OCCURRENCE_BY_PERIOD)
            .datesOfOccurrence(UPDATED_DATES_OF_OCCURRENCE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);
        return frequency;
    }

    @BeforeEach
    public void initTest() {
        frequency = createEntity(em);
    }

    @Test
    @Transactional
    void createFrequency() throws Exception {
        int databaseSizeBeforeCreate = frequencyRepository.findAll().size();
        // Create the Frequency
        FrequencyDTO frequencyDTO = frequencyMapper.toDto(frequency);
        restFrequencyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(frequencyDTO)))
            .andExpect(status().isCreated());

        // Validate the Frequency in the database
        List<Frequency> frequencyList = frequencyRepository.findAll();
        assertThat(frequencyList).hasSize(databaseSizeBeforeCreate + 1);
        Frequency testFrequency = frequencyList.get(frequencyList.size() - 1);
        assertThat(testFrequency.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testFrequency.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testFrequency.getOperationType()).isEqualTo(DEFAULT_OPERATION_TYPE);
        assertThat(testFrequency.getExecutionTime()).isEqualTo(DEFAULT_EXECUTION_TIME);
        assertThat(testFrequency.getExecutionWeekDay()).isEqualTo(DEFAULT_EXECUTION_WEEK_DAY);
        assertThat(testFrequency.getExecutionMonthDay()).isEqualTo(DEFAULT_EXECUTION_MONTH_DAY);
        assertThat(testFrequency.getDaysAfter()).isEqualTo(DEFAULT_DAYS_AFTER);
        assertThat(testFrequency.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testFrequency.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testFrequency.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testFrequency.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testFrequency.getCycle()).isEqualTo(DEFAULT_CYCLE);
        assertThat(testFrequency.getPeriodOfOccurrence()).isEqualTo(DEFAULT_PERIOD_OF_OCCURRENCE);
        assertThat(testFrequency.getNumberOfCycle()).isEqualTo(DEFAULT_NUMBER_OF_CYCLE);
        assertThat(testFrequency.getOccurrenceByPeriod()).isEqualTo(DEFAULT_OCCURRENCE_BY_PERIOD);
        assertThat(testFrequency.getDatesOfOccurrence()).isEqualTo(DEFAULT_DATES_OF_OCCURRENCE);
        assertThat(testFrequency.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testFrequency.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
        assertThat(testFrequency.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
    }

    @Test
    @Transactional
    void createFrequencyWithExistingId() throws Exception {
        // Create the Frequency with an existing ID
        frequency.setId(1L);
        FrequencyDTO frequencyDTO = frequencyMapper.toDto(frequency);

        int databaseSizeBeforeCreate = frequencyRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restFrequencyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(frequencyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Frequency in the database
        List<Frequency> frequencyList = frequencyRepository.findAll();
        assertThat(frequencyList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = frequencyRepository.findAll().size();
        // set the field null
        frequency.setName(null);

        // Create the Frequency, which fails.
        FrequencyDTO frequencyDTO = frequencyMapper.toDto(frequency);

        restFrequencyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(frequencyDTO)))
            .andExpect(status().isBadRequest());

        List<Frequency> frequencyList = frequencyRepository.findAll();
        assertThat(frequencyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = frequencyRepository.findAll().size();
        // set the field null
        frequency.setType(null);

        // Create the Frequency, which fails.
        FrequencyDTO frequencyDTO = frequencyMapper.toDto(frequency);

        restFrequencyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(frequencyDTO)))
            .andExpect(status().isBadRequest());

        List<Frequency> frequencyList = frequencyRepository.findAll();
        assertThat(frequencyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkOperationTypeIsRequired() throws Exception {
        int databaseSizeBeforeTest = frequencyRepository.findAll().size();
        // set the field null
        frequency.setOperationType(null);

        // Create the Frequency, which fails.
        FrequencyDTO frequencyDTO = frequencyMapper.toDto(frequency);

        restFrequencyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(frequencyDTO)))
            .andExpect(status().isBadRequest());

        List<Frequency> frequencyList = frequencyRepository.findAll();
        assertThat(frequencyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkExecutionTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = frequencyRepository.findAll().size();
        // set the field null
        frequency.setExecutionTime(null);

        // Create the Frequency, which fails.
        FrequencyDTO frequencyDTO = frequencyMapper.toDto(frequency);

        restFrequencyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(frequencyDTO)))
            .andExpect(status().isBadRequest());

        List<Frequency> frequencyList = frequencyRepository.findAll();
        assertThat(frequencyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCreatedByIsRequired() throws Exception {
        int databaseSizeBeforeTest = frequencyRepository.findAll().size();
        // set the field null
        frequency.setCreatedBy(null);

        // Create the Frequency, which fails.
        FrequencyDTO frequencyDTO = frequencyMapper.toDto(frequency);

        restFrequencyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(frequencyDTO)))
            .andExpect(status().isBadRequest());

        List<Frequency> frequencyList = frequencyRepository.findAll();
        assertThat(frequencyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCreatedDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = frequencyRepository.findAll().size();
        // set the field null
        frequency.setCreatedDate(null);

        // Create the Frequency, which fails.
        FrequencyDTO frequencyDTO = frequencyMapper.toDto(frequency);

        restFrequencyMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(frequencyDTO)))
            .andExpect(status().isBadRequest());

        List<Frequency> frequencyList = frequencyRepository.findAll();
        assertThat(frequencyList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllFrequencies() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList
        restFrequencyMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(frequency.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].operationType").value(hasItem(DEFAULT_OPERATION_TYPE.toString())))
            .andExpect(jsonPath("$.[*].executionTime").value(hasItem(DEFAULT_EXECUTION_TIME)))
            .andExpect(jsonPath("$.[*].executionWeekDay").value(hasItem(DEFAULT_EXECUTION_WEEK_DAY)))
            .andExpect(jsonPath("$.[*].executionMonthDay").value(hasItem(DEFAULT_EXECUTION_MONTH_DAY)))
            .andExpect(jsonPath("$.[*].daysAfter").value(hasItem(DEFAULT_DAYS_AFTER)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].cycle").value(hasItem(DEFAULT_CYCLE.toString())))
            .andExpect(jsonPath("$.[*].periodOfOccurrence").value(hasItem(DEFAULT_PERIOD_OF_OCCURRENCE.toString())))
            .andExpect(jsonPath("$.[*].numberOfCycle").value(hasItem(DEFAULT_NUMBER_OF_CYCLE)))
            .andExpect(jsonPath("$.[*].occurrenceByPeriod").value(hasItem(DEFAULT_OCCURRENCE_BY_PERIOD)))
            .andExpect(jsonPath("$.[*].datesOfOccurrence").value(hasItem(DEFAULT_DATES_OF_OCCURRENCE)))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)));
    }

    @Test
    @Transactional
    void getFrequency() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get the frequency
        restFrequencyMockMvc
            .perform(get(ENTITY_API_URL_ID, frequency.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(frequency.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.operationType").value(DEFAULT_OPERATION_TYPE.toString()))
            .andExpect(jsonPath("$.executionTime").value(DEFAULT_EXECUTION_TIME))
            .andExpect(jsonPath("$.executionWeekDay").value(DEFAULT_EXECUTION_WEEK_DAY))
            .andExpect(jsonPath("$.executionMonthDay").value(DEFAULT_EXECUTION_MONTH_DAY))
            .andExpect(jsonPath("$.daysAfter").value(DEFAULT_DAYS_AFTER))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.cycle").value(DEFAULT_CYCLE.toString()))
            .andExpect(jsonPath("$.periodOfOccurrence").value(DEFAULT_PERIOD_OF_OCCURRENCE.toString()))
            .andExpect(jsonPath("$.numberOfCycle").value(DEFAULT_NUMBER_OF_CYCLE))
            .andExpect(jsonPath("$.occurrenceByPeriod").value(DEFAULT_OCCURRENCE_BY_PERIOD))
            .andExpect(jsonPath("$.datesOfOccurrence").value(DEFAULT_DATES_OF_OCCURRENCE))
            .andExpect(jsonPath("$.spare1").value(DEFAULT_SPARE_1))
            .andExpect(jsonPath("$.spare2").value(DEFAULT_SPARE_2))
            .andExpect(jsonPath("$.spare3").value(DEFAULT_SPARE_3));
    }

    @Test
    @Transactional
    void getFrequenciesByIdFiltering() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        Long id = frequency.getId();

        defaultFrequencyShouldBeFound("id.equals=" + id);
        defaultFrequencyShouldNotBeFound("id.notEquals=" + id);

        defaultFrequencyShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultFrequencyShouldNotBeFound("id.greaterThan=" + id);

        defaultFrequencyShouldBeFound("id.lessThanOrEqual=" + id);
        defaultFrequencyShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllFrequenciesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where name equals to DEFAULT_NAME
        defaultFrequencyShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the frequencyList where name equals to UPDATED_NAME
        defaultFrequencyShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllFrequenciesByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where name not equals to DEFAULT_NAME
        defaultFrequencyShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the frequencyList where name not equals to UPDATED_NAME
        defaultFrequencyShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllFrequenciesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where name in DEFAULT_NAME or UPDATED_NAME
        defaultFrequencyShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the frequencyList where name equals to UPDATED_NAME
        defaultFrequencyShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllFrequenciesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where name is not null
        defaultFrequencyShouldBeFound("name.specified=true");

        // Get all the frequencyList where name is null
        defaultFrequencyShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllFrequenciesByNameContainsSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where name contains DEFAULT_NAME
        defaultFrequencyShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the frequencyList where name contains UPDATED_NAME
        defaultFrequencyShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllFrequenciesByNameNotContainsSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where name does not contain DEFAULT_NAME
        defaultFrequencyShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the frequencyList where name does not contain UPDATED_NAME
        defaultFrequencyShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllFrequenciesByTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where type equals to DEFAULT_TYPE
        defaultFrequencyShouldBeFound("type.equals=" + DEFAULT_TYPE);

        // Get all the frequencyList where type equals to UPDATED_TYPE
        defaultFrequencyShouldNotBeFound("type.equals=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where type not equals to DEFAULT_TYPE
        defaultFrequencyShouldNotBeFound("type.notEquals=" + DEFAULT_TYPE);

        // Get all the frequencyList where type not equals to UPDATED_TYPE
        defaultFrequencyShouldBeFound("type.notEquals=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByTypeIsInShouldWork() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where type in DEFAULT_TYPE or UPDATED_TYPE
        defaultFrequencyShouldBeFound("type.in=" + DEFAULT_TYPE + "," + UPDATED_TYPE);

        // Get all the frequencyList where type equals to UPDATED_TYPE
        defaultFrequencyShouldNotBeFound("type.in=" + UPDATED_TYPE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where type is not null
        defaultFrequencyShouldBeFound("type.specified=true");

        // Get all the frequencyList where type is null
        defaultFrequencyShouldNotBeFound("type.specified=false");
    }

    @Test
    @Transactional
    void getAllFrequenciesByOperationTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where operationType equals to DEFAULT_OPERATION_TYPE
        defaultFrequencyShouldBeFound("operationType.equals=" + DEFAULT_OPERATION_TYPE);

        // Get all the frequencyList where operationType equals to UPDATED_OPERATION_TYPE
        defaultFrequencyShouldNotBeFound("operationType.equals=" + UPDATED_OPERATION_TYPE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByOperationTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where operationType not equals to DEFAULT_OPERATION_TYPE
        defaultFrequencyShouldNotBeFound("operationType.notEquals=" + DEFAULT_OPERATION_TYPE);

        // Get all the frequencyList where operationType not equals to UPDATED_OPERATION_TYPE
        defaultFrequencyShouldBeFound("operationType.notEquals=" + UPDATED_OPERATION_TYPE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByOperationTypeIsInShouldWork() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where operationType in DEFAULT_OPERATION_TYPE or UPDATED_OPERATION_TYPE
        defaultFrequencyShouldBeFound("operationType.in=" + DEFAULT_OPERATION_TYPE + "," + UPDATED_OPERATION_TYPE);

        // Get all the frequencyList where operationType equals to UPDATED_OPERATION_TYPE
        defaultFrequencyShouldNotBeFound("operationType.in=" + UPDATED_OPERATION_TYPE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByOperationTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where operationType is not null
        defaultFrequencyShouldBeFound("operationType.specified=true");

        // Get all the frequencyList where operationType is null
        defaultFrequencyShouldNotBeFound("operationType.specified=false");
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionTimeIsEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionTime equals to DEFAULT_EXECUTION_TIME
        defaultFrequencyShouldBeFound("executionTime.equals=" + DEFAULT_EXECUTION_TIME);

        // Get all the frequencyList where executionTime equals to UPDATED_EXECUTION_TIME
        defaultFrequencyShouldNotBeFound("executionTime.equals=" + UPDATED_EXECUTION_TIME);
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionTimeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionTime not equals to DEFAULT_EXECUTION_TIME
        defaultFrequencyShouldNotBeFound("executionTime.notEquals=" + DEFAULT_EXECUTION_TIME);

        // Get all the frequencyList where executionTime not equals to UPDATED_EXECUTION_TIME
        defaultFrequencyShouldBeFound("executionTime.notEquals=" + UPDATED_EXECUTION_TIME);
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionTimeIsInShouldWork() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionTime in DEFAULT_EXECUTION_TIME or UPDATED_EXECUTION_TIME
        defaultFrequencyShouldBeFound("executionTime.in=" + DEFAULT_EXECUTION_TIME + "," + UPDATED_EXECUTION_TIME);

        // Get all the frequencyList where executionTime equals to UPDATED_EXECUTION_TIME
        defaultFrequencyShouldNotBeFound("executionTime.in=" + UPDATED_EXECUTION_TIME);
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionTimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionTime is not null
        defaultFrequencyShouldBeFound("executionTime.specified=true");

        // Get all the frequencyList where executionTime is null
        defaultFrequencyShouldNotBeFound("executionTime.specified=false");
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionTimeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionTime is greater than or equal to DEFAULT_EXECUTION_TIME
        defaultFrequencyShouldBeFound("executionTime.greaterThanOrEqual=" + DEFAULT_EXECUTION_TIME);

        // Get all the frequencyList where executionTime is greater than or equal to UPDATED_EXECUTION_TIME
        defaultFrequencyShouldNotBeFound("executionTime.greaterThanOrEqual=" + UPDATED_EXECUTION_TIME);
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionTimeIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionTime is less than or equal to DEFAULT_EXECUTION_TIME
        defaultFrequencyShouldBeFound("executionTime.lessThanOrEqual=" + DEFAULT_EXECUTION_TIME);

        // Get all the frequencyList where executionTime is less than or equal to SMALLER_EXECUTION_TIME
        defaultFrequencyShouldNotBeFound("executionTime.lessThanOrEqual=" + SMALLER_EXECUTION_TIME);
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionTimeIsLessThanSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionTime is less than DEFAULT_EXECUTION_TIME
        defaultFrequencyShouldNotBeFound("executionTime.lessThan=" + DEFAULT_EXECUTION_TIME);

        // Get all the frequencyList where executionTime is less than UPDATED_EXECUTION_TIME
        defaultFrequencyShouldBeFound("executionTime.lessThan=" + UPDATED_EXECUTION_TIME);
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionTimeIsGreaterThanSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionTime is greater than DEFAULT_EXECUTION_TIME
        defaultFrequencyShouldNotBeFound("executionTime.greaterThan=" + DEFAULT_EXECUTION_TIME);

        // Get all the frequencyList where executionTime is greater than SMALLER_EXECUTION_TIME
        defaultFrequencyShouldBeFound("executionTime.greaterThan=" + SMALLER_EXECUTION_TIME);
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionWeekDayIsEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionWeekDay equals to DEFAULT_EXECUTION_WEEK_DAY
        defaultFrequencyShouldBeFound("executionWeekDay.equals=" + DEFAULT_EXECUTION_WEEK_DAY);

        // Get all the frequencyList where executionWeekDay equals to UPDATED_EXECUTION_WEEK_DAY
        defaultFrequencyShouldNotBeFound("executionWeekDay.equals=" + UPDATED_EXECUTION_WEEK_DAY);
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionWeekDayIsNotEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionWeekDay not equals to DEFAULT_EXECUTION_WEEK_DAY
        defaultFrequencyShouldNotBeFound("executionWeekDay.notEquals=" + DEFAULT_EXECUTION_WEEK_DAY);

        // Get all the frequencyList where executionWeekDay not equals to UPDATED_EXECUTION_WEEK_DAY
        defaultFrequencyShouldBeFound("executionWeekDay.notEquals=" + UPDATED_EXECUTION_WEEK_DAY);
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionWeekDayIsInShouldWork() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionWeekDay in DEFAULT_EXECUTION_WEEK_DAY or UPDATED_EXECUTION_WEEK_DAY
        defaultFrequencyShouldBeFound("executionWeekDay.in=" + DEFAULT_EXECUTION_WEEK_DAY + "," + UPDATED_EXECUTION_WEEK_DAY);

        // Get all the frequencyList where executionWeekDay equals to UPDATED_EXECUTION_WEEK_DAY
        defaultFrequencyShouldNotBeFound("executionWeekDay.in=" + UPDATED_EXECUTION_WEEK_DAY);
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionWeekDayIsNullOrNotNull() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionWeekDay is not null
        defaultFrequencyShouldBeFound("executionWeekDay.specified=true");

        // Get all the frequencyList where executionWeekDay is null
        defaultFrequencyShouldNotBeFound("executionWeekDay.specified=false");
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionWeekDayIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionWeekDay is greater than or equal to DEFAULT_EXECUTION_WEEK_DAY
        defaultFrequencyShouldBeFound("executionWeekDay.greaterThanOrEqual=" + DEFAULT_EXECUTION_WEEK_DAY);

        // Get all the frequencyList where executionWeekDay is greater than or equal to (DEFAULT_EXECUTION_WEEK_DAY + 1)
        defaultFrequencyShouldNotBeFound("executionWeekDay.greaterThanOrEqual=" + (DEFAULT_EXECUTION_WEEK_DAY + 1));
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionWeekDayIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionWeekDay is less than or equal to DEFAULT_EXECUTION_WEEK_DAY
        defaultFrequencyShouldBeFound("executionWeekDay.lessThanOrEqual=" + DEFAULT_EXECUTION_WEEK_DAY);

        // Get all the frequencyList where executionWeekDay is less than or equal to SMALLER_EXECUTION_WEEK_DAY
        defaultFrequencyShouldNotBeFound("executionWeekDay.lessThanOrEqual=" + SMALLER_EXECUTION_WEEK_DAY);
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionWeekDayIsLessThanSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionWeekDay is less than DEFAULT_EXECUTION_WEEK_DAY
        defaultFrequencyShouldNotBeFound("executionWeekDay.lessThan=" + DEFAULT_EXECUTION_WEEK_DAY);

        // Get all the frequencyList where executionWeekDay is less than (DEFAULT_EXECUTION_WEEK_DAY + 1)
        defaultFrequencyShouldBeFound("executionWeekDay.lessThan=" + (DEFAULT_EXECUTION_WEEK_DAY + 1));
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionWeekDayIsGreaterThanSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionWeekDay is greater than DEFAULT_EXECUTION_WEEK_DAY
        defaultFrequencyShouldNotBeFound("executionWeekDay.greaterThan=" + DEFAULT_EXECUTION_WEEK_DAY);

        // Get all the frequencyList where executionWeekDay is greater than SMALLER_EXECUTION_WEEK_DAY
        defaultFrequencyShouldBeFound("executionWeekDay.greaterThan=" + SMALLER_EXECUTION_WEEK_DAY);
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionMonthDayIsEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionMonthDay equals to DEFAULT_EXECUTION_MONTH_DAY
        defaultFrequencyShouldBeFound("executionMonthDay.equals=" + DEFAULT_EXECUTION_MONTH_DAY);

        // Get all the frequencyList where executionMonthDay equals to UPDATED_EXECUTION_MONTH_DAY
        defaultFrequencyShouldNotBeFound("executionMonthDay.equals=" + UPDATED_EXECUTION_MONTH_DAY);
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionMonthDayIsNotEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionMonthDay not equals to DEFAULT_EXECUTION_MONTH_DAY
        defaultFrequencyShouldNotBeFound("executionMonthDay.notEquals=" + DEFAULT_EXECUTION_MONTH_DAY);

        // Get all the frequencyList where executionMonthDay not equals to UPDATED_EXECUTION_MONTH_DAY
        defaultFrequencyShouldBeFound("executionMonthDay.notEquals=" + UPDATED_EXECUTION_MONTH_DAY);
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionMonthDayIsInShouldWork() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionMonthDay in DEFAULT_EXECUTION_MONTH_DAY or UPDATED_EXECUTION_MONTH_DAY
        defaultFrequencyShouldBeFound("executionMonthDay.in=" + DEFAULT_EXECUTION_MONTH_DAY + "," + UPDATED_EXECUTION_MONTH_DAY);

        // Get all the frequencyList where executionMonthDay equals to UPDATED_EXECUTION_MONTH_DAY
        defaultFrequencyShouldNotBeFound("executionMonthDay.in=" + UPDATED_EXECUTION_MONTH_DAY);
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionMonthDayIsNullOrNotNull() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionMonthDay is not null
        defaultFrequencyShouldBeFound("executionMonthDay.specified=true");

        // Get all the frequencyList where executionMonthDay is null
        defaultFrequencyShouldNotBeFound("executionMonthDay.specified=false");
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionMonthDayIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionMonthDay is greater than or equal to DEFAULT_EXECUTION_MONTH_DAY
        defaultFrequencyShouldBeFound("executionMonthDay.greaterThanOrEqual=" + DEFAULT_EXECUTION_MONTH_DAY);

        // Get all the frequencyList where executionMonthDay is greater than or equal to (DEFAULT_EXECUTION_MONTH_DAY + 1)
        defaultFrequencyShouldNotBeFound("executionMonthDay.greaterThanOrEqual=" + (DEFAULT_EXECUTION_MONTH_DAY + 1));
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionMonthDayIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionMonthDay is less than or equal to DEFAULT_EXECUTION_MONTH_DAY
        defaultFrequencyShouldBeFound("executionMonthDay.lessThanOrEqual=" + DEFAULT_EXECUTION_MONTH_DAY);

        // Get all the frequencyList where executionMonthDay is less than or equal to SMALLER_EXECUTION_MONTH_DAY
        defaultFrequencyShouldNotBeFound("executionMonthDay.lessThanOrEqual=" + SMALLER_EXECUTION_MONTH_DAY);
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionMonthDayIsLessThanSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionMonthDay is less than DEFAULT_EXECUTION_MONTH_DAY
        defaultFrequencyShouldNotBeFound("executionMonthDay.lessThan=" + DEFAULT_EXECUTION_MONTH_DAY);

        // Get all the frequencyList where executionMonthDay is less than (DEFAULT_EXECUTION_MONTH_DAY + 1)
        defaultFrequencyShouldBeFound("executionMonthDay.lessThan=" + (DEFAULT_EXECUTION_MONTH_DAY + 1));
    }

    @Test
    @Transactional
    void getAllFrequenciesByExecutionMonthDayIsGreaterThanSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where executionMonthDay is greater than DEFAULT_EXECUTION_MONTH_DAY
        defaultFrequencyShouldNotBeFound("executionMonthDay.greaterThan=" + DEFAULT_EXECUTION_MONTH_DAY);

        // Get all the frequencyList where executionMonthDay is greater than SMALLER_EXECUTION_MONTH_DAY
        defaultFrequencyShouldBeFound("executionMonthDay.greaterThan=" + SMALLER_EXECUTION_MONTH_DAY);
    }

    @Test
    @Transactional
    void getAllFrequenciesByDaysAfterIsEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where daysAfter equals to DEFAULT_DAYS_AFTER
        defaultFrequencyShouldBeFound("daysAfter.equals=" + DEFAULT_DAYS_AFTER);

        // Get all the frequencyList where daysAfter equals to UPDATED_DAYS_AFTER
        defaultFrequencyShouldNotBeFound("daysAfter.equals=" + UPDATED_DAYS_AFTER);
    }

    @Test
    @Transactional
    void getAllFrequenciesByDaysAfterIsNotEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where daysAfter not equals to DEFAULT_DAYS_AFTER
        defaultFrequencyShouldNotBeFound("daysAfter.notEquals=" + DEFAULT_DAYS_AFTER);

        // Get all the frequencyList where daysAfter not equals to UPDATED_DAYS_AFTER
        defaultFrequencyShouldBeFound("daysAfter.notEquals=" + UPDATED_DAYS_AFTER);
    }

    @Test
    @Transactional
    void getAllFrequenciesByDaysAfterIsInShouldWork() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where daysAfter in DEFAULT_DAYS_AFTER or UPDATED_DAYS_AFTER
        defaultFrequencyShouldBeFound("daysAfter.in=" + DEFAULT_DAYS_AFTER + "," + UPDATED_DAYS_AFTER);

        // Get all the frequencyList where daysAfter equals to UPDATED_DAYS_AFTER
        defaultFrequencyShouldNotBeFound("daysAfter.in=" + UPDATED_DAYS_AFTER);
    }

    @Test
    @Transactional
    void getAllFrequenciesByDaysAfterIsNullOrNotNull() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where daysAfter is not null
        defaultFrequencyShouldBeFound("daysAfter.specified=true");

        // Get all the frequencyList where daysAfter is null
        defaultFrequencyShouldNotBeFound("daysAfter.specified=false");
    }

    @Test
    @Transactional
    void getAllFrequenciesByDaysAfterIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where daysAfter is greater than or equal to DEFAULT_DAYS_AFTER
        defaultFrequencyShouldBeFound("daysAfter.greaterThanOrEqual=" + DEFAULT_DAYS_AFTER);

        // Get all the frequencyList where daysAfter is greater than or equal to (DEFAULT_DAYS_AFTER + 1)
        defaultFrequencyShouldNotBeFound("daysAfter.greaterThanOrEqual=" + (DEFAULT_DAYS_AFTER + 1));
    }

    @Test
    @Transactional
    void getAllFrequenciesByDaysAfterIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where daysAfter is less than or equal to DEFAULT_DAYS_AFTER
        defaultFrequencyShouldBeFound("daysAfter.lessThanOrEqual=" + DEFAULT_DAYS_AFTER);

        // Get all the frequencyList where daysAfter is less than or equal to SMALLER_DAYS_AFTER
        defaultFrequencyShouldNotBeFound("daysAfter.lessThanOrEqual=" + SMALLER_DAYS_AFTER);
    }

    @Test
    @Transactional
    void getAllFrequenciesByDaysAfterIsLessThanSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where daysAfter is less than DEFAULT_DAYS_AFTER
        defaultFrequencyShouldNotBeFound("daysAfter.lessThan=" + DEFAULT_DAYS_AFTER);

        // Get all the frequencyList where daysAfter is less than (DEFAULT_DAYS_AFTER + 1)
        defaultFrequencyShouldBeFound("daysAfter.lessThan=" + (DEFAULT_DAYS_AFTER + 1));
    }

    @Test
    @Transactional
    void getAllFrequenciesByDaysAfterIsGreaterThanSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where daysAfter is greater than DEFAULT_DAYS_AFTER
        defaultFrequencyShouldNotBeFound("daysAfter.greaterThan=" + DEFAULT_DAYS_AFTER);

        // Get all the frequencyList where daysAfter is greater than SMALLER_DAYS_AFTER
        defaultFrequencyShouldBeFound("daysAfter.greaterThan=" + SMALLER_DAYS_AFTER);
    }

    @Test
    @Transactional
    void getAllFrequenciesByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where createdBy equals to DEFAULT_CREATED_BY
        defaultFrequencyShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the frequencyList where createdBy equals to UPDATED_CREATED_BY
        defaultFrequencyShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllFrequenciesByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where createdBy not equals to DEFAULT_CREATED_BY
        defaultFrequencyShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the frequencyList where createdBy not equals to UPDATED_CREATED_BY
        defaultFrequencyShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllFrequenciesByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultFrequencyShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the frequencyList where createdBy equals to UPDATED_CREATED_BY
        defaultFrequencyShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllFrequenciesByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where createdBy is not null
        defaultFrequencyShouldBeFound("createdBy.specified=true");

        // Get all the frequencyList where createdBy is null
        defaultFrequencyShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    void getAllFrequenciesByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where createdBy contains DEFAULT_CREATED_BY
        defaultFrequencyShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the frequencyList where createdBy contains UPDATED_CREATED_BY
        defaultFrequencyShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllFrequenciesByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where createdBy does not contain DEFAULT_CREATED_BY
        defaultFrequencyShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the frequencyList where createdBy does not contain UPDATED_CREATED_BY
        defaultFrequencyShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllFrequenciesByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where createdDate equals to DEFAULT_CREATED_DATE
        defaultFrequencyShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the frequencyList where createdDate equals to UPDATED_CREATED_DATE
        defaultFrequencyShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultFrequencyShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the frequencyList where createdDate not equals to UPDATED_CREATED_DATE
        defaultFrequencyShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultFrequencyShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the frequencyList where createdDate equals to UPDATED_CREATED_DATE
        defaultFrequencyShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where createdDate is not null
        defaultFrequencyShouldBeFound("createdDate.specified=true");

        // Get all the frequencyList where createdDate is null
        defaultFrequencyShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    void getAllFrequenciesByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultFrequencyShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the frequencyList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultFrequencyShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllFrequenciesByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultFrequencyShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the frequencyList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultFrequencyShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllFrequenciesByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultFrequencyShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the frequencyList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultFrequencyShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllFrequenciesByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where lastModifiedBy is not null
        defaultFrequencyShouldBeFound("lastModifiedBy.specified=true");

        // Get all the frequencyList where lastModifiedBy is null
        defaultFrequencyShouldNotBeFound("lastModifiedBy.specified=false");
    }

    @Test
    @Transactional
    void getAllFrequenciesByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultFrequencyShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the frequencyList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultFrequencyShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllFrequenciesByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultFrequencyShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the frequencyList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultFrequencyShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllFrequenciesByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultFrequencyShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the frequencyList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultFrequencyShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultFrequencyShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the frequencyList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultFrequencyShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultFrequencyShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the frequencyList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultFrequencyShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where lastModifiedDate is not null
        defaultFrequencyShouldBeFound("lastModifiedDate.specified=true");

        // Get all the frequencyList where lastModifiedDate is null
        defaultFrequencyShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    void getAllFrequenciesByCycleIsEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where cycle equals to DEFAULT_CYCLE
        defaultFrequencyShouldBeFound("cycle.equals=" + DEFAULT_CYCLE);

        // Get all the frequencyList where cycle equals to UPDATED_CYCLE
        defaultFrequencyShouldNotBeFound("cycle.equals=" + UPDATED_CYCLE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByCycleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where cycle not equals to DEFAULT_CYCLE
        defaultFrequencyShouldNotBeFound("cycle.notEquals=" + DEFAULT_CYCLE);

        // Get all the frequencyList where cycle not equals to UPDATED_CYCLE
        defaultFrequencyShouldBeFound("cycle.notEquals=" + UPDATED_CYCLE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByCycleIsInShouldWork() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where cycle in DEFAULT_CYCLE or UPDATED_CYCLE
        defaultFrequencyShouldBeFound("cycle.in=" + DEFAULT_CYCLE + "," + UPDATED_CYCLE);

        // Get all the frequencyList where cycle equals to UPDATED_CYCLE
        defaultFrequencyShouldNotBeFound("cycle.in=" + UPDATED_CYCLE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByCycleIsNullOrNotNull() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where cycle is not null
        defaultFrequencyShouldBeFound("cycle.specified=true");

        // Get all the frequencyList where cycle is null
        defaultFrequencyShouldNotBeFound("cycle.specified=false");
    }

    @Test
    @Transactional
    void getAllFrequenciesByPeriodOfOccurrenceIsEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where periodOfOccurrence equals to DEFAULT_PERIOD_OF_OCCURRENCE
        defaultFrequencyShouldBeFound("periodOfOccurrence.equals=" + DEFAULT_PERIOD_OF_OCCURRENCE);

        // Get all the frequencyList where periodOfOccurrence equals to UPDATED_PERIOD_OF_OCCURRENCE
        defaultFrequencyShouldNotBeFound("periodOfOccurrence.equals=" + UPDATED_PERIOD_OF_OCCURRENCE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByPeriodOfOccurrenceIsNotEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where periodOfOccurrence not equals to DEFAULT_PERIOD_OF_OCCURRENCE
        defaultFrequencyShouldNotBeFound("periodOfOccurrence.notEquals=" + DEFAULT_PERIOD_OF_OCCURRENCE);

        // Get all the frequencyList where periodOfOccurrence not equals to UPDATED_PERIOD_OF_OCCURRENCE
        defaultFrequencyShouldBeFound("periodOfOccurrence.notEquals=" + UPDATED_PERIOD_OF_OCCURRENCE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByPeriodOfOccurrenceIsInShouldWork() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where periodOfOccurrence in DEFAULT_PERIOD_OF_OCCURRENCE or UPDATED_PERIOD_OF_OCCURRENCE
        defaultFrequencyShouldBeFound("periodOfOccurrence.in=" + DEFAULT_PERIOD_OF_OCCURRENCE + "," + UPDATED_PERIOD_OF_OCCURRENCE);

        // Get all the frequencyList where periodOfOccurrence equals to UPDATED_PERIOD_OF_OCCURRENCE
        defaultFrequencyShouldNotBeFound("periodOfOccurrence.in=" + UPDATED_PERIOD_OF_OCCURRENCE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByPeriodOfOccurrenceIsNullOrNotNull() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where periodOfOccurrence is not null
        defaultFrequencyShouldBeFound("periodOfOccurrence.specified=true");

        // Get all the frequencyList where periodOfOccurrence is null
        defaultFrequencyShouldNotBeFound("periodOfOccurrence.specified=false");
    }

    @Test
    @Transactional
    void getAllFrequenciesByNumberOfCycleIsEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where numberOfCycle equals to DEFAULT_NUMBER_OF_CYCLE
        defaultFrequencyShouldBeFound("numberOfCycle.equals=" + DEFAULT_NUMBER_OF_CYCLE);

        // Get all the frequencyList where numberOfCycle equals to UPDATED_NUMBER_OF_CYCLE
        defaultFrequencyShouldNotBeFound("numberOfCycle.equals=" + UPDATED_NUMBER_OF_CYCLE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByNumberOfCycleIsNotEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where numberOfCycle not equals to DEFAULT_NUMBER_OF_CYCLE
        defaultFrequencyShouldNotBeFound("numberOfCycle.notEquals=" + DEFAULT_NUMBER_OF_CYCLE);

        // Get all the frequencyList where numberOfCycle not equals to UPDATED_NUMBER_OF_CYCLE
        defaultFrequencyShouldBeFound("numberOfCycle.notEquals=" + UPDATED_NUMBER_OF_CYCLE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByNumberOfCycleIsInShouldWork() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where numberOfCycle in DEFAULT_NUMBER_OF_CYCLE or UPDATED_NUMBER_OF_CYCLE
        defaultFrequencyShouldBeFound("numberOfCycle.in=" + DEFAULT_NUMBER_OF_CYCLE + "," + UPDATED_NUMBER_OF_CYCLE);

        // Get all the frequencyList where numberOfCycle equals to UPDATED_NUMBER_OF_CYCLE
        defaultFrequencyShouldNotBeFound("numberOfCycle.in=" + UPDATED_NUMBER_OF_CYCLE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByNumberOfCycleIsNullOrNotNull() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where numberOfCycle is not null
        defaultFrequencyShouldBeFound("numberOfCycle.specified=true");

        // Get all the frequencyList where numberOfCycle is null
        defaultFrequencyShouldNotBeFound("numberOfCycle.specified=false");
    }

    @Test
    @Transactional
    void getAllFrequenciesByNumberOfCycleIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where numberOfCycle is greater than or equal to DEFAULT_NUMBER_OF_CYCLE
        defaultFrequencyShouldBeFound("numberOfCycle.greaterThanOrEqual=" + DEFAULT_NUMBER_OF_CYCLE);

        // Get all the frequencyList where numberOfCycle is greater than or equal to UPDATED_NUMBER_OF_CYCLE
        defaultFrequencyShouldNotBeFound("numberOfCycle.greaterThanOrEqual=" + UPDATED_NUMBER_OF_CYCLE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByNumberOfCycleIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where numberOfCycle is less than or equal to DEFAULT_NUMBER_OF_CYCLE
        defaultFrequencyShouldBeFound("numberOfCycle.lessThanOrEqual=" + DEFAULT_NUMBER_OF_CYCLE);

        // Get all the frequencyList where numberOfCycle is less than or equal to SMALLER_NUMBER_OF_CYCLE
        defaultFrequencyShouldNotBeFound("numberOfCycle.lessThanOrEqual=" + SMALLER_NUMBER_OF_CYCLE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByNumberOfCycleIsLessThanSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where numberOfCycle is less than DEFAULT_NUMBER_OF_CYCLE
        defaultFrequencyShouldNotBeFound("numberOfCycle.lessThan=" + DEFAULT_NUMBER_OF_CYCLE);

        // Get all the frequencyList where numberOfCycle is less than UPDATED_NUMBER_OF_CYCLE
        defaultFrequencyShouldBeFound("numberOfCycle.lessThan=" + UPDATED_NUMBER_OF_CYCLE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByNumberOfCycleIsGreaterThanSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where numberOfCycle is greater than DEFAULT_NUMBER_OF_CYCLE
        defaultFrequencyShouldNotBeFound("numberOfCycle.greaterThan=" + DEFAULT_NUMBER_OF_CYCLE);

        // Get all the frequencyList where numberOfCycle is greater than SMALLER_NUMBER_OF_CYCLE
        defaultFrequencyShouldBeFound("numberOfCycle.greaterThan=" + SMALLER_NUMBER_OF_CYCLE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByOccurrenceByPeriodIsEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where occurrenceByPeriod equals to DEFAULT_OCCURRENCE_BY_PERIOD
        defaultFrequencyShouldBeFound("occurrenceByPeriod.equals=" + DEFAULT_OCCURRENCE_BY_PERIOD);

        // Get all the frequencyList where occurrenceByPeriod equals to UPDATED_OCCURRENCE_BY_PERIOD
        defaultFrequencyShouldNotBeFound("occurrenceByPeriod.equals=" + UPDATED_OCCURRENCE_BY_PERIOD);
    }

    @Test
    @Transactional
    void getAllFrequenciesByOccurrenceByPeriodIsNotEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where occurrenceByPeriod not equals to DEFAULT_OCCURRENCE_BY_PERIOD
        defaultFrequencyShouldNotBeFound("occurrenceByPeriod.notEquals=" + DEFAULT_OCCURRENCE_BY_PERIOD);

        // Get all the frequencyList where occurrenceByPeriod not equals to UPDATED_OCCURRENCE_BY_PERIOD
        defaultFrequencyShouldBeFound("occurrenceByPeriod.notEquals=" + UPDATED_OCCURRENCE_BY_PERIOD);
    }

    @Test
    @Transactional
    void getAllFrequenciesByOccurrenceByPeriodIsInShouldWork() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where occurrenceByPeriod in DEFAULT_OCCURRENCE_BY_PERIOD or UPDATED_OCCURRENCE_BY_PERIOD
        defaultFrequencyShouldBeFound("occurrenceByPeriod.in=" + DEFAULT_OCCURRENCE_BY_PERIOD + "," + UPDATED_OCCURRENCE_BY_PERIOD);

        // Get all the frequencyList where occurrenceByPeriod equals to UPDATED_OCCURRENCE_BY_PERIOD
        defaultFrequencyShouldNotBeFound("occurrenceByPeriod.in=" + UPDATED_OCCURRENCE_BY_PERIOD);
    }

    @Test
    @Transactional
    void getAllFrequenciesByOccurrenceByPeriodIsNullOrNotNull() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where occurrenceByPeriod is not null
        defaultFrequencyShouldBeFound("occurrenceByPeriod.specified=true");

        // Get all the frequencyList where occurrenceByPeriod is null
        defaultFrequencyShouldNotBeFound("occurrenceByPeriod.specified=false");
    }

    @Test
    @Transactional
    void getAllFrequenciesByOccurrenceByPeriodIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where occurrenceByPeriod is greater than or equal to DEFAULT_OCCURRENCE_BY_PERIOD
        defaultFrequencyShouldBeFound("occurrenceByPeriod.greaterThanOrEqual=" + DEFAULT_OCCURRENCE_BY_PERIOD);

        // Get all the frequencyList where occurrenceByPeriod is greater than or equal to UPDATED_OCCURRENCE_BY_PERIOD
        defaultFrequencyShouldNotBeFound("occurrenceByPeriod.greaterThanOrEqual=" + UPDATED_OCCURRENCE_BY_PERIOD);
    }

    @Test
    @Transactional
    void getAllFrequenciesByOccurrenceByPeriodIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where occurrenceByPeriod is less than or equal to DEFAULT_OCCURRENCE_BY_PERIOD
        defaultFrequencyShouldBeFound("occurrenceByPeriod.lessThanOrEqual=" + DEFAULT_OCCURRENCE_BY_PERIOD);

        // Get all the frequencyList where occurrenceByPeriod is less than or equal to SMALLER_OCCURRENCE_BY_PERIOD
        defaultFrequencyShouldNotBeFound("occurrenceByPeriod.lessThanOrEqual=" + SMALLER_OCCURRENCE_BY_PERIOD);
    }

    @Test
    @Transactional
    void getAllFrequenciesByOccurrenceByPeriodIsLessThanSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where occurrenceByPeriod is less than DEFAULT_OCCURRENCE_BY_PERIOD
        defaultFrequencyShouldNotBeFound("occurrenceByPeriod.lessThan=" + DEFAULT_OCCURRENCE_BY_PERIOD);

        // Get all the frequencyList where occurrenceByPeriod is less than UPDATED_OCCURRENCE_BY_PERIOD
        defaultFrequencyShouldBeFound("occurrenceByPeriod.lessThan=" + UPDATED_OCCURRENCE_BY_PERIOD);
    }

    @Test
    @Transactional
    void getAllFrequenciesByOccurrenceByPeriodIsGreaterThanSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where occurrenceByPeriod is greater than DEFAULT_OCCURRENCE_BY_PERIOD
        defaultFrequencyShouldNotBeFound("occurrenceByPeriod.greaterThan=" + DEFAULT_OCCURRENCE_BY_PERIOD);

        // Get all the frequencyList where occurrenceByPeriod is greater than SMALLER_OCCURRENCE_BY_PERIOD
        defaultFrequencyShouldBeFound("occurrenceByPeriod.greaterThan=" + SMALLER_OCCURRENCE_BY_PERIOD);
    }

    @Test
    @Transactional
    void getAllFrequenciesByDatesOfOccurrenceIsEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where datesOfOccurrence equals to DEFAULT_DATES_OF_OCCURRENCE
        defaultFrequencyShouldBeFound("datesOfOccurrence.equals=" + DEFAULT_DATES_OF_OCCURRENCE);

        // Get all the frequencyList where datesOfOccurrence equals to UPDATED_DATES_OF_OCCURRENCE
        defaultFrequencyShouldNotBeFound("datesOfOccurrence.equals=" + UPDATED_DATES_OF_OCCURRENCE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByDatesOfOccurrenceIsNotEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where datesOfOccurrence not equals to DEFAULT_DATES_OF_OCCURRENCE
        defaultFrequencyShouldNotBeFound("datesOfOccurrence.notEquals=" + DEFAULT_DATES_OF_OCCURRENCE);

        // Get all the frequencyList where datesOfOccurrence not equals to UPDATED_DATES_OF_OCCURRENCE
        defaultFrequencyShouldBeFound("datesOfOccurrence.notEquals=" + UPDATED_DATES_OF_OCCURRENCE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByDatesOfOccurrenceIsInShouldWork() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where datesOfOccurrence in DEFAULT_DATES_OF_OCCURRENCE or UPDATED_DATES_OF_OCCURRENCE
        defaultFrequencyShouldBeFound("datesOfOccurrence.in=" + DEFAULT_DATES_OF_OCCURRENCE + "," + UPDATED_DATES_OF_OCCURRENCE);

        // Get all the frequencyList where datesOfOccurrence equals to UPDATED_DATES_OF_OCCURRENCE
        defaultFrequencyShouldNotBeFound("datesOfOccurrence.in=" + UPDATED_DATES_OF_OCCURRENCE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByDatesOfOccurrenceIsNullOrNotNull() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where datesOfOccurrence is not null
        defaultFrequencyShouldBeFound("datesOfOccurrence.specified=true");

        // Get all the frequencyList where datesOfOccurrence is null
        defaultFrequencyShouldNotBeFound("datesOfOccurrence.specified=false");
    }

    @Test
    @Transactional
    void getAllFrequenciesByDatesOfOccurrenceContainsSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where datesOfOccurrence contains DEFAULT_DATES_OF_OCCURRENCE
        defaultFrequencyShouldBeFound("datesOfOccurrence.contains=" + DEFAULT_DATES_OF_OCCURRENCE);

        // Get all the frequencyList where datesOfOccurrence contains UPDATED_DATES_OF_OCCURRENCE
        defaultFrequencyShouldNotBeFound("datesOfOccurrence.contains=" + UPDATED_DATES_OF_OCCURRENCE);
    }

    @Test
    @Transactional
    void getAllFrequenciesByDatesOfOccurrenceNotContainsSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where datesOfOccurrence does not contain DEFAULT_DATES_OF_OCCURRENCE
        defaultFrequencyShouldNotBeFound("datesOfOccurrence.doesNotContain=" + DEFAULT_DATES_OF_OCCURRENCE);

        // Get all the frequencyList where datesOfOccurrence does not contain UPDATED_DATES_OF_OCCURRENCE
        defaultFrequencyShouldBeFound("datesOfOccurrence.doesNotContain=" + UPDATED_DATES_OF_OCCURRENCE);
    }

    @Test
    @Transactional
    void getAllFrequenciesBySpare1IsEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where spare1 equals to DEFAULT_SPARE_1
        defaultFrequencyShouldBeFound("spare1.equals=" + DEFAULT_SPARE_1);

        // Get all the frequencyList where spare1 equals to UPDATED_SPARE_1
        defaultFrequencyShouldNotBeFound("spare1.equals=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllFrequenciesBySpare1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where spare1 not equals to DEFAULT_SPARE_1
        defaultFrequencyShouldNotBeFound("spare1.notEquals=" + DEFAULT_SPARE_1);

        // Get all the frequencyList where spare1 not equals to UPDATED_SPARE_1
        defaultFrequencyShouldBeFound("spare1.notEquals=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllFrequenciesBySpare1IsInShouldWork() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where spare1 in DEFAULT_SPARE_1 or UPDATED_SPARE_1
        defaultFrequencyShouldBeFound("spare1.in=" + DEFAULT_SPARE_1 + "," + UPDATED_SPARE_1);

        // Get all the frequencyList where spare1 equals to UPDATED_SPARE_1
        defaultFrequencyShouldNotBeFound("spare1.in=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllFrequenciesBySpare1IsNullOrNotNull() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where spare1 is not null
        defaultFrequencyShouldBeFound("spare1.specified=true");

        // Get all the frequencyList where spare1 is null
        defaultFrequencyShouldNotBeFound("spare1.specified=false");
    }

    @Test
    @Transactional
    void getAllFrequenciesBySpare1ContainsSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where spare1 contains DEFAULT_SPARE_1
        defaultFrequencyShouldBeFound("spare1.contains=" + DEFAULT_SPARE_1);

        // Get all the frequencyList where spare1 contains UPDATED_SPARE_1
        defaultFrequencyShouldNotBeFound("spare1.contains=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllFrequenciesBySpare1NotContainsSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where spare1 does not contain DEFAULT_SPARE_1
        defaultFrequencyShouldNotBeFound("spare1.doesNotContain=" + DEFAULT_SPARE_1);

        // Get all the frequencyList where spare1 does not contain UPDATED_SPARE_1
        defaultFrequencyShouldBeFound("spare1.doesNotContain=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllFrequenciesBySpare2IsEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where spare2 equals to DEFAULT_SPARE_2
        defaultFrequencyShouldBeFound("spare2.equals=" + DEFAULT_SPARE_2);

        // Get all the frequencyList where spare2 equals to UPDATED_SPARE_2
        defaultFrequencyShouldNotBeFound("spare2.equals=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllFrequenciesBySpare2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where spare2 not equals to DEFAULT_SPARE_2
        defaultFrequencyShouldNotBeFound("spare2.notEquals=" + DEFAULT_SPARE_2);

        // Get all the frequencyList where spare2 not equals to UPDATED_SPARE_2
        defaultFrequencyShouldBeFound("spare2.notEquals=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllFrequenciesBySpare2IsInShouldWork() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where spare2 in DEFAULT_SPARE_2 or UPDATED_SPARE_2
        defaultFrequencyShouldBeFound("spare2.in=" + DEFAULT_SPARE_2 + "," + UPDATED_SPARE_2);

        // Get all the frequencyList where spare2 equals to UPDATED_SPARE_2
        defaultFrequencyShouldNotBeFound("spare2.in=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllFrequenciesBySpare2IsNullOrNotNull() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where spare2 is not null
        defaultFrequencyShouldBeFound("spare2.specified=true");

        // Get all the frequencyList where spare2 is null
        defaultFrequencyShouldNotBeFound("spare2.specified=false");
    }

    @Test
    @Transactional
    void getAllFrequenciesBySpare2ContainsSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where spare2 contains DEFAULT_SPARE_2
        defaultFrequencyShouldBeFound("spare2.contains=" + DEFAULT_SPARE_2);

        // Get all the frequencyList where spare2 contains UPDATED_SPARE_2
        defaultFrequencyShouldNotBeFound("spare2.contains=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllFrequenciesBySpare2NotContainsSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where spare2 does not contain DEFAULT_SPARE_2
        defaultFrequencyShouldNotBeFound("spare2.doesNotContain=" + DEFAULT_SPARE_2);

        // Get all the frequencyList where spare2 does not contain UPDATED_SPARE_2
        defaultFrequencyShouldBeFound("spare2.doesNotContain=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllFrequenciesBySpare3IsEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where spare3 equals to DEFAULT_SPARE_3
        defaultFrequencyShouldBeFound("spare3.equals=" + DEFAULT_SPARE_3);

        // Get all the frequencyList where spare3 equals to UPDATED_SPARE_3
        defaultFrequencyShouldNotBeFound("spare3.equals=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllFrequenciesBySpare3IsNotEqualToSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where spare3 not equals to DEFAULT_SPARE_3
        defaultFrequencyShouldNotBeFound("spare3.notEquals=" + DEFAULT_SPARE_3);

        // Get all the frequencyList where spare3 not equals to UPDATED_SPARE_3
        defaultFrequencyShouldBeFound("spare3.notEquals=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllFrequenciesBySpare3IsInShouldWork() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where spare3 in DEFAULT_SPARE_3 or UPDATED_SPARE_3
        defaultFrequencyShouldBeFound("spare3.in=" + DEFAULT_SPARE_3 + "," + UPDATED_SPARE_3);

        // Get all the frequencyList where spare3 equals to UPDATED_SPARE_3
        defaultFrequencyShouldNotBeFound("spare3.in=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllFrequenciesBySpare3IsNullOrNotNull() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where spare3 is not null
        defaultFrequencyShouldBeFound("spare3.specified=true");

        // Get all the frequencyList where spare3 is null
        defaultFrequencyShouldNotBeFound("spare3.specified=false");
    }

    @Test
    @Transactional
    void getAllFrequenciesBySpare3ContainsSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where spare3 contains DEFAULT_SPARE_3
        defaultFrequencyShouldBeFound("spare3.contains=" + DEFAULT_SPARE_3);

        // Get all the frequencyList where spare3 contains UPDATED_SPARE_3
        defaultFrequencyShouldNotBeFound("spare3.contains=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllFrequenciesBySpare3NotContainsSomething() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        // Get all the frequencyList where spare3 does not contain DEFAULT_SPARE_3
        defaultFrequencyShouldNotBeFound("spare3.doesNotContain=" + DEFAULT_SPARE_3);

        // Get all the frequencyList where spare3 does not contain UPDATED_SPARE_3
        defaultFrequencyShouldBeFound("spare3.doesNotContain=" + UPDATED_SPARE_3);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultFrequencyShouldBeFound(String filter) throws Exception {
        restFrequencyMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(frequency.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].operationType").value(hasItem(DEFAULT_OPERATION_TYPE.toString())))
            .andExpect(jsonPath("$.[*].executionTime").value(hasItem(DEFAULT_EXECUTION_TIME)))
            .andExpect(jsonPath("$.[*].executionWeekDay").value(hasItem(DEFAULT_EXECUTION_WEEK_DAY)))
            .andExpect(jsonPath("$.[*].executionMonthDay").value(hasItem(DEFAULT_EXECUTION_MONTH_DAY)))
            .andExpect(jsonPath("$.[*].daysAfter").value(hasItem(DEFAULT_DAYS_AFTER)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].cycle").value(hasItem(DEFAULT_CYCLE.toString())))
            .andExpect(jsonPath("$.[*].periodOfOccurrence").value(hasItem(DEFAULT_PERIOD_OF_OCCURRENCE.toString())))
            .andExpect(jsonPath("$.[*].numberOfCycle").value(hasItem(DEFAULT_NUMBER_OF_CYCLE)))
            .andExpect(jsonPath("$.[*].occurrenceByPeriod").value(hasItem(DEFAULT_OCCURRENCE_BY_PERIOD)))
            .andExpect(jsonPath("$.[*].datesOfOccurrence").value(hasItem(DEFAULT_DATES_OF_OCCURRENCE)))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)));

        // Check, that the count call also returns 1
        restFrequencyMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultFrequencyShouldNotBeFound(String filter) throws Exception {
        restFrequencyMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restFrequencyMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingFrequency() throws Exception {
        // Get the frequency
        restFrequencyMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewFrequency() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        int databaseSizeBeforeUpdate = frequencyRepository.findAll().size();

        // Update the frequency
        Frequency updatedFrequency = frequencyRepository.findById(frequency.getId()).get();
        // Disconnect from session so that the updates on updatedFrequency are not directly saved in db
        em.detach(updatedFrequency);
        updatedFrequency
            .name(UPDATED_NAME)
            .type(UPDATED_TYPE)
            .operationType(UPDATED_OPERATION_TYPE)
            .executionTime(UPDATED_EXECUTION_TIME)
            .executionWeekDay(UPDATED_EXECUTION_WEEK_DAY)
            .executionMonthDay(UPDATED_EXECUTION_MONTH_DAY)
            .daysAfter(UPDATED_DAYS_AFTER)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .cycle(UPDATED_CYCLE)
            .periodOfOccurrence(UPDATED_PERIOD_OF_OCCURRENCE)
            .numberOfCycle(UPDATED_NUMBER_OF_CYCLE)
            .occurrenceByPeriod(UPDATED_OCCURRENCE_BY_PERIOD)
            .datesOfOccurrence(UPDATED_DATES_OF_OCCURRENCE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);
        FrequencyDTO frequencyDTO = frequencyMapper.toDto(updatedFrequency);

        restFrequencyMockMvc
            .perform(
                put(ENTITY_API_URL_ID, frequencyDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(frequencyDTO))
            )
            .andExpect(status().isOk());

        // Validate the Frequency in the database
        List<Frequency> frequencyList = frequencyRepository.findAll();
        assertThat(frequencyList).hasSize(databaseSizeBeforeUpdate);
        Frequency testFrequency = frequencyList.get(frequencyList.size() - 1);
        assertThat(testFrequency.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testFrequency.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testFrequency.getOperationType()).isEqualTo(UPDATED_OPERATION_TYPE);
        assertThat(testFrequency.getExecutionTime()).isEqualTo(UPDATED_EXECUTION_TIME);
        assertThat(testFrequency.getExecutionWeekDay()).isEqualTo(UPDATED_EXECUTION_WEEK_DAY);
        assertThat(testFrequency.getExecutionMonthDay()).isEqualTo(UPDATED_EXECUTION_MONTH_DAY);
        assertThat(testFrequency.getDaysAfter()).isEqualTo(UPDATED_DAYS_AFTER);
        assertThat(testFrequency.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testFrequency.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testFrequency.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testFrequency.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testFrequency.getCycle()).isEqualTo(UPDATED_CYCLE);
        assertThat(testFrequency.getPeriodOfOccurrence()).isEqualTo(UPDATED_PERIOD_OF_OCCURRENCE);
        assertThat(testFrequency.getNumberOfCycle()).isEqualTo(UPDATED_NUMBER_OF_CYCLE);
        assertThat(testFrequency.getOccurrenceByPeriod()).isEqualTo(UPDATED_OCCURRENCE_BY_PERIOD);
        assertThat(testFrequency.getDatesOfOccurrence()).isEqualTo(UPDATED_DATES_OF_OCCURRENCE);
        assertThat(testFrequency.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testFrequency.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testFrequency.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void putNonExistingFrequency() throws Exception {
        int databaseSizeBeforeUpdate = frequencyRepository.findAll().size();
        frequency.setId(count.incrementAndGet());

        // Create the Frequency
        FrequencyDTO frequencyDTO = frequencyMapper.toDto(frequency);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFrequencyMockMvc
            .perform(
                put(ENTITY_API_URL_ID, frequencyDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(frequencyDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Frequency in the database
        List<Frequency> frequencyList = frequencyRepository.findAll();
        assertThat(frequencyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchFrequency() throws Exception {
        int databaseSizeBeforeUpdate = frequencyRepository.findAll().size();
        frequency.setId(count.incrementAndGet());

        // Create the Frequency
        FrequencyDTO frequencyDTO = frequencyMapper.toDto(frequency);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFrequencyMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(frequencyDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Frequency in the database
        List<Frequency> frequencyList = frequencyRepository.findAll();
        assertThat(frequencyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamFrequency() throws Exception {
        int databaseSizeBeforeUpdate = frequencyRepository.findAll().size();
        frequency.setId(count.incrementAndGet());

        // Create the Frequency
        FrequencyDTO frequencyDTO = frequencyMapper.toDto(frequency);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFrequencyMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(frequencyDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Frequency in the database
        List<Frequency> frequencyList = frequencyRepository.findAll();
        assertThat(frequencyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateFrequencyWithPatch() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        int databaseSizeBeforeUpdate = frequencyRepository.findAll().size();

        // Update the frequency using partial update
        Frequency partialUpdatedFrequency = new Frequency();
        partialUpdatedFrequency.setId(frequency.getId());

        partialUpdatedFrequency
            .name(UPDATED_NAME)
            .executionWeekDay(UPDATED_EXECUTION_WEEK_DAY)
            .daysAfter(UPDATED_DAYS_AFTER)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .numberOfCycle(UPDATED_NUMBER_OF_CYCLE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2);

        restFrequencyMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFrequency.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFrequency))
            )
            .andExpect(status().isOk());

        // Validate the Frequency in the database
        List<Frequency> frequencyList = frequencyRepository.findAll();
        assertThat(frequencyList).hasSize(databaseSizeBeforeUpdate);
        Frequency testFrequency = frequencyList.get(frequencyList.size() - 1);
        assertThat(testFrequency.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testFrequency.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testFrequency.getOperationType()).isEqualTo(DEFAULT_OPERATION_TYPE);
        assertThat(testFrequency.getExecutionTime()).isEqualTo(DEFAULT_EXECUTION_TIME);
        assertThat(testFrequency.getExecutionWeekDay()).isEqualTo(UPDATED_EXECUTION_WEEK_DAY);
        assertThat(testFrequency.getExecutionMonthDay()).isEqualTo(DEFAULT_EXECUTION_MONTH_DAY);
        assertThat(testFrequency.getDaysAfter()).isEqualTo(UPDATED_DAYS_AFTER);
        assertThat(testFrequency.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testFrequency.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testFrequency.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testFrequency.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testFrequency.getCycle()).isEqualTo(DEFAULT_CYCLE);
        assertThat(testFrequency.getPeriodOfOccurrence()).isEqualTo(DEFAULT_PERIOD_OF_OCCURRENCE);
        assertThat(testFrequency.getNumberOfCycle()).isEqualTo(UPDATED_NUMBER_OF_CYCLE);
        assertThat(testFrequency.getOccurrenceByPeriod()).isEqualTo(DEFAULT_OCCURRENCE_BY_PERIOD);
        assertThat(testFrequency.getDatesOfOccurrence()).isEqualTo(DEFAULT_DATES_OF_OCCURRENCE);
        assertThat(testFrequency.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testFrequency.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testFrequency.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
    }

    @Test
    @Transactional
    void fullUpdateFrequencyWithPatch() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        int databaseSizeBeforeUpdate = frequencyRepository.findAll().size();

        // Update the frequency using partial update
        Frequency partialUpdatedFrequency = new Frequency();
        partialUpdatedFrequency.setId(frequency.getId());

        partialUpdatedFrequency
            .name(UPDATED_NAME)
            .type(UPDATED_TYPE)
            .operationType(UPDATED_OPERATION_TYPE)
            .executionTime(UPDATED_EXECUTION_TIME)
            .executionWeekDay(UPDATED_EXECUTION_WEEK_DAY)
            .executionMonthDay(UPDATED_EXECUTION_MONTH_DAY)
            .daysAfter(UPDATED_DAYS_AFTER)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .cycle(UPDATED_CYCLE)
            .periodOfOccurrence(UPDATED_PERIOD_OF_OCCURRENCE)
            .numberOfCycle(UPDATED_NUMBER_OF_CYCLE)
            .occurrenceByPeriod(UPDATED_OCCURRENCE_BY_PERIOD)
            .datesOfOccurrence(UPDATED_DATES_OF_OCCURRENCE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);

        restFrequencyMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFrequency.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFrequency))
            )
            .andExpect(status().isOk());

        // Validate the Frequency in the database
        List<Frequency> frequencyList = frequencyRepository.findAll();
        assertThat(frequencyList).hasSize(databaseSizeBeforeUpdate);
        Frequency testFrequency = frequencyList.get(frequencyList.size() - 1);
        assertThat(testFrequency.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testFrequency.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testFrequency.getOperationType()).isEqualTo(UPDATED_OPERATION_TYPE);
        assertThat(testFrequency.getExecutionTime()).isEqualTo(UPDATED_EXECUTION_TIME);
        assertThat(testFrequency.getExecutionWeekDay()).isEqualTo(UPDATED_EXECUTION_WEEK_DAY);
        assertThat(testFrequency.getExecutionMonthDay()).isEqualTo(UPDATED_EXECUTION_MONTH_DAY);
        assertThat(testFrequency.getDaysAfter()).isEqualTo(UPDATED_DAYS_AFTER);
        assertThat(testFrequency.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testFrequency.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testFrequency.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testFrequency.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testFrequency.getCycle()).isEqualTo(UPDATED_CYCLE);
        assertThat(testFrequency.getPeriodOfOccurrence()).isEqualTo(UPDATED_PERIOD_OF_OCCURRENCE);
        assertThat(testFrequency.getNumberOfCycle()).isEqualTo(UPDATED_NUMBER_OF_CYCLE);
        assertThat(testFrequency.getOccurrenceByPeriod()).isEqualTo(UPDATED_OCCURRENCE_BY_PERIOD);
        assertThat(testFrequency.getDatesOfOccurrence()).isEqualTo(UPDATED_DATES_OF_OCCURRENCE);
        assertThat(testFrequency.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testFrequency.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testFrequency.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void patchNonExistingFrequency() throws Exception {
        int databaseSizeBeforeUpdate = frequencyRepository.findAll().size();
        frequency.setId(count.incrementAndGet());

        // Create the Frequency
        FrequencyDTO frequencyDTO = frequencyMapper.toDto(frequency);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFrequencyMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, frequencyDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(frequencyDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Frequency in the database
        List<Frequency> frequencyList = frequencyRepository.findAll();
        assertThat(frequencyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchFrequency() throws Exception {
        int databaseSizeBeforeUpdate = frequencyRepository.findAll().size();
        frequency.setId(count.incrementAndGet());

        // Create the Frequency
        FrequencyDTO frequencyDTO = frequencyMapper.toDto(frequency);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFrequencyMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(frequencyDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Frequency in the database
        List<Frequency> frequencyList = frequencyRepository.findAll();
        assertThat(frequencyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamFrequency() throws Exception {
        int databaseSizeBeforeUpdate = frequencyRepository.findAll().size();
        frequency.setId(count.incrementAndGet());

        // Create the Frequency
        FrequencyDTO frequencyDTO = frequencyMapper.toDto(frequency);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFrequencyMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(frequencyDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Frequency in the database
        List<Frequency> frequencyList = frequencyRepository.findAll();
        assertThat(frequencyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteFrequency() throws Exception {
        // Initialize the database
        frequencyRepository.saveAndFlush(frequency);

        int databaseSizeBeforeDelete = frequencyRepository.findAll().size();

        // Delete the frequency
        restFrequencyMockMvc
            .perform(delete(ENTITY_API_URL_ID, frequency.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Frequency> frequencyList = frequencyRepository.findAll();
        assertThat(frequencyList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
