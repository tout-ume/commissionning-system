package sn.free.commissioning.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.IntegrationTest;
import sn.free.commissioning.domain.PartnerProfile;
import sn.free.commissioning.repository.PartnerProfileRepository;
import sn.free.commissioning.service.criteria.PartnerProfileCriteria;
import sn.free.commissioning.service.dto.PartnerProfileDTO;
import sn.free.commissioning.service.mapper.PartnerProfileMapper;

/**
 * Integration tests for the {@link PartnerProfileResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PartnerProfileResourceIT {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_LEVEL = 1;
    private static final Integer UPDATED_LEVEL = 2;
    private static final Integer SMALLER_LEVEL = 1 - 1;

    private static final Long DEFAULT_PARENT_PARTNER_PROFILE_ID = 1L;
    private static final Long UPDATED_PARENT_PARTNER_PROFILE_ID = 2L;
    private static final Long SMALLER_PARENT_PARTNER_PROFILE_ID = 1L - 1L;

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_CAN_HAVE_MULTIPLE_ZONES = false;
    private static final Boolean UPDATED_CAN_HAVE_MULTIPLE_ZONES = true;

    private static final Integer DEFAULT_ZONE_THRESHOLD = 1;
    private static final Integer UPDATED_ZONE_THRESHOLD = 2;
    private static final Integer SMALLER_ZONE_THRESHOLD = 1 - 1;

    private static final Integer DEFAULT_SIM_BACKUP_THRESHOLD = 1;
    private static final Integer UPDATED_SIM_BACKUP_THRESHOLD = 2;
    private static final Integer SMALLER_SIM_BACKUP_THRESHOLD = 1 - 1;

    private static final Boolean DEFAULT_CAN_DO_PROFILE_ASSIGNMENT = false;
    private static final Boolean UPDATED_CAN_DO_PROFILE_ASSIGNMENT = true;

    private static final String ENTITY_API_URL = "/api/partner-profiles";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PartnerProfileRepository partnerProfileRepository;

    @Autowired
    private PartnerProfileMapper partnerProfileMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPartnerProfileMockMvc;

    private PartnerProfile partnerProfile;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PartnerProfile createEntity(EntityManager em) {
        PartnerProfile partnerProfile = new PartnerProfile()
            .code(DEFAULT_CODE)
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .level(DEFAULT_LEVEL)
            .parentPartnerProfileId(DEFAULT_PARENT_PARTNER_PROFILE_ID)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .canHaveMultipleZones(DEFAULT_CAN_HAVE_MULTIPLE_ZONES)
            .zoneThreshold(DEFAULT_ZONE_THRESHOLD)
            .simBackupThreshold(DEFAULT_SIM_BACKUP_THRESHOLD)
            .canDoProfileAssignment(DEFAULT_CAN_DO_PROFILE_ASSIGNMENT);
        return partnerProfile;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PartnerProfile createUpdatedEntity(EntityManager em) {
        PartnerProfile partnerProfile = new PartnerProfile()
            .code(UPDATED_CODE)
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .level(UPDATED_LEVEL)
            .parentPartnerProfileId(UPDATED_PARENT_PARTNER_PROFILE_ID)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .canHaveMultipleZones(UPDATED_CAN_HAVE_MULTIPLE_ZONES)
            .zoneThreshold(UPDATED_ZONE_THRESHOLD)
            .simBackupThreshold(UPDATED_SIM_BACKUP_THRESHOLD)
            .canDoProfileAssignment(UPDATED_CAN_DO_PROFILE_ASSIGNMENT);
        return partnerProfile;
    }

    @BeforeEach
    public void initTest() {
        partnerProfile = createEntity(em);
    }

    @Test
    @Transactional
    void createPartnerProfile() throws Exception {
        int databaseSizeBeforeCreate = partnerProfileRepository.findAll().size();
        // Create the PartnerProfile
        PartnerProfileDTO partnerProfileDTO = partnerProfileMapper.toDto(partnerProfile);
        restPartnerProfileMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(partnerProfileDTO))
            )
            .andExpect(status().isCreated());

        // Validate the PartnerProfile in the database
        List<PartnerProfile> partnerProfileList = partnerProfileRepository.findAll();
        assertThat(partnerProfileList).hasSize(databaseSizeBeforeCreate + 1);
        PartnerProfile testPartnerProfile = partnerProfileList.get(partnerProfileList.size() - 1);
        assertThat(testPartnerProfile.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testPartnerProfile.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPartnerProfile.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testPartnerProfile.getLevel()).isEqualTo(DEFAULT_LEVEL);
        assertThat(testPartnerProfile.getParentPartnerProfileId()).isEqualTo(DEFAULT_PARENT_PARTNER_PROFILE_ID);
        assertThat(testPartnerProfile.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testPartnerProfile.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testPartnerProfile.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testPartnerProfile.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testPartnerProfile.getCanHaveMultipleZones()).isEqualTo(DEFAULT_CAN_HAVE_MULTIPLE_ZONES);
        assertThat(testPartnerProfile.getZoneThreshold()).isEqualTo(DEFAULT_ZONE_THRESHOLD);
        assertThat(testPartnerProfile.getSimBackupThreshold()).isEqualTo(DEFAULT_SIM_BACKUP_THRESHOLD);
        assertThat(testPartnerProfile.getCanDoProfileAssignment()).isEqualTo(DEFAULT_CAN_DO_PROFILE_ASSIGNMENT);
    }

    @Test
    @Transactional
    void createPartnerProfileWithExistingId() throws Exception {
        // Create the PartnerProfile with an existing ID
        partnerProfile.setId(1L);
        PartnerProfileDTO partnerProfileDTO = partnerProfileMapper.toDto(partnerProfile);

        int databaseSizeBeforeCreate = partnerProfileRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPartnerProfileMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(partnerProfileDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PartnerProfile in the database
        List<PartnerProfile> partnerProfileList = partnerProfileRepository.findAll();
        assertThat(partnerProfileList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = partnerProfileRepository.findAll().size();
        // set the field null
        partnerProfile.setCode(null);

        // Create the PartnerProfile, which fails.
        PartnerProfileDTO partnerProfileDTO = partnerProfileMapper.toDto(partnerProfile);

        restPartnerProfileMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(partnerProfileDTO))
            )
            .andExpect(status().isBadRequest());

        List<PartnerProfile> partnerProfileList = partnerProfileRepository.findAll();
        assertThat(partnerProfileList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = partnerProfileRepository.findAll().size();
        // set the field null
        partnerProfile.setName(null);

        // Create the PartnerProfile, which fails.
        PartnerProfileDTO partnerProfileDTO = partnerProfileMapper.toDto(partnerProfile);

        restPartnerProfileMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(partnerProfileDTO))
            )
            .andExpect(status().isBadRequest());

        List<PartnerProfile> partnerProfileList = partnerProfileRepository.findAll();
        assertThat(partnerProfileList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCreatedByIsRequired() throws Exception {
        int databaseSizeBeforeTest = partnerProfileRepository.findAll().size();
        // set the field null
        partnerProfile.setCreatedBy(null);

        // Create the PartnerProfile, which fails.
        PartnerProfileDTO partnerProfileDTO = partnerProfileMapper.toDto(partnerProfile);

        restPartnerProfileMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(partnerProfileDTO))
            )
            .andExpect(status().isBadRequest());

        List<PartnerProfile> partnerProfileList = partnerProfileRepository.findAll();
        assertThat(partnerProfileList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPartnerProfiles() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList
        restPartnerProfileMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(partnerProfile.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL)))
            .andExpect(jsonPath("$.[*].parentPartnerProfileId").value(hasItem(DEFAULT_PARENT_PARTNER_PROFILE_ID.intValue())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].canHaveMultipleZones").value(hasItem(DEFAULT_CAN_HAVE_MULTIPLE_ZONES.booleanValue())))
            .andExpect(jsonPath("$.[*].zoneThreshold").value(hasItem(DEFAULT_ZONE_THRESHOLD)))
            .andExpect(jsonPath("$.[*].simBackupThreshold").value(hasItem(DEFAULT_SIM_BACKUP_THRESHOLD)))
            .andExpect(jsonPath("$.[*].canDoProfileAssignment").value(hasItem(DEFAULT_CAN_DO_PROFILE_ASSIGNMENT.booleanValue())));
    }

    @Test
    @Transactional
    void getPartnerProfile() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get the partnerProfile
        restPartnerProfileMockMvc
            .perform(get(ENTITY_API_URL_ID, partnerProfile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(partnerProfile.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.level").value(DEFAULT_LEVEL))
            .andExpect(jsonPath("$.parentPartnerProfileId").value(DEFAULT_PARENT_PARTNER_PROFILE_ID.intValue()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.canHaveMultipleZones").value(DEFAULT_CAN_HAVE_MULTIPLE_ZONES.booleanValue()))
            .andExpect(jsonPath("$.zoneThreshold").value(DEFAULT_ZONE_THRESHOLD))
            .andExpect(jsonPath("$.simBackupThreshold").value(DEFAULT_SIM_BACKUP_THRESHOLD))
            .andExpect(jsonPath("$.canDoProfileAssignment").value(DEFAULT_CAN_DO_PROFILE_ASSIGNMENT.booleanValue()));
    }

    @Test
    @Transactional
    void getPartnerProfilesByIdFiltering() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        Long id = partnerProfile.getId();

        defaultPartnerProfileShouldBeFound("id.equals=" + id);
        defaultPartnerProfileShouldNotBeFound("id.notEquals=" + id);

        defaultPartnerProfileShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPartnerProfileShouldNotBeFound("id.greaterThan=" + id);

        defaultPartnerProfileShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPartnerProfileShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where code equals to DEFAULT_CODE
        defaultPartnerProfileShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the partnerProfileList where code equals to UPDATED_CODE
        defaultPartnerProfileShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where code not equals to DEFAULT_CODE
        defaultPartnerProfileShouldNotBeFound("code.notEquals=" + DEFAULT_CODE);

        // Get all the partnerProfileList where code not equals to UPDATED_CODE
        defaultPartnerProfileShouldBeFound("code.notEquals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where code in DEFAULT_CODE or UPDATED_CODE
        defaultPartnerProfileShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the partnerProfileList where code equals to UPDATED_CODE
        defaultPartnerProfileShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where code is not null
        defaultPartnerProfileShouldBeFound("code.specified=true");

        // Get all the partnerProfileList where code is null
        defaultPartnerProfileShouldNotBeFound("code.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCodeContainsSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where code contains DEFAULT_CODE
        defaultPartnerProfileShouldBeFound("code.contains=" + DEFAULT_CODE);

        // Get all the partnerProfileList where code contains UPDATED_CODE
        defaultPartnerProfileShouldNotBeFound("code.contains=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCodeNotContainsSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where code does not contain DEFAULT_CODE
        defaultPartnerProfileShouldNotBeFound("code.doesNotContain=" + DEFAULT_CODE);

        // Get all the partnerProfileList where code does not contain UPDATED_CODE
        defaultPartnerProfileShouldBeFound("code.doesNotContain=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where name equals to DEFAULT_NAME
        defaultPartnerProfileShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the partnerProfileList where name equals to UPDATED_NAME
        defaultPartnerProfileShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where name not equals to DEFAULT_NAME
        defaultPartnerProfileShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the partnerProfileList where name not equals to UPDATED_NAME
        defaultPartnerProfileShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where name in DEFAULT_NAME or UPDATED_NAME
        defaultPartnerProfileShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the partnerProfileList where name equals to UPDATED_NAME
        defaultPartnerProfileShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where name is not null
        defaultPartnerProfileShouldBeFound("name.specified=true");

        // Get all the partnerProfileList where name is null
        defaultPartnerProfileShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByNameContainsSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where name contains DEFAULT_NAME
        defaultPartnerProfileShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the partnerProfileList where name contains UPDATED_NAME
        defaultPartnerProfileShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByNameNotContainsSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where name does not contain DEFAULT_NAME
        defaultPartnerProfileShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the partnerProfileList where name does not contain UPDATED_NAME
        defaultPartnerProfileShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where description equals to DEFAULT_DESCRIPTION
        defaultPartnerProfileShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the partnerProfileList where description equals to UPDATED_DESCRIPTION
        defaultPartnerProfileShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where description not equals to DEFAULT_DESCRIPTION
        defaultPartnerProfileShouldNotBeFound("description.notEquals=" + DEFAULT_DESCRIPTION);

        // Get all the partnerProfileList where description not equals to UPDATED_DESCRIPTION
        defaultPartnerProfileShouldBeFound("description.notEquals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultPartnerProfileShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the partnerProfileList where description equals to UPDATED_DESCRIPTION
        defaultPartnerProfileShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where description is not null
        defaultPartnerProfileShouldBeFound("description.specified=true");

        // Get all the partnerProfileList where description is null
        defaultPartnerProfileShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where description contains DEFAULT_DESCRIPTION
        defaultPartnerProfileShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the partnerProfileList where description contains UPDATED_DESCRIPTION
        defaultPartnerProfileShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where description does not contain DEFAULT_DESCRIPTION
        defaultPartnerProfileShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the partnerProfileList where description does not contain UPDATED_DESCRIPTION
        defaultPartnerProfileShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByLevelIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where level equals to DEFAULT_LEVEL
        defaultPartnerProfileShouldBeFound("level.equals=" + DEFAULT_LEVEL);

        // Get all the partnerProfileList where level equals to UPDATED_LEVEL
        defaultPartnerProfileShouldNotBeFound("level.equals=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByLevelIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where level not equals to DEFAULT_LEVEL
        defaultPartnerProfileShouldNotBeFound("level.notEquals=" + DEFAULT_LEVEL);

        // Get all the partnerProfileList where level not equals to UPDATED_LEVEL
        defaultPartnerProfileShouldBeFound("level.notEquals=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByLevelIsInShouldWork() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where level in DEFAULT_LEVEL or UPDATED_LEVEL
        defaultPartnerProfileShouldBeFound("level.in=" + DEFAULT_LEVEL + "," + UPDATED_LEVEL);

        // Get all the partnerProfileList where level equals to UPDATED_LEVEL
        defaultPartnerProfileShouldNotBeFound("level.in=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByLevelIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where level is not null
        defaultPartnerProfileShouldBeFound("level.specified=true");

        // Get all the partnerProfileList where level is null
        defaultPartnerProfileShouldNotBeFound("level.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByLevelIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where level is greater than or equal to DEFAULT_LEVEL
        defaultPartnerProfileShouldBeFound("level.greaterThanOrEqual=" + DEFAULT_LEVEL);

        // Get all the partnerProfileList where level is greater than or equal to UPDATED_LEVEL
        defaultPartnerProfileShouldNotBeFound("level.greaterThanOrEqual=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByLevelIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where level is less than or equal to DEFAULT_LEVEL
        defaultPartnerProfileShouldBeFound("level.lessThanOrEqual=" + DEFAULT_LEVEL);

        // Get all the partnerProfileList where level is less than or equal to SMALLER_LEVEL
        defaultPartnerProfileShouldNotBeFound("level.lessThanOrEqual=" + SMALLER_LEVEL);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByLevelIsLessThanSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where level is less than DEFAULT_LEVEL
        defaultPartnerProfileShouldNotBeFound("level.lessThan=" + DEFAULT_LEVEL);

        // Get all the partnerProfileList where level is less than UPDATED_LEVEL
        defaultPartnerProfileShouldBeFound("level.lessThan=" + UPDATED_LEVEL);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByLevelIsGreaterThanSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where level is greater than DEFAULT_LEVEL
        defaultPartnerProfileShouldNotBeFound("level.greaterThan=" + DEFAULT_LEVEL);

        // Get all the partnerProfileList where level is greater than SMALLER_LEVEL
        defaultPartnerProfileShouldBeFound("level.greaterThan=" + SMALLER_LEVEL);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByParentPartnerProfileIdIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where parentPartnerProfileId equals to DEFAULT_PARENT_PARTNER_PROFILE_ID
        defaultPartnerProfileShouldBeFound("parentPartnerProfileId.equals=" + DEFAULT_PARENT_PARTNER_PROFILE_ID);

        // Get all the partnerProfileList where parentPartnerProfileId equals to UPDATED_PARENT_PARTNER_PROFILE_ID
        defaultPartnerProfileShouldNotBeFound("parentPartnerProfileId.equals=" + UPDATED_PARENT_PARTNER_PROFILE_ID);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByParentPartnerProfileIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where parentPartnerProfileId not equals to DEFAULT_PARENT_PARTNER_PROFILE_ID
        defaultPartnerProfileShouldNotBeFound("parentPartnerProfileId.notEquals=" + DEFAULT_PARENT_PARTNER_PROFILE_ID);

        // Get all the partnerProfileList where parentPartnerProfileId not equals to UPDATED_PARENT_PARTNER_PROFILE_ID
        defaultPartnerProfileShouldBeFound("parentPartnerProfileId.notEquals=" + UPDATED_PARENT_PARTNER_PROFILE_ID);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByParentPartnerProfileIdIsInShouldWork() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where parentPartnerProfileId in DEFAULT_PARENT_PARTNER_PROFILE_ID or UPDATED_PARENT_PARTNER_PROFILE_ID
        defaultPartnerProfileShouldBeFound(
            "parentPartnerProfileId.in=" + DEFAULT_PARENT_PARTNER_PROFILE_ID + "," + UPDATED_PARENT_PARTNER_PROFILE_ID
        );

        // Get all the partnerProfileList where parentPartnerProfileId equals to UPDATED_PARENT_PARTNER_PROFILE_ID
        defaultPartnerProfileShouldNotBeFound("parentPartnerProfileId.in=" + UPDATED_PARENT_PARTNER_PROFILE_ID);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByParentPartnerProfileIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where parentPartnerProfileId is not null
        defaultPartnerProfileShouldBeFound("parentPartnerProfileId.specified=true");

        // Get all the partnerProfileList where parentPartnerProfileId is null
        defaultPartnerProfileShouldNotBeFound("parentPartnerProfileId.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByParentPartnerProfileIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where parentPartnerProfileId is greater than or equal to DEFAULT_PARENT_PARTNER_PROFILE_ID
        defaultPartnerProfileShouldBeFound("parentPartnerProfileId.greaterThanOrEqual=" + DEFAULT_PARENT_PARTNER_PROFILE_ID);

        // Get all the partnerProfileList where parentPartnerProfileId is greater than or equal to UPDATED_PARENT_PARTNER_PROFILE_ID
        defaultPartnerProfileShouldNotBeFound("parentPartnerProfileId.greaterThanOrEqual=" + UPDATED_PARENT_PARTNER_PROFILE_ID);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByParentPartnerProfileIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where parentPartnerProfileId is less than or equal to DEFAULT_PARENT_PARTNER_PROFILE_ID
        defaultPartnerProfileShouldBeFound("parentPartnerProfileId.lessThanOrEqual=" + DEFAULT_PARENT_PARTNER_PROFILE_ID);

        // Get all the partnerProfileList where parentPartnerProfileId is less than or equal to SMALLER_PARENT_PARTNER_PROFILE_ID
        defaultPartnerProfileShouldNotBeFound("parentPartnerProfileId.lessThanOrEqual=" + SMALLER_PARENT_PARTNER_PROFILE_ID);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByParentPartnerProfileIdIsLessThanSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where parentPartnerProfileId is less than DEFAULT_PARENT_PARTNER_PROFILE_ID
        defaultPartnerProfileShouldNotBeFound("parentPartnerProfileId.lessThan=" + DEFAULT_PARENT_PARTNER_PROFILE_ID);

        // Get all the partnerProfileList where parentPartnerProfileId is less than UPDATED_PARENT_PARTNER_PROFILE_ID
        defaultPartnerProfileShouldBeFound("parentPartnerProfileId.lessThan=" + UPDATED_PARENT_PARTNER_PROFILE_ID);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByParentPartnerProfileIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where parentPartnerProfileId is greater than DEFAULT_PARENT_PARTNER_PROFILE_ID
        defaultPartnerProfileShouldNotBeFound("parentPartnerProfileId.greaterThan=" + DEFAULT_PARENT_PARTNER_PROFILE_ID);

        // Get all the partnerProfileList where parentPartnerProfileId is greater than SMALLER_PARENT_PARTNER_PROFILE_ID
        defaultPartnerProfileShouldBeFound("parentPartnerProfileId.greaterThan=" + SMALLER_PARENT_PARTNER_PROFILE_ID);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where createdBy equals to DEFAULT_CREATED_BY
        defaultPartnerProfileShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the partnerProfileList where createdBy equals to UPDATED_CREATED_BY
        defaultPartnerProfileShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where createdBy not equals to DEFAULT_CREATED_BY
        defaultPartnerProfileShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the partnerProfileList where createdBy not equals to UPDATED_CREATED_BY
        defaultPartnerProfileShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultPartnerProfileShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the partnerProfileList where createdBy equals to UPDATED_CREATED_BY
        defaultPartnerProfileShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where createdBy is not null
        defaultPartnerProfileShouldBeFound("createdBy.specified=true");

        // Get all the partnerProfileList where createdBy is null
        defaultPartnerProfileShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where createdBy contains DEFAULT_CREATED_BY
        defaultPartnerProfileShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the partnerProfileList where createdBy contains UPDATED_CREATED_BY
        defaultPartnerProfileShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where createdBy does not contain DEFAULT_CREATED_BY
        defaultPartnerProfileShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the partnerProfileList where createdBy does not contain UPDATED_CREATED_BY
        defaultPartnerProfileShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where createdDate equals to DEFAULT_CREATED_DATE
        defaultPartnerProfileShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the partnerProfileList where createdDate equals to UPDATED_CREATED_DATE
        defaultPartnerProfileShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultPartnerProfileShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the partnerProfileList where createdDate not equals to UPDATED_CREATED_DATE
        defaultPartnerProfileShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultPartnerProfileShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the partnerProfileList where createdDate equals to UPDATED_CREATED_DATE
        defaultPartnerProfileShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where createdDate is not null
        defaultPartnerProfileShouldBeFound("createdDate.specified=true");

        // Get all the partnerProfileList where createdDate is null
        defaultPartnerProfileShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultPartnerProfileShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the partnerProfileList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultPartnerProfileShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultPartnerProfileShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the partnerProfileList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultPartnerProfileShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultPartnerProfileShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the partnerProfileList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultPartnerProfileShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where lastModifiedBy is not null
        defaultPartnerProfileShouldBeFound("lastModifiedBy.specified=true");

        // Get all the partnerProfileList where lastModifiedBy is null
        defaultPartnerProfileShouldNotBeFound("lastModifiedBy.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultPartnerProfileShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the partnerProfileList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultPartnerProfileShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultPartnerProfileShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the partnerProfileList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultPartnerProfileShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultPartnerProfileShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the partnerProfileList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultPartnerProfileShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultPartnerProfileShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the partnerProfileList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultPartnerProfileShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultPartnerProfileShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the partnerProfileList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultPartnerProfileShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where lastModifiedDate is not null
        defaultPartnerProfileShouldBeFound("lastModifiedDate.specified=true");

        // Get all the partnerProfileList where lastModifiedDate is null
        defaultPartnerProfileShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCanHaveMultipleZonesIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where canHaveMultipleZones equals to DEFAULT_CAN_HAVE_MULTIPLE_ZONES
        defaultPartnerProfileShouldBeFound("canHaveMultipleZones.equals=" + DEFAULT_CAN_HAVE_MULTIPLE_ZONES);

        // Get all the partnerProfileList where canHaveMultipleZones equals to UPDATED_CAN_HAVE_MULTIPLE_ZONES
        defaultPartnerProfileShouldNotBeFound("canHaveMultipleZones.equals=" + UPDATED_CAN_HAVE_MULTIPLE_ZONES);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCanHaveMultipleZonesIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where canHaveMultipleZones not equals to DEFAULT_CAN_HAVE_MULTIPLE_ZONES
        defaultPartnerProfileShouldNotBeFound("canHaveMultipleZones.notEquals=" + DEFAULT_CAN_HAVE_MULTIPLE_ZONES);

        // Get all the partnerProfileList where canHaveMultipleZones not equals to UPDATED_CAN_HAVE_MULTIPLE_ZONES
        defaultPartnerProfileShouldBeFound("canHaveMultipleZones.notEquals=" + UPDATED_CAN_HAVE_MULTIPLE_ZONES);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCanHaveMultipleZonesIsInShouldWork() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where canHaveMultipleZones in DEFAULT_CAN_HAVE_MULTIPLE_ZONES or UPDATED_CAN_HAVE_MULTIPLE_ZONES
        defaultPartnerProfileShouldBeFound(
            "canHaveMultipleZones.in=" + DEFAULT_CAN_HAVE_MULTIPLE_ZONES + "," + UPDATED_CAN_HAVE_MULTIPLE_ZONES
        );

        // Get all the partnerProfileList where canHaveMultipleZones equals to UPDATED_CAN_HAVE_MULTIPLE_ZONES
        defaultPartnerProfileShouldNotBeFound("canHaveMultipleZones.in=" + UPDATED_CAN_HAVE_MULTIPLE_ZONES);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCanHaveMultipleZonesIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where canHaveMultipleZones is not null
        defaultPartnerProfileShouldBeFound("canHaveMultipleZones.specified=true");

        // Get all the partnerProfileList where canHaveMultipleZones is null
        defaultPartnerProfileShouldNotBeFound("canHaveMultipleZones.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByZoneThresholdIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where zoneThreshold equals to DEFAULT_ZONE_THRESHOLD
        defaultPartnerProfileShouldBeFound("zoneThreshold.equals=" + DEFAULT_ZONE_THRESHOLD);

        // Get all the partnerProfileList where zoneThreshold equals to UPDATED_ZONE_THRESHOLD
        defaultPartnerProfileShouldNotBeFound("zoneThreshold.equals=" + UPDATED_ZONE_THRESHOLD);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByZoneThresholdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where zoneThreshold not equals to DEFAULT_ZONE_THRESHOLD
        defaultPartnerProfileShouldNotBeFound("zoneThreshold.notEquals=" + DEFAULT_ZONE_THRESHOLD);

        // Get all the partnerProfileList where zoneThreshold not equals to UPDATED_ZONE_THRESHOLD
        defaultPartnerProfileShouldBeFound("zoneThreshold.notEquals=" + UPDATED_ZONE_THRESHOLD);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByZoneThresholdIsInShouldWork() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where zoneThreshold in DEFAULT_ZONE_THRESHOLD or UPDATED_ZONE_THRESHOLD
        defaultPartnerProfileShouldBeFound("zoneThreshold.in=" + DEFAULT_ZONE_THRESHOLD + "," + UPDATED_ZONE_THRESHOLD);

        // Get all the partnerProfileList where zoneThreshold equals to UPDATED_ZONE_THRESHOLD
        defaultPartnerProfileShouldNotBeFound("zoneThreshold.in=" + UPDATED_ZONE_THRESHOLD);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByZoneThresholdIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where zoneThreshold is not null
        defaultPartnerProfileShouldBeFound("zoneThreshold.specified=true");

        // Get all the partnerProfileList where zoneThreshold is null
        defaultPartnerProfileShouldNotBeFound("zoneThreshold.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByZoneThresholdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where zoneThreshold is greater than or equal to DEFAULT_ZONE_THRESHOLD
        defaultPartnerProfileShouldBeFound("zoneThreshold.greaterThanOrEqual=" + DEFAULT_ZONE_THRESHOLD);

        // Get all the partnerProfileList where zoneThreshold is greater than or equal to UPDATED_ZONE_THRESHOLD
        defaultPartnerProfileShouldNotBeFound("zoneThreshold.greaterThanOrEqual=" + UPDATED_ZONE_THRESHOLD);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByZoneThresholdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where zoneThreshold is less than or equal to DEFAULT_ZONE_THRESHOLD
        defaultPartnerProfileShouldBeFound("zoneThreshold.lessThanOrEqual=" + DEFAULT_ZONE_THRESHOLD);

        // Get all the partnerProfileList where zoneThreshold is less than or equal to SMALLER_ZONE_THRESHOLD
        defaultPartnerProfileShouldNotBeFound("zoneThreshold.lessThanOrEqual=" + SMALLER_ZONE_THRESHOLD);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByZoneThresholdIsLessThanSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where zoneThreshold is less than DEFAULT_ZONE_THRESHOLD
        defaultPartnerProfileShouldNotBeFound("zoneThreshold.lessThan=" + DEFAULT_ZONE_THRESHOLD);

        // Get all the partnerProfileList where zoneThreshold is less than UPDATED_ZONE_THRESHOLD
        defaultPartnerProfileShouldBeFound("zoneThreshold.lessThan=" + UPDATED_ZONE_THRESHOLD);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByZoneThresholdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where zoneThreshold is greater than DEFAULT_ZONE_THRESHOLD
        defaultPartnerProfileShouldNotBeFound("zoneThreshold.greaterThan=" + DEFAULT_ZONE_THRESHOLD);

        // Get all the partnerProfileList where zoneThreshold is greater than SMALLER_ZONE_THRESHOLD
        defaultPartnerProfileShouldBeFound("zoneThreshold.greaterThan=" + SMALLER_ZONE_THRESHOLD);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesBySimBackupThresholdIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where simBackupThreshold equals to DEFAULT_SIM_BACKUP_THRESHOLD
        defaultPartnerProfileShouldBeFound("simBackupThreshold.equals=" + DEFAULT_SIM_BACKUP_THRESHOLD);

        // Get all the partnerProfileList where simBackupThreshold equals to UPDATED_SIM_BACKUP_THRESHOLD
        defaultPartnerProfileShouldNotBeFound("simBackupThreshold.equals=" + UPDATED_SIM_BACKUP_THRESHOLD);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesBySimBackupThresholdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where simBackupThreshold not equals to DEFAULT_SIM_BACKUP_THRESHOLD
        defaultPartnerProfileShouldNotBeFound("simBackupThreshold.notEquals=" + DEFAULT_SIM_BACKUP_THRESHOLD);

        // Get all the partnerProfileList where simBackupThreshold not equals to UPDATED_SIM_BACKUP_THRESHOLD
        defaultPartnerProfileShouldBeFound("simBackupThreshold.notEquals=" + UPDATED_SIM_BACKUP_THRESHOLD);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesBySimBackupThresholdIsInShouldWork() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where simBackupThreshold in DEFAULT_SIM_BACKUP_THRESHOLD or UPDATED_SIM_BACKUP_THRESHOLD
        defaultPartnerProfileShouldBeFound("simBackupThreshold.in=" + DEFAULT_SIM_BACKUP_THRESHOLD + "," + UPDATED_SIM_BACKUP_THRESHOLD);

        // Get all the partnerProfileList where simBackupThreshold equals to UPDATED_SIM_BACKUP_THRESHOLD
        defaultPartnerProfileShouldNotBeFound("simBackupThreshold.in=" + UPDATED_SIM_BACKUP_THRESHOLD);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesBySimBackupThresholdIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where simBackupThreshold is not null
        defaultPartnerProfileShouldBeFound("simBackupThreshold.specified=true");

        // Get all the partnerProfileList where simBackupThreshold is null
        defaultPartnerProfileShouldNotBeFound("simBackupThreshold.specified=false");
    }

    @Test
    @Transactional
    void getAllPartnerProfilesBySimBackupThresholdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where simBackupThreshold is greater than or equal to DEFAULT_SIM_BACKUP_THRESHOLD
        defaultPartnerProfileShouldBeFound("simBackupThreshold.greaterThanOrEqual=" + DEFAULT_SIM_BACKUP_THRESHOLD);

        // Get all the partnerProfileList where simBackupThreshold is greater than or equal to UPDATED_SIM_BACKUP_THRESHOLD
        defaultPartnerProfileShouldNotBeFound("simBackupThreshold.greaterThanOrEqual=" + UPDATED_SIM_BACKUP_THRESHOLD);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesBySimBackupThresholdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where simBackupThreshold is less than or equal to DEFAULT_SIM_BACKUP_THRESHOLD
        defaultPartnerProfileShouldBeFound("simBackupThreshold.lessThanOrEqual=" + DEFAULT_SIM_BACKUP_THRESHOLD);

        // Get all the partnerProfileList where simBackupThreshold is less than or equal to SMALLER_SIM_BACKUP_THRESHOLD
        defaultPartnerProfileShouldNotBeFound("simBackupThreshold.lessThanOrEqual=" + SMALLER_SIM_BACKUP_THRESHOLD);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesBySimBackupThresholdIsLessThanSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where simBackupThreshold is less than DEFAULT_SIM_BACKUP_THRESHOLD
        defaultPartnerProfileShouldNotBeFound("simBackupThreshold.lessThan=" + DEFAULT_SIM_BACKUP_THRESHOLD);

        // Get all the partnerProfileList where simBackupThreshold is less than UPDATED_SIM_BACKUP_THRESHOLD
        defaultPartnerProfileShouldBeFound("simBackupThreshold.lessThan=" + UPDATED_SIM_BACKUP_THRESHOLD);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesBySimBackupThresholdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where simBackupThreshold is greater than DEFAULT_SIM_BACKUP_THRESHOLD
        defaultPartnerProfileShouldNotBeFound("simBackupThreshold.greaterThan=" + DEFAULT_SIM_BACKUP_THRESHOLD);

        // Get all the partnerProfileList where simBackupThreshold is greater than SMALLER_SIM_BACKUP_THRESHOLD
        defaultPartnerProfileShouldBeFound("simBackupThreshold.greaterThan=" + SMALLER_SIM_BACKUP_THRESHOLD);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCanDoProfileAssignmentIsEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where canDoProfileAssignment equals to DEFAULT_CAN_DO_PROFILE_ASSIGNMENT
        defaultPartnerProfileShouldBeFound("canDoProfileAssignment.equals=" + DEFAULT_CAN_DO_PROFILE_ASSIGNMENT);

        // Get all the partnerProfileList where canDoProfileAssignment equals to UPDATED_CAN_DO_PROFILE_ASSIGNMENT
        defaultPartnerProfileShouldNotBeFound("canDoProfileAssignment.equals=" + UPDATED_CAN_DO_PROFILE_ASSIGNMENT);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCanDoProfileAssignmentIsNotEqualToSomething() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where canDoProfileAssignment not equals to DEFAULT_CAN_DO_PROFILE_ASSIGNMENT
        defaultPartnerProfileShouldNotBeFound("canDoProfileAssignment.notEquals=" + DEFAULT_CAN_DO_PROFILE_ASSIGNMENT);

        // Get all the partnerProfileList where canDoProfileAssignment not equals to UPDATED_CAN_DO_PROFILE_ASSIGNMENT
        defaultPartnerProfileShouldBeFound("canDoProfileAssignment.notEquals=" + UPDATED_CAN_DO_PROFILE_ASSIGNMENT);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCanDoProfileAssignmentIsInShouldWork() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where canDoProfileAssignment in DEFAULT_CAN_DO_PROFILE_ASSIGNMENT or UPDATED_CAN_DO_PROFILE_ASSIGNMENT
        defaultPartnerProfileShouldBeFound(
            "canDoProfileAssignment.in=" + DEFAULT_CAN_DO_PROFILE_ASSIGNMENT + "," + UPDATED_CAN_DO_PROFILE_ASSIGNMENT
        );

        // Get all the partnerProfileList where canDoProfileAssignment equals to UPDATED_CAN_DO_PROFILE_ASSIGNMENT
        defaultPartnerProfileShouldNotBeFound("canDoProfileAssignment.in=" + UPDATED_CAN_DO_PROFILE_ASSIGNMENT);
    }

    @Test
    @Transactional
    void getAllPartnerProfilesByCanDoProfileAssignmentIsNullOrNotNull() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        // Get all the partnerProfileList where canDoProfileAssignment is not null
        defaultPartnerProfileShouldBeFound("canDoProfileAssignment.specified=true");

        // Get all the partnerProfileList where canDoProfileAssignment is null
        defaultPartnerProfileShouldNotBeFound("canDoProfileAssignment.specified=false");
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPartnerProfileShouldBeFound(String filter) throws Exception {
        restPartnerProfileMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(partnerProfile.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].level").value(hasItem(DEFAULT_LEVEL)))
            .andExpect(jsonPath("$.[*].parentPartnerProfileId").value(hasItem(DEFAULT_PARENT_PARTNER_PROFILE_ID.intValue())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].canHaveMultipleZones").value(hasItem(DEFAULT_CAN_HAVE_MULTIPLE_ZONES.booleanValue())))
            .andExpect(jsonPath("$.[*].zoneThreshold").value(hasItem(DEFAULT_ZONE_THRESHOLD)))
            .andExpect(jsonPath("$.[*].simBackupThreshold").value(hasItem(DEFAULT_SIM_BACKUP_THRESHOLD)))
            .andExpect(jsonPath("$.[*].canDoProfileAssignment").value(hasItem(DEFAULT_CAN_DO_PROFILE_ASSIGNMENT.booleanValue())));

        // Check, that the count call also returns 1
        restPartnerProfileMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPartnerProfileShouldNotBeFound(String filter) throws Exception {
        restPartnerProfileMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPartnerProfileMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingPartnerProfile() throws Exception {
        // Get the partnerProfile
        restPartnerProfileMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPartnerProfile() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        int databaseSizeBeforeUpdate = partnerProfileRepository.findAll().size();

        // Update the partnerProfile
        PartnerProfile updatedPartnerProfile = partnerProfileRepository.findById(partnerProfile.getId()).get();
        // Disconnect from session so that the updates on updatedPartnerProfile are not directly saved in db
        em.detach(updatedPartnerProfile);
        updatedPartnerProfile
            .code(UPDATED_CODE)
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .level(UPDATED_LEVEL)
            .parentPartnerProfileId(UPDATED_PARENT_PARTNER_PROFILE_ID)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .canHaveMultipleZones(UPDATED_CAN_HAVE_MULTIPLE_ZONES)
            .zoneThreshold(UPDATED_ZONE_THRESHOLD)
            .simBackupThreshold(UPDATED_SIM_BACKUP_THRESHOLD)
            .canDoProfileAssignment(UPDATED_CAN_DO_PROFILE_ASSIGNMENT);
        PartnerProfileDTO partnerProfileDTO = partnerProfileMapper.toDto(updatedPartnerProfile);

        restPartnerProfileMockMvc
            .perform(
                put(ENTITY_API_URL_ID, partnerProfileDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(partnerProfileDTO))
            )
            .andExpect(status().isOk());

        // Validate the PartnerProfile in the database
        List<PartnerProfile> partnerProfileList = partnerProfileRepository.findAll();
        assertThat(partnerProfileList).hasSize(databaseSizeBeforeUpdate);
        PartnerProfile testPartnerProfile = partnerProfileList.get(partnerProfileList.size() - 1);
        assertThat(testPartnerProfile.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testPartnerProfile.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPartnerProfile.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPartnerProfile.getLevel()).isEqualTo(UPDATED_LEVEL);
        assertThat(testPartnerProfile.getParentPartnerProfileId()).isEqualTo(UPDATED_PARENT_PARTNER_PROFILE_ID);
        assertThat(testPartnerProfile.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testPartnerProfile.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testPartnerProfile.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testPartnerProfile.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testPartnerProfile.getCanHaveMultipleZones()).isEqualTo(UPDATED_CAN_HAVE_MULTIPLE_ZONES);
        assertThat(testPartnerProfile.getZoneThreshold()).isEqualTo(UPDATED_ZONE_THRESHOLD);
        assertThat(testPartnerProfile.getSimBackupThreshold()).isEqualTo(UPDATED_SIM_BACKUP_THRESHOLD);
        assertThat(testPartnerProfile.getCanDoProfileAssignment()).isEqualTo(UPDATED_CAN_DO_PROFILE_ASSIGNMENT);
    }

    @Test
    @Transactional
    void putNonExistingPartnerProfile() throws Exception {
        int databaseSizeBeforeUpdate = partnerProfileRepository.findAll().size();
        partnerProfile.setId(count.incrementAndGet());

        // Create the PartnerProfile
        PartnerProfileDTO partnerProfileDTO = partnerProfileMapper.toDto(partnerProfile);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPartnerProfileMockMvc
            .perform(
                put(ENTITY_API_URL_ID, partnerProfileDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(partnerProfileDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PartnerProfile in the database
        List<PartnerProfile> partnerProfileList = partnerProfileRepository.findAll();
        assertThat(partnerProfileList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPartnerProfile() throws Exception {
        int databaseSizeBeforeUpdate = partnerProfileRepository.findAll().size();
        partnerProfile.setId(count.incrementAndGet());

        // Create the PartnerProfile
        PartnerProfileDTO partnerProfileDTO = partnerProfileMapper.toDto(partnerProfile);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPartnerProfileMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(partnerProfileDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PartnerProfile in the database
        List<PartnerProfile> partnerProfileList = partnerProfileRepository.findAll();
        assertThat(partnerProfileList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPartnerProfile() throws Exception {
        int databaseSizeBeforeUpdate = partnerProfileRepository.findAll().size();
        partnerProfile.setId(count.incrementAndGet());

        // Create the PartnerProfile
        PartnerProfileDTO partnerProfileDTO = partnerProfileMapper.toDto(partnerProfile);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPartnerProfileMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(partnerProfileDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PartnerProfile in the database
        List<PartnerProfile> partnerProfileList = partnerProfileRepository.findAll();
        assertThat(partnerProfileList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePartnerProfileWithPatch() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        int databaseSizeBeforeUpdate = partnerProfileRepository.findAll().size();

        // Update the partnerProfile using partial update
        PartnerProfile partialUpdatedPartnerProfile = new PartnerProfile();
        partialUpdatedPartnerProfile.setId(partnerProfile.getId());

        partialUpdatedPartnerProfile
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .parentPartnerProfileId(UPDATED_PARENT_PARTNER_PROFILE_ID)
            .createdBy(UPDATED_CREATED_BY)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .simBackupThreshold(UPDATED_SIM_BACKUP_THRESHOLD)
            .canDoProfileAssignment(UPDATED_CAN_DO_PROFILE_ASSIGNMENT);

        restPartnerProfileMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPartnerProfile.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPartnerProfile))
            )
            .andExpect(status().isOk());

        // Validate the PartnerProfile in the database
        List<PartnerProfile> partnerProfileList = partnerProfileRepository.findAll();
        assertThat(partnerProfileList).hasSize(databaseSizeBeforeUpdate);
        PartnerProfile testPartnerProfile = partnerProfileList.get(partnerProfileList.size() - 1);
        assertThat(testPartnerProfile.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testPartnerProfile.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPartnerProfile.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPartnerProfile.getLevel()).isEqualTo(DEFAULT_LEVEL);
        assertThat(testPartnerProfile.getParentPartnerProfileId()).isEqualTo(UPDATED_PARENT_PARTNER_PROFILE_ID);
        assertThat(testPartnerProfile.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testPartnerProfile.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testPartnerProfile.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testPartnerProfile.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testPartnerProfile.getCanHaveMultipleZones()).isEqualTo(DEFAULT_CAN_HAVE_MULTIPLE_ZONES);
        assertThat(testPartnerProfile.getZoneThreshold()).isEqualTo(DEFAULT_ZONE_THRESHOLD);
        assertThat(testPartnerProfile.getSimBackupThreshold()).isEqualTo(UPDATED_SIM_BACKUP_THRESHOLD);
        assertThat(testPartnerProfile.getCanDoProfileAssignment()).isEqualTo(UPDATED_CAN_DO_PROFILE_ASSIGNMENT);
    }

    @Test
    @Transactional
    void fullUpdatePartnerProfileWithPatch() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        int databaseSizeBeforeUpdate = partnerProfileRepository.findAll().size();

        // Update the partnerProfile using partial update
        PartnerProfile partialUpdatedPartnerProfile = new PartnerProfile();
        partialUpdatedPartnerProfile.setId(partnerProfile.getId());

        partialUpdatedPartnerProfile
            .code(UPDATED_CODE)
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .level(UPDATED_LEVEL)
            .parentPartnerProfileId(UPDATED_PARENT_PARTNER_PROFILE_ID)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .canHaveMultipleZones(UPDATED_CAN_HAVE_MULTIPLE_ZONES)
            .zoneThreshold(UPDATED_ZONE_THRESHOLD)
            .simBackupThreshold(UPDATED_SIM_BACKUP_THRESHOLD)
            .canDoProfileAssignment(UPDATED_CAN_DO_PROFILE_ASSIGNMENT);

        restPartnerProfileMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPartnerProfile.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPartnerProfile))
            )
            .andExpect(status().isOk());

        // Validate the PartnerProfile in the database
        List<PartnerProfile> partnerProfileList = partnerProfileRepository.findAll();
        assertThat(partnerProfileList).hasSize(databaseSizeBeforeUpdate);
        PartnerProfile testPartnerProfile = partnerProfileList.get(partnerProfileList.size() - 1);
        assertThat(testPartnerProfile.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testPartnerProfile.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPartnerProfile.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPartnerProfile.getLevel()).isEqualTo(UPDATED_LEVEL);
        assertThat(testPartnerProfile.getParentPartnerProfileId()).isEqualTo(UPDATED_PARENT_PARTNER_PROFILE_ID);
        assertThat(testPartnerProfile.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testPartnerProfile.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testPartnerProfile.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testPartnerProfile.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testPartnerProfile.getCanHaveMultipleZones()).isEqualTo(UPDATED_CAN_HAVE_MULTIPLE_ZONES);
        assertThat(testPartnerProfile.getZoneThreshold()).isEqualTo(UPDATED_ZONE_THRESHOLD);
        assertThat(testPartnerProfile.getSimBackupThreshold()).isEqualTo(UPDATED_SIM_BACKUP_THRESHOLD);
        assertThat(testPartnerProfile.getCanDoProfileAssignment()).isEqualTo(UPDATED_CAN_DO_PROFILE_ASSIGNMENT);
    }

    @Test
    @Transactional
    void patchNonExistingPartnerProfile() throws Exception {
        int databaseSizeBeforeUpdate = partnerProfileRepository.findAll().size();
        partnerProfile.setId(count.incrementAndGet());

        // Create the PartnerProfile
        PartnerProfileDTO partnerProfileDTO = partnerProfileMapper.toDto(partnerProfile);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPartnerProfileMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partnerProfileDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partnerProfileDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PartnerProfile in the database
        List<PartnerProfile> partnerProfileList = partnerProfileRepository.findAll();
        assertThat(partnerProfileList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPartnerProfile() throws Exception {
        int databaseSizeBeforeUpdate = partnerProfileRepository.findAll().size();
        partnerProfile.setId(count.incrementAndGet());

        // Create the PartnerProfile
        PartnerProfileDTO partnerProfileDTO = partnerProfileMapper.toDto(partnerProfile);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPartnerProfileMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partnerProfileDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the PartnerProfile in the database
        List<PartnerProfile> partnerProfileList = partnerProfileRepository.findAll();
        assertThat(partnerProfileList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPartnerProfile() throws Exception {
        int databaseSizeBeforeUpdate = partnerProfileRepository.findAll().size();
        partnerProfile.setId(count.incrementAndGet());

        // Create the PartnerProfile
        PartnerProfileDTO partnerProfileDTO = partnerProfileMapper.toDto(partnerProfile);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPartnerProfileMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partnerProfileDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the PartnerProfile in the database
        List<PartnerProfile> partnerProfileList = partnerProfileRepository.findAll();
        assertThat(partnerProfileList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePartnerProfile() throws Exception {
        // Initialize the database
        partnerProfileRepository.saveAndFlush(partnerProfile);

        int databaseSizeBeforeDelete = partnerProfileRepository.findAll().size();

        // Delete the partnerProfile
        restPartnerProfileMockMvc
            .perform(delete(ENTITY_API_URL_ID, partnerProfile.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PartnerProfile> partnerProfileList = partnerProfileRepository.findAll();
        assertThat(partnerProfileList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
