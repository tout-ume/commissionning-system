package sn.free.commissioning.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.IntegrationTest;
import sn.free.commissioning.domain.Territory;
import sn.free.commissioning.domain.Zone;
import sn.free.commissioning.repository.TerritoryRepository;
import sn.free.commissioning.service.criteria.TerritoryCriteria;
import sn.free.commissioning.service.dto.TerritoryDTO;
import sn.free.commissioning.service.mapper.TerritoryMapper;

/**
 * Integration tests for the {@link TerritoryResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class TerritoryResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_STATE = "AAAAAAAAAA";
    private static final String UPDATED_STATE = "BBBBBBBBBB";

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/territories";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private TerritoryRepository territoryRepository;

    @Autowired
    private TerritoryMapper territoryMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTerritoryMockMvc;

    private Territory territory;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Territory createEntity(EntityManager em) {
        Territory territory = new Territory()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .state(DEFAULT_STATE)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE);
        return territory;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Territory createUpdatedEntity(EntityManager em) {
        Territory territory = new Territory()
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .state(UPDATED_STATE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE);
        return territory;
    }

    @BeforeEach
    public void initTest() {
        territory = createEntity(em);
    }

    @Test
    @Transactional
    void createTerritory() throws Exception {
        int databaseSizeBeforeCreate = territoryRepository.findAll().size();
        // Create the Territory
        TerritoryDTO territoryDTO = territoryMapper.toDto(territory);
        restTerritoryMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(territoryDTO)))
            .andExpect(status().isCreated());

        // Validate the Territory in the database
        List<Territory> territoryList = territoryRepository.findAll();
        assertThat(territoryList).hasSize(databaseSizeBeforeCreate + 1);
        Territory testTerritory = territoryList.get(territoryList.size() - 1);
        assertThat(testTerritory.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTerritory.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testTerritory.getState()).isEqualTo(DEFAULT_STATE);
        assertThat(testTerritory.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testTerritory.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testTerritory.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testTerritory.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void createTerritoryWithExistingId() throws Exception {
        // Create the Territory with an existing ID
        territory.setId(1L);
        TerritoryDTO territoryDTO = territoryMapper.toDto(territory);

        int databaseSizeBeforeCreate = territoryRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restTerritoryMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(territoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Territory in the database
        List<Territory> territoryList = territoryRepository.findAll();
        assertThat(territoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = territoryRepository.findAll().size();
        // set the field null
        territory.setName(null);

        // Create the Territory, which fails.
        TerritoryDTO territoryDTO = territoryMapper.toDto(territory);

        restTerritoryMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(territoryDTO)))
            .andExpect(status().isBadRequest());

        List<Territory> territoryList = territoryRepository.findAll();
        assertThat(territoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCreatedByIsRequired() throws Exception {
        int databaseSizeBeforeTest = territoryRepository.findAll().size();
        // set the field null
        territory.setCreatedBy(null);

        // Create the Territory, which fails.
        TerritoryDTO territoryDTO = territoryMapper.toDto(territory);

        restTerritoryMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(territoryDTO)))
            .andExpect(status().isBadRequest());

        List<Territory> territoryList = territoryRepository.findAll();
        assertThat(territoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllTerritories() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList
        restTerritoryMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(territory.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())));
    }

    @Test
    @Transactional
    void getTerritory() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get the territory
        restTerritoryMockMvc
            .perform(get(ENTITY_API_URL_ID, territory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(territory.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.state").value(DEFAULT_STATE))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()));
    }

    @Test
    @Transactional
    void getTerritoriesByIdFiltering() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        Long id = territory.getId();

        defaultTerritoryShouldBeFound("id.equals=" + id);
        defaultTerritoryShouldNotBeFound("id.notEquals=" + id);

        defaultTerritoryShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTerritoryShouldNotBeFound("id.greaterThan=" + id);

        defaultTerritoryShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTerritoryShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllTerritoriesByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where name equals to DEFAULT_NAME
        defaultTerritoryShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the territoryList where name equals to UPDATED_NAME
        defaultTerritoryShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllTerritoriesByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where name not equals to DEFAULT_NAME
        defaultTerritoryShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the territoryList where name not equals to UPDATED_NAME
        defaultTerritoryShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllTerritoriesByNameIsInShouldWork() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where name in DEFAULT_NAME or UPDATED_NAME
        defaultTerritoryShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the territoryList where name equals to UPDATED_NAME
        defaultTerritoryShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllTerritoriesByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where name is not null
        defaultTerritoryShouldBeFound("name.specified=true");

        // Get all the territoryList where name is null
        defaultTerritoryShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    void getAllTerritoriesByNameContainsSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where name contains DEFAULT_NAME
        defaultTerritoryShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the territoryList where name contains UPDATED_NAME
        defaultTerritoryShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllTerritoriesByNameNotContainsSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where name does not contain DEFAULT_NAME
        defaultTerritoryShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the territoryList where name does not contain UPDATED_NAME
        defaultTerritoryShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    void getAllTerritoriesByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where description equals to DEFAULT_DESCRIPTION
        defaultTerritoryShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the territoryList where description equals to UPDATED_DESCRIPTION
        defaultTerritoryShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllTerritoriesByDescriptionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where description not equals to DEFAULT_DESCRIPTION
        defaultTerritoryShouldNotBeFound("description.notEquals=" + DEFAULT_DESCRIPTION);

        // Get all the territoryList where description not equals to UPDATED_DESCRIPTION
        defaultTerritoryShouldBeFound("description.notEquals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllTerritoriesByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultTerritoryShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the territoryList where description equals to UPDATED_DESCRIPTION
        defaultTerritoryShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllTerritoriesByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where description is not null
        defaultTerritoryShouldBeFound("description.specified=true");

        // Get all the territoryList where description is null
        defaultTerritoryShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    void getAllTerritoriesByDescriptionContainsSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where description contains DEFAULT_DESCRIPTION
        defaultTerritoryShouldBeFound("description.contains=" + DEFAULT_DESCRIPTION);

        // Get all the territoryList where description contains UPDATED_DESCRIPTION
        defaultTerritoryShouldNotBeFound("description.contains=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllTerritoriesByDescriptionNotContainsSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where description does not contain DEFAULT_DESCRIPTION
        defaultTerritoryShouldNotBeFound("description.doesNotContain=" + DEFAULT_DESCRIPTION);

        // Get all the territoryList where description does not contain UPDATED_DESCRIPTION
        defaultTerritoryShouldBeFound("description.doesNotContain=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void getAllTerritoriesByStateIsEqualToSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where state equals to DEFAULT_STATE
        defaultTerritoryShouldBeFound("state.equals=" + DEFAULT_STATE);

        // Get all the territoryList where state equals to UPDATED_STATE
        defaultTerritoryShouldNotBeFound("state.equals=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    void getAllTerritoriesByStateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where state not equals to DEFAULT_STATE
        defaultTerritoryShouldNotBeFound("state.notEquals=" + DEFAULT_STATE);

        // Get all the territoryList where state not equals to UPDATED_STATE
        defaultTerritoryShouldBeFound("state.notEquals=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    void getAllTerritoriesByStateIsInShouldWork() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where state in DEFAULT_STATE or UPDATED_STATE
        defaultTerritoryShouldBeFound("state.in=" + DEFAULT_STATE + "," + UPDATED_STATE);

        // Get all the territoryList where state equals to UPDATED_STATE
        defaultTerritoryShouldNotBeFound("state.in=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    void getAllTerritoriesByStateIsNullOrNotNull() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where state is not null
        defaultTerritoryShouldBeFound("state.specified=true");

        // Get all the territoryList where state is null
        defaultTerritoryShouldNotBeFound("state.specified=false");
    }

    @Test
    @Transactional
    void getAllTerritoriesByStateContainsSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where state contains DEFAULT_STATE
        defaultTerritoryShouldBeFound("state.contains=" + DEFAULT_STATE);

        // Get all the territoryList where state contains UPDATED_STATE
        defaultTerritoryShouldNotBeFound("state.contains=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    void getAllTerritoriesByStateNotContainsSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where state does not contain DEFAULT_STATE
        defaultTerritoryShouldNotBeFound("state.doesNotContain=" + DEFAULT_STATE);

        // Get all the territoryList where state does not contain UPDATED_STATE
        defaultTerritoryShouldBeFound("state.doesNotContain=" + UPDATED_STATE);
    }

    @Test
    @Transactional
    void getAllTerritoriesByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where createdBy equals to DEFAULT_CREATED_BY
        defaultTerritoryShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the territoryList where createdBy equals to UPDATED_CREATED_BY
        defaultTerritoryShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllTerritoriesByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where createdBy not equals to DEFAULT_CREATED_BY
        defaultTerritoryShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the territoryList where createdBy not equals to UPDATED_CREATED_BY
        defaultTerritoryShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllTerritoriesByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultTerritoryShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the territoryList where createdBy equals to UPDATED_CREATED_BY
        defaultTerritoryShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllTerritoriesByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where createdBy is not null
        defaultTerritoryShouldBeFound("createdBy.specified=true");

        // Get all the territoryList where createdBy is null
        defaultTerritoryShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    void getAllTerritoriesByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where createdBy contains DEFAULT_CREATED_BY
        defaultTerritoryShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the territoryList where createdBy contains UPDATED_CREATED_BY
        defaultTerritoryShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllTerritoriesByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where createdBy does not contain DEFAULT_CREATED_BY
        defaultTerritoryShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the territoryList where createdBy does not contain UPDATED_CREATED_BY
        defaultTerritoryShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllTerritoriesByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where createdDate equals to DEFAULT_CREATED_DATE
        defaultTerritoryShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the territoryList where createdDate equals to UPDATED_CREATED_DATE
        defaultTerritoryShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllTerritoriesByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultTerritoryShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the territoryList where createdDate not equals to UPDATED_CREATED_DATE
        defaultTerritoryShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllTerritoriesByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultTerritoryShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the territoryList where createdDate equals to UPDATED_CREATED_DATE
        defaultTerritoryShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllTerritoriesByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where createdDate is not null
        defaultTerritoryShouldBeFound("createdDate.specified=true");

        // Get all the territoryList where createdDate is null
        defaultTerritoryShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    void getAllTerritoriesByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultTerritoryShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the territoryList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultTerritoryShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllTerritoriesByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultTerritoryShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the territoryList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultTerritoryShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllTerritoriesByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultTerritoryShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the territoryList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultTerritoryShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllTerritoriesByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where lastModifiedBy is not null
        defaultTerritoryShouldBeFound("lastModifiedBy.specified=true");

        // Get all the territoryList where lastModifiedBy is null
        defaultTerritoryShouldNotBeFound("lastModifiedBy.specified=false");
    }

    @Test
    @Transactional
    void getAllTerritoriesByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultTerritoryShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the territoryList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultTerritoryShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllTerritoriesByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultTerritoryShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the territoryList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultTerritoryShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllTerritoriesByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultTerritoryShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the territoryList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultTerritoryShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllTerritoriesByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultTerritoryShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the territoryList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultTerritoryShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllTerritoriesByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultTerritoryShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the territoryList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultTerritoryShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllTerritoriesByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        // Get all the territoryList where lastModifiedDate is not null
        defaultTerritoryShouldBeFound("lastModifiedDate.specified=true");

        // Get all the territoryList where lastModifiedDate is null
        defaultTerritoryShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    void getAllTerritoriesByZoneIsEqualToSomething() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);
        Zone zone;
        if (TestUtil.findAll(em, Zone.class).isEmpty()) {
            zone = ZoneResourceIT.createEntity(em);
            em.persist(zone);
            em.flush();
        } else {
            zone = TestUtil.findAll(em, Zone.class).get(0);
        }
        em.persist(zone);
        em.flush();
        territory.addZone(zone);
        territoryRepository.saveAndFlush(territory);
        Long zoneId = zone.getId();

        // Get all the territoryList where zone equals to zoneId
        defaultTerritoryShouldBeFound("zoneId.equals=" + zoneId);

        // Get all the territoryList where zone equals to (zoneId + 1)
        defaultTerritoryShouldNotBeFound("zoneId.equals=" + (zoneId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTerritoryShouldBeFound(String filter) throws Exception {
        restTerritoryMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(territory.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].state").value(hasItem(DEFAULT_STATE)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())));

        // Check, that the count call also returns 1
        restTerritoryMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTerritoryShouldNotBeFound(String filter) throws Exception {
        restTerritoryMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTerritoryMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingTerritory() throws Exception {
        // Get the territory
        restTerritoryMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewTerritory() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        int databaseSizeBeforeUpdate = territoryRepository.findAll().size();

        // Update the territory
        Territory updatedTerritory = territoryRepository.findById(territory.getId()).get();
        // Disconnect from session so that the updates on updatedTerritory are not directly saved in db
        em.detach(updatedTerritory);
        updatedTerritory
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .state(UPDATED_STATE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE);
        TerritoryDTO territoryDTO = territoryMapper.toDto(updatedTerritory);

        restTerritoryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, territoryDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(territoryDTO))
            )
            .andExpect(status().isOk());

        // Validate the Territory in the database
        List<Territory> territoryList = territoryRepository.findAll();
        assertThat(territoryList).hasSize(databaseSizeBeforeUpdate);
        Territory testTerritory = territoryList.get(territoryList.size() - 1);
        assertThat(testTerritory.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTerritory.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testTerritory.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testTerritory.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testTerritory.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testTerritory.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testTerritory.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void putNonExistingTerritory() throws Exception {
        int databaseSizeBeforeUpdate = territoryRepository.findAll().size();
        territory.setId(count.incrementAndGet());

        // Create the Territory
        TerritoryDTO territoryDTO = territoryMapper.toDto(territory);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTerritoryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, territoryDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(territoryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Territory in the database
        List<Territory> territoryList = territoryRepository.findAll();
        assertThat(territoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchTerritory() throws Exception {
        int databaseSizeBeforeUpdate = territoryRepository.findAll().size();
        territory.setId(count.incrementAndGet());

        // Create the Territory
        TerritoryDTO territoryDTO = territoryMapper.toDto(territory);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTerritoryMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(territoryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Territory in the database
        List<Territory> territoryList = territoryRepository.findAll();
        assertThat(territoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamTerritory() throws Exception {
        int databaseSizeBeforeUpdate = territoryRepository.findAll().size();
        territory.setId(count.incrementAndGet());

        // Create the Territory
        TerritoryDTO territoryDTO = territoryMapper.toDto(territory);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTerritoryMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(territoryDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Territory in the database
        List<Territory> territoryList = territoryRepository.findAll();
        assertThat(territoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateTerritoryWithPatch() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        int databaseSizeBeforeUpdate = territoryRepository.findAll().size();

        // Update the territory using partial update
        Territory partialUpdatedTerritory = new Territory();
        partialUpdatedTerritory.setId(territory.getId());

        partialUpdatedTerritory
            .state(UPDATED_STATE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE);

        restTerritoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTerritory.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTerritory))
            )
            .andExpect(status().isOk());

        // Validate the Territory in the database
        List<Territory> territoryList = territoryRepository.findAll();
        assertThat(territoryList).hasSize(databaseSizeBeforeUpdate);
        Territory testTerritory = territoryList.get(territoryList.size() - 1);
        assertThat(testTerritory.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTerritory.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testTerritory.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testTerritory.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testTerritory.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testTerritory.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testTerritory.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void fullUpdateTerritoryWithPatch() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        int databaseSizeBeforeUpdate = territoryRepository.findAll().size();

        // Update the territory using partial update
        Territory partialUpdatedTerritory = new Territory();
        partialUpdatedTerritory.setId(territory.getId());

        partialUpdatedTerritory
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .state(UPDATED_STATE)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE);

        restTerritoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTerritory.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTerritory))
            )
            .andExpect(status().isOk());

        // Validate the Territory in the database
        List<Territory> territoryList = territoryRepository.findAll();
        assertThat(territoryList).hasSize(databaseSizeBeforeUpdate);
        Territory testTerritory = territoryList.get(territoryList.size() - 1);
        assertThat(testTerritory.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTerritory.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testTerritory.getState()).isEqualTo(UPDATED_STATE);
        assertThat(testTerritory.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testTerritory.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testTerritory.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testTerritory.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void patchNonExistingTerritory() throws Exception {
        int databaseSizeBeforeUpdate = territoryRepository.findAll().size();
        territory.setId(count.incrementAndGet());

        // Create the Territory
        TerritoryDTO territoryDTO = territoryMapper.toDto(territory);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTerritoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, territoryDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(territoryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Territory in the database
        List<Territory> territoryList = territoryRepository.findAll();
        assertThat(territoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchTerritory() throws Exception {
        int databaseSizeBeforeUpdate = territoryRepository.findAll().size();
        territory.setId(count.incrementAndGet());

        // Create the Territory
        TerritoryDTO territoryDTO = territoryMapper.toDto(territory);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTerritoryMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(territoryDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Territory in the database
        List<Territory> territoryList = territoryRepository.findAll();
        assertThat(territoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamTerritory() throws Exception {
        int databaseSizeBeforeUpdate = territoryRepository.findAll().size();
        territory.setId(count.incrementAndGet());

        // Create the Territory
        TerritoryDTO territoryDTO = territoryMapper.toDto(territory);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTerritoryMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(territoryDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Territory in the database
        List<Territory> territoryList = territoryRepository.findAll();
        assertThat(territoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteTerritory() throws Exception {
        // Initialize the database
        territoryRepository.saveAndFlush(territory);

        int databaseSizeBeforeDelete = territoryRepository.findAll().size();

        // Delete the territory
        restTerritoryMockMvc
            .perform(delete(ENTITY_API_URL_ID, territory.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Territory> territoryList = territoryRepository.findAll();
        assertThat(territoryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
