package sn.free.commissioning.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.IntegrationTest;
import sn.free.commissioning.domain.Commission;
import sn.free.commissioning.domain.TransactionOperation;
import sn.free.commissioning.repository.TransactionOperationRepository;
import sn.free.commissioning.service.criteria.TransactionOperationCriteria;
import sn.free.commissioning.service.dto.TransactionOperationDTO;
import sn.free.commissioning.service.mapper.TransactionOperationMapper;

/**
 * Integration tests for the {@link TransactionOperationResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class TransactionOperationResourceIT {

    private static final Double DEFAULT_AMOUNT = 1D;
    private static final Double UPDATED_AMOUNT = 2D;
    private static final Double SMALLER_AMOUNT = 1D - 1D;

    private static final String DEFAULT_SUBS_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_SUBS_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_AGENT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_AGENT_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE_TRANSACTION = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_TRANSACTION = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_TRANSACTION_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_TRANSACTION_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_SENDER_ZONE = "AAAAAAAAAA";
    private static final String UPDATED_SENDER_ZONE = "BBBBBBBBBB";

    private static final String DEFAULT_SENDER_PROFILE = "AAAAAAAAAA";
    private static final String UPDATED_SENDER_PROFILE = "BBBBBBBBBB";

    private static final String DEFAULT_CODE_TERRITORY = "AAAAAAAAAA";
    private static final String UPDATED_CODE_TERRITORY = "BBBBBBBBBB";

    private static final String DEFAULT_SUB_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_SUB_TYPE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_OPERATION_SHORT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_OPERATION_SHORT_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_OPERATION_SHORT_DATE = LocalDate.ofEpochDay(-1L);

    private static final Instant DEFAULT_OPERATION_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_OPERATION_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_IS_FRAUD = false;
    private static final Boolean UPDATED_IS_FRAUD = true;

    private static final Instant DEFAULT_TAGGED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TAGGED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_FRAUD_SOURCE = "AAAAAAAAAA";
    private static final String UPDATED_FRAUD_SOURCE = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENT = "BBBBBBBBBB";

    private static final Instant DEFAULT_FALSE_POSITIVE_DETECTED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FALSE_POSITIVE_DETECTED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_TID = "AAAAAAAAAA";
    private static final String UPDATED_TID = "BBBBBBBBBB";

    private static final String DEFAULT_PARENT_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_PARENT_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_FCT_DT = "AAAAAAAAAA";
    private static final String UPDATED_FCT_DT = "BBBBBBBBBB";

    private static final String DEFAULT_PARENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_PARENT_ID = "BBBBBBBBBB";

    private static final Instant DEFAULT_CANCELED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CANCELED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CANCELED_ID = "AAAAAAAAAA";
    private static final String UPDATED_CANCELED_ID = "BBBBBBBBBB";

    private static final String DEFAULT_PRODUCT_ID = "AAAAAAAAAA";
    private static final String UPDATED_PRODUCT_ID = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/transaction-operations";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private TransactionOperationRepository transactionOperationRepository;

    @Autowired
    private TransactionOperationMapper transactionOperationMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restTransactionOperationMockMvc;

    private TransactionOperation transactionOperation;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TransactionOperation createEntity(EntityManager em) {
        TransactionOperation transactionOperation = new TransactionOperation()
            .amount(DEFAULT_AMOUNT)
            .subsMsisdn(DEFAULT_SUBS_MSISDN)
            .agentMsisdn(DEFAULT_AGENT_MSISDN)
            .typeTransaction(DEFAULT_TYPE_TRANSACTION)
            .createdAt(DEFAULT_CREATED_AT)
            .transactionStatus(DEFAULT_TRANSACTION_STATUS)
            .senderZone(DEFAULT_SENDER_ZONE)
            .senderProfile(DEFAULT_SENDER_PROFILE)
            .codeTerritory(DEFAULT_CODE_TERRITORY)
            .subType(DEFAULT_SUB_TYPE)
            .operationShortDate(DEFAULT_OPERATION_SHORT_DATE)
            .operationDate(DEFAULT_OPERATION_DATE)
            .isFraud(DEFAULT_IS_FRAUD)
            .taggedAt(DEFAULT_TAGGED_AT)
            .fraudSource(DEFAULT_FRAUD_SOURCE)
            .comment(DEFAULT_COMMENT)
            .falsePositiveDetectedAt(DEFAULT_FALSE_POSITIVE_DETECTED_AT)
            .tid(DEFAULT_TID)
            .parentMsisdn(DEFAULT_PARENT_MSISDN)
            .fctDt(DEFAULT_FCT_DT)
            .parentId(DEFAULT_PARENT_ID)
            .canceledAt(DEFAULT_CANCELED_AT)
            .canceledId(DEFAULT_CANCELED_ID)
            .productId(DEFAULT_PRODUCT_ID);
        return transactionOperation;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TransactionOperation createUpdatedEntity(EntityManager em) {
        TransactionOperation transactionOperation = new TransactionOperation()
            .amount(UPDATED_AMOUNT)
            .subsMsisdn(UPDATED_SUBS_MSISDN)
            .agentMsisdn(UPDATED_AGENT_MSISDN)
            .typeTransaction(UPDATED_TYPE_TRANSACTION)
            .createdAt(UPDATED_CREATED_AT)
            .transactionStatus(UPDATED_TRANSACTION_STATUS)
            .senderZone(UPDATED_SENDER_ZONE)
            .senderProfile(UPDATED_SENDER_PROFILE)
            .codeTerritory(UPDATED_CODE_TERRITORY)
            .subType(UPDATED_SUB_TYPE)
            .operationShortDate(UPDATED_OPERATION_SHORT_DATE)
            .operationDate(UPDATED_OPERATION_DATE)
            .isFraud(UPDATED_IS_FRAUD)
            .taggedAt(UPDATED_TAGGED_AT)
            .fraudSource(UPDATED_FRAUD_SOURCE)
            .comment(UPDATED_COMMENT)
            .falsePositiveDetectedAt(UPDATED_FALSE_POSITIVE_DETECTED_AT)
            .tid(UPDATED_TID)
            .parentMsisdn(UPDATED_PARENT_MSISDN)
            .fctDt(UPDATED_FCT_DT)
            .parentId(UPDATED_PARENT_ID)
            .canceledAt(UPDATED_CANCELED_AT)
            .canceledId(UPDATED_CANCELED_ID)
            .productId(UPDATED_PRODUCT_ID);
        return transactionOperation;
    }

    @BeforeEach
    public void initTest() {
        transactionOperation = createEntity(em);
    }

    @Test
    @Transactional
    void createTransactionOperation() throws Exception {
        int databaseSizeBeforeCreate = transactionOperationRepository.findAll().size();
        // Create the TransactionOperation
        TransactionOperationDTO transactionOperationDTO = transactionOperationMapper.toDto(transactionOperation);
        restTransactionOperationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(transactionOperationDTO))
            )
            .andExpect(status().isCreated());

        // Validate the TransactionOperation in the database
        List<TransactionOperation> transactionOperationList = transactionOperationRepository.findAll();
        assertThat(transactionOperationList).hasSize(databaseSizeBeforeCreate + 1);
        TransactionOperation testTransactionOperation = transactionOperationList.get(transactionOperationList.size() - 1);
        assertThat(testTransactionOperation.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testTransactionOperation.getSubsMsisdn()).isEqualTo(DEFAULT_SUBS_MSISDN);
        assertThat(testTransactionOperation.getAgentMsisdn()).isEqualTo(DEFAULT_AGENT_MSISDN);
        assertThat(testTransactionOperation.getTypeTransaction()).isEqualTo(DEFAULT_TYPE_TRANSACTION);
        assertThat(testTransactionOperation.getCreatedAt()).isEqualTo(DEFAULT_CREATED_AT);
        assertThat(testTransactionOperation.getTransactionStatus()).isEqualTo(DEFAULT_TRANSACTION_STATUS);
        assertThat(testTransactionOperation.getSenderZone()).isEqualTo(DEFAULT_SENDER_ZONE);
        assertThat(testTransactionOperation.getSenderProfile()).isEqualTo(DEFAULT_SENDER_PROFILE);
        assertThat(testTransactionOperation.getCodeTerritory()).isEqualTo(DEFAULT_CODE_TERRITORY);
        assertThat(testTransactionOperation.getSubType()).isEqualTo(DEFAULT_SUB_TYPE);
        assertThat(testTransactionOperation.getOperationShortDate()).isEqualTo(DEFAULT_OPERATION_SHORT_DATE);
        assertThat(testTransactionOperation.getOperationDate()).isEqualTo(DEFAULT_OPERATION_DATE);
        assertThat(testTransactionOperation.getIsFraud()).isEqualTo(DEFAULT_IS_FRAUD);
        assertThat(testTransactionOperation.getTaggedAt()).isEqualTo(DEFAULT_TAGGED_AT);
        assertThat(testTransactionOperation.getFraudSource()).isEqualTo(DEFAULT_FRAUD_SOURCE);
        assertThat(testTransactionOperation.getComment()).isEqualTo(DEFAULT_COMMENT);
        assertThat(testTransactionOperation.getFalsePositiveDetectedAt()).isEqualTo(DEFAULT_FALSE_POSITIVE_DETECTED_AT);
        assertThat(testTransactionOperation.getTid()).isEqualTo(DEFAULT_TID);
        assertThat(testTransactionOperation.getParentMsisdn()).isEqualTo(DEFAULT_PARENT_MSISDN);
        assertThat(testTransactionOperation.getFctDt()).isEqualTo(DEFAULT_FCT_DT);
        assertThat(testTransactionOperation.getParentId()).isEqualTo(DEFAULT_PARENT_ID);
        assertThat(testTransactionOperation.getCanceledAt()).isEqualTo(DEFAULT_CANCELED_AT);
        assertThat(testTransactionOperation.getCanceledId()).isEqualTo(DEFAULT_CANCELED_ID);
        assertThat(testTransactionOperation.getProductId()).isEqualTo(DEFAULT_PRODUCT_ID);
    }

    @Test
    @Transactional
    void createTransactionOperationWithExistingId() throws Exception {
        // Create the TransactionOperation with an existing ID
        transactionOperation.setId(1L);
        TransactionOperationDTO transactionOperationDTO = transactionOperationMapper.toDto(transactionOperation);

        int databaseSizeBeforeCreate = transactionOperationRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restTransactionOperationMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(transactionOperationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TransactionOperation in the database
        List<TransactionOperation> transactionOperationList = transactionOperationRepository.findAll();
        assertThat(transactionOperationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllTransactionOperations() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList
        restTransactionOperationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(transactionOperation.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].subsMsisdn").value(hasItem(DEFAULT_SUBS_MSISDN)))
            .andExpect(jsonPath("$.[*].agentMsisdn").value(hasItem(DEFAULT_AGENT_MSISDN)))
            .andExpect(jsonPath("$.[*].typeTransaction").value(hasItem(DEFAULT_TYPE_TRANSACTION)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].transactionStatus").value(hasItem(DEFAULT_TRANSACTION_STATUS)))
            .andExpect(jsonPath("$.[*].senderZone").value(hasItem(DEFAULT_SENDER_ZONE)))
            .andExpect(jsonPath("$.[*].senderProfile").value(hasItem(DEFAULT_SENDER_PROFILE)))
            .andExpect(jsonPath("$.[*].codeTerritory").value(hasItem(DEFAULT_CODE_TERRITORY)))
            .andExpect(jsonPath("$.[*].subType").value(hasItem(DEFAULT_SUB_TYPE)))
            .andExpect(jsonPath("$.[*].operationShortDate").value(hasItem(DEFAULT_OPERATION_SHORT_DATE.toString())))
            .andExpect(jsonPath("$.[*].operationDate").value(hasItem(DEFAULT_OPERATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].isFraud").value(hasItem(DEFAULT_IS_FRAUD.booleanValue())))
            .andExpect(jsonPath("$.[*].taggedAt").value(hasItem(DEFAULT_TAGGED_AT.toString())))
            .andExpect(jsonPath("$.[*].fraudSource").value(hasItem(DEFAULT_FRAUD_SOURCE)))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT)))
            .andExpect(jsonPath("$.[*].falsePositiveDetectedAt").value(hasItem(DEFAULT_FALSE_POSITIVE_DETECTED_AT.toString())))
            .andExpect(jsonPath("$.[*].tid").value(hasItem(DEFAULT_TID)))
            .andExpect(jsonPath("$.[*].parentMsisdn").value(hasItem(DEFAULT_PARENT_MSISDN)))
            .andExpect(jsonPath("$.[*].fctDt").value(hasItem(DEFAULT_FCT_DT)))
            .andExpect(jsonPath("$.[*].parentId").value(hasItem(DEFAULT_PARENT_ID)))
            .andExpect(jsonPath("$.[*].canceledAt").value(hasItem(DEFAULT_CANCELED_AT.toString())))
            .andExpect(jsonPath("$.[*].canceledId").value(hasItem(DEFAULT_CANCELED_ID)))
            .andExpect(jsonPath("$.[*].productId").value(hasItem(DEFAULT_PRODUCT_ID)));
    }

    @Test
    @Transactional
    void getTransactionOperation() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get the transactionOperation
        restTransactionOperationMockMvc
            .perform(get(ENTITY_API_URL_ID, transactionOperation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(transactionOperation.getId().intValue()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.subsMsisdn").value(DEFAULT_SUBS_MSISDN))
            .andExpect(jsonPath("$.agentMsisdn").value(DEFAULT_AGENT_MSISDN))
            .andExpect(jsonPath("$.typeTransaction").value(DEFAULT_TYPE_TRANSACTION))
            .andExpect(jsonPath("$.createdAt").value(DEFAULT_CREATED_AT.toString()))
            .andExpect(jsonPath("$.transactionStatus").value(DEFAULT_TRANSACTION_STATUS))
            .andExpect(jsonPath("$.senderZone").value(DEFAULT_SENDER_ZONE))
            .andExpect(jsonPath("$.senderProfile").value(DEFAULT_SENDER_PROFILE))
            .andExpect(jsonPath("$.codeTerritory").value(DEFAULT_CODE_TERRITORY))
            .andExpect(jsonPath("$.subType").value(DEFAULT_SUB_TYPE))
            .andExpect(jsonPath("$.operationShortDate").value(DEFAULT_OPERATION_SHORT_DATE.toString()))
            .andExpect(jsonPath("$.operationDate").value(DEFAULT_OPERATION_DATE.toString()))
            .andExpect(jsonPath("$.isFraud").value(DEFAULT_IS_FRAUD.booleanValue()))
            .andExpect(jsonPath("$.taggedAt").value(DEFAULT_TAGGED_AT.toString()))
            .andExpect(jsonPath("$.fraudSource").value(DEFAULT_FRAUD_SOURCE))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT))
            .andExpect(jsonPath("$.falsePositiveDetectedAt").value(DEFAULT_FALSE_POSITIVE_DETECTED_AT.toString()))
            .andExpect(jsonPath("$.tid").value(DEFAULT_TID))
            .andExpect(jsonPath("$.parentMsisdn").value(DEFAULT_PARENT_MSISDN))
            .andExpect(jsonPath("$.fctDt").value(DEFAULT_FCT_DT))
            .andExpect(jsonPath("$.parentId").value(DEFAULT_PARENT_ID))
            .andExpect(jsonPath("$.canceledAt").value(DEFAULT_CANCELED_AT.toString()))
            .andExpect(jsonPath("$.canceledId").value(DEFAULT_CANCELED_ID))
            .andExpect(jsonPath("$.productId").value(DEFAULT_PRODUCT_ID));
    }

    @Test
    @Transactional
    void getTransactionOperationsByIdFiltering() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        Long id = transactionOperation.getId();

        defaultTransactionOperationShouldBeFound("id.equals=" + id);
        defaultTransactionOperationShouldNotBeFound("id.notEquals=" + id);

        defaultTransactionOperationShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultTransactionOperationShouldNotBeFound("id.greaterThan=" + id);

        defaultTransactionOperationShouldBeFound("id.lessThanOrEqual=" + id);
        defaultTransactionOperationShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where amount equals to DEFAULT_AMOUNT
        defaultTransactionOperationShouldBeFound("amount.equals=" + DEFAULT_AMOUNT);

        // Get all the transactionOperationList where amount equals to UPDATED_AMOUNT
        defaultTransactionOperationShouldNotBeFound("amount.equals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByAmountIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where amount not equals to DEFAULT_AMOUNT
        defaultTransactionOperationShouldNotBeFound("amount.notEquals=" + DEFAULT_AMOUNT);

        // Get all the transactionOperationList where amount not equals to UPDATED_AMOUNT
        defaultTransactionOperationShouldBeFound("amount.notEquals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByAmountIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where amount in DEFAULT_AMOUNT or UPDATED_AMOUNT
        defaultTransactionOperationShouldBeFound("amount.in=" + DEFAULT_AMOUNT + "," + UPDATED_AMOUNT);

        // Get all the transactionOperationList where amount equals to UPDATED_AMOUNT
        defaultTransactionOperationShouldNotBeFound("amount.in=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where amount is not null
        defaultTransactionOperationShouldBeFound("amount.specified=true");

        // Get all the transactionOperationList where amount is null
        defaultTransactionOperationShouldNotBeFound("amount.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByAmountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where amount is greater than or equal to DEFAULT_AMOUNT
        defaultTransactionOperationShouldBeFound("amount.greaterThanOrEqual=" + DEFAULT_AMOUNT);

        // Get all the transactionOperationList where amount is greater than or equal to UPDATED_AMOUNT
        defaultTransactionOperationShouldNotBeFound("amount.greaterThanOrEqual=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByAmountIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where amount is less than or equal to DEFAULT_AMOUNT
        defaultTransactionOperationShouldBeFound("amount.lessThanOrEqual=" + DEFAULT_AMOUNT);

        // Get all the transactionOperationList where amount is less than or equal to SMALLER_AMOUNT
        defaultTransactionOperationShouldNotBeFound("amount.lessThanOrEqual=" + SMALLER_AMOUNT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByAmountIsLessThanSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where amount is less than DEFAULT_AMOUNT
        defaultTransactionOperationShouldNotBeFound("amount.lessThan=" + DEFAULT_AMOUNT);

        // Get all the transactionOperationList where amount is less than UPDATED_AMOUNT
        defaultTransactionOperationShouldBeFound("amount.lessThan=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByAmountIsGreaterThanSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where amount is greater than DEFAULT_AMOUNT
        defaultTransactionOperationShouldNotBeFound("amount.greaterThan=" + DEFAULT_AMOUNT);

        // Get all the transactionOperationList where amount is greater than SMALLER_AMOUNT
        defaultTransactionOperationShouldBeFound("amount.greaterThan=" + SMALLER_AMOUNT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySubsMsisdnIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where subsMsisdn equals to DEFAULT_SUBS_MSISDN
        defaultTransactionOperationShouldBeFound("subsMsisdn.equals=" + DEFAULT_SUBS_MSISDN);

        // Get all the transactionOperationList where subsMsisdn equals to UPDATED_SUBS_MSISDN
        defaultTransactionOperationShouldNotBeFound("subsMsisdn.equals=" + UPDATED_SUBS_MSISDN);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySubsMsisdnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where subsMsisdn not equals to DEFAULT_SUBS_MSISDN
        defaultTransactionOperationShouldNotBeFound("subsMsisdn.notEquals=" + DEFAULT_SUBS_MSISDN);

        // Get all the transactionOperationList where subsMsisdn not equals to UPDATED_SUBS_MSISDN
        defaultTransactionOperationShouldBeFound("subsMsisdn.notEquals=" + UPDATED_SUBS_MSISDN);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySubsMsisdnIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where subsMsisdn in DEFAULT_SUBS_MSISDN or UPDATED_SUBS_MSISDN
        defaultTransactionOperationShouldBeFound("subsMsisdn.in=" + DEFAULT_SUBS_MSISDN + "," + UPDATED_SUBS_MSISDN);

        // Get all the transactionOperationList where subsMsisdn equals to UPDATED_SUBS_MSISDN
        defaultTransactionOperationShouldNotBeFound("subsMsisdn.in=" + UPDATED_SUBS_MSISDN);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySubsMsisdnIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where subsMsisdn is not null
        defaultTransactionOperationShouldBeFound("subsMsisdn.specified=true");

        // Get all the transactionOperationList where subsMsisdn is null
        defaultTransactionOperationShouldNotBeFound("subsMsisdn.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySubsMsisdnContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where subsMsisdn contains DEFAULT_SUBS_MSISDN
        defaultTransactionOperationShouldBeFound("subsMsisdn.contains=" + DEFAULT_SUBS_MSISDN);

        // Get all the transactionOperationList where subsMsisdn contains UPDATED_SUBS_MSISDN
        defaultTransactionOperationShouldNotBeFound("subsMsisdn.contains=" + UPDATED_SUBS_MSISDN);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySubsMsisdnNotContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where subsMsisdn does not contain DEFAULT_SUBS_MSISDN
        defaultTransactionOperationShouldNotBeFound("subsMsisdn.doesNotContain=" + DEFAULT_SUBS_MSISDN);

        // Get all the transactionOperationList where subsMsisdn does not contain UPDATED_SUBS_MSISDN
        defaultTransactionOperationShouldBeFound("subsMsisdn.doesNotContain=" + UPDATED_SUBS_MSISDN);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByAgentMsisdnIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where agentMsisdn equals to DEFAULT_AGENT_MSISDN
        defaultTransactionOperationShouldBeFound("agentMsisdn.equals=" + DEFAULT_AGENT_MSISDN);

        // Get all the transactionOperationList where agentMsisdn equals to UPDATED_AGENT_MSISDN
        defaultTransactionOperationShouldNotBeFound("agentMsisdn.equals=" + UPDATED_AGENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByAgentMsisdnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where agentMsisdn not equals to DEFAULT_AGENT_MSISDN
        defaultTransactionOperationShouldNotBeFound("agentMsisdn.notEquals=" + DEFAULT_AGENT_MSISDN);

        // Get all the transactionOperationList where agentMsisdn not equals to UPDATED_AGENT_MSISDN
        defaultTransactionOperationShouldBeFound("agentMsisdn.notEquals=" + UPDATED_AGENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByAgentMsisdnIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where agentMsisdn in DEFAULT_AGENT_MSISDN or UPDATED_AGENT_MSISDN
        defaultTransactionOperationShouldBeFound("agentMsisdn.in=" + DEFAULT_AGENT_MSISDN + "," + UPDATED_AGENT_MSISDN);

        // Get all the transactionOperationList where agentMsisdn equals to UPDATED_AGENT_MSISDN
        defaultTransactionOperationShouldNotBeFound("agentMsisdn.in=" + UPDATED_AGENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByAgentMsisdnIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where agentMsisdn is not null
        defaultTransactionOperationShouldBeFound("agentMsisdn.specified=true");

        // Get all the transactionOperationList where agentMsisdn is null
        defaultTransactionOperationShouldNotBeFound("agentMsisdn.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByAgentMsisdnContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where agentMsisdn contains DEFAULT_AGENT_MSISDN
        defaultTransactionOperationShouldBeFound("agentMsisdn.contains=" + DEFAULT_AGENT_MSISDN);

        // Get all the transactionOperationList where agentMsisdn contains UPDATED_AGENT_MSISDN
        defaultTransactionOperationShouldNotBeFound("agentMsisdn.contains=" + UPDATED_AGENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByAgentMsisdnNotContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where agentMsisdn does not contain DEFAULT_AGENT_MSISDN
        defaultTransactionOperationShouldNotBeFound("agentMsisdn.doesNotContain=" + DEFAULT_AGENT_MSISDN);

        // Get all the transactionOperationList where agentMsisdn does not contain UPDATED_AGENT_MSISDN
        defaultTransactionOperationShouldBeFound("agentMsisdn.doesNotContain=" + UPDATED_AGENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTypeTransactionIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where typeTransaction equals to DEFAULT_TYPE_TRANSACTION
        defaultTransactionOperationShouldBeFound("typeTransaction.equals=" + DEFAULT_TYPE_TRANSACTION);

        // Get all the transactionOperationList where typeTransaction equals to UPDATED_TYPE_TRANSACTION
        defaultTransactionOperationShouldNotBeFound("typeTransaction.equals=" + UPDATED_TYPE_TRANSACTION);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTypeTransactionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where typeTransaction not equals to DEFAULT_TYPE_TRANSACTION
        defaultTransactionOperationShouldNotBeFound("typeTransaction.notEquals=" + DEFAULT_TYPE_TRANSACTION);

        // Get all the transactionOperationList where typeTransaction not equals to UPDATED_TYPE_TRANSACTION
        defaultTransactionOperationShouldBeFound("typeTransaction.notEquals=" + UPDATED_TYPE_TRANSACTION);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTypeTransactionIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where typeTransaction in DEFAULT_TYPE_TRANSACTION or UPDATED_TYPE_TRANSACTION
        defaultTransactionOperationShouldBeFound("typeTransaction.in=" + DEFAULT_TYPE_TRANSACTION + "," + UPDATED_TYPE_TRANSACTION);

        // Get all the transactionOperationList where typeTransaction equals to UPDATED_TYPE_TRANSACTION
        defaultTransactionOperationShouldNotBeFound("typeTransaction.in=" + UPDATED_TYPE_TRANSACTION);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTypeTransactionIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where typeTransaction is not null
        defaultTransactionOperationShouldBeFound("typeTransaction.specified=true");

        // Get all the transactionOperationList where typeTransaction is null
        defaultTransactionOperationShouldNotBeFound("typeTransaction.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTypeTransactionContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where typeTransaction contains DEFAULT_TYPE_TRANSACTION
        defaultTransactionOperationShouldBeFound("typeTransaction.contains=" + DEFAULT_TYPE_TRANSACTION);

        // Get all the transactionOperationList where typeTransaction contains UPDATED_TYPE_TRANSACTION
        defaultTransactionOperationShouldNotBeFound("typeTransaction.contains=" + UPDATED_TYPE_TRANSACTION);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTypeTransactionNotContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where typeTransaction does not contain DEFAULT_TYPE_TRANSACTION
        defaultTransactionOperationShouldNotBeFound("typeTransaction.doesNotContain=" + DEFAULT_TYPE_TRANSACTION);

        // Get all the transactionOperationList where typeTransaction does not contain UPDATED_TYPE_TRANSACTION
        defaultTransactionOperationShouldBeFound("typeTransaction.doesNotContain=" + UPDATED_TYPE_TRANSACTION);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCreatedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where createdAt equals to DEFAULT_CREATED_AT
        defaultTransactionOperationShouldBeFound("createdAt.equals=" + DEFAULT_CREATED_AT);

        // Get all the transactionOperationList where createdAt equals to UPDATED_CREATED_AT
        defaultTransactionOperationShouldNotBeFound("createdAt.equals=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCreatedAtIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where createdAt not equals to DEFAULT_CREATED_AT
        defaultTransactionOperationShouldNotBeFound("createdAt.notEquals=" + DEFAULT_CREATED_AT);

        // Get all the transactionOperationList where createdAt not equals to UPDATED_CREATED_AT
        defaultTransactionOperationShouldBeFound("createdAt.notEquals=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCreatedAtIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where createdAt in DEFAULT_CREATED_AT or UPDATED_CREATED_AT
        defaultTransactionOperationShouldBeFound("createdAt.in=" + DEFAULT_CREATED_AT + "," + UPDATED_CREATED_AT);

        // Get all the transactionOperationList where createdAt equals to UPDATED_CREATED_AT
        defaultTransactionOperationShouldNotBeFound("createdAt.in=" + UPDATED_CREATED_AT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCreatedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where createdAt is not null
        defaultTransactionOperationShouldBeFound("createdAt.specified=true");

        // Get all the transactionOperationList where createdAt is null
        defaultTransactionOperationShouldNotBeFound("createdAt.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTransactionStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where transactionStatus equals to DEFAULT_TRANSACTION_STATUS
        defaultTransactionOperationShouldBeFound("transactionStatus.equals=" + DEFAULT_TRANSACTION_STATUS);

        // Get all the transactionOperationList where transactionStatus equals to UPDATED_TRANSACTION_STATUS
        defaultTransactionOperationShouldNotBeFound("transactionStatus.equals=" + UPDATED_TRANSACTION_STATUS);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTransactionStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where transactionStatus not equals to DEFAULT_TRANSACTION_STATUS
        defaultTransactionOperationShouldNotBeFound("transactionStatus.notEquals=" + DEFAULT_TRANSACTION_STATUS);

        // Get all the transactionOperationList where transactionStatus not equals to UPDATED_TRANSACTION_STATUS
        defaultTransactionOperationShouldBeFound("transactionStatus.notEquals=" + UPDATED_TRANSACTION_STATUS);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTransactionStatusIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where transactionStatus in DEFAULT_TRANSACTION_STATUS or UPDATED_TRANSACTION_STATUS
        defaultTransactionOperationShouldBeFound("transactionStatus.in=" + DEFAULT_TRANSACTION_STATUS + "," + UPDATED_TRANSACTION_STATUS);

        // Get all the transactionOperationList where transactionStatus equals to UPDATED_TRANSACTION_STATUS
        defaultTransactionOperationShouldNotBeFound("transactionStatus.in=" + UPDATED_TRANSACTION_STATUS);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTransactionStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where transactionStatus is not null
        defaultTransactionOperationShouldBeFound("transactionStatus.specified=true");

        // Get all the transactionOperationList where transactionStatus is null
        defaultTransactionOperationShouldNotBeFound("transactionStatus.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTransactionStatusContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where transactionStatus contains DEFAULT_TRANSACTION_STATUS
        defaultTransactionOperationShouldBeFound("transactionStatus.contains=" + DEFAULT_TRANSACTION_STATUS);

        // Get all the transactionOperationList where transactionStatus contains UPDATED_TRANSACTION_STATUS
        defaultTransactionOperationShouldNotBeFound("transactionStatus.contains=" + UPDATED_TRANSACTION_STATUS);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTransactionStatusNotContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where transactionStatus does not contain DEFAULT_TRANSACTION_STATUS
        defaultTransactionOperationShouldNotBeFound("transactionStatus.doesNotContain=" + DEFAULT_TRANSACTION_STATUS);

        // Get all the transactionOperationList where transactionStatus does not contain UPDATED_TRANSACTION_STATUS
        defaultTransactionOperationShouldBeFound("transactionStatus.doesNotContain=" + UPDATED_TRANSACTION_STATUS);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySenderZoneIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where senderZone equals to DEFAULT_SENDER_ZONE
        defaultTransactionOperationShouldBeFound("senderZone.equals=" + DEFAULT_SENDER_ZONE);

        // Get all the transactionOperationList where senderZone equals to UPDATED_SENDER_ZONE
        defaultTransactionOperationShouldNotBeFound("senderZone.equals=" + UPDATED_SENDER_ZONE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySenderZoneIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where senderZone not equals to DEFAULT_SENDER_ZONE
        defaultTransactionOperationShouldNotBeFound("senderZone.notEquals=" + DEFAULT_SENDER_ZONE);

        // Get all the transactionOperationList where senderZone not equals to UPDATED_SENDER_ZONE
        defaultTransactionOperationShouldBeFound("senderZone.notEquals=" + UPDATED_SENDER_ZONE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySenderZoneIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where senderZone in DEFAULT_SENDER_ZONE or UPDATED_SENDER_ZONE
        defaultTransactionOperationShouldBeFound("senderZone.in=" + DEFAULT_SENDER_ZONE + "," + UPDATED_SENDER_ZONE);

        // Get all the transactionOperationList where senderZone equals to UPDATED_SENDER_ZONE
        defaultTransactionOperationShouldNotBeFound("senderZone.in=" + UPDATED_SENDER_ZONE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySenderZoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where senderZone is not null
        defaultTransactionOperationShouldBeFound("senderZone.specified=true");

        // Get all the transactionOperationList where senderZone is null
        defaultTransactionOperationShouldNotBeFound("senderZone.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySenderZoneContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where senderZone contains DEFAULT_SENDER_ZONE
        defaultTransactionOperationShouldBeFound("senderZone.contains=" + DEFAULT_SENDER_ZONE);

        // Get all the transactionOperationList where senderZone contains UPDATED_SENDER_ZONE
        defaultTransactionOperationShouldNotBeFound("senderZone.contains=" + UPDATED_SENDER_ZONE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySenderZoneNotContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where senderZone does not contain DEFAULT_SENDER_ZONE
        defaultTransactionOperationShouldNotBeFound("senderZone.doesNotContain=" + DEFAULT_SENDER_ZONE);

        // Get all the transactionOperationList where senderZone does not contain UPDATED_SENDER_ZONE
        defaultTransactionOperationShouldBeFound("senderZone.doesNotContain=" + UPDATED_SENDER_ZONE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySenderProfileIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where senderProfile equals to DEFAULT_SENDER_PROFILE
        defaultTransactionOperationShouldBeFound("senderProfile.equals=" + DEFAULT_SENDER_PROFILE);

        // Get all the transactionOperationList where senderProfile equals to UPDATED_SENDER_PROFILE
        defaultTransactionOperationShouldNotBeFound("senderProfile.equals=" + UPDATED_SENDER_PROFILE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySenderProfileIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where senderProfile not equals to DEFAULT_SENDER_PROFILE
        defaultTransactionOperationShouldNotBeFound("senderProfile.notEquals=" + DEFAULT_SENDER_PROFILE);

        // Get all the transactionOperationList where senderProfile not equals to UPDATED_SENDER_PROFILE
        defaultTransactionOperationShouldBeFound("senderProfile.notEquals=" + UPDATED_SENDER_PROFILE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySenderProfileIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where senderProfile in DEFAULT_SENDER_PROFILE or UPDATED_SENDER_PROFILE
        defaultTransactionOperationShouldBeFound("senderProfile.in=" + DEFAULT_SENDER_PROFILE + "," + UPDATED_SENDER_PROFILE);

        // Get all the transactionOperationList where senderProfile equals to UPDATED_SENDER_PROFILE
        defaultTransactionOperationShouldNotBeFound("senderProfile.in=" + UPDATED_SENDER_PROFILE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySenderProfileIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where senderProfile is not null
        defaultTransactionOperationShouldBeFound("senderProfile.specified=true");

        // Get all the transactionOperationList where senderProfile is null
        defaultTransactionOperationShouldNotBeFound("senderProfile.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySenderProfileContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where senderProfile contains DEFAULT_SENDER_PROFILE
        defaultTransactionOperationShouldBeFound("senderProfile.contains=" + DEFAULT_SENDER_PROFILE);

        // Get all the transactionOperationList where senderProfile contains UPDATED_SENDER_PROFILE
        defaultTransactionOperationShouldNotBeFound("senderProfile.contains=" + UPDATED_SENDER_PROFILE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySenderProfileNotContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where senderProfile does not contain DEFAULT_SENDER_PROFILE
        defaultTransactionOperationShouldNotBeFound("senderProfile.doesNotContain=" + DEFAULT_SENDER_PROFILE);

        // Get all the transactionOperationList where senderProfile does not contain UPDATED_SENDER_PROFILE
        defaultTransactionOperationShouldBeFound("senderProfile.doesNotContain=" + UPDATED_SENDER_PROFILE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCodeTerritoryIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where codeTerritory equals to DEFAULT_CODE_TERRITORY
        defaultTransactionOperationShouldBeFound("codeTerritory.equals=" + DEFAULT_CODE_TERRITORY);

        // Get all the transactionOperationList where codeTerritory equals to UPDATED_CODE_TERRITORY
        defaultTransactionOperationShouldNotBeFound("codeTerritory.equals=" + UPDATED_CODE_TERRITORY);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCodeTerritoryIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where codeTerritory not equals to DEFAULT_CODE_TERRITORY
        defaultTransactionOperationShouldNotBeFound("codeTerritory.notEquals=" + DEFAULT_CODE_TERRITORY);

        // Get all the transactionOperationList where codeTerritory not equals to UPDATED_CODE_TERRITORY
        defaultTransactionOperationShouldBeFound("codeTerritory.notEquals=" + UPDATED_CODE_TERRITORY);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCodeTerritoryIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where codeTerritory in DEFAULT_CODE_TERRITORY or UPDATED_CODE_TERRITORY
        defaultTransactionOperationShouldBeFound("codeTerritory.in=" + DEFAULT_CODE_TERRITORY + "," + UPDATED_CODE_TERRITORY);

        // Get all the transactionOperationList where codeTerritory equals to UPDATED_CODE_TERRITORY
        defaultTransactionOperationShouldNotBeFound("codeTerritory.in=" + UPDATED_CODE_TERRITORY);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCodeTerritoryIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where codeTerritory is not null
        defaultTransactionOperationShouldBeFound("codeTerritory.specified=true");

        // Get all the transactionOperationList where codeTerritory is null
        defaultTransactionOperationShouldNotBeFound("codeTerritory.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCodeTerritoryContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where codeTerritory contains DEFAULT_CODE_TERRITORY
        defaultTransactionOperationShouldBeFound("codeTerritory.contains=" + DEFAULT_CODE_TERRITORY);

        // Get all the transactionOperationList where codeTerritory contains UPDATED_CODE_TERRITORY
        defaultTransactionOperationShouldNotBeFound("codeTerritory.contains=" + UPDATED_CODE_TERRITORY);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCodeTerritoryNotContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where codeTerritory does not contain DEFAULT_CODE_TERRITORY
        defaultTransactionOperationShouldNotBeFound("codeTerritory.doesNotContain=" + DEFAULT_CODE_TERRITORY);

        // Get all the transactionOperationList where codeTerritory does not contain UPDATED_CODE_TERRITORY
        defaultTransactionOperationShouldBeFound("codeTerritory.doesNotContain=" + UPDATED_CODE_TERRITORY);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySubTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where subType equals to DEFAULT_SUB_TYPE
        defaultTransactionOperationShouldBeFound("subType.equals=" + DEFAULT_SUB_TYPE);

        // Get all the transactionOperationList where subType equals to UPDATED_SUB_TYPE
        defaultTransactionOperationShouldNotBeFound("subType.equals=" + UPDATED_SUB_TYPE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySubTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where subType not equals to DEFAULT_SUB_TYPE
        defaultTransactionOperationShouldNotBeFound("subType.notEquals=" + DEFAULT_SUB_TYPE);

        // Get all the transactionOperationList where subType not equals to UPDATED_SUB_TYPE
        defaultTransactionOperationShouldBeFound("subType.notEquals=" + UPDATED_SUB_TYPE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySubTypeIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where subType in DEFAULT_SUB_TYPE or UPDATED_SUB_TYPE
        defaultTransactionOperationShouldBeFound("subType.in=" + DEFAULT_SUB_TYPE + "," + UPDATED_SUB_TYPE);

        // Get all the transactionOperationList where subType equals to UPDATED_SUB_TYPE
        defaultTransactionOperationShouldNotBeFound("subType.in=" + UPDATED_SUB_TYPE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySubTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where subType is not null
        defaultTransactionOperationShouldBeFound("subType.specified=true");

        // Get all the transactionOperationList where subType is null
        defaultTransactionOperationShouldNotBeFound("subType.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySubTypeContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where subType contains DEFAULT_SUB_TYPE
        defaultTransactionOperationShouldBeFound("subType.contains=" + DEFAULT_SUB_TYPE);

        // Get all the transactionOperationList where subType contains UPDATED_SUB_TYPE
        defaultTransactionOperationShouldNotBeFound("subType.contains=" + UPDATED_SUB_TYPE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsBySubTypeNotContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where subType does not contain DEFAULT_SUB_TYPE
        defaultTransactionOperationShouldNotBeFound("subType.doesNotContain=" + DEFAULT_SUB_TYPE);

        // Get all the transactionOperationList where subType does not contain UPDATED_SUB_TYPE
        defaultTransactionOperationShouldBeFound("subType.doesNotContain=" + UPDATED_SUB_TYPE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByOperationShortDateIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where operationShortDate equals to DEFAULT_OPERATION_SHORT_DATE
        defaultTransactionOperationShouldBeFound("operationShortDate.equals=" + DEFAULT_OPERATION_SHORT_DATE);

        // Get all the transactionOperationList where operationShortDate equals to UPDATED_OPERATION_SHORT_DATE
        defaultTransactionOperationShouldNotBeFound("operationShortDate.equals=" + UPDATED_OPERATION_SHORT_DATE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByOperationShortDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where operationShortDate not equals to DEFAULT_OPERATION_SHORT_DATE
        defaultTransactionOperationShouldNotBeFound("operationShortDate.notEquals=" + DEFAULT_OPERATION_SHORT_DATE);

        // Get all the transactionOperationList where operationShortDate not equals to UPDATED_OPERATION_SHORT_DATE
        defaultTransactionOperationShouldBeFound("operationShortDate.notEquals=" + UPDATED_OPERATION_SHORT_DATE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByOperationShortDateIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where operationShortDate in DEFAULT_OPERATION_SHORT_DATE or UPDATED_OPERATION_SHORT_DATE
        defaultTransactionOperationShouldBeFound(
            "operationShortDate.in=" + DEFAULT_OPERATION_SHORT_DATE + "," + UPDATED_OPERATION_SHORT_DATE
        );

        // Get all the transactionOperationList where operationShortDate equals to UPDATED_OPERATION_SHORT_DATE
        defaultTransactionOperationShouldNotBeFound("operationShortDate.in=" + UPDATED_OPERATION_SHORT_DATE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByOperationShortDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where operationShortDate is not null
        defaultTransactionOperationShouldBeFound("operationShortDate.specified=true");

        // Get all the transactionOperationList where operationShortDate is null
        defaultTransactionOperationShouldNotBeFound("operationShortDate.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByOperationShortDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where operationShortDate is greater than or equal to DEFAULT_OPERATION_SHORT_DATE
        defaultTransactionOperationShouldBeFound("operationShortDate.greaterThanOrEqual=" + DEFAULT_OPERATION_SHORT_DATE);

        // Get all the transactionOperationList where operationShortDate is greater than or equal to UPDATED_OPERATION_SHORT_DATE
        defaultTransactionOperationShouldNotBeFound("operationShortDate.greaterThanOrEqual=" + UPDATED_OPERATION_SHORT_DATE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByOperationShortDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where operationShortDate is less than or equal to DEFAULT_OPERATION_SHORT_DATE
        defaultTransactionOperationShouldBeFound("operationShortDate.lessThanOrEqual=" + DEFAULT_OPERATION_SHORT_DATE);

        // Get all the transactionOperationList where operationShortDate is less than or equal to SMALLER_OPERATION_SHORT_DATE
        defaultTransactionOperationShouldNotBeFound("operationShortDate.lessThanOrEqual=" + SMALLER_OPERATION_SHORT_DATE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByOperationShortDateIsLessThanSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where operationShortDate is less than DEFAULT_OPERATION_SHORT_DATE
        defaultTransactionOperationShouldNotBeFound("operationShortDate.lessThan=" + DEFAULT_OPERATION_SHORT_DATE);

        // Get all the transactionOperationList where operationShortDate is less than UPDATED_OPERATION_SHORT_DATE
        defaultTransactionOperationShouldBeFound("operationShortDate.lessThan=" + UPDATED_OPERATION_SHORT_DATE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByOperationShortDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where operationShortDate is greater than DEFAULT_OPERATION_SHORT_DATE
        defaultTransactionOperationShouldNotBeFound("operationShortDate.greaterThan=" + DEFAULT_OPERATION_SHORT_DATE);

        // Get all the transactionOperationList where operationShortDate is greater than SMALLER_OPERATION_SHORT_DATE
        defaultTransactionOperationShouldBeFound("operationShortDate.greaterThan=" + SMALLER_OPERATION_SHORT_DATE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByOperationDateIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where operationDate equals to DEFAULT_OPERATION_DATE
        defaultTransactionOperationShouldBeFound("operationDate.equals=" + DEFAULT_OPERATION_DATE);

        // Get all the transactionOperationList where operationDate equals to UPDATED_OPERATION_DATE
        defaultTransactionOperationShouldNotBeFound("operationDate.equals=" + UPDATED_OPERATION_DATE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByOperationDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where operationDate not equals to DEFAULT_OPERATION_DATE
        defaultTransactionOperationShouldNotBeFound("operationDate.notEquals=" + DEFAULT_OPERATION_DATE);

        // Get all the transactionOperationList where operationDate not equals to UPDATED_OPERATION_DATE
        defaultTransactionOperationShouldBeFound("operationDate.notEquals=" + UPDATED_OPERATION_DATE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByOperationDateIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where operationDate in DEFAULT_OPERATION_DATE or UPDATED_OPERATION_DATE
        defaultTransactionOperationShouldBeFound("operationDate.in=" + DEFAULT_OPERATION_DATE + "," + UPDATED_OPERATION_DATE);

        // Get all the transactionOperationList where operationDate equals to UPDATED_OPERATION_DATE
        defaultTransactionOperationShouldNotBeFound("operationDate.in=" + UPDATED_OPERATION_DATE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByOperationDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where operationDate is not null
        defaultTransactionOperationShouldBeFound("operationDate.specified=true");

        // Get all the transactionOperationList where operationDate is null
        defaultTransactionOperationShouldNotBeFound("operationDate.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByIsFraudIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where isFraud equals to DEFAULT_IS_FRAUD
        defaultTransactionOperationShouldBeFound("isFraud.equals=" + DEFAULT_IS_FRAUD);

        // Get all the transactionOperationList where isFraud equals to UPDATED_IS_FRAUD
        defaultTransactionOperationShouldNotBeFound("isFraud.equals=" + UPDATED_IS_FRAUD);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByIsFraudIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where isFraud not equals to DEFAULT_IS_FRAUD
        defaultTransactionOperationShouldNotBeFound("isFraud.notEquals=" + DEFAULT_IS_FRAUD);

        // Get all the transactionOperationList where isFraud not equals to UPDATED_IS_FRAUD
        defaultTransactionOperationShouldBeFound("isFraud.notEquals=" + UPDATED_IS_FRAUD);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByIsFraudIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where isFraud in DEFAULT_IS_FRAUD or UPDATED_IS_FRAUD
        defaultTransactionOperationShouldBeFound("isFraud.in=" + DEFAULT_IS_FRAUD + "," + UPDATED_IS_FRAUD);

        // Get all the transactionOperationList where isFraud equals to UPDATED_IS_FRAUD
        defaultTransactionOperationShouldNotBeFound("isFraud.in=" + UPDATED_IS_FRAUD);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByIsFraudIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where isFraud is not null
        defaultTransactionOperationShouldBeFound("isFraud.specified=true");

        // Get all the transactionOperationList where isFraud is null
        defaultTransactionOperationShouldNotBeFound("isFraud.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTaggedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where taggedAt equals to DEFAULT_TAGGED_AT
        defaultTransactionOperationShouldBeFound("taggedAt.equals=" + DEFAULT_TAGGED_AT);

        // Get all the transactionOperationList where taggedAt equals to UPDATED_TAGGED_AT
        defaultTransactionOperationShouldNotBeFound("taggedAt.equals=" + UPDATED_TAGGED_AT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTaggedAtIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where taggedAt not equals to DEFAULT_TAGGED_AT
        defaultTransactionOperationShouldNotBeFound("taggedAt.notEquals=" + DEFAULT_TAGGED_AT);

        // Get all the transactionOperationList where taggedAt not equals to UPDATED_TAGGED_AT
        defaultTransactionOperationShouldBeFound("taggedAt.notEquals=" + UPDATED_TAGGED_AT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTaggedAtIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where taggedAt in DEFAULT_TAGGED_AT or UPDATED_TAGGED_AT
        defaultTransactionOperationShouldBeFound("taggedAt.in=" + DEFAULT_TAGGED_AT + "," + UPDATED_TAGGED_AT);

        // Get all the transactionOperationList where taggedAt equals to UPDATED_TAGGED_AT
        defaultTransactionOperationShouldNotBeFound("taggedAt.in=" + UPDATED_TAGGED_AT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTaggedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where taggedAt is not null
        defaultTransactionOperationShouldBeFound("taggedAt.specified=true");

        // Get all the transactionOperationList where taggedAt is null
        defaultTransactionOperationShouldNotBeFound("taggedAt.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByFraudSourceIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where fraudSource equals to DEFAULT_FRAUD_SOURCE
        defaultTransactionOperationShouldBeFound("fraudSource.equals=" + DEFAULT_FRAUD_SOURCE);

        // Get all the transactionOperationList where fraudSource equals to UPDATED_FRAUD_SOURCE
        defaultTransactionOperationShouldNotBeFound("fraudSource.equals=" + UPDATED_FRAUD_SOURCE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByFraudSourceIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where fraudSource not equals to DEFAULT_FRAUD_SOURCE
        defaultTransactionOperationShouldNotBeFound("fraudSource.notEquals=" + DEFAULT_FRAUD_SOURCE);

        // Get all the transactionOperationList where fraudSource not equals to UPDATED_FRAUD_SOURCE
        defaultTransactionOperationShouldBeFound("fraudSource.notEquals=" + UPDATED_FRAUD_SOURCE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByFraudSourceIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where fraudSource in DEFAULT_FRAUD_SOURCE or UPDATED_FRAUD_SOURCE
        defaultTransactionOperationShouldBeFound("fraudSource.in=" + DEFAULT_FRAUD_SOURCE + "," + UPDATED_FRAUD_SOURCE);

        // Get all the transactionOperationList where fraudSource equals to UPDATED_FRAUD_SOURCE
        defaultTransactionOperationShouldNotBeFound("fraudSource.in=" + UPDATED_FRAUD_SOURCE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByFraudSourceIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where fraudSource is not null
        defaultTransactionOperationShouldBeFound("fraudSource.specified=true");

        // Get all the transactionOperationList where fraudSource is null
        defaultTransactionOperationShouldNotBeFound("fraudSource.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByFraudSourceContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where fraudSource contains DEFAULT_FRAUD_SOURCE
        defaultTransactionOperationShouldBeFound("fraudSource.contains=" + DEFAULT_FRAUD_SOURCE);

        // Get all the transactionOperationList where fraudSource contains UPDATED_FRAUD_SOURCE
        defaultTransactionOperationShouldNotBeFound("fraudSource.contains=" + UPDATED_FRAUD_SOURCE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByFraudSourceNotContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where fraudSource does not contain DEFAULT_FRAUD_SOURCE
        defaultTransactionOperationShouldNotBeFound("fraudSource.doesNotContain=" + DEFAULT_FRAUD_SOURCE);

        // Get all the transactionOperationList where fraudSource does not contain UPDATED_FRAUD_SOURCE
        defaultTransactionOperationShouldBeFound("fraudSource.doesNotContain=" + UPDATED_FRAUD_SOURCE);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCommentIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where comment equals to DEFAULT_COMMENT
        defaultTransactionOperationShouldBeFound("comment.equals=" + DEFAULT_COMMENT);

        // Get all the transactionOperationList where comment equals to UPDATED_COMMENT
        defaultTransactionOperationShouldNotBeFound("comment.equals=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCommentIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where comment not equals to DEFAULT_COMMENT
        defaultTransactionOperationShouldNotBeFound("comment.notEquals=" + DEFAULT_COMMENT);

        // Get all the transactionOperationList where comment not equals to UPDATED_COMMENT
        defaultTransactionOperationShouldBeFound("comment.notEquals=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCommentIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where comment in DEFAULT_COMMENT or UPDATED_COMMENT
        defaultTransactionOperationShouldBeFound("comment.in=" + DEFAULT_COMMENT + "," + UPDATED_COMMENT);

        // Get all the transactionOperationList where comment equals to UPDATED_COMMENT
        defaultTransactionOperationShouldNotBeFound("comment.in=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCommentIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where comment is not null
        defaultTransactionOperationShouldBeFound("comment.specified=true");

        // Get all the transactionOperationList where comment is null
        defaultTransactionOperationShouldNotBeFound("comment.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCommentContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where comment contains DEFAULT_COMMENT
        defaultTransactionOperationShouldBeFound("comment.contains=" + DEFAULT_COMMENT);

        // Get all the transactionOperationList where comment contains UPDATED_COMMENT
        defaultTransactionOperationShouldNotBeFound("comment.contains=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCommentNotContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where comment does not contain DEFAULT_COMMENT
        defaultTransactionOperationShouldNotBeFound("comment.doesNotContain=" + DEFAULT_COMMENT);

        // Get all the transactionOperationList where comment does not contain UPDATED_COMMENT
        defaultTransactionOperationShouldBeFound("comment.doesNotContain=" + UPDATED_COMMENT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByFalsePositiveDetectedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where falsePositiveDetectedAt equals to DEFAULT_FALSE_POSITIVE_DETECTED_AT
        defaultTransactionOperationShouldBeFound("falsePositiveDetectedAt.equals=" + DEFAULT_FALSE_POSITIVE_DETECTED_AT);

        // Get all the transactionOperationList where falsePositiveDetectedAt equals to UPDATED_FALSE_POSITIVE_DETECTED_AT
        defaultTransactionOperationShouldNotBeFound("falsePositiveDetectedAt.equals=" + UPDATED_FALSE_POSITIVE_DETECTED_AT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByFalsePositiveDetectedAtIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where falsePositiveDetectedAt not equals to DEFAULT_FALSE_POSITIVE_DETECTED_AT
        defaultTransactionOperationShouldNotBeFound("falsePositiveDetectedAt.notEquals=" + DEFAULT_FALSE_POSITIVE_DETECTED_AT);

        // Get all the transactionOperationList where falsePositiveDetectedAt not equals to UPDATED_FALSE_POSITIVE_DETECTED_AT
        defaultTransactionOperationShouldBeFound("falsePositiveDetectedAt.notEquals=" + UPDATED_FALSE_POSITIVE_DETECTED_AT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByFalsePositiveDetectedAtIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where falsePositiveDetectedAt in DEFAULT_FALSE_POSITIVE_DETECTED_AT or UPDATED_FALSE_POSITIVE_DETECTED_AT
        defaultTransactionOperationShouldBeFound(
            "falsePositiveDetectedAt.in=" + DEFAULT_FALSE_POSITIVE_DETECTED_AT + "," + UPDATED_FALSE_POSITIVE_DETECTED_AT
        );

        // Get all the transactionOperationList where falsePositiveDetectedAt equals to UPDATED_FALSE_POSITIVE_DETECTED_AT
        defaultTransactionOperationShouldNotBeFound("falsePositiveDetectedAt.in=" + UPDATED_FALSE_POSITIVE_DETECTED_AT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByFalsePositiveDetectedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where falsePositiveDetectedAt is not null
        defaultTransactionOperationShouldBeFound("falsePositiveDetectedAt.specified=true");

        // Get all the transactionOperationList where falsePositiveDetectedAt is null
        defaultTransactionOperationShouldNotBeFound("falsePositiveDetectedAt.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTidIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where tid equals to DEFAULT_TID
        defaultTransactionOperationShouldBeFound("tid.equals=" + DEFAULT_TID);

        // Get all the transactionOperationList where tid equals to UPDATED_TID
        defaultTransactionOperationShouldNotBeFound("tid.equals=" + UPDATED_TID);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTidIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where tid not equals to DEFAULT_TID
        defaultTransactionOperationShouldNotBeFound("tid.notEquals=" + DEFAULT_TID);

        // Get all the transactionOperationList where tid not equals to UPDATED_TID
        defaultTransactionOperationShouldBeFound("tid.notEquals=" + UPDATED_TID);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTidIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where tid in DEFAULT_TID or UPDATED_TID
        defaultTransactionOperationShouldBeFound("tid.in=" + DEFAULT_TID + "," + UPDATED_TID);

        // Get all the transactionOperationList where tid equals to UPDATED_TID
        defaultTransactionOperationShouldNotBeFound("tid.in=" + UPDATED_TID);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTidIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where tid is not null
        defaultTransactionOperationShouldBeFound("tid.specified=true");

        // Get all the transactionOperationList where tid is null
        defaultTransactionOperationShouldNotBeFound("tid.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTidContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where tid contains DEFAULT_TID
        defaultTransactionOperationShouldBeFound("tid.contains=" + DEFAULT_TID);

        // Get all the transactionOperationList where tid contains UPDATED_TID
        defaultTransactionOperationShouldNotBeFound("tid.contains=" + UPDATED_TID);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByTidNotContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where tid does not contain DEFAULT_TID
        defaultTransactionOperationShouldNotBeFound("tid.doesNotContain=" + DEFAULT_TID);

        // Get all the transactionOperationList where tid does not contain UPDATED_TID
        defaultTransactionOperationShouldBeFound("tid.doesNotContain=" + UPDATED_TID);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByParentMsisdnIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where parentMsisdn equals to DEFAULT_PARENT_MSISDN
        defaultTransactionOperationShouldBeFound("parentMsisdn.equals=" + DEFAULT_PARENT_MSISDN);

        // Get all the transactionOperationList where parentMsisdn equals to UPDATED_PARENT_MSISDN
        defaultTransactionOperationShouldNotBeFound("parentMsisdn.equals=" + UPDATED_PARENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByParentMsisdnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where parentMsisdn not equals to DEFAULT_PARENT_MSISDN
        defaultTransactionOperationShouldNotBeFound("parentMsisdn.notEquals=" + DEFAULT_PARENT_MSISDN);

        // Get all the transactionOperationList where parentMsisdn not equals to UPDATED_PARENT_MSISDN
        defaultTransactionOperationShouldBeFound("parentMsisdn.notEquals=" + UPDATED_PARENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByParentMsisdnIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where parentMsisdn in DEFAULT_PARENT_MSISDN or UPDATED_PARENT_MSISDN
        defaultTransactionOperationShouldBeFound("parentMsisdn.in=" + DEFAULT_PARENT_MSISDN + "," + UPDATED_PARENT_MSISDN);

        // Get all the transactionOperationList where parentMsisdn equals to UPDATED_PARENT_MSISDN
        defaultTransactionOperationShouldNotBeFound("parentMsisdn.in=" + UPDATED_PARENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByParentMsisdnIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where parentMsisdn is not null
        defaultTransactionOperationShouldBeFound("parentMsisdn.specified=true");

        // Get all the transactionOperationList where parentMsisdn is null
        defaultTransactionOperationShouldNotBeFound("parentMsisdn.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByParentMsisdnContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where parentMsisdn contains DEFAULT_PARENT_MSISDN
        defaultTransactionOperationShouldBeFound("parentMsisdn.contains=" + DEFAULT_PARENT_MSISDN);

        // Get all the transactionOperationList where parentMsisdn contains UPDATED_PARENT_MSISDN
        defaultTransactionOperationShouldNotBeFound("parentMsisdn.contains=" + UPDATED_PARENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByParentMsisdnNotContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where parentMsisdn does not contain DEFAULT_PARENT_MSISDN
        defaultTransactionOperationShouldNotBeFound("parentMsisdn.doesNotContain=" + DEFAULT_PARENT_MSISDN);

        // Get all the transactionOperationList where parentMsisdn does not contain UPDATED_PARENT_MSISDN
        defaultTransactionOperationShouldBeFound("parentMsisdn.doesNotContain=" + UPDATED_PARENT_MSISDN);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByFctDtIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where fctDt equals to DEFAULT_FCT_DT
        defaultTransactionOperationShouldBeFound("fctDt.equals=" + DEFAULT_FCT_DT);

        // Get all the transactionOperationList where fctDt equals to UPDATED_FCT_DT
        defaultTransactionOperationShouldNotBeFound("fctDt.equals=" + UPDATED_FCT_DT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByFctDtIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where fctDt not equals to DEFAULT_FCT_DT
        defaultTransactionOperationShouldNotBeFound("fctDt.notEquals=" + DEFAULT_FCT_DT);

        // Get all the transactionOperationList where fctDt not equals to UPDATED_FCT_DT
        defaultTransactionOperationShouldBeFound("fctDt.notEquals=" + UPDATED_FCT_DT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByFctDtIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where fctDt in DEFAULT_FCT_DT or UPDATED_FCT_DT
        defaultTransactionOperationShouldBeFound("fctDt.in=" + DEFAULT_FCT_DT + "," + UPDATED_FCT_DT);

        // Get all the transactionOperationList where fctDt equals to UPDATED_FCT_DT
        defaultTransactionOperationShouldNotBeFound("fctDt.in=" + UPDATED_FCT_DT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByFctDtIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where fctDt is not null
        defaultTransactionOperationShouldBeFound("fctDt.specified=true");

        // Get all the transactionOperationList where fctDt is null
        defaultTransactionOperationShouldNotBeFound("fctDt.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByFctDtContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where fctDt contains DEFAULT_FCT_DT
        defaultTransactionOperationShouldBeFound("fctDt.contains=" + DEFAULT_FCT_DT);

        // Get all the transactionOperationList where fctDt contains UPDATED_FCT_DT
        defaultTransactionOperationShouldNotBeFound("fctDt.contains=" + UPDATED_FCT_DT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByFctDtNotContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where fctDt does not contain DEFAULT_FCT_DT
        defaultTransactionOperationShouldNotBeFound("fctDt.doesNotContain=" + DEFAULT_FCT_DT);

        // Get all the transactionOperationList where fctDt does not contain UPDATED_FCT_DT
        defaultTransactionOperationShouldBeFound("fctDt.doesNotContain=" + UPDATED_FCT_DT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByParentIdIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where parentId equals to DEFAULT_PARENT_ID
        defaultTransactionOperationShouldBeFound("parentId.equals=" + DEFAULT_PARENT_ID);

        // Get all the transactionOperationList where parentId equals to UPDATED_PARENT_ID
        defaultTransactionOperationShouldNotBeFound("parentId.equals=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByParentIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where parentId not equals to DEFAULT_PARENT_ID
        defaultTransactionOperationShouldNotBeFound("parentId.notEquals=" + DEFAULT_PARENT_ID);

        // Get all the transactionOperationList where parentId not equals to UPDATED_PARENT_ID
        defaultTransactionOperationShouldBeFound("parentId.notEquals=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByParentIdIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where parentId in DEFAULT_PARENT_ID or UPDATED_PARENT_ID
        defaultTransactionOperationShouldBeFound("parentId.in=" + DEFAULT_PARENT_ID + "," + UPDATED_PARENT_ID);

        // Get all the transactionOperationList where parentId equals to UPDATED_PARENT_ID
        defaultTransactionOperationShouldNotBeFound("parentId.in=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByParentIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where parentId is not null
        defaultTransactionOperationShouldBeFound("parentId.specified=true");

        // Get all the transactionOperationList where parentId is null
        defaultTransactionOperationShouldNotBeFound("parentId.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByParentIdContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where parentId contains DEFAULT_PARENT_ID
        defaultTransactionOperationShouldBeFound("parentId.contains=" + DEFAULT_PARENT_ID);

        // Get all the transactionOperationList where parentId contains UPDATED_PARENT_ID
        defaultTransactionOperationShouldNotBeFound("parentId.contains=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByParentIdNotContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where parentId does not contain DEFAULT_PARENT_ID
        defaultTransactionOperationShouldNotBeFound("parentId.doesNotContain=" + DEFAULT_PARENT_ID);

        // Get all the transactionOperationList where parentId does not contain UPDATED_PARENT_ID
        defaultTransactionOperationShouldBeFound("parentId.doesNotContain=" + UPDATED_PARENT_ID);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCanceledAtIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where canceledAt equals to DEFAULT_CANCELED_AT
        defaultTransactionOperationShouldBeFound("canceledAt.equals=" + DEFAULT_CANCELED_AT);

        // Get all the transactionOperationList where canceledAt equals to UPDATED_CANCELED_AT
        defaultTransactionOperationShouldNotBeFound("canceledAt.equals=" + UPDATED_CANCELED_AT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCanceledAtIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where canceledAt not equals to DEFAULT_CANCELED_AT
        defaultTransactionOperationShouldNotBeFound("canceledAt.notEquals=" + DEFAULT_CANCELED_AT);

        // Get all the transactionOperationList where canceledAt not equals to UPDATED_CANCELED_AT
        defaultTransactionOperationShouldBeFound("canceledAt.notEquals=" + UPDATED_CANCELED_AT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCanceledAtIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where canceledAt in DEFAULT_CANCELED_AT or UPDATED_CANCELED_AT
        defaultTransactionOperationShouldBeFound("canceledAt.in=" + DEFAULT_CANCELED_AT + "," + UPDATED_CANCELED_AT);

        // Get all the transactionOperationList where canceledAt equals to UPDATED_CANCELED_AT
        defaultTransactionOperationShouldNotBeFound("canceledAt.in=" + UPDATED_CANCELED_AT);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCanceledAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where canceledAt is not null
        defaultTransactionOperationShouldBeFound("canceledAt.specified=true");

        // Get all the transactionOperationList where canceledAt is null
        defaultTransactionOperationShouldNotBeFound("canceledAt.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCanceledIdIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where canceledId equals to DEFAULT_CANCELED_ID
        defaultTransactionOperationShouldBeFound("canceledId.equals=" + DEFAULT_CANCELED_ID);

        // Get all the transactionOperationList where canceledId equals to UPDATED_CANCELED_ID
        defaultTransactionOperationShouldNotBeFound("canceledId.equals=" + UPDATED_CANCELED_ID);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCanceledIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where canceledId not equals to DEFAULT_CANCELED_ID
        defaultTransactionOperationShouldNotBeFound("canceledId.notEquals=" + DEFAULT_CANCELED_ID);

        // Get all the transactionOperationList where canceledId not equals to UPDATED_CANCELED_ID
        defaultTransactionOperationShouldBeFound("canceledId.notEquals=" + UPDATED_CANCELED_ID);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCanceledIdIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where canceledId in DEFAULT_CANCELED_ID or UPDATED_CANCELED_ID
        defaultTransactionOperationShouldBeFound("canceledId.in=" + DEFAULT_CANCELED_ID + "," + UPDATED_CANCELED_ID);

        // Get all the transactionOperationList where canceledId equals to UPDATED_CANCELED_ID
        defaultTransactionOperationShouldNotBeFound("canceledId.in=" + UPDATED_CANCELED_ID);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCanceledIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where canceledId is not null
        defaultTransactionOperationShouldBeFound("canceledId.specified=true");

        // Get all the transactionOperationList where canceledId is null
        defaultTransactionOperationShouldNotBeFound("canceledId.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCanceledIdContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where canceledId contains DEFAULT_CANCELED_ID
        defaultTransactionOperationShouldBeFound("canceledId.contains=" + DEFAULT_CANCELED_ID);

        // Get all the transactionOperationList where canceledId contains UPDATED_CANCELED_ID
        defaultTransactionOperationShouldNotBeFound("canceledId.contains=" + UPDATED_CANCELED_ID);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCanceledIdNotContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where canceledId does not contain DEFAULT_CANCELED_ID
        defaultTransactionOperationShouldNotBeFound("canceledId.doesNotContain=" + DEFAULT_CANCELED_ID);

        // Get all the transactionOperationList where canceledId does not contain UPDATED_CANCELED_ID
        defaultTransactionOperationShouldBeFound("canceledId.doesNotContain=" + UPDATED_CANCELED_ID);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByProductIdIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where productId equals to DEFAULT_PRODUCT_ID
        defaultTransactionOperationShouldBeFound("productId.equals=" + DEFAULT_PRODUCT_ID);

        // Get all the transactionOperationList where productId equals to UPDATED_PRODUCT_ID
        defaultTransactionOperationShouldNotBeFound("productId.equals=" + UPDATED_PRODUCT_ID);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByProductIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where productId not equals to DEFAULT_PRODUCT_ID
        defaultTransactionOperationShouldNotBeFound("productId.notEquals=" + DEFAULT_PRODUCT_ID);

        // Get all the transactionOperationList where productId not equals to UPDATED_PRODUCT_ID
        defaultTransactionOperationShouldBeFound("productId.notEquals=" + UPDATED_PRODUCT_ID);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByProductIdIsInShouldWork() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where productId in DEFAULT_PRODUCT_ID or UPDATED_PRODUCT_ID
        defaultTransactionOperationShouldBeFound("productId.in=" + DEFAULT_PRODUCT_ID + "," + UPDATED_PRODUCT_ID);

        // Get all the transactionOperationList where productId equals to UPDATED_PRODUCT_ID
        defaultTransactionOperationShouldNotBeFound("productId.in=" + UPDATED_PRODUCT_ID);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByProductIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where productId is not null
        defaultTransactionOperationShouldBeFound("productId.specified=true");

        // Get all the transactionOperationList where productId is null
        defaultTransactionOperationShouldNotBeFound("productId.specified=false");
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByProductIdContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where productId contains DEFAULT_PRODUCT_ID
        defaultTransactionOperationShouldBeFound("productId.contains=" + DEFAULT_PRODUCT_ID);

        // Get all the transactionOperationList where productId contains UPDATED_PRODUCT_ID
        defaultTransactionOperationShouldNotBeFound("productId.contains=" + UPDATED_PRODUCT_ID);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByProductIdNotContainsSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        // Get all the transactionOperationList where productId does not contain DEFAULT_PRODUCT_ID
        defaultTransactionOperationShouldNotBeFound("productId.doesNotContain=" + DEFAULT_PRODUCT_ID);

        // Get all the transactionOperationList where productId does not contain UPDATED_PRODUCT_ID
        defaultTransactionOperationShouldBeFound("productId.doesNotContain=" + UPDATED_PRODUCT_ID);
    }

    @Test
    @Transactional
    void getAllTransactionOperationsByCommissionsIsEqualToSomething() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);
        Commission commissions;
        if (TestUtil.findAll(em, Commission.class).isEmpty()) {
            commissions = CommissionResourceIT.createEntity(em);
            em.persist(commissions);
            em.flush();
        } else {
            commissions = TestUtil.findAll(em, Commission.class).get(0);
        }
        em.persist(commissions);
        em.flush();
        transactionOperation.addCommissions(commissions);
        transactionOperationRepository.saveAndFlush(transactionOperation);
        Long commissionsId = commissions.getId();

        // Get all the transactionOperationList where commissions equals to commissionsId
        defaultTransactionOperationShouldBeFound("commissionsId.equals=" + commissionsId);

        // Get all the transactionOperationList where commissions equals to (commissionsId + 1)
        defaultTransactionOperationShouldNotBeFound("commissionsId.equals=" + (commissionsId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTransactionOperationShouldBeFound(String filter) throws Exception {
        restTransactionOperationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(transactionOperation.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].subsMsisdn").value(hasItem(DEFAULT_SUBS_MSISDN)))
            .andExpect(jsonPath("$.[*].agentMsisdn").value(hasItem(DEFAULT_AGENT_MSISDN)))
            .andExpect(jsonPath("$.[*].typeTransaction").value(hasItem(DEFAULT_TYPE_TRANSACTION)))
            .andExpect(jsonPath("$.[*].createdAt").value(hasItem(DEFAULT_CREATED_AT.toString())))
            .andExpect(jsonPath("$.[*].transactionStatus").value(hasItem(DEFAULT_TRANSACTION_STATUS)))
            .andExpect(jsonPath("$.[*].senderZone").value(hasItem(DEFAULT_SENDER_ZONE)))
            .andExpect(jsonPath("$.[*].senderProfile").value(hasItem(DEFAULT_SENDER_PROFILE)))
            .andExpect(jsonPath("$.[*].codeTerritory").value(hasItem(DEFAULT_CODE_TERRITORY)))
            .andExpect(jsonPath("$.[*].subType").value(hasItem(DEFAULT_SUB_TYPE)))
            .andExpect(jsonPath("$.[*].operationShortDate").value(hasItem(DEFAULT_OPERATION_SHORT_DATE.toString())))
            .andExpect(jsonPath("$.[*].operationDate").value(hasItem(DEFAULT_OPERATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].isFraud").value(hasItem(DEFAULT_IS_FRAUD.booleanValue())))
            .andExpect(jsonPath("$.[*].taggedAt").value(hasItem(DEFAULT_TAGGED_AT.toString())))
            .andExpect(jsonPath("$.[*].fraudSource").value(hasItem(DEFAULT_FRAUD_SOURCE)))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT)))
            .andExpect(jsonPath("$.[*].falsePositiveDetectedAt").value(hasItem(DEFAULT_FALSE_POSITIVE_DETECTED_AT.toString())))
            .andExpect(jsonPath("$.[*].tid").value(hasItem(DEFAULT_TID)))
            .andExpect(jsonPath("$.[*].parentMsisdn").value(hasItem(DEFAULT_PARENT_MSISDN)))
            .andExpect(jsonPath("$.[*].fctDt").value(hasItem(DEFAULT_FCT_DT)))
            .andExpect(jsonPath("$.[*].parentId").value(hasItem(DEFAULT_PARENT_ID)))
            .andExpect(jsonPath("$.[*].canceledAt").value(hasItem(DEFAULT_CANCELED_AT.toString())))
            .andExpect(jsonPath("$.[*].canceledId").value(hasItem(DEFAULT_CANCELED_ID)))
            .andExpect(jsonPath("$.[*].productId").value(hasItem(DEFAULT_PRODUCT_ID)));

        // Check, that the count call also returns 1
        restTransactionOperationMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTransactionOperationShouldNotBeFound(String filter) throws Exception {
        restTransactionOperationMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTransactionOperationMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingTransactionOperation() throws Exception {
        // Get the transactionOperation
        restTransactionOperationMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewTransactionOperation() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        int databaseSizeBeforeUpdate = transactionOperationRepository.findAll().size();

        // Update the transactionOperation
        TransactionOperation updatedTransactionOperation = transactionOperationRepository.findById(transactionOperation.getId()).get();
        // Disconnect from session so that the updates on updatedTransactionOperation are not directly saved in db
        em.detach(updatedTransactionOperation);
        updatedTransactionOperation
            .amount(UPDATED_AMOUNT)
            .subsMsisdn(UPDATED_SUBS_MSISDN)
            .agentMsisdn(UPDATED_AGENT_MSISDN)
            .typeTransaction(UPDATED_TYPE_TRANSACTION)
            .createdAt(UPDATED_CREATED_AT)
            .transactionStatus(UPDATED_TRANSACTION_STATUS)
            .senderZone(UPDATED_SENDER_ZONE)
            .senderProfile(UPDATED_SENDER_PROFILE)
            .codeTerritory(UPDATED_CODE_TERRITORY)
            .subType(UPDATED_SUB_TYPE)
            .operationShortDate(UPDATED_OPERATION_SHORT_DATE)
            .operationDate(UPDATED_OPERATION_DATE)
            .isFraud(UPDATED_IS_FRAUD)
            .taggedAt(UPDATED_TAGGED_AT)
            .fraudSource(UPDATED_FRAUD_SOURCE)
            .comment(UPDATED_COMMENT)
            .falsePositiveDetectedAt(UPDATED_FALSE_POSITIVE_DETECTED_AT)
            .tid(UPDATED_TID)
            .parentMsisdn(UPDATED_PARENT_MSISDN)
            .fctDt(UPDATED_FCT_DT)
            .parentId(UPDATED_PARENT_ID)
            .canceledAt(UPDATED_CANCELED_AT)
            .canceledId(UPDATED_CANCELED_ID)
            .productId(UPDATED_PRODUCT_ID);
        TransactionOperationDTO transactionOperationDTO = transactionOperationMapper.toDto(updatedTransactionOperation);

        restTransactionOperationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, transactionOperationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(transactionOperationDTO))
            )
            .andExpect(status().isOk());

        // Validate the TransactionOperation in the database
        List<TransactionOperation> transactionOperationList = transactionOperationRepository.findAll();
        assertThat(transactionOperationList).hasSize(databaseSizeBeforeUpdate);
        TransactionOperation testTransactionOperation = transactionOperationList.get(transactionOperationList.size() - 1);
        assertThat(testTransactionOperation.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testTransactionOperation.getSubsMsisdn()).isEqualTo(UPDATED_SUBS_MSISDN);
        assertThat(testTransactionOperation.getAgentMsisdn()).isEqualTo(UPDATED_AGENT_MSISDN);
        assertThat(testTransactionOperation.getTypeTransaction()).isEqualTo(UPDATED_TYPE_TRANSACTION);
        assertThat(testTransactionOperation.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testTransactionOperation.getTransactionStatus()).isEqualTo(UPDATED_TRANSACTION_STATUS);
        assertThat(testTransactionOperation.getSenderZone()).isEqualTo(UPDATED_SENDER_ZONE);
        assertThat(testTransactionOperation.getSenderProfile()).isEqualTo(UPDATED_SENDER_PROFILE);
        assertThat(testTransactionOperation.getCodeTerritory()).isEqualTo(UPDATED_CODE_TERRITORY);
        assertThat(testTransactionOperation.getSubType()).isEqualTo(UPDATED_SUB_TYPE);
        assertThat(testTransactionOperation.getOperationShortDate()).isEqualTo(UPDATED_OPERATION_SHORT_DATE);
        assertThat(testTransactionOperation.getOperationDate()).isEqualTo(UPDATED_OPERATION_DATE);
        assertThat(testTransactionOperation.getIsFraud()).isEqualTo(UPDATED_IS_FRAUD);
        assertThat(testTransactionOperation.getTaggedAt()).isEqualTo(UPDATED_TAGGED_AT);
        assertThat(testTransactionOperation.getFraudSource()).isEqualTo(UPDATED_FRAUD_SOURCE);
        assertThat(testTransactionOperation.getComment()).isEqualTo(UPDATED_COMMENT);
        assertThat(testTransactionOperation.getFalsePositiveDetectedAt()).isEqualTo(UPDATED_FALSE_POSITIVE_DETECTED_AT);
        assertThat(testTransactionOperation.getTid()).isEqualTo(UPDATED_TID);
        assertThat(testTransactionOperation.getParentMsisdn()).isEqualTo(UPDATED_PARENT_MSISDN);
        assertThat(testTransactionOperation.getFctDt()).isEqualTo(UPDATED_FCT_DT);
        assertThat(testTransactionOperation.getParentId()).isEqualTo(UPDATED_PARENT_ID);
        assertThat(testTransactionOperation.getCanceledAt()).isEqualTo(UPDATED_CANCELED_AT);
        assertThat(testTransactionOperation.getCanceledId()).isEqualTo(UPDATED_CANCELED_ID);
        assertThat(testTransactionOperation.getProductId()).isEqualTo(UPDATED_PRODUCT_ID);
    }

    @Test
    @Transactional
    void putNonExistingTransactionOperation() throws Exception {
        int databaseSizeBeforeUpdate = transactionOperationRepository.findAll().size();
        transactionOperation.setId(count.incrementAndGet());

        // Create the TransactionOperation
        TransactionOperationDTO transactionOperationDTO = transactionOperationMapper.toDto(transactionOperation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTransactionOperationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, transactionOperationDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(transactionOperationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TransactionOperation in the database
        List<TransactionOperation> transactionOperationList = transactionOperationRepository.findAll();
        assertThat(transactionOperationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchTransactionOperation() throws Exception {
        int databaseSizeBeforeUpdate = transactionOperationRepository.findAll().size();
        transactionOperation.setId(count.incrementAndGet());

        // Create the TransactionOperation
        TransactionOperationDTO transactionOperationDTO = transactionOperationMapper.toDto(transactionOperation);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTransactionOperationMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(transactionOperationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TransactionOperation in the database
        List<TransactionOperation> transactionOperationList = transactionOperationRepository.findAll();
        assertThat(transactionOperationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamTransactionOperation() throws Exception {
        int databaseSizeBeforeUpdate = transactionOperationRepository.findAll().size();
        transactionOperation.setId(count.incrementAndGet());

        // Create the TransactionOperation
        TransactionOperationDTO transactionOperationDTO = transactionOperationMapper.toDto(transactionOperation);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTransactionOperationMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(transactionOperationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the TransactionOperation in the database
        List<TransactionOperation> transactionOperationList = transactionOperationRepository.findAll();
        assertThat(transactionOperationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateTransactionOperationWithPatch() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        int databaseSizeBeforeUpdate = transactionOperationRepository.findAll().size();

        // Update the transactionOperation using partial update
        TransactionOperation partialUpdatedTransactionOperation = new TransactionOperation();
        partialUpdatedTransactionOperation.setId(transactionOperation.getId());

        partialUpdatedTransactionOperation
            .createdAt(UPDATED_CREATED_AT)
            .subType(UPDATED_SUB_TYPE)
            .operationDate(UPDATED_OPERATION_DATE)
            .comment(UPDATED_COMMENT)
            .tid(UPDATED_TID)
            .parentMsisdn(UPDATED_PARENT_MSISDN)
            .fctDt(UPDATED_FCT_DT)
            .canceledAt(UPDATED_CANCELED_AT)
            .canceledId(UPDATED_CANCELED_ID)
            .productId(UPDATED_PRODUCT_ID);

        restTransactionOperationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTransactionOperation.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTransactionOperation))
            )
            .andExpect(status().isOk());

        // Validate the TransactionOperation in the database
        List<TransactionOperation> transactionOperationList = transactionOperationRepository.findAll();
        assertThat(transactionOperationList).hasSize(databaseSizeBeforeUpdate);
        TransactionOperation testTransactionOperation = transactionOperationList.get(transactionOperationList.size() - 1);
        assertThat(testTransactionOperation.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testTransactionOperation.getSubsMsisdn()).isEqualTo(DEFAULT_SUBS_MSISDN);
        assertThat(testTransactionOperation.getAgentMsisdn()).isEqualTo(DEFAULT_AGENT_MSISDN);
        assertThat(testTransactionOperation.getTypeTransaction()).isEqualTo(DEFAULT_TYPE_TRANSACTION);
        assertThat(testTransactionOperation.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testTransactionOperation.getTransactionStatus()).isEqualTo(DEFAULT_TRANSACTION_STATUS);
        assertThat(testTransactionOperation.getSenderZone()).isEqualTo(DEFAULT_SENDER_ZONE);
        assertThat(testTransactionOperation.getSenderProfile()).isEqualTo(DEFAULT_SENDER_PROFILE);
        assertThat(testTransactionOperation.getCodeTerritory()).isEqualTo(DEFAULT_CODE_TERRITORY);
        assertThat(testTransactionOperation.getSubType()).isEqualTo(UPDATED_SUB_TYPE);
        assertThat(testTransactionOperation.getOperationShortDate()).isEqualTo(DEFAULT_OPERATION_SHORT_DATE);
        assertThat(testTransactionOperation.getOperationDate()).isEqualTo(UPDATED_OPERATION_DATE);
        assertThat(testTransactionOperation.getIsFraud()).isEqualTo(DEFAULT_IS_FRAUD);
        assertThat(testTransactionOperation.getTaggedAt()).isEqualTo(DEFAULT_TAGGED_AT);
        assertThat(testTransactionOperation.getFraudSource()).isEqualTo(DEFAULT_FRAUD_SOURCE);
        assertThat(testTransactionOperation.getComment()).isEqualTo(UPDATED_COMMENT);
        assertThat(testTransactionOperation.getFalsePositiveDetectedAt()).isEqualTo(DEFAULT_FALSE_POSITIVE_DETECTED_AT);
        assertThat(testTransactionOperation.getTid()).isEqualTo(UPDATED_TID);
        assertThat(testTransactionOperation.getParentMsisdn()).isEqualTo(UPDATED_PARENT_MSISDN);
        assertThat(testTransactionOperation.getFctDt()).isEqualTo(UPDATED_FCT_DT);
        assertThat(testTransactionOperation.getParentId()).isEqualTo(DEFAULT_PARENT_ID);
        assertThat(testTransactionOperation.getCanceledAt()).isEqualTo(UPDATED_CANCELED_AT);
        assertThat(testTransactionOperation.getCanceledId()).isEqualTo(UPDATED_CANCELED_ID);
        assertThat(testTransactionOperation.getProductId()).isEqualTo(UPDATED_PRODUCT_ID);
    }

    @Test
    @Transactional
    void fullUpdateTransactionOperationWithPatch() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        int databaseSizeBeforeUpdate = transactionOperationRepository.findAll().size();

        // Update the transactionOperation using partial update
        TransactionOperation partialUpdatedTransactionOperation = new TransactionOperation();
        partialUpdatedTransactionOperation.setId(transactionOperation.getId());

        partialUpdatedTransactionOperation
            .amount(UPDATED_AMOUNT)
            .subsMsisdn(UPDATED_SUBS_MSISDN)
            .agentMsisdn(UPDATED_AGENT_MSISDN)
            .typeTransaction(UPDATED_TYPE_TRANSACTION)
            .createdAt(UPDATED_CREATED_AT)
            .transactionStatus(UPDATED_TRANSACTION_STATUS)
            .senderZone(UPDATED_SENDER_ZONE)
            .senderProfile(UPDATED_SENDER_PROFILE)
            .codeTerritory(UPDATED_CODE_TERRITORY)
            .subType(UPDATED_SUB_TYPE)
            .operationShortDate(UPDATED_OPERATION_SHORT_DATE)
            .operationDate(UPDATED_OPERATION_DATE)
            .isFraud(UPDATED_IS_FRAUD)
            .taggedAt(UPDATED_TAGGED_AT)
            .fraudSource(UPDATED_FRAUD_SOURCE)
            .comment(UPDATED_COMMENT)
            .falsePositiveDetectedAt(UPDATED_FALSE_POSITIVE_DETECTED_AT)
            .tid(UPDATED_TID)
            .parentMsisdn(UPDATED_PARENT_MSISDN)
            .fctDt(UPDATED_FCT_DT)
            .parentId(UPDATED_PARENT_ID)
            .canceledAt(UPDATED_CANCELED_AT)
            .canceledId(UPDATED_CANCELED_ID)
            .productId(UPDATED_PRODUCT_ID);

        restTransactionOperationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedTransactionOperation.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedTransactionOperation))
            )
            .andExpect(status().isOk());

        // Validate the TransactionOperation in the database
        List<TransactionOperation> transactionOperationList = transactionOperationRepository.findAll();
        assertThat(transactionOperationList).hasSize(databaseSizeBeforeUpdate);
        TransactionOperation testTransactionOperation = transactionOperationList.get(transactionOperationList.size() - 1);
        assertThat(testTransactionOperation.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testTransactionOperation.getSubsMsisdn()).isEqualTo(UPDATED_SUBS_MSISDN);
        assertThat(testTransactionOperation.getAgentMsisdn()).isEqualTo(UPDATED_AGENT_MSISDN);
        assertThat(testTransactionOperation.getTypeTransaction()).isEqualTo(UPDATED_TYPE_TRANSACTION);
        assertThat(testTransactionOperation.getCreatedAt()).isEqualTo(UPDATED_CREATED_AT);
        assertThat(testTransactionOperation.getTransactionStatus()).isEqualTo(UPDATED_TRANSACTION_STATUS);
        assertThat(testTransactionOperation.getSenderZone()).isEqualTo(UPDATED_SENDER_ZONE);
        assertThat(testTransactionOperation.getSenderProfile()).isEqualTo(UPDATED_SENDER_PROFILE);
        assertThat(testTransactionOperation.getCodeTerritory()).isEqualTo(UPDATED_CODE_TERRITORY);
        assertThat(testTransactionOperation.getSubType()).isEqualTo(UPDATED_SUB_TYPE);
        assertThat(testTransactionOperation.getOperationShortDate()).isEqualTo(UPDATED_OPERATION_SHORT_DATE);
        assertThat(testTransactionOperation.getOperationDate()).isEqualTo(UPDATED_OPERATION_DATE);
        assertThat(testTransactionOperation.getIsFraud()).isEqualTo(UPDATED_IS_FRAUD);
        assertThat(testTransactionOperation.getTaggedAt()).isEqualTo(UPDATED_TAGGED_AT);
        assertThat(testTransactionOperation.getFraudSource()).isEqualTo(UPDATED_FRAUD_SOURCE);
        assertThat(testTransactionOperation.getComment()).isEqualTo(UPDATED_COMMENT);
        assertThat(testTransactionOperation.getFalsePositiveDetectedAt()).isEqualTo(UPDATED_FALSE_POSITIVE_DETECTED_AT);
        assertThat(testTransactionOperation.getTid()).isEqualTo(UPDATED_TID);
        assertThat(testTransactionOperation.getParentMsisdn()).isEqualTo(UPDATED_PARENT_MSISDN);
        assertThat(testTransactionOperation.getFctDt()).isEqualTo(UPDATED_FCT_DT);
        assertThat(testTransactionOperation.getParentId()).isEqualTo(UPDATED_PARENT_ID);
        assertThat(testTransactionOperation.getCanceledAt()).isEqualTo(UPDATED_CANCELED_AT);
        assertThat(testTransactionOperation.getCanceledId()).isEqualTo(UPDATED_CANCELED_ID);
        assertThat(testTransactionOperation.getProductId()).isEqualTo(UPDATED_PRODUCT_ID);
    }

    @Test
    @Transactional
    void patchNonExistingTransactionOperation() throws Exception {
        int databaseSizeBeforeUpdate = transactionOperationRepository.findAll().size();
        transactionOperation.setId(count.incrementAndGet());

        // Create the TransactionOperation
        TransactionOperationDTO transactionOperationDTO = transactionOperationMapper.toDto(transactionOperation);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTransactionOperationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, transactionOperationDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(transactionOperationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TransactionOperation in the database
        List<TransactionOperation> transactionOperationList = transactionOperationRepository.findAll();
        assertThat(transactionOperationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchTransactionOperation() throws Exception {
        int databaseSizeBeforeUpdate = transactionOperationRepository.findAll().size();
        transactionOperation.setId(count.incrementAndGet());

        // Create the TransactionOperation
        TransactionOperationDTO transactionOperationDTO = transactionOperationMapper.toDto(transactionOperation);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTransactionOperationMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(transactionOperationDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the TransactionOperation in the database
        List<TransactionOperation> transactionOperationList = transactionOperationRepository.findAll();
        assertThat(transactionOperationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamTransactionOperation() throws Exception {
        int databaseSizeBeforeUpdate = transactionOperationRepository.findAll().size();
        transactionOperation.setId(count.incrementAndGet());

        // Create the TransactionOperation
        TransactionOperationDTO transactionOperationDTO = transactionOperationMapper.toDto(transactionOperation);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restTransactionOperationMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(transactionOperationDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the TransactionOperation in the database
        List<TransactionOperation> transactionOperationList = transactionOperationRepository.findAll();
        assertThat(transactionOperationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteTransactionOperation() throws Exception {
        // Initialize the database
        transactionOperationRepository.saveAndFlush(transactionOperation);

        int databaseSizeBeforeDelete = transactionOperationRepository.findAll().size();

        // Delete the transactionOperation
        restTransactionOperationMockMvc
            .perform(delete(ENTITY_API_URL_ID, transactionOperation.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<TransactionOperation> transactionOperationList = transactionOperationRepository.findAll();
        assertThat(transactionOperationList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
