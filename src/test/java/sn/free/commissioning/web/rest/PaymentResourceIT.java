package sn.free.commissioning.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.IntegrationTest;
import sn.free.commissioning.domain.Commission;
import sn.free.commissioning.domain.Payment;
import sn.free.commissioning.domain.enumeration.PaymentStatus;
import sn.free.commissioning.repository.PaymentRepository;
import sn.free.commissioning.service.criteria.PaymentCriteria;
import sn.free.commissioning.service.dto.PaymentDTO;
import sn.free.commissioning.service.mapper.PaymentMapper;

/**
 * Integration tests for the {@link PaymentResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PaymentResourceIT {

    private static final Double DEFAULT_AMOUNT = 1D;
    private static final Double UPDATED_AMOUNT = 2D;
    private static final Double SMALLER_AMOUNT = 1D - 1D;

    private static final String DEFAULT_SENDER_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_SENDER_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_RECEIVER_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_RECEIVER_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_RECEIVER_PROFILE = "AAAAAAAAAA";
    private static final String UPDATED_RECEIVER_PROFILE = "BBBBBBBBBB";

    private static final Instant DEFAULT_PERIOD_START_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_PERIOD_START_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_PERIOD_END_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_PERIOD_END_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Double DEFAULT_EXECUTION_DURATION = 1D;
    private static final Double UPDATED_EXECUTION_DURATION = 2D;
    private static final Double SMALLER_EXECUTION_DURATION = 1D - 1D;

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final PaymentStatus DEFAULT_PAYMENT_STATUS = PaymentStatus.FAILURE;
    private static final PaymentStatus UPDATED_PAYMENT_STATUS = PaymentStatus.SUCCESS;

    private static final String DEFAULT_SPARE_1 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_2 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_3 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_3 = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/payments";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private PaymentMapper paymentMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPaymentMockMvc;

    private Payment payment;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Payment createEntity(EntityManager em) {
        Payment payment = new Payment()
            .amount(DEFAULT_AMOUNT)
            .senderMsisdn(DEFAULT_SENDER_MSISDN)
            .receiverMsisdn(DEFAULT_RECEIVER_MSISDN)
            .receiverProfile(DEFAULT_RECEIVER_PROFILE)
            .periodStartDate(DEFAULT_PERIOD_START_DATE)
            .periodEndDate(DEFAULT_PERIOD_END_DATE)
            .executionDuration(DEFAULT_EXECUTION_DURATION)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .paymentStatus(DEFAULT_PAYMENT_STATUS)
            .spare1(DEFAULT_SPARE_1)
            .spare2(DEFAULT_SPARE_2)
            .spare3(DEFAULT_SPARE_3);
        return payment;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Payment createUpdatedEntity(EntityManager em) {
        Payment payment = new Payment()
            .amount(UPDATED_AMOUNT)
            .senderMsisdn(UPDATED_SENDER_MSISDN)
            .receiverMsisdn(UPDATED_RECEIVER_MSISDN)
            .receiverProfile(UPDATED_RECEIVER_PROFILE)
            .periodStartDate(UPDATED_PERIOD_START_DATE)
            .periodEndDate(UPDATED_PERIOD_END_DATE)
            .executionDuration(UPDATED_EXECUTION_DURATION)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .paymentStatus(UPDATED_PAYMENT_STATUS)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);
        return payment;
    }

    @BeforeEach
    public void initTest() {
        payment = createEntity(em);
    }

    @Test
    @Transactional
    void createPayment() throws Exception {
        int databaseSizeBeforeCreate = paymentRepository.findAll().size();
        // Create the Payment
        PaymentDTO paymentDTO = paymentMapper.toDto(payment);
        restPaymentMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentDTO)))
            .andExpect(status().isCreated());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeCreate + 1);
        Payment testPayment = paymentList.get(paymentList.size() - 1);
        assertThat(testPayment.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testPayment.getSenderMsisdn()).isEqualTo(DEFAULT_SENDER_MSISDN);
        assertThat(testPayment.getReceiverMsisdn()).isEqualTo(DEFAULT_RECEIVER_MSISDN);
        assertThat(testPayment.getReceiverProfile()).isEqualTo(DEFAULT_RECEIVER_PROFILE);
        assertThat(testPayment.getPeriodStartDate()).isEqualTo(DEFAULT_PERIOD_START_DATE);
        assertThat(testPayment.getPeriodEndDate()).isEqualTo(DEFAULT_PERIOD_END_DATE);
        assertThat(testPayment.getExecutionDuration()).isEqualTo(DEFAULT_EXECUTION_DURATION);
        assertThat(testPayment.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testPayment.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testPayment.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testPayment.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testPayment.getPaymentStatus()).isEqualTo(DEFAULT_PAYMENT_STATUS);
        assertThat(testPayment.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testPayment.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
        assertThat(testPayment.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
    }

    @Test
    @Transactional
    void createPaymentWithExistingId() throws Exception {
        // Create the Payment with an existing ID
        payment.setId(1L);
        PaymentDTO paymentDTO = paymentMapper.toDto(payment);

        int databaseSizeBeforeCreate = paymentRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPaymentMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkCreatedByIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentRepository.findAll().size();
        // set the field null
        payment.setCreatedBy(null);

        // Create the Payment, which fails.
        PaymentDTO paymentDTO = paymentMapper.toDto(payment);

        restPaymentMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentDTO)))
            .andExpect(status().isBadRequest());

        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPayments() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList
        restPaymentMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(payment.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].senderMsisdn").value(hasItem(DEFAULT_SENDER_MSISDN)))
            .andExpect(jsonPath("$.[*].receiverMsisdn").value(hasItem(DEFAULT_RECEIVER_MSISDN)))
            .andExpect(jsonPath("$.[*].receiverProfile").value(hasItem(DEFAULT_RECEIVER_PROFILE)))
            .andExpect(jsonPath("$.[*].periodStartDate").value(hasItem(DEFAULT_PERIOD_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].periodEndDate").value(hasItem(DEFAULT_PERIOD_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].executionDuration").value(hasItem(DEFAULT_EXECUTION_DURATION.doubleValue())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].paymentStatus").value(hasItem(DEFAULT_PAYMENT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)));
    }

    @Test
    @Transactional
    void getPayment() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get the payment
        restPaymentMockMvc
            .perform(get(ENTITY_API_URL_ID, payment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(payment.getId().intValue()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.senderMsisdn").value(DEFAULT_SENDER_MSISDN))
            .andExpect(jsonPath("$.receiverMsisdn").value(DEFAULT_RECEIVER_MSISDN))
            .andExpect(jsonPath("$.receiverProfile").value(DEFAULT_RECEIVER_PROFILE))
            .andExpect(jsonPath("$.periodStartDate").value(DEFAULT_PERIOD_START_DATE.toString()))
            .andExpect(jsonPath("$.periodEndDate").value(DEFAULT_PERIOD_END_DATE.toString()))
            .andExpect(jsonPath("$.executionDuration").value(DEFAULT_EXECUTION_DURATION.doubleValue()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.paymentStatus").value(DEFAULT_PAYMENT_STATUS.toString()))
            .andExpect(jsonPath("$.spare1").value(DEFAULT_SPARE_1))
            .andExpect(jsonPath("$.spare2").value(DEFAULT_SPARE_2))
            .andExpect(jsonPath("$.spare3").value(DEFAULT_SPARE_3));
    }

    @Test
    @Transactional
    void getPaymentsByIdFiltering() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        Long id = payment.getId();

        defaultPaymentShouldBeFound("id.equals=" + id);
        defaultPaymentShouldNotBeFound("id.notEquals=" + id);

        defaultPaymentShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPaymentShouldNotBeFound("id.greaterThan=" + id);

        defaultPaymentShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPaymentShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllPaymentsByAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where amount equals to DEFAULT_AMOUNT
        defaultPaymentShouldBeFound("amount.equals=" + DEFAULT_AMOUNT);

        // Get all the paymentList where amount equals to UPDATED_AMOUNT
        defaultPaymentShouldNotBeFound("amount.equals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllPaymentsByAmountIsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where amount not equals to DEFAULT_AMOUNT
        defaultPaymentShouldNotBeFound("amount.notEquals=" + DEFAULT_AMOUNT);

        // Get all the paymentList where amount not equals to UPDATED_AMOUNT
        defaultPaymentShouldBeFound("amount.notEquals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllPaymentsByAmountIsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where amount in DEFAULT_AMOUNT or UPDATED_AMOUNT
        defaultPaymentShouldBeFound("amount.in=" + DEFAULT_AMOUNT + "," + UPDATED_AMOUNT);

        // Get all the paymentList where amount equals to UPDATED_AMOUNT
        defaultPaymentShouldNotBeFound("amount.in=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllPaymentsByAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where amount is not null
        defaultPaymentShouldBeFound("amount.specified=true");

        // Get all the paymentList where amount is null
        defaultPaymentShouldNotBeFound("amount.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsByAmountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where amount is greater than or equal to DEFAULT_AMOUNT
        defaultPaymentShouldBeFound("amount.greaterThanOrEqual=" + DEFAULT_AMOUNT);

        // Get all the paymentList where amount is greater than or equal to UPDATED_AMOUNT
        defaultPaymentShouldNotBeFound("amount.greaterThanOrEqual=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllPaymentsByAmountIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where amount is less than or equal to DEFAULT_AMOUNT
        defaultPaymentShouldBeFound("amount.lessThanOrEqual=" + DEFAULT_AMOUNT);

        // Get all the paymentList where amount is less than or equal to SMALLER_AMOUNT
        defaultPaymentShouldNotBeFound("amount.lessThanOrEqual=" + SMALLER_AMOUNT);
    }

    @Test
    @Transactional
    void getAllPaymentsByAmountIsLessThanSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where amount is less than DEFAULT_AMOUNT
        defaultPaymentShouldNotBeFound("amount.lessThan=" + DEFAULT_AMOUNT);

        // Get all the paymentList where amount is less than UPDATED_AMOUNT
        defaultPaymentShouldBeFound("amount.lessThan=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllPaymentsByAmountIsGreaterThanSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where amount is greater than DEFAULT_AMOUNT
        defaultPaymentShouldNotBeFound("amount.greaterThan=" + DEFAULT_AMOUNT);

        // Get all the paymentList where amount is greater than SMALLER_AMOUNT
        defaultPaymentShouldBeFound("amount.greaterThan=" + SMALLER_AMOUNT);
    }

    @Test
    @Transactional
    void getAllPaymentsBySenderMsisdnIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where senderMsisdn equals to DEFAULT_SENDER_MSISDN
        defaultPaymentShouldBeFound("senderMsisdn.equals=" + DEFAULT_SENDER_MSISDN);

        // Get all the paymentList where senderMsisdn equals to UPDATED_SENDER_MSISDN
        defaultPaymentShouldNotBeFound("senderMsisdn.equals=" + UPDATED_SENDER_MSISDN);
    }

    @Test
    @Transactional
    void getAllPaymentsBySenderMsisdnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where senderMsisdn not equals to DEFAULT_SENDER_MSISDN
        defaultPaymentShouldNotBeFound("senderMsisdn.notEquals=" + DEFAULT_SENDER_MSISDN);

        // Get all the paymentList where senderMsisdn not equals to UPDATED_SENDER_MSISDN
        defaultPaymentShouldBeFound("senderMsisdn.notEquals=" + UPDATED_SENDER_MSISDN);
    }

    @Test
    @Transactional
    void getAllPaymentsBySenderMsisdnIsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where senderMsisdn in DEFAULT_SENDER_MSISDN or UPDATED_SENDER_MSISDN
        defaultPaymentShouldBeFound("senderMsisdn.in=" + DEFAULT_SENDER_MSISDN + "," + UPDATED_SENDER_MSISDN);

        // Get all the paymentList where senderMsisdn equals to UPDATED_SENDER_MSISDN
        defaultPaymentShouldNotBeFound("senderMsisdn.in=" + UPDATED_SENDER_MSISDN);
    }

    @Test
    @Transactional
    void getAllPaymentsBySenderMsisdnIsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where senderMsisdn is not null
        defaultPaymentShouldBeFound("senderMsisdn.specified=true");

        // Get all the paymentList where senderMsisdn is null
        defaultPaymentShouldNotBeFound("senderMsisdn.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsBySenderMsisdnContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where senderMsisdn contains DEFAULT_SENDER_MSISDN
        defaultPaymentShouldBeFound("senderMsisdn.contains=" + DEFAULT_SENDER_MSISDN);

        // Get all the paymentList where senderMsisdn contains UPDATED_SENDER_MSISDN
        defaultPaymentShouldNotBeFound("senderMsisdn.contains=" + UPDATED_SENDER_MSISDN);
    }

    @Test
    @Transactional
    void getAllPaymentsBySenderMsisdnNotContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where senderMsisdn does not contain DEFAULT_SENDER_MSISDN
        defaultPaymentShouldNotBeFound("senderMsisdn.doesNotContain=" + DEFAULT_SENDER_MSISDN);

        // Get all the paymentList where senderMsisdn does not contain UPDATED_SENDER_MSISDN
        defaultPaymentShouldBeFound("senderMsisdn.doesNotContain=" + UPDATED_SENDER_MSISDN);
    }

    @Test
    @Transactional
    void getAllPaymentsByReceiverMsisdnIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where receiverMsisdn equals to DEFAULT_RECEIVER_MSISDN
        defaultPaymentShouldBeFound("receiverMsisdn.equals=" + DEFAULT_RECEIVER_MSISDN);

        // Get all the paymentList where receiverMsisdn equals to UPDATED_RECEIVER_MSISDN
        defaultPaymentShouldNotBeFound("receiverMsisdn.equals=" + UPDATED_RECEIVER_MSISDN);
    }

    @Test
    @Transactional
    void getAllPaymentsByReceiverMsisdnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where receiverMsisdn not equals to DEFAULT_RECEIVER_MSISDN
        defaultPaymentShouldNotBeFound("receiverMsisdn.notEquals=" + DEFAULT_RECEIVER_MSISDN);

        // Get all the paymentList where receiverMsisdn not equals to UPDATED_RECEIVER_MSISDN
        defaultPaymentShouldBeFound("receiverMsisdn.notEquals=" + UPDATED_RECEIVER_MSISDN);
    }

    @Test
    @Transactional
    void getAllPaymentsByReceiverMsisdnIsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where receiverMsisdn in DEFAULT_RECEIVER_MSISDN or UPDATED_RECEIVER_MSISDN
        defaultPaymentShouldBeFound("receiverMsisdn.in=" + DEFAULT_RECEIVER_MSISDN + "," + UPDATED_RECEIVER_MSISDN);

        // Get all the paymentList where receiverMsisdn equals to UPDATED_RECEIVER_MSISDN
        defaultPaymentShouldNotBeFound("receiverMsisdn.in=" + UPDATED_RECEIVER_MSISDN);
    }

    @Test
    @Transactional
    void getAllPaymentsByReceiverMsisdnIsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where receiverMsisdn is not null
        defaultPaymentShouldBeFound("receiverMsisdn.specified=true");

        // Get all the paymentList where receiverMsisdn is null
        defaultPaymentShouldNotBeFound("receiverMsisdn.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsByReceiverMsisdnContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where receiverMsisdn contains DEFAULT_RECEIVER_MSISDN
        defaultPaymentShouldBeFound("receiverMsisdn.contains=" + DEFAULT_RECEIVER_MSISDN);

        // Get all the paymentList where receiverMsisdn contains UPDATED_RECEIVER_MSISDN
        defaultPaymentShouldNotBeFound("receiverMsisdn.contains=" + UPDATED_RECEIVER_MSISDN);
    }

    @Test
    @Transactional
    void getAllPaymentsByReceiverMsisdnNotContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where receiverMsisdn does not contain DEFAULT_RECEIVER_MSISDN
        defaultPaymentShouldNotBeFound("receiverMsisdn.doesNotContain=" + DEFAULT_RECEIVER_MSISDN);

        // Get all the paymentList where receiverMsisdn does not contain UPDATED_RECEIVER_MSISDN
        defaultPaymentShouldBeFound("receiverMsisdn.doesNotContain=" + UPDATED_RECEIVER_MSISDN);
    }

    @Test
    @Transactional
    void getAllPaymentsByReceiverProfileIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where receiverProfile equals to DEFAULT_RECEIVER_PROFILE
        defaultPaymentShouldBeFound("receiverProfile.equals=" + DEFAULT_RECEIVER_PROFILE);

        // Get all the paymentList where receiverProfile equals to UPDATED_RECEIVER_PROFILE
        defaultPaymentShouldNotBeFound("receiverProfile.equals=" + UPDATED_RECEIVER_PROFILE);
    }

    @Test
    @Transactional
    void getAllPaymentsByReceiverProfileIsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where receiverProfile not equals to DEFAULT_RECEIVER_PROFILE
        defaultPaymentShouldNotBeFound("receiverProfile.notEquals=" + DEFAULT_RECEIVER_PROFILE);

        // Get all the paymentList where receiverProfile not equals to UPDATED_RECEIVER_PROFILE
        defaultPaymentShouldBeFound("receiverProfile.notEquals=" + UPDATED_RECEIVER_PROFILE);
    }

    @Test
    @Transactional
    void getAllPaymentsByReceiverProfileIsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where receiverProfile in DEFAULT_RECEIVER_PROFILE or UPDATED_RECEIVER_PROFILE
        defaultPaymentShouldBeFound("receiverProfile.in=" + DEFAULT_RECEIVER_PROFILE + "," + UPDATED_RECEIVER_PROFILE);

        // Get all the paymentList where receiverProfile equals to UPDATED_RECEIVER_PROFILE
        defaultPaymentShouldNotBeFound("receiverProfile.in=" + UPDATED_RECEIVER_PROFILE);
    }

    @Test
    @Transactional
    void getAllPaymentsByReceiverProfileIsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where receiverProfile is not null
        defaultPaymentShouldBeFound("receiverProfile.specified=true");

        // Get all the paymentList where receiverProfile is null
        defaultPaymentShouldNotBeFound("receiverProfile.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsByReceiverProfileContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where receiverProfile contains DEFAULT_RECEIVER_PROFILE
        defaultPaymentShouldBeFound("receiverProfile.contains=" + DEFAULT_RECEIVER_PROFILE);

        // Get all the paymentList where receiverProfile contains UPDATED_RECEIVER_PROFILE
        defaultPaymentShouldNotBeFound("receiverProfile.contains=" + UPDATED_RECEIVER_PROFILE);
    }

    @Test
    @Transactional
    void getAllPaymentsByReceiverProfileNotContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where receiverProfile does not contain DEFAULT_RECEIVER_PROFILE
        defaultPaymentShouldNotBeFound("receiverProfile.doesNotContain=" + DEFAULT_RECEIVER_PROFILE);

        // Get all the paymentList where receiverProfile does not contain UPDATED_RECEIVER_PROFILE
        defaultPaymentShouldBeFound("receiverProfile.doesNotContain=" + UPDATED_RECEIVER_PROFILE);
    }

    @Test
    @Transactional
    void getAllPaymentsByPeriodStartDateIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where periodStartDate equals to DEFAULT_PERIOD_START_DATE
        defaultPaymentShouldBeFound("periodStartDate.equals=" + DEFAULT_PERIOD_START_DATE);

        // Get all the paymentList where periodStartDate equals to UPDATED_PERIOD_START_DATE
        defaultPaymentShouldNotBeFound("periodStartDate.equals=" + UPDATED_PERIOD_START_DATE);
    }

    @Test
    @Transactional
    void getAllPaymentsByPeriodStartDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where periodStartDate not equals to DEFAULT_PERIOD_START_DATE
        defaultPaymentShouldNotBeFound("periodStartDate.notEquals=" + DEFAULT_PERIOD_START_DATE);

        // Get all the paymentList where periodStartDate not equals to UPDATED_PERIOD_START_DATE
        defaultPaymentShouldBeFound("periodStartDate.notEquals=" + UPDATED_PERIOD_START_DATE);
    }

    @Test
    @Transactional
    void getAllPaymentsByPeriodStartDateIsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where periodStartDate in DEFAULT_PERIOD_START_DATE or UPDATED_PERIOD_START_DATE
        defaultPaymentShouldBeFound("periodStartDate.in=" + DEFAULT_PERIOD_START_DATE + "," + UPDATED_PERIOD_START_DATE);

        // Get all the paymentList where periodStartDate equals to UPDATED_PERIOD_START_DATE
        defaultPaymentShouldNotBeFound("periodStartDate.in=" + UPDATED_PERIOD_START_DATE);
    }

    @Test
    @Transactional
    void getAllPaymentsByPeriodStartDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where periodStartDate is not null
        defaultPaymentShouldBeFound("periodStartDate.specified=true");

        // Get all the paymentList where periodStartDate is null
        defaultPaymentShouldNotBeFound("periodStartDate.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsByPeriodEndDateIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where periodEndDate equals to DEFAULT_PERIOD_END_DATE
        defaultPaymentShouldBeFound("periodEndDate.equals=" + DEFAULT_PERIOD_END_DATE);

        // Get all the paymentList where periodEndDate equals to UPDATED_PERIOD_END_DATE
        defaultPaymentShouldNotBeFound("periodEndDate.equals=" + UPDATED_PERIOD_END_DATE);
    }

    @Test
    @Transactional
    void getAllPaymentsByPeriodEndDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where periodEndDate not equals to DEFAULT_PERIOD_END_DATE
        defaultPaymentShouldNotBeFound("periodEndDate.notEquals=" + DEFAULT_PERIOD_END_DATE);

        // Get all the paymentList where periodEndDate not equals to UPDATED_PERIOD_END_DATE
        defaultPaymentShouldBeFound("periodEndDate.notEquals=" + UPDATED_PERIOD_END_DATE);
    }

    @Test
    @Transactional
    void getAllPaymentsByPeriodEndDateIsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where periodEndDate in DEFAULT_PERIOD_END_DATE or UPDATED_PERIOD_END_DATE
        defaultPaymentShouldBeFound("periodEndDate.in=" + DEFAULT_PERIOD_END_DATE + "," + UPDATED_PERIOD_END_DATE);

        // Get all the paymentList where periodEndDate equals to UPDATED_PERIOD_END_DATE
        defaultPaymentShouldNotBeFound("periodEndDate.in=" + UPDATED_PERIOD_END_DATE);
    }

    @Test
    @Transactional
    void getAllPaymentsByPeriodEndDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where periodEndDate is not null
        defaultPaymentShouldBeFound("periodEndDate.specified=true");

        // Get all the paymentList where periodEndDate is null
        defaultPaymentShouldNotBeFound("periodEndDate.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsByExecutionDurationIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where executionDuration equals to DEFAULT_EXECUTION_DURATION
        defaultPaymentShouldBeFound("executionDuration.equals=" + DEFAULT_EXECUTION_DURATION);

        // Get all the paymentList where executionDuration equals to UPDATED_EXECUTION_DURATION
        defaultPaymentShouldNotBeFound("executionDuration.equals=" + UPDATED_EXECUTION_DURATION);
    }

    @Test
    @Transactional
    void getAllPaymentsByExecutionDurationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where executionDuration not equals to DEFAULT_EXECUTION_DURATION
        defaultPaymentShouldNotBeFound("executionDuration.notEquals=" + DEFAULT_EXECUTION_DURATION);

        // Get all the paymentList where executionDuration not equals to UPDATED_EXECUTION_DURATION
        defaultPaymentShouldBeFound("executionDuration.notEquals=" + UPDATED_EXECUTION_DURATION);
    }

    @Test
    @Transactional
    void getAllPaymentsByExecutionDurationIsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where executionDuration in DEFAULT_EXECUTION_DURATION or UPDATED_EXECUTION_DURATION
        defaultPaymentShouldBeFound("executionDuration.in=" + DEFAULT_EXECUTION_DURATION + "," + UPDATED_EXECUTION_DURATION);

        // Get all the paymentList where executionDuration equals to UPDATED_EXECUTION_DURATION
        defaultPaymentShouldNotBeFound("executionDuration.in=" + UPDATED_EXECUTION_DURATION);
    }

    @Test
    @Transactional
    void getAllPaymentsByExecutionDurationIsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where executionDuration is not null
        defaultPaymentShouldBeFound("executionDuration.specified=true");

        // Get all the paymentList where executionDuration is null
        defaultPaymentShouldNotBeFound("executionDuration.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsByExecutionDurationIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where executionDuration is greater than or equal to DEFAULT_EXECUTION_DURATION
        defaultPaymentShouldBeFound("executionDuration.greaterThanOrEqual=" + DEFAULT_EXECUTION_DURATION);

        // Get all the paymentList where executionDuration is greater than or equal to UPDATED_EXECUTION_DURATION
        defaultPaymentShouldNotBeFound("executionDuration.greaterThanOrEqual=" + UPDATED_EXECUTION_DURATION);
    }

    @Test
    @Transactional
    void getAllPaymentsByExecutionDurationIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where executionDuration is less than or equal to DEFAULT_EXECUTION_DURATION
        defaultPaymentShouldBeFound("executionDuration.lessThanOrEqual=" + DEFAULT_EXECUTION_DURATION);

        // Get all the paymentList where executionDuration is less than or equal to SMALLER_EXECUTION_DURATION
        defaultPaymentShouldNotBeFound("executionDuration.lessThanOrEqual=" + SMALLER_EXECUTION_DURATION);
    }

    @Test
    @Transactional
    void getAllPaymentsByExecutionDurationIsLessThanSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where executionDuration is less than DEFAULT_EXECUTION_DURATION
        defaultPaymentShouldNotBeFound("executionDuration.lessThan=" + DEFAULT_EXECUTION_DURATION);

        // Get all the paymentList where executionDuration is less than UPDATED_EXECUTION_DURATION
        defaultPaymentShouldBeFound("executionDuration.lessThan=" + UPDATED_EXECUTION_DURATION);
    }

    @Test
    @Transactional
    void getAllPaymentsByExecutionDurationIsGreaterThanSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where executionDuration is greater than DEFAULT_EXECUTION_DURATION
        defaultPaymentShouldNotBeFound("executionDuration.greaterThan=" + DEFAULT_EXECUTION_DURATION);

        // Get all the paymentList where executionDuration is greater than SMALLER_EXECUTION_DURATION
        defaultPaymentShouldBeFound("executionDuration.greaterThan=" + SMALLER_EXECUTION_DURATION);
    }

    @Test
    @Transactional
    void getAllPaymentsByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where createdBy equals to DEFAULT_CREATED_BY
        defaultPaymentShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the paymentList where createdBy equals to UPDATED_CREATED_BY
        defaultPaymentShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPaymentsByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where createdBy not equals to DEFAULT_CREATED_BY
        defaultPaymentShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the paymentList where createdBy not equals to UPDATED_CREATED_BY
        defaultPaymentShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPaymentsByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultPaymentShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the paymentList where createdBy equals to UPDATED_CREATED_BY
        defaultPaymentShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPaymentsByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where createdBy is not null
        defaultPaymentShouldBeFound("createdBy.specified=true");

        // Get all the paymentList where createdBy is null
        defaultPaymentShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where createdBy contains DEFAULT_CREATED_BY
        defaultPaymentShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the paymentList where createdBy contains UPDATED_CREATED_BY
        defaultPaymentShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPaymentsByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where createdBy does not contain DEFAULT_CREATED_BY
        defaultPaymentShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the paymentList where createdBy does not contain UPDATED_CREATED_BY
        defaultPaymentShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPaymentsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where createdDate equals to DEFAULT_CREATED_DATE
        defaultPaymentShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the paymentList where createdDate equals to UPDATED_CREATED_DATE
        defaultPaymentShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllPaymentsByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultPaymentShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the paymentList where createdDate not equals to UPDATED_CREATED_DATE
        defaultPaymentShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllPaymentsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultPaymentShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the paymentList where createdDate equals to UPDATED_CREATED_DATE
        defaultPaymentShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllPaymentsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where createdDate is not null
        defaultPaymentShouldBeFound("createdDate.specified=true");

        // Get all the paymentList where createdDate is null
        defaultPaymentShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultPaymentShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the paymentList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultPaymentShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllPaymentsByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultPaymentShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the paymentList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultPaymentShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllPaymentsByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultPaymentShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the paymentList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultPaymentShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllPaymentsByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where lastModifiedBy is not null
        defaultPaymentShouldBeFound("lastModifiedBy.specified=true");

        // Get all the paymentList where lastModifiedBy is null
        defaultPaymentShouldNotBeFound("lastModifiedBy.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultPaymentShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the paymentList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultPaymentShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllPaymentsByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultPaymentShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the paymentList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultPaymentShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllPaymentsByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultPaymentShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the paymentList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultPaymentShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllPaymentsByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultPaymentShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the paymentList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultPaymentShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllPaymentsByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultPaymentShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the paymentList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultPaymentShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllPaymentsByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where lastModifiedDate is not null
        defaultPaymentShouldBeFound("lastModifiedDate.specified=true");

        // Get all the paymentList where lastModifiedDate is null
        defaultPaymentShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsByPaymentStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where paymentStatus equals to DEFAULT_PAYMENT_STATUS
        defaultPaymentShouldBeFound("paymentStatus.equals=" + DEFAULT_PAYMENT_STATUS);

        // Get all the paymentList where paymentStatus equals to UPDATED_PAYMENT_STATUS
        defaultPaymentShouldNotBeFound("paymentStatus.equals=" + UPDATED_PAYMENT_STATUS);
    }

    @Test
    @Transactional
    void getAllPaymentsByPaymentStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where paymentStatus not equals to DEFAULT_PAYMENT_STATUS
        defaultPaymentShouldNotBeFound("paymentStatus.notEquals=" + DEFAULT_PAYMENT_STATUS);

        // Get all the paymentList where paymentStatus not equals to UPDATED_PAYMENT_STATUS
        defaultPaymentShouldBeFound("paymentStatus.notEquals=" + UPDATED_PAYMENT_STATUS);
    }

    @Test
    @Transactional
    void getAllPaymentsByPaymentStatusIsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where paymentStatus in DEFAULT_PAYMENT_STATUS or UPDATED_PAYMENT_STATUS
        defaultPaymentShouldBeFound("paymentStatus.in=" + DEFAULT_PAYMENT_STATUS + "," + UPDATED_PAYMENT_STATUS);

        // Get all the paymentList where paymentStatus equals to UPDATED_PAYMENT_STATUS
        defaultPaymentShouldNotBeFound("paymentStatus.in=" + UPDATED_PAYMENT_STATUS);
    }

    @Test
    @Transactional
    void getAllPaymentsByPaymentStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where paymentStatus is not null
        defaultPaymentShouldBeFound("paymentStatus.specified=true");

        // Get all the paymentList where paymentStatus is null
        defaultPaymentShouldNotBeFound("paymentStatus.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsBySpare1IsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where spare1 equals to DEFAULT_SPARE_1
        defaultPaymentShouldBeFound("spare1.equals=" + DEFAULT_SPARE_1);

        // Get all the paymentList where spare1 equals to UPDATED_SPARE_1
        defaultPaymentShouldNotBeFound("spare1.equals=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllPaymentsBySpare1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where spare1 not equals to DEFAULT_SPARE_1
        defaultPaymentShouldNotBeFound("spare1.notEquals=" + DEFAULT_SPARE_1);

        // Get all the paymentList where spare1 not equals to UPDATED_SPARE_1
        defaultPaymentShouldBeFound("spare1.notEquals=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllPaymentsBySpare1IsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where spare1 in DEFAULT_SPARE_1 or UPDATED_SPARE_1
        defaultPaymentShouldBeFound("spare1.in=" + DEFAULT_SPARE_1 + "," + UPDATED_SPARE_1);

        // Get all the paymentList where spare1 equals to UPDATED_SPARE_1
        defaultPaymentShouldNotBeFound("spare1.in=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllPaymentsBySpare1IsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where spare1 is not null
        defaultPaymentShouldBeFound("spare1.specified=true");

        // Get all the paymentList where spare1 is null
        defaultPaymentShouldNotBeFound("spare1.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsBySpare1ContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where spare1 contains DEFAULT_SPARE_1
        defaultPaymentShouldBeFound("spare1.contains=" + DEFAULT_SPARE_1);

        // Get all the paymentList where spare1 contains UPDATED_SPARE_1
        defaultPaymentShouldNotBeFound("spare1.contains=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllPaymentsBySpare1NotContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where spare1 does not contain DEFAULT_SPARE_1
        defaultPaymentShouldNotBeFound("spare1.doesNotContain=" + DEFAULT_SPARE_1);

        // Get all the paymentList where spare1 does not contain UPDATED_SPARE_1
        defaultPaymentShouldBeFound("spare1.doesNotContain=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllPaymentsBySpare2IsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where spare2 equals to DEFAULT_SPARE_2
        defaultPaymentShouldBeFound("spare2.equals=" + DEFAULT_SPARE_2);

        // Get all the paymentList where spare2 equals to UPDATED_SPARE_2
        defaultPaymentShouldNotBeFound("spare2.equals=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllPaymentsBySpare2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where spare2 not equals to DEFAULT_SPARE_2
        defaultPaymentShouldNotBeFound("spare2.notEquals=" + DEFAULT_SPARE_2);

        // Get all the paymentList where spare2 not equals to UPDATED_SPARE_2
        defaultPaymentShouldBeFound("spare2.notEquals=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllPaymentsBySpare2IsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where spare2 in DEFAULT_SPARE_2 or UPDATED_SPARE_2
        defaultPaymentShouldBeFound("spare2.in=" + DEFAULT_SPARE_2 + "," + UPDATED_SPARE_2);

        // Get all the paymentList where spare2 equals to UPDATED_SPARE_2
        defaultPaymentShouldNotBeFound("spare2.in=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllPaymentsBySpare2IsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where spare2 is not null
        defaultPaymentShouldBeFound("spare2.specified=true");

        // Get all the paymentList where spare2 is null
        defaultPaymentShouldNotBeFound("spare2.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsBySpare2ContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where spare2 contains DEFAULT_SPARE_2
        defaultPaymentShouldBeFound("spare2.contains=" + DEFAULT_SPARE_2);

        // Get all the paymentList where spare2 contains UPDATED_SPARE_2
        defaultPaymentShouldNotBeFound("spare2.contains=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllPaymentsBySpare2NotContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where spare2 does not contain DEFAULT_SPARE_2
        defaultPaymentShouldNotBeFound("spare2.doesNotContain=" + DEFAULT_SPARE_2);

        // Get all the paymentList where spare2 does not contain UPDATED_SPARE_2
        defaultPaymentShouldBeFound("spare2.doesNotContain=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllPaymentsBySpare3IsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where spare3 equals to DEFAULT_SPARE_3
        defaultPaymentShouldBeFound("spare3.equals=" + DEFAULT_SPARE_3);

        // Get all the paymentList where spare3 equals to UPDATED_SPARE_3
        defaultPaymentShouldNotBeFound("spare3.equals=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllPaymentsBySpare3IsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where spare3 not equals to DEFAULT_SPARE_3
        defaultPaymentShouldNotBeFound("spare3.notEquals=" + DEFAULT_SPARE_3);

        // Get all the paymentList where spare3 not equals to UPDATED_SPARE_3
        defaultPaymentShouldBeFound("spare3.notEquals=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllPaymentsBySpare3IsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where spare3 in DEFAULT_SPARE_3 or UPDATED_SPARE_3
        defaultPaymentShouldBeFound("spare3.in=" + DEFAULT_SPARE_3 + "," + UPDATED_SPARE_3);

        // Get all the paymentList where spare3 equals to UPDATED_SPARE_3
        defaultPaymentShouldNotBeFound("spare3.in=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllPaymentsBySpare3IsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where spare3 is not null
        defaultPaymentShouldBeFound("spare3.specified=true");

        // Get all the paymentList where spare3 is null
        defaultPaymentShouldNotBeFound("spare3.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsBySpare3ContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where spare3 contains DEFAULT_SPARE_3
        defaultPaymentShouldBeFound("spare3.contains=" + DEFAULT_SPARE_3);

        // Get all the paymentList where spare3 contains UPDATED_SPARE_3
        defaultPaymentShouldNotBeFound("spare3.contains=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllPaymentsBySpare3NotContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where spare3 does not contain DEFAULT_SPARE_3
        defaultPaymentShouldNotBeFound("spare3.doesNotContain=" + DEFAULT_SPARE_3);

        // Get all the paymentList where spare3 does not contain UPDATED_SPARE_3
        defaultPaymentShouldBeFound("spare3.doesNotContain=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllPaymentsByCommissionIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);
        Commission commission;
        if (TestUtil.findAll(em, Commission.class).isEmpty()) {
            commission = CommissionResourceIT.createEntity(em);
            em.persist(commission);
            em.flush();
        } else {
            commission = TestUtil.findAll(em, Commission.class).get(0);
        }
        em.persist(commission);
        em.flush();
        payment.addCommission(commission);
        paymentRepository.saveAndFlush(payment);
        Long commissionId = commission.getId();

        // Get all the paymentList where commission equals to commissionId
        defaultPaymentShouldBeFound("commissionId.equals=" + commissionId);

        // Get all the paymentList where commission equals to (commissionId + 1)
        defaultPaymentShouldNotBeFound("commissionId.equals=" + (commissionId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPaymentShouldBeFound(String filter) throws Exception {
        restPaymentMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(payment.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].senderMsisdn").value(hasItem(DEFAULT_SENDER_MSISDN)))
            .andExpect(jsonPath("$.[*].receiverMsisdn").value(hasItem(DEFAULT_RECEIVER_MSISDN)))
            .andExpect(jsonPath("$.[*].receiverProfile").value(hasItem(DEFAULT_RECEIVER_PROFILE)))
            .andExpect(jsonPath("$.[*].periodStartDate").value(hasItem(DEFAULT_PERIOD_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].periodEndDate").value(hasItem(DEFAULT_PERIOD_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].executionDuration").value(hasItem(DEFAULT_EXECUTION_DURATION.doubleValue())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].paymentStatus").value(hasItem(DEFAULT_PAYMENT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)));

        // Check, that the count call also returns 1
        restPaymentMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPaymentShouldNotBeFound(String filter) throws Exception {
        restPaymentMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPaymentMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingPayment() throws Exception {
        // Get the payment
        restPaymentMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPayment() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();

        // Update the payment
        Payment updatedPayment = paymentRepository.findById(payment.getId()).get();
        // Disconnect from session so that the updates on updatedPayment are not directly saved in db
        em.detach(updatedPayment);
        updatedPayment
            .amount(UPDATED_AMOUNT)
            .senderMsisdn(UPDATED_SENDER_MSISDN)
            .receiverMsisdn(UPDATED_RECEIVER_MSISDN)
            .receiverProfile(UPDATED_RECEIVER_PROFILE)
            .periodStartDate(UPDATED_PERIOD_START_DATE)
            .periodEndDate(UPDATED_PERIOD_END_DATE)
            .executionDuration(UPDATED_EXECUTION_DURATION)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .paymentStatus(UPDATED_PAYMENT_STATUS)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);
        PaymentDTO paymentDTO = paymentMapper.toDto(updatedPayment);

        restPaymentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, paymentDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(paymentDTO))
            )
            .andExpect(status().isOk());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
        Payment testPayment = paymentList.get(paymentList.size() - 1);
        assertThat(testPayment.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testPayment.getSenderMsisdn()).isEqualTo(UPDATED_SENDER_MSISDN);
        assertThat(testPayment.getReceiverMsisdn()).isEqualTo(UPDATED_RECEIVER_MSISDN);
        assertThat(testPayment.getReceiverProfile()).isEqualTo(UPDATED_RECEIVER_PROFILE);
        assertThat(testPayment.getPeriodStartDate()).isEqualTo(UPDATED_PERIOD_START_DATE);
        assertThat(testPayment.getPeriodEndDate()).isEqualTo(UPDATED_PERIOD_END_DATE);
        assertThat(testPayment.getExecutionDuration()).isEqualTo(UPDATED_EXECUTION_DURATION);
        assertThat(testPayment.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testPayment.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testPayment.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testPayment.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testPayment.getPaymentStatus()).isEqualTo(UPDATED_PAYMENT_STATUS);
        assertThat(testPayment.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testPayment.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testPayment.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void putNonExistingPayment() throws Exception {
        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();
        payment.setId(count.incrementAndGet());

        // Create the Payment
        PaymentDTO paymentDTO = paymentMapper.toDto(payment);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPaymentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, paymentDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(paymentDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPayment() throws Exception {
        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();
        payment.setId(count.incrementAndGet());

        // Create the Payment
        PaymentDTO paymentDTO = paymentMapper.toDto(payment);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(paymentDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPayment() throws Exception {
        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();
        payment.setId(count.incrementAndGet());

        // Create the Payment
        PaymentDTO paymentDTO = paymentMapper.toDto(payment);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePaymentWithPatch() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();

        // Update the payment using partial update
        Payment partialUpdatedPayment = new Payment();
        partialUpdatedPayment.setId(payment.getId());

        partialUpdatedPayment
            .senderMsisdn(UPDATED_SENDER_MSISDN)
            .periodStartDate(UPDATED_PERIOD_START_DATE)
            .executionDuration(UPDATED_EXECUTION_DURATION)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .spare1(UPDATED_SPARE_1);

        restPaymentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPayment.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPayment))
            )
            .andExpect(status().isOk());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
        Payment testPayment = paymentList.get(paymentList.size() - 1);
        assertThat(testPayment.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testPayment.getSenderMsisdn()).isEqualTo(UPDATED_SENDER_MSISDN);
        assertThat(testPayment.getReceiverMsisdn()).isEqualTo(DEFAULT_RECEIVER_MSISDN);
        assertThat(testPayment.getReceiverProfile()).isEqualTo(DEFAULT_RECEIVER_PROFILE);
        assertThat(testPayment.getPeriodStartDate()).isEqualTo(UPDATED_PERIOD_START_DATE);
        assertThat(testPayment.getPeriodEndDate()).isEqualTo(DEFAULT_PERIOD_END_DATE);
        assertThat(testPayment.getExecutionDuration()).isEqualTo(UPDATED_EXECUTION_DURATION);
        assertThat(testPayment.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testPayment.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testPayment.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testPayment.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testPayment.getPaymentStatus()).isEqualTo(DEFAULT_PAYMENT_STATUS);
        assertThat(testPayment.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testPayment.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
        assertThat(testPayment.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
    }

    @Test
    @Transactional
    void fullUpdatePaymentWithPatch() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();

        // Update the payment using partial update
        Payment partialUpdatedPayment = new Payment();
        partialUpdatedPayment.setId(payment.getId());

        partialUpdatedPayment
            .amount(UPDATED_AMOUNT)
            .senderMsisdn(UPDATED_SENDER_MSISDN)
            .receiverMsisdn(UPDATED_RECEIVER_MSISDN)
            .receiverProfile(UPDATED_RECEIVER_PROFILE)
            .periodStartDate(UPDATED_PERIOD_START_DATE)
            .periodEndDate(UPDATED_PERIOD_END_DATE)
            .executionDuration(UPDATED_EXECUTION_DURATION)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .paymentStatus(UPDATED_PAYMENT_STATUS)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);

        restPaymentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPayment.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPayment))
            )
            .andExpect(status().isOk());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
        Payment testPayment = paymentList.get(paymentList.size() - 1);
        assertThat(testPayment.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testPayment.getSenderMsisdn()).isEqualTo(UPDATED_SENDER_MSISDN);
        assertThat(testPayment.getReceiverMsisdn()).isEqualTo(UPDATED_RECEIVER_MSISDN);
        assertThat(testPayment.getReceiverProfile()).isEqualTo(UPDATED_RECEIVER_PROFILE);
        assertThat(testPayment.getPeriodStartDate()).isEqualTo(UPDATED_PERIOD_START_DATE);
        assertThat(testPayment.getPeriodEndDate()).isEqualTo(UPDATED_PERIOD_END_DATE);
        assertThat(testPayment.getExecutionDuration()).isEqualTo(UPDATED_EXECUTION_DURATION);
        assertThat(testPayment.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testPayment.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testPayment.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testPayment.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testPayment.getPaymentStatus()).isEqualTo(UPDATED_PAYMENT_STATUS);
        assertThat(testPayment.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testPayment.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testPayment.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void patchNonExistingPayment() throws Exception {
        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();
        payment.setId(count.incrementAndGet());

        // Create the Payment
        PaymentDTO paymentDTO = paymentMapper.toDto(payment);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPaymentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, paymentDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(paymentDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPayment() throws Exception {
        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();
        payment.setId(count.incrementAndGet());

        // Create the Payment
        PaymentDTO paymentDTO = paymentMapper.toDto(payment);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(paymentDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPayment() throws Exception {
        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();
        payment.setId(count.incrementAndGet());

        // Create the Payment
        PaymentDTO paymentDTO = paymentMapper.toDto(payment);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(paymentDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePayment() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        int databaseSizeBeforeDelete = paymentRepository.findAll().size();

        // Delete the payment
        restPaymentMockMvc
            .perform(delete(ENTITY_API_URL_ID, payment.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
