package sn.free.commissioning.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.IntegrationTest;
import sn.free.commissioning.domain.Commission;
import sn.free.commissioning.domain.Payment;
import sn.free.commissioning.domain.enumeration.PaymentStatus;
import sn.free.commissioning.repository.PaymentRepository;
import sn.free.commissioning.service.criteria.PaymentCriteria;
import sn.free.commissioning.service.dto.PaymentDTO;
import sn.free.commissioning.service.mapper.PaymentMapper;

/**
 * Integration tests for the {@link PaymentResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PaymentEngineResourceIT {

    private static final Double DEFAULT_AMOUT = 1D;
    private static final Double UPDATED_AMOUT = 2D;
    private static final Double SMALLER_AMOUT = 1D - 1D;

    private static final String DEFAULT_SENDER_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_SENDER_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_RECEIVER_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_RECEIVER_MSISDN = "BBBBBBBBBB";

    private static final Instant DEFAULT_START_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_START_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_END_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_END_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Double DEFAULT_DURATION = 1.0;
    private static final Double UPDATED_DURATION = 2.0;
    private static final Double SMALLER_DURATION = 1.0 - 1.0;

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final PaymentStatus DEFAULT_PAYMENT_STATUS = PaymentStatus.FAILURE;
    private static final PaymentStatus UPDATED_PAYMENT_STATUS = PaymentStatus.SUCCESS;

    private static final String ENTITY_API_URL = "/api/payments";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private PaymentMapper paymentMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPaymentMockMvc;

    private Payment payment;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Payment createEntity(EntityManager em) {
        Payment payment = new Payment()
            .amount(DEFAULT_AMOUT)
            .senderMsisdn(DEFAULT_SENDER_MSISDN)
            .receiverMsisdn(DEFAULT_RECEIVER_MSISDN)
            .periodStartDate(DEFAULT_START_DATE)
            .periodEndDate(DEFAULT_END_DATE)
            .executionDuration(DEFAULT_DURATION)
            .createdBy(DEFAULT_CREATED_BY)
            .paymentStatus(DEFAULT_PAYMENT_STATUS);
        return payment;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Payment createUpdatedEntity(EntityManager em) {
        Payment payment = new Payment()
            .amount(UPDATED_AMOUT)
            .senderMsisdn(UPDATED_SENDER_MSISDN)
            .receiverMsisdn(UPDATED_RECEIVER_MSISDN)
            .periodStartDate(UPDATED_START_DATE)
            .periodEndDate(UPDATED_END_DATE)
            .executionDuration(UPDATED_DURATION)
            .createdBy(UPDATED_CREATED_BY)
            .paymentStatus(UPDATED_PAYMENT_STATUS);
        return payment;
    }

    @BeforeEach
    public void initTest() {
        payment = createEntity(em);
    }

    @Test
    @Transactional
    void createPayment() throws Exception {
        int databaseSizeBeforeCreate = paymentRepository.findAll().size();
        // Create the Payment
        PaymentDTO paymentDTO = paymentMapper.toDto(payment);
        restPaymentMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentDTO)))
            .andExpect(status().isCreated());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeCreate + 1);
        Payment testPayment = paymentList.get(paymentList.size() - 1);
        assertThat(testPayment.getAmount()).isEqualTo(DEFAULT_AMOUT);
        assertThat(testPayment.getSenderMsisdn()).isEqualTo(DEFAULT_SENDER_MSISDN);
        assertThat(testPayment.getReceiverMsisdn()).isEqualTo(DEFAULT_RECEIVER_MSISDN);
        assertThat(testPayment.getPeriodStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testPayment.getPeriodEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testPayment.getExecutionDuration()).isEqualTo(DEFAULT_DURATION);
        assertThat(testPayment.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testPayment.getPaymentStatus()).isEqualTo(DEFAULT_PAYMENT_STATUS);
    }

    @Test
    @Transactional
    void createPaymentWithExistingId() throws Exception {
        // Create the Payment with an existing ID
        payment.setId(1L);
        PaymentDTO paymentDTO = paymentMapper.toDto(payment);

        int databaseSizeBeforeCreate = paymentRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPaymentMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkCreatedByIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentRepository.findAll().size();
        // set the field null
        payment.setCreatedBy(null);

        // Create the Payment, which fails.
        PaymentDTO paymentDTO = paymentMapper.toDto(payment);

        restPaymentMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentDTO)))
            .andExpect(status().isBadRequest());

        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllPayments() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList
        restPaymentMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(payment.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUT.doubleValue())))
            .andExpect(jsonPath("$.[*].senderMsisdn").value(hasItem(DEFAULT_SENDER_MSISDN)))
            .andExpect(jsonPath("$.[*].receiverMsisdn").value(hasItem(DEFAULT_RECEIVER_MSISDN)))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].duration").value(hasItem(DEFAULT_DURATION)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].paymentStatus").value(hasItem(DEFAULT_PAYMENT_STATUS.toString())));
    }

    @Test
    @Transactional
    void getPayment() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get the payment
        restPaymentMockMvc
            .perform(get(ENTITY_API_URL_ID, payment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(payment.getId().intValue()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUT.doubleValue()))
            .andExpect(jsonPath("$.senderMsisdn").value(DEFAULT_SENDER_MSISDN))
            .andExpect(jsonPath("$.receiverMsisdn").value(DEFAULT_RECEIVER_MSISDN))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()))
            .andExpect(jsonPath("$.duration").value(DEFAULT_DURATION))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.paymentStatus").value(DEFAULT_PAYMENT_STATUS.toString()));
    }

    @Test
    @Transactional
    void getPaymentsByIdFiltering() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        Long id = payment.getId();

        defaultPaymentShouldBeFound("id.equals=" + id);
        defaultPaymentShouldNotBeFound("id.notEquals=" + id);

        defaultPaymentShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPaymentShouldNotBeFound("id.greaterThan=" + id);

        defaultPaymentShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPaymentShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllPaymentsByAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where amount equals to DEFAULT_AMOUT
        defaultPaymentShouldBeFound("amount.equals=" + DEFAULT_AMOUT);

        // Get all the paymentList where amount equals to UPDATED_AMOUT
        defaultPaymentShouldNotBeFound("amount.equals=" + UPDATED_AMOUT);
    }

    @Test
    @Transactional
    void getAllPaymentsByAmountIsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where amount not equals to DEFAULT_AMOUT
        defaultPaymentShouldNotBeFound("amount.notEquals=" + DEFAULT_AMOUT);

        // Get all the paymentList where amount not equals to UPDATED_AMOUT
        defaultPaymentShouldBeFound("amount.notEquals=" + UPDATED_AMOUT);
    }

    @Test
    @Transactional
    void getAllPaymentsByAmountIsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where amount in DEFAULT_AMOUT or UPDATED_AMOUT
        defaultPaymentShouldBeFound("amount.in=" + DEFAULT_AMOUT + "," + UPDATED_AMOUT);

        // Get all the paymentList where amount equals to UPDATED_AMOUT
        defaultPaymentShouldNotBeFound("amount.in=" + UPDATED_AMOUT);
    }

    @Test
    @Transactional
    void getAllPaymentsByAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where amount is not null
        defaultPaymentShouldBeFound("amount.specified=true");

        // Get all the paymentList where amount is null
        defaultPaymentShouldNotBeFound("amount.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsByAmountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where amount is greater than or equal to DEFAULT_AMOUT
        defaultPaymentShouldBeFound("amount.greaterThanOrEqual=" + DEFAULT_AMOUT);

        // Get all the paymentList where amount is greater than or equal to UPDATED_AMOUT
        defaultPaymentShouldNotBeFound("amount.greaterThanOrEqual=" + UPDATED_AMOUT);
    }

    @Test
    @Transactional
    void getAllPaymentsByAmountIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where amount is less than or equal to DEFAULT_AMOUT
        defaultPaymentShouldBeFound("amount.lessThanOrEqual=" + DEFAULT_AMOUT);

        // Get all the paymentList where amount is less than or equal to SMALLER_AMOUT
        defaultPaymentShouldNotBeFound("amount.lessThanOrEqual=" + SMALLER_AMOUT);
    }

    @Test
    @Transactional
    void getAllPaymentsByAmountIsLessThanSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where amount is less than DEFAULT_AMOUT
        defaultPaymentShouldNotBeFound("amount.lessThan=" + DEFAULT_AMOUT);

        // Get all the paymentList where amount is less than UPDATED_AMOUT
        defaultPaymentShouldBeFound("amount.lessThan=" + UPDATED_AMOUT);
    }

    @Test
    @Transactional
    void getAllPaymentsByAmountIsGreaterThanSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where amount is greater than DEFAULT_AMOUT
        defaultPaymentShouldNotBeFound("amount.greaterThan=" + DEFAULT_AMOUT);

        // Get all the paymentList where amount is greater than SMALLER_AMOUT
        defaultPaymentShouldBeFound("amount.greaterThan=" + SMALLER_AMOUT);
    }

    @Test
    @Transactional
    void getAllPaymentsBySenderMsisdnIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where senderMsisdn equals to DEFAULT_SENDER_MSISDN
        defaultPaymentShouldBeFound("senderMsisdn.equals=" + DEFAULT_SENDER_MSISDN);

        // Get all the paymentList where senderMsisdn equals to UPDATED_SENDER_MSISDN
        defaultPaymentShouldNotBeFound("senderMsisdn.equals=" + UPDATED_SENDER_MSISDN);
    }

    @Test
    @Transactional
    void getAllPaymentsBySenderMsisdnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where senderMsisdn not equals to DEFAULT_SENDER_MSISDN
        defaultPaymentShouldNotBeFound("senderMsisdn.notEquals=" + DEFAULT_SENDER_MSISDN);

        // Get all the paymentList where senderMsisdn not equals to UPDATED_SENDER_MSISDN
        defaultPaymentShouldBeFound("senderMsisdn.notEquals=" + UPDATED_SENDER_MSISDN);
    }

    @Test
    @Transactional
    void getAllPaymentsBySenderMsisdnIsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where senderMsisdn in DEFAULT_SENDER_MSISDN or UPDATED_SENDER_MSISDN
        defaultPaymentShouldBeFound("senderMsisdn.in=" + DEFAULT_SENDER_MSISDN + "," + UPDATED_SENDER_MSISDN);

        // Get all the paymentList where senderMsisdn equals to UPDATED_SENDER_MSISDN
        defaultPaymentShouldNotBeFound("senderMsisdn.in=" + UPDATED_SENDER_MSISDN);
    }

    @Test
    @Transactional
    void getAllPaymentsBySenderMsisdnIsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where senderMsisdn is not null
        defaultPaymentShouldBeFound("senderMsisdn.specified=true");

        // Get all the paymentList where senderMsisdn is null
        defaultPaymentShouldNotBeFound("senderMsisdn.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsBySenderMsisdnContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where senderMsisdn contains DEFAULT_SENDER_MSISDN
        defaultPaymentShouldBeFound("senderMsisdn.contains=" + DEFAULT_SENDER_MSISDN);

        // Get all the paymentList where senderMsisdn contains UPDATED_SENDER_MSISDN
        defaultPaymentShouldNotBeFound("senderMsisdn.contains=" + UPDATED_SENDER_MSISDN);
    }

    @Test
    @Transactional
    void getAllPaymentsBySenderMsisdnNotContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where senderMsisdn does not contain DEFAULT_SENDER_MSISDN
        defaultPaymentShouldNotBeFound("senderMsisdn.doesNotContain=" + DEFAULT_SENDER_MSISDN);

        // Get all the paymentList where senderMsisdn does not contain UPDATED_SENDER_MSISDN
        defaultPaymentShouldBeFound("senderMsisdn.doesNotContain=" + UPDATED_SENDER_MSISDN);
    }

    @Test
    @Transactional
    void getAllPaymentsByReceiverMsisdnIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where receiverMsisdn equals to DEFAULT_RECEIVER_MSISDN
        defaultPaymentShouldBeFound("receiverMsisdn.equals=" + DEFAULT_RECEIVER_MSISDN);

        // Get all the paymentList where receiverMsisdn equals to UPDATED_RECEIVER_MSISDN
        defaultPaymentShouldNotBeFound("receiverMsisdn.equals=" + UPDATED_RECEIVER_MSISDN);
    }

    @Test
    @Transactional
    void getAllPaymentsByReceiverMsisdnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where receiverMsisdn not equals to DEFAULT_RECEIVER_MSISDN
        defaultPaymentShouldNotBeFound("receiverMsisdn.notEquals=" + DEFAULT_RECEIVER_MSISDN);

        // Get all the paymentList where receiverMsisdn not equals to UPDATED_RECEIVER_MSISDN
        defaultPaymentShouldBeFound("receiverMsisdn.notEquals=" + UPDATED_RECEIVER_MSISDN);
    }

    @Test
    @Transactional
    void getAllPaymentsByReceiverMsisdnIsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where receiverMsisdn in DEFAULT_RECEIVER_MSISDN or UPDATED_RECEIVER_MSISDN
        defaultPaymentShouldBeFound("receiverMsisdn.in=" + DEFAULT_RECEIVER_MSISDN + "," + UPDATED_RECEIVER_MSISDN);

        // Get all the paymentList where receiverMsisdn equals to UPDATED_RECEIVER_MSISDN
        defaultPaymentShouldNotBeFound("receiverMsisdn.in=" + UPDATED_RECEIVER_MSISDN);
    }

    @Test
    @Transactional
    void getAllPaymentsByReceiverMsisdnIsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where receiverMsisdn is not null
        defaultPaymentShouldBeFound("receiverMsisdn.specified=true");

        // Get all the paymentList where receiverMsisdn is null
        defaultPaymentShouldNotBeFound("receiverMsisdn.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsByReceiverMsisdnContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where receiverMsisdn contains DEFAULT_RECEIVER_MSISDN
        defaultPaymentShouldBeFound("receiverMsisdn.contains=" + DEFAULT_RECEIVER_MSISDN);

        // Get all the paymentList where receiverMsisdn contains UPDATED_RECEIVER_MSISDN
        defaultPaymentShouldNotBeFound("receiverMsisdn.contains=" + UPDATED_RECEIVER_MSISDN);
    }

    @Test
    @Transactional
    void getAllPaymentsByReceiverMsisdnNotContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where receiverMsisdn does not contain DEFAULT_RECEIVER_MSISDN
        defaultPaymentShouldNotBeFound("receiverMsisdn.doesNotContain=" + DEFAULT_RECEIVER_MSISDN);

        // Get all the paymentList where receiverMsisdn does not contain UPDATED_RECEIVER_MSISDN
        defaultPaymentShouldBeFound("receiverMsisdn.doesNotContain=" + UPDATED_RECEIVER_MSISDN);
    }

    @Test
    @Transactional
    void getAllPaymentsByStartDateIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where startDate equals to DEFAULT_START_DATE
        defaultPaymentShouldBeFound("startDate.equals=" + DEFAULT_START_DATE);

        // Get all the paymentList where startDate equals to UPDATED_START_DATE
        defaultPaymentShouldNotBeFound("startDate.equals=" + UPDATED_START_DATE);
    }

    @Test
    @Transactional
    void getAllPaymentsByStartDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where startDate not equals to DEFAULT_START_DATE
        defaultPaymentShouldNotBeFound("startDate.notEquals=" + DEFAULT_START_DATE);

        // Get all the paymentList where startDate not equals to UPDATED_START_DATE
        defaultPaymentShouldBeFound("startDate.notEquals=" + UPDATED_START_DATE);
    }

    @Test
    @Transactional
    void getAllPaymentsByStartDateIsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where startDate in DEFAULT_START_DATE or UPDATED_START_DATE
        defaultPaymentShouldBeFound("startDate.in=" + DEFAULT_START_DATE + "," + UPDATED_START_DATE);

        // Get all the paymentList where startDate equals to UPDATED_START_DATE
        defaultPaymentShouldNotBeFound("startDate.in=" + UPDATED_START_DATE);
    }

    @Test
    @Transactional
    void getAllPaymentsByStartDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where startDate is not null
        defaultPaymentShouldBeFound("startDate.specified=true");

        // Get all the paymentList where startDate is null
        defaultPaymentShouldNotBeFound("startDate.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsByEndDateIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where endDate equals to DEFAULT_END_DATE
        defaultPaymentShouldBeFound("endDate.equals=" + DEFAULT_END_DATE);

        // Get all the paymentList where endDate equals to UPDATED_END_DATE
        defaultPaymentShouldNotBeFound("endDate.equals=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    void getAllPaymentsByEndDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where endDate not equals to DEFAULT_END_DATE
        defaultPaymentShouldNotBeFound("endDate.notEquals=" + DEFAULT_END_DATE);

        // Get all the paymentList where endDate not equals to UPDATED_END_DATE
        defaultPaymentShouldBeFound("endDate.notEquals=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    void getAllPaymentsByEndDateIsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where endDate in DEFAULT_END_DATE or UPDATED_END_DATE
        defaultPaymentShouldBeFound("endDate.in=" + DEFAULT_END_DATE + "," + UPDATED_END_DATE);

        // Get all the paymentList where endDate equals to UPDATED_END_DATE
        defaultPaymentShouldNotBeFound("endDate.in=" + UPDATED_END_DATE);
    }

    @Test
    @Transactional
    void getAllPaymentsByEndDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where endDate is not null
        defaultPaymentShouldBeFound("endDate.specified=true");

        // Get all the paymentList where endDate is null
        defaultPaymentShouldNotBeFound("endDate.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsByDurationIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where duration equals to DEFAULT_DURATION
        defaultPaymentShouldBeFound("duration.equals=" + DEFAULT_DURATION);

        // Get all the paymentList where duration equals to UPDATED_DURATION
        defaultPaymentShouldNotBeFound("duration.equals=" + UPDATED_DURATION);
    }

    @Test
    @Transactional
    void getAllPaymentsByDurationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where duration not equals to DEFAULT_DURATION
        defaultPaymentShouldNotBeFound("duration.notEquals=" + DEFAULT_DURATION);

        // Get all the paymentList where duration not equals to UPDATED_DURATION
        defaultPaymentShouldBeFound("duration.notEquals=" + UPDATED_DURATION);
    }

    @Test
    @Transactional
    void getAllPaymentsByDurationIsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where duration in DEFAULT_DURATION or UPDATED_DURATION
        defaultPaymentShouldBeFound("duration.in=" + DEFAULT_DURATION + "," + UPDATED_DURATION);

        // Get all the paymentList where duration equals to UPDATED_DURATION
        defaultPaymentShouldNotBeFound("duration.in=" + UPDATED_DURATION);
    }

    @Test
    @Transactional
    void getAllPaymentsByDurationIsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where duration is not null
        defaultPaymentShouldBeFound("duration.specified=true");

        // Get all the paymentList where duration is null
        defaultPaymentShouldNotBeFound("duration.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsByDurationIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where duration is greater than or equal to DEFAULT_DURATION
        defaultPaymentShouldBeFound("duration.greaterThanOrEqual=" + DEFAULT_DURATION);

        // Get all the paymentList where duration is greater than or equal to UPDATED_DURATION
        defaultPaymentShouldNotBeFound("duration.greaterThanOrEqual=" + UPDATED_DURATION);
    }

    @Test
    @Transactional
    void getAllPaymentsByDurationIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where duration is less than or equal to DEFAULT_DURATION
        defaultPaymentShouldBeFound("duration.lessThanOrEqual=" + DEFAULT_DURATION);

        // Get all the paymentList where duration is less than or equal to SMALLER_DURATION
        defaultPaymentShouldNotBeFound("duration.lessThanOrEqual=" + SMALLER_DURATION);
    }

    @Test
    @Transactional
    void getAllPaymentsByDurationIsLessThanSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where duration is less than DEFAULT_DURATION
        defaultPaymentShouldNotBeFound("duration.lessThan=" + DEFAULT_DURATION);

        // Get all the paymentList where duration is less than UPDATED_DURATION
        defaultPaymentShouldBeFound("duration.lessThan=" + UPDATED_DURATION);
    }

    @Test
    @Transactional
    void getAllPaymentsByDurationIsGreaterThanSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where duration is greater than DEFAULT_DURATION
        defaultPaymentShouldNotBeFound("duration.greaterThan=" + DEFAULT_DURATION);

        // Get all the paymentList where duration is greater than SMALLER_DURATION
        defaultPaymentShouldBeFound("duration.greaterThan=" + SMALLER_DURATION);
    }

    @Test
    @Transactional
    void getAllPaymentsByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where createdBy equals to DEFAULT_CREATED_BY
        defaultPaymentShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the paymentList where createdBy equals to UPDATED_CREATED_BY
        defaultPaymentShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPaymentsByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where createdBy not equals to DEFAULT_CREATED_BY
        defaultPaymentShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the paymentList where createdBy not equals to UPDATED_CREATED_BY
        defaultPaymentShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPaymentsByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultPaymentShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the paymentList where createdBy equals to UPDATED_CREATED_BY
        defaultPaymentShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPaymentsByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where createdBy is not null
        defaultPaymentShouldBeFound("createdBy.specified=true");

        // Get all the paymentList where createdBy is null
        defaultPaymentShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where createdBy contains DEFAULT_CREATED_BY
        defaultPaymentShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the paymentList where createdBy contains UPDATED_CREATED_BY
        defaultPaymentShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPaymentsByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where createdBy does not contain DEFAULT_CREATED_BY
        defaultPaymentShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the paymentList where createdBy does not contain UPDATED_CREATED_BY
        defaultPaymentShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllPaymentsByPaymentStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where paymentStatus equals to DEFAULT_PAYMENT_STATUS
        defaultPaymentShouldBeFound("paymentStatus.equals=" + DEFAULT_PAYMENT_STATUS);

        // Get all the paymentList where paymentStatus equals to UPDATED_PAYMENT_STATUS
        defaultPaymentShouldNotBeFound("paymentStatus.equals=" + UPDATED_PAYMENT_STATUS);
    }

    @Test
    @Transactional
    void getAllPaymentsByPaymentStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where paymentStatus not equals to DEFAULT_PAYMENT_STATUS
        defaultPaymentShouldNotBeFound("paymentStatus.notEquals=" + DEFAULT_PAYMENT_STATUS);

        // Get all the paymentList where paymentStatus not equals to UPDATED_PAYMENT_STATUS
        defaultPaymentShouldBeFound("paymentStatus.notEquals=" + UPDATED_PAYMENT_STATUS);
    }

    @Test
    @Transactional
    void getAllPaymentsByPaymentStatusIsInShouldWork() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where paymentStatus in DEFAULT_PAYMENT_STATUS or UPDATED_PAYMENT_STATUS
        defaultPaymentShouldBeFound("paymentStatus.in=" + DEFAULT_PAYMENT_STATUS + "," + UPDATED_PAYMENT_STATUS);

        // Get all the paymentList where paymentStatus equals to UPDATED_PAYMENT_STATUS
        defaultPaymentShouldNotBeFound("paymentStatus.in=" + UPDATED_PAYMENT_STATUS);
    }

    @Test
    @Transactional
    void getAllPaymentsByPaymentStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList where paymentStatus is not null
        defaultPaymentShouldBeFound("paymentStatus.specified=true");

        // Get all the paymentList where paymentStatus is null
        defaultPaymentShouldNotBeFound("paymentStatus.specified=false");
    }

    @Test
    @Transactional
    void getAllPaymentsByCommissionIsEqualToSomething() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);
        Commission commission;
        if (TestUtil.findAll(em, Commission.class).isEmpty()) {
            commission = CommissionResourceIT.createEntity(em);
            em.persist(commission);
            em.flush();
        } else {
            commission = TestUtil.findAll(em, Commission.class).get(0);
        }
        em.persist(commission);
        em.flush();
        payment.addCommission(commission);
        paymentRepository.saveAndFlush(payment);
        Long commissionId = commission.getId();

        // Get all the paymentList where commission equals to commissionId
        defaultPaymentShouldBeFound("commissionId.equals=" + commissionId);

        // Get all the paymentList where commission equals to (commissionId + 1)
        defaultPaymentShouldNotBeFound("commissionId.equals=" + (commissionId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPaymentShouldBeFound(String filter) throws Exception {
        restPaymentMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(payment.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUT.doubleValue())))
            .andExpect(jsonPath("$.[*].senderMsisdn").value(hasItem(DEFAULT_SENDER_MSISDN)))
            .andExpect(jsonPath("$.[*].receiverMsisdn").value(hasItem(DEFAULT_RECEIVER_MSISDN)))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].duration").value(hasItem(DEFAULT_DURATION)))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].paymentStatus").value(hasItem(DEFAULT_PAYMENT_STATUS.toString())));

        // Check, that the count call also returns 1
        restPaymentMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPaymentShouldNotBeFound(String filter) throws Exception {
        restPaymentMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPaymentMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingPayment() throws Exception {
        // Get the payment
        restPaymentMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPayment() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();

        // Update the payment
        Payment updatedPayment = paymentRepository.findById(payment.getId()).get();
        // Disconnect from session so that the updates on updatedPayment are not directly saved in db
        em.detach(updatedPayment);
        updatedPayment
            .amount(UPDATED_AMOUT)
            .senderMsisdn(UPDATED_SENDER_MSISDN)
            .receiverMsisdn(UPDATED_RECEIVER_MSISDN)
            .periodStartDate(UPDATED_START_DATE)
            .periodEndDate(UPDATED_END_DATE)
            .executionDuration(UPDATED_DURATION)
            .createdBy(UPDATED_CREATED_BY)
            .paymentStatus(UPDATED_PAYMENT_STATUS);
        PaymentDTO paymentDTO = paymentMapper.toDto(updatedPayment);

        restPaymentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, paymentDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(paymentDTO))
            )
            .andExpect(status().isOk());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
        Payment testPayment = paymentList.get(paymentList.size() - 1);
        assertThat(testPayment.getAmount()).isEqualTo(UPDATED_AMOUT);
        assertThat(testPayment.getSenderMsisdn()).isEqualTo(UPDATED_SENDER_MSISDN);
        assertThat(testPayment.getReceiverMsisdn()).isEqualTo(UPDATED_RECEIVER_MSISDN);
        assertThat(testPayment.getPeriodStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testPayment.getPeriodEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testPayment.getExecutionDuration()).isEqualTo(UPDATED_DURATION);
        assertThat(testPayment.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testPayment.getPaymentStatus()).isEqualTo(UPDATED_PAYMENT_STATUS);
    }

    @Test
    @Transactional
    void putNonExistingPayment() throws Exception {
        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();
        payment.setId(count.incrementAndGet());

        // Create the Payment
        PaymentDTO paymentDTO = paymentMapper.toDto(payment);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPaymentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, paymentDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(paymentDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPayment() throws Exception {
        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();
        payment.setId(count.incrementAndGet());

        // Create the Payment
        PaymentDTO paymentDTO = paymentMapper.toDto(payment);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(paymentDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPayment() throws Exception {
        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();
        payment.setId(count.incrementAndGet());

        // Create the Payment
        PaymentDTO paymentDTO = paymentMapper.toDto(payment);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(paymentDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePaymentWithPatch() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();

        // Update the payment using partial update
        Payment partialUpdatedPayment = new Payment();
        partialUpdatedPayment.setId(payment.getId());

        partialUpdatedPayment.senderMsisdn(UPDATED_SENDER_MSISDN).periodEndDate(UPDATED_END_DATE).createdBy(UPDATED_CREATED_BY);

        restPaymentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPayment.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPayment))
            )
            .andExpect(status().isOk());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
        Payment testPayment = paymentList.get(paymentList.size() - 1);
        assertThat(testPayment.getAmount()).isEqualTo(DEFAULT_AMOUT);
        assertThat(testPayment.getSenderMsisdn()).isEqualTo(UPDATED_SENDER_MSISDN);
        assertThat(testPayment.getReceiverMsisdn()).isEqualTo(DEFAULT_RECEIVER_MSISDN);
        assertThat(testPayment.getPeriodStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testPayment.getPeriodEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testPayment.getExecutionDuration()).isEqualTo(DEFAULT_DURATION);
        assertThat(testPayment.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testPayment.getPaymentStatus()).isEqualTo(DEFAULT_PAYMENT_STATUS);
    }

    @Test
    @Transactional
    void fullUpdatePaymentWithPatch() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();

        // Update the payment using partial update
        Payment partialUpdatedPayment = new Payment();
        partialUpdatedPayment.setId(payment.getId());

        partialUpdatedPayment
            .amount(UPDATED_AMOUT)
            .senderMsisdn(UPDATED_SENDER_MSISDN)
            .receiverMsisdn(UPDATED_RECEIVER_MSISDN)
            .periodStartDate(UPDATED_START_DATE)
            .periodEndDate(UPDATED_END_DATE)
            .executionDuration(UPDATED_DURATION)
            .createdBy(UPDATED_CREATED_BY)
            .paymentStatus(UPDATED_PAYMENT_STATUS);

        restPaymentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPayment.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPayment))
            )
            .andExpect(status().isOk());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
        Payment testPayment = paymentList.get(paymentList.size() - 1);
        assertThat(testPayment.getAmount()).isEqualTo(UPDATED_AMOUT);
        assertThat(testPayment.getSenderMsisdn()).isEqualTo(UPDATED_SENDER_MSISDN);
        assertThat(testPayment.getReceiverMsisdn()).isEqualTo(UPDATED_RECEIVER_MSISDN);
        assertThat(testPayment.getPeriodStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testPayment.getPeriodEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testPayment.getExecutionDuration()).isEqualTo(UPDATED_DURATION);
        assertThat(testPayment.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testPayment.getPaymentStatus()).isEqualTo(UPDATED_PAYMENT_STATUS);
    }

    @Test
    @Transactional
    void patchNonExistingPayment() throws Exception {
        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();
        payment.setId(count.incrementAndGet());

        // Create the Payment
        PaymentDTO paymentDTO = paymentMapper.toDto(payment);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPaymentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, paymentDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(paymentDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPayment() throws Exception {
        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();
        payment.setId(count.incrementAndGet());

        // Create the Payment
        PaymentDTO paymentDTO = paymentMapper.toDto(payment);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(paymentDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPayment() throws Exception {
        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();
        payment.setId(count.incrementAndGet());

        // Create the Payment
        PaymentDTO paymentDTO = paymentMapper.toDto(payment);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPaymentMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(paymentDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePayment() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        int databaseSizeBeforeDelete = paymentRepository.findAll().size();

        // Delete the payment
        restPaymentMockMvc
            .perform(delete(ENTITY_API_URL_ID, payment.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
