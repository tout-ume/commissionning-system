package sn.free.commissioning.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.IntegrationTest;
import sn.free.commissioning.domain.CalculationExecutionTrace;
import sn.free.commissioning.repository.CalculationExecutionTraceRepository;
import sn.free.commissioning.service.criteria.CalculationExecutionTraceCriteria;
import sn.free.commissioning.service.dto.CalculationExecutionTraceDTO;
import sn.free.commissioning.service.mapper.CalculationExecutionTraceMapper;

/**
 * Integration tests for the {@link CalculationExecutionTraceResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class CalculationExecutionTraceResourceIT {

    private static final Double DEFAULT_FREQUENCY_ID = 1D;
    private static final Double UPDATED_FREQUENCY_ID = 2D;
    private static final Double SMALLER_FREQUENCY_ID = 1D - 1D;

    private static final Instant DEFAULT_EXECUTION_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_EXECUTION_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_IS_EXECUTED = false;
    private static final Boolean UPDATED_IS_EXECUTED = true;

    private static final String DEFAULT_EXECUTION_RESULT = "AAAAAAAAAA";
    private static final String UPDATED_EXECUTION_RESULT = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_1 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_2 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_3 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_3 = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/calculation-execution-traces";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CalculationExecutionTraceRepository calculationExecutionTraceRepository;

    @Autowired
    private CalculationExecutionTraceMapper calculationExecutionTraceMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCalculationExecutionTraceMockMvc;

    private CalculationExecutionTrace calculationExecutionTrace;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CalculationExecutionTrace createEntity(EntityManager em) {
        CalculationExecutionTrace calculationExecutionTrace = new CalculationExecutionTrace()
            .frequencyId(DEFAULT_FREQUENCY_ID)
            .executionDate(DEFAULT_EXECUTION_DATE)
            .isExecuted(DEFAULT_IS_EXECUTED)
            .executionResult(DEFAULT_EXECUTION_RESULT)
            .spare1(DEFAULT_SPARE_1)
            .spare2(DEFAULT_SPARE_2)
            .spare3(DEFAULT_SPARE_3);
        return calculationExecutionTrace;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CalculationExecutionTrace createUpdatedEntity(EntityManager em) {
        CalculationExecutionTrace calculationExecutionTrace = new CalculationExecutionTrace()
            .frequencyId(UPDATED_FREQUENCY_ID)
            .executionDate(UPDATED_EXECUTION_DATE)
            .isExecuted(UPDATED_IS_EXECUTED)
            .executionResult(UPDATED_EXECUTION_RESULT)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);
        return calculationExecutionTrace;
    }

    @BeforeEach
    public void initTest() {
        calculationExecutionTrace = createEntity(em);
    }

    @Test
    @Transactional
    void createCalculationExecutionTrace() throws Exception {
        int databaseSizeBeforeCreate = calculationExecutionTraceRepository.findAll().size();
        // Create the CalculationExecutionTrace
        CalculationExecutionTraceDTO calculationExecutionTraceDTO = calculationExecutionTraceMapper.toDto(calculationExecutionTrace);
        restCalculationExecutionTraceMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(calculationExecutionTraceDTO))
            )
            .andExpect(status().isCreated());

        // Validate the CalculationExecutionTrace in the database
        List<CalculationExecutionTrace> calculationExecutionTraceList = calculationExecutionTraceRepository.findAll();
        assertThat(calculationExecutionTraceList).hasSize(databaseSizeBeforeCreate + 1);
        CalculationExecutionTrace testCalculationExecutionTrace = calculationExecutionTraceList.get(
            calculationExecutionTraceList.size() - 1
        );
        assertThat(testCalculationExecutionTrace.getFrequencyId()).isEqualTo(DEFAULT_FREQUENCY_ID);
        assertThat(testCalculationExecutionTrace.getExecutionDate()).isEqualTo(DEFAULT_EXECUTION_DATE);
        assertThat(testCalculationExecutionTrace.getIsExecuted()).isEqualTo(DEFAULT_IS_EXECUTED);
        assertThat(testCalculationExecutionTrace.getExecutionResult()).isEqualTo(DEFAULT_EXECUTION_RESULT);
        assertThat(testCalculationExecutionTrace.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testCalculationExecutionTrace.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
        assertThat(testCalculationExecutionTrace.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
    }

    @Test
    @Transactional
    void createCalculationExecutionTraceWithExistingId() throws Exception {
        // Create the CalculationExecutionTrace with an existing ID
        calculationExecutionTrace.setId(1L);
        CalculationExecutionTraceDTO calculationExecutionTraceDTO = calculationExecutionTraceMapper.toDto(calculationExecutionTrace);

        int databaseSizeBeforeCreate = calculationExecutionTraceRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCalculationExecutionTraceMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(calculationExecutionTraceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CalculationExecutionTrace in the database
        List<CalculationExecutionTrace> calculationExecutionTraceList = calculationExecutionTraceRepository.findAll();
        assertThat(calculationExecutionTraceList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkFrequencyIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = calculationExecutionTraceRepository.findAll().size();
        // set the field null
        calculationExecutionTrace.setFrequencyId(null);

        // Create the CalculationExecutionTrace, which fails.
        CalculationExecutionTraceDTO calculationExecutionTraceDTO = calculationExecutionTraceMapper.toDto(calculationExecutionTrace);

        restCalculationExecutionTraceMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(calculationExecutionTraceDTO))
            )
            .andExpect(status().isBadRequest());

        List<CalculationExecutionTrace> calculationExecutionTraceList = calculationExecutionTraceRepository.findAll();
        assertThat(calculationExecutionTraceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkExecutionDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = calculationExecutionTraceRepository.findAll().size();
        // set the field null
        calculationExecutionTrace.setExecutionDate(null);

        // Create the CalculationExecutionTrace, which fails.
        CalculationExecutionTraceDTO calculationExecutionTraceDTO = calculationExecutionTraceMapper.toDto(calculationExecutionTrace);

        restCalculationExecutionTraceMockMvc
            .perform(
                post(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(calculationExecutionTraceDTO))
            )
            .andExpect(status().isBadRequest());

        List<CalculationExecutionTrace> calculationExecutionTraceList = calculationExecutionTraceRepository.findAll();
        assertThat(calculationExecutionTraceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTraces() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList
        restCalculationExecutionTraceMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(calculationExecutionTrace.getId().intValue())))
            .andExpect(jsonPath("$.[*].frequencyId").value(hasItem(DEFAULT_FREQUENCY_ID.doubleValue())))
            .andExpect(jsonPath("$.[*].executionDate").value(hasItem(DEFAULT_EXECUTION_DATE.toString())))
            .andExpect(jsonPath("$.[*].isExecuted").value(hasItem(DEFAULT_IS_EXECUTED.booleanValue())))
            .andExpect(jsonPath("$.[*].executionResult").value(hasItem(DEFAULT_EXECUTION_RESULT)))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)));
    }

    @Test
    @Transactional
    void getCalculationExecutionTrace() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get the calculationExecutionTrace
        restCalculationExecutionTraceMockMvc
            .perform(get(ENTITY_API_URL_ID, calculationExecutionTrace.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(calculationExecutionTrace.getId().intValue()))
            .andExpect(jsonPath("$.frequencyId").value(DEFAULT_FREQUENCY_ID.doubleValue()))
            .andExpect(jsonPath("$.executionDate").value(DEFAULT_EXECUTION_DATE.toString()))
            .andExpect(jsonPath("$.isExecuted").value(DEFAULT_IS_EXECUTED.booleanValue()))
            .andExpect(jsonPath("$.executionResult").value(DEFAULT_EXECUTION_RESULT))
            .andExpect(jsonPath("$.spare1").value(DEFAULT_SPARE_1))
            .andExpect(jsonPath("$.spare2").value(DEFAULT_SPARE_2))
            .andExpect(jsonPath("$.spare3").value(DEFAULT_SPARE_3));
    }

    @Test
    @Transactional
    void getCalculationExecutionTracesByIdFiltering() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        Long id = calculationExecutionTrace.getId();

        defaultCalculationExecutionTraceShouldBeFound("id.equals=" + id);
        defaultCalculationExecutionTraceShouldNotBeFound("id.notEquals=" + id);

        defaultCalculationExecutionTraceShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCalculationExecutionTraceShouldNotBeFound("id.greaterThan=" + id);

        defaultCalculationExecutionTraceShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCalculationExecutionTraceShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByFrequencyIdIsEqualToSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where frequencyId equals to DEFAULT_FREQUENCY_ID
        defaultCalculationExecutionTraceShouldBeFound("frequencyId.equals=" + DEFAULT_FREQUENCY_ID);

        // Get all the calculationExecutionTraceList where frequencyId equals to UPDATED_FREQUENCY_ID
        defaultCalculationExecutionTraceShouldNotBeFound("frequencyId.equals=" + UPDATED_FREQUENCY_ID);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByFrequencyIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where frequencyId not equals to DEFAULT_FREQUENCY_ID
        defaultCalculationExecutionTraceShouldNotBeFound("frequencyId.notEquals=" + DEFAULT_FREQUENCY_ID);

        // Get all the calculationExecutionTraceList where frequencyId not equals to UPDATED_FREQUENCY_ID
        defaultCalculationExecutionTraceShouldBeFound("frequencyId.notEquals=" + UPDATED_FREQUENCY_ID);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByFrequencyIdIsInShouldWork() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where frequencyId in DEFAULT_FREQUENCY_ID or UPDATED_FREQUENCY_ID
        defaultCalculationExecutionTraceShouldBeFound("frequencyId.in=" + DEFAULT_FREQUENCY_ID + "," + UPDATED_FREQUENCY_ID);

        // Get all the calculationExecutionTraceList where frequencyId equals to UPDATED_FREQUENCY_ID
        defaultCalculationExecutionTraceShouldNotBeFound("frequencyId.in=" + UPDATED_FREQUENCY_ID);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByFrequencyIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where frequencyId is not null
        defaultCalculationExecutionTraceShouldBeFound("frequencyId.specified=true");

        // Get all the calculationExecutionTraceList where frequencyId is null
        defaultCalculationExecutionTraceShouldNotBeFound("frequencyId.specified=false");
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByFrequencyIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where frequencyId is greater than or equal to DEFAULT_FREQUENCY_ID
        defaultCalculationExecutionTraceShouldBeFound("frequencyId.greaterThanOrEqual=" + DEFAULT_FREQUENCY_ID);

        // Get all the calculationExecutionTraceList where frequencyId is greater than or equal to UPDATED_FREQUENCY_ID
        defaultCalculationExecutionTraceShouldNotBeFound("frequencyId.greaterThanOrEqual=" + UPDATED_FREQUENCY_ID);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByFrequencyIdIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where frequencyId is less than or equal to DEFAULT_FREQUENCY_ID
        defaultCalculationExecutionTraceShouldBeFound("frequencyId.lessThanOrEqual=" + DEFAULT_FREQUENCY_ID);

        // Get all the calculationExecutionTraceList where frequencyId is less than or equal to SMALLER_FREQUENCY_ID
        defaultCalculationExecutionTraceShouldNotBeFound("frequencyId.lessThanOrEqual=" + SMALLER_FREQUENCY_ID);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByFrequencyIdIsLessThanSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where frequencyId is less than DEFAULT_FREQUENCY_ID
        defaultCalculationExecutionTraceShouldNotBeFound("frequencyId.lessThan=" + DEFAULT_FREQUENCY_ID);

        // Get all the calculationExecutionTraceList where frequencyId is less than UPDATED_FREQUENCY_ID
        defaultCalculationExecutionTraceShouldBeFound("frequencyId.lessThan=" + UPDATED_FREQUENCY_ID);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByFrequencyIdIsGreaterThanSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where frequencyId is greater than DEFAULT_FREQUENCY_ID
        defaultCalculationExecutionTraceShouldNotBeFound("frequencyId.greaterThan=" + DEFAULT_FREQUENCY_ID);

        // Get all the calculationExecutionTraceList where frequencyId is greater than SMALLER_FREQUENCY_ID
        defaultCalculationExecutionTraceShouldBeFound("frequencyId.greaterThan=" + SMALLER_FREQUENCY_ID);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByExecutionDateIsEqualToSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where executionDate equals to DEFAULT_EXECUTION_DATE
        defaultCalculationExecutionTraceShouldBeFound("executionDate.equals=" + DEFAULT_EXECUTION_DATE);

        // Get all the calculationExecutionTraceList where executionDate equals to UPDATED_EXECUTION_DATE
        defaultCalculationExecutionTraceShouldNotBeFound("executionDate.equals=" + UPDATED_EXECUTION_DATE);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByExecutionDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where executionDate not equals to DEFAULT_EXECUTION_DATE
        defaultCalculationExecutionTraceShouldNotBeFound("executionDate.notEquals=" + DEFAULT_EXECUTION_DATE);

        // Get all the calculationExecutionTraceList where executionDate not equals to UPDATED_EXECUTION_DATE
        defaultCalculationExecutionTraceShouldBeFound("executionDate.notEquals=" + UPDATED_EXECUTION_DATE);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByExecutionDateIsInShouldWork() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where executionDate in DEFAULT_EXECUTION_DATE or UPDATED_EXECUTION_DATE
        defaultCalculationExecutionTraceShouldBeFound("executionDate.in=" + DEFAULT_EXECUTION_DATE + "," + UPDATED_EXECUTION_DATE);

        // Get all the calculationExecutionTraceList where executionDate equals to UPDATED_EXECUTION_DATE
        defaultCalculationExecutionTraceShouldNotBeFound("executionDate.in=" + UPDATED_EXECUTION_DATE);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByExecutionDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where executionDate is not null
        defaultCalculationExecutionTraceShouldBeFound("executionDate.specified=true");

        // Get all the calculationExecutionTraceList where executionDate is null
        defaultCalculationExecutionTraceShouldNotBeFound("executionDate.specified=false");
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByIsExecutedIsEqualToSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where isExecuted equals to DEFAULT_IS_EXECUTED
        defaultCalculationExecutionTraceShouldBeFound("isExecuted.equals=" + DEFAULT_IS_EXECUTED);

        // Get all the calculationExecutionTraceList where isExecuted equals to UPDATED_IS_EXECUTED
        defaultCalculationExecutionTraceShouldNotBeFound("isExecuted.equals=" + UPDATED_IS_EXECUTED);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByIsExecutedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where isExecuted not equals to DEFAULT_IS_EXECUTED
        defaultCalculationExecutionTraceShouldNotBeFound("isExecuted.notEquals=" + DEFAULT_IS_EXECUTED);

        // Get all the calculationExecutionTraceList where isExecuted not equals to UPDATED_IS_EXECUTED
        defaultCalculationExecutionTraceShouldBeFound("isExecuted.notEquals=" + UPDATED_IS_EXECUTED);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByIsExecutedIsInShouldWork() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where isExecuted in DEFAULT_IS_EXECUTED or UPDATED_IS_EXECUTED
        defaultCalculationExecutionTraceShouldBeFound("isExecuted.in=" + DEFAULT_IS_EXECUTED + "," + UPDATED_IS_EXECUTED);

        // Get all the calculationExecutionTraceList where isExecuted equals to UPDATED_IS_EXECUTED
        defaultCalculationExecutionTraceShouldNotBeFound("isExecuted.in=" + UPDATED_IS_EXECUTED);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByIsExecutedIsNullOrNotNull() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where isExecuted is not null
        defaultCalculationExecutionTraceShouldBeFound("isExecuted.specified=true");

        // Get all the calculationExecutionTraceList where isExecuted is null
        defaultCalculationExecutionTraceShouldNotBeFound("isExecuted.specified=false");
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByExecutionResultIsEqualToSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where executionResult equals to DEFAULT_EXECUTION_RESULT
        defaultCalculationExecutionTraceShouldBeFound("executionResult.equals=" + DEFAULT_EXECUTION_RESULT);

        // Get all the calculationExecutionTraceList where executionResult equals to UPDATED_EXECUTION_RESULT
        defaultCalculationExecutionTraceShouldNotBeFound("executionResult.equals=" + UPDATED_EXECUTION_RESULT);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByExecutionResultIsNotEqualToSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where executionResult not equals to DEFAULT_EXECUTION_RESULT
        defaultCalculationExecutionTraceShouldNotBeFound("executionResult.notEquals=" + DEFAULT_EXECUTION_RESULT);

        // Get all the calculationExecutionTraceList where executionResult not equals to UPDATED_EXECUTION_RESULT
        defaultCalculationExecutionTraceShouldBeFound("executionResult.notEquals=" + UPDATED_EXECUTION_RESULT);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByExecutionResultIsInShouldWork() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where executionResult in DEFAULT_EXECUTION_RESULT or UPDATED_EXECUTION_RESULT
        defaultCalculationExecutionTraceShouldBeFound("executionResult.in=" + DEFAULT_EXECUTION_RESULT + "," + UPDATED_EXECUTION_RESULT);

        // Get all the calculationExecutionTraceList where executionResult equals to UPDATED_EXECUTION_RESULT
        defaultCalculationExecutionTraceShouldNotBeFound("executionResult.in=" + UPDATED_EXECUTION_RESULT);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByExecutionResultIsNullOrNotNull() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where executionResult is not null
        defaultCalculationExecutionTraceShouldBeFound("executionResult.specified=true");

        // Get all the calculationExecutionTraceList where executionResult is null
        defaultCalculationExecutionTraceShouldNotBeFound("executionResult.specified=false");
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByExecutionResultContainsSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where executionResult contains DEFAULT_EXECUTION_RESULT
        defaultCalculationExecutionTraceShouldBeFound("executionResult.contains=" + DEFAULT_EXECUTION_RESULT);

        // Get all the calculationExecutionTraceList where executionResult contains UPDATED_EXECUTION_RESULT
        defaultCalculationExecutionTraceShouldNotBeFound("executionResult.contains=" + UPDATED_EXECUTION_RESULT);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesByExecutionResultNotContainsSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where executionResult does not contain DEFAULT_EXECUTION_RESULT
        defaultCalculationExecutionTraceShouldNotBeFound("executionResult.doesNotContain=" + DEFAULT_EXECUTION_RESULT);

        // Get all the calculationExecutionTraceList where executionResult does not contain UPDATED_EXECUTION_RESULT
        defaultCalculationExecutionTraceShouldBeFound("executionResult.doesNotContain=" + UPDATED_EXECUTION_RESULT);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesBySpare1IsEqualToSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where spare1 equals to DEFAULT_SPARE_1
        defaultCalculationExecutionTraceShouldBeFound("spare1.equals=" + DEFAULT_SPARE_1);

        // Get all the calculationExecutionTraceList where spare1 equals to UPDATED_SPARE_1
        defaultCalculationExecutionTraceShouldNotBeFound("spare1.equals=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesBySpare1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where spare1 not equals to DEFAULT_SPARE_1
        defaultCalculationExecutionTraceShouldNotBeFound("spare1.notEquals=" + DEFAULT_SPARE_1);

        // Get all the calculationExecutionTraceList where spare1 not equals to UPDATED_SPARE_1
        defaultCalculationExecutionTraceShouldBeFound("spare1.notEquals=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesBySpare1IsInShouldWork() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where spare1 in DEFAULT_SPARE_1 or UPDATED_SPARE_1
        defaultCalculationExecutionTraceShouldBeFound("spare1.in=" + DEFAULT_SPARE_1 + "," + UPDATED_SPARE_1);

        // Get all the calculationExecutionTraceList where spare1 equals to UPDATED_SPARE_1
        defaultCalculationExecutionTraceShouldNotBeFound("spare1.in=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesBySpare1IsNullOrNotNull() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where spare1 is not null
        defaultCalculationExecutionTraceShouldBeFound("spare1.specified=true");

        // Get all the calculationExecutionTraceList where spare1 is null
        defaultCalculationExecutionTraceShouldNotBeFound("spare1.specified=false");
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesBySpare1ContainsSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where spare1 contains DEFAULT_SPARE_1
        defaultCalculationExecutionTraceShouldBeFound("spare1.contains=" + DEFAULT_SPARE_1);

        // Get all the calculationExecutionTraceList where spare1 contains UPDATED_SPARE_1
        defaultCalculationExecutionTraceShouldNotBeFound("spare1.contains=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesBySpare1NotContainsSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where spare1 does not contain DEFAULT_SPARE_1
        defaultCalculationExecutionTraceShouldNotBeFound("spare1.doesNotContain=" + DEFAULT_SPARE_1);

        // Get all the calculationExecutionTraceList where spare1 does not contain UPDATED_SPARE_1
        defaultCalculationExecutionTraceShouldBeFound("spare1.doesNotContain=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesBySpare2IsEqualToSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where spare2 equals to DEFAULT_SPARE_2
        defaultCalculationExecutionTraceShouldBeFound("spare2.equals=" + DEFAULT_SPARE_2);

        // Get all the calculationExecutionTraceList where spare2 equals to UPDATED_SPARE_2
        defaultCalculationExecutionTraceShouldNotBeFound("spare2.equals=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesBySpare2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where spare2 not equals to DEFAULT_SPARE_2
        defaultCalculationExecutionTraceShouldNotBeFound("spare2.notEquals=" + DEFAULT_SPARE_2);

        // Get all the calculationExecutionTraceList where spare2 not equals to UPDATED_SPARE_2
        defaultCalculationExecutionTraceShouldBeFound("spare2.notEquals=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesBySpare2IsInShouldWork() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where spare2 in DEFAULT_SPARE_2 or UPDATED_SPARE_2
        defaultCalculationExecutionTraceShouldBeFound("spare2.in=" + DEFAULT_SPARE_2 + "," + UPDATED_SPARE_2);

        // Get all the calculationExecutionTraceList where spare2 equals to UPDATED_SPARE_2
        defaultCalculationExecutionTraceShouldNotBeFound("spare2.in=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesBySpare2IsNullOrNotNull() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where spare2 is not null
        defaultCalculationExecutionTraceShouldBeFound("spare2.specified=true");

        // Get all the calculationExecutionTraceList where spare2 is null
        defaultCalculationExecutionTraceShouldNotBeFound("spare2.specified=false");
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesBySpare2ContainsSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where spare2 contains DEFAULT_SPARE_2
        defaultCalculationExecutionTraceShouldBeFound("spare2.contains=" + DEFAULT_SPARE_2);

        // Get all the calculationExecutionTraceList where spare2 contains UPDATED_SPARE_2
        defaultCalculationExecutionTraceShouldNotBeFound("spare2.contains=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesBySpare2NotContainsSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where spare2 does not contain DEFAULT_SPARE_2
        defaultCalculationExecutionTraceShouldNotBeFound("spare2.doesNotContain=" + DEFAULT_SPARE_2);

        // Get all the calculationExecutionTraceList where spare2 does not contain UPDATED_SPARE_2
        defaultCalculationExecutionTraceShouldBeFound("spare2.doesNotContain=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesBySpare3IsEqualToSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where spare3 equals to DEFAULT_SPARE_3
        defaultCalculationExecutionTraceShouldBeFound("spare3.equals=" + DEFAULT_SPARE_3);

        // Get all the calculationExecutionTraceList where spare3 equals to UPDATED_SPARE_3
        defaultCalculationExecutionTraceShouldNotBeFound("spare3.equals=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesBySpare3IsNotEqualToSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where spare3 not equals to DEFAULT_SPARE_3
        defaultCalculationExecutionTraceShouldNotBeFound("spare3.notEquals=" + DEFAULT_SPARE_3);

        // Get all the calculationExecutionTraceList where spare3 not equals to UPDATED_SPARE_3
        defaultCalculationExecutionTraceShouldBeFound("spare3.notEquals=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesBySpare3IsInShouldWork() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where spare3 in DEFAULT_SPARE_3 or UPDATED_SPARE_3
        defaultCalculationExecutionTraceShouldBeFound("spare3.in=" + DEFAULT_SPARE_3 + "," + UPDATED_SPARE_3);

        // Get all the calculationExecutionTraceList where spare3 equals to UPDATED_SPARE_3
        defaultCalculationExecutionTraceShouldNotBeFound("spare3.in=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesBySpare3IsNullOrNotNull() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where spare3 is not null
        defaultCalculationExecutionTraceShouldBeFound("spare3.specified=true");

        // Get all the calculationExecutionTraceList where spare3 is null
        defaultCalculationExecutionTraceShouldNotBeFound("spare3.specified=false");
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesBySpare3ContainsSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where spare3 contains DEFAULT_SPARE_3
        defaultCalculationExecutionTraceShouldBeFound("spare3.contains=" + DEFAULT_SPARE_3);

        // Get all the calculationExecutionTraceList where spare3 contains UPDATED_SPARE_3
        defaultCalculationExecutionTraceShouldNotBeFound("spare3.contains=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCalculationExecutionTracesBySpare3NotContainsSomething() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        // Get all the calculationExecutionTraceList where spare3 does not contain DEFAULT_SPARE_3
        defaultCalculationExecutionTraceShouldNotBeFound("spare3.doesNotContain=" + DEFAULT_SPARE_3);

        // Get all the calculationExecutionTraceList where spare3 does not contain UPDATED_SPARE_3
        defaultCalculationExecutionTraceShouldBeFound("spare3.doesNotContain=" + UPDATED_SPARE_3);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCalculationExecutionTraceShouldBeFound(String filter) throws Exception {
        restCalculationExecutionTraceMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(calculationExecutionTrace.getId().intValue())))
            .andExpect(jsonPath("$.[*].frequencyId").value(hasItem(DEFAULT_FREQUENCY_ID.doubleValue())))
            .andExpect(jsonPath("$.[*].executionDate").value(hasItem(DEFAULT_EXECUTION_DATE.toString())))
            .andExpect(jsonPath("$.[*].isExecuted").value(hasItem(DEFAULT_IS_EXECUTED.booleanValue())))
            .andExpect(jsonPath("$.[*].executionResult").value(hasItem(DEFAULT_EXECUTION_RESULT)))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)));

        // Check, that the count call also returns 1
        restCalculationExecutionTraceMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCalculationExecutionTraceShouldNotBeFound(String filter) throws Exception {
        restCalculationExecutionTraceMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCalculationExecutionTraceMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingCalculationExecutionTrace() throws Exception {
        // Get the calculationExecutionTrace
        restCalculationExecutionTraceMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCalculationExecutionTrace() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        int databaseSizeBeforeUpdate = calculationExecutionTraceRepository.findAll().size();

        // Update the calculationExecutionTrace
        CalculationExecutionTrace updatedCalculationExecutionTrace = calculationExecutionTraceRepository
            .findById(calculationExecutionTrace.getId())
            .get();
        // Disconnect from session so that the updates on updatedCalculationExecutionTrace are not directly saved in db
        em.detach(updatedCalculationExecutionTrace);
        updatedCalculationExecutionTrace
            .frequencyId(UPDATED_FREQUENCY_ID)
            .executionDate(UPDATED_EXECUTION_DATE)
            .isExecuted(UPDATED_IS_EXECUTED)
            .executionResult(UPDATED_EXECUTION_RESULT)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);
        CalculationExecutionTraceDTO calculationExecutionTraceDTO = calculationExecutionTraceMapper.toDto(updatedCalculationExecutionTrace);

        restCalculationExecutionTraceMockMvc
            .perform(
                put(ENTITY_API_URL_ID, calculationExecutionTraceDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(calculationExecutionTraceDTO))
            )
            .andExpect(status().isOk());

        // Validate the CalculationExecutionTrace in the database
        List<CalculationExecutionTrace> calculationExecutionTraceList = calculationExecutionTraceRepository.findAll();
        assertThat(calculationExecutionTraceList).hasSize(databaseSizeBeforeUpdate);
        CalculationExecutionTrace testCalculationExecutionTrace = calculationExecutionTraceList.get(
            calculationExecutionTraceList.size() - 1
        );
        assertThat(testCalculationExecutionTrace.getFrequencyId()).isEqualTo(UPDATED_FREQUENCY_ID);
        assertThat(testCalculationExecutionTrace.getExecutionDate()).isEqualTo(UPDATED_EXECUTION_DATE);
        assertThat(testCalculationExecutionTrace.getIsExecuted()).isEqualTo(UPDATED_IS_EXECUTED);
        assertThat(testCalculationExecutionTrace.getExecutionResult()).isEqualTo(UPDATED_EXECUTION_RESULT);
        assertThat(testCalculationExecutionTrace.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testCalculationExecutionTrace.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testCalculationExecutionTrace.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void putNonExistingCalculationExecutionTrace() throws Exception {
        int databaseSizeBeforeUpdate = calculationExecutionTraceRepository.findAll().size();
        calculationExecutionTrace.setId(count.incrementAndGet());

        // Create the CalculationExecutionTrace
        CalculationExecutionTraceDTO calculationExecutionTraceDTO = calculationExecutionTraceMapper.toDto(calculationExecutionTrace);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCalculationExecutionTraceMockMvc
            .perform(
                put(ENTITY_API_URL_ID, calculationExecutionTraceDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(calculationExecutionTraceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CalculationExecutionTrace in the database
        List<CalculationExecutionTrace> calculationExecutionTraceList = calculationExecutionTraceRepository.findAll();
        assertThat(calculationExecutionTraceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCalculationExecutionTrace() throws Exception {
        int databaseSizeBeforeUpdate = calculationExecutionTraceRepository.findAll().size();
        calculationExecutionTrace.setId(count.incrementAndGet());

        // Create the CalculationExecutionTrace
        CalculationExecutionTraceDTO calculationExecutionTraceDTO = calculationExecutionTraceMapper.toDto(calculationExecutionTrace);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCalculationExecutionTraceMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(calculationExecutionTraceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CalculationExecutionTrace in the database
        List<CalculationExecutionTrace> calculationExecutionTraceList = calculationExecutionTraceRepository.findAll();
        assertThat(calculationExecutionTraceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCalculationExecutionTrace() throws Exception {
        int databaseSizeBeforeUpdate = calculationExecutionTraceRepository.findAll().size();
        calculationExecutionTrace.setId(count.incrementAndGet());

        // Create the CalculationExecutionTrace
        CalculationExecutionTraceDTO calculationExecutionTraceDTO = calculationExecutionTraceMapper.toDto(calculationExecutionTrace);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCalculationExecutionTraceMockMvc
            .perform(
                put(ENTITY_API_URL)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(calculationExecutionTraceDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CalculationExecutionTrace in the database
        List<CalculationExecutionTrace> calculationExecutionTraceList = calculationExecutionTraceRepository.findAll();
        assertThat(calculationExecutionTraceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCalculationExecutionTraceWithPatch() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        int databaseSizeBeforeUpdate = calculationExecutionTraceRepository.findAll().size();

        // Update the calculationExecutionTrace using partial update
        CalculationExecutionTrace partialUpdatedCalculationExecutionTrace = new CalculationExecutionTrace();
        partialUpdatedCalculationExecutionTrace.setId(calculationExecutionTrace.getId());

        partialUpdatedCalculationExecutionTrace.executionDate(UPDATED_EXECUTION_DATE);

        restCalculationExecutionTraceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCalculationExecutionTrace.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCalculationExecutionTrace))
            )
            .andExpect(status().isOk());

        // Validate the CalculationExecutionTrace in the database
        List<CalculationExecutionTrace> calculationExecutionTraceList = calculationExecutionTraceRepository.findAll();
        assertThat(calculationExecutionTraceList).hasSize(databaseSizeBeforeUpdate);
        CalculationExecutionTrace testCalculationExecutionTrace = calculationExecutionTraceList.get(
            calculationExecutionTraceList.size() - 1
        );
        assertThat(testCalculationExecutionTrace.getFrequencyId()).isEqualTo(DEFAULT_FREQUENCY_ID);
        assertThat(testCalculationExecutionTrace.getExecutionDate()).isEqualTo(UPDATED_EXECUTION_DATE);
        assertThat(testCalculationExecutionTrace.getIsExecuted()).isEqualTo(DEFAULT_IS_EXECUTED);
        assertThat(testCalculationExecutionTrace.getExecutionResult()).isEqualTo(DEFAULT_EXECUTION_RESULT);
        assertThat(testCalculationExecutionTrace.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testCalculationExecutionTrace.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
        assertThat(testCalculationExecutionTrace.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
    }

    @Test
    @Transactional
    void fullUpdateCalculationExecutionTraceWithPatch() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        int databaseSizeBeforeUpdate = calculationExecutionTraceRepository.findAll().size();

        // Update the calculationExecutionTrace using partial update
        CalculationExecutionTrace partialUpdatedCalculationExecutionTrace = new CalculationExecutionTrace();
        partialUpdatedCalculationExecutionTrace.setId(calculationExecutionTrace.getId());

        partialUpdatedCalculationExecutionTrace
            .frequencyId(UPDATED_FREQUENCY_ID)
            .executionDate(UPDATED_EXECUTION_DATE)
            .isExecuted(UPDATED_IS_EXECUTED)
            .executionResult(UPDATED_EXECUTION_RESULT)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);

        restCalculationExecutionTraceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCalculationExecutionTrace.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCalculationExecutionTrace))
            )
            .andExpect(status().isOk());

        // Validate the CalculationExecutionTrace in the database
        List<CalculationExecutionTrace> calculationExecutionTraceList = calculationExecutionTraceRepository.findAll();
        assertThat(calculationExecutionTraceList).hasSize(databaseSizeBeforeUpdate);
        CalculationExecutionTrace testCalculationExecutionTrace = calculationExecutionTraceList.get(
            calculationExecutionTraceList.size() - 1
        );
        assertThat(testCalculationExecutionTrace.getFrequencyId()).isEqualTo(UPDATED_FREQUENCY_ID);
        assertThat(testCalculationExecutionTrace.getExecutionDate()).isEqualTo(UPDATED_EXECUTION_DATE);
        assertThat(testCalculationExecutionTrace.getIsExecuted()).isEqualTo(UPDATED_IS_EXECUTED);
        assertThat(testCalculationExecutionTrace.getExecutionResult()).isEqualTo(UPDATED_EXECUTION_RESULT);
        assertThat(testCalculationExecutionTrace.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testCalculationExecutionTrace.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testCalculationExecutionTrace.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void patchNonExistingCalculationExecutionTrace() throws Exception {
        int databaseSizeBeforeUpdate = calculationExecutionTraceRepository.findAll().size();
        calculationExecutionTrace.setId(count.incrementAndGet());

        // Create the CalculationExecutionTrace
        CalculationExecutionTraceDTO calculationExecutionTraceDTO = calculationExecutionTraceMapper.toDto(calculationExecutionTrace);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCalculationExecutionTraceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, calculationExecutionTraceDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(calculationExecutionTraceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CalculationExecutionTrace in the database
        List<CalculationExecutionTrace> calculationExecutionTraceList = calculationExecutionTraceRepository.findAll();
        assertThat(calculationExecutionTraceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCalculationExecutionTrace() throws Exception {
        int databaseSizeBeforeUpdate = calculationExecutionTraceRepository.findAll().size();
        calculationExecutionTrace.setId(count.incrementAndGet());

        // Create the CalculationExecutionTrace
        CalculationExecutionTraceDTO calculationExecutionTraceDTO = calculationExecutionTraceMapper.toDto(calculationExecutionTrace);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCalculationExecutionTraceMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(calculationExecutionTraceDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the CalculationExecutionTrace in the database
        List<CalculationExecutionTrace> calculationExecutionTraceList = calculationExecutionTraceRepository.findAll();
        assertThat(calculationExecutionTraceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCalculationExecutionTrace() throws Exception {
        int databaseSizeBeforeUpdate = calculationExecutionTraceRepository.findAll().size();
        calculationExecutionTrace.setId(count.incrementAndGet());

        // Create the CalculationExecutionTrace
        CalculationExecutionTraceDTO calculationExecutionTraceDTO = calculationExecutionTraceMapper.toDto(calculationExecutionTrace);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCalculationExecutionTraceMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(calculationExecutionTraceDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the CalculationExecutionTrace in the database
        List<CalculationExecutionTrace> calculationExecutionTraceList = calculationExecutionTraceRepository.findAll();
        assertThat(calculationExecutionTraceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCalculationExecutionTrace() throws Exception {
        // Initialize the database
        calculationExecutionTraceRepository.saveAndFlush(calculationExecutionTrace);

        int databaseSizeBeforeDelete = calculationExecutionTraceRepository.findAll().size();

        // Delete the calculationExecutionTrace
        restCalculationExecutionTraceMockMvc
            .perform(delete(ENTITY_API_URL_ID, calculationExecutionTrace.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<CalculationExecutionTrace> calculationExecutionTraceList = calculationExecutionTraceRepository.findAll();
        assertThat(calculationExecutionTraceList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
