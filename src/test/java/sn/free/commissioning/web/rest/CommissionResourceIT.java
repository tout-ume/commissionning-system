package sn.free.commissioning.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.IntegrationTest;
import sn.free.commissioning.domain.Commission;
import sn.free.commissioning.domain.CommissionAccount;
import sn.free.commissioning.domain.ConfigurationPlan;
import sn.free.commissioning.domain.Payment;
import sn.free.commissioning.domain.TransactionOperation;
import sn.free.commissioning.domain.enumeration.CommissionPaymentStatus;
import sn.free.commissioning.domain.enumeration.CommissioningPlanType;
import sn.free.commissioning.repository.CommissionRepository;
import sn.free.commissioning.service.CommissionService;
import sn.free.commissioning.service.criteria.CommissionCriteria;
import sn.free.commissioning.service.dto.CommissionDTO;
import sn.free.commissioning.service.mapper.CommissionMapper;

/**
 * Integration tests for the {@link CommissionResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class CommissionResourceIT {

    private static final Double DEFAULT_AMOUNT = 1D;
    private static final Double UPDATED_AMOUNT = 2D;
    private static final Double SMALLER_AMOUNT = 1D - 1D;

    private static final Instant DEFAULT_CALCULATED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CALCULATED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_CALCULATION_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CALCULATION_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final LocalDate DEFAULT_CALCULATION_SHORT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CALCULATION_SHORT_DATE = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_CALCULATION_SHORT_DATE = LocalDate.ofEpochDay(-1L);

    private static final String DEFAULT_SENDER_MSISDN = "AAAAAAAAAA";
    private static final String UPDATED_SENDER_MSISDN = "BBBBBBBBBB";

    private static final String DEFAULT_SENDER_PROFILE = "AAAAAAAAAA";
    private static final String UPDATED_SENDER_PROFILE = "BBBBBBBBBB";

    private static final String DEFAULT_SERVICE_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_SERVICE_TYPE = "BBBBBBBBBB";

    private static final CommissionPaymentStatus DEFAULT_COMMISSION_PAYMENT_STATUS = CommissionPaymentStatus.TO_BE_PAID;
    private static final CommissionPaymentStatus UPDATED_COMMISSION_PAYMENT_STATUS = CommissionPaymentStatus.TO_BE_PAID_TO_PARTNER;

    private static final CommissioningPlanType DEFAULT_COMMISSIONING_PLAN_TYPE = CommissioningPlanType.MFS;
    private static final CommissioningPlanType UPDATED_COMMISSIONING_PLAN_TYPE = CommissioningPlanType.MOBILE;

    private static final Double DEFAULT_GLOBAL_NETWORK_COMMISSION_AMOUNT = 1D;
    private static final Double UPDATED_GLOBAL_NETWORK_COMMISSION_AMOUNT = 2D;
    private static final Double SMALLER_GLOBAL_NETWORK_COMMISSION_AMOUNT = 1D - 1D;

    private static final Double DEFAULT_TRANSACTIONS_AMOUNT = 1D;
    private static final Double UPDATED_TRANSACTIONS_AMOUNT = 2D;
    private static final Double SMALLER_TRANSACTIONS_AMOUNT = 1D - 1D;

    private static final Double DEFAULT_FRAUD_AMOUNT = 1D;
    private static final Double UPDATED_FRAUD_AMOUNT = 2D;
    private static final Double SMALLER_FRAUD_AMOUNT = 1D - 1D;

    private static final String DEFAULT_SPARE_1 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_2 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_3 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_3 = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/commissions";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CommissionRepository commissionRepository;

    @Mock
    private CommissionRepository commissionRepositoryMock;

    @Autowired
    private CommissionMapper commissionMapper;

    @Mock
    private CommissionService commissionServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restCommissionMockMvc;

    private Commission commission;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Commission createEntity(EntityManager em) {
        Commission commission = new Commission()
            .amount(DEFAULT_AMOUNT)
            .calculatedAt(DEFAULT_CALCULATED_AT)
            .calculationDate(DEFAULT_CALCULATION_DATE)
            .calculationShortDate(DEFAULT_CALCULATION_SHORT_DATE)
            .senderMsisdn(DEFAULT_SENDER_MSISDN)
            .senderProfile(DEFAULT_SENDER_PROFILE)
            .serviceType(DEFAULT_SERVICE_TYPE)
            .commissionPaymentStatus(DEFAULT_COMMISSION_PAYMENT_STATUS)
            .commissioningPlanType(DEFAULT_COMMISSIONING_PLAN_TYPE)
            .globalNetworkCommissionAmount(DEFAULT_GLOBAL_NETWORK_COMMISSION_AMOUNT)
            .transactionsAmount(DEFAULT_TRANSACTIONS_AMOUNT)
            .fraudAmount(DEFAULT_FRAUD_AMOUNT)
            .spare1(DEFAULT_SPARE_1)
            .spare2(DEFAULT_SPARE_2)
            .spare3(DEFAULT_SPARE_3);
        return commission;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Commission createUpdatedEntity(EntityManager em) {
        Commission commission = new Commission()
            .amount(UPDATED_AMOUNT)
            .calculatedAt(UPDATED_CALCULATED_AT)
            .calculationDate(UPDATED_CALCULATION_DATE)
            .calculationShortDate(UPDATED_CALCULATION_SHORT_DATE)
            .senderMsisdn(UPDATED_SENDER_MSISDN)
            .senderProfile(UPDATED_SENDER_PROFILE)
            .serviceType(UPDATED_SERVICE_TYPE)
            .commissionPaymentStatus(UPDATED_COMMISSION_PAYMENT_STATUS)
            .commissioningPlanType(UPDATED_COMMISSIONING_PLAN_TYPE)
            .globalNetworkCommissionAmount(UPDATED_GLOBAL_NETWORK_COMMISSION_AMOUNT)
            .transactionsAmount(UPDATED_TRANSACTIONS_AMOUNT)
            .fraudAmount(UPDATED_FRAUD_AMOUNT)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);
        return commission;
    }

    @BeforeEach
    public void initTest() {
        commission = createEntity(em);
    }

    @Test
    @Transactional
    void createCommission() throws Exception {
        int databaseSizeBeforeCreate = commissionRepository.findAll().size();
        // Create the Commission
        CommissionDTO commissionDTO = commissionMapper.toDto(commission);
        restCommissionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(commissionDTO)))
            .andExpect(status().isCreated());

        // Validate the Commission in the database
        List<Commission> commissionList = commissionRepository.findAll();
        assertThat(commissionList).hasSize(databaseSizeBeforeCreate + 1);
        Commission testCommission = commissionList.get(commissionList.size() - 1);
        assertThat(testCommission.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testCommission.getCalculatedAt()).isEqualTo(DEFAULT_CALCULATED_AT);
        assertThat(testCommission.getCalculationDate()).isEqualTo(DEFAULT_CALCULATION_DATE);
        assertThat(testCommission.getCalculationShortDate()).isEqualTo(DEFAULT_CALCULATION_SHORT_DATE);
        assertThat(testCommission.getSenderMsisdn()).isEqualTo(DEFAULT_SENDER_MSISDN);
        assertThat(testCommission.getSenderProfile()).isEqualTo(DEFAULT_SENDER_PROFILE);
        assertThat(testCommission.getServiceType()).isEqualTo(DEFAULT_SERVICE_TYPE);
        assertThat(testCommission.getCommissionPaymentStatus()).isEqualTo(DEFAULT_COMMISSION_PAYMENT_STATUS);
        assertThat(testCommission.getCommissioningPlanType()).isEqualTo(DEFAULT_COMMISSIONING_PLAN_TYPE);
        assertThat(testCommission.getGlobalNetworkCommissionAmount()).isEqualTo(DEFAULT_GLOBAL_NETWORK_COMMISSION_AMOUNT);
        assertThat(testCommission.getTransactionsAmount()).isEqualTo(DEFAULT_TRANSACTIONS_AMOUNT);
        assertThat(testCommission.getFraudAmount()).isEqualTo(DEFAULT_FRAUD_AMOUNT);
        assertThat(testCommission.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testCommission.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
        assertThat(testCommission.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
    }

    @Test
    @Transactional
    void createCommissionWithExistingId() throws Exception {
        // Create the Commission with an existing ID
        commission.setId(1L);
        CommissionDTO commissionDTO = commissionMapper.toDto(commission);

        int databaseSizeBeforeCreate = commissionRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restCommissionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(commissionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Commission in the database
        List<Commission> commissionList = commissionRepository.findAll();
        assertThat(commissionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkAmountIsRequired() throws Exception {
        int databaseSizeBeforeTest = commissionRepository.findAll().size();
        // set the field null
        commission.setAmount(null);

        // Create the Commission, which fails.
        CommissionDTO commissionDTO = commissionMapper.toDto(commission);

        restCommissionMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(commissionDTO)))
            .andExpect(status().isBadRequest());

        List<Commission> commissionList = commissionRepository.findAll();
        assertThat(commissionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllCommissions() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList
        restCommissionMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(commission.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].calculatedAt").value(hasItem(DEFAULT_CALCULATED_AT.toString())))
            .andExpect(jsonPath("$.[*].calculationDate").value(hasItem(DEFAULT_CALCULATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].calculationShortDate").value(hasItem(DEFAULT_CALCULATION_SHORT_DATE.toString())))
            .andExpect(jsonPath("$.[*].senderMsisdn").value(hasItem(DEFAULT_SENDER_MSISDN)))
            .andExpect(jsonPath("$.[*].senderProfile").value(hasItem(DEFAULT_SENDER_PROFILE)))
            .andExpect(jsonPath("$.[*].serviceType").value(hasItem(DEFAULT_SERVICE_TYPE)))
            .andExpect(jsonPath("$.[*].commissionPaymentStatus").value(hasItem(DEFAULT_COMMISSION_PAYMENT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].commissioningPlanType").value(hasItem(DEFAULT_COMMISSIONING_PLAN_TYPE.toString())))
            .andExpect(
                jsonPath("$.[*].globalNetworkCommissionAmount").value(hasItem(DEFAULT_GLOBAL_NETWORK_COMMISSION_AMOUNT.doubleValue()))
            )
            .andExpect(jsonPath("$.[*].transactionsAmount").value(hasItem(DEFAULT_TRANSACTIONS_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].fraudAmount").value(hasItem(DEFAULT_FRAUD_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllCommissionsWithEagerRelationshipsIsEnabled() throws Exception {
        when(commissionServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restCommissionMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(commissionServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllCommissionsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(commissionServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restCommissionMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(commissionServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    void getCommission() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get the commission
        restCommissionMockMvc
            .perform(get(ENTITY_API_URL_ID, commission.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(commission.getId().intValue()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.calculatedAt").value(DEFAULT_CALCULATED_AT.toString()))
            .andExpect(jsonPath("$.calculationDate").value(DEFAULT_CALCULATION_DATE.toString()))
            .andExpect(jsonPath("$.calculationShortDate").value(DEFAULT_CALCULATION_SHORT_DATE.toString()))
            .andExpect(jsonPath("$.senderMsisdn").value(DEFAULT_SENDER_MSISDN))
            .andExpect(jsonPath("$.senderProfile").value(DEFAULT_SENDER_PROFILE))
            .andExpect(jsonPath("$.serviceType").value(DEFAULT_SERVICE_TYPE))
            .andExpect(jsonPath("$.commissionPaymentStatus").value(DEFAULT_COMMISSION_PAYMENT_STATUS.toString()))
            .andExpect(jsonPath("$.commissioningPlanType").value(DEFAULT_COMMISSIONING_PLAN_TYPE.toString()))
            .andExpect(jsonPath("$.globalNetworkCommissionAmount").value(DEFAULT_GLOBAL_NETWORK_COMMISSION_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.transactionsAmount").value(DEFAULT_TRANSACTIONS_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.fraudAmount").value(DEFAULT_FRAUD_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.spare1").value(DEFAULT_SPARE_1))
            .andExpect(jsonPath("$.spare2").value(DEFAULT_SPARE_2))
            .andExpect(jsonPath("$.spare3").value(DEFAULT_SPARE_3));
    }

    @Test
    @Transactional
    void getCommissionsByIdFiltering() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        Long id = commission.getId();

        defaultCommissionShouldBeFound("id.equals=" + id);
        defaultCommissionShouldNotBeFound("id.notEquals=" + id);

        defaultCommissionShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultCommissionShouldNotBeFound("id.greaterThan=" + id);

        defaultCommissionShouldBeFound("id.lessThanOrEqual=" + id);
        defaultCommissionShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllCommissionsByAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where amount equals to DEFAULT_AMOUNT
        defaultCommissionShouldBeFound("amount.equals=" + DEFAULT_AMOUNT);

        // Get all the commissionList where amount equals to UPDATED_AMOUNT
        defaultCommissionShouldNotBeFound("amount.equals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByAmountIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where amount not equals to DEFAULT_AMOUNT
        defaultCommissionShouldNotBeFound("amount.notEquals=" + DEFAULT_AMOUNT);

        // Get all the commissionList where amount not equals to UPDATED_AMOUNT
        defaultCommissionShouldBeFound("amount.notEquals=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByAmountIsInShouldWork() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where amount in DEFAULT_AMOUNT or UPDATED_AMOUNT
        defaultCommissionShouldBeFound("amount.in=" + DEFAULT_AMOUNT + "," + UPDATED_AMOUNT);

        // Get all the commissionList where amount equals to UPDATED_AMOUNT
        defaultCommissionShouldNotBeFound("amount.in=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where amount is not null
        defaultCommissionShouldBeFound("amount.specified=true");

        // Get all the commissionList where amount is null
        defaultCommissionShouldNotBeFound("amount.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionsByAmountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where amount is greater than or equal to DEFAULT_AMOUNT
        defaultCommissionShouldBeFound("amount.greaterThanOrEqual=" + DEFAULT_AMOUNT);

        // Get all the commissionList where amount is greater than or equal to UPDATED_AMOUNT
        defaultCommissionShouldNotBeFound("amount.greaterThanOrEqual=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByAmountIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where amount is less than or equal to DEFAULT_AMOUNT
        defaultCommissionShouldBeFound("amount.lessThanOrEqual=" + DEFAULT_AMOUNT);

        // Get all the commissionList where amount is less than or equal to SMALLER_AMOUNT
        defaultCommissionShouldNotBeFound("amount.lessThanOrEqual=" + SMALLER_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByAmountIsLessThanSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where amount is less than DEFAULT_AMOUNT
        defaultCommissionShouldNotBeFound("amount.lessThan=" + DEFAULT_AMOUNT);

        // Get all the commissionList where amount is less than UPDATED_AMOUNT
        defaultCommissionShouldBeFound("amount.lessThan=" + UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByAmountIsGreaterThanSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where amount is greater than DEFAULT_AMOUNT
        defaultCommissionShouldNotBeFound("amount.greaterThan=" + DEFAULT_AMOUNT);

        // Get all the commissionList where amount is greater than SMALLER_AMOUNT
        defaultCommissionShouldBeFound("amount.greaterThan=" + SMALLER_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByCalculatedAtIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where calculatedAt equals to DEFAULT_CALCULATED_AT
        defaultCommissionShouldBeFound("calculatedAt.equals=" + DEFAULT_CALCULATED_AT);

        // Get all the commissionList where calculatedAt equals to UPDATED_CALCULATED_AT
        defaultCommissionShouldNotBeFound("calculatedAt.equals=" + UPDATED_CALCULATED_AT);
    }

    @Test
    @Transactional
    void getAllCommissionsByCalculatedAtIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where calculatedAt not equals to DEFAULT_CALCULATED_AT
        defaultCommissionShouldNotBeFound("calculatedAt.notEquals=" + DEFAULT_CALCULATED_AT);

        // Get all the commissionList where calculatedAt not equals to UPDATED_CALCULATED_AT
        defaultCommissionShouldBeFound("calculatedAt.notEquals=" + UPDATED_CALCULATED_AT);
    }

    @Test
    @Transactional
    void getAllCommissionsByCalculatedAtIsInShouldWork() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where calculatedAt in DEFAULT_CALCULATED_AT or UPDATED_CALCULATED_AT
        defaultCommissionShouldBeFound("calculatedAt.in=" + DEFAULT_CALCULATED_AT + "," + UPDATED_CALCULATED_AT);

        // Get all the commissionList where calculatedAt equals to UPDATED_CALCULATED_AT
        defaultCommissionShouldNotBeFound("calculatedAt.in=" + UPDATED_CALCULATED_AT);
    }

    @Test
    @Transactional
    void getAllCommissionsByCalculatedAtIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where calculatedAt is not null
        defaultCommissionShouldBeFound("calculatedAt.specified=true");

        // Get all the commissionList where calculatedAt is null
        defaultCommissionShouldNotBeFound("calculatedAt.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionsByCalculationDateIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where calculationDate equals to DEFAULT_CALCULATION_DATE
        defaultCommissionShouldBeFound("calculationDate.equals=" + DEFAULT_CALCULATION_DATE);

        // Get all the commissionList where calculationDate equals to UPDATED_CALCULATION_DATE
        defaultCommissionShouldNotBeFound("calculationDate.equals=" + UPDATED_CALCULATION_DATE);
    }

    @Test
    @Transactional
    void getAllCommissionsByCalculationDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where calculationDate not equals to DEFAULT_CALCULATION_DATE
        defaultCommissionShouldNotBeFound("calculationDate.notEquals=" + DEFAULT_CALCULATION_DATE);

        // Get all the commissionList where calculationDate not equals to UPDATED_CALCULATION_DATE
        defaultCommissionShouldBeFound("calculationDate.notEquals=" + UPDATED_CALCULATION_DATE);
    }

    @Test
    @Transactional
    void getAllCommissionsByCalculationDateIsInShouldWork() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where calculationDate in DEFAULT_CALCULATION_DATE or UPDATED_CALCULATION_DATE
        defaultCommissionShouldBeFound("calculationDate.in=" + DEFAULT_CALCULATION_DATE + "," + UPDATED_CALCULATION_DATE);

        // Get all the commissionList where calculationDate equals to UPDATED_CALCULATION_DATE
        defaultCommissionShouldNotBeFound("calculationDate.in=" + UPDATED_CALCULATION_DATE);
    }

    @Test
    @Transactional
    void getAllCommissionsByCalculationDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where calculationDate is not null
        defaultCommissionShouldBeFound("calculationDate.specified=true");

        // Get all the commissionList where calculationDate is null
        defaultCommissionShouldNotBeFound("calculationDate.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionsByCalculationShortDateIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where calculationShortDate equals to DEFAULT_CALCULATION_SHORT_DATE
        defaultCommissionShouldBeFound("calculationShortDate.equals=" + DEFAULT_CALCULATION_SHORT_DATE);

        // Get all the commissionList where calculationShortDate equals to UPDATED_CALCULATION_SHORT_DATE
        defaultCommissionShouldNotBeFound("calculationShortDate.equals=" + UPDATED_CALCULATION_SHORT_DATE);
    }

    @Test
    @Transactional
    void getAllCommissionsByCalculationShortDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where calculationShortDate not equals to DEFAULT_CALCULATION_SHORT_DATE
        defaultCommissionShouldNotBeFound("calculationShortDate.notEquals=" + DEFAULT_CALCULATION_SHORT_DATE);

        // Get all the commissionList where calculationShortDate not equals to UPDATED_CALCULATION_SHORT_DATE
        defaultCommissionShouldBeFound("calculationShortDate.notEquals=" + UPDATED_CALCULATION_SHORT_DATE);
    }

    @Test
    @Transactional
    void getAllCommissionsByCalculationShortDateIsInShouldWork() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where calculationShortDate in DEFAULT_CALCULATION_SHORT_DATE or UPDATED_CALCULATION_SHORT_DATE
        defaultCommissionShouldBeFound("calculationShortDate.in=" + DEFAULT_CALCULATION_SHORT_DATE + "," + UPDATED_CALCULATION_SHORT_DATE);

        // Get all the commissionList where calculationShortDate equals to UPDATED_CALCULATION_SHORT_DATE
        defaultCommissionShouldNotBeFound("calculationShortDate.in=" + UPDATED_CALCULATION_SHORT_DATE);
    }

    @Test
    @Transactional
    void getAllCommissionsByCalculationShortDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where calculationShortDate is not null
        defaultCommissionShouldBeFound("calculationShortDate.specified=true");

        // Get all the commissionList where calculationShortDate is null
        defaultCommissionShouldNotBeFound("calculationShortDate.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionsByCalculationShortDateIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where calculationShortDate is greater than or equal to DEFAULT_CALCULATION_SHORT_DATE
        defaultCommissionShouldBeFound("calculationShortDate.greaterThanOrEqual=" + DEFAULT_CALCULATION_SHORT_DATE);

        // Get all the commissionList where calculationShortDate is greater than or equal to UPDATED_CALCULATION_SHORT_DATE
        defaultCommissionShouldNotBeFound("calculationShortDate.greaterThanOrEqual=" + UPDATED_CALCULATION_SHORT_DATE);
    }

    @Test
    @Transactional
    void getAllCommissionsByCalculationShortDateIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where calculationShortDate is less than or equal to DEFAULT_CALCULATION_SHORT_DATE
        defaultCommissionShouldBeFound("calculationShortDate.lessThanOrEqual=" + DEFAULT_CALCULATION_SHORT_DATE);

        // Get all the commissionList where calculationShortDate is less than or equal to SMALLER_CALCULATION_SHORT_DATE
        defaultCommissionShouldNotBeFound("calculationShortDate.lessThanOrEqual=" + SMALLER_CALCULATION_SHORT_DATE);
    }

    @Test
    @Transactional
    void getAllCommissionsByCalculationShortDateIsLessThanSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where calculationShortDate is less than DEFAULT_CALCULATION_SHORT_DATE
        defaultCommissionShouldNotBeFound("calculationShortDate.lessThan=" + DEFAULT_CALCULATION_SHORT_DATE);

        // Get all the commissionList where calculationShortDate is less than UPDATED_CALCULATION_SHORT_DATE
        defaultCommissionShouldBeFound("calculationShortDate.lessThan=" + UPDATED_CALCULATION_SHORT_DATE);
    }

    @Test
    @Transactional
    void getAllCommissionsByCalculationShortDateIsGreaterThanSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where calculationShortDate is greater than DEFAULT_CALCULATION_SHORT_DATE
        defaultCommissionShouldNotBeFound("calculationShortDate.greaterThan=" + DEFAULT_CALCULATION_SHORT_DATE);

        // Get all the commissionList where calculationShortDate is greater than SMALLER_CALCULATION_SHORT_DATE
        defaultCommissionShouldBeFound("calculationShortDate.greaterThan=" + SMALLER_CALCULATION_SHORT_DATE);
    }

    @Test
    @Transactional
    void getAllCommissionsBySenderMsisdnIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where senderMsisdn equals to DEFAULT_SENDER_MSISDN
        defaultCommissionShouldBeFound("senderMsisdn.equals=" + DEFAULT_SENDER_MSISDN);

        // Get all the commissionList where senderMsisdn equals to UPDATED_SENDER_MSISDN
        defaultCommissionShouldNotBeFound("senderMsisdn.equals=" + UPDATED_SENDER_MSISDN);
    }

    @Test
    @Transactional
    void getAllCommissionsBySenderMsisdnIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where senderMsisdn not equals to DEFAULT_SENDER_MSISDN
        defaultCommissionShouldNotBeFound("senderMsisdn.notEquals=" + DEFAULT_SENDER_MSISDN);

        // Get all the commissionList where senderMsisdn not equals to UPDATED_SENDER_MSISDN
        defaultCommissionShouldBeFound("senderMsisdn.notEquals=" + UPDATED_SENDER_MSISDN);
    }

    @Test
    @Transactional
    void getAllCommissionsBySenderMsisdnIsInShouldWork() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where senderMsisdn in DEFAULT_SENDER_MSISDN or UPDATED_SENDER_MSISDN
        defaultCommissionShouldBeFound("senderMsisdn.in=" + DEFAULT_SENDER_MSISDN + "," + UPDATED_SENDER_MSISDN);

        // Get all the commissionList where senderMsisdn equals to UPDATED_SENDER_MSISDN
        defaultCommissionShouldNotBeFound("senderMsisdn.in=" + UPDATED_SENDER_MSISDN);
    }

    @Test
    @Transactional
    void getAllCommissionsBySenderMsisdnIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where senderMsisdn is not null
        defaultCommissionShouldBeFound("senderMsisdn.specified=true");

        // Get all the commissionList where senderMsisdn is null
        defaultCommissionShouldNotBeFound("senderMsisdn.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionsBySenderMsisdnContainsSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where senderMsisdn contains DEFAULT_SENDER_MSISDN
        defaultCommissionShouldBeFound("senderMsisdn.contains=" + DEFAULT_SENDER_MSISDN);

        // Get all the commissionList where senderMsisdn contains UPDATED_SENDER_MSISDN
        defaultCommissionShouldNotBeFound("senderMsisdn.contains=" + UPDATED_SENDER_MSISDN);
    }

    @Test
    @Transactional
    void getAllCommissionsBySenderMsisdnNotContainsSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where senderMsisdn does not contain DEFAULT_SENDER_MSISDN
        defaultCommissionShouldNotBeFound("senderMsisdn.doesNotContain=" + DEFAULT_SENDER_MSISDN);

        // Get all the commissionList where senderMsisdn does not contain UPDATED_SENDER_MSISDN
        defaultCommissionShouldBeFound("senderMsisdn.doesNotContain=" + UPDATED_SENDER_MSISDN);
    }

    @Test
    @Transactional
    void getAllCommissionsBySenderProfileIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where senderProfile equals to DEFAULT_SENDER_PROFILE
        defaultCommissionShouldBeFound("senderProfile.equals=" + DEFAULT_SENDER_PROFILE);

        // Get all the commissionList where senderProfile equals to UPDATED_SENDER_PROFILE
        defaultCommissionShouldNotBeFound("senderProfile.equals=" + UPDATED_SENDER_PROFILE);
    }

    @Test
    @Transactional
    void getAllCommissionsBySenderProfileIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where senderProfile not equals to DEFAULT_SENDER_PROFILE
        defaultCommissionShouldNotBeFound("senderProfile.notEquals=" + DEFAULT_SENDER_PROFILE);

        // Get all the commissionList where senderProfile not equals to UPDATED_SENDER_PROFILE
        defaultCommissionShouldBeFound("senderProfile.notEquals=" + UPDATED_SENDER_PROFILE);
    }

    @Test
    @Transactional
    void getAllCommissionsBySenderProfileIsInShouldWork() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where senderProfile in DEFAULT_SENDER_PROFILE or UPDATED_SENDER_PROFILE
        defaultCommissionShouldBeFound("senderProfile.in=" + DEFAULT_SENDER_PROFILE + "," + UPDATED_SENDER_PROFILE);

        // Get all the commissionList where senderProfile equals to UPDATED_SENDER_PROFILE
        defaultCommissionShouldNotBeFound("senderProfile.in=" + UPDATED_SENDER_PROFILE);
    }

    @Test
    @Transactional
    void getAllCommissionsBySenderProfileIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where senderProfile is not null
        defaultCommissionShouldBeFound("senderProfile.specified=true");

        // Get all the commissionList where senderProfile is null
        defaultCommissionShouldNotBeFound("senderProfile.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionsBySenderProfileContainsSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where senderProfile contains DEFAULT_SENDER_PROFILE
        defaultCommissionShouldBeFound("senderProfile.contains=" + DEFAULT_SENDER_PROFILE);

        // Get all the commissionList where senderProfile contains UPDATED_SENDER_PROFILE
        defaultCommissionShouldNotBeFound("senderProfile.contains=" + UPDATED_SENDER_PROFILE);
    }

    @Test
    @Transactional
    void getAllCommissionsBySenderProfileNotContainsSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where senderProfile does not contain DEFAULT_SENDER_PROFILE
        defaultCommissionShouldNotBeFound("senderProfile.doesNotContain=" + DEFAULT_SENDER_PROFILE);

        // Get all the commissionList where senderProfile does not contain UPDATED_SENDER_PROFILE
        defaultCommissionShouldBeFound("senderProfile.doesNotContain=" + UPDATED_SENDER_PROFILE);
    }

    @Test
    @Transactional
    void getAllCommissionsByServiceTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where serviceType equals to DEFAULT_SERVICE_TYPE
        defaultCommissionShouldBeFound("serviceType.equals=" + DEFAULT_SERVICE_TYPE);

        // Get all the commissionList where serviceType equals to UPDATED_SERVICE_TYPE
        defaultCommissionShouldNotBeFound("serviceType.equals=" + UPDATED_SERVICE_TYPE);
    }

    @Test
    @Transactional
    void getAllCommissionsByServiceTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where serviceType not equals to DEFAULT_SERVICE_TYPE
        defaultCommissionShouldNotBeFound("serviceType.notEquals=" + DEFAULT_SERVICE_TYPE);

        // Get all the commissionList where serviceType not equals to UPDATED_SERVICE_TYPE
        defaultCommissionShouldBeFound("serviceType.notEquals=" + UPDATED_SERVICE_TYPE);
    }

    @Test
    @Transactional
    void getAllCommissionsByServiceTypeIsInShouldWork() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where serviceType in DEFAULT_SERVICE_TYPE or UPDATED_SERVICE_TYPE
        defaultCommissionShouldBeFound("serviceType.in=" + DEFAULT_SERVICE_TYPE + "," + UPDATED_SERVICE_TYPE);

        // Get all the commissionList where serviceType equals to UPDATED_SERVICE_TYPE
        defaultCommissionShouldNotBeFound("serviceType.in=" + UPDATED_SERVICE_TYPE);
    }

    @Test
    @Transactional
    void getAllCommissionsByServiceTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where serviceType is not null
        defaultCommissionShouldBeFound("serviceType.specified=true");

        // Get all the commissionList where serviceType is null
        defaultCommissionShouldNotBeFound("serviceType.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionsByServiceTypeContainsSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where serviceType contains DEFAULT_SERVICE_TYPE
        defaultCommissionShouldBeFound("serviceType.contains=" + DEFAULT_SERVICE_TYPE);

        // Get all the commissionList where serviceType contains UPDATED_SERVICE_TYPE
        defaultCommissionShouldNotBeFound("serviceType.contains=" + UPDATED_SERVICE_TYPE);
    }

    @Test
    @Transactional
    void getAllCommissionsByServiceTypeNotContainsSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where serviceType does not contain DEFAULT_SERVICE_TYPE
        defaultCommissionShouldNotBeFound("serviceType.doesNotContain=" + DEFAULT_SERVICE_TYPE);

        // Get all the commissionList where serviceType does not contain UPDATED_SERVICE_TYPE
        defaultCommissionShouldBeFound("serviceType.doesNotContain=" + UPDATED_SERVICE_TYPE);
    }

    @Test
    @Transactional
    void getAllCommissionsByCommissionPaymentStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where commissionPaymentStatus equals to DEFAULT_COMMISSION_PAYMENT_STATUS
        defaultCommissionShouldBeFound("commissionPaymentStatus.equals=" + DEFAULT_COMMISSION_PAYMENT_STATUS);

        // Get all the commissionList where commissionPaymentStatus equals to UPDATED_COMMISSION_PAYMENT_STATUS
        defaultCommissionShouldNotBeFound("commissionPaymentStatus.equals=" + UPDATED_COMMISSION_PAYMENT_STATUS);
    }

    @Test
    @Transactional
    void getAllCommissionsByCommissionPaymentStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where commissionPaymentStatus not equals to DEFAULT_COMMISSION_PAYMENT_STATUS
        defaultCommissionShouldNotBeFound("commissionPaymentStatus.notEquals=" + DEFAULT_COMMISSION_PAYMENT_STATUS);

        // Get all the commissionList where commissionPaymentStatus not equals to UPDATED_COMMISSION_PAYMENT_STATUS
        defaultCommissionShouldBeFound("commissionPaymentStatus.notEquals=" + UPDATED_COMMISSION_PAYMENT_STATUS);
    }

    @Test
    @Transactional
    void getAllCommissionsByCommissionPaymentStatusIsInShouldWork() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where commissionPaymentStatus in DEFAULT_COMMISSION_PAYMENT_STATUS or UPDATED_COMMISSION_PAYMENT_STATUS
        defaultCommissionShouldBeFound(
            "commissionPaymentStatus.in=" + DEFAULT_COMMISSION_PAYMENT_STATUS + "," + UPDATED_COMMISSION_PAYMENT_STATUS
        );

        // Get all the commissionList where commissionPaymentStatus equals to UPDATED_COMMISSION_PAYMENT_STATUS
        defaultCommissionShouldNotBeFound("commissionPaymentStatus.in=" + UPDATED_COMMISSION_PAYMENT_STATUS);
    }

    @Test
    @Transactional
    void getAllCommissionsByCommissionPaymentStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where commissionPaymentStatus is not null
        defaultCommissionShouldBeFound("commissionPaymentStatus.specified=true");

        // Get all the commissionList where commissionPaymentStatus is null
        defaultCommissionShouldNotBeFound("commissionPaymentStatus.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionsByCommissioningPlanTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where commissioningPlanType equals to DEFAULT_COMMISSIONING_PLAN_TYPE
        defaultCommissionShouldBeFound("commissioningPlanType.equals=" + DEFAULT_COMMISSIONING_PLAN_TYPE);

        // Get all the commissionList where commissioningPlanType equals to UPDATED_COMMISSIONING_PLAN_TYPE
        defaultCommissionShouldNotBeFound("commissioningPlanType.equals=" + UPDATED_COMMISSIONING_PLAN_TYPE);
    }

    @Test
    @Transactional
    void getAllCommissionsByCommissioningPlanTypeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where commissioningPlanType not equals to DEFAULT_COMMISSIONING_PLAN_TYPE
        defaultCommissionShouldNotBeFound("commissioningPlanType.notEquals=" + DEFAULT_COMMISSIONING_PLAN_TYPE);

        // Get all the commissionList where commissioningPlanType not equals to UPDATED_COMMISSIONING_PLAN_TYPE
        defaultCommissionShouldBeFound("commissioningPlanType.notEquals=" + UPDATED_COMMISSIONING_PLAN_TYPE);
    }

    @Test
    @Transactional
    void getAllCommissionsByCommissioningPlanTypeIsInShouldWork() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where commissioningPlanType in DEFAULT_COMMISSIONING_PLAN_TYPE or UPDATED_COMMISSIONING_PLAN_TYPE
        defaultCommissionShouldBeFound(
            "commissioningPlanType.in=" + DEFAULT_COMMISSIONING_PLAN_TYPE + "," + UPDATED_COMMISSIONING_PLAN_TYPE
        );

        // Get all the commissionList where commissioningPlanType equals to UPDATED_COMMISSIONING_PLAN_TYPE
        defaultCommissionShouldNotBeFound("commissioningPlanType.in=" + UPDATED_COMMISSIONING_PLAN_TYPE);
    }

    @Test
    @Transactional
    void getAllCommissionsByCommissioningPlanTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where commissioningPlanType is not null
        defaultCommissionShouldBeFound("commissioningPlanType.specified=true");

        // Get all the commissionList where commissioningPlanType is null
        defaultCommissionShouldNotBeFound("commissioningPlanType.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionsByGlobalNetworkCommissionAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where globalNetworkCommissionAmount equals to DEFAULT_GLOBAL_NETWORK_COMMISSION_AMOUNT
        defaultCommissionShouldBeFound("globalNetworkCommissionAmount.equals=" + DEFAULT_GLOBAL_NETWORK_COMMISSION_AMOUNT);

        // Get all the commissionList where globalNetworkCommissionAmount equals to UPDATED_GLOBAL_NETWORK_COMMISSION_AMOUNT
        defaultCommissionShouldNotBeFound("globalNetworkCommissionAmount.equals=" + UPDATED_GLOBAL_NETWORK_COMMISSION_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByGlobalNetworkCommissionAmountIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where globalNetworkCommissionAmount not equals to DEFAULT_GLOBAL_NETWORK_COMMISSION_AMOUNT
        defaultCommissionShouldNotBeFound("globalNetworkCommissionAmount.notEquals=" + DEFAULT_GLOBAL_NETWORK_COMMISSION_AMOUNT);

        // Get all the commissionList where globalNetworkCommissionAmount not equals to UPDATED_GLOBAL_NETWORK_COMMISSION_AMOUNT
        defaultCommissionShouldBeFound("globalNetworkCommissionAmount.notEquals=" + UPDATED_GLOBAL_NETWORK_COMMISSION_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByGlobalNetworkCommissionAmountIsInShouldWork() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where globalNetworkCommissionAmount in DEFAULT_GLOBAL_NETWORK_COMMISSION_AMOUNT or UPDATED_GLOBAL_NETWORK_COMMISSION_AMOUNT
        defaultCommissionShouldBeFound(
            "globalNetworkCommissionAmount.in=" + DEFAULT_GLOBAL_NETWORK_COMMISSION_AMOUNT + "," + UPDATED_GLOBAL_NETWORK_COMMISSION_AMOUNT
        );

        // Get all the commissionList where globalNetworkCommissionAmount equals to UPDATED_GLOBAL_NETWORK_COMMISSION_AMOUNT
        defaultCommissionShouldNotBeFound("globalNetworkCommissionAmount.in=" + UPDATED_GLOBAL_NETWORK_COMMISSION_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByGlobalNetworkCommissionAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where globalNetworkCommissionAmount is not null
        defaultCommissionShouldBeFound("globalNetworkCommissionAmount.specified=true");

        // Get all the commissionList where globalNetworkCommissionAmount is null
        defaultCommissionShouldNotBeFound("globalNetworkCommissionAmount.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionsByGlobalNetworkCommissionAmountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where globalNetworkCommissionAmount is greater than or equal to DEFAULT_GLOBAL_NETWORK_COMMISSION_AMOUNT
        defaultCommissionShouldBeFound("globalNetworkCommissionAmount.greaterThanOrEqual=" + DEFAULT_GLOBAL_NETWORK_COMMISSION_AMOUNT);

        // Get all the commissionList where globalNetworkCommissionAmount is greater than or equal to UPDATED_GLOBAL_NETWORK_COMMISSION_AMOUNT
        defaultCommissionShouldNotBeFound("globalNetworkCommissionAmount.greaterThanOrEqual=" + UPDATED_GLOBAL_NETWORK_COMMISSION_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByGlobalNetworkCommissionAmountIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where globalNetworkCommissionAmount is less than or equal to DEFAULT_GLOBAL_NETWORK_COMMISSION_AMOUNT
        defaultCommissionShouldBeFound("globalNetworkCommissionAmount.lessThanOrEqual=" + DEFAULT_GLOBAL_NETWORK_COMMISSION_AMOUNT);

        // Get all the commissionList where globalNetworkCommissionAmount is less than or equal to SMALLER_GLOBAL_NETWORK_COMMISSION_AMOUNT
        defaultCommissionShouldNotBeFound("globalNetworkCommissionAmount.lessThanOrEqual=" + SMALLER_GLOBAL_NETWORK_COMMISSION_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByGlobalNetworkCommissionAmountIsLessThanSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where globalNetworkCommissionAmount is less than DEFAULT_GLOBAL_NETWORK_COMMISSION_AMOUNT
        defaultCommissionShouldNotBeFound("globalNetworkCommissionAmount.lessThan=" + DEFAULT_GLOBAL_NETWORK_COMMISSION_AMOUNT);

        // Get all the commissionList where globalNetworkCommissionAmount is less than UPDATED_GLOBAL_NETWORK_COMMISSION_AMOUNT
        defaultCommissionShouldBeFound("globalNetworkCommissionAmount.lessThan=" + UPDATED_GLOBAL_NETWORK_COMMISSION_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByGlobalNetworkCommissionAmountIsGreaterThanSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where globalNetworkCommissionAmount is greater than DEFAULT_GLOBAL_NETWORK_COMMISSION_AMOUNT
        defaultCommissionShouldNotBeFound("globalNetworkCommissionAmount.greaterThan=" + DEFAULT_GLOBAL_NETWORK_COMMISSION_AMOUNT);

        // Get all the commissionList where globalNetworkCommissionAmount is greater than SMALLER_GLOBAL_NETWORK_COMMISSION_AMOUNT
        defaultCommissionShouldBeFound("globalNetworkCommissionAmount.greaterThan=" + SMALLER_GLOBAL_NETWORK_COMMISSION_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByTransactionsAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where transactionsAmount equals to DEFAULT_TRANSACTIONS_AMOUNT
        defaultCommissionShouldBeFound("transactionsAmount.equals=" + DEFAULT_TRANSACTIONS_AMOUNT);

        // Get all the commissionList where transactionsAmount equals to UPDATED_TRANSACTIONS_AMOUNT
        defaultCommissionShouldNotBeFound("transactionsAmount.equals=" + UPDATED_TRANSACTIONS_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByTransactionsAmountIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where transactionsAmount not equals to DEFAULT_TRANSACTIONS_AMOUNT
        defaultCommissionShouldNotBeFound("transactionsAmount.notEquals=" + DEFAULT_TRANSACTIONS_AMOUNT);

        // Get all the commissionList where transactionsAmount not equals to UPDATED_TRANSACTIONS_AMOUNT
        defaultCommissionShouldBeFound("transactionsAmount.notEquals=" + UPDATED_TRANSACTIONS_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByTransactionsAmountIsInShouldWork() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where transactionsAmount in DEFAULT_TRANSACTIONS_AMOUNT or UPDATED_TRANSACTIONS_AMOUNT
        defaultCommissionShouldBeFound("transactionsAmount.in=" + DEFAULT_TRANSACTIONS_AMOUNT + "," + UPDATED_TRANSACTIONS_AMOUNT);

        // Get all the commissionList where transactionsAmount equals to UPDATED_TRANSACTIONS_AMOUNT
        defaultCommissionShouldNotBeFound("transactionsAmount.in=" + UPDATED_TRANSACTIONS_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByTransactionsAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where transactionsAmount is not null
        defaultCommissionShouldBeFound("transactionsAmount.specified=true");

        // Get all the commissionList where transactionsAmount is null
        defaultCommissionShouldNotBeFound("transactionsAmount.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionsByTransactionsAmountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where transactionsAmount is greater than or equal to DEFAULT_TRANSACTIONS_AMOUNT
        defaultCommissionShouldBeFound("transactionsAmount.greaterThanOrEqual=" + DEFAULT_TRANSACTIONS_AMOUNT);

        // Get all the commissionList where transactionsAmount is greater than or equal to UPDATED_TRANSACTIONS_AMOUNT
        defaultCommissionShouldNotBeFound("transactionsAmount.greaterThanOrEqual=" + UPDATED_TRANSACTIONS_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByTransactionsAmountIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where transactionsAmount is less than or equal to DEFAULT_TRANSACTIONS_AMOUNT
        defaultCommissionShouldBeFound("transactionsAmount.lessThanOrEqual=" + DEFAULT_TRANSACTIONS_AMOUNT);

        // Get all the commissionList where transactionsAmount is less than or equal to SMALLER_TRANSACTIONS_AMOUNT
        defaultCommissionShouldNotBeFound("transactionsAmount.lessThanOrEqual=" + SMALLER_TRANSACTIONS_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByTransactionsAmountIsLessThanSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where transactionsAmount is less than DEFAULT_TRANSACTIONS_AMOUNT
        defaultCommissionShouldNotBeFound("transactionsAmount.lessThan=" + DEFAULT_TRANSACTIONS_AMOUNT);

        // Get all the commissionList where transactionsAmount is less than UPDATED_TRANSACTIONS_AMOUNT
        defaultCommissionShouldBeFound("transactionsAmount.lessThan=" + UPDATED_TRANSACTIONS_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByTransactionsAmountIsGreaterThanSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where transactionsAmount is greater than DEFAULT_TRANSACTIONS_AMOUNT
        defaultCommissionShouldNotBeFound("transactionsAmount.greaterThan=" + DEFAULT_TRANSACTIONS_AMOUNT);

        // Get all the commissionList where transactionsAmount is greater than SMALLER_TRANSACTIONS_AMOUNT
        defaultCommissionShouldBeFound("transactionsAmount.greaterThan=" + SMALLER_TRANSACTIONS_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByFraudAmountIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where fraudAmount equals to DEFAULT_FRAUD_AMOUNT
        defaultCommissionShouldBeFound("fraudAmount.equals=" + DEFAULT_FRAUD_AMOUNT);

        // Get all the commissionList where fraudAmount equals to UPDATED_FRAUD_AMOUNT
        defaultCommissionShouldNotBeFound("fraudAmount.equals=" + UPDATED_FRAUD_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByFraudAmountIsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where fraudAmount not equals to DEFAULT_FRAUD_AMOUNT
        defaultCommissionShouldNotBeFound("fraudAmount.notEquals=" + DEFAULT_FRAUD_AMOUNT);

        // Get all the commissionList where fraudAmount not equals to UPDATED_FRAUD_AMOUNT
        defaultCommissionShouldBeFound("fraudAmount.notEquals=" + UPDATED_FRAUD_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByFraudAmountIsInShouldWork() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where fraudAmount in DEFAULT_FRAUD_AMOUNT or UPDATED_FRAUD_AMOUNT
        defaultCommissionShouldBeFound("fraudAmount.in=" + DEFAULT_FRAUD_AMOUNT + "," + UPDATED_FRAUD_AMOUNT);

        // Get all the commissionList where fraudAmount equals to UPDATED_FRAUD_AMOUNT
        defaultCommissionShouldNotBeFound("fraudAmount.in=" + UPDATED_FRAUD_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByFraudAmountIsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where fraudAmount is not null
        defaultCommissionShouldBeFound("fraudAmount.specified=true");

        // Get all the commissionList where fraudAmount is null
        defaultCommissionShouldNotBeFound("fraudAmount.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionsByFraudAmountIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where fraudAmount is greater than or equal to DEFAULT_FRAUD_AMOUNT
        defaultCommissionShouldBeFound("fraudAmount.greaterThanOrEqual=" + DEFAULT_FRAUD_AMOUNT);

        // Get all the commissionList where fraudAmount is greater than or equal to UPDATED_FRAUD_AMOUNT
        defaultCommissionShouldNotBeFound("fraudAmount.greaterThanOrEqual=" + UPDATED_FRAUD_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByFraudAmountIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where fraudAmount is less than or equal to DEFAULT_FRAUD_AMOUNT
        defaultCommissionShouldBeFound("fraudAmount.lessThanOrEqual=" + DEFAULT_FRAUD_AMOUNT);

        // Get all the commissionList where fraudAmount is less than or equal to SMALLER_FRAUD_AMOUNT
        defaultCommissionShouldNotBeFound("fraudAmount.lessThanOrEqual=" + SMALLER_FRAUD_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByFraudAmountIsLessThanSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where fraudAmount is less than DEFAULT_FRAUD_AMOUNT
        defaultCommissionShouldNotBeFound("fraudAmount.lessThan=" + DEFAULT_FRAUD_AMOUNT);

        // Get all the commissionList where fraudAmount is less than UPDATED_FRAUD_AMOUNT
        defaultCommissionShouldBeFound("fraudAmount.lessThan=" + UPDATED_FRAUD_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsByFraudAmountIsGreaterThanSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where fraudAmount is greater than DEFAULT_FRAUD_AMOUNT
        defaultCommissionShouldNotBeFound("fraudAmount.greaterThan=" + DEFAULT_FRAUD_AMOUNT);

        // Get all the commissionList where fraudAmount is greater than SMALLER_FRAUD_AMOUNT
        defaultCommissionShouldBeFound("fraudAmount.greaterThan=" + SMALLER_FRAUD_AMOUNT);
    }

    @Test
    @Transactional
    void getAllCommissionsBySpare1IsEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where spare1 equals to DEFAULT_SPARE_1
        defaultCommissionShouldBeFound("spare1.equals=" + DEFAULT_SPARE_1);

        // Get all the commissionList where spare1 equals to UPDATED_SPARE_1
        defaultCommissionShouldNotBeFound("spare1.equals=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCommissionsBySpare1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where spare1 not equals to DEFAULT_SPARE_1
        defaultCommissionShouldNotBeFound("spare1.notEquals=" + DEFAULT_SPARE_1);

        // Get all the commissionList where spare1 not equals to UPDATED_SPARE_1
        defaultCommissionShouldBeFound("spare1.notEquals=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCommissionsBySpare1IsInShouldWork() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where spare1 in DEFAULT_SPARE_1 or UPDATED_SPARE_1
        defaultCommissionShouldBeFound("spare1.in=" + DEFAULT_SPARE_1 + "," + UPDATED_SPARE_1);

        // Get all the commissionList where spare1 equals to UPDATED_SPARE_1
        defaultCommissionShouldNotBeFound("spare1.in=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCommissionsBySpare1IsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where spare1 is not null
        defaultCommissionShouldBeFound("spare1.specified=true");

        // Get all the commissionList where spare1 is null
        defaultCommissionShouldNotBeFound("spare1.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionsBySpare1ContainsSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where spare1 contains DEFAULT_SPARE_1
        defaultCommissionShouldBeFound("spare1.contains=" + DEFAULT_SPARE_1);

        // Get all the commissionList where spare1 contains UPDATED_SPARE_1
        defaultCommissionShouldNotBeFound("spare1.contains=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCommissionsBySpare1NotContainsSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where spare1 does not contain DEFAULT_SPARE_1
        defaultCommissionShouldNotBeFound("spare1.doesNotContain=" + DEFAULT_SPARE_1);

        // Get all the commissionList where spare1 does not contain UPDATED_SPARE_1
        defaultCommissionShouldBeFound("spare1.doesNotContain=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllCommissionsBySpare2IsEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where spare2 equals to DEFAULT_SPARE_2
        defaultCommissionShouldBeFound("spare2.equals=" + DEFAULT_SPARE_2);

        // Get all the commissionList where spare2 equals to UPDATED_SPARE_2
        defaultCommissionShouldNotBeFound("spare2.equals=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCommissionsBySpare2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where spare2 not equals to DEFAULT_SPARE_2
        defaultCommissionShouldNotBeFound("spare2.notEquals=" + DEFAULT_SPARE_2);

        // Get all the commissionList where spare2 not equals to UPDATED_SPARE_2
        defaultCommissionShouldBeFound("spare2.notEquals=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCommissionsBySpare2IsInShouldWork() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where spare2 in DEFAULT_SPARE_2 or UPDATED_SPARE_2
        defaultCommissionShouldBeFound("spare2.in=" + DEFAULT_SPARE_2 + "," + UPDATED_SPARE_2);

        // Get all the commissionList where spare2 equals to UPDATED_SPARE_2
        defaultCommissionShouldNotBeFound("spare2.in=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCommissionsBySpare2IsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where spare2 is not null
        defaultCommissionShouldBeFound("spare2.specified=true");

        // Get all the commissionList where spare2 is null
        defaultCommissionShouldNotBeFound("spare2.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionsBySpare2ContainsSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where spare2 contains DEFAULT_SPARE_2
        defaultCommissionShouldBeFound("spare2.contains=" + DEFAULT_SPARE_2);

        // Get all the commissionList where spare2 contains UPDATED_SPARE_2
        defaultCommissionShouldNotBeFound("spare2.contains=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCommissionsBySpare2NotContainsSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where spare2 does not contain DEFAULT_SPARE_2
        defaultCommissionShouldNotBeFound("spare2.doesNotContain=" + DEFAULT_SPARE_2);

        // Get all the commissionList where spare2 does not contain UPDATED_SPARE_2
        defaultCommissionShouldBeFound("spare2.doesNotContain=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllCommissionsBySpare3IsEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where spare3 equals to DEFAULT_SPARE_3
        defaultCommissionShouldBeFound("spare3.equals=" + DEFAULT_SPARE_3);

        // Get all the commissionList where spare3 equals to UPDATED_SPARE_3
        defaultCommissionShouldNotBeFound("spare3.equals=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCommissionsBySpare3IsNotEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where spare3 not equals to DEFAULT_SPARE_3
        defaultCommissionShouldNotBeFound("spare3.notEquals=" + DEFAULT_SPARE_3);

        // Get all the commissionList where spare3 not equals to UPDATED_SPARE_3
        defaultCommissionShouldBeFound("spare3.notEquals=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCommissionsBySpare3IsInShouldWork() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where spare3 in DEFAULT_SPARE_3 or UPDATED_SPARE_3
        defaultCommissionShouldBeFound("spare3.in=" + DEFAULT_SPARE_3 + "," + UPDATED_SPARE_3);

        // Get all the commissionList where spare3 equals to UPDATED_SPARE_3
        defaultCommissionShouldNotBeFound("spare3.in=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCommissionsBySpare3IsNullOrNotNull() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where spare3 is not null
        defaultCommissionShouldBeFound("spare3.specified=true");

        // Get all the commissionList where spare3 is null
        defaultCommissionShouldNotBeFound("spare3.specified=false");
    }

    @Test
    @Transactional
    void getAllCommissionsBySpare3ContainsSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where spare3 contains DEFAULT_SPARE_3
        defaultCommissionShouldBeFound("spare3.contains=" + DEFAULT_SPARE_3);

        // Get all the commissionList where spare3 contains UPDATED_SPARE_3
        defaultCommissionShouldNotBeFound("spare3.contains=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCommissionsBySpare3NotContainsSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        // Get all the commissionList where spare3 does not contain DEFAULT_SPARE_3
        defaultCommissionShouldNotBeFound("spare3.doesNotContain=" + DEFAULT_SPARE_3);

        // Get all the commissionList where spare3 does not contain UPDATED_SPARE_3
        defaultCommissionShouldBeFound("spare3.doesNotContain=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllCommissionsByCommissionAccountIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);
        CommissionAccount commissionAccount;
        if (TestUtil.findAll(em, CommissionAccount.class).isEmpty()) {
            commissionAccount = CommissionAccountResourceIT.createEntity(em);
            em.persist(commissionAccount);
            em.flush();
        } else {
            commissionAccount = TestUtil.findAll(em, CommissionAccount.class).get(0);
        }
        em.persist(commissionAccount);
        em.flush();
        commission.setCommissionAccount(commissionAccount);
        commissionRepository.saveAndFlush(commission);
        Long commissionAccountId = commissionAccount.getId();

        // Get all the commissionList where commissionAccount equals to commissionAccountId
        defaultCommissionShouldBeFound("commissionAccountId.equals=" + commissionAccountId);

        // Get all the commissionList where commissionAccount equals to (commissionAccountId + 1)
        defaultCommissionShouldNotBeFound("commissionAccountId.equals=" + (commissionAccountId + 1));
    }

    @Test
    @Transactional
    void getAllCommissionsByTransactionOperationIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);
        TransactionOperation transactionOperation;
        if (TestUtil.findAll(em, TransactionOperation.class).isEmpty()) {
            transactionOperation = TransactionOperationResourceIT.createEntity(em);
            em.persist(transactionOperation);
            em.flush();
        } else {
            transactionOperation = TestUtil.findAll(em, TransactionOperation.class).get(0);
        }
        em.persist(transactionOperation);
        em.flush();
        commission.addTransactionOperation(transactionOperation);
        commissionRepository.saveAndFlush(commission);
        Long transactionOperationId = transactionOperation.getId();

        // Get all the commissionList where transactionOperation equals to transactionOperationId
        defaultCommissionShouldBeFound("transactionOperationId.equals=" + transactionOperationId);

        // Get all the commissionList where transactionOperation equals to (transactionOperationId + 1)
        defaultCommissionShouldNotBeFound("transactionOperationId.equals=" + (transactionOperationId + 1));
    }

    @Test
    @Transactional
    void getAllCommissionsByConfigurationPlanIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);
        ConfigurationPlan configurationPlan;
        if (TestUtil.findAll(em, ConfigurationPlan.class).isEmpty()) {
            configurationPlan = ConfigurationPlanResourceIT.createEntity(em);
            em.persist(configurationPlan);
            em.flush();
        } else {
            configurationPlan = TestUtil.findAll(em, ConfigurationPlan.class).get(0);
        }
        em.persist(configurationPlan);
        em.flush();
        commission.setConfigurationPlan(configurationPlan);
        commissionRepository.saveAndFlush(commission);
        Long configurationPlanId = configurationPlan.getId();

        // Get all the commissionList where configurationPlan equals to configurationPlanId
        defaultCommissionShouldBeFound("configurationPlanId.equals=" + configurationPlanId);

        // Get all the commissionList where configurationPlan equals to (configurationPlanId + 1)
        defaultCommissionShouldNotBeFound("configurationPlanId.equals=" + (configurationPlanId + 1));
    }

    @Test
    @Transactional
    void getAllCommissionsByPaymentIsEqualToSomething() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);
        Payment payment;
        if (TestUtil.findAll(em, Payment.class).isEmpty()) {
            payment = PaymentResourceIT.createEntity(em);
            em.persist(payment);
            em.flush();
        } else {
            payment = TestUtil.findAll(em, Payment.class).get(0);
        }
        em.persist(payment);
        em.flush();
        commission.setPayment(payment);
        commissionRepository.saveAndFlush(commission);
        Long paymentId = payment.getId();

        // Get all the commissionList where payment equals to paymentId
        defaultCommissionShouldBeFound("paymentId.equals=" + paymentId);

        // Get all the commissionList where payment equals to (paymentId + 1)
        defaultCommissionShouldNotBeFound("paymentId.equals=" + (paymentId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCommissionShouldBeFound(String filter) throws Exception {
        restCommissionMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(commission.getId().intValue())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].calculatedAt").value(hasItem(DEFAULT_CALCULATED_AT.toString())))
            .andExpect(jsonPath("$.[*].calculationDate").value(hasItem(DEFAULT_CALCULATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].calculationShortDate").value(hasItem(DEFAULT_CALCULATION_SHORT_DATE.toString())))
            .andExpect(jsonPath("$.[*].senderMsisdn").value(hasItem(DEFAULT_SENDER_MSISDN)))
            .andExpect(jsonPath("$.[*].senderProfile").value(hasItem(DEFAULT_SENDER_PROFILE)))
            .andExpect(jsonPath("$.[*].serviceType").value(hasItem(DEFAULT_SERVICE_TYPE)))
            .andExpect(jsonPath("$.[*].commissionPaymentStatus").value(hasItem(DEFAULT_COMMISSION_PAYMENT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].commissioningPlanType").value(hasItem(DEFAULT_COMMISSIONING_PLAN_TYPE.toString())))
            .andExpect(
                jsonPath("$.[*].globalNetworkCommissionAmount").value(hasItem(DEFAULT_GLOBAL_NETWORK_COMMISSION_AMOUNT.doubleValue()))
            )
            .andExpect(jsonPath("$.[*].transactionsAmount").value(hasItem(DEFAULT_TRANSACTIONS_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].fraudAmount").value(hasItem(DEFAULT_FRAUD_AMOUNT.doubleValue())))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)));

        // Check, that the count call also returns 1
        restCommissionMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCommissionShouldNotBeFound(String filter) throws Exception {
        restCommissionMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCommissionMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingCommission() throws Exception {
        // Get the commission
        restCommissionMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewCommission() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        int databaseSizeBeforeUpdate = commissionRepository.findAll().size();

        // Update the commission
        Commission updatedCommission = commissionRepository.findById(commission.getId()).get();
        // Disconnect from session so that the updates on updatedCommission are not directly saved in db
        em.detach(updatedCommission);
        updatedCommission
            .amount(UPDATED_AMOUNT)
            .calculatedAt(UPDATED_CALCULATED_AT)
            .calculationDate(UPDATED_CALCULATION_DATE)
            .calculationShortDate(UPDATED_CALCULATION_SHORT_DATE)
            .senderMsisdn(UPDATED_SENDER_MSISDN)
            .senderProfile(UPDATED_SENDER_PROFILE)
            .serviceType(UPDATED_SERVICE_TYPE)
            .commissionPaymentStatus(UPDATED_COMMISSION_PAYMENT_STATUS)
            .commissioningPlanType(UPDATED_COMMISSIONING_PLAN_TYPE)
            .globalNetworkCommissionAmount(UPDATED_GLOBAL_NETWORK_COMMISSION_AMOUNT)
            .transactionsAmount(UPDATED_TRANSACTIONS_AMOUNT)
            .fraudAmount(UPDATED_FRAUD_AMOUNT)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);
        CommissionDTO commissionDTO = commissionMapper.toDto(updatedCommission);

        restCommissionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, commissionDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissionDTO))
            )
            .andExpect(status().isOk());

        // Validate the Commission in the database
        List<Commission> commissionList = commissionRepository.findAll();
        assertThat(commissionList).hasSize(databaseSizeBeforeUpdate);
        Commission testCommission = commissionList.get(commissionList.size() - 1);
        assertThat(testCommission.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testCommission.getCalculatedAt()).isEqualTo(UPDATED_CALCULATED_AT);
        assertThat(testCommission.getCalculationDate()).isEqualTo(UPDATED_CALCULATION_DATE);
        assertThat(testCommission.getCalculationShortDate()).isEqualTo(UPDATED_CALCULATION_SHORT_DATE);
        assertThat(testCommission.getSenderMsisdn()).isEqualTo(UPDATED_SENDER_MSISDN);
        assertThat(testCommission.getSenderProfile()).isEqualTo(UPDATED_SENDER_PROFILE);
        assertThat(testCommission.getServiceType()).isEqualTo(UPDATED_SERVICE_TYPE);
        assertThat(testCommission.getCommissionPaymentStatus()).isEqualTo(UPDATED_COMMISSION_PAYMENT_STATUS);
        assertThat(testCommission.getCommissioningPlanType()).isEqualTo(UPDATED_COMMISSIONING_PLAN_TYPE);
        assertThat(testCommission.getGlobalNetworkCommissionAmount()).isEqualTo(UPDATED_GLOBAL_NETWORK_COMMISSION_AMOUNT);
        assertThat(testCommission.getTransactionsAmount()).isEqualTo(UPDATED_TRANSACTIONS_AMOUNT);
        assertThat(testCommission.getFraudAmount()).isEqualTo(UPDATED_FRAUD_AMOUNT);
        assertThat(testCommission.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testCommission.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testCommission.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void putNonExistingCommission() throws Exception {
        int databaseSizeBeforeUpdate = commissionRepository.findAll().size();
        commission.setId(count.incrementAndGet());

        // Create the Commission
        CommissionDTO commissionDTO = commissionMapper.toDto(commission);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCommissionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, commissionDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Commission in the database
        List<Commission> commissionList = commissionRepository.findAll();
        assertThat(commissionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchCommission() throws Exception {
        int databaseSizeBeforeUpdate = commissionRepository.findAll().size();
        commission.setId(count.incrementAndGet());

        // Create the Commission
        CommissionDTO commissionDTO = commissionMapper.toDto(commission);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommissionMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(commissionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Commission in the database
        List<Commission> commissionList = commissionRepository.findAll();
        assertThat(commissionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamCommission() throws Exception {
        int databaseSizeBeforeUpdate = commissionRepository.findAll().size();
        commission.setId(count.incrementAndGet());

        // Create the Commission
        CommissionDTO commissionDTO = commissionMapper.toDto(commission);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommissionMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(commissionDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Commission in the database
        List<Commission> commissionList = commissionRepository.findAll();
        assertThat(commissionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateCommissionWithPatch() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        int databaseSizeBeforeUpdate = commissionRepository.findAll().size();

        // Update the commission using partial update
        Commission partialUpdatedCommission = new Commission();
        partialUpdatedCommission.setId(commission.getId());

        partialUpdatedCommission
            .calculatedAt(UPDATED_CALCULATED_AT)
            .calculationDate(UPDATED_CALCULATION_DATE)
            .calculationShortDate(UPDATED_CALCULATION_SHORT_DATE)
            .senderProfile(UPDATED_SENDER_PROFILE)
            .globalNetworkCommissionAmount(UPDATED_GLOBAL_NETWORK_COMMISSION_AMOUNT)
            .transactionsAmount(UPDATED_TRANSACTIONS_AMOUNT)
            .fraudAmount(UPDATED_FRAUD_AMOUNT)
            .spare2(UPDATED_SPARE_2);

        restCommissionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCommission.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCommission))
            )
            .andExpect(status().isOk());

        // Validate the Commission in the database
        List<Commission> commissionList = commissionRepository.findAll();
        assertThat(commissionList).hasSize(databaseSizeBeforeUpdate);
        Commission testCommission = commissionList.get(commissionList.size() - 1);
        assertThat(testCommission.getAmount()).isEqualTo(DEFAULT_AMOUNT);
        assertThat(testCommission.getCalculatedAt()).isEqualTo(UPDATED_CALCULATED_AT);
        assertThat(testCommission.getCalculationDate()).isEqualTo(UPDATED_CALCULATION_DATE);
        assertThat(testCommission.getCalculationShortDate()).isEqualTo(UPDATED_CALCULATION_SHORT_DATE);
        assertThat(testCommission.getSenderMsisdn()).isEqualTo(DEFAULT_SENDER_MSISDN);
        assertThat(testCommission.getSenderProfile()).isEqualTo(UPDATED_SENDER_PROFILE);
        assertThat(testCommission.getServiceType()).isEqualTo(DEFAULT_SERVICE_TYPE);
        assertThat(testCommission.getCommissionPaymentStatus()).isEqualTo(DEFAULT_COMMISSION_PAYMENT_STATUS);
        assertThat(testCommission.getCommissioningPlanType()).isEqualTo(DEFAULT_COMMISSIONING_PLAN_TYPE);
        assertThat(testCommission.getGlobalNetworkCommissionAmount()).isEqualTo(UPDATED_GLOBAL_NETWORK_COMMISSION_AMOUNT);
        assertThat(testCommission.getTransactionsAmount()).isEqualTo(UPDATED_TRANSACTIONS_AMOUNT);
        assertThat(testCommission.getFraudAmount()).isEqualTo(UPDATED_FRAUD_AMOUNT);
        assertThat(testCommission.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testCommission.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testCommission.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
    }

    @Test
    @Transactional
    void fullUpdateCommissionWithPatch() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        int databaseSizeBeforeUpdate = commissionRepository.findAll().size();

        // Update the commission using partial update
        Commission partialUpdatedCommission = new Commission();
        partialUpdatedCommission.setId(commission.getId());

        partialUpdatedCommission
            .amount(UPDATED_AMOUNT)
            .calculatedAt(UPDATED_CALCULATED_AT)
            .calculationDate(UPDATED_CALCULATION_DATE)
            .calculationShortDate(UPDATED_CALCULATION_SHORT_DATE)
            .senderMsisdn(UPDATED_SENDER_MSISDN)
            .senderProfile(UPDATED_SENDER_PROFILE)
            .serviceType(UPDATED_SERVICE_TYPE)
            .commissionPaymentStatus(UPDATED_COMMISSION_PAYMENT_STATUS)
            .commissioningPlanType(UPDATED_COMMISSIONING_PLAN_TYPE)
            .globalNetworkCommissionAmount(UPDATED_GLOBAL_NETWORK_COMMISSION_AMOUNT)
            .transactionsAmount(UPDATED_TRANSACTIONS_AMOUNT)
            .fraudAmount(UPDATED_FRAUD_AMOUNT)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);

        restCommissionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedCommission.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedCommission))
            )
            .andExpect(status().isOk());

        // Validate the Commission in the database
        List<Commission> commissionList = commissionRepository.findAll();
        assertThat(commissionList).hasSize(databaseSizeBeforeUpdate);
        Commission testCommission = commissionList.get(commissionList.size() - 1);
        assertThat(testCommission.getAmount()).isEqualTo(UPDATED_AMOUNT);
        assertThat(testCommission.getCalculatedAt()).isEqualTo(UPDATED_CALCULATED_AT);
        assertThat(testCommission.getCalculationDate()).isEqualTo(UPDATED_CALCULATION_DATE);
        assertThat(testCommission.getCalculationShortDate()).isEqualTo(UPDATED_CALCULATION_SHORT_DATE);
        assertThat(testCommission.getSenderMsisdn()).isEqualTo(UPDATED_SENDER_MSISDN);
        assertThat(testCommission.getSenderProfile()).isEqualTo(UPDATED_SENDER_PROFILE);
        assertThat(testCommission.getServiceType()).isEqualTo(UPDATED_SERVICE_TYPE);
        assertThat(testCommission.getCommissionPaymentStatus()).isEqualTo(UPDATED_COMMISSION_PAYMENT_STATUS);
        assertThat(testCommission.getCommissioningPlanType()).isEqualTo(UPDATED_COMMISSIONING_PLAN_TYPE);
        assertThat(testCommission.getGlobalNetworkCommissionAmount()).isEqualTo(UPDATED_GLOBAL_NETWORK_COMMISSION_AMOUNT);
        assertThat(testCommission.getTransactionsAmount()).isEqualTo(UPDATED_TRANSACTIONS_AMOUNT);
        assertThat(testCommission.getFraudAmount()).isEqualTo(UPDATED_FRAUD_AMOUNT);
        assertThat(testCommission.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testCommission.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testCommission.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void patchNonExistingCommission() throws Exception {
        int databaseSizeBeforeUpdate = commissionRepository.findAll().size();
        commission.setId(count.incrementAndGet());

        // Create the Commission
        CommissionDTO commissionDTO = commissionMapper.toDto(commission);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCommissionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, commissionDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(commissionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Commission in the database
        List<Commission> commissionList = commissionRepository.findAll();
        assertThat(commissionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchCommission() throws Exception {
        int databaseSizeBeforeUpdate = commissionRepository.findAll().size();
        commission.setId(count.incrementAndGet());

        // Create the Commission
        CommissionDTO commissionDTO = commissionMapper.toDto(commission);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommissionMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(commissionDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Commission in the database
        List<Commission> commissionList = commissionRepository.findAll();
        assertThat(commissionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamCommission() throws Exception {
        int databaseSizeBeforeUpdate = commissionRepository.findAll().size();
        commission.setId(count.incrementAndGet());

        // Create the Commission
        CommissionDTO commissionDTO = commissionMapper.toDto(commission);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restCommissionMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(commissionDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Commission in the database
        List<Commission> commissionList = commissionRepository.findAll();
        assertThat(commissionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteCommission() throws Exception {
        // Initialize the database
        commissionRepository.saveAndFlush(commission);

        int databaseSizeBeforeDelete = commissionRepository.findAll().size();

        // Delete the commission
        restCommissionMockMvc
            .perform(delete(ENTITY_API_URL_ID, commission.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Commission> commissionList = commissionRepository.findAll();
        assertThat(commissionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
