package sn.free.commissioning.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.IntegrationTest;
import sn.free.commissioning.domain.Blacklisted;
import sn.free.commissioning.domain.CommissioningPlan;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.repository.BlacklistedRepository;
import sn.free.commissioning.service.criteria.BlacklistedCriteria;
import sn.free.commissioning.service.dto.BlacklistedDTO;
import sn.free.commissioning.service.mapper.BlacklistedMapper;

/**
 * Integration tests for the {@link BlacklistedResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class BlacklistedResourceIT {

    private static final String DEFAULT_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REASON = "BBBBBBBBBB";

    private static final Boolean DEFAULT_BLOCKED = false;
    private static final Boolean UPDATED_BLOCKED = true;

    private static final String DEFAULT_CREATED_BY = "AAAAAAAAAA";
    private static final String UPDATED_CREATED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_LAST_MODIFIED_BY = "AAAAAAAAAA";
    private static final String UPDATED_LAST_MODIFIED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_MODIFIED_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_MODIFIED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_SPARE_1 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_2 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_2 = "BBBBBBBBBB";

    private static final String DEFAULT_SPARE_3 = "AAAAAAAAAA";
    private static final String UPDATED_SPARE_3 = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/blacklisteds";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private BlacklistedRepository blacklistedRepository;

    @Autowired
    private BlacklistedMapper blacklistedMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBlacklistedMockMvc;

    private Blacklisted blacklisted;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Blacklisted createEntity(EntityManager em) {
        Blacklisted blacklisted = new Blacklisted()
            .reason(DEFAULT_REASON)
            .blocked(DEFAULT_BLOCKED)
            .createdBy(DEFAULT_CREATED_BY)
            .createdDate(DEFAULT_CREATED_DATE)
            .lastModifiedBy(DEFAULT_LAST_MODIFIED_BY)
            .lastModifiedDate(DEFAULT_LAST_MODIFIED_DATE)
            .spare1(DEFAULT_SPARE_1)
            .spare2(DEFAULT_SPARE_2)
            .spare3(DEFAULT_SPARE_3);
        return blacklisted;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Blacklisted createUpdatedEntity(EntityManager em) {
        Blacklisted blacklisted = new Blacklisted()
            .reason(UPDATED_REASON)
            .blocked(UPDATED_BLOCKED)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);
        return blacklisted;
    }

    @BeforeEach
    public void initTest() {
        blacklisted = createEntity(em);
    }

    @Test
    @Transactional
    void createBlacklisted() throws Exception {
        int databaseSizeBeforeCreate = blacklistedRepository.findAll().size();
        // Create the Blacklisted
        BlacklistedDTO blacklistedDTO = blacklistedMapper.toDto(blacklisted);
        restBlacklistedMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(blacklistedDTO))
            )
            .andExpect(status().isCreated());

        // Validate the Blacklisted in the database
        List<Blacklisted> blacklistedList = blacklistedRepository.findAll();
        assertThat(blacklistedList).hasSize(databaseSizeBeforeCreate + 1);
        Blacklisted testBlacklisted = blacklistedList.get(blacklistedList.size() - 1);
        assertThat(testBlacklisted.getReason()).isEqualTo(DEFAULT_REASON);
        assertThat(testBlacklisted.getBlocked()).isEqualTo(DEFAULT_BLOCKED);
        assertThat(testBlacklisted.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testBlacklisted.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testBlacklisted.getLastModifiedBy()).isEqualTo(DEFAULT_LAST_MODIFIED_BY);
        assertThat(testBlacklisted.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testBlacklisted.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testBlacklisted.getSpare2()).isEqualTo(DEFAULT_SPARE_2);
        assertThat(testBlacklisted.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
    }

    @Test
    @Transactional
    void createBlacklistedWithExistingId() throws Exception {
        // Create the Blacklisted with an existing ID
        blacklisted.setId(1L);
        BlacklistedDTO blacklistedDTO = blacklistedMapper.toDto(blacklisted);

        int databaseSizeBeforeCreate = blacklistedRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restBlacklistedMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(blacklistedDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Blacklisted in the database
        List<Blacklisted> blacklistedList = blacklistedRepository.findAll();
        assertThat(blacklistedList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkCreatedByIsRequired() throws Exception {
        int databaseSizeBeforeTest = blacklistedRepository.findAll().size();
        // set the field null
        blacklisted.setCreatedBy(null);

        // Create the Blacklisted, which fails.
        BlacklistedDTO blacklistedDTO = blacklistedMapper.toDto(blacklisted);

        restBlacklistedMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(blacklistedDTO))
            )
            .andExpect(status().isBadRequest());

        List<Blacklisted> blacklistedList = blacklistedRepository.findAll();
        assertThat(blacklistedList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllBlacklisteds() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList
        restBlacklistedMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(blacklisted.getId().intValue())))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON)))
            .andExpect(jsonPath("$.[*].blocked").value(hasItem(DEFAULT_BLOCKED.booleanValue())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)));
    }

    @Test
    @Transactional
    void getBlacklisted() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get the blacklisted
        restBlacklistedMockMvc
            .perform(get(ENTITY_API_URL_ID, blacklisted.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(blacklisted.getId().intValue()))
            .andExpect(jsonPath("$.reason").value(DEFAULT_REASON))
            .andExpect(jsonPath("$.blocked").value(DEFAULT_BLOCKED.booleanValue()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.lastModifiedBy").value(DEFAULT_LAST_MODIFIED_BY))
            .andExpect(jsonPath("$.lastModifiedDate").value(DEFAULT_LAST_MODIFIED_DATE.toString()))
            .andExpect(jsonPath("$.spare1").value(DEFAULT_SPARE_1))
            .andExpect(jsonPath("$.spare2").value(DEFAULT_SPARE_2))
            .andExpect(jsonPath("$.spare3").value(DEFAULT_SPARE_3));
    }

    @Test
    @Transactional
    void getBlacklistedsByIdFiltering() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        Long id = blacklisted.getId();

        defaultBlacklistedShouldBeFound("id.equals=" + id);
        defaultBlacklistedShouldNotBeFound("id.notEquals=" + id);

        defaultBlacklistedShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultBlacklistedShouldNotBeFound("id.greaterThan=" + id);

        defaultBlacklistedShouldBeFound("id.lessThanOrEqual=" + id);
        defaultBlacklistedShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByReasonIsEqualToSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where reason equals to DEFAULT_REASON
        defaultBlacklistedShouldBeFound("reason.equals=" + DEFAULT_REASON);

        // Get all the blacklistedList where reason equals to UPDATED_REASON
        defaultBlacklistedShouldNotBeFound("reason.equals=" + UPDATED_REASON);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByReasonIsNotEqualToSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where reason not equals to DEFAULT_REASON
        defaultBlacklistedShouldNotBeFound("reason.notEquals=" + DEFAULT_REASON);

        // Get all the blacklistedList where reason not equals to UPDATED_REASON
        defaultBlacklistedShouldBeFound("reason.notEquals=" + UPDATED_REASON);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByReasonIsInShouldWork() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where reason in DEFAULT_REASON or UPDATED_REASON
        defaultBlacklistedShouldBeFound("reason.in=" + DEFAULT_REASON + "," + UPDATED_REASON);

        // Get all the blacklistedList where reason equals to UPDATED_REASON
        defaultBlacklistedShouldNotBeFound("reason.in=" + UPDATED_REASON);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByReasonIsNullOrNotNull() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where reason is not null
        defaultBlacklistedShouldBeFound("reason.specified=true");

        // Get all the blacklistedList where reason is null
        defaultBlacklistedShouldNotBeFound("reason.specified=false");
    }

    @Test
    @Transactional
    void getAllBlacklistedsByReasonContainsSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where reason contains DEFAULT_REASON
        defaultBlacklistedShouldBeFound("reason.contains=" + DEFAULT_REASON);

        // Get all the blacklistedList where reason contains UPDATED_REASON
        defaultBlacklistedShouldNotBeFound("reason.contains=" + UPDATED_REASON);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByReasonNotContainsSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where reason does not contain DEFAULT_REASON
        defaultBlacklistedShouldNotBeFound("reason.doesNotContain=" + DEFAULT_REASON);

        // Get all the blacklistedList where reason does not contain UPDATED_REASON
        defaultBlacklistedShouldBeFound("reason.doesNotContain=" + UPDATED_REASON);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByBlockedIsEqualToSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where blocked equals to DEFAULT_BLOCKED
        defaultBlacklistedShouldBeFound("blocked.equals=" + DEFAULT_BLOCKED);

        // Get all the blacklistedList where blocked equals to UPDATED_BLOCKED
        defaultBlacklistedShouldNotBeFound("blocked.equals=" + UPDATED_BLOCKED);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByBlockedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where blocked not equals to DEFAULT_BLOCKED
        defaultBlacklistedShouldNotBeFound("blocked.notEquals=" + DEFAULT_BLOCKED);

        // Get all the blacklistedList where blocked not equals to UPDATED_BLOCKED
        defaultBlacklistedShouldBeFound("blocked.notEquals=" + UPDATED_BLOCKED);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByBlockedIsInShouldWork() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where blocked in DEFAULT_BLOCKED or UPDATED_BLOCKED
        defaultBlacklistedShouldBeFound("blocked.in=" + DEFAULT_BLOCKED + "," + UPDATED_BLOCKED);

        // Get all the blacklistedList where blocked equals to UPDATED_BLOCKED
        defaultBlacklistedShouldNotBeFound("blocked.in=" + UPDATED_BLOCKED);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByBlockedIsNullOrNotNull() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where blocked is not null
        defaultBlacklistedShouldBeFound("blocked.specified=true");

        // Get all the blacklistedList where blocked is null
        defaultBlacklistedShouldNotBeFound("blocked.specified=false");
    }

    @Test
    @Transactional
    void getAllBlacklistedsByCreatedByIsEqualToSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where createdBy equals to DEFAULT_CREATED_BY
        defaultBlacklistedShouldBeFound("createdBy.equals=" + DEFAULT_CREATED_BY);

        // Get all the blacklistedList where createdBy equals to UPDATED_CREATED_BY
        defaultBlacklistedShouldNotBeFound("createdBy.equals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByCreatedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where createdBy not equals to DEFAULT_CREATED_BY
        defaultBlacklistedShouldNotBeFound("createdBy.notEquals=" + DEFAULT_CREATED_BY);

        // Get all the blacklistedList where createdBy not equals to UPDATED_CREATED_BY
        defaultBlacklistedShouldBeFound("createdBy.notEquals=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByCreatedByIsInShouldWork() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where createdBy in DEFAULT_CREATED_BY or UPDATED_CREATED_BY
        defaultBlacklistedShouldBeFound("createdBy.in=" + DEFAULT_CREATED_BY + "," + UPDATED_CREATED_BY);

        // Get all the blacklistedList where createdBy equals to UPDATED_CREATED_BY
        defaultBlacklistedShouldNotBeFound("createdBy.in=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByCreatedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where createdBy is not null
        defaultBlacklistedShouldBeFound("createdBy.specified=true");

        // Get all the blacklistedList where createdBy is null
        defaultBlacklistedShouldNotBeFound("createdBy.specified=false");
    }

    @Test
    @Transactional
    void getAllBlacklistedsByCreatedByContainsSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where createdBy contains DEFAULT_CREATED_BY
        defaultBlacklistedShouldBeFound("createdBy.contains=" + DEFAULT_CREATED_BY);

        // Get all the blacklistedList where createdBy contains UPDATED_CREATED_BY
        defaultBlacklistedShouldNotBeFound("createdBy.contains=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByCreatedByNotContainsSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where createdBy does not contain DEFAULT_CREATED_BY
        defaultBlacklistedShouldNotBeFound("createdBy.doesNotContain=" + DEFAULT_CREATED_BY);

        // Get all the blacklistedList where createdBy does not contain UPDATED_CREATED_BY
        defaultBlacklistedShouldBeFound("createdBy.doesNotContain=" + UPDATED_CREATED_BY);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByCreatedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where createdDate equals to DEFAULT_CREATED_DATE
        defaultBlacklistedShouldBeFound("createdDate.equals=" + DEFAULT_CREATED_DATE);

        // Get all the blacklistedList where createdDate equals to UPDATED_CREATED_DATE
        defaultBlacklistedShouldNotBeFound("createdDate.equals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByCreatedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where createdDate not equals to DEFAULT_CREATED_DATE
        defaultBlacklistedShouldNotBeFound("createdDate.notEquals=" + DEFAULT_CREATED_DATE);

        // Get all the blacklistedList where createdDate not equals to UPDATED_CREATED_DATE
        defaultBlacklistedShouldBeFound("createdDate.notEquals=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByCreatedDateIsInShouldWork() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where createdDate in DEFAULT_CREATED_DATE or UPDATED_CREATED_DATE
        defaultBlacklistedShouldBeFound("createdDate.in=" + DEFAULT_CREATED_DATE + "," + UPDATED_CREATED_DATE);

        // Get all the blacklistedList where createdDate equals to UPDATED_CREATED_DATE
        defaultBlacklistedShouldNotBeFound("createdDate.in=" + UPDATED_CREATED_DATE);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByCreatedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where createdDate is not null
        defaultBlacklistedShouldBeFound("createdDate.specified=true");

        // Get all the blacklistedList where createdDate is null
        defaultBlacklistedShouldNotBeFound("createdDate.specified=false");
    }

    @Test
    @Transactional
    void getAllBlacklistedsByLastModifiedByIsEqualToSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where lastModifiedBy equals to DEFAULT_LAST_MODIFIED_BY
        defaultBlacklistedShouldBeFound("lastModifiedBy.equals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the blacklistedList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultBlacklistedShouldNotBeFound("lastModifiedBy.equals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByLastModifiedByIsNotEqualToSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where lastModifiedBy not equals to DEFAULT_LAST_MODIFIED_BY
        defaultBlacklistedShouldNotBeFound("lastModifiedBy.notEquals=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the blacklistedList where lastModifiedBy not equals to UPDATED_LAST_MODIFIED_BY
        defaultBlacklistedShouldBeFound("lastModifiedBy.notEquals=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByLastModifiedByIsInShouldWork() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where lastModifiedBy in DEFAULT_LAST_MODIFIED_BY or UPDATED_LAST_MODIFIED_BY
        defaultBlacklistedShouldBeFound("lastModifiedBy.in=" + DEFAULT_LAST_MODIFIED_BY + "," + UPDATED_LAST_MODIFIED_BY);

        // Get all the blacklistedList where lastModifiedBy equals to UPDATED_LAST_MODIFIED_BY
        defaultBlacklistedShouldNotBeFound("lastModifiedBy.in=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByLastModifiedByIsNullOrNotNull() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where lastModifiedBy is not null
        defaultBlacklistedShouldBeFound("lastModifiedBy.specified=true");

        // Get all the blacklistedList where lastModifiedBy is null
        defaultBlacklistedShouldNotBeFound("lastModifiedBy.specified=false");
    }

    @Test
    @Transactional
    void getAllBlacklistedsByLastModifiedByContainsSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where lastModifiedBy contains DEFAULT_LAST_MODIFIED_BY
        defaultBlacklistedShouldBeFound("lastModifiedBy.contains=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the blacklistedList where lastModifiedBy contains UPDATED_LAST_MODIFIED_BY
        defaultBlacklistedShouldNotBeFound("lastModifiedBy.contains=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByLastModifiedByNotContainsSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where lastModifiedBy does not contain DEFAULT_LAST_MODIFIED_BY
        defaultBlacklistedShouldNotBeFound("lastModifiedBy.doesNotContain=" + DEFAULT_LAST_MODIFIED_BY);

        // Get all the blacklistedList where lastModifiedBy does not contain UPDATED_LAST_MODIFIED_BY
        defaultBlacklistedShouldBeFound("lastModifiedBy.doesNotContain=" + UPDATED_LAST_MODIFIED_BY);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByLastModifiedDateIsEqualToSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where lastModifiedDate equals to DEFAULT_LAST_MODIFIED_DATE
        defaultBlacklistedShouldBeFound("lastModifiedDate.equals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the blacklistedList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultBlacklistedShouldNotBeFound("lastModifiedDate.equals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByLastModifiedDateIsNotEqualToSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where lastModifiedDate not equals to DEFAULT_LAST_MODIFIED_DATE
        defaultBlacklistedShouldNotBeFound("lastModifiedDate.notEquals=" + DEFAULT_LAST_MODIFIED_DATE);

        // Get all the blacklistedList where lastModifiedDate not equals to UPDATED_LAST_MODIFIED_DATE
        defaultBlacklistedShouldBeFound("lastModifiedDate.notEquals=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByLastModifiedDateIsInShouldWork() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where lastModifiedDate in DEFAULT_LAST_MODIFIED_DATE or UPDATED_LAST_MODIFIED_DATE
        defaultBlacklistedShouldBeFound("lastModifiedDate.in=" + DEFAULT_LAST_MODIFIED_DATE + "," + UPDATED_LAST_MODIFIED_DATE);

        // Get all the blacklistedList where lastModifiedDate equals to UPDATED_LAST_MODIFIED_DATE
        defaultBlacklistedShouldNotBeFound("lastModifiedDate.in=" + UPDATED_LAST_MODIFIED_DATE);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByLastModifiedDateIsNullOrNotNull() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where lastModifiedDate is not null
        defaultBlacklistedShouldBeFound("lastModifiedDate.specified=true");

        // Get all the blacklistedList where lastModifiedDate is null
        defaultBlacklistedShouldNotBeFound("lastModifiedDate.specified=false");
    }

    @Test
    @Transactional
    void getAllBlacklistedsBySpare1IsEqualToSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where spare1 equals to DEFAULT_SPARE_1
        defaultBlacklistedShouldBeFound("spare1.equals=" + DEFAULT_SPARE_1);

        // Get all the blacklistedList where spare1 equals to UPDATED_SPARE_1
        defaultBlacklistedShouldNotBeFound("spare1.equals=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllBlacklistedsBySpare1IsNotEqualToSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where spare1 not equals to DEFAULT_SPARE_1
        defaultBlacklistedShouldNotBeFound("spare1.notEquals=" + DEFAULT_SPARE_1);

        // Get all the blacklistedList where spare1 not equals to UPDATED_SPARE_1
        defaultBlacklistedShouldBeFound("spare1.notEquals=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllBlacklistedsBySpare1IsInShouldWork() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where spare1 in DEFAULT_SPARE_1 or UPDATED_SPARE_1
        defaultBlacklistedShouldBeFound("spare1.in=" + DEFAULT_SPARE_1 + "," + UPDATED_SPARE_1);

        // Get all the blacklistedList where spare1 equals to UPDATED_SPARE_1
        defaultBlacklistedShouldNotBeFound("spare1.in=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllBlacklistedsBySpare1IsNullOrNotNull() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where spare1 is not null
        defaultBlacklistedShouldBeFound("spare1.specified=true");

        // Get all the blacklistedList where spare1 is null
        defaultBlacklistedShouldNotBeFound("spare1.specified=false");
    }

    @Test
    @Transactional
    void getAllBlacklistedsBySpare1ContainsSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where spare1 contains DEFAULT_SPARE_1
        defaultBlacklistedShouldBeFound("spare1.contains=" + DEFAULT_SPARE_1);

        // Get all the blacklistedList where spare1 contains UPDATED_SPARE_1
        defaultBlacklistedShouldNotBeFound("spare1.contains=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllBlacklistedsBySpare1NotContainsSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where spare1 does not contain DEFAULT_SPARE_1
        defaultBlacklistedShouldNotBeFound("spare1.doesNotContain=" + DEFAULT_SPARE_1);

        // Get all the blacklistedList where spare1 does not contain UPDATED_SPARE_1
        defaultBlacklistedShouldBeFound("spare1.doesNotContain=" + UPDATED_SPARE_1);
    }

    @Test
    @Transactional
    void getAllBlacklistedsBySpare2IsEqualToSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where spare2 equals to DEFAULT_SPARE_2
        defaultBlacklistedShouldBeFound("spare2.equals=" + DEFAULT_SPARE_2);

        // Get all the blacklistedList where spare2 equals to UPDATED_SPARE_2
        defaultBlacklistedShouldNotBeFound("spare2.equals=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllBlacklistedsBySpare2IsNotEqualToSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where spare2 not equals to DEFAULT_SPARE_2
        defaultBlacklistedShouldNotBeFound("spare2.notEquals=" + DEFAULT_SPARE_2);

        // Get all the blacklistedList where spare2 not equals to UPDATED_SPARE_2
        defaultBlacklistedShouldBeFound("spare2.notEquals=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllBlacklistedsBySpare2IsInShouldWork() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where spare2 in DEFAULT_SPARE_2 or UPDATED_SPARE_2
        defaultBlacklistedShouldBeFound("spare2.in=" + DEFAULT_SPARE_2 + "," + UPDATED_SPARE_2);

        // Get all the blacklistedList where spare2 equals to UPDATED_SPARE_2
        defaultBlacklistedShouldNotBeFound("spare2.in=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllBlacklistedsBySpare2IsNullOrNotNull() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where spare2 is not null
        defaultBlacklistedShouldBeFound("spare2.specified=true");

        // Get all the blacklistedList where spare2 is null
        defaultBlacklistedShouldNotBeFound("spare2.specified=false");
    }

    @Test
    @Transactional
    void getAllBlacklistedsBySpare2ContainsSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where spare2 contains DEFAULT_SPARE_2
        defaultBlacklistedShouldBeFound("spare2.contains=" + DEFAULT_SPARE_2);

        // Get all the blacklistedList where spare2 contains UPDATED_SPARE_2
        defaultBlacklistedShouldNotBeFound("spare2.contains=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllBlacklistedsBySpare2NotContainsSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where spare2 does not contain DEFAULT_SPARE_2
        defaultBlacklistedShouldNotBeFound("spare2.doesNotContain=" + DEFAULT_SPARE_2);

        // Get all the blacklistedList where spare2 does not contain UPDATED_SPARE_2
        defaultBlacklistedShouldBeFound("spare2.doesNotContain=" + UPDATED_SPARE_2);
    }

    @Test
    @Transactional
    void getAllBlacklistedsBySpare3IsEqualToSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where spare3 equals to DEFAULT_SPARE_3
        defaultBlacklistedShouldBeFound("spare3.equals=" + DEFAULT_SPARE_3);

        // Get all the blacklistedList where spare3 equals to UPDATED_SPARE_3
        defaultBlacklistedShouldNotBeFound("spare3.equals=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllBlacklistedsBySpare3IsNotEqualToSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where spare3 not equals to DEFAULT_SPARE_3
        defaultBlacklistedShouldNotBeFound("spare3.notEquals=" + DEFAULT_SPARE_3);

        // Get all the blacklistedList where spare3 not equals to UPDATED_SPARE_3
        defaultBlacklistedShouldBeFound("spare3.notEquals=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllBlacklistedsBySpare3IsInShouldWork() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where spare3 in DEFAULT_SPARE_3 or UPDATED_SPARE_3
        defaultBlacklistedShouldBeFound("spare3.in=" + DEFAULT_SPARE_3 + "," + UPDATED_SPARE_3);

        // Get all the blacklistedList where spare3 equals to UPDATED_SPARE_3
        defaultBlacklistedShouldNotBeFound("spare3.in=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllBlacklistedsBySpare3IsNullOrNotNull() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where spare3 is not null
        defaultBlacklistedShouldBeFound("spare3.specified=true");

        // Get all the blacklistedList where spare3 is null
        defaultBlacklistedShouldNotBeFound("spare3.specified=false");
    }

    @Test
    @Transactional
    void getAllBlacklistedsBySpare3ContainsSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where spare3 contains DEFAULT_SPARE_3
        defaultBlacklistedShouldBeFound("spare3.contains=" + DEFAULT_SPARE_3);

        // Get all the blacklistedList where spare3 contains UPDATED_SPARE_3
        defaultBlacklistedShouldNotBeFound("spare3.contains=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllBlacklistedsBySpare3NotContainsSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        // Get all the blacklistedList where spare3 does not contain DEFAULT_SPARE_3
        defaultBlacklistedShouldNotBeFound("spare3.doesNotContain=" + DEFAULT_SPARE_3);

        // Get all the blacklistedList where spare3 does not contain UPDATED_SPARE_3
        defaultBlacklistedShouldBeFound("spare3.doesNotContain=" + UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void getAllBlacklistedsByCommissioningPlanIsEqualToSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);
        CommissioningPlan commissioningPlan;
        if (TestUtil.findAll(em, CommissioningPlan.class).isEmpty()) {
            commissioningPlan = CommissioningPlanResourceIT.createEntity(em);
            em.persist(commissioningPlan);
            em.flush();
        } else {
            commissioningPlan = TestUtil.findAll(em, CommissioningPlan.class).get(0);
        }
        em.persist(commissioningPlan);
        em.flush();
        blacklisted.setCommissioningPlan(commissioningPlan);
        blacklistedRepository.saveAndFlush(blacklisted);
        Long commissioningPlanId = commissioningPlan.getId();

        // Get all the blacklistedList where commissioningPlan equals to commissioningPlanId
        defaultBlacklistedShouldBeFound("commissioningPlanId.equals=" + commissioningPlanId);

        // Get all the blacklistedList where commissioningPlan equals to (commissioningPlanId + 1)
        defaultBlacklistedShouldNotBeFound("commissioningPlanId.equals=" + (commissioningPlanId + 1));
    }

    @Test
    @Transactional
    void getAllBlacklistedsByPartnerIsEqualToSomething() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);
        Partner partner;
        if (TestUtil.findAll(em, Partner.class).isEmpty()) {
            partner = PartnerResourceIT.createEntity(em);
            em.persist(partner);
            em.flush();
        } else {
            partner = TestUtil.findAll(em, Partner.class).get(0);
        }
        em.persist(partner);
        em.flush();
        blacklisted.setPartner(partner);
        blacklistedRepository.saveAndFlush(blacklisted);
        Long partnerId = partner.getId();

        // Get all the blacklistedList where partner equals to partnerId
        defaultBlacklistedShouldBeFound("partnerId.equals=" + partnerId);

        // Get all the blacklistedList where partner equals to (partnerId + 1)
        defaultBlacklistedShouldNotBeFound("partnerId.equals=" + (partnerId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultBlacklistedShouldBeFound(String filter) throws Exception {
        restBlacklistedMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(blacklisted.getId().intValue())))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON)))
            .andExpect(jsonPath("$.[*].blocked").value(hasItem(DEFAULT_BLOCKED.booleanValue())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY)))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].lastModifiedBy").value(hasItem(DEFAULT_LAST_MODIFIED_BY)))
            .andExpect(jsonPath("$.[*].lastModifiedDate").value(hasItem(DEFAULT_LAST_MODIFIED_DATE.toString())))
            .andExpect(jsonPath("$.[*].spare1").value(hasItem(DEFAULT_SPARE_1)))
            .andExpect(jsonPath("$.[*].spare2").value(hasItem(DEFAULT_SPARE_2)))
            .andExpect(jsonPath("$.[*].spare3").value(hasItem(DEFAULT_SPARE_3)));

        // Check, that the count call also returns 1
        restBlacklistedMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultBlacklistedShouldNotBeFound(String filter) throws Exception {
        restBlacklistedMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restBlacklistedMockMvc
            .perform(get(ENTITY_API_URL + "/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    void getNonExistingBlacklisted() throws Exception {
        // Get the blacklisted
        restBlacklistedMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewBlacklisted() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        int databaseSizeBeforeUpdate = blacklistedRepository.findAll().size();

        // Update the blacklisted
        Blacklisted updatedBlacklisted = blacklistedRepository.findById(blacklisted.getId()).get();
        // Disconnect from session so that the updates on updatedBlacklisted are not directly saved in db
        em.detach(updatedBlacklisted);
        updatedBlacklisted
            .reason(UPDATED_REASON)
            .blocked(UPDATED_BLOCKED)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);
        BlacklistedDTO blacklistedDTO = blacklistedMapper.toDto(updatedBlacklisted);

        restBlacklistedMockMvc
            .perform(
                put(ENTITY_API_URL_ID, blacklistedDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(blacklistedDTO))
            )
            .andExpect(status().isOk());

        // Validate the Blacklisted in the database
        List<Blacklisted> blacklistedList = blacklistedRepository.findAll();
        assertThat(blacklistedList).hasSize(databaseSizeBeforeUpdate);
        Blacklisted testBlacklisted = blacklistedList.get(blacklistedList.size() - 1);
        assertThat(testBlacklisted.getReason()).isEqualTo(UPDATED_REASON);
        assertThat(testBlacklisted.getBlocked()).isEqualTo(UPDATED_BLOCKED);
        assertThat(testBlacklisted.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testBlacklisted.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testBlacklisted.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testBlacklisted.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testBlacklisted.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testBlacklisted.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testBlacklisted.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void putNonExistingBlacklisted() throws Exception {
        int databaseSizeBeforeUpdate = blacklistedRepository.findAll().size();
        blacklisted.setId(count.incrementAndGet());

        // Create the Blacklisted
        BlacklistedDTO blacklistedDTO = blacklistedMapper.toDto(blacklisted);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBlacklistedMockMvc
            .perform(
                put(ENTITY_API_URL_ID, blacklistedDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(blacklistedDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Blacklisted in the database
        List<Blacklisted> blacklistedList = blacklistedRepository.findAll();
        assertThat(blacklistedList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchBlacklisted() throws Exception {
        int databaseSizeBeforeUpdate = blacklistedRepository.findAll().size();
        blacklisted.setId(count.incrementAndGet());

        // Create the Blacklisted
        BlacklistedDTO blacklistedDTO = blacklistedMapper.toDto(blacklisted);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBlacklistedMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(blacklistedDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Blacklisted in the database
        List<Blacklisted> blacklistedList = blacklistedRepository.findAll();
        assertThat(blacklistedList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamBlacklisted() throws Exception {
        int databaseSizeBeforeUpdate = blacklistedRepository.findAll().size();
        blacklisted.setId(count.incrementAndGet());

        // Create the Blacklisted
        BlacklistedDTO blacklistedDTO = blacklistedMapper.toDto(blacklisted);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBlacklistedMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(blacklistedDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Blacklisted in the database
        List<Blacklisted> blacklistedList = blacklistedRepository.findAll();
        assertThat(blacklistedList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateBlacklistedWithPatch() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        int databaseSizeBeforeUpdate = blacklistedRepository.findAll().size();

        // Update the blacklisted using partial update
        Blacklisted partialUpdatedBlacklisted = new Blacklisted();
        partialUpdatedBlacklisted.setId(blacklisted.getId());

        partialUpdatedBlacklisted
            .reason(UPDATED_REASON)
            .blocked(UPDATED_BLOCKED)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .spare2(UPDATED_SPARE_2);

        restBlacklistedMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBlacklisted.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBlacklisted))
            )
            .andExpect(status().isOk());

        // Validate the Blacklisted in the database
        List<Blacklisted> blacklistedList = blacklistedRepository.findAll();
        assertThat(blacklistedList).hasSize(databaseSizeBeforeUpdate);
        Blacklisted testBlacklisted = blacklistedList.get(blacklistedList.size() - 1);
        assertThat(testBlacklisted.getReason()).isEqualTo(UPDATED_REASON);
        assertThat(testBlacklisted.getBlocked()).isEqualTo(UPDATED_BLOCKED);
        assertThat(testBlacklisted.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testBlacklisted.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testBlacklisted.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testBlacklisted.getLastModifiedDate()).isEqualTo(DEFAULT_LAST_MODIFIED_DATE);
        assertThat(testBlacklisted.getSpare1()).isEqualTo(DEFAULT_SPARE_1);
        assertThat(testBlacklisted.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testBlacklisted.getSpare3()).isEqualTo(DEFAULT_SPARE_3);
    }

    @Test
    @Transactional
    void fullUpdateBlacklistedWithPatch() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        int databaseSizeBeforeUpdate = blacklistedRepository.findAll().size();

        // Update the blacklisted using partial update
        Blacklisted partialUpdatedBlacklisted = new Blacklisted();
        partialUpdatedBlacklisted.setId(blacklisted.getId());

        partialUpdatedBlacklisted
            .reason(UPDATED_REASON)
            .blocked(UPDATED_BLOCKED)
            .createdBy(UPDATED_CREATED_BY)
            .createdDate(UPDATED_CREATED_DATE)
            .lastModifiedBy(UPDATED_LAST_MODIFIED_BY)
            .lastModifiedDate(UPDATED_LAST_MODIFIED_DATE)
            .spare1(UPDATED_SPARE_1)
            .spare2(UPDATED_SPARE_2)
            .spare3(UPDATED_SPARE_3);

        restBlacklistedMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedBlacklisted.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBlacklisted))
            )
            .andExpect(status().isOk());

        // Validate the Blacklisted in the database
        List<Blacklisted> blacklistedList = blacklistedRepository.findAll();
        assertThat(blacklistedList).hasSize(databaseSizeBeforeUpdate);
        Blacklisted testBlacklisted = blacklistedList.get(blacklistedList.size() - 1);
        assertThat(testBlacklisted.getReason()).isEqualTo(UPDATED_REASON);
        assertThat(testBlacklisted.getBlocked()).isEqualTo(UPDATED_BLOCKED);
        assertThat(testBlacklisted.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testBlacklisted.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testBlacklisted.getLastModifiedBy()).isEqualTo(UPDATED_LAST_MODIFIED_BY);
        assertThat(testBlacklisted.getLastModifiedDate()).isEqualTo(UPDATED_LAST_MODIFIED_DATE);
        assertThat(testBlacklisted.getSpare1()).isEqualTo(UPDATED_SPARE_1);
        assertThat(testBlacklisted.getSpare2()).isEqualTo(UPDATED_SPARE_2);
        assertThat(testBlacklisted.getSpare3()).isEqualTo(UPDATED_SPARE_3);
    }

    @Test
    @Transactional
    void patchNonExistingBlacklisted() throws Exception {
        int databaseSizeBeforeUpdate = blacklistedRepository.findAll().size();
        blacklisted.setId(count.incrementAndGet());

        // Create the Blacklisted
        BlacklistedDTO blacklistedDTO = blacklistedMapper.toDto(blacklisted);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBlacklistedMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, blacklistedDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(blacklistedDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Blacklisted in the database
        List<Blacklisted> blacklistedList = blacklistedRepository.findAll();
        assertThat(blacklistedList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchBlacklisted() throws Exception {
        int databaseSizeBeforeUpdate = blacklistedRepository.findAll().size();
        blacklisted.setId(count.incrementAndGet());

        // Create the Blacklisted
        BlacklistedDTO blacklistedDTO = blacklistedMapper.toDto(blacklisted);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBlacklistedMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(blacklistedDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Blacklisted in the database
        List<Blacklisted> blacklistedList = blacklistedRepository.findAll();
        assertThat(blacklistedList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamBlacklisted() throws Exception {
        int databaseSizeBeforeUpdate = blacklistedRepository.findAll().size();
        blacklisted.setId(count.incrementAndGet());

        // Create the Blacklisted
        BlacklistedDTO blacklistedDTO = blacklistedMapper.toDto(blacklisted);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restBlacklistedMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(blacklistedDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Blacklisted in the database
        List<Blacklisted> blacklistedList = blacklistedRepository.findAll();
        assertThat(blacklistedList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteBlacklisted() throws Exception {
        // Initialize the database
        blacklistedRepository.saveAndFlush(blacklisted);

        int databaseSizeBeforeDelete = blacklistedRepository.findAll().size();

        // Delete the blacklisted
        restBlacklistedMockMvc
            .perform(delete(ENTITY_API_URL_ID, blacklisted.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Blacklisted> blacklistedList = blacklistedRepository.findAll();
        assertThat(blacklistedList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
