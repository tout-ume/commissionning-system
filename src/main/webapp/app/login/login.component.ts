import { Component, ViewChild, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { LoginService } from 'app/login/login.service';
import { AccountService } from 'app/core/auth/account.service';
import Swal from 'sweetalert2';
import { BnNgIdleService } from 'bn-ng-idle';

@Component({
  selector: 'jhi-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, AfterViewInit {
  @ViewChild('username', { static: false })
  username!: ElementRef;
  isLoading = false;

  authenticationError = false;

  loginForm = this.fb.group({
    username: [null, [Validators.required]],
    password: [null, [Validators.required]],
    rememberMe: [false],
  });

  constructor(
    private accountService: AccountService,
    private loginService: LoginService,
    private router: Router,
    private fb: FormBuilder,
    private bnIdle: BnNgIdleService
  ) {}

  ngOnInit(): void {
    // if already authenticated then navigate to home page
    this.accountService.identity().subscribe(() => {
      if (this.accountService.isAuthenticated()) {
        this.setupIdleSessionListener();
        this.router.navigate(['']);
      }
    });
  }

  ngAfterViewInit(): void {
    this.username.nativeElement.focus();
  }

  logout(): void {
    this.loginService.logout();
    this.router.navigate(['/']);
  }

  setupIdleSessionListener(): void {
    this.bnIdle.startWatching(600).subscribe((isTimedOut: boolean) => {
      if (isTimedOut) {
        let timerInterval: any;
        Swal.fire({
          title: 'Attention!',
          confirmButtonColor: '#3085d6',
          confirmButtonText: 'Non, je suis là!',
          html: 'Vous allez être déconnectés dans <b>30</b> secondes.',
          timer: 30000,
          icon: 'warning',
          didOpen() {
            const b = Swal.getHtmlContainer()!.querySelector('b');
            timerInterval = setInterval(() => {
              if (b != null) {
                b.textContent = String(Math.floor(Swal.getTimerLeft()! / 1000));
              }
            }, 1000);
          },
          willClose() {
            clearInterval(timerInterval);
          },
        }).then(result => {
          if (result.isConfirmed) {
            this.bnIdle.resetTimer();
          } else if (result.dismiss === Swal.DismissReason.timer) {
            this.logout();
          }
        });
      }
    });
  }

  login(): void {
    this.isLoading = true;
    this.loginService
      .login({
        username: this.loginForm.get('username')!.value,
        password: this.loginForm.get('password')!.value,
        rememberMe: this.loginForm.get('rememberMe')!.value,
      })
      .subscribe({
        next: () => {
          this.setupIdleSessionListener();
          this.authenticationError = false;
          this.isLoading = false;
          if (!this.router.getCurrentNavigation()) {
            // There were no routing during login (eg from navigationToStoredUrl)
            this.router.navigate(['home']);
          }
        },
        error: () => {
          this.authenticationError = true;
          this.isLoading = false;
        },
      });
  }
}
