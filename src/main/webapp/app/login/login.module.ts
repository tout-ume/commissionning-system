import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import { LOGIN_ROUTE } from './login.route';
import { LoginComponent } from './login.component';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [SharedModule, RouterModule.forChild([LOGIN_ROUTE]), NgxLoadingModule],
  declarations: [LoginComponent],
})
export class LoginModule {}
