import dayjs from 'dayjs/esm';
import { ICommission } from 'app/entities/commission/commission.model';
import { PaymentStatus } from 'app/entities/enumerations/payment-status.model';

export interface IPayment {
  id?: number;
  amount?: number | null;
  senderMsisdn?: string | null;
  receiverMsisdn?: string | null;
  receiverProfile?: string | null;
  periodStartDate?: dayjs.Dayjs | null;
  periodEndDate?: dayjs.Dayjs | null;
  executionDuration?: number | null;
  createdBy?: string;
  createdDate?: dayjs.Dayjs | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: dayjs.Dayjs | null;
  paymentStatus?: PaymentStatus | null;
  spare1?: string | null;
  spare2?: string | null;
  spare3?: string | null;
  commissions?: ICommission[] | null;
}

export class Payment implements IPayment {
  constructor(
    public id?: number,
    public amount?: number | null,
    public senderMsisdn?: string | null,
    public receiverMsisdn?: string | null,
    public receiverProfile?: string | null,
    public periodStartDate?: dayjs.Dayjs | null,
    public periodEndDate?: dayjs.Dayjs | null,
    public executionDuration?: number | null,
    public createdBy?: string,
    public createdDate?: dayjs.Dayjs | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: dayjs.Dayjs | null,
    public paymentStatus?: PaymentStatus | null,
    public spare1?: string | null,
    public spare2?: string | null,
    public spare3?: string | null,
    public commissions?: ICommission[] | null
  ) {}
}

export function getPaymentIdentifier(payment: IPayment): number | undefined {
  return payment.id;
}
