import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { PaymentComponent } from './list/payment.component';
import { PaymentDetailComponent } from './detail/payment-detail.component';
import { PaymentUpdateComponent } from './update/payment-update.component';
import { PaymentDeleteDialogComponent } from './delete/payment-delete-dialog.component';
import { PaymentRoutingModule } from './route/payment-routing.module';
import { NgxLoadingModule } from 'ngx-loading';
import { MatFormFieldModule } from '@angular/material/form-field';
import { NgxMultipleDatesModule } from 'ngx-multiple-dates';
import { MatDatepickerModule } from '@angular/material/datepicker';

@NgModule({
  imports: [SharedModule, PaymentRoutingModule, NgxLoadingModule, MatFormFieldModule, NgxMultipleDatesModule, MatDatepickerModule],
  declarations: [PaymentComponent, PaymentDetailComponent, PaymentUpdateComponent, PaymentDeleteDialogComponent],
  entryComponents: [PaymentDeleteDialogComponent],
})
export class PaymentModule {}
