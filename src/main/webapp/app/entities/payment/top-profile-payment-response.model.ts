import { PaymentResult } from './payment-result.model';
import { Commission } from '../commission/commission.model';

export interface ITopProfilePaymentResponse {
  result?: PaymentResult;
  commission?: Commission;
}

export class TopProfilePaymentResponse implements ITopProfilePaymentResponse {
  constructor(public result?: PaymentResult, public commission?: Commission) {}
}
