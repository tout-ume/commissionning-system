export interface IDailyCalculationInfo {
  frequencyId?: number;
  days?: string[] | null;
}

export class DailyCalculationInfo implements IDailyCalculationInfo {
  constructor(public frequencyId?: number, public days?: string[] | null) {}
}
