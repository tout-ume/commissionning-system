export interface ICalculationResult {
  message?: string;
}

export class CalculationResult implements ICalculationResult {
  constructor(public message?: string) {}
}
