import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPayment, getPaymentIdentifier } from '../payment.model';
import { IDailyCalculationInfo } from '../calculation.model';
import { ICalculationResult } from '../calculation-result.model';
import { ITopProfilePaymentResponse } from '../top-profile-payment-response.model';
import { ICommissionBulk } from '../commission-bulk.model';

export type EntityResponseType = HttpResponse<IPayment>;
export type EntityArrayResponseType = HttpResponse<IPayment[]>;

@Injectable({ providedIn: 'root' })
export class PaymentService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/payment');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  recalculate(dailyCalculationInfo: IDailyCalculationInfo, planId: number | undefined): Observable<HttpResponse<ICalculationResult>> {
    return this.http
      .post<ICalculationResult>(
        this.applicationConfigService.getEndpointFor('api/calculation/calculate-for-days/' + planId!.toString()),
        dailyCalculationInfo,
        {
          observe: 'response',
        }
      )
      .pipe(map((res: HttpResponse<ICalculationResult>) => res));
  }

  getOperationsBalance(): Observable<HttpResponse<string>> {
    return this.http
      .get<string>(`${this.resourceUrl}/operation-balance`, { observe: 'response' })
      .pipe(map((res: HttpResponse<string>) => res));
  }

  performPayment(commissionIds: number[]): Observable<HttpResponse<ITopProfilePaymentResponse[]>> {
    return this.http
      .post<ITopProfilePaymentResponse[]>(
        this.applicationConfigService.getEndpointFor(this.resourceUrl + '/pay-top-profile'),
        commissionIds,
        { observe: 'response' }
      )
      .pipe(map((res: HttpResponse<ITopProfilePaymentResponse[]>) => res));
  }

  performBulkPayment(commissions: ICommissionBulk[]): Observable<HttpResponse<ICommissionBulk[]>> {
    return this.http
      .post<ICommissionBulk[]>(this.applicationConfigService.getEndpointFor(this.resourceUrl + '/bulk-payment'), commissions, {
        observe: 'response',
      })
      .pipe(map((res: HttpResponse<ICommissionBulk[]>) => res));
  }

  create(payment: IPayment): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(payment);
    return this.http
      .post<IPayment>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(payment: IPayment): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(payment);
    return this.http
      .put<IPayment>(`${this.resourceUrl}/${getPaymentIdentifier(payment) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(payment: IPayment): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(payment);
    return this.http
      .patch<IPayment>(`${this.resourceUrl}/${getPaymentIdentifier(payment) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IPayment>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPayment[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPaymentToCollectionIfMissing(paymentCollection: IPayment[], ...paymentsToCheck: (IPayment | null | undefined)[]): IPayment[] {
    const payments: IPayment[] = paymentsToCheck.filter(isPresent);
    if (payments.length > 0) {
      const paymentCollectionIdentifiers = paymentCollection.map(paymentItem => getPaymentIdentifier(paymentItem)!);
      const paymentsToAdd = payments.filter(paymentItem => {
        const paymentIdentifier = getPaymentIdentifier(paymentItem);
        if (paymentIdentifier == null || paymentCollectionIdentifiers.includes(paymentIdentifier)) {
          return false;
        }
        paymentCollectionIdentifiers.push(paymentIdentifier);
        return true;
      });
      return [...paymentsToAdd, ...paymentCollection];
    }
    return paymentCollection;
  }

  protected convertDateFromClient(payment: IPayment): IPayment {
    return Object.assign({}, payment, {
      periodStartDate: payment.periodStartDate?.isValid() ? payment.periodStartDate.toJSON() : undefined,
      periodEndDate: payment.periodEndDate?.isValid() ? payment.periodEndDate.toJSON() : undefined,
      createdDate: payment.createdDate?.isValid() ? payment.createdDate.toJSON() : undefined,
      lastModifiedDate: payment.lastModifiedDate?.isValid() ? payment.lastModifiedDate.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.periodStartDate = res.body.periodStartDate ? dayjs(res.body.periodStartDate) : undefined;
      res.body.periodEndDate = res.body.periodEndDate ? dayjs(res.body.periodEndDate) : undefined;
      res.body.createdDate = res.body.createdDate ? dayjs(res.body.createdDate) : undefined;
      res.body.lastModifiedDate = res.body.lastModifiedDate ? dayjs(res.body.lastModifiedDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((payment: IPayment) => {
        payment.periodStartDate = payment.periodStartDate ? dayjs(payment.periodStartDate) : undefined;
        payment.periodEndDate = payment.periodEndDate ? dayjs(payment.periodEndDate) : undefined;
        payment.createdDate = payment.createdDate ? dayjs(payment.createdDate) : undefined;
        payment.lastModifiedDate = payment.lastModifiedDate ? dayjs(payment.lastModifiedDate) : undefined;
      });
    }
    return res;
  }
}
