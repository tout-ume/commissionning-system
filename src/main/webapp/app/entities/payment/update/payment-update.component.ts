import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { CommissionService } from '../../commission/service/commission.service';
import { CommissioningPlanService } from '../../commissioning-plan/service/commissioning-plan.service';
import { ICommissioningPlan } from '../../commissioning-plan/commissioning-plan.model';
import { PaymentService } from '../service/payment.service';
import { DailyCalculationInfo, IDailyCalculationInfo } from '../calculation.model';
import Swal from 'sweetalert2';
import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from '../../../config/input.constants';
import { ITopProfilePaymentResponse } from '../top-profile-payment-response.model';
import { ICommissionBulk } from '../commission-bulk.model';
import { PaymentStatus } from '../../enumerations/payment-status.model';
import { ASC, DESC, ITEMS_PER_PAGE } from '../../../config/pagination.constants';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'jhi-payment-update',
  templateUrl: './payment-update.component.html',
})
export class PaymentUpdateComponent implements OnInit {
  isLoading = false;
  isLoadingBalance = false;
  canShowCalendar = false;
  canShowSearchResult = false;
  canPay = true;
  canSearchCommissions = true;
  canRecalculate = false;
  commissionBulk!: ICommissionBulk[];
  totalItems = 0;
  itemsPerPage = 100;
  page?: number;
  predicate = 'id';
  ascending!: boolean;
  ngbPaginationPage = 1;
  commissioningPlans: ICommissioningPlan[] | null | undefined;
  selectedPlan!: ICommissioningPlan | null;
  dates: Date[] = [];
  topProfilePayments!: ITopProfilePaymentResponse[] | null;
  partnersPayments!: ICommissionBulk[] | null;
  min = new Date(+new Date() - 14 * 24 * 60 * 60 * 1000);
  max = new Date(+new Date() - 24 * 60 * 60 * 1000);
  operationsBalance!: number;
  lastBalanceUpdate!: dayjs.Dayjs;
  totalAmount = 0;
  allProfiles: string[] = [];
  selectedProfiles: string[] = [];
  Dayjs = dayjs;

  editForm = this.fb.group({
    commissioningPlan: [],
  });

  constructor(
    protected commissionService: CommissionService,
    protected commissioningPlanService: CommissioningPlanService,
    protected paymentService: PaymentService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.commissioningPlanService.getPlansToPayToday().subscribe(response => {
      if (response.body) {
        this.commissioningPlans = response.body;
        this.getOperationsBalance(true);
      }
    });
  }

  toDayjs(date: string): dayjs.Dayjs {
    return dayjs(date, DATE_TIME_FORMAT);
  }

  getOperationsBalance(fromInit: boolean): void {
    this.isLoadingBalance = true;
    this.paymentService.getOperationsBalance().subscribe(
      response => {
        this.isLoadingBalance = false;
        this.operationsBalance = Number(response.body!);
        this.lastBalanceUpdate = dayjs();
      },
      error => {
        console.warn('********************* Error: ', error);
        this.isLoadingBalance = false;
        if (!fromInit) {
          Swal.fire({
            title: 'Erreur!',
            // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
            text: 'Une erreur est survenue, lors de la récupération du solde.',
            icon: 'error',
            confirmButtonText: 'Okay',
          });
        }
      }
    );
  }

  recalculate(): void {
    this.isLoading = true;
    const recalculationInfo: IDailyCalculationInfo = new DailyCalculationInfo();
    recalculationInfo.days = this.dates.map(item => item.toJSON());
    recalculationInfo.frequencyId = this.selectedPlan?.calculusFrequency?.id;
    this.paymentService.recalculate(recalculationInfo, this.selectedPlan?.id).subscribe(
      response => {
        this.isLoading = false;
        Swal.fire({
          title: 'Succès!',
          text: response.body!.message,
          icon: 'success',
          confirmButtonText: 'Okay',
        });
        console.warn('********************** Response', response.body);
        this.fetchCommissions();
        this.toggleShowCalendar();
      },
      error => {
        console.warn('********************* Error: ', error);
        Swal.fire({
          title: 'Erreur!',
          // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
          text: `Une erreur est survenue, veuillez réessayer. Message: ${error.error.title}`,
          icon: 'error',
          confirmButtonText: 'Okay',
        });
      }
    );
  }

  performPayment(): void {
    if (this.canPay && this.totalAmount > 0 && this.totalAmount <= this.operationsBalance) {
      Swal.fire({
        title: 'Confirmation',
        text: 'Êtes-vous sûre de vouloir procéder au paiement?',
        showCancelButton: true,
        confirmButtonText: 'Payer',
        cancelButtonText: 'Annuler',
      }).then(result => {
        if (result.isConfirmed) {
          this.isLoading = true;
          let hasError = false;
          const commissions = this.commissionBulk.filter(item => item.isSelected);
          this.paymentService.performBulkPayment(commissions).subscribe(
            response => {
              this.isLoading = false;
              this.partnersPayments = response.body;
              this.canPay = false;
              this.canShowSearchResult = false;
              console.warn('********************** Response', response.body);
              this.partnersPayments?.forEach(item => {
                if (item.paymentResult!.status === PaymentStatus.FAILURE) {
                  hasError = true;
                }
              });
              if (hasError) {
                Swal.fire({
                  title: 'Attention!',
                  text: 'Certains paiements ne sont pas passés. Veuillez vérifier',
                  icon: 'warning',
                  confirmButtonText: 'Okay',
                });
              } else {
                Swal.fire({
                  title: 'Succès!',
                  text: 'Paiement effectué!',
                  icon: 'success',
                  confirmButtonText: 'Okay',
                });
              }
            },
            error => {
              this.isLoading = false;
              console.warn('********************* Error: ', error);
              Swal.fire({
                title: 'Erreur!',
                // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
                text: `Une erreur est survenue, veuillez réessayer. Message: ${error.error.title}`,
                icon: 'error',
                confirmButtonText: 'Okay',
              });
            }
          );
        }
      });
    }
  }

  toggleShowCalendar(): void {
    this.canShowSearchResult = !this.canShowSearchResult;
    this.canShowCalendar = !this.canShowCalendar;
    this.canSearchCommissions = !this.canSearchCommissions;
    this.canPay = !this.canPay;
  }

  computeTotalAmount(): void {
    this.isLoading = true;
    this.totalAmount = 0;
    this.commissionBulk.forEach(item => {
      if (this.selectedProfiles.includes(item.partnerProfile!)) {
        item.isSelected = true;
        const amount = item.commissions?.reduce((a, b) => a + b.amount!, 0);
        this.totalAmount += amount!;
      } else {
        item.isSelected = false;
      }
    });
    this.isLoading = false;
  }

  toggleProfile(profile: string): void {
    if (this.selectedProfiles.includes(profile)) {
      const index = this.selectedProfiles.indexOf(profile);
      this.selectedProfiles.splice(index, 1);
    } else {
      this.selectedProfiles.push(profile);
    }
    this.computeTotalAmount();
  }

  fetchCommissions(page?: number): void {
    this.isLoading = true;
    this.totalAmount = 0;
    const pageToLoad: number = page ?? this.page ?? 1;
    this.allProfiles = [];
    this.canShowSearchResult = false;
    this.selectedPlan = this.editForm.get('commissioningPlan')?.value;
    if (this.selectedPlan?.id != null) {
      this.commissionService
        .getAllProfilesCommissionsByPlan(this.selectedPlan.id, {
          page: pageToLoad - 1,
          size: this.itemsPerPage,
        })
        .subscribe(
          result => {
            if (result.body) {
              this.commissionBulk = result.body;
              this.totalItems = Number(result.headers.get('X-Total-Count'));
              this.page = pageToLoad;
              if (result.body.length > 0) {
                this.canShowSearchResult = true;
                this.canRecalculate = true;
                this.canPay = true;
                this.commissionBulk.forEach(item => {
                  item.isSelected = true;
                  if (!this.allProfiles.includes(item.partnerProfile!)) {
                    this.allProfiles.push(item.partnerProfile!);
                  }
                  const amount = item.commissions?.reduce((a, b) => a + b.amount!, 0);
                  this.totalAmount += amount!;
                });
                this.selectedProfiles = Object.assign([], this.allProfiles);
              }
            }
            this.isLoading = false;
          },
          error => {
            console.warn('******************* Error ', error);
            this.isLoading = false;
            if (error.status === 480) {
              Swal.fire({
                title: 'Erreur!',
                // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
                text: `Une erreur est survenue. Message: ${error.error}`,
                icon: 'error',
                confirmButtonText: 'Okay',
              });
            }
          }
        );
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    // this.isSaving = true;
    // const payment = this.createFromForm();
    // if (payment.id !== undefined) {
    //   this.subscribeToSaveResponse(this.paymentService.update(payment));
    // } else {
    //   this.subscribeToSaveResponse(this.paymentService.create(payment));
    // }
  }

  // protected subscribeToSaveResponse(result: Observable<HttpResponse<IPayment>>): void {
  //   result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
  //     next: () => this.onSaveSuccess(),
  //     error: () => this.onSaveError(),
  //   });
  // }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isLoading = false;
  }

  protected sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? ASC : DESC)];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  // protected updateForm(payment: IPayment): void {
  //   this.editForm.patchValue({
  //     id: payment.id,
  //     amount: payment.amount,
  //     senderMsisdn: payment.senderMsisdn,
  //     receiverMsisdn: payment.receiverMsisdn,
  //     periodStartDate: payment.periodStartDate ? payment.periodStartDate.format(DATE_TIME_FORMAT) : null,
  //     periodEndDate: payment.periodEndDate ? payment.periodEndDate.format(DATE_TIME_FORMAT) : null,
  //     executionDuration: payment.executionDuration,
  //     createdBy: payment.createdBy,
  //     paymentStatus: payment.paymentStatus,
  //     spare1: payment.spare1,
  //     spare2: payment.spare2,
  //     spare3: payment.spare3,
  //   });
  // }
  //
  // protected createFromForm(): IPayment {
  //   return {
  //     ...new Payment(),
  //     id: this.editForm.get(['id'])!.value,
  //     amount: this.editForm.get(['amount'])!.value,
  //     senderMsisdn: this.editForm.get(['senderMsisdn'])!.value,
  //     receiverMsisdn: this.editForm.get(['receiverMsisdn'])!.value,
  //     periodStartDate: this.editForm.get(['periodStartDate'])!.value
  //       ? dayjs(this.editForm.get(['periodStartDate'])!.value, DATE_TIME_FORMAT)
  //       : undefined,
  //     periodEndDate: this.editForm.get(['periodEndDate'])!.value
  //       ? dayjs(this.editForm.get(['periodEndDate'])!.value, DATE_TIME_FORMAT)
  //       : undefined,
  //     executionDuration: this.editForm.get(['executionDuration'])!.value,
  //     createdBy: this.editForm.get(['createdBy'])!.value,
  //     paymentStatus: this.editForm.get(['paymentStatus'])!.value,
  //     spare1: this.editForm.get(['spare1'])!.value,
  //     spare2: this.editForm.get(['spare2'])!.value,
  //     spare3: this.editForm.get(['spare3'])!.value,
  //   };
  // }
}
