import { ICommission } from 'app/entities/commission/commission.model';
import { CommissioningPlanType } from '../enumerations/commissioning-plan-type.model';
import { PaymentResult } from './payment-result.model';

export interface ICommissionBulk {
  partnerMsisdn?: string | null;
  partnerProfile?: string | null;
  totalAmount?: number | null;
  commissioningPlanType?: CommissioningPlanType | null;
  commissions?: ICommission[] | null;
  paymentResult?: PaymentResult | null;
  isSelected?: boolean | null;
}

export class CommissionBulk implements ICommissionBulk {
  constructor(
    public partnerMsisdn?: string | null,
    public partnerProfile?: string | null,
    public totalAmount?: number | null,
    public commissioningPlanType?: CommissioningPlanType | null,
    public commissions?: ICommission[] | null,
    public paymentResult?: PaymentResult | null,
    public isSelected: boolean = true
  ) {}
}
