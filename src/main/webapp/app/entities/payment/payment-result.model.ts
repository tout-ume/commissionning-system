import { PaymentStatus } from '../enumerations/payment-status.model';

export interface IPaymentResult {
  status?: PaymentStatus;
  message?: string;
}

export class PaymentResult implements IPaymentResult {
  constructor(public status?: PaymentStatus, public message?: string) {}
}
