import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { getAuthorityMirrorIdentifier, IAuthorityMirror } from '../update/authority-mirror.model';
import { createRequestOption } from '../../../core/request/request-util';

export type EntityResponseType = HttpResponse<IAuthorityMirror>;
export type EntityArrayResponseType = HttpResponse<IAuthorityMirror[]>;

@Injectable({ providedIn: 'root' })
export class AuthorityMirrorService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/authority-mirrors');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(authorityMirror: IAuthorityMirror): Observable<EntityResponseType> {
    return this.http
      .post<IAuthorityMirror>(this.resourceUrl, authorityMirror, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => res));
  }

  update(authorityMirror: IAuthorityMirror): Observable<EntityResponseType> {
    return this.http
      .put<IAuthorityMirror>(`${this.resourceUrl}/${getAuthorityMirrorIdentifier(authorityMirror) as number}`, authorityMirror, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => res));
  }

  partialUpdate(authorityMirror: IAuthorityMirror): Observable<EntityResponseType> {
    return this.http
      .patch<IAuthorityMirror>(`${this.resourceUrl}/${getAuthorityMirrorIdentifier(authorityMirror) as number}`, authorityMirror, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => res));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IAuthorityMirror>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => res));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IAuthorityMirror[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => res));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addAuthorityMirrorToCollectionIfMissing(
    authorityMirrorCollection: IAuthorityMirror[],
    ...authorityMirrorsToCheck: (IAuthorityMirror | null | undefined)[]
  ): IAuthorityMirror[] {
    const authorityMirrors: IAuthorityMirror[] = authorityMirrorsToCheck.filter(isPresent);
    if (authorityMirrors.length > 0) {
      const authorityMirrorCollectionIdentifiers = authorityMirrorCollection.map(
        authorityMirrorItem => getAuthorityMirrorIdentifier(authorityMirrorItem)!
      );
      const authorityMirrorsToAdd = authorityMirrors.filter(authorityMirrorItem => {
        const authorityMirrorIdentifier = getAuthorityMirrorIdentifier(authorityMirrorItem);
        if (authorityMirrorIdentifier == null || authorityMirrorCollectionIdentifiers.includes(authorityMirrorIdentifier)) {
          return false;
        }
        authorityMirrorCollectionIdentifiers.push(authorityMirrorIdentifier);
        return true;
      });
      return [...authorityMirrorsToAdd, ...authorityMirrorCollection];
    }
    return authorityMirrorCollection;
  }
}
