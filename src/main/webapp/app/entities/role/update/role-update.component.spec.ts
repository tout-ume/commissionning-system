import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { RoleService } from '../service/role.service';
import { IRole, Role } from '../role.model';
import { IAuthorityMirror } from 'app/entities/authority-mirror/authority-mirror.model';
import { AuthorityMirrorService } from 'app/entities/authority-mirror/service/authority-mirror.service';

import { RoleUpdateComponent } from './role-update.component';

describe('Role Management Update Component', () => {
  let comp: RoleUpdateComponent;
  let fixture: ComponentFixture<RoleUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let roleService: RoleService;
  let authorityMirrorService: AuthorityMirrorService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [RoleUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(RoleUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(RoleUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    roleService = TestBed.inject(RoleService);
    authorityMirrorService = TestBed.inject(AuthorityMirrorService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call AuthorityMirror query and add missing value', () => {
      const role: IRole = { id: 456 };
      const authorities: IAuthorityMirror[] = [{ id: 29405 }];
      role.authorities = authorities;

      const authorityMirrorCollection: IAuthorityMirror[] = [{ id: 59361 }];
      jest.spyOn(authorityMirrorService, 'query').mockReturnValue(of(new HttpResponse({ body: authorityMirrorCollection })));
      const additionalAuthorityMirrors = [...authorities];
      const expectedCollection: IAuthorityMirror[] = [...additionalAuthorityMirrors, ...authorityMirrorCollection];
      jest.spyOn(authorityMirrorService, 'addAuthorityMirrorToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ role });
      comp.ngOnInit();

      expect(authorityMirrorService.query).toHaveBeenCalled();
      expect(authorityMirrorService.addAuthorityMirrorToCollectionIfMissing).toHaveBeenCalledWith(
        authorityMirrorCollection,
        ...additionalAuthorityMirrors
      );
      expect(comp.authorityMirrorsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const role: IRole = { id: 456 };
      const authorities: IAuthorityMirror = { id: 11256 };
      role.authorities = [authorities];

      activatedRoute.data = of({ role });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(role));
      expect(comp.authorityMirrorsSharedCollection).toContain(authorities);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Role>>();
      const role = { id: 123 };
      jest.spyOn(roleService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ role });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: role }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(roleService.update).toHaveBeenCalledWith(role);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Role>>();
      const role = new Role();
      jest.spyOn(roleService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ role });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: role }));
      saveSubject.complete();

      // THEN
      expect(roleService.create).toHaveBeenCalledWith(role);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Role>>();
      const role = { id: 123 };
      jest.spyOn(roleService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ role });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(roleService.update).toHaveBeenCalledWith(role);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackAuthorityMirrorById', () => {
      it('Should return tracked AuthorityMirror primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackAuthorityMirrorById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });

  describe('Getting selected relationships', () => {
    describe('getSelectedAuthorityMirror', () => {
      it('Should return option if no AuthorityMirror is selected', () => {
        const option = { id: 123 };
        const result = comp.getSelectedAuthorityMirror(option);
        expect(result === option).toEqual(true);
      });

      it('Should return selected AuthorityMirror for according option', () => {
        const option = { id: 123 };
        const selected = { id: 123 };
        const selected2 = { id: 456 };
        const result = comp.getSelectedAuthorityMirror(option, [selected2, selected]);
        expect(result === selected).toEqual(true);
        expect(result === selected2).toEqual(false);
        expect(result === option).toEqual(false);
      });

      it('Should return option if this AuthorityMirror is not selected', () => {
        const option = { id: 123 };
        const selected = { id: 456 };
        const result = comp.getSelectedAuthorityMirror(option, [selected]);
        expect(result === option).toEqual(true);
        expect(result === selected).toEqual(false);
      });
    });
  });
});
