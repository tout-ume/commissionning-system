import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IRole, Role } from '../role.model';
import { RoleService } from '../service/role.service';
import { IAuthorityMirror } from './authority-mirror.model';
import { AuthorityMirrorService } from '../service/authority-mirror.service';
import { AccountService } from '../../../core/auth/account.service';

@Component({
  selector: 'jhi-role-update',
  templateUrl: './role-update.component.html',
})
export class RoleUpdateComponent implements OnInit {
  isSaving = false;
  connectedUser: string | undefined;

  authorityMirrorsSharedCollection: IAuthorityMirror[] = [];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required, Validators.maxLength(50)]],
    description: [null, [Validators.maxLength(50)]],
    authorities: [null, [Validators.required]],
  });

  constructor(
    protected roleService: RoleService,
    protected authorityMirrorService: AuthorityMirrorService,
    protected activatedRoute: ActivatedRoute,
    protected accountService: AccountService,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ role }) => {
      if (role.id === undefined) {
        const today = dayjs().startOf('day');
        role.createdDate = today;
        role.lastModifiedDate = today;
      }

      this.updateForm(role);

      this.loadRelationshipsOptions();
    });
    this.accountService.getAuthenticationState().subscribe(account => {
      if (account) {
        this.connectedUser = account.login;
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const role = this.createFromForm();
    if (role.id !== undefined) {
      this.subscribeToSaveResponse(this.roleService.update(role));
    } else {
      this.subscribeToSaveResponse(this.roleService.create(role));
    }
  }

  trackAuthorityMirrorById(_index: number, item: IAuthorityMirror): number {
    return item.id!;
  }

  getSelectedAuthorityMirror(option: IAuthorityMirror, selectedVals?: IAuthorityMirror[]): IAuthorityMirror {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRole>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(role: IRole): void {
    this.editForm.patchValue({
      id: role.id,
      name: role.name,
      description: role.description,
      createdBy: role.createdBy,
      createdDate: role.createdDate ? role.createdDate.format(DATE_TIME_FORMAT) : null,
      lastModifiedBy: role.lastModifiedBy,
      lastModifiedDate: role.lastModifiedDate ? role.lastModifiedDate.format(DATE_TIME_FORMAT) : null,
      authorities: role.authorities,
    });

    this.authorityMirrorsSharedCollection = this.authorityMirrorService.addAuthorityMirrorToCollectionIfMissing(
      this.authorityMirrorsSharedCollection,
      ...(role.authorities ?? [])
    );
  }

  protected loadRelationshipsOptions(): void {
    this.authorityMirrorService
      .query()
      .pipe(map((res: HttpResponse<IAuthorityMirror[]>) => res.body ?? []))
      .pipe(
        map((authorityMirrors: IAuthorityMirror[]) =>
          this.authorityMirrorService.addAuthorityMirrorToCollectionIfMissing(
            authorityMirrors,
            ...(this.editForm.get('authorities')!.value ?? [])
          )
        )
      )
      .subscribe((authorityMirrors: IAuthorityMirror[]) => (this.authorityMirrorsSharedCollection = authorityMirrors));
  }

  protected createFromForm(): IRole {
    return {
      ...new Role(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      description: this.editForm.get(['description'])!.value,
      createdDate: dayjs(dayjs().startOf('second'), DATE_TIME_FORMAT),
      createdBy: this.connectedUser,
      authorities: this.editForm.get(['authorities'])!.value,
    };
  }
}
