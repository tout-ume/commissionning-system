export interface IAuthorityMirror {
  id?: number;
  name?: string;
}

export class AuthorityMirror implements IAuthorityMirror {
  constructor(public id?: number, public name?: string) {}
}

export function getAuthorityMirrorIdentifier(authorityMirror: IAuthorityMirror): number | undefined {
  return authorityMirror.id;
}
