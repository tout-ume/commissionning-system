import dayjs from 'dayjs/esm';
import { IAuthorityMirror } from './update/authority-mirror.model';

export interface IRole {
  id?: number;
  name?: string;
  description?: string | null;
  createdBy?: string;
  createdDate?: dayjs.Dayjs;
  lastModifiedBy?: string | null;
  lastModifiedDate?: dayjs.Dayjs | null;
  spare1?: string | null;
  spare2?: string | null;
  spare3?: string | null;
  authorities?: IAuthorityMirror[] | null;
}

export class Role implements IRole {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string | null,
    public createdBy?: string,
    public createdDate?: dayjs.Dayjs,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: dayjs.Dayjs | null,
    public spare1?: string | null,
    public spare2?: string | null,
    public spare3?: string | null,
    public authorities?: IAuthorityMirror[] | null
  ) {}
}

export function getRoleIdentifier(role: IRole): number | undefined {
  return role.id;
}
