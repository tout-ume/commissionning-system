import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPartnerProfile, getPartnerProfileIdentifier } from '../partner-profile.model';

export type EntityResponseType = HttpResponse<IPartnerProfile>;
export type EntityArrayResponseType = HttpResponse<IPartnerProfile[]>;

@Injectable({ providedIn: 'root' })
export class PartnerProfileService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/partner-profiles');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(partnerProfile: IPartnerProfile): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(partnerProfile);
    return this.http
      .post<IPartnerProfile>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(partnerProfile: IPartnerProfile): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(partnerProfile);
    return this.http
      .put<IPartnerProfile>(`${this.resourceUrl}/${getPartnerProfileIdentifier(partnerProfile) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(partnerProfile: IPartnerProfile): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(partnerProfile);
    return this.http
      .patch<IPartnerProfile>(`${this.resourceUrl}/${getPartnerProfileIdentifier(partnerProfile) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IPartnerProfile>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPartnerProfile[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPartnerProfileToCollectionIfMissing(
    partnerProfileCollection: IPartnerProfile[],
    ...partnerProfilesToCheck: (IPartnerProfile | null | undefined)[]
  ): IPartnerProfile[] {
    const partnerProfiles: IPartnerProfile[] = partnerProfilesToCheck.filter(isPresent);
    if (partnerProfiles.length > 0) {
      const partnerProfileCollectionIdentifiers = partnerProfileCollection.map(
        partnerProfileItem => getPartnerProfileIdentifier(partnerProfileItem)!
      );
      const partnerProfilesToAdd = partnerProfiles.filter(partnerProfileItem => {
        const partnerProfileIdentifier = getPartnerProfileIdentifier(partnerProfileItem);
        if (partnerProfileIdentifier == null || partnerProfileCollectionIdentifiers.includes(partnerProfileIdentifier)) {
          return false;
        }
        partnerProfileCollectionIdentifiers.push(partnerProfileIdentifier);
        return true;
      });
      return [...partnerProfilesToAdd, ...partnerProfileCollection];
    }
    return partnerProfileCollection;
  }

  protected convertDateFromClient(partnerProfile: IPartnerProfile): IPartnerProfile {
    return Object.assign({}, partnerProfile, {
      createdDate: partnerProfile.createdDate?.isValid() ? partnerProfile.createdDate.toJSON() : undefined,
      lastModifiedDate: partnerProfile.lastModifiedDate?.isValid() ? partnerProfile.lastModifiedDate.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdDate = res.body.createdDate ? dayjs(res.body.createdDate) : undefined;
      res.body.lastModifiedDate = res.body.lastModifiedDate ? dayjs(res.body.lastModifiedDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((partnerProfile: IPartnerProfile) => {
        partnerProfile.createdDate = partnerProfile.createdDate ? dayjs(partnerProfile.createdDate) : undefined;
        partnerProfile.lastModifiedDate = partnerProfile.lastModifiedDate ? dayjs(partnerProfile.lastModifiedDate) : undefined;
      });
    }
    return res;
  }
}
