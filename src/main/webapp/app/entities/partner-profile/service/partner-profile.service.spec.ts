import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IPartnerProfile, PartnerProfile } from '../partner-profile.model';

import { PartnerProfileService } from './partner-profile.service';

describe('PartnerProfile Service', () => {
  let service: PartnerProfileService;
  let httpMock: HttpTestingController;
  let elemDefault: IPartnerProfile;
  let expectedResult: IPartnerProfile | IPartnerProfile[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(PartnerProfileService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      code: 'AAAAAAA',
      name: 'AAAAAAA',
      description: 'AAAAAAA',
      level: 0,
      parentPartnerProfileId: 0,
      createdBy: 'AAAAAAA',
      createdDate: currentDate,
      lastModifiedBy: 'AAAAAAA',
      lastModifiedDate: currentDate,
      canHaveMultipleZones: false,
      zoneThreshold: 0,
      simBackupThreshold: 0,
      canDoProfileAssignment: false,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          createdDate: currentDate.format(DATE_TIME_FORMAT),
          lastModifiedDate: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a PartnerProfile', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          createdDate: currentDate.format(DATE_TIME_FORMAT),
          lastModifiedDate: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdDate: currentDate,
          lastModifiedDate: currentDate,
        },
        returnedFromService
      );

      service.create(new PartnerProfile()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a PartnerProfile', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          code: 'BBBBBB',
          name: 'BBBBBB',
          description: 'BBBBBB',
          level: 1,
          parentPartnerProfileId: 1,
          createdBy: 'BBBBBB',
          createdDate: currentDate.format(DATE_TIME_FORMAT),
          lastModifiedBy: 'BBBBBB',
          lastModifiedDate: currentDate.format(DATE_TIME_FORMAT),
          canHaveMultipleZones: true,
          zoneThreshold: 1,
          simBackupThreshold: 1,
          canDoProfileAssignment: true,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdDate: currentDate,
          lastModifiedDate: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a PartnerProfile', () => {
      const patchObject = Object.assign(
        {
          name: 'BBBBBB',
          description: 'BBBBBB',
          level: 1,
          parentPartnerProfileId: 1,
          createdBy: 'BBBBBB',
          createdDate: currentDate.format(DATE_TIME_FORMAT),
          lastModifiedBy: 'BBBBBB',
          canHaveMultipleZones: true,
          zoneThreshold: 1,
        },
        new PartnerProfile()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          createdDate: currentDate,
          lastModifiedDate: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of PartnerProfile', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          code: 'BBBBBB',
          name: 'BBBBBB',
          description: 'BBBBBB',
          level: 1,
          parentPartnerProfileId: 1,
          createdBy: 'BBBBBB',
          createdDate: currentDate.format(DATE_TIME_FORMAT),
          lastModifiedBy: 'BBBBBB',
          lastModifiedDate: currentDate.format(DATE_TIME_FORMAT),
          canHaveMultipleZones: true,
          zoneThreshold: 1,
          simBackupThreshold: 1,
          canDoProfileAssignment: true,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdDate: currentDate,
          lastModifiedDate: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a PartnerProfile', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addPartnerProfileToCollectionIfMissing', () => {
      it('should add a PartnerProfile to an empty array', () => {
        const partnerProfile: IPartnerProfile = { id: 123 };
        expectedResult = service.addPartnerProfileToCollectionIfMissing([], partnerProfile);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(partnerProfile);
      });

      it('should not add a PartnerProfile to an array that contains it', () => {
        const partnerProfile: IPartnerProfile = { id: 123 };
        const partnerProfileCollection: IPartnerProfile[] = [
          {
            ...partnerProfile,
          },
          { id: 456 },
        ];
        expectedResult = service.addPartnerProfileToCollectionIfMissing(partnerProfileCollection, partnerProfile);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a PartnerProfile to an array that doesn't contain it", () => {
        const partnerProfile: IPartnerProfile = { id: 123 };
        const partnerProfileCollection: IPartnerProfile[] = [{ id: 456 }];
        expectedResult = service.addPartnerProfileToCollectionIfMissing(partnerProfileCollection, partnerProfile);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(partnerProfile);
      });

      it('should add only unique PartnerProfile to an array', () => {
        const partnerProfileArray: IPartnerProfile[] = [{ id: 123 }, { id: 456 }, { id: 34553 }];
        const partnerProfileCollection: IPartnerProfile[] = [{ id: 123 }];
        expectedResult = service.addPartnerProfileToCollectionIfMissing(partnerProfileCollection, ...partnerProfileArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const partnerProfile: IPartnerProfile = { id: 123 };
        const partnerProfile2: IPartnerProfile = { id: 456 };
        expectedResult = service.addPartnerProfileToCollectionIfMissing([], partnerProfile, partnerProfile2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(partnerProfile);
        expect(expectedResult).toContain(partnerProfile2);
      });

      it('should accept null and undefined values', () => {
        const partnerProfile: IPartnerProfile = { id: 123 };
        expectedResult = service.addPartnerProfileToCollectionIfMissing([], null, partnerProfile, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(partnerProfile);
      });

      it('should return initial array if no PartnerProfile is added', () => {
        const partnerProfileCollection: IPartnerProfile[] = [{ id: 123 }];
        expectedResult = service.addPartnerProfileToCollectionIfMissing(partnerProfileCollection, undefined, null);
        expect(expectedResult).toEqual(partnerProfileCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
