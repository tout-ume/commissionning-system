import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { IPartnerProfile, PartnerProfile } from '../partner-profile.model';
import { PartnerProfileService } from '../service/partner-profile.service';

import { PartnerProfileRoutingResolveService } from './partner-profile-routing-resolve.service';

describe('PartnerProfile routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: PartnerProfileRoutingResolveService;
  let service: PartnerProfileService;
  let resultPartnerProfile: IPartnerProfile | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(PartnerProfileRoutingResolveService);
    service = TestBed.inject(PartnerProfileService);
    resultPartnerProfile = undefined;
  });

  describe('resolve', () => {
    it('should return IPartnerProfile returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPartnerProfile = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultPartnerProfile).toEqual({ id: 123 });
    });

    it('should return new IPartnerProfile if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPartnerProfile = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultPartnerProfile).toEqual(new PartnerProfile());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as PartnerProfile })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultPartnerProfile = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultPartnerProfile).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
