import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPartnerProfile, PartnerProfile } from '../partner-profile.model';
import { PartnerProfileService } from '../service/partner-profile.service';

@Injectable({ providedIn: 'root' })
export class PartnerProfileRoutingResolveService implements Resolve<IPartnerProfile> {
  constructor(protected service: PartnerProfileService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPartnerProfile> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((partnerProfile: HttpResponse<PartnerProfile>) => {
          if (partnerProfile.body) {
            return of(partnerProfile.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new PartnerProfile());
  }
}
