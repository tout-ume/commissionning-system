import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { PartnerProfileComponent } from '../list/partner-profile.component';
import { PartnerProfileDetailComponent } from '../detail/partner-profile-detail.component';
import { PartnerProfileUpdateComponent } from '../update/partner-profile-update.component';
import { PartnerProfileRoutingResolveService } from './partner-profile-routing-resolve.service';

const partnerProfileRoute: Routes = [
  {
    path: '',
    component: PartnerProfileComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PartnerProfileDetailComponent,
    resolve: {
      partnerProfile: PartnerProfileRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PartnerProfileUpdateComponent,
    resolve: {
      partnerProfile: PartnerProfileRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PartnerProfileUpdateComponent,
    resolve: {
      partnerProfile: PartnerProfileRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(partnerProfileRoute)],
  exports: [RouterModule],
})
export class PartnerProfileRoutingModule {}
