import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { PartnerProfileComponent } from './list/partner-profile.component';
import { PartnerProfileDetailComponent } from './detail/partner-profile-detail.component';
import { PartnerProfileUpdateComponent } from './update/partner-profile-update.component';
import { PartnerProfileDeleteDialogComponent } from './delete/partner-profile-delete-dialog.component';
import { PartnerProfileRoutingModule } from './route/partner-profile-routing.module';

@NgModule({
  imports: [SharedModule, PartnerProfileRoutingModule],
  declarations: [
    PartnerProfileComponent,
    PartnerProfileDetailComponent,
    PartnerProfileUpdateComponent,
    PartnerProfileDeleteDialogComponent,
  ],
  entryComponents: [PartnerProfileDeleteDialogComponent],
})
export class PartnerProfileModule {}
