import dayjs from 'dayjs/esm';

export interface IPartnerProfile {
  id?: number;
  code?: string;
  name?: string;
  description?: string | null;
  level?: number | null;
  parentPartnerProfileId?: number | null;
  createdBy?: string;
  createdDate?: dayjs.Dayjs | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: dayjs.Dayjs | null;
  canHaveMultipleZones?: boolean | null;
  zoneThreshold?: number | null;
  simBackupThreshold?: number | null;
  canDoProfileAssignment?: boolean | null;
}

export class PartnerProfile implements IPartnerProfile {
  constructor(
    public id?: number,
    public code?: string,
    public name?: string,
    public description?: string | null,
    public level?: number | null,
    public parentPartnerProfileId?: number | null,
    public createdBy?: string,
    public createdDate?: dayjs.Dayjs | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: dayjs.Dayjs | null,
    public canHaveMultipleZones?: boolean | null,
    public zoneThreshold?: number | null,
    public simBackupThreshold?: number | null,
    public canDoProfileAssignment?: boolean | null
  ) {
    this.canHaveMultipleZones = this.canHaveMultipleZones ?? false;
    this.canDoProfileAssignment = this.canDoProfileAssignment ?? false;
  }
}

export function getPartnerProfileIdentifier(partnerProfile: IPartnerProfile): number | undefined {
  return partnerProfile.id;
}
