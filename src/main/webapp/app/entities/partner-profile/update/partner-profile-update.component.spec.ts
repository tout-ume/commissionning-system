import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { PartnerProfileService } from '../service/partner-profile.service';
import { IPartnerProfile, PartnerProfile } from '../partner-profile.model';

import { PartnerProfileUpdateComponent } from './partner-profile-update.component';

describe('PartnerProfile Management Update Component', () => {
  let comp: PartnerProfileUpdateComponent;
  let fixture: ComponentFixture<PartnerProfileUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let partnerProfileService: PartnerProfileService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [PartnerProfileUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(PartnerProfileUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PartnerProfileUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    partnerProfileService = TestBed.inject(PartnerProfileService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const partnerProfile: IPartnerProfile = { id: 456 };

      activatedRoute.data = of({ partnerProfile });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(partnerProfile));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<PartnerProfile>>();
      const partnerProfile = { id: 123 };
      jest.spyOn(partnerProfileService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ partnerProfile });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: partnerProfile }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(partnerProfileService.update).toHaveBeenCalledWith(partnerProfile);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<PartnerProfile>>();
      const partnerProfile = new PartnerProfile();
      jest.spyOn(partnerProfileService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ partnerProfile });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: partnerProfile }));
      saveSubject.complete();

      // THEN
      expect(partnerProfileService.create).toHaveBeenCalledWith(partnerProfile);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<PartnerProfile>>();
      const partnerProfile = { id: 123 };
      jest.spyOn(partnerProfileService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ partnerProfile });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(partnerProfileService.update).toHaveBeenCalledWith(partnerProfile);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
