import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IPartnerProfile, PartnerProfile } from '../partner-profile.model';
import { PartnerProfileService } from '../service/partner-profile.service';

@Component({
  selector: 'jhi-partner-profile-update',
  templateUrl: './partner-profile-update.component.html',
})
export class PartnerProfileUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    code: [null, [Validators.required]],
    name: [null, [Validators.required]],
    description: [],
    level: [],
    parentPartnerProfileId: [],
    createdBy: [null, [Validators.required, Validators.maxLength(50)]],
    createdDate: [],
    lastModifiedBy: [null, [Validators.maxLength(50)]],
    lastModifiedDate: [],
    canHaveMultipleZones: [],
    zoneThreshold: [],
    simBackupThreshold: [],
    canDoProfileAssignment: [],
  });

  constructor(
    protected partnerProfileService: PartnerProfileService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ partnerProfile }) => {
      if (partnerProfile.id === undefined) {
        const today = dayjs().startOf('day');
        partnerProfile.createdDate = today;
        partnerProfile.lastModifiedDate = today;
      }

      this.updateForm(partnerProfile);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const partnerProfile = this.createFromForm();
    if (partnerProfile.id !== undefined) {
      this.subscribeToSaveResponse(this.partnerProfileService.update(partnerProfile));
    } else {
      this.subscribeToSaveResponse(this.partnerProfileService.create(partnerProfile));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPartnerProfile>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(partnerProfile: IPartnerProfile): void {
    this.editForm.patchValue({
      id: partnerProfile.id,
      code: partnerProfile.code,
      name: partnerProfile.name,
      description: partnerProfile.description,
      level: partnerProfile.level,
      parentPartnerProfileId: partnerProfile.parentPartnerProfileId,
      createdBy: partnerProfile.createdBy,
      createdDate: partnerProfile.createdDate ? partnerProfile.createdDate.format(DATE_TIME_FORMAT) : null,
      lastModifiedBy: partnerProfile.lastModifiedBy,
      lastModifiedDate: partnerProfile.lastModifiedDate ? partnerProfile.lastModifiedDate.format(DATE_TIME_FORMAT) : null,
      canHaveMultipleZones: partnerProfile.canHaveMultipleZones,
      zoneThreshold: partnerProfile.zoneThreshold,
      simBackupThreshold: partnerProfile.simBackupThreshold,
      canDoProfileAssignment: partnerProfile.canDoProfileAssignment,
    });
  }

  protected createFromForm(): IPartnerProfile {
    return {
      ...new PartnerProfile(),
      id: this.editForm.get(['id'])!.value,
      code: this.editForm.get(['code'])!.value,
      name: this.editForm.get(['name'])!.value,
      description: this.editForm.get(['description'])!.value,
      level: this.editForm.get(['level'])!.value,
      parentPartnerProfileId: this.editForm.get(['parentPartnerProfileId'])!.value,
      createdBy: this.editForm.get(['createdBy'])!.value,
      createdDate: this.editForm.get(['createdDate'])!.value
        ? dayjs(this.editForm.get(['createdDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      lastModifiedBy: this.editForm.get(['lastModifiedBy'])!.value,
      lastModifiedDate: this.editForm.get(['lastModifiedDate'])!.value
        ? dayjs(this.editForm.get(['lastModifiedDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      canHaveMultipleZones: this.editForm.get(['canHaveMultipleZones'])!.value,
      zoneThreshold: this.editForm.get(['zoneThreshold'])!.value,
      simBackupThreshold: this.editForm.get(['simBackupThreshold'])!.value,
      canDoProfileAssignment: this.editForm.get(['canDoProfileAssignment'])!.value,
    };
  }
}
