import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IPartnerProfile } from '../partner-profile.model';
import { PartnerProfileService } from '../service/partner-profile.service';

@Component({
  templateUrl: './partner-profile-delete-dialog.component.html',
})
export class PartnerProfileDeleteDialogComponent {
  partnerProfile?: IPartnerProfile;

  constructor(protected partnerProfileService: PartnerProfileService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.partnerProfileService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
