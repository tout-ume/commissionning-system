import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PartnerProfileDetailComponent } from './partner-profile-detail.component';

describe('PartnerProfile Management Detail Component', () => {
  let comp: PartnerProfileDetailComponent;
  let fixture: ComponentFixture<PartnerProfileDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PartnerProfileDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ partnerProfile: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(PartnerProfileDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(PartnerProfileDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load partnerProfile on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.partnerProfile).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
