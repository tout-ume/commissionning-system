import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPartnerProfile } from '../partner-profile.model';

@Component({
  selector: 'jhi-partner-profile-detail',
  templateUrl: './partner-profile-detail.component.html',
})
export class PartnerProfileDetailComponent implements OnInit {
  partnerProfile: IPartnerProfile | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ partnerProfile }) => {
      this.partnerProfile = partnerProfile;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
