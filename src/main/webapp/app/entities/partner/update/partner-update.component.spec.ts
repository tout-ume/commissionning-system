import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { PartnerService } from '../service/partner.service';
import { IPartner, Partner } from '../partner.model';
import { IZone } from 'app/entities/zone/zone.model';
import { ZoneService } from 'app/entities/zone/service/zone.service';

import { PartnerUpdateComponent } from './partner-update.component';

describe('Partner Management Update Component', () => {
  let comp: PartnerUpdateComponent;
  let fixture: ComponentFixture<PartnerUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let partnerService: PartnerService;
  let zoneService: ZoneService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [PartnerUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(PartnerUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PartnerUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    partnerService = TestBed.inject(PartnerService);
    zoneService = TestBed.inject(ZoneService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Partner query and add missing value', () => {
      const partner: IPartner = { id: 456 };
      const partner: IPartner = { id: 26851 };
      partner.partner = partner;

      const partnerCollection: IPartner[] = [{ id: 96813 }];
      jest.spyOn(partnerService, 'query').mockReturnValue(of(new HttpResponse({ body: partnerCollection })));
      const additionalPartners = [partner];
      const expectedCollection: IPartner[] = [...additionalPartners, ...partnerCollection];
      jest.spyOn(partnerService, 'addPartnerToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ partner });
      comp.ngOnInit();

      expect(partnerService.query).toHaveBeenCalled();
      expect(partnerService.addPartnerToCollectionIfMissing).toHaveBeenCalledWith(partnerCollection, ...additionalPartners);
      expect(comp.partnersSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Zone query and add missing value', () => {
      const partner: IPartner = { id: 456 };
      const zones: IZone[] = [{ id: 76179 }];
      partner.zones = zones;

      const zoneCollection: IZone[] = [{ id: 13320 }];
      jest.spyOn(zoneService, 'query').mockReturnValue(of(new HttpResponse({ body: zoneCollection })));
      const additionalZones = [...zones];
      const expectedCollection: IZone[] = [...additionalZones, ...zoneCollection];
      jest.spyOn(zoneService, 'addZoneToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ partner });
      comp.ngOnInit();

      expect(zoneService.query).toHaveBeenCalled();
      expect(zoneService.addZoneToCollectionIfMissing).toHaveBeenCalledWith(zoneCollection, ...additionalZones);
      expect(comp.zonesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const partner: IPartner = { id: 456 };
      const partner: IPartner = { id: 87236 };
      partner.partner = partner;
      const zones: IZone = { id: 92022 };
      partner.zones = [zones];

      activatedRoute.data = of({ partner });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(partner));
      expect(comp.partnersSharedCollection).toContain(partner);
      expect(comp.zonesSharedCollection).toContain(zones);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Partner>>();
      const partner = { id: 123 };
      jest.spyOn(partnerService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ partner });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: partner }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(partnerService.update).toHaveBeenCalledWith(partner);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Partner>>();
      const partner = new Partner();
      jest.spyOn(partnerService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ partner });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: partner }));
      saveSubject.complete();

      // THEN
      expect(partnerService.create).toHaveBeenCalledWith(partner);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Partner>>();
      const partner = { id: 123 };
      jest.spyOn(partnerService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ partner });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(partnerService.update).toHaveBeenCalledWith(partner);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackPartnerById', () => {
      it('Should return tracked Partner primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackPartnerById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackZoneById', () => {
      it('Should return tracked Zone primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackZoneById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });

  describe('Getting selected relationships', () => {
    describe('getSelectedZone', () => {
      it('Should return option if no Zone is selected', () => {
        const option = { id: 123 };
        const result = comp.getSelectedZone(option);
        expect(result === option).toEqual(true);
      });

      it('Should return selected Zone for according option', () => {
        const option = { id: 123 };
        const selected = { id: 123 };
        const selected2 = { id: 456 };
        const result = comp.getSelectedZone(option, [selected2, selected]);
        expect(result === selected).toEqual(true);
        expect(result === selected2).toEqual(false);
        expect(result === option).toEqual(false);
      });

      it('Should return option if this Zone is not selected', () => {
        const option = { id: 123 };
        const selected = { id: 456 };
        const result = comp.getSelectedZone(option, [selected]);
        expect(result === option).toEqual(true);
        expect(result === selected).toEqual(false);
      });
    });
  });
});
