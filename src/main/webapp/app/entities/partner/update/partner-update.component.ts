import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IPartner, Partner } from '../partner.model';
import { PartnerService } from '../service/partner.service';
import { IZone } from 'app/entities/zone/zone.model';
import { ZoneService } from 'app/entities/zone/service/zone.service';

@Component({
  selector: 'jhi-partner-update',
  templateUrl: './partner-update.component.html',
})
export class PartnerUpdateComponent implements OnInit {
  isSaving = false;

  partnersSharedCollection: IPartner[] = [];
  zonesSharedCollection: IZone[] = [];

  editForm = this.fb.group({
    id: [],
    msisdn: [null, [Validators.required]],
    name: [],
    surname: [],
    state: [],
    partnerProfileId: [],
    parentId: [],
    canResetPin: [],
    createdBy: [null, [Validators.required]],
    createdDate: [],
    lastModifiedBy: [],
    lastModifiedDate: [],
    lastTransactionDate: [],
    zones: [],
    partner: [],
  });

  constructor(
    protected partnerService: PartnerService,
    protected zoneService: ZoneService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ partner }) => {
      if (partner.id === undefined) {
        const today = dayjs().startOf('day');
        partner.createdDate = today;
        partner.lastModifiedDate = today;
        partner.lastTransactionDate = today;
      }

      this.updateForm(partner);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const partner = this.createFromForm();
    if (partner.id !== undefined) {
      this.subscribeToSaveResponse(this.partnerService.update(partner));
    } else {
      this.subscribeToSaveResponse(this.partnerService.create(partner));
    }
  }

  trackPartnerById(_index: number, item: IPartner): number {
    return item.id!;
  }

  trackZoneById(_index: number, item: IZone): number {
    return item.id!;
  }

  getSelectedZone(option: IZone, selectedVals?: IZone[]): IZone {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPartner>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(partner: IPartner): void {
    this.editForm.patchValue({
      id: partner.id,
      msisdn: partner.msisdn,
      name: partner.name,
      surname: partner.surname,
      state: partner.state,
      partnerProfileId: partner.partnerProfileId,
      parentId: partner.parentId,
      canResetPin: partner.canResetPin,
      createdBy: partner.createdBy,
      createdDate: partner.createdDate ? partner.createdDate.format(DATE_TIME_FORMAT) : null,
      lastModifiedBy: partner.lastModifiedBy,
      lastModifiedDate: partner.lastModifiedDate ? partner.lastModifiedDate.format(DATE_TIME_FORMAT) : null,
      lastTransactionDate: partner.lastTransactionDate ? partner.lastTransactionDate.format(DATE_TIME_FORMAT) : null,
      zones: partner.zones,
      partner: partner.partner,
    });

    this.partnersSharedCollection = this.partnerService.addPartnerToCollectionIfMissing(this.partnersSharedCollection, partner.partner);
    this.zonesSharedCollection = this.zoneService.addZoneToCollectionIfMissing(this.zonesSharedCollection, ...(partner.zones ?? []));
  }

  protected loadRelationshipsOptions(): void {
    this.partnerService
      .query()
      .pipe(map((res: HttpResponse<IPartner[]>) => res.body ?? []))
      .pipe(
        map((partners: IPartner[]) => this.partnerService.addPartnerToCollectionIfMissing(partners, this.editForm.get('partner')!.value))
      )
      .subscribe((partners: IPartner[]) => (this.partnersSharedCollection = partners));

    this.zoneService
      .query()
      .pipe(map((res: HttpResponse<IZone[]>) => res.body ?? []))
      .pipe(map((zones: IZone[]) => this.zoneService.addZoneToCollectionIfMissing(zones, ...(this.editForm.get('zones')!.value ?? []))))
      .subscribe((zones: IZone[]) => (this.zonesSharedCollection = zones));
  }

  protected createFromForm(): IPartner {
    return {
      ...new Partner(),
      id: this.editForm.get(['id'])!.value,
      msisdn: this.editForm.get(['msisdn'])!.value,
      name: this.editForm.get(['name'])!.value,
      surname: this.editForm.get(['surname'])!.value,
      state: this.editForm.get(['state'])!.value,
      partnerProfileId: this.editForm.get(['partnerProfileId'])!.value,
      parentId: this.editForm.get(['parentId'])!.value,
      canResetPin: this.editForm.get(['canResetPin'])!.value,
      createdBy: this.editForm.get(['createdBy'])!.value,
      createdDate: this.editForm.get(['createdDate'])!.value
        ? dayjs(this.editForm.get(['createdDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      lastModifiedBy: this.editForm.get(['lastModifiedBy'])!.value,
      lastModifiedDate: this.editForm.get(['lastModifiedDate'])!.value
        ? dayjs(this.editForm.get(['lastModifiedDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      lastTransactionDate: this.editForm.get(['lastTransactionDate'])!.value
        ? dayjs(this.editForm.get(['lastTransactionDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      zones: this.editForm.get(['zones'])!.value,
      partner: this.editForm.get(['partner'])!.value,
    };
  }
}
