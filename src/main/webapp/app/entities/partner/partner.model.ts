import dayjs from 'dayjs/esm';
import { IBlacklisted } from 'app/entities/blacklisted/blacklisted.model';
import { IZone } from 'app/entities/zone/zone.model';

export interface IPartner {
  id?: number;
  msisdn?: string;
  name?: string | null;
  surname?: string | null;
  state?: string | null;
  partnerProfileId?: number | null;
  parentId?: number | null;
  canResetPin?: boolean | null;
  createdBy?: string;
  createdDate?: dayjs.Dayjs | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: dayjs.Dayjs | null;
  lastTransactionDate?: dayjs.Dayjs | null;
  blacklisteds?: IBlacklisted[] | null;
  children?: IPartner[] | null;
  zones?: IZone[] | null;
  partner?: IPartner | null;
}

export class Partner implements IPartner {
  constructor(
    public id?: number,
    public msisdn?: string,
    public name?: string | null,
    public surname?: string | null,
    public state?: string | null,
    public partnerProfileId?: number | null,
    public parentId?: number | null,
    public canResetPin?: boolean | null,
    public createdBy?: string,
    public createdDate?: dayjs.Dayjs | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: dayjs.Dayjs | null,
    public lastTransactionDate?: dayjs.Dayjs | null,
    public blacklisteds?: IBlacklisted[] | null,
    public children?: IPartner[] | null,
    public zones?: IZone[] | null,
    public partner?: IPartner | null
  ) {
    this.canResetPin = this.canResetPin ?? false;
  }
}

export function getPartnerIdentifier(partner: IPartner): number | undefined {
  return partner.id;
}
