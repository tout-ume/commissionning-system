import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TransactionOperationDetailComponent } from './transaction-operation-detail.component';

describe('TransactionOperation Management Detail Component', () => {
  let comp: TransactionOperationDetailComponent;
  let fixture: ComponentFixture<TransactionOperationDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TransactionOperationDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ transactionOperation: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(TransactionOperationDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(TransactionOperationDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load transactionOperation on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.transactionOperation).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
