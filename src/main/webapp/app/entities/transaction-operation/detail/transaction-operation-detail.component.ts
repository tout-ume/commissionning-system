import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ITransactionOperation } from '../transaction-operation.model';

@Component({
  selector: 'jhi-transaction-operation-detail',
  templateUrl: './transaction-operation-detail.component.html',
})
export class TransactionOperationDetailComponent implements OnInit {
  transactionOperation: ITransactionOperation | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ transactionOperation }) => {
      this.transactionOperation = transactionOperation;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
