import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ITransactionOperation, getTransactionOperationIdentifier } from '../transaction-operation.model';

export type EntityResponseType = HttpResponse<ITransactionOperation>;
export type EntityArrayResponseType = HttpResponse<ITransactionOperation[]>;

@Injectable({ providedIn: 'root' })
export class TransactionOperationService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/transaction-operations');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(transactionOperation: ITransactionOperation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(transactionOperation);
    return this.http
      .post<ITransactionOperation>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(transactionOperation: ITransactionOperation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(transactionOperation);
    return this.http
      .put<ITransactionOperation>(`${this.resourceUrl}/${getTransactionOperationIdentifier(transactionOperation) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(transactionOperation: ITransactionOperation): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(transactionOperation);
    return this.http
      .patch<ITransactionOperation>(`${this.resourceUrl}/${getTransactionOperationIdentifier(transactionOperation) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITransactionOperation>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITransactionOperation[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addTransactionOperationToCollectionIfMissing(
    transactionOperationCollection: ITransactionOperation[],
    ...transactionOperationsToCheck: (ITransactionOperation | null | undefined)[]
  ): ITransactionOperation[] {
    const transactionOperations: ITransactionOperation[] = transactionOperationsToCheck.filter(isPresent);
    if (transactionOperations.length > 0) {
      const transactionOperationCollectionIdentifiers = transactionOperationCollection.map(
        transactionOperationItem => getTransactionOperationIdentifier(transactionOperationItem)!
      );
      const transactionOperationsToAdd = transactionOperations.filter(transactionOperationItem => {
        const transactionOperationIdentifier = getTransactionOperationIdentifier(transactionOperationItem);
        if (transactionOperationIdentifier == null || transactionOperationCollectionIdentifiers.includes(transactionOperationIdentifier)) {
          return false;
        }
        transactionOperationCollectionIdentifiers.push(transactionOperationIdentifier);
        return true;
      });
      return [...transactionOperationsToAdd, ...transactionOperationCollection];
    }
    return transactionOperationCollection;
  }

  protected convertDateFromClient(transactionOperation: ITransactionOperation): ITransactionOperation {
    return Object.assign({}, transactionOperation, {
      createdAt: transactionOperation.createdAt?.isValid() ? transactionOperation.createdAt.toJSON() : undefined,
      operationShortDate: transactionOperation.operationShortDate?.isValid()
        ? transactionOperation.operationShortDate.format(DATE_FORMAT)
        : undefined,
      operationDate: transactionOperation.operationDate?.isValid() ? transactionOperation.operationDate.toJSON() : undefined,
      taggedAt: transactionOperation.taggedAt?.isValid() ? transactionOperation.taggedAt.toJSON() : undefined,
      falsePositiveDetectedAt: transactionOperation.falsePositiveDetectedAt?.isValid()
        ? transactionOperation.falsePositiveDetectedAt.toJSON()
        : undefined,
      canceledAt: transactionOperation.canceledAt?.isValid() ? transactionOperation.canceledAt.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdAt = res.body.createdAt ? dayjs(res.body.createdAt) : undefined;
      res.body.operationShortDate = res.body.operationShortDate ? dayjs(res.body.operationShortDate) : undefined;
      res.body.operationDate = res.body.operationDate ? dayjs(res.body.operationDate) : undefined;
      res.body.taggedAt = res.body.taggedAt ? dayjs(res.body.taggedAt) : undefined;
      res.body.falsePositiveDetectedAt = res.body.falsePositiveDetectedAt ? dayjs(res.body.falsePositiveDetectedAt) : undefined;
      res.body.canceledAt = res.body.canceledAt ? dayjs(res.body.canceledAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((transactionOperation: ITransactionOperation) => {
        transactionOperation.createdAt = transactionOperation.createdAt ? dayjs(transactionOperation.createdAt) : undefined;
        transactionOperation.operationShortDate = transactionOperation.operationShortDate
          ? dayjs(transactionOperation.operationShortDate)
          : undefined;
        transactionOperation.operationDate = transactionOperation.operationDate ? dayjs(transactionOperation.operationDate) : undefined;
        transactionOperation.taggedAt = transactionOperation.taggedAt ? dayjs(transactionOperation.taggedAt) : undefined;
        transactionOperation.falsePositiveDetectedAt = transactionOperation.falsePositiveDetectedAt
          ? dayjs(transactionOperation.falsePositiveDetectedAt)
          : undefined;
        transactionOperation.canceledAt = transactionOperation.canceledAt ? dayjs(transactionOperation.canceledAt) : undefined;
      });
    }
    return res;
  }
}
