import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT, DATE_TIME_FORMAT } from 'app/config/input.constants';
import { ITransactionOperation, TransactionOperation } from '../transaction-operation.model';

import { TransactionOperationService } from './transaction-operation.service';

describe('TransactionOperation Service', () => {
  let service: TransactionOperationService;
  let httpMock: HttpTestingController;
  let elemDefault: ITransactionOperation;
  let expectedResult: ITransactionOperation | ITransactionOperation[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(TransactionOperationService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      amount: 0,
      subsMsisdn: 'AAAAAAA',
      agentMsisdn: 'AAAAAAA',
      typeTransaction: 'AAAAAAA',
      createdAt: currentDate,
      transactionStatus: 'AAAAAAA',
      senderZone: 'AAAAAAA',
      senderProfile: 'AAAAAAA',
      codeTerritory: 'AAAAAAA',
      subType: 'AAAAAAA',
      operationShortDate: currentDate,
      operationDate: currentDate,
      isFraud: false,
      taggedAt: currentDate,
      fraudSource: 'AAAAAAA',
      comment: 'AAAAAAA',
      falsePositiveDetectedAt: currentDate,
      tid: 'AAAAAAA',
      parentMsisdn: 'AAAAAAA',
      fctDt: 'AAAAAAA',
      parentId: 'AAAAAAA',
      canceledAt: currentDate,
      canceledId: 'AAAAAAA',
      productId: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          operationShortDate: currentDate.format(DATE_FORMAT),
          operationDate: currentDate.format(DATE_TIME_FORMAT),
          taggedAt: currentDate.format(DATE_TIME_FORMAT),
          falsePositiveDetectedAt: currentDate.format(DATE_TIME_FORMAT),
          canceledAt: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a TransactionOperation', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          operationShortDate: currentDate.format(DATE_FORMAT),
          operationDate: currentDate.format(DATE_TIME_FORMAT),
          taggedAt: currentDate.format(DATE_TIME_FORMAT),
          falsePositiveDetectedAt: currentDate.format(DATE_TIME_FORMAT),
          canceledAt: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdAt: currentDate,
          operationShortDate: currentDate,
          operationDate: currentDate,
          taggedAt: currentDate,
          falsePositiveDetectedAt: currentDate,
          canceledAt: currentDate,
        },
        returnedFromService
      );

      service.create(new TransactionOperation()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a TransactionOperation', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          amount: 1,
          subsMsisdn: 'BBBBBB',
          agentMsisdn: 'BBBBBB',
          typeTransaction: 'BBBBBB',
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          transactionStatus: 'BBBBBB',
          senderZone: 'BBBBBB',
          senderProfile: 'BBBBBB',
          codeTerritory: 'BBBBBB',
          subType: 'BBBBBB',
          operationShortDate: currentDate.format(DATE_FORMAT),
          operationDate: currentDate.format(DATE_TIME_FORMAT),
          isFraud: true,
          taggedAt: currentDate.format(DATE_TIME_FORMAT),
          fraudSource: 'BBBBBB',
          comment: 'BBBBBB',
          falsePositiveDetectedAt: currentDate.format(DATE_TIME_FORMAT),
          tid: 'BBBBBB',
          parentMsisdn: 'BBBBBB',
          fctDt: 'BBBBBB',
          parentId: 'BBBBBB',
          canceledAt: currentDate.format(DATE_TIME_FORMAT),
          canceledId: 'BBBBBB',
          productId: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdAt: currentDate,
          operationShortDate: currentDate,
          operationDate: currentDate,
          taggedAt: currentDate,
          falsePositiveDetectedAt: currentDate,
          canceledAt: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a TransactionOperation', () => {
      const patchObject = Object.assign(
        {
          amount: 1,
          subsMsisdn: 'BBBBBB',
          typeTransaction: 'BBBBBB',
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          transactionStatus: 'BBBBBB',
          senderProfile: 'BBBBBB',
          codeTerritory: 'BBBBBB',
          operationDate: currentDate.format(DATE_TIME_FORMAT),
          taggedAt: currentDate.format(DATE_TIME_FORMAT),
          tid: 'BBBBBB',
          parentId: 'BBBBBB',
          canceledAt: currentDate.format(DATE_TIME_FORMAT),
          canceledId: 'BBBBBB',
        },
        new TransactionOperation()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          createdAt: currentDate,
          operationShortDate: currentDate,
          operationDate: currentDate,
          taggedAt: currentDate,
          falsePositiveDetectedAt: currentDate,
          canceledAt: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of TransactionOperation', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          amount: 1,
          subsMsisdn: 'BBBBBB',
          agentMsisdn: 'BBBBBB',
          typeTransaction: 'BBBBBB',
          createdAt: currentDate.format(DATE_TIME_FORMAT),
          transactionStatus: 'BBBBBB',
          senderZone: 'BBBBBB',
          senderProfile: 'BBBBBB',
          codeTerritory: 'BBBBBB',
          subType: 'BBBBBB',
          operationShortDate: currentDate.format(DATE_FORMAT),
          operationDate: currentDate.format(DATE_TIME_FORMAT),
          isFraud: true,
          taggedAt: currentDate.format(DATE_TIME_FORMAT),
          fraudSource: 'BBBBBB',
          comment: 'BBBBBB',
          falsePositiveDetectedAt: currentDate.format(DATE_TIME_FORMAT),
          tid: 'BBBBBB',
          parentMsisdn: 'BBBBBB',
          fctDt: 'BBBBBB',
          parentId: 'BBBBBB',
          canceledAt: currentDate.format(DATE_TIME_FORMAT),
          canceledId: 'BBBBBB',
          productId: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdAt: currentDate,
          operationShortDate: currentDate,
          operationDate: currentDate,
          taggedAt: currentDate,
          falsePositiveDetectedAt: currentDate,
          canceledAt: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a TransactionOperation', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addTransactionOperationToCollectionIfMissing', () => {
      it('should add a TransactionOperation to an empty array', () => {
        const transactionOperation: ITransactionOperation = { id: 123 };
        expectedResult = service.addTransactionOperationToCollectionIfMissing([], transactionOperation);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(transactionOperation);
      });

      it('should not add a TransactionOperation to an array that contains it', () => {
        const transactionOperation: ITransactionOperation = { id: 123 };
        const transactionOperationCollection: ITransactionOperation[] = [
          {
            ...transactionOperation,
          },
          { id: 456 },
        ];
        expectedResult = service.addTransactionOperationToCollectionIfMissing(transactionOperationCollection, transactionOperation);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a TransactionOperation to an array that doesn't contain it", () => {
        const transactionOperation: ITransactionOperation = { id: 123 };
        const transactionOperationCollection: ITransactionOperation[] = [{ id: 456 }];
        expectedResult = service.addTransactionOperationToCollectionIfMissing(transactionOperationCollection, transactionOperation);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(transactionOperation);
      });

      it('should add only unique TransactionOperation to an array', () => {
        const transactionOperationArray: ITransactionOperation[] = [{ id: 123 }, { id: 456 }, { id: 28516 }];
        const transactionOperationCollection: ITransactionOperation[] = [{ id: 123 }];
        expectedResult = service.addTransactionOperationToCollectionIfMissing(transactionOperationCollection, ...transactionOperationArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const transactionOperation: ITransactionOperation = { id: 123 };
        const transactionOperation2: ITransactionOperation = { id: 456 };
        expectedResult = service.addTransactionOperationToCollectionIfMissing([], transactionOperation, transactionOperation2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(transactionOperation);
        expect(expectedResult).toContain(transactionOperation2);
      });

      it('should accept null and undefined values', () => {
        const transactionOperation: ITransactionOperation = { id: 123 };
        expectedResult = service.addTransactionOperationToCollectionIfMissing([], null, transactionOperation, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(transactionOperation);
      });

      it('should return initial array if no TransactionOperation is added', () => {
        const transactionOperationCollection: ITransactionOperation[] = [{ id: 123 }];
        expectedResult = service.addTransactionOperationToCollectionIfMissing(transactionOperationCollection, undefined, null);
        expect(expectedResult).toEqual(transactionOperationCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
