import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { TransactionOperationComponent } from './list/transaction-operation.component';
import { TransactionOperationDetailComponent } from './detail/transaction-operation-detail.component';
import { TransactionOperationUpdateComponent } from './update/transaction-operation-update.component';
import { TransactionOperationDeleteDialogComponent } from './delete/transaction-operation-delete-dialog.component';
import { TransactionOperationRoutingModule } from './route/transaction-operation-routing.module';

@NgModule({
  imports: [SharedModule, TransactionOperationRoutingModule],
  declarations: [
    TransactionOperationComponent,
    TransactionOperationDetailComponent,
    TransactionOperationUpdateComponent,
    TransactionOperationDeleteDialogComponent,
  ],
  entryComponents: [TransactionOperationDeleteDialogComponent],
})
export class TransactionOperationModule {}
