import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { ITransactionOperation, TransactionOperation } from '../transaction-operation.model';
import { TransactionOperationService } from '../service/transaction-operation.service';

@Component({
  selector: 'jhi-transaction-operation-update',
  templateUrl: './transaction-operation-update.component.html',
})
export class TransactionOperationUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    amount: [],
    subsMsisdn: [],
    agentMsisdn: [],
    typeTransaction: [],
    createdAt: [],
    transactionStatus: [],
    senderZone: [],
    senderProfile: [],
    codeTerritory: [],
    subType: [],
    operationShortDate: [],
    operationDate: [],
    isFraud: [],
    taggedAt: [],
    fraudSource: [],
    comment: [],
    falsePositiveDetectedAt: [],
    tid: [],
    parentMsisdn: [],
    fctDt: [],
    parentId: [],
    canceledAt: [],
    canceledId: [],
    productId: [],
  });

  constructor(
    protected transactionOperationService: TransactionOperationService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ transactionOperation }) => {
      if (transactionOperation.id === undefined) {
        const today = dayjs().startOf('day');
        transactionOperation.createdAt = today;
        transactionOperation.operationDate = today;
        transactionOperation.taggedAt = today;
        transactionOperation.falsePositiveDetectedAt = today;
        transactionOperation.canceledAt = today;
      }

      this.updateForm(transactionOperation);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const transactionOperation = this.createFromForm();
    if (transactionOperation.id !== undefined) {
      this.subscribeToSaveResponse(this.transactionOperationService.update(transactionOperation));
    } else {
      this.subscribeToSaveResponse(this.transactionOperationService.create(transactionOperation));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITransactionOperation>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(transactionOperation: ITransactionOperation): void {
    this.editForm.patchValue({
      id: transactionOperation.id,
      amount: transactionOperation.amount,
      subsMsisdn: transactionOperation.subsMsisdn,
      agentMsisdn: transactionOperation.agentMsisdn,
      typeTransaction: transactionOperation.typeTransaction,
      createdAt: transactionOperation.createdAt ? transactionOperation.createdAt.format(DATE_TIME_FORMAT) : null,
      transactionStatus: transactionOperation.transactionStatus,
      senderZone: transactionOperation.senderZone,
      senderProfile: transactionOperation.senderProfile,
      codeTerritory: transactionOperation.codeTerritory,
      subType: transactionOperation.subType,
      operationShortDate: transactionOperation.operationShortDate,
      operationDate: transactionOperation.operationDate ? transactionOperation.operationDate.format(DATE_TIME_FORMAT) : null,
      isFraud: transactionOperation.isFraud,
      taggedAt: transactionOperation.taggedAt ? transactionOperation.taggedAt.format(DATE_TIME_FORMAT) : null,
      fraudSource: transactionOperation.fraudSource,
      comment: transactionOperation.comment,
      falsePositiveDetectedAt: transactionOperation.falsePositiveDetectedAt
        ? transactionOperation.falsePositiveDetectedAt.format(DATE_TIME_FORMAT)
        : null,
      tid: transactionOperation.tid,
      parentMsisdn: transactionOperation.parentMsisdn,
      fctDt: transactionOperation.fctDt,
      parentId: transactionOperation.parentId,
      canceledAt: transactionOperation.canceledAt ? transactionOperation.canceledAt.format(DATE_TIME_FORMAT) : null,
      canceledId: transactionOperation.canceledId,
      productId: transactionOperation.productId,
    });
  }

  protected createFromForm(): ITransactionOperation {
    return {
      ...new TransactionOperation(),
      id: this.editForm.get(['id'])!.value,
      amount: this.editForm.get(['amount'])!.value,
      subsMsisdn: this.editForm.get(['subsMsisdn'])!.value,
      agentMsisdn: this.editForm.get(['agentMsisdn'])!.value,
      typeTransaction: this.editForm.get(['typeTransaction'])!.value,
      createdAt: this.editForm.get(['createdAt'])!.value ? dayjs(this.editForm.get(['createdAt'])!.value, DATE_TIME_FORMAT) : undefined,
      transactionStatus: this.editForm.get(['transactionStatus'])!.value,
      senderZone: this.editForm.get(['senderZone'])!.value,
      senderProfile: this.editForm.get(['senderProfile'])!.value,
      codeTerritory: this.editForm.get(['codeTerritory'])!.value,
      subType: this.editForm.get(['subType'])!.value,
      operationShortDate: this.editForm.get(['operationShortDate'])!.value,
      operationDate: this.editForm.get(['operationDate'])!.value
        ? dayjs(this.editForm.get(['operationDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      isFraud: this.editForm.get(['isFraud'])!.value,
      taggedAt: this.editForm.get(['taggedAt'])!.value ? dayjs(this.editForm.get(['taggedAt'])!.value, DATE_TIME_FORMAT) : undefined,
      fraudSource: this.editForm.get(['fraudSource'])!.value,
      comment: this.editForm.get(['comment'])!.value,
      falsePositiveDetectedAt: this.editForm.get(['falsePositiveDetectedAt'])!.value
        ? dayjs(this.editForm.get(['falsePositiveDetectedAt'])!.value, DATE_TIME_FORMAT)
        : undefined,
      tid: this.editForm.get(['tid'])!.value,
      parentMsisdn: this.editForm.get(['parentMsisdn'])!.value,
      fctDt: this.editForm.get(['fctDt'])!.value,
      parentId: this.editForm.get(['parentId'])!.value,
      canceledAt: this.editForm.get(['canceledAt'])!.value ? dayjs(this.editForm.get(['canceledAt'])!.value, DATE_TIME_FORMAT) : undefined,
      canceledId: this.editForm.get(['canceledId'])!.value,
      productId: this.editForm.get(['productId'])!.value,
    };
  }
}
