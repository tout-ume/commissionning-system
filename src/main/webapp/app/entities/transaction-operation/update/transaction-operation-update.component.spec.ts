import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { TransactionOperationService } from '../service/transaction-operation.service';
import { ITransactionOperation, TransactionOperation } from '../transaction-operation.model';

import { TransactionOperationUpdateComponent } from './transaction-operation-update.component';

describe('TransactionOperation Management Update Component', () => {
  let comp: TransactionOperationUpdateComponent;
  let fixture: ComponentFixture<TransactionOperationUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let transactionOperationService: TransactionOperationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [TransactionOperationUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(TransactionOperationUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(TransactionOperationUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    transactionOperationService = TestBed.inject(TransactionOperationService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const transactionOperation: ITransactionOperation = { id: 456 };

      activatedRoute.data = of({ transactionOperation });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(transactionOperation));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<TransactionOperation>>();
      const transactionOperation = { id: 123 };
      jest.spyOn(transactionOperationService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ transactionOperation });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: transactionOperation }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(transactionOperationService.update).toHaveBeenCalledWith(transactionOperation);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<TransactionOperation>>();
      const transactionOperation = new TransactionOperation();
      jest.spyOn(transactionOperationService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ transactionOperation });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: transactionOperation }));
      saveSubject.complete();

      // THEN
      expect(transactionOperationService.create).toHaveBeenCalledWith(transactionOperation);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<TransactionOperation>>();
      const transactionOperation = { id: 123 };
      jest.spyOn(transactionOperationService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ transactionOperation });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(transactionOperationService.update).toHaveBeenCalledWith(transactionOperation);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
