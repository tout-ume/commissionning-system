import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ITransactionOperation } from '../transaction-operation.model';
import { TransactionOperationService } from '../service/transaction-operation.service';

@Component({
  templateUrl: './transaction-operation-delete-dialog.component.html',
})
export class TransactionOperationDeleteDialogComponent {
  transactionOperation?: ITransactionOperation;

  constructor(protected transactionOperationService: TransactionOperationService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.transactionOperationService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
