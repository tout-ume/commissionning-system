import dayjs from 'dayjs/esm';
import { ICommission } from 'app/entities/commission/commission.model';

export interface ITransactionOperation {
  id?: number;
  amount?: number | null;
  subsMsisdn?: string | null;
  agentMsisdn?: string | null;
  typeTransaction?: string | null;
  createdAt?: dayjs.Dayjs | null;
  transactionStatus?: string | null;
  senderZone?: string | null;
  senderProfile?: string | null;
  codeTerritory?: string | null;
  subType?: string | null;
  operationShortDate?: dayjs.Dayjs | null;
  operationDate?: dayjs.Dayjs | null;
  isFraud?: boolean | null;
  taggedAt?: dayjs.Dayjs | null;
  fraudSource?: string | null;
  comment?: string | null;
  falsePositiveDetectedAt?: dayjs.Dayjs | null;
  tid?: string | null;
  parentMsisdn?: string | null;
  fctDt?: string | null;
  parentId?: string | null;
  canceledAt?: dayjs.Dayjs | null;
  canceledId?: string | null;
  productId?: string | null;
  commissions?: ICommission[] | null;
}

export class TransactionOperation implements ITransactionOperation {
  constructor(
    public id?: number,
    public amount?: number | null,
    public subsMsisdn?: string | null,
    public agentMsisdn?: string | null,
    public typeTransaction?: string | null,
    public createdAt?: dayjs.Dayjs | null,
    public transactionStatus?: string | null,
    public senderZone?: string | null,
    public senderProfile?: string | null,
    public codeTerritory?: string | null,
    public subType?: string | null,
    public operationShortDate?: dayjs.Dayjs | null,
    public operationDate?: dayjs.Dayjs | null,
    public isFraud?: boolean | null,
    public taggedAt?: dayjs.Dayjs | null,
    public fraudSource?: string | null,
    public comment?: string | null,
    public falsePositiveDetectedAt?: dayjs.Dayjs | null,
    public tid?: string | null,
    public parentMsisdn?: string | null,
    public fctDt?: string | null,
    public parentId?: string | null,
    public canceledAt?: dayjs.Dayjs | null,
    public canceledId?: string | null,
    public productId?: string | null,
    public commissions?: ICommission[] | null
  ) {
    this.isFraud = this.isFraud ?? false;
  }
}

export function getTransactionOperationIdentifier(transactionOperation: ITransactionOperation): number | undefined {
  return transactionOperation.id;
}
