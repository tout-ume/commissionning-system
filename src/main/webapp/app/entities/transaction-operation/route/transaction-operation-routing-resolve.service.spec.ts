import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { ITransactionOperation, TransactionOperation } from '../transaction-operation.model';
import { TransactionOperationService } from '../service/transaction-operation.service';

import { TransactionOperationRoutingResolveService } from './transaction-operation-routing-resolve.service';

describe('TransactionOperation routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: TransactionOperationRoutingResolveService;
  let service: TransactionOperationService;
  let resultTransactionOperation: ITransactionOperation | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(TransactionOperationRoutingResolveService);
    service = TestBed.inject(TransactionOperationService);
    resultTransactionOperation = undefined;
  });

  describe('resolve', () => {
    it('should return ITransactionOperation returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultTransactionOperation = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultTransactionOperation).toEqual({ id: 123 });
    });

    it('should return new ITransactionOperation if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultTransactionOperation = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultTransactionOperation).toEqual(new TransactionOperation());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as TransactionOperation })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultTransactionOperation = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultTransactionOperation).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
