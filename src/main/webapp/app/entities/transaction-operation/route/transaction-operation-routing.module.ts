import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { TransactionOperationComponent } from '../list/transaction-operation.component';
import { TransactionOperationDetailComponent } from '../detail/transaction-operation-detail.component';
import { TransactionOperationUpdateComponent } from '../update/transaction-operation-update.component';
import { TransactionOperationRoutingResolveService } from './transaction-operation-routing-resolve.service';

const transactionOperationRoute: Routes = [
  {
    path: '',
    component: TransactionOperationComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TransactionOperationDetailComponent,
    resolve: {
      transactionOperation: TransactionOperationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TransactionOperationUpdateComponent,
    resolve: {
      transactionOperation: TransactionOperationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TransactionOperationUpdateComponent,
    resolve: {
      transactionOperation: TransactionOperationRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(transactionOperationRoute)],
  exports: [RouterModule],
})
export class TransactionOperationRoutingModule {}
