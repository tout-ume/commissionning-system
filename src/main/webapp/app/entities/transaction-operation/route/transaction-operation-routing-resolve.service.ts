import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ITransactionOperation, TransactionOperation } from '../transaction-operation.model';
import { TransactionOperationService } from '../service/transaction-operation.service';

@Injectable({ providedIn: 'root' })
export class TransactionOperationRoutingResolveService implements Resolve<ITransactionOperation> {
  constructor(protected service: TransactionOperationService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITransactionOperation> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((transactionOperation: HttpResponse<TransactionOperation>) => {
          if (transactionOperation.body) {
            return of(transactionOperation.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new TransactionOperation());
  }
}
