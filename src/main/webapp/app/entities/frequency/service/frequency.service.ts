import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IFrequency, getFrequencyIdentifier } from '../frequency.model';

export type EntityResponseType = HttpResponse<IFrequency>;
export type EntityArrayResponseType = HttpResponse<IFrequency[]>;

@Injectable({ providedIn: 'root' })
export class FrequencyService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/frequencies');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(frequency: IFrequency): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(frequency);
    return this.http
      .post<IFrequency>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(frequency: IFrequency): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(frequency);
    return this.http
      .put<IFrequency>(`${this.resourceUrl}/${getFrequencyIdentifier(frequency) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(frequency: IFrequency): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(frequency);
    return this.http
      .patch<IFrequency>(`${this.resourceUrl}/${getFrequencyIdentifier(frequency) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IFrequency>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IFrequency[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addFrequencyToCollectionIfMissing(
    frequencyCollection: IFrequency[],
    ...frequenciesToCheck: (IFrequency | null | undefined)[]
  ): IFrequency[] {
    const frequencies: IFrequency[] = frequenciesToCheck.filter(isPresent);
    if (frequencies.length > 0) {
      const frequencyCollectionIdentifiers = frequencyCollection.map(frequencyItem => getFrequencyIdentifier(frequencyItem)!);
      const frequenciesToAdd = frequencies.filter(frequencyItem => {
        const frequencyIdentifier = getFrequencyIdentifier(frequencyItem);
        if (frequencyIdentifier == null || frequencyCollectionIdentifiers.includes(frequencyIdentifier)) {
          return false;
        }
        frequencyCollectionIdentifiers.push(frequencyIdentifier);
        return true;
      });
      return [...frequenciesToAdd, ...frequencyCollection];
    }
    return frequencyCollection;
  }

  protected convertDateFromClient(frequency: IFrequency): IFrequency {
    return Object.assign({}, frequency, {
      createdDate: frequency.createdDate?.isValid() ? frequency.createdDate.toJSON() : undefined,
      lastModifiedDate: frequency.lastModifiedDate?.isValid() ? frequency.lastModifiedDate.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdDate = res.body.createdDate ? dayjs(res.body.createdDate) : undefined;
      res.body.lastModifiedDate = res.body.lastModifiedDate ? dayjs(res.body.lastModifiedDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((frequency: IFrequency) => {
        frequency.createdDate = frequency.createdDate ? dayjs(frequency.createdDate) : undefined;
        frequency.lastModifiedDate = frequency.lastModifiedDate ? dayjs(frequency.lastModifiedDate) : undefined;
      });
    }
    return res;
  }
}
