import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { FrequencyType } from 'app/entities/enumerations/frequency-type.model';
import { OperationType } from 'app/entities/enumerations/operation-type.model';
import { Cycle } from 'app/entities/enumerations/cycle.model';
import { PeriodOfOccurrence } from 'app/entities/enumerations/period-of-occurrence.model';
import { IFrequency, Frequency } from '../frequency.model';

import { FrequencyService } from './frequency.service';

describe('Frequency Service', () => {
  let service: FrequencyService;
  let httpMock: HttpTestingController;
  let elemDefault: IFrequency;
  let expectedResult: IFrequency | IFrequency[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(FrequencyService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      name: 'AAAAAAA',
      type: FrequencyType.INSTANTLY,
      operationType: OperationType.CALCULUS,
      executionTime: 0,
      executionWeekDay: 0,
      executionMonthDay: 0,
      daysAfter: 0,
      createdBy: 'AAAAAAA',
      createdDate: currentDate,
      lastModifiedBy: 'AAAAAAA',
      lastModifiedDate: currentDate,
      cycle: Cycle.DAYS,
      periodOfOccurrence: PeriodOfOccurrence.WEEK,
      numberOfCycle: 0,
      occurrenceByPeriod: 0,
      datesOfOccurrence: 'AAAAAAA',
      spare1: 'AAAAAAA',
      spare2: 'AAAAAAA',
      spare3: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          createdDate: currentDate.format(DATE_TIME_FORMAT),
          lastModifiedDate: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Frequency', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          createdDate: currentDate.format(DATE_TIME_FORMAT),
          lastModifiedDate: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdDate: currentDate,
          lastModifiedDate: currentDate,
        },
        returnedFromService
      );

      service.create(new Frequency()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Frequency', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          type: 'BBBBBB',
          operationType: 'BBBBBB',
          executionTime: 1,
          executionWeekDay: 1,
          executionMonthDay: 1,
          daysAfter: 1,
          createdBy: 'BBBBBB',
          createdDate: currentDate.format(DATE_TIME_FORMAT),
          lastModifiedBy: 'BBBBBB',
          lastModifiedDate: currentDate.format(DATE_TIME_FORMAT),
          cycle: 'BBBBBB',
          periodOfOccurrence: 'BBBBBB',
          numberOfCycle: 1,
          occurrenceByPeriod: 1,
          datesOfOccurrence: 'BBBBBB',
          spare1: 'BBBBBB',
          spare2: 'BBBBBB',
          spare3: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdDate: currentDate,
          lastModifiedDate: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Frequency', () => {
      const patchObject = Object.assign(
        {
          type: 'BBBBBB',
          operationType: 'BBBBBB',
          executionTime: 1,
          executionMonthDay: 1,
          createdDate: currentDate.format(DATE_TIME_FORMAT),
          lastModifiedBy: 'BBBBBB',
          lastModifiedDate: currentDate.format(DATE_TIME_FORMAT),
          periodOfOccurrence: 'BBBBBB',
          numberOfCycle: 1,
          occurrenceByPeriod: 1,
          datesOfOccurrence: 'BBBBBB',
        },
        new Frequency()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          createdDate: currentDate,
          lastModifiedDate: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Frequency', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          type: 'BBBBBB',
          operationType: 'BBBBBB',
          executionTime: 1,
          executionWeekDay: 1,
          executionMonthDay: 1,
          daysAfter: 1,
          createdBy: 'BBBBBB',
          createdDate: currentDate.format(DATE_TIME_FORMAT),
          lastModifiedBy: 'BBBBBB',
          lastModifiedDate: currentDate.format(DATE_TIME_FORMAT),
          cycle: 'BBBBBB',
          periodOfOccurrence: 'BBBBBB',
          numberOfCycle: 1,
          occurrenceByPeriod: 1,
          datesOfOccurrence: 'BBBBBB',
          spare1: 'BBBBBB',
          spare2: 'BBBBBB',
          spare3: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdDate: currentDate,
          lastModifiedDate: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Frequency', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addFrequencyToCollectionIfMissing', () => {
      it('should add a Frequency to an empty array', () => {
        const frequency: IFrequency = { id: 123 };
        expectedResult = service.addFrequencyToCollectionIfMissing([], frequency);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(frequency);
      });

      it('should not add a Frequency to an array that contains it', () => {
        const frequency: IFrequency = { id: 123 };
        const frequencyCollection: IFrequency[] = [
          {
            ...frequency,
          },
          { id: 456 },
        ];
        expectedResult = service.addFrequencyToCollectionIfMissing(frequencyCollection, frequency);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Frequency to an array that doesn't contain it", () => {
        const frequency: IFrequency = { id: 123 };
        const frequencyCollection: IFrequency[] = [{ id: 456 }];
        expectedResult = service.addFrequencyToCollectionIfMissing(frequencyCollection, frequency);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(frequency);
      });

      it('should add only unique Frequency to an array', () => {
        const frequencyArray: IFrequency[] = [{ id: 123 }, { id: 456 }, { id: 48169 }];
        const frequencyCollection: IFrequency[] = [{ id: 123 }];
        expectedResult = service.addFrequencyToCollectionIfMissing(frequencyCollection, ...frequencyArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const frequency: IFrequency = { id: 123 };
        const frequency2: IFrequency = { id: 456 };
        expectedResult = service.addFrequencyToCollectionIfMissing([], frequency, frequency2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(frequency);
        expect(expectedResult).toContain(frequency2);
      });

      it('should accept null and undefined values', () => {
        const frequency: IFrequency = { id: 123 };
        expectedResult = service.addFrequencyToCollectionIfMissing([], null, frequency, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(frequency);
      });

      it('should return initial array if no Frequency is added', () => {
        const frequencyCollection: IFrequency[] = [{ id: 123 }];
        expectedResult = service.addFrequencyToCollectionIfMissing(frequencyCollection, undefined, null);
        expect(expectedResult).toEqual(frequencyCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
