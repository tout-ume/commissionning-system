import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import dayjs from 'dayjs/esm';

import { Frequency, IFrequency } from '../frequency.model';
import { FrequencyService } from '../service/frequency.service';
import { FrequencyType } from 'app/entities/enumerations/frequency-type.model';
import { DATE_TIME_FORMAT } from '../../../config/input.constants';
import { AccountService } from '../../../core/auth/account.service';
import { OperationType } from '../../enumerations/operation-type.model';
import { Cycle } from 'app/entities/enumerations/cycle.model';
import { PeriodOfOccurrence } from 'app/entities/enumerations/period-of-occurrence.model';
import Swal from 'sweetalert2';

@Component({
  selector: 'jhi-frequency-update',
  templateUrl: './frequency-update.component.html',
})
export class FrequencyUpdateComponent implements OnInit {
  isSaving = false;
  frequencyTypeValues = Object.keys(FrequencyType);
  frequencyOperationTypeValues = Object.keys(OperationType);
  cycleValues = Object.keys(Cycle);
  periodOfOccurrenceValues = Object.keys(PeriodOfOccurrence);
  connectedUser: string | undefined;
  isCalculation = true;
  numberOfoccurrence = 0;
  weekDays: any[] = [
    { id: 1, label: 'Lundi' },
    { id: 2, label: 'Mardi' },
    { id: 3, label: 'Mercredi' },
    { id: 4, label: 'Jeudi' },
    { id: 5, label: 'Vendredi' },
    { id: 6, label: 'Samedi' },
    { id: 7, label: 'Dimanche' },
  ];

  editForm = this.fb.group({
    name: [{ value: null, disabled: true }],
    type: [null, [Validators.required]],
    operationType: [null, [Validators.required]],
  });

  constructor(
    protected frequencyService: FrequencyService,
    protected accountService: AccountService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ frequency }) => {
      if (frequency.id === undefined) {
        const today = dayjs().startOf('day');
        frequency.createdDate = today;
        frequency.lastModifiedDate = today;
      }

      this.accountService.getAuthenticationState().subscribe(account => {
        if (account) {
          this.connectedUser = account.login;
        }
      });
      // this.updateForm(frequency);
    });
  }

  previousState(): void {
    window.history.back();
  }

  /**
   * Récupère le numéro du jour de la semaine
   *
   * @param {string} label - Le libellé du jour.
   * @returns {number} Le numéro du jour
   */
  getExecutionWeekDayNumber(label: string): number {
    switch (label) {
      case 'Lundi':
        return 1;
      case 'Mardi':
        return 2;
      case 'Mercredi':
        return 3;
      case 'Jeudi':
        return 4;
      case 'Vendredi':
        return 5;
      case 'Samedi':
        return 6;
      case 'Dimanche':
        return 7;
      default:
        return 0;
    }
  }

  /**
   * Génère un nom pour la nouvelle fréquence
   *
   * @returns {string} Le nom de la nouvelle fréquence
   */
  generateFrequencyName(): string {
    let value: '';
    let cycleCode: '';
    const type = this.editForm.get('type')?.value;
    switch (type) {
      case 'INSTANTLY':
        return 'Instantanée';
      case 'DAILY':
        value = this.editForm.get('executionTime')!.value;
        return value + '' !== 'null' ? 'Journalière_' + value : 'Journalière_';
      case 'WEEKLY':
        value = this.editForm.get('executionWeekDay')!.value;
        return value + '' !== 'null' ? 'Hebdomadaire_' + value : 'Hebdomadaire_';
      case 'MONTHLY':
        value = this.editForm.get('executionMonthDay')!.value;
        return value + '' !== 'null' ? 'Mensuelle_' + value : 'Mensuelle_';
      case 'QUARTERLY':
        value = this.editForm.get('daysAfter')!.value;
        return value + '' !== 'null' ? 'Trimestrielle_' + value : 'Trimestrielle_';
      case 'SEMI_ANNUALLY':
        value = this.editForm.get('daysAfter')!.value;
        return value + '' !== 'null' ? 'Semestrielle_' + value : 'Semestrielle_';
      case 'CYCLIC':
        value = this.editForm.get('numberOfCycle')!.value;
        cycleCode = this.editForm.get('cycle')!.value;
        return value + '' !== 'null' ? 'Tous_les_' + value + '_' + this.getLabelFromCode(cycleCode) : 'Tous_les_';
      case 'RECCURRENT':
        value = this.editForm.get('occurrenceByPeriod')!.value;
        cycleCode = this.editForm.get('periodOfOccurrence')!.value;
        return value + '' !== 'null' ? value + '_fois_par_' + this.getLabelFromCode(cycleCode) : '_fois_par_';
      default:
        return '';
    }
  }

  /**
   * Annule tous les validateurs des différents champs
   *
   */
  cancelAllValidators(): void {
    this.editForm.removeControl('executionTime');
    this.editForm.removeControl('executionWeekDay');
    this.editForm.removeControl('executionMonthDay');
    this.editForm.removeControl('daysAfter');
    this.editForm.removeControl('periodOfOccurrence');
    this.editForm.removeControl('occurrenceByPeriod');
  }

  /**
   * Mets à jour les validateurs des différents champs selon le type de fréquence choisit
   *
   */
  updateValidators(): void {
    this.cancelAllValidators();
    this.cancelOccurrenceDatesValidators();
    const type = this.editForm.get('type')!.value;
    switch (type) {
      case 'DAILY':
        // Add validator for executionTime's field
        this.editForm.addControl('executionTime', this.fb.control(null, [Validators.required]));
        break;
      case 'WEEKLY':
        // Add validator for executionWeekDay's field
        this.editForm.addControl('executionWeekDay', this.fb.control(null, [Validators.required, Validators.min(0), Validators.max(6)]));
        break;
      case 'MONTHLY':
        // Add validator for executionMonthDay's field
        this.editForm.addControl('executionMonthDay', this.fb.control(null, [Validators.required, Validators.min(1), Validators.max(31)]));
        break;
      case 'QUARTERLY':
        // Add validator for executionMonthDay and daysAfter fields
        this.editForm.addControl('executionMonthDay', this.fb.control(null, [Validators.required, Validators.min(1), Validators.max(31)]));
        this.editForm.addControl('daysAfter', this.fb.control(null, [Validators.required, Validators.min(0), Validators.max(31)]));
        break;
      case 'SEMI_ANNUALLY':
        // Add validator for executionMonthDay and daysAfter fields
        this.editForm.addControl('executionMonthDay', this.fb.control(null, [Validators.required, Validators.min(1), Validators.max(31)]));
        this.editForm.addControl('daysAfter', this.fb.control(null, [Validators.required, Validators.min(0), Validators.max(31)]));
        break;
      case 'CYCLIC':
        // Add validator for cycle and numberOfCycle fields
        this.editForm.addControl('cycle', this.fb.control(null, [Validators.required]));
        this.editForm.addControl('numberOfCycle', this.fb.control(null, [Validators.required, Validators.min(2), Validators.max(5)]));
        break;
      case 'RECCURRENT':
        // Add validator for periodOfOccurrence and occurrenceByPeriod fields
        this.editForm.addControl('periodOfOccurrence', this.fb.control(null, [Validators.required]));
        this.editForm.addControl('occurrenceByPeriod', this.fb.control(null, [Validators.required, Validators.min(2), Validators.max(5)]));
        break;
      default:
        break;
    }
  }

  cancelOccurrenceDatesValidators(): void {
    if (this.editForm.get('occurrenceByPeriod')?.value) {
      for (let i = 1; i <= this.editForm.get('occurrenceByPeriod')?.value; i++) {
        this.editForm.removeControl(`occurrenceDate${i}`);
      }
    }
    this.numberOfoccurrence = 0;
  }

  updateOccurrenceDatesValidators(): void {
    if (this.editForm.get('occurrenceByPeriod')?.value) {
      for (let i = 1; i <= this.editForm.get('occurrenceByPeriod')?.value; i++) {
        this.editForm.get('periodOfOccurrence')?.value === 'WEEK'
          ? this.editForm.addControl(`occurrenceDate${i}`, this.fb.control(null, [Validators.required]))
          : this.editForm.addControl(
              `occurrenceDate${i}`,
              this.fb.control(null, [Validators.required, Validators.min(0), Validators.max(28)])
            );
      }
    }
    this.numberOfoccurrence = this.editForm.get('occurrenceByPeriod')?.value;
  }

  counter(i: number | null): Array<any> {
    return new Array(i);
  }

  /**
   * Renvoie un libéllé lisible du type de fréquence
   * @param {string} code - Code de la fréquence.
   * @returns {string} Le libellé correspondant au type de fréquence
   */
  getFrequencyTypeFromCode(code: string): string {
    switch (code) {
      case 'INSTANTLY':
        return 'Instantanée';
      case 'DAILY':
        return 'Journalière';
      case 'WEEKLY':
        return 'Hebdomadaire';
      case 'MONTHLY':
        return 'Mensuelle';
      case 'QUARTERLY':
        return 'Trimestrielle';
      case 'SEMI_ANNUALLY':
        return 'Semestrielle';
      case 'CYCLIC':
        return 'Cyclique (tous les x ...)';
      case 'RECCURRENT':
        return 'Récurrent (x fois par ...)';
      default:
        return '';
    }
  }

  getLabelFromCode(code: string): string {
    switch (code) {
      case OperationType.CALCULUS:
        return 'Calcul';
      case OperationType.PAYMENT:
        return 'Paiement';
      case Cycle.MONTHS:
        return 'Mois';
      case Cycle.WEEKS:
        return 'Semaines';
      case Cycle.DAYS:
        return 'Jours';
      case PeriodOfOccurrence.WEEK:
        return 'Semaine';
      case PeriodOfOccurrence.MONTH:
        return 'Mois';
      default:
        return '';
    }
  }

  save(): void {
    this.isSaving = true;
    const frequency = this.createFromForm();
    if (this.editForm.get('occurrenceByPeriod')?.value) {
      let chain = '';
      for (let i = 1; i <= this.editForm.get('occurrenceByPeriod')?.value; i++) {
        // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
        chain += `${this.editForm.get([`occurrenceDate${i}`])?.value}`;
        i !== this.editForm.get('occurrenceByPeriod')?.value ? (chain += ',') : '';
      }
      frequency.datesOfOccurrence = chain;
    }
    if (frequency.id !== undefined) {
      this.subscribeToSaveResponse(this.frequencyService.update(frequency));
    } else {
      this.subscribeToSaveResponse(this.frequencyService.create(frequency));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFrequency>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      error => {
        console.warn('********************* Error: ', error);
        Swal.fire({
          title: 'Erreur!',
          // eslint-disable-next-line @typescript-eslint/restrict-template-expressions
          text: `Une erreur est survenue, veuillez réessayer. Message: ${error.error.title}`,
          icon: 'error',
          confirmButtonText: 'Okay',
        });
      }
    );
  }

  protected onSaveSuccess(): void {
    this.cancelOccurrenceDatesValidators();
    this.editForm.reset();
    Swal.fire({
      title: 'Succès!',
      text: 'La fréquence a été ajoutée avec succès!',
      icon: 'success',
      confirmButtonText: 'Okay',
    });
    // this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(frequency: IFrequency): void {
    this.editForm.patchValue({
      name: frequency.name,
      type: frequency.type,
      operationType: frequency.operationType,
      executionTime: frequency.executionTime,
      executionWeekDay: frequency.executionWeekDay,
      executionMonthDay: frequency.executionMonthDay,
      daysAfter: frequency.daysAfter,
      // createdBy: frequency.createdBy,
      // createdDate: frequency.createdDate ? frequency.createdDate.format(DATE_TIME_FORMAT) : null,
      // lastModifiedBy: frequency.lastModifiedBy,
      // lastModifiedDate: frequency.lastModifiedDate ? frequency.lastModifiedDate.format(DATE_TIME_FORMAT) : null,
    });
  }

  protected createFromForm(): IFrequency {
    return {
      ...new Frequency(),
      name: this.generateFrequencyName(),
      type: this.editForm.get(['type'])!.value,
      operationType: this.editForm.get(['operationType'])!.value,
      executionTime: this.editForm.get(['executionTime']) ? this.editForm.get(['executionTime'])!.value : 0,
      executionWeekDay: this.editForm.get(['executionWeekDay'])
        ? this.getExecutionWeekDayNumber(this.editForm.get(['executionWeekDay'])!.value)
        : undefined,
      executionMonthDay: this.editForm.get(['executionMonthDay']) ? this.editForm.get(['executionMonthDay'])!.value : undefined,
      daysAfter: this.editForm.get(['daysAfter']) ? this.editForm.get(['daysAfter'])!.value : undefined,
      cycle: this.editForm.get(['cycle']) ? this.editForm.get(['cycle'])!.value : undefined,
      periodOfOccurrence: this.editForm.get(['periodOfOccurrence']) ? this.editForm.get(['periodOfOccurrence'])!.value : undefined,
      numberOfCycle: this.editForm.get(['numberOfCycle']) ? this.editForm.get(['numberOfCycle'])!.value : undefined,
      occurrenceByPeriod: this.editForm.get(['occurrenceByPeriod']) ? this.editForm.get(['occurrenceByPeriod'])!.value : undefined,
      createdBy: this.connectedUser,
      createdDate: dayjs(dayjs().startOf('second'), DATE_TIME_FORMAT),
    };
  }
}
