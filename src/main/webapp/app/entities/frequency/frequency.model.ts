import dayjs from 'dayjs/esm';
import { FrequencyType } from 'app/entities/enumerations/frequency-type.model';
import { OperationType } from 'app/entities/enumerations/operation-type.model';
import { Cycle } from 'app/entities/enumerations/cycle.model';
import { PeriodOfOccurrence } from 'app/entities/enumerations/period-of-occurrence.model';

export interface IFrequency {
  id?: number;
  name?: string;
  type?: FrequencyType;
  operationType?: OperationType;
  executionTime?: number;
  executionWeekDay?: number | null;
  executionMonthDay?: number | null;
  daysAfter?: number | null;
  createdBy?: string;
  createdDate?: dayjs.Dayjs;
  lastModifiedBy?: string | null;
  lastModifiedDate?: dayjs.Dayjs | null;
  cycle?: Cycle | null;
  periodOfOccurrence?: PeriodOfOccurrence | null;
  numberOfCycle?: number | null;
  occurrenceByPeriod?: number | null;
  datesOfOccurrence?: string | null;
  spare1?: string | null;
  spare2?: string | null;
  spare3?: string | null;
}

export class Frequency implements IFrequency {
  constructor(
    public id?: number,
    public name?: string,
    public type?: FrequencyType,
    public operationType?: OperationType,
    public executionTime?: number,
    public executionWeekDay?: number | null,
    public executionMonthDay?: number | null,
    public daysAfter?: number | null,
    public createdBy?: string,
    public createdDate?: dayjs.Dayjs,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: dayjs.Dayjs | null,
    public cycle?: Cycle | null,
    public periodOfOccurrence?: PeriodOfOccurrence | null,
    public numberOfCycle?: number | null,
    public occurrenceByPeriod?: number | null,
    public datesOfOccurrence?: string | null,
    public spare1?: string | null,
    public spare2?: string | null,
    public spare3?: string | null
  ) {}
}

export function getFrequencyIdentifier(frequency: IFrequency): number | undefined {
  return frequency.id;
}
