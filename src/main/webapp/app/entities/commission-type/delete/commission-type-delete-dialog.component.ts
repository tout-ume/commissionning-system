import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICommissionType } from '../commission-type.model';
import { CommissionTypeService } from '../service/commission-type.service';

@Component({
  templateUrl: './commission-type-delete-dialog.component.html',
})
export class CommissionTypeDeleteDialogComponent {
  commissionType?: ICommissionType;

  constructor(protected commissionTypeService: CommissionTypeService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.commissionTypeService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
