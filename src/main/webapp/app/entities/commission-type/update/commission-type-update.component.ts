import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ICommissionType, CommissionType } from '../commission-type.model';
import { CommissionTypeService } from '../service/commission-type.service';

@Component({
  selector: 'jhi-commission-type-update',
  templateUrl: './commission-type-update.component.html',
})
export class CommissionTypeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    tauxValue: [],
  });

  constructor(
    protected commissionTypeService: CommissionTypeService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ commissionType }) => {
      this.updateForm(commissionType);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const commissionType = this.createFromForm();
    if (commissionType.id !== undefined) {
      this.subscribeToSaveResponse(this.commissionTypeService.update(commissionType));
    } else {
      this.subscribeToSaveResponse(this.commissionTypeService.create(commissionType));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICommissionType>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(commissionType: ICommissionType): void {
    this.editForm.patchValue({
      id: commissionType.id,
      tauxValue: commissionType.tauxValue,
    });
  }

  protected createFromForm(): ICommissionType {
    return {
      ...new CommissionType(),
      id: this.editForm.get(['id'])!.value,
      tauxValue: this.editForm.get(['tauxValue'])!.value,
    };
  }
}
