import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { CommissionTypeService } from '../service/commission-type.service';
import { ICommissionType, CommissionType } from '../commission-type.model';

import { CommissionTypeUpdateComponent } from './commission-type-update.component';

describe('CommissionType Management Update Component', () => {
  let comp: CommissionTypeUpdateComponent;
  let fixture: ComponentFixture<CommissionTypeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let commissionTypeService: CommissionTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [CommissionTypeUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CommissionTypeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CommissionTypeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    commissionTypeService = TestBed.inject(CommissionTypeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const commissionType: ICommissionType = { id: 456 };

      activatedRoute.data = of({ commissionType });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(commissionType));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CommissionType>>();
      const commissionType = { id: 123 };
      jest.spyOn(commissionTypeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ commissionType });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: commissionType }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(commissionTypeService.update).toHaveBeenCalledWith(commissionType);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CommissionType>>();
      const commissionType = new CommissionType();
      jest.spyOn(commissionTypeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ commissionType });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: commissionType }));
      saveSubject.complete();

      // THEN
      expect(commissionTypeService.create).toHaveBeenCalledWith(commissionType);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CommissionType>>();
      const commissionType = { id: 123 };
      jest.spyOn(commissionTypeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ commissionType });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(commissionTypeService.update).toHaveBeenCalledWith(commissionType);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
