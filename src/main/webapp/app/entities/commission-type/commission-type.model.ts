import { CommissionTypeType } from '../enumerations/commission-type-type';
import { PalierValueType } from '../enumerations/palier-value-type';

export interface ICommissionType {
  id?: number;
  tauxValue?: number | null;
  minValue?: number | null;
  maxValue?: number | null;
  palierValue?: number | null;
  commissionTypeType?: CommissionTypeType | null;
  palierValueType?: PalierValueType | null;
  isInfinity?: boolean;
  isPalierFix?: boolean;
}

export class CommissionType implements ICommissionType {
  constructor(
    public id?: number,
    public tauxValue?: number | null,
    public minValue?: number | null,
    public maxValue?: number | null,
    public palierValue?: number | null,
    public commissionTypeType?: CommissionTypeType | null,
    public palierValueType?: PalierValueType | null,
    public isInfinity?: boolean,
    public isPalierFix?: boolean
  ) {}
}

export function getCommissionTypeIdentifier(commissionType: ICommissionType): number | undefined {
  return commissionType.id;
}
