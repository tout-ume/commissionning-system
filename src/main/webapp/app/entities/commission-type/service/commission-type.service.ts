import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICommissionType, getCommissionTypeIdentifier } from '../commission-type.model';

export type EntityResponseType = HttpResponse<ICommissionType>;
export type EntityArrayResponseType = HttpResponse<ICommissionType[]>;

@Injectable({ providedIn: 'root' })
export class CommissionTypeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/commission-types');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(commissionType: ICommissionType): Observable<EntityResponseType> {
    return this.http.post<ICommissionType>(this.resourceUrl, commissionType, { observe: 'response' });
  }

  update(commissionType: ICommissionType): Observable<EntityResponseType> {
    return this.http.put<ICommissionType>(`${this.resourceUrl}/${getCommissionTypeIdentifier(commissionType) as number}`, commissionType, {
      observe: 'response',
    });
  }

  partialUpdate(commissionType: ICommissionType): Observable<EntityResponseType> {
    return this.http.patch<ICommissionType>(
      `${this.resourceUrl}/${getCommissionTypeIdentifier(commissionType) as number}`,
      commissionType,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICommissionType>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICommissionType[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCommissionTypeToCollectionIfMissing(
    commissionTypeCollection: ICommissionType[],
    ...commissionTypesToCheck: (ICommissionType | null | undefined)[]
  ): ICommissionType[] {
    const commissionTypes: ICommissionType[] = commissionTypesToCheck.filter(isPresent);
    if (commissionTypes.length > 0) {
      const commissionTypeCollectionIdentifiers = commissionTypeCollection.map(
        commissionTypeItem => getCommissionTypeIdentifier(commissionTypeItem)!
      );
      const commissionTypesToAdd = commissionTypes.filter(commissionTypeItem => {
        const commissionTypeIdentifier = getCommissionTypeIdentifier(commissionTypeItem);
        if (commissionTypeIdentifier == null || commissionTypeCollectionIdentifiers.includes(commissionTypeIdentifier)) {
          return false;
        }
        commissionTypeCollectionIdentifiers.push(commissionTypeIdentifier);
        return true;
      });
      return [...commissionTypesToAdd, ...commissionTypeCollection];
    }
    return commissionTypeCollection;
  }
}
