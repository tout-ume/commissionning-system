import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ICommissionType, CommissionType } from '../commission-type.model';

import { CommissionTypeService } from './commission-type.service';

describe('CommissionType Service', () => {
  let service: CommissionTypeService;
  let httpMock: HttpTestingController;
  let elemDefault: ICommissionType;
  let expectedResult: ICommissionType | ICommissionType[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(CommissionTypeService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      tauxValue: 0,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a CommissionType', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new CommissionType()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a CommissionType', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          tauxValue: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a CommissionType', () => {
      const patchObject = Object.assign(
        {
          tauxValue: 1,
        },
        new CommissionType()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of CommissionType', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          tauxValue: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a CommissionType', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addCommissionTypeToCollectionIfMissing', () => {
      it('should add a CommissionType to an empty array', () => {
        const commissionType: ICommissionType = { id: 123 };
        expectedResult = service.addCommissionTypeToCollectionIfMissing([], commissionType);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(commissionType);
      });

      it('should not add a CommissionType to an array that contains it', () => {
        const commissionType: ICommissionType = { id: 123 };
        const commissionTypeCollection: ICommissionType[] = [
          {
            ...commissionType,
          },
          { id: 456 },
        ];
        expectedResult = service.addCommissionTypeToCollectionIfMissing(commissionTypeCollection, commissionType);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a CommissionType to an array that doesn't contain it", () => {
        const commissionType: ICommissionType = { id: 123 };
        const commissionTypeCollection: ICommissionType[] = [{ id: 456 }];
        expectedResult = service.addCommissionTypeToCollectionIfMissing(commissionTypeCollection, commissionType);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(commissionType);
      });

      it('should add only unique CommissionType to an array', () => {
        const commissionTypeArray: ICommissionType[] = [{ id: 123 }, { id: 456 }, { id: 53443 }];
        const commissionTypeCollection: ICommissionType[] = [{ id: 123 }];
        expectedResult = service.addCommissionTypeToCollectionIfMissing(commissionTypeCollection, ...commissionTypeArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const commissionType: ICommissionType = { id: 123 };
        const commissionType2: ICommissionType = { id: 456 };
        expectedResult = service.addCommissionTypeToCollectionIfMissing([], commissionType, commissionType2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(commissionType);
        expect(expectedResult).toContain(commissionType2);
      });

      it('should accept null and undefined values', () => {
        const commissionType: ICommissionType = { id: 123 };
        expectedResult = service.addCommissionTypeToCollectionIfMissing([], null, commissionType, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(commissionType);
      });

      it('should return initial array if no CommissionType is added', () => {
        const commissionTypeCollection: ICommissionType[] = [{ id: 123 }];
        expectedResult = service.addCommissionTypeToCollectionIfMissing(commissionTypeCollection, undefined, null);
        expect(expectedResult).toEqual(commissionTypeCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
