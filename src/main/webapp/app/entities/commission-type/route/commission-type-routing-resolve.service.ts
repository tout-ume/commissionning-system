import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICommissionType, CommissionType } from '../commission-type.model';
import { CommissionTypeService } from '../service/commission-type.service';

@Injectable({ providedIn: 'root' })
export class CommissionTypeRoutingResolveService implements Resolve<ICommissionType> {
  constructor(protected service: CommissionTypeService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICommissionType> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((commissionType: HttpResponse<CommissionType>) => {
          if (commissionType.body) {
            return of(commissionType.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CommissionType());
  }
}
