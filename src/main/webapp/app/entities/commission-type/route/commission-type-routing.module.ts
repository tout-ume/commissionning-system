import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CommissionTypeComponent } from '../list/commission-type.component';
import { CommissionTypeDetailComponent } from '../detail/commission-type-detail.component';
import { CommissionTypeUpdateComponent } from '../update/commission-type-update.component';
import { CommissionTypeRoutingResolveService } from './commission-type-routing-resolve.service';

const commissionTypeRoute: Routes = [
  {
    path: '',
    component: CommissionTypeComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CommissionTypeDetailComponent,
    resolve: {
      commissionType: CommissionTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CommissionTypeUpdateComponent,
    resolve: {
      commissionType: CommissionTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CommissionTypeUpdateComponent,
    resolve: {
      commissionType: CommissionTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(commissionTypeRoute)],
  exports: [RouterModule],
})
export class CommissionTypeRoutingModule {}
