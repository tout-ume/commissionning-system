import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { CommissionTypeComponent } from './list/commission-type.component';
import { CommissionTypeDetailComponent } from './detail/commission-type-detail.component';
import { CommissionTypeUpdateComponent } from './update/commission-type-update.component';
import { CommissionTypeDeleteDialogComponent } from './delete/commission-type-delete-dialog.component';
import { CommissionTypeRoutingModule } from './route/commission-type-routing.module';

@NgModule({
  imports: [SharedModule, CommissionTypeRoutingModule],
  declarations: [
    CommissionTypeComponent,
    CommissionTypeDetailComponent,
    CommissionTypeUpdateComponent,
    CommissionTypeDeleteDialogComponent,
  ],
  entryComponents: [CommissionTypeDeleteDialogComponent],
})
export class CommissionTypeModule {}
