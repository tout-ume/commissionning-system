import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CommissionTypeDetailComponent } from './commission-type-detail.component';

describe('CommissionType Management Detail Component', () => {
  let comp: CommissionTypeDetailComponent;
  let fixture: ComponentFixture<CommissionTypeDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CommissionTypeDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ commissionType: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(CommissionTypeDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(CommissionTypeDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load commissionType on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.commissionType).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
