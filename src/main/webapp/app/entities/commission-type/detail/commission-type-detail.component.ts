import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICommissionType } from '../commission-type.model';

@Component({
  selector: 'jhi-commission-type-detail',
  templateUrl: './commission-type-detail.component.html',
})
export class CommissionTypeDetailComponent implements OnInit {
  commissionType: ICommissionType | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ commissionType }) => {
      this.commissionType = commissionType;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
