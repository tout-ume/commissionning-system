export enum PaymentStrategy {
  AUTOMATIC = 'AUTOMATIC',

  MANUAL = 'MANUAL',

  SCHEDULED = 'SCHEDULED',
}
