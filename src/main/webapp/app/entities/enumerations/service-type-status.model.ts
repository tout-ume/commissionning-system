export enum ServiceTypeStatus {
  ACTIVATED = 'ACTIVATED',

  DEACTIVATED = 'DEACTIVATED',
}
