export enum CommissioningPlanState {
  CREATED = 'CREATED',

  ARCHIVED = 'ARCHIVED',

  IN_USE = 'IN_USE',

  DRAFT = 'DRAFT',
}
