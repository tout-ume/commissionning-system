export enum Cycle {
  DAYS = 'DAYS',

  WEEKS = 'WEEKS',

  MONTHS = 'MONTHS',
}
