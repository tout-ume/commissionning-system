export enum CommissionPaymentStatus {
  TO_BE_PAID = 'TO_BE_PAID',

  TO_BE_PAID_TO_PARTNER = 'TO_BE_PAID_TO_PARTNER',

  PAID = 'PAID',

  FAILED = 'FAILED',

  PENDING = 'PENDING',

  BLOCKED = 'BLOCKED',
}
