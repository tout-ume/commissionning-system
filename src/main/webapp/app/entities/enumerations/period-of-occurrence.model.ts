export enum PeriodOfOccurrence {
  WEEK = 'WEEK',

  MONTH = 'MONTH',
}
