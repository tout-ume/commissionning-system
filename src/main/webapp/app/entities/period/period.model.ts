import dayjs from 'dayjs/esm';
import { FrequencyType } from 'app/entities/enumerations/frequency-type.model';
import { OperationType } from 'app/entities/enumerations/operation-type.model';

export interface IPeriod {
  id?: number;
  monthDayFrom?: number | null;
  monthDayTo?: number | null;
  weekDayFrom?: number | null;
  weekDayTo?: number | null;
  dayHourFrom?: number | null;
  dayHourTo?: number | null;
  isPreviousDayHourFrom?: boolean | null;
  isPreviousDayHourTo?: boolean | null;
  isPreviousMonthDayFrom?: boolean | null;
  isPreviousMonthDayTo?: boolean | null;
  isCompleteDay?: boolean | null;
  isCompleteMonth?: boolean | null;
  periodType?: FrequencyType;
  operationType?: OperationType;
  createdBy?: string;
  createdDate?: dayjs.Dayjs;
  lastModifiedBy?: string | null;
  lastModifiedDate?: dayjs.Dayjs | null;
  daysOrDatesOfOccurrence?: string | null;
  spare1?: string | null;
  spare2?: string | null;
  spare3?: string | null;
}

export class Period implements IPeriod {
  constructor(
    public id?: number,
    public monthDayFrom?: number | null,
    public monthDayTo?: number | null,
    public weekDayFrom?: number | null,
    public weekDayTo?: number | null,
    public dayHourFrom?: number | null,
    public dayHourTo?: number | null,
    public isPreviousDayHourFrom?: boolean | null,
    public isPreviousDayHourTo?: boolean | null,
    public isPreviousMonthDayFrom?: boolean | null,
    public isPreviousMonthDayTo?: boolean | null,
    public isCompleteDay?: boolean | null,
    public isCompleteMonth?: boolean | null,
    public periodType?: FrequencyType,
    public operationType?: OperationType,
    public createdBy?: string,
    public createdDate?: dayjs.Dayjs,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: dayjs.Dayjs | null,
    public daysOrDatesOfOccurrence?: string | null,
    public spare1?: string | null,
    public spare2?: string | null,
    public spare3?: string | null
  ) {
    this.isPreviousDayHourFrom = this.isPreviousDayHourFrom ?? false;
    this.isPreviousDayHourTo = this.isPreviousDayHourTo ?? false;
    this.isPreviousMonthDayFrom = this.isPreviousMonthDayFrom ?? false;
    this.isPreviousMonthDayTo = this.isPreviousMonthDayTo ?? false;
    this.isCompleteDay = this.isCompleteDay ?? false;
    this.isCompleteMonth = this.isCompleteMonth ?? false;
  }
}

export function getPeriodIdentifier(period: IPeriod): number | undefined {
  return period.id;
}
