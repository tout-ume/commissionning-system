import { Component, Input } from '@angular/core';

import { IPeriod } from '../period.model';

@Component({
  selector: 'jhi-period-detail',
  templateUrl: './period-detail.component.html',
})
export class PeriodDetailComponent {
  @Input() period: IPeriod | null | undefined;

  previousState(): void {
    window.history.back();
  }

  /**
   * Récupère le jour de la semaine
   *
   * @param {number} num - Le numéro du jour.
   * @returns {string} - Le libellé du jour.
   */
  getExecutionWeekDayLabel(num: number | string | null | undefined): string {
    switch (Number(num)) {
      case 1:
        return 'Lundi';
      case 2:
        return 'Mardi';
      case 3:
        return 'Mercredi';
      case 4:
        return 'Jeudi';
      case 5:
        return 'Vendredi';
      case 6:
        return 'Samedi';
      case 7:
        return 'Dimanche';
      default:
        return '';
    }
  }

  counter(i: number): Array<any> {
    return new Array(i);
  }
}
