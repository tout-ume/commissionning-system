import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IPeriod, Period } from '../period.model';
import { PeriodService } from '../service/period.service';
import { FrequencyType } from 'app/entities/enumerations/frequency-type.model';
import { OperationType } from 'app/entities/enumerations/operation-type.model';

@Component({
  selector: 'jhi-period-update',
  templateUrl: './period-update.component.html',
})
export class PeriodUpdateComponent implements OnInit {
  isSaving = false;
  frequencyTypeValues = Object.keys(FrequencyType);
  operationTypeValues = Object.keys(OperationType);

  editForm = this.fb.group({
    id: [],
    monthDayFrom: [],
    monthDayTo: [],
    weekDayFrom: [],
    weekDayTo: [],
    dayHourFrom: [],
    dayHourTo: [],
    isPreviousDayHourFrom: [],
    isPreviousDayHourTo: [],
    isPreviousMonthDayFrom: [],
    isPreviousMonthDayTo: [],
    isCompleteDay: [],
    isCompleteMonth: [],
    periodType: [null, [Validators.required]],
    operationType: [null, [Validators.required]],
    createdBy: [null, [Validators.required, Validators.maxLength(50)]],
    createdDate: [null, [Validators.required]],
    lastModifiedBy: [null, [Validators.maxLength(50)]],
    lastModifiedDate: [],
    daysOrDatesOfOccurrence: [],
    spare1: [],
    spare2: [],
    spare3: [],
  });

  constructor(protected periodService: PeriodService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ period }) => {
      if (period.id === undefined) {
        const today = dayjs().startOf('day');
        period.createdDate = today;
        period.lastModifiedDate = today;
      }

      this.updateForm(period);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const period = this.createFromForm();
    if (period.id !== undefined) {
      this.subscribeToSaveResponse(this.periodService.update(period));
    } else {
      this.subscribeToSaveResponse(this.periodService.create(period));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPeriod>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(period: IPeriod): void {
    this.editForm.patchValue({
      id: period.id,
      monthDayFrom: period.monthDayFrom,
      monthDayTo: period.monthDayTo,
      weekDayFrom: period.weekDayFrom,
      weekDayTo: period.weekDayTo,
      dayHourFrom: period.dayHourFrom,
      dayHourTo: period.dayHourTo,
      isPreviousDayHourFrom: period.isPreviousDayHourFrom,
      isPreviousDayHourTo: period.isPreviousDayHourTo,
      isPreviousMonthDayFrom: period.isPreviousMonthDayFrom,
      isPreviousMonthDayTo: period.isPreviousMonthDayTo,
      isCompleteDay: period.isCompleteDay,
      isCompleteMonth: period.isCompleteMonth,
      periodType: period.periodType,
      operationType: period.operationType,
      createdBy: period.createdBy,
      createdDate: period.createdDate ? period.createdDate.format(DATE_TIME_FORMAT) : null,
      lastModifiedBy: period.lastModifiedBy,
      lastModifiedDate: period.lastModifiedDate ? period.lastModifiedDate.format(DATE_TIME_FORMAT) : null,
      daysOrDatesOfOccurrence: period.daysOrDatesOfOccurrence,
      spare1: period.spare1,
      spare2: period.spare2,
      spare3: period.spare3,
    });
  }

  protected createFromForm(): IPeriod {
    return {
      ...new Period(),
      id: this.editForm.get(['id'])!.value,
      monthDayFrom: this.editForm.get(['monthDayFrom'])!.value,
      monthDayTo: this.editForm.get(['monthDayTo'])!.value,
      weekDayFrom: this.editForm.get(['weekDayFrom'])!.value,
      weekDayTo: this.editForm.get(['weekDayTo'])!.value,
      dayHourFrom: this.editForm.get(['dayHourFrom'])!.value,
      dayHourTo: this.editForm.get(['dayHourTo'])!.value,
      isPreviousDayHourFrom: this.editForm.get(['isPreviousDayHourFrom'])!.value,
      isPreviousDayHourTo: this.editForm.get(['isPreviousDayHourTo'])!.value,
      isPreviousMonthDayFrom: this.editForm.get(['isPreviousMonthDayFrom'])!.value,
      isPreviousMonthDayTo: this.editForm.get(['isPreviousMonthDayTo'])!.value,
      isCompleteDay: this.editForm.get(['isCompleteDay'])!.value,
      isCompleteMonth: this.editForm.get(['isCompleteMonth'])!.value,
      periodType: this.editForm.get(['periodType'])!.value,
      operationType: this.editForm.get(['operationType'])!.value,
      createdBy: this.editForm.get(['createdBy'])!.value,
      createdDate: this.editForm.get(['createdDate'])!.value
        ? dayjs(this.editForm.get(['createdDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      lastModifiedBy: this.editForm.get(['lastModifiedBy'])!.value,
      lastModifiedDate: this.editForm.get(['lastModifiedDate'])!.value
        ? dayjs(this.editForm.get(['lastModifiedDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      daysOrDatesOfOccurrence: this.editForm.get(['daysOrDatesOfOccurrence'])!.value,
      spare1: this.editForm.get(['spare1'])!.value,
      spare2: this.editForm.get(['spare2'])!.value,
      spare3: this.editForm.get(['spare3'])!.value,
    };
  }
}
