import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IServiceType, ServiceType } from '../service-type.model';
import { ServiceTypeService } from '../service/service-type.service';
import { ServiceTypeStatus } from 'app/entities/enumerations/service-type-status.model';
import { CommissioningPlanType } from 'app/entities/enumerations/commissioning-plan-type.model';

@Component({
  selector: 'jhi-service-type-update',
  templateUrl: './service-type-update.component.html',
})
export class ServiceTypeUpdateComponent implements OnInit {
  isSaving = false;
  serviceTypeStatusValues = Object.keys(ServiceTypeStatus);
  commissioningPlanTypeValues = Object.keys(CommissioningPlanType);

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    code: [null, [Validators.required]],
    description: [],
    state: [null, [Validators.required]],
    isCosRelated: [null, [Validators.required]],
    commissioningPlanType: [],
    createdBy: [null, [Validators.required, Validators.maxLength(50)]],
    createdDate: [],
    lastModifiedBy: [null, [Validators.maxLength(50)]],
    lastModifiedDate: [],
  });

  constructor(protected serviceTypeService: ServiceTypeService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ serviceType }) => {
      if (serviceType.id === undefined) {
        const today = dayjs().startOf('day');
        serviceType.createdDate = today;
        serviceType.lastModifiedDate = today;
      }

      this.updateForm(serviceType);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const serviceType = this.createFromForm();
    if (serviceType.id !== undefined) {
      this.subscribeToSaveResponse(this.serviceTypeService.update(serviceType));
    } else {
      this.subscribeToSaveResponse(this.serviceTypeService.create(serviceType));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IServiceType>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(serviceType: IServiceType): void {
    this.editForm.patchValue({
      id: serviceType.id,
      name: serviceType.name,
      code: serviceType.code,
      description: serviceType.description,
      state: serviceType.state,
      isCosRelated: serviceType.isCosRelated,
      commissioningPlanType: serviceType.commissioningPlanType,
      createdBy: serviceType.createdBy,
      createdDate: serviceType.createdDate ? serviceType.createdDate.format(DATE_TIME_FORMAT) : null,
      lastModifiedBy: serviceType.lastModifiedBy,
      lastModifiedDate: serviceType.lastModifiedDate ? serviceType.lastModifiedDate.format(DATE_TIME_FORMAT) : null,
    });
  }

  protected createFromForm(): IServiceType {
    return {
      ...new ServiceType(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      code: this.editForm.get(['code'])!.value,
      description: this.editForm.get(['description'])!.value,
      state: this.editForm.get(['state'])!.value,
      isCosRelated: this.editForm.get(['isCosRelated'])!.value,
      commissioningPlanType: this.editForm.get(['commissioningPlanType'])!.value,
      createdBy: this.editForm.get(['createdBy'])!.value,
      createdDate: this.editForm.get(['createdDate'])!.value
        ? dayjs(this.editForm.get(['createdDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      lastModifiedBy: this.editForm.get(['lastModifiedBy'])!.value,
      lastModifiedDate: this.editForm.get(['lastModifiedDate'])!.value
        ? dayjs(this.editForm.get(['lastModifiedDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
    };
  }
}
