import dayjs from 'dayjs/esm';
import { ICommissioningPlan } from 'app/entities/commissioning-plan/commissioning-plan.model';
import { ServiceTypeStatus } from 'app/entities/enumerations/service-type-status.model';
import { CommissioningPlanType } from 'app/entities/enumerations/commissioning-plan-type.model';

export interface IServiceType {
  id?: number;
  name?: string;
  code?: string;
  description?: string | null;
  state?: ServiceTypeStatus;
  isCosRelated?: boolean;
  commissioningPlanType?: CommissioningPlanType | null;
  createdBy?: string;
  createdDate?: dayjs.Dayjs | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: dayjs.Dayjs | null;
  commissionPlans?: ICommissioningPlan[] | null;
}

export class ServiceType implements IServiceType {
  constructor(
    public id?: number,
    public name?: string,
    public code?: string,
    public description?: string | null,
    public state?: ServiceTypeStatus,
    public isCosRelated?: boolean,
    public commissioningPlanType?: CommissioningPlanType | null,
    public createdBy?: string,
    public createdDate?: dayjs.Dayjs | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: dayjs.Dayjs | null,
    public commissionPlans?: ICommissioningPlan[] | null
  ) {
    this.isCosRelated = this.isCosRelated ?? false;
  }
}

export function getServiceTypeIdentifier(serviceType: IServiceType): number | undefined {
  return serviceType.id;
}
