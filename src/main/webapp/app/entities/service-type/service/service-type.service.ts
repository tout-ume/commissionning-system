import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IServiceType, getServiceTypeIdentifier } from '../service-type.model';

export type EntityResponseType = HttpResponse<IServiceType>;
export type EntityArrayResponseType = HttpResponse<IServiceType[]>;

@Injectable({ providedIn: 'root' })
export class ServiceTypeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/service-types');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(serviceType: IServiceType): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(serviceType);
    return this.http
      .post<IServiceType>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(serviceType: IServiceType): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(serviceType);
    return this.http
      .put<IServiceType>(`${this.resourceUrl}/${getServiceTypeIdentifier(serviceType) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(serviceType: IServiceType): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(serviceType);
    return this.http
      .patch<IServiceType>(`${this.resourceUrl}/${getServiceTypeIdentifier(serviceType) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IServiceType>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IServiceType[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addServiceTypeToCollectionIfMissing(
    serviceTypeCollection: IServiceType[],
    ...serviceTypesToCheck: (IServiceType | null | undefined)[]
  ): IServiceType[] {
    const serviceTypes: IServiceType[] = serviceTypesToCheck.filter(isPresent);
    if (serviceTypes.length > 0) {
      const serviceTypeCollectionIdentifiers = serviceTypeCollection.map(serviceTypeItem => getServiceTypeIdentifier(serviceTypeItem)!);
      const serviceTypesToAdd = serviceTypes.filter(serviceTypeItem => {
        const serviceTypeIdentifier = getServiceTypeIdentifier(serviceTypeItem);
        if (serviceTypeIdentifier == null || serviceTypeCollectionIdentifiers.includes(serviceTypeIdentifier)) {
          return false;
        }
        serviceTypeCollectionIdentifiers.push(serviceTypeIdentifier);
        return true;
      });
      return [...serviceTypesToAdd, ...serviceTypeCollection];
    }
    return serviceTypeCollection;
  }

  protected convertDateFromClient(serviceType: IServiceType): IServiceType {
    return Object.assign({}, serviceType, {
      createdDate: serviceType.createdDate?.isValid() ? serviceType.createdDate.toJSON() : undefined,
      lastModifiedDate: serviceType.lastModifiedDate?.isValid() ? serviceType.lastModifiedDate.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdDate = res.body.createdDate ? dayjs(res.body.createdDate) : undefined;
      res.body.lastModifiedDate = res.body.lastModifiedDate ? dayjs(res.body.lastModifiedDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((serviceType: IServiceType) => {
        serviceType.createdDate = serviceType.createdDate ? dayjs(serviceType.createdDate) : undefined;
        serviceType.lastModifiedDate = serviceType.lastModifiedDate ? dayjs(serviceType.lastModifiedDate) : undefined;
      });
    }
    return res;
  }
}
