import { ICommissionType } from 'app/entities/commission-type/commission-type.model';
import { IPartnerProfile } from 'app/entities/partner-profile/partner-profile.model';
import { ICommissioningPlan } from 'app/entities/commissioning-plan/commissioning-plan.model';
import { ITerritory } from '../territory/territory.model';
import { IZone } from '../zone/zone.model';

export interface IConfigurationPlan {
  id?: number;
  commissionTypes?: ICommissionType[] | null;
  zones?: IZone[] | null;
  partnerProfile?: IPartnerProfile | null;
  commissioningPlan?: ICommissioningPlan | null;
  usePalier?: boolean | false;
  isSelected?: boolean | false;
  availableTerritories?: ITerritory[] | null;
  areAllTerritoriesSelected?: boolean | false;
}

export class ConfigurationPlan implements IConfigurationPlan {
  constructor(
    public id?: number,
    public commissionTypes?: ICommissionType[] | null,
    public zones?: IZone[] | null,
    public partnerProfile?: IPartnerProfile | null,
    public commissioningPlan?: ICommissioningPlan | null,
    public usePalier?: boolean | false,
    public isSelected?: boolean | false,
    public availableTerritories?: ITerritory[] | null,
    public areAllTerritoriesSelected?: boolean | false
  ) {}
}

export function getConfigurationPlanIdentifier(configurationPlan: IConfigurationPlan): number | undefined {
  return configurationPlan.id;
}
