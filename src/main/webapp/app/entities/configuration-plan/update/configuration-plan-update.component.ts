import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IConfigurationPlan, ConfigurationPlan } from '../configuration-plan.model';
import { ConfigurationPlanService } from '../service/configuration-plan.service';
import { IPartnerProfile } from 'app/entities/partner-profile/partner-profile.model';
import { PartnerProfileService } from 'app/entities/partner-profile/service/partner-profile.service';
import { ICommissioningPlan } from 'app/entities/commissioning-plan/commissioning-plan.model';
import { CommissioningPlanService } from 'app/entities/commissioning-plan/service/commissioning-plan.service';
import { IZone } from 'app/entities/zone/zone.model';
import { ZoneService } from 'app/entities/zone/service/zone.service';

@Component({
  selector: 'jhi-configuration-plan-update',
  templateUrl: './configuration-plan-update.component.html',
})
export class ConfigurationPlanUpdateComponent implements OnInit {
  isSaving = false;

  partnerProfilesSharedCollection: IPartnerProfile[] = [];
  commissioningPlansSharedCollection: ICommissioningPlan[] = [];
  zonesSharedCollection: IZone[] = [];

  editForm = this.fb.group({
    id: [],
    partnerProfile: [],
    commissioningPlan: [],
    zones: [],
  });

  constructor(
    protected configurationPlanService: ConfigurationPlanService,
    protected partnerProfileService: PartnerProfileService,
    protected commissioningPlanService: CommissioningPlanService,
    protected zoneService: ZoneService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ configurationPlan }) => {
      this.updateForm(configurationPlan);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const configurationPlan = this.createFromForm();
    if (configurationPlan.id !== undefined) {
      this.subscribeToSaveResponse(this.configurationPlanService.update(configurationPlan));
    } else {
      this.subscribeToSaveResponse(this.configurationPlanService.create(configurationPlan));
    }
  }

  trackPartnerProfileById(_index: number, item: IPartnerProfile): number {
    return item.id!;
  }

  trackCommissioningPlanById(_index: number, item: ICommissioningPlan): number {
    return item.id!;
  }

  trackZoneById(_index: number, item: IZone): number {
    return item.id!;
  }

  getSelectedZone(option: IZone, selectedVals?: IZone[]): IZone {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IConfigurationPlan>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(configurationPlan: IConfigurationPlan): void {
    this.editForm.patchValue({
      id: configurationPlan.id,
      partnerProfile: configurationPlan.partnerProfile,
      commissioningPlan: configurationPlan.commissioningPlan,
      zones: configurationPlan.zones,
    });

    this.partnerProfilesSharedCollection = this.partnerProfileService.addPartnerProfileToCollectionIfMissing(
      this.partnerProfilesSharedCollection,
      configurationPlan.partnerProfile
    );
    this.commissioningPlansSharedCollection = this.commissioningPlanService.addCommissioningPlanToCollectionIfMissing(
      this.commissioningPlansSharedCollection,
      configurationPlan.commissioningPlan
    );
    this.zonesSharedCollection = this.zoneService.addZoneToCollectionIfMissing(
      this.zonesSharedCollection,
      ...(configurationPlan.zones ?? [])
    );
  }

  protected loadRelationshipsOptions(): void {
    this.partnerProfileService
      .query()
      .pipe(map((res: HttpResponse<IPartnerProfile[]>) => res.body ?? []))
      .pipe(
        map((partnerProfiles: IPartnerProfile[]) =>
          this.partnerProfileService.addPartnerProfileToCollectionIfMissing(partnerProfiles, this.editForm.get('partnerProfile')!.value)
        )
      )
      .subscribe((partnerProfiles: IPartnerProfile[]) => (this.partnerProfilesSharedCollection = partnerProfiles));

    this.commissioningPlanService
      .query()
      .pipe(map((res: HttpResponse<ICommissioningPlan[]>) => res.body ?? []))
      .pipe(
        map((commissioningPlans: ICommissioningPlan[]) =>
          this.commissioningPlanService.addCommissioningPlanToCollectionIfMissing(
            commissioningPlans,
            this.editForm.get('commissioningPlan')!.value
          )
        )
      )
      .subscribe((commissioningPlans: ICommissioningPlan[]) => (this.commissioningPlansSharedCollection = commissioningPlans));

    this.zoneService
      .query()
      .pipe(map((res: HttpResponse<IZone[]>) => res.body ?? []))
      .pipe(map((zones: IZone[]) => this.zoneService.addZoneToCollectionIfMissing(zones, ...(this.editForm.get('zones')!.value ?? []))))
      .subscribe((zones: IZone[]) => (this.zonesSharedCollection = zones));
  }

  protected createFromForm(): IConfigurationPlan {
    return {
      ...new ConfigurationPlan(),
      id: this.editForm.get(['id'])!.value,
      partnerProfile: this.editForm.get(['partnerProfile'])!.value,
      commissioningPlan: this.editForm.get(['commissioningPlan'])!.value,
      zones: this.editForm.get(['zones'])!.value,
    };
  }
}
