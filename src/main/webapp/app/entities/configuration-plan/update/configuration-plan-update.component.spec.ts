import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ConfigurationPlanService } from '../service/configuration-plan.service';
import { IConfigurationPlan, ConfigurationPlan } from '../configuration-plan.model';
import { IPartnerProfile } from 'app/entities/partner-profile/partner-profile.model';
import { PartnerProfileService } from 'app/entities/partner-profile/service/partner-profile.service';
import { ICommissioningPlan } from 'app/entities/commissioning-plan/commissioning-plan.model';
import { CommissioningPlanService } from 'app/entities/commissioning-plan/service/commissioning-plan.service';
import { IZone } from 'app/entities/zone/zone.model';
import { ZoneService } from 'app/entities/zone/service/zone.service';

import { ConfigurationPlanUpdateComponent } from './configuration-plan-update.component';

describe('ConfigurationPlan Management Update Component', () => {
  let comp: ConfigurationPlanUpdateComponent;
  let fixture: ComponentFixture<ConfigurationPlanUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let configurationPlanService: ConfigurationPlanService;
  let partnerProfileService: PartnerProfileService;
  let commissioningPlanService: CommissioningPlanService;
  let zoneService: ZoneService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [ConfigurationPlanUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ConfigurationPlanUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ConfigurationPlanUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    configurationPlanService = TestBed.inject(ConfigurationPlanService);
    partnerProfileService = TestBed.inject(PartnerProfileService);
    commissioningPlanService = TestBed.inject(CommissioningPlanService);
    zoneService = TestBed.inject(ZoneService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call PartnerProfile query and add missing value', () => {
      const configurationPlan: IConfigurationPlan = { id: 456 };
      const partnerProfile: IPartnerProfile = { id: 92817 };
      configurationPlan.partnerProfile = partnerProfile;

      const partnerProfileCollection: IPartnerProfile[] = [{ id: 24198 }];
      jest.spyOn(partnerProfileService, 'query').mockReturnValue(of(new HttpResponse({ body: partnerProfileCollection })));
      const additionalPartnerProfiles = [partnerProfile];
      const expectedCollection: IPartnerProfile[] = [...additionalPartnerProfiles, ...partnerProfileCollection];
      jest.spyOn(partnerProfileService, 'addPartnerProfileToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ configurationPlan });
      comp.ngOnInit();

      expect(partnerProfileService.query).toHaveBeenCalled();
      expect(partnerProfileService.addPartnerProfileToCollectionIfMissing).toHaveBeenCalledWith(
        partnerProfileCollection,
        ...additionalPartnerProfiles
      );
      expect(comp.partnerProfilesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call CommissioningPlan query and add missing value', () => {
      const configurationPlan: IConfigurationPlan = { id: 456 };
      const commissioningPlan: ICommissioningPlan = { id: 45251 };
      configurationPlan.commissioningPlan = commissioningPlan;

      const commissioningPlanCollection: ICommissioningPlan[] = [{ id: 53506 }];
      jest.spyOn(commissioningPlanService, 'query').mockReturnValue(of(new HttpResponse({ body: commissioningPlanCollection })));
      const additionalCommissioningPlans = [commissioningPlan];
      const expectedCollection: ICommissioningPlan[] = [...additionalCommissioningPlans, ...commissioningPlanCollection];
      jest.spyOn(commissioningPlanService, 'addCommissioningPlanToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ configurationPlan });
      comp.ngOnInit();

      expect(commissioningPlanService.query).toHaveBeenCalled();
      expect(commissioningPlanService.addCommissioningPlanToCollectionIfMissing).toHaveBeenCalledWith(
        commissioningPlanCollection,
        ...additionalCommissioningPlans
      );
      expect(comp.commissioningPlansSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Zone query and add missing value', () => {
      const configurationPlan: IConfigurationPlan = { id: 456 };
      const zones: IZone[] = [{ id: 11700 }];
      configurationPlan.zones = zones;

      const zoneCollection: IZone[] = [{ id: 76973 }];
      jest.spyOn(zoneService, 'query').mockReturnValue(of(new HttpResponse({ body: zoneCollection })));
      const additionalZones = [...zones];
      const expectedCollection: IZone[] = [...additionalZones, ...zoneCollection];
      jest.spyOn(zoneService, 'addZoneToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ configurationPlan });
      comp.ngOnInit();

      expect(zoneService.query).toHaveBeenCalled();
      expect(zoneService.addZoneToCollectionIfMissing).toHaveBeenCalledWith(zoneCollection, ...additionalZones);
      expect(comp.zonesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const configurationPlan: IConfigurationPlan = { id: 456 };
      const partnerProfile: IPartnerProfile = { id: 69107 };
      configurationPlan.partnerProfile = partnerProfile;
      const commissioningPlan: ICommissioningPlan = { id: 96811 };
      configurationPlan.commissioningPlan = commissioningPlan;
      const zones: IZone = { id: 91708 };
      configurationPlan.zones = [zones];

      activatedRoute.data = of({ configurationPlan });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(configurationPlan));
      expect(comp.partnerProfilesSharedCollection).toContain(partnerProfile);
      expect(comp.commissioningPlansSharedCollection).toContain(commissioningPlan);
      expect(comp.zonesSharedCollection).toContain(zones);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ConfigurationPlan>>();
      const configurationPlan = { id: 123 };
      jest.spyOn(configurationPlanService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ configurationPlan });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: configurationPlan }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(configurationPlanService.update).toHaveBeenCalledWith(configurationPlan);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ConfigurationPlan>>();
      const configurationPlan = new ConfigurationPlan();
      jest.spyOn(configurationPlanService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ configurationPlan });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: configurationPlan }));
      saveSubject.complete();

      // THEN
      expect(configurationPlanService.create).toHaveBeenCalledWith(configurationPlan);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ConfigurationPlan>>();
      const configurationPlan = { id: 123 };
      jest.spyOn(configurationPlanService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ configurationPlan });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(configurationPlanService.update).toHaveBeenCalledWith(configurationPlan);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackPartnerProfileById', () => {
      it('Should return tracked PartnerProfile primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackPartnerProfileById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackCommissioningPlanById', () => {
      it('Should return tracked CommissioningPlan primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCommissioningPlanById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackZoneById', () => {
      it('Should return tracked Zone primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackZoneById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });

  describe('Getting selected relationships', () => {
    describe('getSelectedZone', () => {
      it('Should return option if no Zone is selected', () => {
        const option = { id: 123 };
        const result = comp.getSelectedZone(option);
        expect(result === option).toEqual(true);
      });

      it('Should return selected Zone for according option', () => {
        const option = { id: 123 };
        const selected = { id: 123 };
        const selected2 = { id: 456 };
        const result = comp.getSelectedZone(option, [selected2, selected]);
        expect(result === selected).toEqual(true);
        expect(result === selected2).toEqual(false);
        expect(result === option).toEqual(false);
      });

      it('Should return option if this Zone is not selected', () => {
        const option = { id: 123 };
        const selected = { id: 456 };
        const result = comp.getSelectedZone(option, [selected]);
        expect(result === option).toEqual(true);
        expect(result === selected).toEqual(false);
      });
    });
  });
});
