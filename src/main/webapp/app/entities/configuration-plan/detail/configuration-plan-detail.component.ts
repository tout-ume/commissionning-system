import { Component, Input } from '@angular/core';
import { IConfigurationPlan } from '../configuration-plan.model';

@Component({
  selector: 'jhi-configuration-plan-detail',
  templateUrl: './configuration-plan-detail.component.html',
})
export class ConfigurationPlanDetailComponent {
  @Input() configurationPlan: IConfigurationPlan | null | undefined;

  previousState(): void {
    window.history.back();
  }
}
