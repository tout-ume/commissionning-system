import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ConfigurationPlanDetailComponent } from './configuration-plan-detail.component';

describe('ConfigurationPlan Management Detail Component', () => {
  let comp: ConfigurationPlanDetailComponent;
  let fixture: ComponentFixture<ConfigurationPlanDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ConfigurationPlanDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ configurationPlan: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(ConfigurationPlanDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ConfigurationPlanDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load configurationPlan on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.configurationPlan).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
