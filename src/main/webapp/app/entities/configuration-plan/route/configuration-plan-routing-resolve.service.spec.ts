import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { IConfigurationPlan, ConfigurationPlan } from '../configuration-plan.model';
import { ConfigurationPlanService } from '../service/configuration-plan.service';

import { ConfigurationPlanRoutingResolveService } from './configuration-plan-routing-resolve.service';

describe('ConfigurationPlan routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: ConfigurationPlanRoutingResolveService;
  let service: ConfigurationPlanService;
  let resultConfigurationPlan: IConfigurationPlan | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(ConfigurationPlanRoutingResolveService);
    service = TestBed.inject(ConfigurationPlanService);
    resultConfigurationPlan = undefined;
  });

  describe('resolve', () => {
    it('should return IConfigurationPlan returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultConfigurationPlan = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultConfigurationPlan).toEqual({ id: 123 });
    });

    it('should return new IConfigurationPlan if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultConfigurationPlan = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultConfigurationPlan).toEqual(new ConfigurationPlan());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as ConfigurationPlan })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultConfigurationPlan = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultConfigurationPlan).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
