import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IConfigurationPlan, ConfigurationPlan } from '../configuration-plan.model';
import { ConfigurationPlanService } from '../service/configuration-plan.service';

@Injectable({ providedIn: 'root' })
export class ConfigurationPlanRoutingResolveService implements Resolve<IConfigurationPlan> {
  constructor(protected service: ConfigurationPlanService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IConfigurationPlan> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((configurationPlan: HttpResponse<ConfigurationPlan>) => {
          if (configurationPlan.body) {
            return of(configurationPlan.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ConfigurationPlan());
  }
}
