import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ConfigurationPlanComponent } from '../list/configuration-plan.component';
import { ConfigurationPlanDetailComponent } from '../detail/configuration-plan-detail.component';
import { ConfigurationPlanUpdateComponent } from '../update/configuration-plan-update.component';
import { ConfigurationPlanRoutingResolveService } from './configuration-plan-routing-resolve.service';

const configurationPlanRoute: Routes = [
  {
    path: '',
    component: ConfigurationPlanComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ConfigurationPlanDetailComponent,
    resolve: {
      configurationPlan: ConfigurationPlanRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ConfigurationPlanUpdateComponent,
    resolve: {
      configurationPlan: ConfigurationPlanRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ConfigurationPlanUpdateComponent,
    resolve: {
      configurationPlan: ConfigurationPlanRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(configurationPlanRoute)],
  exports: [RouterModule],
})
export class ConfigurationPlanRoutingModule {}
