import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ConfigurationPlanComponent } from './list/configuration-plan.component';
import { ConfigurationPlanDetailComponent } from './detail/configuration-plan-detail.component';
import { ConfigurationPlanUpdateComponent } from './update/configuration-plan-update.component';
import { ConfigurationPlanDeleteDialogComponent } from './delete/configuration-plan-delete-dialog.component';
import { ConfigurationPlanRoutingModule } from './route/configuration-plan-routing.module';

@NgModule({
  imports: [SharedModule, ConfigurationPlanRoutingModule],
  declarations: [
    ConfigurationPlanComponent,
    ConfigurationPlanDetailComponent,
    ConfigurationPlanUpdateComponent,
    ConfigurationPlanDeleteDialogComponent,
  ],
  entryComponents: [ConfigurationPlanDeleteDialogComponent],
  exports: [ConfigurationPlanDetailComponent],
})
export class ConfigurationPlanModule {}
