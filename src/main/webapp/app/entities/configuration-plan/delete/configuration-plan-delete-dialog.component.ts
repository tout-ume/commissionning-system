import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IConfigurationPlan } from '../configuration-plan.model';
import { ConfigurationPlanService } from '../service/configuration-plan.service';

@Component({
  templateUrl: './configuration-plan-delete-dialog.component.html',
})
export class ConfigurationPlanDeleteDialogComponent {
  configurationPlan?: IConfigurationPlan;

  constructor(protected configurationPlanService: ConfigurationPlanService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.configurationPlanService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
