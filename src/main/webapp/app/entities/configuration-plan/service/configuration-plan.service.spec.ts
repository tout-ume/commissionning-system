import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IConfigurationPlan, ConfigurationPlan } from '../configuration-plan.model';

import { ConfigurationPlanService } from './configuration-plan.service';

describe('ConfigurationPlan Service', () => {
  let service: ConfigurationPlanService;
  let httpMock: HttpTestingController;
  let elemDefault: IConfigurationPlan;
  let expectedResult: IConfigurationPlan | IConfigurationPlan[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ConfigurationPlanService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      spare1: 'AAAAAAA',
      spare2: 'AAAAAAA',
      spare3: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a ConfigurationPlan', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new ConfigurationPlan()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a ConfigurationPlan', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          spare1: 'BBBBBB',
          spare2: 'BBBBBB',
          spare3: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a ConfigurationPlan', () => {
      const patchObject = Object.assign(
        {
          spare1: 'BBBBBB',
          spare2: 'BBBBBB',
        },
        new ConfigurationPlan()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of ConfigurationPlan', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          spare1: 'BBBBBB',
          spare2: 'BBBBBB',
          spare3: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a ConfigurationPlan', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addConfigurationPlanToCollectionIfMissing', () => {
      it('should add a ConfigurationPlan to an empty array', () => {
        const configurationPlan: IConfigurationPlan = { id: 123 };
        expectedResult = service.addConfigurationPlanToCollectionIfMissing([], configurationPlan);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(configurationPlan);
      });

      it('should not add a ConfigurationPlan to an array that contains it', () => {
        const configurationPlan: IConfigurationPlan = { id: 123 };
        const configurationPlanCollection: IConfigurationPlan[] = [
          {
            ...configurationPlan,
          },
          { id: 456 },
        ];
        expectedResult = service.addConfigurationPlanToCollectionIfMissing(configurationPlanCollection, configurationPlan);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a ConfigurationPlan to an array that doesn't contain it", () => {
        const configurationPlan: IConfigurationPlan = { id: 123 };
        const configurationPlanCollection: IConfigurationPlan[] = [{ id: 456 }];
        expectedResult = service.addConfigurationPlanToCollectionIfMissing(configurationPlanCollection, configurationPlan);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(configurationPlan);
      });

      it('should add only unique ConfigurationPlan to an array', () => {
        const configurationPlanArray: IConfigurationPlan[] = [{ id: 123 }, { id: 456 }, { id: 61678 }];
        const configurationPlanCollection: IConfigurationPlan[] = [{ id: 123 }];
        expectedResult = service.addConfigurationPlanToCollectionIfMissing(configurationPlanCollection, ...configurationPlanArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const configurationPlan: IConfigurationPlan = { id: 123 };
        const configurationPlan2: IConfigurationPlan = { id: 456 };
        expectedResult = service.addConfigurationPlanToCollectionIfMissing([], configurationPlan, configurationPlan2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(configurationPlan);
        expect(expectedResult).toContain(configurationPlan2);
      });

      it('should accept null and undefined values', () => {
        const configurationPlan: IConfigurationPlan = { id: 123 };
        expectedResult = service.addConfigurationPlanToCollectionIfMissing([], null, configurationPlan, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(configurationPlan);
      });

      it('should return initial array if no ConfigurationPlan is added', () => {
        const configurationPlanCollection: IConfigurationPlan[] = [{ id: 123 }];
        expectedResult = service.addConfigurationPlanToCollectionIfMissing(configurationPlanCollection, undefined, null);
        expect(expectedResult).toEqual(configurationPlanCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
