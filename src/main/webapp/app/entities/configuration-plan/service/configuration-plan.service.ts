import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IConfigurationPlan, getConfigurationPlanIdentifier } from '../configuration-plan.model';

export type EntityResponseType = HttpResponse<IConfigurationPlan>;
export type EntityArrayResponseType = HttpResponse<IConfigurationPlan[]>;

@Injectable({ providedIn: 'root' })
export class ConfigurationPlanService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/configuration-plans');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(configurationPlan: IConfigurationPlan): Observable<EntityResponseType> {
    return this.http.post<IConfigurationPlan>(this.resourceUrl, configurationPlan, { observe: 'response' });
  }

  update(configurationPlan: IConfigurationPlan): Observable<EntityResponseType> {
    return this.http.put<IConfigurationPlan>(
      `${this.resourceUrl}/${getConfigurationPlanIdentifier(configurationPlan) as number}`,
      configurationPlan,
      { observe: 'response' }
    );
  }

  partialUpdate(configurationPlan: IConfigurationPlan): Observable<EntityResponseType> {
    return this.http.patch<IConfigurationPlan>(
      `${this.resourceUrl}/${getConfigurationPlanIdentifier(configurationPlan) as number}`,
      configurationPlan,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IConfigurationPlan>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IConfigurationPlan[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addConfigurationPlanToCollectionIfMissing(
    configurationPlanCollection: IConfigurationPlan[],
    ...configurationPlansToCheck: (IConfigurationPlan | null | undefined)[]
  ): IConfigurationPlan[] {
    const configurationPlans: IConfigurationPlan[] = configurationPlansToCheck.filter(isPresent);
    if (configurationPlans.length > 0) {
      const configurationPlanCollectionIdentifiers = configurationPlanCollection.map(
        configurationPlanItem => getConfigurationPlanIdentifier(configurationPlanItem)!
      );
      const configurationPlansToAdd = configurationPlans.filter(configurationPlanItem => {
        const configurationPlanIdentifier = getConfigurationPlanIdentifier(configurationPlanItem);
        if (configurationPlanIdentifier == null || configurationPlanCollectionIdentifiers.includes(configurationPlanIdentifier)) {
          return false;
        }
        configurationPlanCollectionIdentifiers.push(configurationPlanIdentifier);
        return true;
      });
      return [...configurationPlansToAdd, ...configurationPlanCollection];
    }
    return configurationPlanCollection;
  }
}
