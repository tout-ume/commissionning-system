import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CommissionAccountDetailComponent } from './commission-account-detail.component';

describe('CommissionAccount Management Detail Component', () => {
  let comp: CommissionAccountDetailComponent;
  let fixture: ComponentFixture<CommissionAccountDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CommissionAccountDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ commissionAccount: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(CommissionAccountDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(CommissionAccountDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load commissionAccount on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.commissionAccount).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
