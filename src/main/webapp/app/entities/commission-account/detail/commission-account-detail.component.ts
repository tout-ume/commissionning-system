import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICommissionAccount } from '../commission-account.model';

@Component({
  selector: 'jhi-commission-account-detail',
  templateUrl: './commission-account-detail.component.html',
})
export class CommissionAccountDetailComponent implements OnInit {
  commissionAccount: ICommissionAccount | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ commissionAccount }) => {
      this.commissionAccount = commissionAccount;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
