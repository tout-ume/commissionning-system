import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICommissionAccount, CommissionAccount } from '../commission-account.model';
import { CommissionAccountService } from '../service/commission-account.service';

@Injectable({ providedIn: 'root' })
export class CommissionAccountRoutingResolveService implements Resolve<ICommissionAccount> {
  constructor(protected service: CommissionAccountService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICommissionAccount> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((commissionAccount: HttpResponse<CommissionAccount>) => {
          if (commissionAccount.body) {
            return of(commissionAccount.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CommissionAccount());
  }
}
