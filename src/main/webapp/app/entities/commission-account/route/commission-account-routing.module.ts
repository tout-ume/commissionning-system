import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CommissionAccountComponent } from '../list/commission-account.component';
import { CommissionAccountDetailComponent } from '../detail/commission-account-detail.component';
import { CommissionAccountUpdateComponent } from '../update/commission-account-update.component';
import { CommissionAccountRoutingResolveService } from './commission-account-routing-resolve.service';

const commissionAccountRoute: Routes = [
  {
    path: '',
    component: CommissionAccountComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CommissionAccountDetailComponent,
    resolve: {
      commissionAccount: CommissionAccountRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CommissionAccountUpdateComponent,
    resolve: {
      commissionAccount: CommissionAccountRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CommissionAccountUpdateComponent,
    resolve: {
      commissionAccount: CommissionAccountRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(commissionAccountRoute)],
  exports: [RouterModule],
})
export class CommissionAccountRoutingModule {}
