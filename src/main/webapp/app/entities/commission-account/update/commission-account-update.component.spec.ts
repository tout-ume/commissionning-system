import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { CommissionAccountService } from '../service/commission-account.service';
import { ICommissionAccount, CommissionAccount } from '../commission-account.model';
import { IPartner } from 'app/entities/partner/partner.model';
import { PartnerService } from 'app/entities/partner/service/partner.service';

import { CommissionAccountUpdateComponent } from './commission-account-update.component';

describe('CommissionAccount Management Update Component', () => {
  let comp: CommissionAccountUpdateComponent;
  let fixture: ComponentFixture<CommissionAccountUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let commissionAccountService: CommissionAccountService;
  let partnerService: PartnerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [CommissionAccountUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CommissionAccountUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CommissionAccountUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    commissionAccountService = TestBed.inject(CommissionAccountService);
    partnerService = TestBed.inject(PartnerService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call partner query and add missing value', () => {
      const commissionAccount: ICommissionAccount = { id: 456 };
      const partner: IPartner = { id: 30488 };
      commissionAccount.partner = partner;

      const partnerCollection: IPartner[] = [{ id: 62078 }];
      jest.spyOn(partnerService, 'query').mockReturnValue(of(new HttpResponse({ body: partnerCollection })));
      const expectedCollection: IPartner[] = [partner, ...partnerCollection];
      jest.spyOn(partnerService, 'addPartnerToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ commissionAccount });
      comp.ngOnInit();

      expect(partnerService.query).toHaveBeenCalled();
      expect(partnerService.addPartnerToCollectionIfMissing).toHaveBeenCalledWith(partnerCollection, partner);
      expect(comp.partnersCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const commissionAccount: ICommissionAccount = { id: 456 };
      const partner: IPartner = { id: 66893 };
      commissionAccount.partner = partner;

      activatedRoute.data = of({ commissionAccount });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(commissionAccount));
      expect(comp.partnersCollection).toContain(partner);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CommissionAccount>>();
      const commissionAccount = { id: 123 };
      jest.spyOn(commissionAccountService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ commissionAccount });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: commissionAccount }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(commissionAccountService.update).toHaveBeenCalledWith(commissionAccount);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CommissionAccount>>();
      const commissionAccount = new CommissionAccount();
      jest.spyOn(commissionAccountService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ commissionAccount });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: commissionAccount }));
      saveSubject.complete();

      // THEN
      expect(commissionAccountService.create).toHaveBeenCalledWith(commissionAccount);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CommissionAccount>>();
      const commissionAccount = { id: 123 };
      jest.spyOn(commissionAccountService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ commissionAccount });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(commissionAccountService.update).toHaveBeenCalledWith(commissionAccount);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackPartnerById', () => {
      it('Should return tracked Partner primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackPartnerById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
