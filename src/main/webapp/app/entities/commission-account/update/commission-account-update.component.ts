import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ICommissionAccount, CommissionAccount } from '../commission-account.model';
import { CommissionAccountService } from '../service/commission-account.service';
import { IPartner } from 'app/entities/partner/partner.model';
import { PartnerService } from 'app/entities/partner/service/partner.service';

@Component({
  selector: 'jhi-commission-account-update',
  templateUrl: './commission-account-update.component.html',
})
export class CommissionAccountUpdateComponent implements OnInit {
  isSaving = false;

  partnersCollection: IPartner[] = [];

  editForm = this.fb.group({
    id: [],
    calculatedBalance: [],
    paidBalance: [],
    spare1: [],
    spare2: [],
    spare3: [],
    partner: [],
  });

  constructor(
    protected commissionAccountService: CommissionAccountService,
    protected partnerService: PartnerService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ commissionAccount }) => {
      this.updateForm(commissionAccount);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const commissionAccount = this.createFromForm();
    if (commissionAccount.id !== undefined) {
      this.subscribeToSaveResponse(this.commissionAccountService.update(commissionAccount));
    } else {
      this.subscribeToSaveResponse(this.commissionAccountService.create(commissionAccount));
    }
  }

  trackPartnerById(_index: number, item: IPartner): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICommissionAccount>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(commissionAccount: ICommissionAccount): void {
    this.editForm.patchValue({
      id: commissionAccount.id,
      calculatedBalance: commissionAccount.calculatedBalance,
      paidBalance: commissionAccount.paidBalance,
      spare1: commissionAccount.spare1,
      spare2: commissionAccount.spare2,
      spare3: commissionAccount.spare3,
      partner: commissionAccount.partner,
    });

    this.partnersCollection = this.partnerService.addPartnerToCollectionIfMissing(this.partnersCollection, commissionAccount.partner);
  }

  protected loadRelationshipsOptions(): void {
    this.partnerService
      .query({ 'commissionAccountId.specified': 'false' })
      .pipe(map((res: HttpResponse<IPartner[]>) => res.body ?? []))
      .pipe(
        map((partners: IPartner[]) => this.partnerService.addPartnerToCollectionIfMissing(partners, this.editForm.get('partner')!.value))
      )
      .subscribe((partners: IPartner[]) => (this.partnersCollection = partners));
  }

  protected createFromForm(): ICommissionAccount {
    return {
      ...new CommissionAccount(),
      id: this.editForm.get(['id'])!.value,
      calculatedBalance: this.editForm.get(['calculatedBalance'])!.value,
      paidBalance: this.editForm.get(['paidBalance'])!.value,
      spare1: this.editForm.get(['spare1'])!.value,
      spare2: this.editForm.get(['spare2'])!.value,
      spare3: this.editForm.get(['spare3'])!.value,
      partner: this.editForm.get(['partner'])!.value,
    };
  }
}
