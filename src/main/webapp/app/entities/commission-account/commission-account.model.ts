import { IPartner } from 'app/entities/partner/partner.model';
import { ICommission } from 'app/entities/commission/commission.model';

export interface ICommissionAccount {
  id?: number;
  calculatedBalance?: number | null;
  paidBalance?: number | null;
  spare1?: string | null;
  spare2?: string | null;
  spare3?: string | null;
  partner?: IPartner | null;
  commissions?: ICommission[] | null;
}

export class CommissionAccount implements ICommissionAccount {
  constructor(
    public id?: number,
    public calculatedBalance?: number | null,
    public paidBalance?: number | null,
    public spare1?: string | null,
    public spare2?: string | null,
    public spare3?: string | null,
    public partner?: IPartner | null,
    public commissions?: ICommission[] | null
  ) {}
}

export function getCommissionAccountIdentifier(commissionAccount: ICommissionAccount): number | undefined {
  return commissionAccount.id;
}
