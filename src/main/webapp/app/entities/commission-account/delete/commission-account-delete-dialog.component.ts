import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICommissionAccount } from '../commission-account.model';
import { CommissionAccountService } from '../service/commission-account.service';

@Component({
  templateUrl: './commission-account-delete-dialog.component.html',
})
export class CommissionAccountDeleteDialogComponent {
  commissionAccount?: ICommissionAccount;

  constructor(protected commissionAccountService: CommissionAccountService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.commissionAccountService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
