import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { CommissionAccountComponent } from './list/commission-account.component';
import { CommissionAccountDetailComponent } from './detail/commission-account-detail.component';
import { CommissionAccountUpdateComponent } from './update/commission-account-update.component';
import { CommissionAccountDeleteDialogComponent } from './delete/commission-account-delete-dialog.component';
import { CommissionAccountRoutingModule } from './route/commission-account-routing.module';

@NgModule({
  imports: [SharedModule, CommissionAccountRoutingModule],
  declarations: [
    CommissionAccountComponent,
    CommissionAccountDetailComponent,
    CommissionAccountUpdateComponent,
    CommissionAccountDeleteDialogComponent,
  ],
  entryComponents: [CommissionAccountDeleteDialogComponent],
})
export class CommissionAccountModule {}
