import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICommissionAccount, getCommissionAccountIdentifier } from '../commission-account.model';

export type EntityResponseType = HttpResponse<ICommissionAccount>;
export type EntityArrayResponseType = HttpResponse<ICommissionAccount[]>;

@Injectable({ providedIn: 'root' })
export class CommissionAccountService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/commission-accounts');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(commissionAccount: ICommissionAccount): Observable<EntityResponseType> {
    return this.http.post<ICommissionAccount>(this.resourceUrl, commissionAccount, { observe: 'response' });
  }

  update(commissionAccount: ICommissionAccount): Observable<EntityResponseType> {
    return this.http.put<ICommissionAccount>(
      `${this.resourceUrl}/${getCommissionAccountIdentifier(commissionAccount) as number}`,
      commissionAccount,
      { observe: 'response' }
    );
  }

  partialUpdate(commissionAccount: ICommissionAccount): Observable<EntityResponseType> {
    return this.http.patch<ICommissionAccount>(
      `${this.resourceUrl}/${getCommissionAccountIdentifier(commissionAccount) as number}`,
      commissionAccount,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICommissionAccount>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICommissionAccount[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCommissionAccountToCollectionIfMissing(
    commissionAccountCollection: ICommissionAccount[],
    ...commissionAccountsToCheck: (ICommissionAccount | null | undefined)[]
  ): ICommissionAccount[] {
    const commissionAccounts: ICommissionAccount[] = commissionAccountsToCheck.filter(isPresent);
    if (commissionAccounts.length > 0) {
      const commissionAccountCollectionIdentifiers = commissionAccountCollection.map(
        commissionAccountItem => getCommissionAccountIdentifier(commissionAccountItem)!
      );
      const commissionAccountsToAdd = commissionAccounts.filter(commissionAccountItem => {
        const commissionAccountIdentifier = getCommissionAccountIdentifier(commissionAccountItem);
        if (commissionAccountIdentifier == null || commissionAccountCollectionIdentifiers.includes(commissionAccountIdentifier)) {
          return false;
        }
        commissionAccountCollectionIdentifiers.push(commissionAccountIdentifier);
        return true;
      });
      return [...commissionAccountsToAdd, ...commissionAccountCollection];
    }
    return commissionAccountCollection;
  }
}
