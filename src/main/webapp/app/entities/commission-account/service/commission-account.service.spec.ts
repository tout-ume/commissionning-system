import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ICommissionAccount, CommissionAccount } from '../commission-account.model';

import { CommissionAccountService } from './commission-account.service';

describe('CommissionAccount Service', () => {
  let service: CommissionAccountService;
  let httpMock: HttpTestingController;
  let elemDefault: ICommissionAccount;
  let expectedResult: ICommissionAccount | ICommissionAccount[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(CommissionAccountService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      calculatedBalance: 0,
      paidBalance: 0,
      spare1: 'AAAAAAA',
      spare2: 'AAAAAAA',
      spare3: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a CommissionAccount', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new CommissionAccount()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a CommissionAccount', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          calculatedBalance: 1,
          paidBalance: 1,
          spare1: 'BBBBBB',
          spare2: 'BBBBBB',
          spare3: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a CommissionAccount', () => {
      const patchObject = Object.assign(
        {
          spare1: 'BBBBBB',
        },
        new CommissionAccount()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of CommissionAccount', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          calculatedBalance: 1,
          paidBalance: 1,
          spare1: 'BBBBBB',
          spare2: 'BBBBBB',
          spare3: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a CommissionAccount', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addCommissionAccountToCollectionIfMissing', () => {
      it('should add a CommissionAccount to an empty array', () => {
        const commissionAccount: ICommissionAccount = { id: 123 };
        expectedResult = service.addCommissionAccountToCollectionIfMissing([], commissionAccount);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(commissionAccount);
      });

      it('should not add a CommissionAccount to an array that contains it', () => {
        const commissionAccount: ICommissionAccount = { id: 123 };
        const commissionAccountCollection: ICommissionAccount[] = [
          {
            ...commissionAccount,
          },
          { id: 456 },
        ];
        expectedResult = service.addCommissionAccountToCollectionIfMissing(commissionAccountCollection, commissionAccount);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a CommissionAccount to an array that doesn't contain it", () => {
        const commissionAccount: ICommissionAccount = { id: 123 };
        const commissionAccountCollection: ICommissionAccount[] = [{ id: 456 }];
        expectedResult = service.addCommissionAccountToCollectionIfMissing(commissionAccountCollection, commissionAccount);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(commissionAccount);
      });

      it('should add only unique CommissionAccount to an array', () => {
        const commissionAccountArray: ICommissionAccount[] = [{ id: 123 }, { id: 456 }, { id: 39370 }];
        const commissionAccountCollection: ICommissionAccount[] = [{ id: 123 }];
        expectedResult = service.addCommissionAccountToCollectionIfMissing(commissionAccountCollection, ...commissionAccountArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const commissionAccount: ICommissionAccount = { id: 123 };
        const commissionAccount2: ICommissionAccount = { id: 456 };
        expectedResult = service.addCommissionAccountToCollectionIfMissing([], commissionAccount, commissionAccount2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(commissionAccount);
        expect(expectedResult).toContain(commissionAccount2);
      });

      it('should accept null and undefined values', () => {
        const commissionAccount: ICommissionAccount = { id: 123 };
        expectedResult = service.addCommissionAccountToCollectionIfMissing([], null, commissionAccount, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(commissionAccount);
      });

      it('should return initial array if no CommissionAccount is added', () => {
        const commissionAccountCollection: ICommissionAccount[] = [{ id: 123 }];
        expectedResult = service.addCommissionAccountToCollectionIfMissing(commissionAccountCollection, undefined, null);
        expect(expectedResult).toEqual(commissionAccountCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
