import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { CommissioningPlanComponent } from './list/commissioning-plan.component';
import { CommissioningPlanDetailComponent } from './detail/commissioning-plan-detail.component';
import { CommissioningPlanUpdateComponent } from './update/commissioning-plan-update.component';
import { CommissioningPlanDeleteDialogComponent } from './delete/commissioning-plan-delete-dialog.component';
import { CommissioningPlanRoutingModule } from './route/commissioning-plan-routing.module';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { PeriodModule } from '../period/period.module';
import { ConfigurationPlanModule } from '../configuration-plan/configuration-plan.module';
import { CommissioningPlanStopComponent } from './stop/commissioning-plan-stop/commissioning-plan-stop.component';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [SharedModule, CommissioningPlanRoutingModule, MatSlideToggleModule, PeriodModule, ConfigurationPlanModule, NgxLoadingModule],
  declarations: [
    CommissioningPlanComponent,
    CommissioningPlanDetailComponent,
    CommissioningPlanUpdateComponent,
    CommissioningPlanDeleteDialogComponent,
    CommissioningPlanStopComponent,
  ],
  entryComponents: [CommissioningPlanDeleteDialogComponent],
  exports: [CommissioningPlanComponent],
})
export class CommissioningPlanModule {}
