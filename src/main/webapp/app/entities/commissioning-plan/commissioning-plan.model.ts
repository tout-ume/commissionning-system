import dayjs from 'dayjs/esm';
import { IFrequency } from 'app/entities/frequency/frequency.model';
import { IPeriod } from 'app/entities/period/period.model';
import { IServiceType } from 'app/entities/service-type/service-type.model';
import { IConfigurationPlan } from 'app/entities/configuration-plan/configuration-plan.model';
import { PaymentStrategy } from 'app/entities/enumerations/payment-strategy.model';
import { CommissioningPlanState } from 'app/entities/enumerations/commissioning-plan-state.model';
import { CommissioningPlanType } from 'app/entities/enumerations/commissioning-plan-type.model';

export interface ICommissioningPlan {
  id?: number;
  name?: string;
  beginDate?: dayjs.Dayjs;
  endDate?: dayjs.Dayjs;
  creationDate?: dayjs.Dayjs;
  createdBy?: string;
  archiveDate?: dayjs.Dayjs | null;
  archivedBy?: string | null;
  paymentStrategy?: PaymentStrategy | null;
  state?: CommissioningPlanState;
  commissioningPlanType?: CommissioningPlanType | null;
  spare1?: string | null;
  spare2?: string | null;
  spare3?: string | null;
  paymentFrequency?: IFrequency | null;
  calculusFrequency?: IFrequency | null;
  calculusPeriod?: IPeriod | null;
  paymentPeriod?: IPeriod | null;
  services?: IServiceType[] | null;
  configurationPlans?: IConfigurationPlan[] | null;
}

export class CommissioningPlan implements ICommissioningPlan {
  constructor(
    public id?: number,
    public name?: string,
    public beginDate?: dayjs.Dayjs,
    public endDate?: dayjs.Dayjs,
    public creationDate?: dayjs.Dayjs,
    public createdBy?: string,
    public archiveDate?: dayjs.Dayjs | null,
    public archivedBy?: string | null,
    public paymentStrategy?: PaymentStrategy | null,
    public state?: CommissioningPlanState,
    public commissioningPlanType?: CommissioningPlanType | null,
    public spare1?: string | null,
    public spare2?: string | null,
    public spare3?: string | null,
    public paymentFrequency?: IFrequency | null,
    public calculusFrequency?: IFrequency | null,
    public calculusPeriod?: IPeriod | null,
    public paymentPeriod?: IPeriod | null,
    public services?: IServiceType[] | null,
    public configurationPlans?: IConfigurationPlan[] | null
  ) {}
}

export function getCommissioningPlanIdentifier(commissioningPlan: ICommissioningPlan): number | undefined {
  return commissioningPlan.id;
}
