import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommissioningPlanStopComponent } from './commissioning-plan-stop.component';

describe('CommissioningPlanStopComponent', () => {
  let component: CommissioningPlanStopComponent;
  let fixture: ComponentFixture<CommissioningPlanStopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CommissioningPlanStopComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommissioningPlanStopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
