import { Component, OnInit } from '@angular/core';
import { CommissioningPlanService } from '../../service/commissioning-plan.service';
import { ICommissioningPlan } from '../../commissioning-plan.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigurationPlanService } from '../../../configuration-plan/service/configuration-plan.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import dayjs from 'dayjs/esm';
import { DATE_FORMAT } from '../../../../config/input.constants';

@Component({
  selector: 'jhi-commissioning-plan-stop',
  templateUrl: './commissioning-plan-stop.component.html',
  styleUrls: ['./commissioning-plan-stop.component.scss'],
})
export class CommissioningPlanStopComponent implements OnInit {
  commissioningPlan: ICommissioningPlan | null = null;
  today = dayjs().startOf('day');

  stopForm = this.fb.group({
    id: [],
    endDate: [null, [Validators.required]],
  });

  isSaving = false;

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected configurationPlanService: ConfigurationPlanService,
    protected fb: FormBuilder,
    protected commissioningPlanService: CommissioningPlanService,
    protected router: Router
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ commissioningPlan }) => {
      this.commissioningPlan = commissioningPlan;
      this.stopForm.patchValue({
        endDate: this.today.format(DATE_FORMAT),
      });
    });
  }

  previousState(): void {
    window.history.back();
  }

  handleStopCommissioningPlan(commissioningPlan: ICommissioningPlan): void {
    if (commissioningPlan.id !== undefined) {
      commissioningPlan.endDate = this.stopForm.value.endDate;
      this.subscribeToSaveResponse(this.commissioningPlanService.stop(commissioningPlan));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICommissioningPlan>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    // this.previousState();
    this.router.navigate(['commissioning-plan']);
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }
}
