import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { PaymentStrategy } from 'app/entities/enumerations/payment-strategy.model';
import { CommissioningPlanState } from 'app/entities/enumerations/commissioning-plan-state.model';
import { CommissioningPlanType } from 'app/entities/enumerations/commissioning-plan-type.model';
import { ICommissioningPlan, CommissioningPlan } from '../commissioning-plan.model';

import { CommissioningPlanService } from './commissioning-plan.service';

describe('CommissioningPlan Service', () => {
  let service: CommissioningPlanService;
  let httpMock: HttpTestingController;
  let elemDefault: ICommissioningPlan;
  let expectedResult: ICommissioningPlan | ICommissioningPlan[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(CommissioningPlanService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      name: 'AAAAAAA',
      beginDate: currentDate,
      endDate: currentDate,
      creationDate: currentDate,
      createdBy: 'AAAAAAA',
      archiveDate: currentDate,
      archivedBy: 'AAAAAAA',
      paymentStrategy: PaymentStrategy.AUTOMATIC,
      state: CommissioningPlanState.CREATED,
      commissioningPlanType: CommissioningPlanType.MFS,
      spare1: 'AAAAAAA',
      spare2: 'AAAAAAA',
      spare3: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          beginDate: currentDate.format(DATE_TIME_FORMAT),
          endDate: currentDate.format(DATE_TIME_FORMAT),
          creationDate: currentDate.format(DATE_TIME_FORMAT),
          archiveDate: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a CommissioningPlan', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          beginDate: currentDate.format(DATE_TIME_FORMAT),
          endDate: currentDate.format(DATE_TIME_FORMAT),
          creationDate: currentDate.format(DATE_TIME_FORMAT),
          archiveDate: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          beginDate: currentDate,
          endDate: currentDate,
          creationDate: currentDate,
          archiveDate: currentDate,
        },
        returnedFromService
      );

      service.create(new CommissioningPlan()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a CommissioningPlan', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          beginDate: currentDate.format(DATE_TIME_FORMAT),
          endDate: currentDate.format(DATE_TIME_FORMAT),
          creationDate: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          archiveDate: currentDate.format(DATE_TIME_FORMAT),
          archivedBy: 'BBBBBB',
          paymentStrategy: 'BBBBBB',
          state: 'BBBBBB',
          commissioningPlanType: 'BBBBBB',
          spare1: 'BBBBBB',
          spare2: 'BBBBBB',
          spare3: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          beginDate: currentDate,
          endDate: currentDate,
          creationDate: currentDate,
          archiveDate: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a CommissioningPlan', () => {
      const patchObject = Object.assign(
        {
          name: 'BBBBBB',
          endDate: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          archiveDate: currentDate.format(DATE_TIME_FORMAT),
          paymentStrategy: 'BBBBBB',
          state: 'BBBBBB',
          spare1: 'BBBBBB',
        },
        new CommissioningPlan()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          beginDate: currentDate,
          endDate: currentDate,
          creationDate: currentDate,
          archiveDate: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of CommissioningPlan', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          beginDate: currentDate.format(DATE_TIME_FORMAT),
          endDate: currentDate.format(DATE_TIME_FORMAT),
          creationDate: currentDate.format(DATE_TIME_FORMAT),
          createdBy: 'BBBBBB',
          archiveDate: currentDate.format(DATE_TIME_FORMAT),
          archivedBy: 'BBBBBB',
          paymentStrategy: 'BBBBBB',
          state: 'BBBBBB',
          commissioningPlanType: 'BBBBBB',
          spare1: 'BBBBBB',
          spare2: 'BBBBBB',
          spare3: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          beginDate: currentDate,
          endDate: currentDate,
          creationDate: currentDate,
          archiveDate: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a CommissioningPlan', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addCommissioningPlanToCollectionIfMissing', () => {
      it('should add a CommissioningPlan to an empty array', () => {
        const commissioningPlan: ICommissioningPlan = { id: 123 };
        expectedResult = service.addCommissioningPlanToCollectionIfMissing([], commissioningPlan);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(commissioningPlan);
      });

      it('should not add a CommissioningPlan to an array that contains it', () => {
        const commissioningPlan: ICommissioningPlan = { id: 123 };
        const commissioningPlanCollection: ICommissioningPlan[] = [
          {
            ...commissioningPlan,
          },
          { id: 456 },
        ];
        expectedResult = service.addCommissioningPlanToCollectionIfMissing(commissioningPlanCollection, commissioningPlan);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a CommissioningPlan to an array that doesn't contain it", () => {
        const commissioningPlan: ICommissioningPlan = { id: 123 };
        const commissioningPlanCollection: ICommissioningPlan[] = [{ id: 456 }];
        expectedResult = service.addCommissioningPlanToCollectionIfMissing(commissioningPlanCollection, commissioningPlan);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(commissioningPlan);
      });

      it('should add only unique CommissioningPlan to an array', () => {
        const commissioningPlanArray: ICommissioningPlan[] = [{ id: 123 }, { id: 456 }, { id: 52319 }];
        const commissioningPlanCollection: ICommissioningPlan[] = [{ id: 123 }];
        expectedResult = service.addCommissioningPlanToCollectionIfMissing(commissioningPlanCollection, ...commissioningPlanArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const commissioningPlan: ICommissioningPlan = { id: 123 };
        const commissioningPlan2: ICommissioningPlan = { id: 456 };
        expectedResult = service.addCommissioningPlanToCollectionIfMissing([], commissioningPlan, commissioningPlan2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(commissioningPlan);
        expect(expectedResult).toContain(commissioningPlan2);
      });

      it('should accept null and undefined values', () => {
        const commissioningPlan: ICommissioningPlan = { id: 123 };
        expectedResult = service.addCommissioningPlanToCollectionIfMissing([], null, commissioningPlan, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(commissioningPlan);
      });

      it('should return initial array if no CommissioningPlan is added', () => {
        const commissioningPlanCollection: ICommissioningPlan[] = [{ id: 123 }];
        expectedResult = service.addCommissioningPlanToCollectionIfMissing(commissioningPlanCollection, undefined, null);
        expect(expectedResult).toEqual(commissioningPlanCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
