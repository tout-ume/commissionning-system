import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICommissioningPlan, getCommissioningPlanIdentifier } from '../commissioning-plan.model';

export type EntityResponseType = HttpResponse<ICommissioningPlan>;
export type EntityArrayResponseType = HttpResponse<ICommissioningPlan[]>;

@Injectable({ providedIn: 'root' })
export class CommissioningPlanService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/commissioning-plans');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  getPlansToPayToday(): Observable<EntityArrayResponseType> {
    return this.http
      .get<ICommissioningPlan[]>(this.resourceUrl + '/to-pay-today', { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  create(commissioningPlan: ICommissioningPlan): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(commissioningPlan);
    return this.http
      .post<ICommissioningPlan>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(commissioningPlan: ICommissioningPlan): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(commissioningPlan);
    return this.http
      .put<ICommissioningPlan>(`${this.resourceUrl}/${getCommissioningPlanIdentifier(commissioningPlan) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  stop(commissioningPlan: ICommissioningPlan): Observable<EntityResponseType> {
    const copy = this.convertDateFromClientFront(commissioningPlan);
    return this.http
      .put<ICommissioningPlan>(`${this.resourceUrl}/stop/${getCommissioningPlanIdentifier(commissioningPlan) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(commissioningPlan: ICommissioningPlan): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(commissioningPlan);
    return this.http
      .patch<ICommissioningPlan>(`${this.resourceUrl}/${getCommissioningPlanIdentifier(commissioningPlan) as number}`, copy, {
        observe: 'response',
      })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ICommissioningPlan>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  findByPartner(msisdn: string): Observable<any> {
    return this.http.get(`${this.resourceUrl}/by-partner?msisdn=${msisdn}`, { observe: 'response' });
  }

  findBlockedForPartner(msisdn: string): Observable<any> {
    return this.http.get(`${this.resourceUrl}/blocked-for/${msisdn}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICommissioningPlan[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCommissioningPlanToCollectionIfMissing(
    commissioningPlanCollection: ICommissioningPlan[],
    ...commissioningPlansToCheck: (ICommissioningPlan | null | undefined)[]
  ): ICommissioningPlan[] {
    const commissioningPlans: ICommissioningPlan[] = commissioningPlansToCheck.filter(isPresent);
    if (commissioningPlans.length > 0) {
      const commissioningPlanCollectionIdentifiers = commissioningPlanCollection.map(
        commissioningPlanItem => getCommissioningPlanIdentifier(commissioningPlanItem)!
      );
      const commissioningPlansToAdd = commissioningPlans.filter(commissioningPlanItem => {
        const commissioningPlanIdentifier = getCommissioningPlanIdentifier(commissioningPlanItem);
        if (commissioningPlanIdentifier == null || commissioningPlanCollectionIdentifiers.includes(commissioningPlanIdentifier)) {
          return false;
        }
        commissioningPlanCollectionIdentifiers.push(commissioningPlanIdentifier);
        return true;
      });
      return [...commissioningPlansToAdd, ...commissioningPlanCollection];
    }
    return commissioningPlanCollection;
  }

  protected convertDateFromClient(commissioningPlan: ICommissioningPlan): ICommissioningPlan {
    return Object.assign({}, commissioningPlan, {
      beginDate: commissioningPlan.beginDate?.isValid() ? commissioningPlan.beginDate.toJSON() : undefined,
      endDate: commissioningPlan.endDate?.isValid() ? commissioningPlan.endDate.toJSON() : undefined,
      creationDate: commissioningPlan.creationDate?.isValid() ? commissioningPlan.creationDate.toJSON() : undefined,
      archiveDate: commissioningPlan.archiveDate?.isValid() ? commissioningPlan.archiveDate.toJSON() : undefined,
    });
  }

  protected convertDateFromClientFront(commissioningPlan: ICommissioningPlan): ICommissioningPlan {
    return Object.assign({}, commissioningPlan, {
      beginDate: commissioningPlan.beginDate ? dayjs(commissioningPlan.beginDate) : undefined,
      endDate: commissioningPlan.endDate ? dayjs(commissioningPlan.endDate) : undefined,
      creationDate: commissioningPlan.creationDate ? dayjs(commissioningPlan.creationDate) : undefined,
      archiveDate: commissioningPlan.archiveDate ? dayjs(commissioningPlan.archiveDate) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.beginDate = res.body.beginDate ? dayjs(res.body.beginDate) : undefined;
      res.body.endDate = res.body.endDate ? dayjs(res.body.endDate) : undefined;
      res.body.creationDate = res.body.creationDate ? dayjs(res.body.creationDate) : undefined;
      res.body.archiveDate = res.body.archiveDate ? dayjs(res.body.archiveDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((commissioningPlan: ICommissioningPlan) => {
        commissioningPlan.beginDate = commissioningPlan.beginDate ? dayjs(commissioningPlan.beginDate) : undefined;
        commissioningPlan.endDate = commissioningPlan.endDate ? dayjs(commissioningPlan.endDate) : undefined;
        commissioningPlan.creationDate = commissioningPlan.creationDate ? dayjs(commissioningPlan.creationDate) : undefined;
        commissioningPlan.archiveDate = commissioningPlan.archiveDate ? dayjs(commissioningPlan.archiveDate) : undefined;
      });
    }
    return res;
  }
}
