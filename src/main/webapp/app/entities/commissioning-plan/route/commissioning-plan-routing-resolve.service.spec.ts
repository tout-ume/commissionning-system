import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { ICommissioningPlan, CommissioningPlan } from '../commissioning-plan.model';
import { CommissioningPlanService } from '../service/commissioning-plan.service';

import { CommissioningPlanRoutingResolveService } from './commissioning-plan-routing-resolve.service';

describe('CommissioningPlan routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: CommissioningPlanRoutingResolveService;
  let service: CommissioningPlanService;
  let resultCommissioningPlan: ICommissioningPlan | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(CommissioningPlanRoutingResolveService);
    service = TestBed.inject(CommissioningPlanService);
    resultCommissioningPlan = undefined;
  });

  describe('resolve', () => {
    it('should return ICommissioningPlan returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCommissioningPlan = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultCommissioningPlan).toEqual({ id: 123 });
    });

    it('should return new ICommissioningPlan if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCommissioningPlan = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultCommissioningPlan).toEqual(new CommissioningPlan());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as CommissioningPlan })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultCommissioningPlan = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultCommissioningPlan).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
