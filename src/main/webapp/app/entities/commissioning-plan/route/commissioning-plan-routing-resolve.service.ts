import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICommissioningPlan, CommissioningPlan } from '../commissioning-plan.model';
import { CommissioningPlanService } from '../service/commissioning-plan.service';

@Injectable({ providedIn: 'root' })
export class CommissioningPlanRoutingResolveService implements Resolve<ICommissioningPlan> {
  constructor(protected service: CommissioningPlanService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICommissioningPlan> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((commissioningPlan: HttpResponse<CommissioningPlan>) => {
          if (commissioningPlan.body) {
            return of(commissioningPlan.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new CommissioningPlan());
  }
}
