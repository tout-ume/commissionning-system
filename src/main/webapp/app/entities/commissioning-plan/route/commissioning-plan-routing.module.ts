import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CommissioningPlanComponent } from '../list/commissioning-plan.component';
import { CommissioningPlanDetailComponent } from '../detail/commissioning-plan-detail.component';
import { CommissioningPlanUpdateComponent } from '../update/commissioning-plan-update.component';
import { CommissioningPlanRoutingResolveService } from './commissioning-plan-routing-resolve.service';
import { CommissioningPlanStopComponent } from '../stop/commissioning-plan-stop/commissioning-plan-stop.component';

const commissioningPlanRoute: Routes = [
  {
    path: '',
    component: CommissioningPlanComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CommissioningPlanDetailComponent,
    resolve: {
      commissioningPlan: CommissioningPlanRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/stop',
    component: CommissioningPlanStopComponent,
    resolve: {
      commissioningPlan: CommissioningPlanRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CommissioningPlanUpdateComponent,
    resolve: {
      commissioningPlan: CommissioningPlanRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CommissioningPlanUpdateComponent,
    resolve: {
      commissioningPlan: CommissioningPlanRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(commissioningPlanRoute)],
  exports: [RouterModule],
})
export class CommissioningPlanRoutingModule {}
