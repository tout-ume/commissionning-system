import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICommissioningPlan } from '../commissioning-plan.model';
import { CommissioningPlanService } from '../service/commissioning-plan.service';

@Component({
  templateUrl: './commissioning-plan-delete-dialog.component.html',
})
export class CommissioningPlanDeleteDialogComponent {
  commissioningPlan?: ICommissioningPlan;

  constructor(protected commissioningPlanService: CommissioningPlanService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.commissioningPlanService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
