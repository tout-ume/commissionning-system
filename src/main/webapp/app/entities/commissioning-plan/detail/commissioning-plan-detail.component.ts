import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICommissioningPlan } from '../commissioning-plan.model';
import { ConfigurationPlanService } from '../../configuration-plan/service/configuration-plan.service';

@Component({
  selector: 'jhi-commissioning-plan-detail',
  templateUrl: './commissioning-plan-detail.component.html',
})
export class CommissioningPlanDetailComponent implements OnInit {
  commissioningPlan: ICommissioningPlan | null = null;
  isLoading = false;

  constructor(protected activatedRoute: ActivatedRoute, protected configurationPlanService: ConfigurationPlanService) {}

  ngOnInit(): void {
    this.isLoading = true;
    this.activatedRoute.data.subscribe(({ commissioningPlan }) => {
      this.commissioningPlan = commissioningPlan;
      if (this.commissioningPlan != null) {
        this.configurationPlanService
          .query({
            'commissioningPlanId.equals': this.commissioningPlan.id,
          })
          .subscribe(
            configurations => {
              this.commissioningPlan!.configurationPlans = configurations.body;
              this.isLoading = false;
            },
            () => {
              this.isLoading = false;
            }
          );
      }
    });
  }

  previousState(): void {
    window.history.back();
  }

  /**
   * Récupère le libellé du statut d'un plan de commission
   *
   * @param {string} state - Le code du statut.
   * @returns {string} - Le libellé du statut.
   */
  getPlanStateLabel(state: string | undefined): string {
    switch (state) {
      case 'IN_USE':
        return 'Actif';
      case 'DRAFT':
        return 'Brouillon';
      case 'CREATED':
        return 'Inactif';
      case 'ARCHIVED':
        return 'Archivé';
      default:
        return '';
    }
  }
}
