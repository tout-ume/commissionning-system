import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CommissioningPlanDetailComponent } from './commissioning-plan-detail.component';

describe('CommissioningPlan Management Detail Component', () => {
  let comp: CommissioningPlanDetailComponent;
  let fixture: ComponentFixture<CommissioningPlanDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CommissioningPlanDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ commissioningPlan: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(CommissioningPlanDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(CommissioningPlanDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load commissioningPlan on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.commissioningPlan).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
