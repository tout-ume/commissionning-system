import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { CommissioningPlanService } from '../service/commissioning-plan.service';
import { ICommissioningPlan, CommissioningPlan } from '../commissioning-plan.model';
import { IFrequency } from 'app/entities/frequency/frequency.model';
import { FrequencyService } from 'app/entities/frequency/service/frequency.service';
import { IPeriod } from 'app/entities/period/period.model';
import { PeriodService } from 'app/entities/period/service/period.service';
import { IServiceType } from 'app/entities/service-type/service-type.model';
import { ServiceTypeService } from 'app/entities/service-type/service/service-type.service';

import { CommissioningPlanUpdateComponent } from './commissioning-plan-update.component';

describe('CommissioningPlan Management Update Component', () => {
  let comp: CommissioningPlanUpdateComponent;
  let fixture: ComponentFixture<CommissioningPlanUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let commissioningPlanService: CommissioningPlanService;
  let frequencyService: FrequencyService;
  let periodService: PeriodService;
  let serviceTypeService: ServiceTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [CommissioningPlanUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CommissioningPlanUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CommissioningPlanUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    commissioningPlanService = TestBed.inject(CommissioningPlanService);
    frequencyService = TestBed.inject(FrequencyService);
    periodService = TestBed.inject(PeriodService);
    serviceTypeService = TestBed.inject(ServiceTypeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Frequency query and add missing value', () => {
      const commissioningPlan: ICommissioningPlan = { id: 456 };
      const paymentFrequency: IFrequency = { id: 6193 };
      commissioningPlan.paymentFrequency = paymentFrequency;
      const calculusFrequency: IFrequency = { id: 38401 };
      commissioningPlan.calculusFrequency = calculusFrequency;

      const frequencyCollection: IFrequency[] = [{ id: 26535 }];
      jest.spyOn(frequencyService, 'query').mockReturnValue(of(new HttpResponse({ body: frequencyCollection })));
      const additionalFrequencies = [paymentFrequency, calculusFrequency];
      const expectedCollection: IFrequency[] = [...additionalFrequencies, ...frequencyCollection];
      jest.spyOn(frequencyService, 'addFrequencyToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ commissioningPlan });
      comp.ngOnInit();

      expect(frequencyService.query).toHaveBeenCalled();
      expect(frequencyService.addFrequencyToCollectionIfMissing).toHaveBeenCalledWith(frequencyCollection, ...additionalFrequencies);
      expect(comp.frequenciesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Period query and add missing value', () => {
      const commissioningPlan: ICommissioningPlan = { id: 456 };
      const calculusPeriod: IPeriod = { id: 67953 };
      commissioningPlan.calculusPeriod = calculusPeriod;
      const paymentPeriod: IPeriod = { id: 75926 };
      commissioningPlan.paymentPeriod = paymentPeriod;

      const periodCollection: IPeriod[] = [{ id: 32464 }];
      jest.spyOn(periodService, 'query').mockReturnValue(of(new HttpResponse({ body: periodCollection })));
      const additionalPeriods = [calculusPeriod, paymentPeriod];
      const expectedCollection: IPeriod[] = [...additionalPeriods, ...periodCollection];
      jest.spyOn(periodService, 'addPeriodToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ commissioningPlan });
      comp.ngOnInit();

      expect(periodService.query).toHaveBeenCalled();
      expect(periodService.addPeriodToCollectionIfMissing).toHaveBeenCalledWith(periodCollection, ...additionalPeriods);
      expect(comp.periodsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call ServiceType query and add missing value', () => {
      const commissioningPlan: ICommissioningPlan = { id: 456 };
      const services: IServiceType[] = [{ id: 65871 }];
      commissioningPlan.services = services;

      const serviceTypeCollection: IServiceType[] = [{ id: 43633 }];
      jest.spyOn(serviceTypeService, 'query').mockReturnValue(of(new HttpResponse({ body: serviceTypeCollection })));
      const additionalServiceTypes = [...services];
      const expectedCollection: IServiceType[] = [...additionalServiceTypes, ...serviceTypeCollection];
      jest.spyOn(serviceTypeService, 'addServiceTypeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ commissioningPlan });
      comp.ngOnInit();

      expect(serviceTypeService.query).toHaveBeenCalled();
      expect(serviceTypeService.addServiceTypeToCollectionIfMissing).toHaveBeenCalledWith(serviceTypeCollection, ...additionalServiceTypes);
      expect(comp.serviceTypesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const commissioningPlan: ICommissioningPlan = { id: 456 };
      const paymentFrequency: IFrequency = { id: 30604 };
      commissioningPlan.paymentFrequency = paymentFrequency;
      const calculusFrequency: IFrequency = { id: 21237 };
      commissioningPlan.calculusFrequency = calculusFrequency;
      const calculusPeriod: IPeriod = { id: 46330 };
      commissioningPlan.calculusPeriod = calculusPeriod;
      const paymentPeriod: IPeriod = { id: 19304 };
      commissioningPlan.paymentPeriod = paymentPeriod;
      const services: IServiceType = { id: 56190 };
      commissioningPlan.services = [services];

      activatedRoute.data = of({ commissioningPlan });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(commissioningPlan));
      expect(comp.frequenciesSharedCollection).toContain(paymentFrequency);
      expect(comp.frequenciesSharedCollection).toContain(calculusFrequency);
      expect(comp.periodsSharedCollection).toContain(calculusPeriod);
      expect(comp.periodsSharedCollection).toContain(paymentPeriod);
      expect(comp.serviceTypesSharedCollection).toContain(services);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CommissioningPlan>>();
      const commissioningPlan = { id: 123 };
      jest.spyOn(commissioningPlanService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ commissioningPlan });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: commissioningPlan }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(commissioningPlanService.update).toHaveBeenCalledWith(commissioningPlan);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CommissioningPlan>>();
      const commissioningPlan = new CommissioningPlan();
      jest.spyOn(commissioningPlanService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ commissioningPlan });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: commissioningPlan }));
      saveSubject.complete();

      // THEN
      expect(commissioningPlanService.create).toHaveBeenCalledWith(commissioningPlan);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<CommissioningPlan>>();
      const commissioningPlan = { id: 123 };
      jest.spyOn(commissioningPlanService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ commissioningPlan });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(commissioningPlanService.update).toHaveBeenCalledWith(commissioningPlan);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackFrequencyById', () => {
      it('Should return tracked Frequency primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackFrequencyById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackPeriodById', () => {
      it('Should return tracked Period primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackPeriodById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackServiceTypeById', () => {
      it('Should return tracked ServiceType primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackServiceTypeById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });

  describe('Getting selected relationships', () => {
    describe('getSelectedServiceType', () => {
      it('Should return option if no ServiceType is selected', () => {
        const option = { id: 123 };
        const result = comp.getSelectedServiceType(option);
        expect(result === option).toEqual(true);
      });

      it('Should return selected ServiceType for according option', () => {
        const option = { id: 123 };
        const selected = { id: 123 };
        const selected2 = { id: 456 };
        const result = comp.getSelectedServiceType(option, [selected2, selected]);
        expect(result === selected).toEqual(true);
        expect(result === selected2).toEqual(false);
        expect(result === option).toEqual(false);
      });

      it('Should return option if this ServiceType is not selected', () => {
        const option = { id: 123 };
        const selected = { id: 456 };
        const result = comp.getSelectedServiceType(option, [selected]);
        expect(result === option).toEqual(true);
        expect(result === selected).toEqual(false);
      });
    });
  });
});
