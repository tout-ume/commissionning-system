import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { AbstractControl, FormBuilder, ValidatorFn, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_FORMAT, DATE_TIME_FORMAT } from 'app/config/input.constants';

import { CommissioningPlan, ICommissioningPlan } from '../commissioning-plan.model';
import { CommissioningPlanService } from '../service/commissioning-plan.service';
import { IServiceType } from 'app/entities/service-type/service-type.model';
import { ServiceTypeService } from 'app/entities/service-type/service/service-type.service';
import { CommissioningPlanState } from 'app/entities/enumerations/commissioning-plan-state.model';
import { IPartnerProfile } from '../../partner-profile/partner-profile.model';
import { Frequency, IFrequency } from '../../frequency/frequency.model';
import { FrequencyService } from '../../frequency/service/frequency.service';
import { PartnerProfileService } from '../../partner-profile/service/partner-profile.service';
import { ConfigurationPlan, IConfigurationPlan } from '../../configuration-plan/configuration-plan.model';
import { IZone } from '../../zone/zone.model';
import { ZoneService } from '../../zone/service/zone.service';
import { ITerritory } from '../../territory/territory.model';
import { TerritoryService } from '../../territory/service/territory.service';
import { AccountService } from '../../../core/auth/account.service';
import { ConfigurationPlanService } from '../../configuration-plan/service/configuration-plan.service';
import { CommissionType, ICommissionType } from '../../commission-type/commission-type.model';
import { CommissionTypeType } from '../../enumerations/commission-type-type';
import { PalierValueType } from '../../enumerations/palier-value-type';
import { IPeriod, Period } from '../../period/period.model';
import { OperationType } from '../../enumerations/operation-type.model';
import { FrequencyType } from '../../enumerations/frequency-type.model';

@Component({
  selector: 'jhi-commissioning-plan-update',
  templateUrl: './commissioning-plan-update.component.html',
})
export class CommissioningPlanUpdateComponent implements OnInit {
  isSaving = false;
  serviceTypesSharedCollection: IServiceType[] = [];
  partnerProfilesSharedCollection: IPartnerProfile[] = [];
  zonesSharedCollection: IZone[] = [];
  territorySharedCollection: ITerritory[] = [];
  frequenciesSharedCollection: IFrequency[] = [];
  calculusFrequenciesSharedCollection: IFrequency[] = [];
  paymentFrequenciesSharedCollection: IFrequency[] = [];
  configurationPlans: IConfigurationPlan[] = [];
  configurationPlansToAdd: ConfigurationPlan[] = [];
  connectedUser: string | undefined;
  availablePartnerProfiles: IPartnerProfile[] = [];
  selectedServiceTypes: IServiceType[] = [];
  periodPaymentToShow = 'NONE';
  frequencyPaymentToShow = 'NONE';
  calculusPeriodToShow = 'NONE';
  calculusPeriod: IPeriod = new Period();
  paymentPeriod: IPeriod = new Period();

  paymentFrequency: any = new Frequency();

  weekDays: any[] = [
    { id: 1, label: 'Lundi' },
    { id: 2, label: 'Mardi' },
    { id: 3, label: 'Mercredi' },
    { id: 4, label: 'Jeudi' },
    { id: 5, label: 'Vendredi' },
    { id: 6, label: 'Samedi' },
    { id: 7, label: 'Dimanche' },
  ];

  paymentStrategy: any[] = [
    { id: 1, mode: 'AUTOMATIC', label: 'Automatique' },
    { id: 2, mode: 'SCHEDULED', label: 'Différé' },
    { id: 3, mode: 'MANUAL', label: 'Manuel' },
  ];

  planType: any[] = [
    { id: 1, type: 'MFS', label: 'MFS' },
    { id: 2, type: 'MOBILE', label: 'Mobile' },
  ];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    calculusFrequency: [null, [Validators.required]],
    paymentMode: [null, [Validators.required]],
    planType: [this.planType[0], [Validators.required]],
    beginDate: [null, [Validators.required, this.fromToDate('beginDate', 'endDate')]],
    endDate: [null, [this.fromToDate('beginDate', 'endDate')]],
  });

  configurationPlanForm = this.fb.group({});

  constructor(
    protected commissioningPlanService: CommissioningPlanService,
    protected configurationPlanService: ConfigurationPlanService,
    protected frequencyService: FrequencyService,
    protected partnerProfileService: PartnerProfileService,
    protected serviceTypeService: ServiceTypeService,
    protected zoneService: ZoneService,
    protected territoryService: TerritoryService,
    protected activatedRoute: ActivatedRoute,
    protected accountService: AccountService,
    protected fb: FormBuilder,
    protected router: Router
  ) {}

  fromToDate(fromDateField: string, toDateField: string, errorName = 'fromToDate'): ValidatorFn {
    return (formGroup: AbstractControl): { [key: string]: boolean } | null => {
      const fromDate = formGroup.get(fromDateField)?.value;
      const toDate = formGroup.get(toDateField)?.value;
      if (fromDate !== null && toDate !== null && fromDate > toDate) {
        return { [errorName]: true };
      }
      return null;
    };
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ commissioningPlan }) => {
      if (commissioningPlan.id === undefined) {
        const today = dayjs().startOf('day');
        commissioningPlan.beginDate = today;
        commissioningPlan.endDate = dayjs().set('year', 2079).set('month', 11).set('date', 31);
        commissioningPlan.creationDate = today;
        commissioningPlan.archiveDate = today;
      }

      this.updateForm(commissioningPlan);

      this.loadRelationshipsOptions();
    });
    this.addConfigurationLine();
    this.setAvailablePartnerProfiles();
    this.accountService.getAuthenticationState().subscribe(account => {
      if (account) {
        this.connectedUser = account.login;
      }
    });
  }

  removeAllPeriodsControls(): void {
    this.editForm.removeControl('dayHourFrom');
    this.editForm.removeControl('dayHourTo');
    this.editForm.removeControl('weekDayFrom');
    this.editForm.removeControl('weekDayTo');
    this.editForm.removeControl('monthDayFrom');
    this.editForm.removeControl('monthDayTo');
  }

  removeAllPeriodsControlsOfPayment(): void {
    this.editForm.removeControl('dayHourFromOfPayment');
    this.editForm.removeControl('dayHourToOfPayment');
    this.editForm.removeControl('weekDayFromOfPayment');
    this.editForm.removeControl('weekDayToOfPayment');
    this.editForm.removeControl('monthDayFromOfPayment');
    this.editForm.removeControl('monthDayToOfPayment');
  }

  removeFrequencyPayment(): void {
    this.editForm.removeControl('paymentFrequency');
  }

  onFrequencyCalculusChange(): void {
    this.calculusPeriod = new Period();
    this.removeAllPeriodsControls();
    switch (this.editForm.get('calculusFrequency')?.value.type) {
      case 'DAILY':
        this.calculusPeriodToShow = 'DAILY';
        this.setDailyPeriodValidators();
        break;
      case 'WEEKLY':
        this.calculusPeriodToShow = 'WEEKLY';
        this.setWeeklyPeriodValidators();
        break;
      case 'MONTHLY':
        this.calculusPeriodToShow = 'MONTHLY';
        this.setMonthlyPeriodValidators();
        break;
      default:
        this.calculusPeriodToShow = 'NONE';
        break;
    }
  }

  onPaymentModeChange(): void {
    switch (this.editForm.get('paymentMode')?.value.mode) {
      case 'SCHEDULED':
        this.setFrequencyPayment();
        this.frequencyPaymentToShow = 'SCHEDULED';
        break;
      case 'MANUAL':
        this.removeFrequencyPayment();
        this.frequencyPaymentToShow = 'NONE';
        break;
      case 'AUTOMATIC':
        this.removeFrequencyPayment();
        this.frequencyPaymentToShow = 'NONE';
        break;
      default:
        break;
    }
  }

  onFrequencyPaymentChange(): void {
    this.paymentPeriod = new Period();
    this.removeAllPeriodsControlsOfPayment();
    switch (this.editForm.get('paymentFrequency')?.value.type) {
      case 'DAILY':
        this.periodPaymentToShow = 'DAILY';
        this.setDailyPeriodValidatorsOfPayment();
        break;
      case 'WEEKLY':
        this.periodPaymentToShow = 'WEEKLY';
        this.setWeeklyPeriodValidatorsOfPayment();
        break;
      case 'MONTHLY':
        this.periodPaymentToShow = 'MONTHLY';
        this.setMonthlyPeriodValidatorsOfPayment();
        break;
      default:
        this.periodPaymentToShow = 'NONE';
        break;
    }
  }

  setWeeklyPeriodValidators(): void {
    this.editForm.addControl('weekDayFrom', this.fb.control(null, [Validators.required]));
    this.editForm.addControl('weekDayTo', this.fb.control(null, [Validators.required]));
  }

  setWeeklyPeriodValidatorsOfPayment(): void {
    this.editForm.addControl('weekDayFromOfPayment', this.fb.control(null, [Validators.required]));
    this.editForm.addControl('weekDayToOfPayment', this.fb.control(null, [Validators.required]));
  }

  setMonthlyPeriodValidators(): void {
    this.editForm.addControl('monthDayFrom', this.fb.control(null, [Validators.required, Validators.min(0), Validators.max(28)]));
    this.editForm.addControl('monthDayTo', this.fb.control(null, [Validators.required, Validators.min(0), Validators.max(28)]));
  }

  setMonthlyPeriodValidatorsOfPayment(): void {
    this.editForm.addControl('monthDayFromOfPayment', this.fb.control(null, [Validators.required, Validators.min(0), Validators.max(28)]));
    this.editForm.addControl('monthDayToOfPayment', this.fb.control(null, [Validators.required, Validators.min(0), Validators.max(28)]));
  }

  setDailyPeriodValidators(): void {
    this.editForm.addControl('dayHourFrom', this.fb.control(null, [Validators.required, Validators.min(0), Validators.max(23)]));
    this.editForm.addControl('dayHourTo', this.fb.control(null, [Validators.required, Validators.min(0), Validators.max(23)]));
  }

  setDailyPeriodValidatorsOfPayment(): void {
    this.editForm.addControl('dayHourFromOfPayment', this.fb.control(null, [Validators.required, Validators.min(0), Validators.max(23)]));
    this.editForm.addControl('dayHourToOfPayment', this.fb.control(null, [Validators.required, Validators.min(0), Validators.max(23)]));
  }

  setFrequencyPayment(): void {
    this.editForm.addControl('paymentFrequency', this.fb.control(null, [Validators.required]));
  }

  toggleIsCompleteDay(): void {
    if (this.calculusPeriod.isCompleteDay) {
      this.removeAllPeriodsControls();
    } else {
      this.setDailyPeriodValidators();
    }
  }

  toggleIsLastDayOfTheMonth(condition: boolean, isCalculus: boolean): void {
    const index: number = isCalculus
      ? this.editForm.get('calculusFrequency')?.value.occurrenceByPeriod
      : this.editForm.get('paymentFrequency')?.value.occurrenceByPeriod;
    if (condition) {
      isCalculus
        ? this.editForm.removeControl(`calculusFrequencyOccurrence${index}`)
        : this.editForm.removeControl(`paymentFrequencyOccurrence${index}`);
    } else {
      isCalculus
        ? this.editForm.addControl(`calculusFrequencyOccurrence${index}`, this.fb.control(null, [Validators.required]))
        : this.editForm.addControl(`paymentFrequencyOccurrence${index}`, this.fb.control(null, [Validators.required]));
    }
  }

  toggleIsCompleteDayOfPayment(): void {
    if (this.paymentPeriod.isCompleteDay) {
      this.removeAllPeriodsControlsOfPayment();
    } else {
      this.setDailyPeriodValidatorsOfPayment();
    }
  }

  toggleIsCompleteMonth(): void {
    if (this.calculusPeriod.isCompleteMonth) {
      this.removeAllPeriodsControls();
    } else {
      this.setMonthlyPeriodValidators();
    }
  }

  toggleIsCompleteMonthOfPayment(): void {
    if (this.paymentPeriod.isCompleteMonth) {
      this.removeAllPeriodsControlsOfPayment();
    } else {
      this.setMonthlyPeriodValidatorsOfPayment();
    }
  }

  addOrRemoveZone(index: number, zone: IZone, shouldRemove: boolean | undefined): void {
    if (this.isZoneSelected(index, zone)) {
      if (shouldRemove) {
        // Remove the zone if it is in
        const zoneToRemoveIndex = this.configurationPlans[index].zones?.indexOf(zone);
        if (zoneToRemoveIndex !== undefined && zoneToRemoveIndex >= 0) {
          this.configurationPlans[index].zones?.splice(zoneToRemoveIndex, 1);
        }
      }
    } else {
      // add the zone if not
      this.configurationPlans[index].zones?.push(zone);
    }
  }

  flushSelectedZones(index: number): void {
    this.configurationPlans[index].areAllTerritoriesSelected = false;
    this.configurationPlans[index].zones = [];
  }

  isTerritorySelected(index: number, zones: IZone[] | undefined | null): boolean | undefined {
    let result = true;
    zones?.forEach(zone => {
      if (!this.isZoneSelected(index, zone)) {
        result = false;
      }
    });
    return result;
  }

  isZoneSelected(index: number, zone: IZone): boolean | undefined {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return this.configurationPlans[index].zones?.includes(zone);
  }

  isServiceSelected(service: IServiceType): boolean {
    return this.selectedServiceTypes.includes(service);
  }

  toggleTerritorySelection(index: number, zones: IZone[] | undefined | null): void {
    // if territory is selected it should remove all its zones
    const shouldRemove = this.isTerritorySelected(index, zones);
    zones?.forEach(zone => {
      this.addOrRemoveZone(index, zone, shouldRemove);
    });
  }

  toggleAllTerritoriesSelection(index: number, isSelecting: boolean | undefined): void {
    this.territorySharedCollection.forEach(territory => {
      if (isSelecting) {
        if (!this.isTerritorySelected(index, territory.zones)) {
          this.toggleTerritorySelection(index, territory.zones);
        }
      } else {
        if (this.isTerritorySelected(index, territory.zones)) {
          this.toggleTerritorySelection(index, territory.zones);
        }
      }
    });
  }

  selectProfile(index: number): void {
    this.configurationPlans[index].partnerProfile = this.editForm.get('partnerProfile' + index.toString())?.value;
  }

  addOrRemoveServiceType(serviceType: IServiceType): void {
    // Remove the serviceType
    if (this.isServiceSelected(serviceType)) {
      // Remove the serviceType if it is selected
      const serviceToRemoveIndex = this.selectedServiceTypes.indexOf(serviceType);
      if (serviceToRemoveIndex >= 0) {
        this.selectedServiceTypes.splice(serviceToRemoveIndex, 1);
      }
    } else {
      // add the serviceType, if not
      this.selectedServiceTypes.push(serviceType);
    }
  }

  completeSelectedPlansValues(): IConfigurationPlan[] {
    for (let i = 0; i <= this.configurationPlans.length - 1; i++) {
      if (this.configurationPlans[i].isSelected) {
        if (this.configurationPlans[i].commissionTypes?.length) {
          // Has at least one element
          this.configurationPlans[i].commissionTypes?.forEach(item => {
            item.commissionTypeType === CommissionTypeType.TAUX ? (item.tauxValue = this.editForm.get('taux' + i.toString())?.value) : null;
          });
        }
      }
    }
    return this.configurationPlans;
  }

  isPalierMaxValueDisabled(index: number, palierIndex: number): boolean {
    const palierTotal = this.configurationPlans[index].commissionTypes?.length;
    return palierTotal !== undefined && palierTotal > palierIndex + 1;
  }

  isPalierTheLast(index: number, palierIndex: number): boolean {
    const palierTotal = this.configurationPlans[index].commissionTypes?.length;
    return palierTotal !== undefined && palierTotal === palierIndex + 1;
  }

  isLastPalierToInfinity(index: number): boolean {
    const palierTotal = this.configurationPlans[index].commissionTypes?.length;
    let lastPalier;
    if (palierTotal !== undefined) {
      lastPalier = this.configurationPlans[index].commissionTypes?.[palierTotal - 1];
    }
    return lastPalier !== undefined && lastPalier.isInfinity === true;
  }

  toggleIsInfinity(palier: ICommissionType): void {
    if (palier.isInfinity) {
      palier.maxValue = null;
    }
  }

  addNewCommissionType(index: number, isPalier: boolean): void {
    const palierTotal = this.configurationPlans[index].commissionTypes?.length;
    let minValue: number | null | undefined = 0;
    if (palierTotal && palierTotal > 0) {
      minValue = this.configurationPlans[index].commissionTypes?.[palierTotal - 1].maxValue;
      this.configurationPlans[index].commissionTypes?.push(
        new CommissionType(undefined, null, minValue ? minValue + 1 : 0, null, null, CommissionTypeType.PALIER)
      );
    } else {
      // Very first element case
      this.configurationPlans[index].commissionTypes?.push(
        new CommissionType(undefined, null, isPalier ? 0 : null, null, null, isPalier ? CommissionTypeType.PALIER : CommissionTypeType.TAUX)
      );
    }
  }

  addConfigurationLine(): void {
    const newConfigurationPlan: IConfigurationPlan = new ConfigurationPlan();
    // Initializing zones
    newConfigurationPlan.zones = [];
    newConfigurationPlan.commissionTypes = [];
    newConfigurationPlan.isSelected = true;
    this.configurationPlans.push(newConfigurationPlan);
    // Add CommissionType for the new line
    this.addNewCommissionType(this.configurationPlans.length - 1, false);
    // Add formControls for the new line
    this.editForm.addControl(
      'partnerProfile' + (this.configurationPlans.length - 1).toString(),
      this.fb.control(null, [Validators.required])
    );
    this.editForm.addControl(
      'taux' + (this.configurationPlans.length - 1).toString(),
      this.fb.control(null, [Validators.required, Validators.max(100), Validators.min(0)])
    );
  }

  removeConfigurationLine(index: number): void {
    if (this.configurationPlans.length > 1) {
      this.configurationPlans[index].isSelected = false;
      this.editForm.get('partnerProfile' + index.toString())?.clearValidators();
      this.editForm.get('taux' + index.toString())?.clearValidators();
      this.editForm.get('partnerProfile' + index.toString())?.reset();
      this.editForm.get('taux' + index.toString())?.reset();
    }
  }

  togglePalierTaux(index: number): void {
    // Flush commissionTypes
    this.configurationPlans[index].commissionTypes = [];
    if (this.configurationPlans[index].usePalier) {
      this.addNewCommissionType(index, true);
      this.editForm.get('taux' + index.toString())?.clearValidators();
      this.editForm.get('taux' + index.toString())?.reset();
    } else {
      this.addNewCommissionType(index, false);
      this.editForm.addControl(
        'taux' + (this.configurationPlans.length - 1).toString(),
        this.fb.control(null, [Validators.required, Validators.max(100), Validators.min(0)])
      );
    }
  }

  isProfileSelectedForConfigLine(index: number): boolean {
    return this.editForm.get('partnerProfile' + index.toString())!.value !== null;
  }

  isPalierValid(palier: ICommissionType | undefined): boolean {
    return palier !== undefined
      ? palier.palierValue !== null &&
          palier.palierValue !== undefined &&
          palier.minValue !== null &&
          palier.minValue !== undefined &&
          ((!palier.isInfinity && palier.maxValue !== null && palier.maxValue !== undefined && palier.maxValue > palier.minValue) ||
            palier.isInfinity === true) &&
          ((!palier.isPalierFix && palier.palierValue > 0 && palier.palierValue <= 100) ||
            (palier.isPalierFix === true && palier.palierValue > 0))
      : false;
  }

  isLastConfigLineValid(): boolean {
    const lastItemIndex = this.getSelectedPlans().length;
    return this.isProfileSelectedForConfigLine(lastItemIndex - 1);
  }

  isLastPalierValid(index: number): boolean {
    const lastItemIndex = this.configurationPlans[index].commissionTypes?.length;
    return lastItemIndex !== undefined ? this.isPalierValid(this.configurationPlans[index].commissionTypes?.[lastItemIndex - 1]) : false;
  }

  isAnyPalierInvalid(): boolean {
    let result = false;
    for (const configurationPlan of this.getSelectedPlans()) {
      if (configurationPlan.usePalier && configurationPlan.commissionTypes) {
        if (configurationPlan.commissionTypes.length === 0) {
          result = true;
          break;
        } else {
          for (const palier of configurationPlan.commissionTypes) {
            if (!this.isPalierValid(palier)) {
              result = true;
              break;
            }
          }
          if (result) {
            break;
          }
        }
      } else if (configurationPlan.usePalier && !configurationPlan.commissionTypes) {
        // in case it uses palier but commissionTypes is null
        result = true;
        break;
      }
    }
    return result;
  }

  removePalierLine(configLineIndex: number, palierIndex: number): void {
    this.configurationPlans[configLineIndex].commissionTypes?.splice(palierIndex, 1);
  }

  getSelectedPlans(): IConfigurationPlan[] {
    return this.configurationPlans.filter(item => item.isSelected);
  }

  linesHaveZones(): boolean {
    let result = true;
    for (const configurationPlan of this.getSelectedPlans()) {
      if (configurationPlan.zones && configurationPlan.zones.length <= 0) {
        result = false;
        break;
      }
    }
    return result;
  }

  hasServices(): boolean {
    return this.selectedServiceTypes.length > 0;
  }

  setAvailablePartnerProfiles(): void {
    this.availablePartnerProfiles = this.partnerProfilesSharedCollection.filter(item => !this.hasCompleteZoneSelection(item));
  }

  addLastSelectedProfileToAvailableProfiles(): void {
    this.availablePartnerProfiles.push(this.editForm.get('partnerProfile' + (this.getSelectedPlans().length - 1).toString())?.value);
  }

  setAvailableTerritories(index: number): void {
    this.configurationPlans[index].availableTerritories = [];
    for (const territory of this.territorySharedCollection) {
      const tempTerritory = Object.assign([], territory);
      tempTerritory.zones = [];
      if (territory.zones) {
        for (const zone of territory.zones) {
          if (!this.hasBeenConfiguredForProfile(this.configurationPlans[index].partnerProfile, zone)) {
            tempTerritory.zones.push(zone);
          }
        }
        if (tempTerritory.zones.length > 0) {
          this.configurationPlans[index].availableTerritories?.push(tempTerritory);
        }
      }
    }
  }

  hasBeenConfiguredForProfile(profile: IPartnerProfile | null | undefined, zone: IZone): boolean {
    let result = false;
    for (const configurationLine of this.configurationPlans) {
      if (configurationLine.zones) {
        if (configurationLine.partnerProfile === profile) {
          if (configurationLine.zones.includes(zone)) {
            result = true;
            break;
          }
        }
      }
    }
    return result;
  }

  hasCompleteZoneSelection(profile: IPartnerProfile): boolean {
    let result = true;
    let found = false;
    let profileZones: any[] = [];
    const profileConfigurations = this.configurationPlans.filter(item => item.isSelected && item.partnerProfile === profile);
    found = profileConfigurations.length > 0;
    if (found) {
      for (const profileConfiguration of profileConfigurations) {
        profileZones = profileZones.concat(profileConfiguration.zones);
      }
      for (const zone of this.zonesSharedCollection) {
        if (!profileZones.includes(zone)) {
          result = false;
          break;
        }
      }
    }
    return found && result;
  }

  previousState(): void {
    window.history.back();
  }

  setPalierTypeValues(configPlans: IConfigurationPlan[]): void {
    configPlans.forEach(item => {
      if (item.commissionTypes) {
        item.commissionTypes.forEach(palier => {
          if (palier.commissionTypeType === CommissionTypeType.PALIER) {
            palier.palierValueType = palier.isPalierFix ? PalierValueType.FIXE : PalierValueType.TAUX;
          }
        });
      }
    });
  }

  save(): void {
    console.warn(this.editForm.get(['paymentMode'])!.value.mode === 'SCHEDULED');
    const commissioningPlan = this.createFromForm();
    commissioningPlan.configurationPlans = this.completeSelectedPlansValues().filter(item => item.isSelected);
    this.setPalierTypeValues(commissioningPlan.configurationPlans);
    if (this.editForm.get('calculusFrequency')?.value.type === FrequencyType.INSTANTLY) {
      commissioningPlan.calculusPeriod = null;
    } else {
      this.calculusPeriod.createdBy = commissioningPlan.createdBy;
      this.calculusPeriod.createdDate = commissioningPlan.creationDate;
      this.calculusPeriod.periodType = commissioningPlan.calculusFrequency?.type;
      this.calculusPeriod.operationType = OperationType.CALCULUS;
      this.calculusPeriod.dayHourFrom = this.editForm.get(['dayHourFrom'])?.value;
      this.calculusPeriod.dayHourTo = this.editForm.get(['dayHourTo'])?.value;
      this.calculusPeriod.monthDayFrom = this.editForm.get(['monthDayFrom'])?.value;
      this.calculusPeriod.monthDayTo = this.editForm.get(['monthDayTo'])?.value;
      this.calculusPeriod.weekDayFrom = this.editForm.get(['weekDayFrom'])?.value;
      this.calculusPeriod.weekDayTo = this.editForm.get(['weekDayTo'])?.value;
      commissioningPlan.calculusPeriod = this.calculusPeriod;
    }

    commissioningPlan.paymentFrequency =
      this.editForm.get(['paymentMode'])!.value.mode === 'SCHEDULED' ? this.editForm.get(['paymentFrequency'])!.value : null;

    this.paymentPeriod.createdBy = commissioningPlan.createdBy;
    this.paymentPeriod.createdDate = commissioningPlan.creationDate;
    this.paymentPeriod.periodType = commissioningPlan.paymentFrequency?.type;
    this.paymentPeriod.operationType = OperationType.PAYMENT;
    this.paymentPeriod.dayHourFrom = this.editForm.get(['dayHourFromOfPayment'])?.value;
    this.paymentPeriod.dayHourTo = this.editForm.get(['dayHourToOfPayment'])?.value;
    this.paymentPeriod.monthDayFrom = this.editForm.get(['monthDayFromOfPayment'])?.value;
    this.paymentPeriod.monthDayTo = this.editForm.get(['monthDayToOfPayment'])?.value;
    this.paymentPeriod.weekDayFrom = this.editForm.get(['weekDayFromOfPayment'])?.value;
    this.paymentPeriod.weekDayTo = this.editForm.get(['weekDayToOfPayment'])?.value;
    if (this.editForm.get('paymentFrequency')?.value.periodOfOccurrence) {
      this.paymentPeriod.daysOrDatesOfOccurrence = commissioningPlan.paymentFrequency?.datesOfOccurrence;
    }
    commissioningPlan.paymentPeriod = this.editForm.get(['paymentMode'])!.value.mode === 'SCHEDULED' ? this.paymentPeriod : null;

    this.isSaving = true;
    if (commissioningPlan.id !== undefined) {
      this.subscribeToSaveResponse(this.commissioningPlanService.update(commissioningPlan));
    } else {
      this.subscribeToSaveResponse(this.commissioningPlanService.create(commissioningPlan));
    }
  }

  trackServiceTypeById(_index: number, item: IServiceType): number {
    return item.id!;
  }

  trackFrequencyById(_index: number, item: IFrequency): number {
    return item.id!;
  }

  trackPartnerProfileById(_index: number, item: IPartnerProfile): number {
    return item.id!;
  }

  trackZoneById(_index: number, item: IZone): number {
    return item.id!;
  }

  trackTerritoryById(_index: number, item: ITerritory): number {
    return item.id!;
  }

  getSelectedServiceType(option: IServiceType, selectedVals?: IServiceType[]): IServiceType {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICommissioningPlan>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    // this.previousState();
    this.router.navigate(['commissioning-plan']);
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(commissioningPlan: ICommissioningPlan): void {
    this.editForm.patchValue({
      id: commissioningPlan.id,
      name: commissioningPlan.name,
      calculusFrequency: commissioningPlan.calculusFrequency,
      paymentMode: commissioningPlan.paymentStrategy,
      beginDate: commissioningPlan.beginDate ? commissioningPlan.beginDate.format(DATE_FORMAT) : null,
      endDate: commissioningPlan.endDate ? commissioningPlan.endDate.format(DATE_FORMAT) : null,
      services: commissioningPlan.services,
    });
  }

  protected loadRelationshipsOptions(): void {
    this.partnerProfileService
      .query()
      .pipe(map((res: HttpResponse<IPartnerProfile[]>) => res.body ?? []))
      .subscribe((partnerProfiles: IPartnerProfile[]) => {
        this.partnerProfilesSharedCollection = partnerProfiles;
        this.availablePartnerProfiles = partnerProfiles;
      });

    this.territoryService
      .query()
      .pipe(map((res: HttpResponse<ITerritory[]>) => res.body ?? []))
      .subscribe((territories: ITerritory[]) => {
        this.territorySharedCollection = territories;
        this.territorySharedCollection.map(territory => {
          if (territory.zones) {
            this.zonesSharedCollection = this.zonesSharedCollection.concat(territory.zones);
          }
        });
      });

    this.frequencyService
      .query({ 'operationType.equals': OperationType.CALCULUS })
      .pipe(map((res: HttpResponse<IFrequency[]>) => res.body ?? []))
      .subscribe((frequencies: IFrequency[]) => (this.calculusFrequenciesSharedCollection = frequencies));

    this.frequencyService
      .query({ 'operationType.equals': OperationType.PAYMENT })
      .pipe(map((res: HttpResponse<IFrequency[]>) => res.body ?? []))
      .subscribe((frequencies: IFrequency[]) => (this.paymentFrequenciesSharedCollection = frequencies));

    this.serviceTypeService
      .query()
      .pipe(map((res: HttpResponse<IServiceType[]>) => res.body ?? []))
      .subscribe((serviceTypes: IServiceType[]) => (this.serviceTypesSharedCollection = serviceTypes));
  }

  protected createFromForm(): ICommissioningPlan {
    return {
      ...new CommissioningPlan(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      calculusFrequency: this.editForm.get(['calculusFrequency'])!.value,
      paymentStrategy: this.editForm.get(['paymentMode'])!.value.mode,
      beginDate: this.editForm.get(['beginDate'])!.value
        ? dayjs(
            dayjs(this.editForm.get(['beginDate'])!.value)
              .set('hour', 0)
              .set('minute', 0)
              .set('second', 0),
            DATE_TIME_FORMAT
          )
        : undefined,
      endDate: this.editForm.get(['endDate'])!.value
        ? dayjs(
            dayjs(this.editForm.get(['endDate'])!.value)
              .set('hour', 23)
              .set('minute', 59)
              .set('second', 59),
            DATE_TIME_FORMAT
          )
        : dayjs(
            dayjs().set('year', 2079).set('month', 11).set('date', 31).set('hour', 23).set('minute', 59).set('second', 59),
            DATE_TIME_FORMAT
          ),
      creationDate: dayjs(dayjs().startOf('second'), DATE_TIME_FORMAT),
      createdBy: this.connectedUser,
      state: CommissioningPlanState.IN_USE,
      services: this.selectedServiceTypes,
      commissioningPlanType: this.editForm.get(['planType'])!.value.type,
    };
  }
}
