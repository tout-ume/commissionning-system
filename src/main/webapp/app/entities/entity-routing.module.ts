import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'territory',
        data: { pageTitle: 'Territories' },
        loadChildren: () => import('./territory/territory.module').then(m => m.TerritoryModule),
      },
      {
        path: 'zone',
        data: { pageTitle: 'Zones' },
        loadChildren: () => import('./zone/zone.module').then(m => m.ZoneModule),
      },
      {
        path: 'partner',
        data: { pageTitle: 'Partners' },
        loadChildren: () => import('./partner/partner.module').then(m => m.PartnerModule),
      },
      {
        path: 'commissioning-plan',
        data: { pageTitle: 'Commissioning - Liste des Plans' },
        loadChildren: () => import('./commissioning-plan/commissioning-plan.module').then(m => m.CommissioningPlanModule),
      },
      {
        path: 'service-type',
        data: { pageTitle: 'ServiceTypes' },
        loadChildren: () => import('./service-type/service-type.module').then(m => m.ServiceTypeModule),
      },
      {
        path: 'configuration-plan',
        data: { pageTitle: 'ConfigurationPlans' },
        loadChildren: () => import('./configuration-plan/configuration-plan.module').then(m => m.ConfigurationPlanModule),
      },
      {
        path: 'partner-profile',
        data: { pageTitle: 'PartnerProfiles' },
        loadChildren: () => import('./partner-profile/partner-profile.module').then(m => m.PartnerProfileModule),
      },
      {
        path: 'commission-type',
        data: { pageTitle: 'CommissionTypes' },
        loadChildren: () => import('./commission-type/commission-type.module').then(m => m.CommissionTypeModule),
      },
      {
        path: 'frequency',
        data: { pageTitle: 'Frequencies' },
        loadChildren: () => import('./frequency/frequency.module').then(m => m.FrequencyModule),
      },
      {
        path: 'commission',
        data: { pageTitle: 'Commissions' },
        loadChildren: () => import('./commission/commission.module').then(m => m.CommissionModule),
      },
      {
        path: 'commission-account',
        data: { pageTitle: 'CommissionAccounts' },
        loadChildren: () => import('./commission-account/commission-account.module').then(m => m.CommissionAccountModule),
      },
      {
        path: 'transaction-operation',
        data: { pageTitle: 'TransactionOperations' },
        loadChildren: () => import('./transaction-operation/transaction-operation.module').then(m => m.TransactionOperationModule),
      },
      {
        path: 'period',
        data: { pageTitle: 'Periods' },
        loadChildren: () => import('./period/period.module').then(m => m.PeriodModule),
      },
      {
        path: 'blacklisted',
        data: { pageTitle: 'Blacklisteds' },
        loadChildren: () => import('./blacklisted/blacklisted.module').then(m => m.BlacklistedModule),
      },
      {
        path: 'payment',
        data: { pageTitle: 'Payments' },
        loadChildren: () => import('./payment/payment.module').then(m => m.PaymentModule),
      },
      {
        path: 'role',
        data: { pageTitle: 'Rôles' },
        loadChildren: () => import('./role/role.module').then(m => m.RoleModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
