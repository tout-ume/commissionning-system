import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { BlacklistedDetailComponent } from './blacklisted-detail.component';

describe('Blacklisted Management Detail Component', () => {
  let comp: BlacklistedDetailComponent;
  let fixture: ComponentFixture<BlacklistedDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BlacklistedDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ blacklisted: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(BlacklistedDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(BlacklistedDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load blacklisted on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.blacklisted).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
