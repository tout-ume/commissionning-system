import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBlacklisted } from '../blacklisted.model';

@Component({
  selector: 'jhi-blacklisted-detail',
  templateUrl: './blacklisted-detail.component.html',
})
export class BlacklistedDetailComponent implements OnInit {
  blacklisted: IBlacklisted | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ blacklisted }) => {
      this.blacklisted = blacklisted;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
