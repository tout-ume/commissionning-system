import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { BlacklistedService } from '../service/blacklisted.service';
import { IBlacklisted, Blacklisted } from '../blacklisted.model';
import { ICommissioningPlan } from 'app/entities/commissioning-plan/commissioning-plan.model';
import { CommissioningPlanService } from 'app/entities/commissioning-plan/service/commissioning-plan.service';
import { IPartner } from 'app/entities/partner/partner.model';
import { PartnerService } from 'app/entities/partner/service/partner.service';

import { BlacklistedUpdateComponent } from './blacklisted-update.component';

describe('Blacklisted Management Update Component', () => {
  let comp: BlacklistedUpdateComponent;
  let fixture: ComponentFixture<BlacklistedUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let blacklistedService: BlacklistedService;
  let commissioningPlanService: CommissioningPlanService;
  let partnerService: PartnerService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [BlacklistedUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(BlacklistedUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(BlacklistedUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    blacklistedService = TestBed.inject(BlacklistedService);
    commissioningPlanService = TestBed.inject(CommissioningPlanService);
    partnerService = TestBed.inject(PartnerService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call CommissioningPlan query and add missing value', () => {
      const blacklisted: IBlacklisted = { id: 456 };
      const commissioningPlan: ICommissioningPlan = { id: 87238 };
      blacklisted.commissioningPlan = commissioningPlan;

      const commissioningPlanCollection: ICommissioningPlan[] = [{ id: 12297 }];
      jest.spyOn(commissioningPlanService, 'query').mockReturnValue(of(new HttpResponse({ body: commissioningPlanCollection })));
      const additionalCommissioningPlans = [commissioningPlan];
      const expectedCollection: ICommissioningPlan[] = [...additionalCommissioningPlans, ...commissioningPlanCollection];
      jest.spyOn(commissioningPlanService, 'addCommissioningPlanToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ blacklisted });
      comp.ngOnInit();

      expect(commissioningPlanService.query).toHaveBeenCalled();
      expect(commissioningPlanService.addCommissioningPlanToCollectionIfMissing).toHaveBeenCalledWith(
        commissioningPlanCollection,
        ...additionalCommissioningPlans
      );
      expect(comp.commissioningPlansSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Partner query and add missing value', () => {
      const blacklisted: IBlacklisted = { id: 456 };
      const partner: IPartner = { id: 10002 };
      blacklisted.partner = partner;

      const partnerCollection: IPartner[] = [{ id: 88803 }];
      jest.spyOn(partnerService, 'query').mockReturnValue(of(new HttpResponse({ body: partnerCollection })));
      const additionalPartners = [partner];
      const expectedCollection: IPartner[] = [...additionalPartners, ...partnerCollection];
      jest.spyOn(partnerService, 'addPartnerToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ blacklisted });
      comp.ngOnInit();

      expect(partnerService.query).toHaveBeenCalled();
      expect(partnerService.addPartnerToCollectionIfMissing).toHaveBeenCalledWith(partnerCollection, ...additionalPartners);
      expect(comp.partnersSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const blacklisted: IBlacklisted = { id: 456 };
      const commissioningPlan: ICommissioningPlan = { id: 11440 };
      blacklisted.commissioningPlan = commissioningPlan;
      const partner: IPartner = { id: 33524 };
      blacklisted.partner = partner;

      activatedRoute.data = of({ blacklisted });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(blacklisted));
      expect(comp.commissioningPlansSharedCollection).toContain(commissioningPlan);
      expect(comp.partnersSharedCollection).toContain(partner);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Blacklisted>>();
      const blacklisted = { id: 123 };
      jest.spyOn(blacklistedService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ blacklisted });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: blacklisted }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(blacklistedService.update).toHaveBeenCalledWith(blacklisted);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Blacklisted>>();
      const blacklisted = new Blacklisted();
      jest.spyOn(blacklistedService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ blacklisted });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: blacklisted }));
      saveSubject.complete();

      // THEN
      expect(blacklistedService.create).toHaveBeenCalledWith(blacklisted);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Blacklisted>>();
      const blacklisted = { id: 123 };
      jest.spyOn(blacklistedService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ blacklisted });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(blacklistedService.update).toHaveBeenCalledWith(blacklisted);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackCommissioningPlanById', () => {
      it('Should return tracked CommissioningPlan primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCommissioningPlanById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackPartnerById', () => {
      it('Should return tracked Partner primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackPartnerById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
