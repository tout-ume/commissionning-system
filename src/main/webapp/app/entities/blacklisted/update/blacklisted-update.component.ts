import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder, Validators } from '@angular/forms';
import { BlacklistedService } from '../service/blacklisted.service';
import { CommissioningPlan, ICommissioningPlan } from 'app/entities/commissioning-plan/commissioning-plan.model';
import { CommissioningPlanService } from 'app/entities/commissioning-plan/service/commissioning-plan.service';
import Swal from 'sweetalert2';
import { Blacklisted } from '../blacklisted.model';
import { finalize } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'jhi-blacklisted-update',
  templateUrl: './blacklisted-update.component.html',
})
export class BlacklistedUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    msisdn: [null, [Validators.required]],
    reason: [null, [Validators.required]],
    csv_file: [null, [Validators.required]],
  });
  fileName = '';
  commissioningPlans: CommissioningPlan[] | null = [];
  selectedCommissioningPlans: CommissioningPlan[] = [];
  uploadHasError = false;
  msisdnHasError = false;
  uploadErrorMessage = '';
  msisdnErrorMessage = '';
  locationState: any = null;
  isBlockingList = false;
  isUnblocking = false;
  showCommPlanList = false;
  reasons = ['Raison A', 'Raison B', 'Raison C', 'Raison D'];
  pageTitle = '';

  constructor(
    protected blacklistedService: BlacklistedService,
    protected activatedRoute: ActivatedRoute,
    protected commissioningPlanService: CommissioningPlanService,
    protected fb: FormBuilder,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.locationState = this.location.getState();
    // In case of reload state won't be provided, rely on path
    if (this.locationState?.blockingList === undefined || this.locationState?.unblocking) {
      switch (this.location.path()) {
        case '/blacklisted/unblock-one-partner':
          this.isUnblocking = true;
          this.isBlockingList = false;
          break;
        case '/blacklisted/block-partner-list':
          this.isUnblocking = false;
          this.isBlockingList = true;
          break;
        case '/blacklisted/block-one-partner':
          this.isUnblocking = false;
          this.isBlockingList = false;
          break;
        default:
          break;
      }
    } else {
      this.isBlockingList = this.locationState?.blockingList;
      this.isUnblocking = this.locationState?.unblocking;
    }
    console.warn('*********************** isBlockingList ', this.isBlockingList);
    console.warn('*********************** isUnblocking ', this.isUnblocking);
    if (this.isBlockingList) {
      this.pageTitle = 'Bloquer des partenaires';
      this.editForm.removeControl('msisdn');
      this.editForm.removeControl('reason');
      this.commissioningPlanService
        .query({
          'state.equals': 'IN_USE',
        })
        .subscribe(commissioningPlans => {
          this.commissioningPlans = commissioningPlans.body;
          this.showCommPlanList = true;
        });
    } else {
      this.pageTitle = 'Bloquer un partenaire';
      this.editForm.removeControl('csv_file');
      if (this.isUnblocking) {
        this.pageTitle = 'Débloquer un partenaire';
        this.editForm.removeControl('reason');
      }
    }
  }

  previousState(): void {
    window.history.back();
  }

  onMsisdnChange(event: any): void {
    this.msisdnHasError = false;
    this.msisdnErrorMessage = '';
    this.showCommPlanList = false;
    const msisdn = event.target.value;
    console.warn('*********************** Input value ', msisdn);
    if (msisdn.length === 9) {
      this.isUnblocking
        ? this.commissioningPlanService.findBlockedForPartner(event.target.value).subscribe(
            commissioningPlans => {
              this.commissioningPlans = commissioningPlans.body;
              this.showCommPlanList = true;
            },
            error => {
              this.msisdnHasError = true;
              this.msisdnErrorMessage = error.error.message;
            }
          )
        : this.commissioningPlanService.findByPartner(event.target.value).subscribe(
            commissioningPlans => {
              if (!commissioningPlans.body.isEmpty) {
                this.commissioningPlans = commissioningPlans.body.content;
                this.showCommPlanList = true;
              }
            },
            error => {
              this.msisdnHasError = true;
              this.msisdnErrorMessage = error.error.message;
            }
          );
    }
  }

  onFileSelected(event: any): void {
    this.uploadHasError = false;
    this.uploadErrorMessage = '';
    const file: File = event.target.files[0];
    this.fileName = file.name;
    const formData = new FormData();
    formData.append('file', file);
    this.blacklistedService.uploadPartnersToBlock(formData).subscribe(
      result1 => {
        console.warn('*********************$', result1);
      },
      error => {
        this.editForm.reset();
        this.uploadHasError = true;
        this.uploadErrorMessage = error.error.message;
      }
    );
  }

  save(): void {
    this.isSaving = true;
    if (this.isBlockingList) {
      this.blacklistedService
        .sendCommissionPlansToBlock(this.selectedCommissioningPlans)
        .pipe(finalize(() => this.onSaveFinalize()))
        .subscribe(
          result => {
            console.warn('*********************$ body', result.body);
            if (result.body.status === 'SUCCESS') {
              Swal.fire({
                title: 'Succès!',
                text: 'Les partenaires ont été bloqués.',
                icon: 'success',
                confirmButtonText: 'Okay',
              });
            } else {
              Swal.fire({
                title: 'Attention!',
                text: result.body.message,
                icon: 'info',
                confirmButtonText: 'Okay',
              });
              this.selectedCommissioningPlans = [];
            }
            this.editForm.reset();
            console.warn('********************* selectedCommissions: ', this.selectedCommissioningPlans);
          },
          error => {
            console.warn('********************* Error: ', error);
            Swal.fire({
              title: 'Erreur!',
              text: 'Une erreur est survenue, veuillez réessayer.',
              icon: 'error',
              confirmButtonText: 'Okay',
            });
            this.editForm.reset();
            this.showCommPlanList = false;
            this.selectedCommissioningPlans = [];
          }
        );
    } else {
      this.isUnblocking
        ? this.blacklistedService
            .sendCommissionPlansToUnblockAndMsisdn(this.selectedCommissioningPlans, this.editForm.get('msisdn')?.value)
            .pipe(finalize(() => this.onSaveFinalize()))
            .subscribe(
              result => {
                console.warn('*********************$ body', result.body);
                Swal.fire({
                  title: result.body.status === 'SUCCESS' ? 'Succès!' : 'Erreur',
                  text: result.body.message,
                  icon: result.body.status === 'SUCCESS' ? 'success' : 'error',
                  confirmButtonText: 'Okay',
                });
                this.selectedCommissioningPlans = [];
                this.showCommPlanList = false;
                this.editForm.reset();
                console.warn('********************* selectedCommissions: ', this.selectedCommissioningPlans);
              },
              error => {
                console.warn('********************* Error: ', error);
                Swal.fire({
                  title: 'Erreur!',
                  text: 'Une erreur est survenue, veuillez réessayer.',
                  icon: 'error',
                  confirmButtonText: 'Okay',
                });
                this.editForm.reset();
                this.selectedCommissioningPlans = [];
              }
            )
        : this.blacklistedService
            .sendCommissionPlansToBlockAndMsisdn(
              this.selectedCommissioningPlans,
              this.editForm.get('msisdn')?.value,
              this.editForm.get('reason')?.value
            )
            .pipe(finalize(() => this.onSaveFinalize()))
            .subscribe(
              result => {
                console.warn('*********************$ body', result.body);
                if ((result.body as Array<Blacklisted>).length !== 0) {
                  Swal.fire({
                    title: 'Succès!',
                    text: 'Le partenaire a été bloqué.',
                    icon: 'success',
                    confirmButtonText: 'Okay',
                  });
                  this.selectedCommissioningPlans = [];
                  this.showCommPlanList = false;
                  this.editForm.reset();
                } else {
                  Swal.fire({
                    title: 'Attention!',
                    text: 'Ce partenaire est déjà bloqué pour ces plans de commission.',
                    icon: 'info',
                    confirmButtonText: 'Okay',
                  });
                }
                console.warn('********************* selectedCommissions: ', this.selectedCommissioningPlans);
              },
              error => {
                console.warn('********************* Error: ', error);
                Swal.fire({
                  title: 'Erreur!',
                  text: 'Une erreur est survenue, veuillez réessayer.',
                  icon: 'error',
                  confirmButtonText: 'Okay',
                });
                this.editForm.reset();
                this.selectedCommissioningPlans = [];
              }
            );
    }
  }

  isCommissioningPlanSelected(commissioningPlan: ICommissioningPlan): boolean {
    return this.selectedCommissioningPlans.includes(commissioningPlan);
  }

  addOrRemoveCommissioningPlan(commissioningPlan: ICommissioningPlan): void {
    // Remove the commissioningPlan
    if (this.isCommissioningPlanSelected(commissioningPlan)) {
      // Remove the commissioningPlan if it is selected
      const commissioningPlanToRemoveIndex = this.selectedCommissioningPlans.indexOf(commissioningPlan);
      if (commissioningPlanToRemoveIndex >= 0) {
        this.selectedCommissioningPlans.splice(commissioningPlanToRemoveIndex, 1);
      }
    } else {
      // add the commissioningPlan, if not
      this.selectedCommissioningPlans.push(commissioningPlan);
    }
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }
}
