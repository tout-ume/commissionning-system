import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IBlacklisted, Blacklisted } from '../blacklisted.model';
import { BlacklistedService } from '../service/blacklisted.service';

@Injectable({ providedIn: 'root' })
export class BlacklistedRoutingResolveService implements Resolve<IBlacklisted> {
  constructor(protected service: BlacklistedService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBlacklisted> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((blacklisted: HttpResponse<Blacklisted>) => {
          if (blacklisted.body) {
            return of(blacklisted.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Blacklisted());
  }
}
