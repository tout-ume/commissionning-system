import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { IBlacklisted, Blacklisted } from '../blacklisted.model';
import { BlacklistedService } from '../service/blacklisted.service';

import { BlacklistedRoutingResolveService } from './blacklisted-routing-resolve.service';

describe('Blacklisted routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: BlacklistedRoutingResolveService;
  let service: BlacklistedService;
  let resultBlacklisted: IBlacklisted | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(BlacklistedRoutingResolveService);
    service = TestBed.inject(BlacklistedService);
    resultBlacklisted = undefined;
  });

  describe('resolve', () => {
    it('should return IBlacklisted returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultBlacklisted = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultBlacklisted).toEqual({ id: 123 });
    });

    it('should return new IBlacklisted if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultBlacklisted = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultBlacklisted).toEqual(new Blacklisted());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Blacklisted })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultBlacklisted = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultBlacklisted).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
