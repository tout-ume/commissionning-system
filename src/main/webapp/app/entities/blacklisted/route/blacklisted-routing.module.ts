import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { BlacklistedComponent } from '../list/blacklisted.component';
import { BlacklistedDetailComponent } from '../detail/blacklisted-detail.component';
import { BlacklistedUpdateComponent } from '../update/blacklisted-update.component';
import { BlacklistedRoutingResolveService } from './blacklisted-routing-resolve.service';

const blacklistedRoute: Routes = [
  {
    path: '',
    component: BlacklistedComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BlacklistedDetailComponent,
    resolve: {
      blacklisted: BlacklistedRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'block-partner-list',
    component: BlacklistedUpdateComponent,
    resolve: {
      blacklisted: BlacklistedRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'block-one-partner',
    component: BlacklistedUpdateComponent,
    resolve: {
      blacklisted: BlacklistedRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'unblock-one-partner',
    component: BlacklistedUpdateComponent,
    resolve: {
      blacklisted: BlacklistedRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BlacklistedUpdateComponent,
    resolve: {
      blacklisted: BlacklistedRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(blacklistedRoute)],
  exports: [RouterModule],
})
export class BlacklistedRoutingModule {}
