import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { BlacklistedComponent } from './list/blacklisted.component';
import { BlacklistedDetailComponent } from './detail/blacklisted-detail.component';
import { BlacklistedUpdateComponent } from './update/blacklisted-update.component';
import { BlacklistedDeleteDialogComponent } from './delete/blacklisted-delete-dialog.component';
import { BlacklistedRoutingModule } from './route/blacklisted-routing.module';
import { NgxLoadingModule } from 'ngx-loading';

@NgModule({
  imports: [SharedModule, BlacklistedRoutingModule, NgxLoadingModule],
  declarations: [BlacklistedComponent, BlacklistedDetailComponent, BlacklistedUpdateComponent, BlacklistedDeleteDialogComponent],
  entryComponents: [BlacklistedDeleteDialogComponent],
})
export class BlacklistedModule {}
