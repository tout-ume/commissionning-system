import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IBlacklisted } from '../blacklisted.model';
import { BlacklistedService } from '../service/blacklisted.service';

@Component({
  templateUrl: './blacklisted-delete-dialog.component.html',
})
export class BlacklistedDeleteDialogComponent {
  blacklisted?: IBlacklisted;

  constructor(protected blacklistedService: BlacklistedService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.blacklistedService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
