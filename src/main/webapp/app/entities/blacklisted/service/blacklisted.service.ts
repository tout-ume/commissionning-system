import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IBlacklisted, getBlacklistedIdentifier } from '../blacklisted.model';
import { ICommissioningPlan } from '../../commissioning-plan/commissioning-plan.model';

export type EntityResponseType = HttpResponse<IBlacklisted>;
export type EntityArrayResponseType = HttpResponse<IBlacklisted[]>;

@Injectable({ providedIn: 'root' })
export class BlacklistedService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/blacklists');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  uploadPartnersToBlock(csvFile: FormData): Observable<EntityResponseType> {
    console.warn('********** csvFile : ', csvFile);
    return this.http.post(`${this.resourceUrl}/upload-partners-to-block`, csvFile, { observe: 'response' });
  }

  sendCommissionPlansToBlock(commissioningPlans: ICommissioningPlan[]): Observable<HttpResponse<any>> {
    console.warn('********** commissioningPlans : ', commissioningPlans);
    return this.http.post(`${this.resourceUrl}/block-list`, commissioningPlans, { observe: 'response' });
  }

  sendCommissionPlansToBlockAndMsisdn(
    commissioningPlans: ICommissioningPlan[],
    msisdn: string,
    reason: string
  ): Observable<HttpResponse<any>> {
    console.warn('********** commissioningPlans : ', commissioningPlans);
    console.warn('********** msisdn : ', msisdn);
    console.warn('********** reason : ', reason);
    return this.http.post(`${this.resourceUrl}/block-one?msisdn=${msisdn}&reason=${reason}`, commissioningPlans, { observe: 'response' });
  }

  sendCommissionPlansToUnblockAndMsisdn(commissioningPlans: ICommissioningPlan[], msisdn: string): Observable<HttpResponse<any>> {
    console.warn('********** commissioningPlans : ', commissioningPlans);
    console.warn('********** msisdn : ', msisdn);
    return this.http.post(`${this.resourceUrl}/unblock-one?msisdn=${msisdn}`, commissioningPlans, { observe: 'response' });
  }

  create(blacklisted: IBlacklisted): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(blacklisted);
    return this.http
      .post<IBlacklisted>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(blacklisted: IBlacklisted): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(blacklisted);
    return this.http
      .put<IBlacklisted>(`${this.resourceUrl}/${getBlacklistedIdentifier(blacklisted) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(blacklisted: IBlacklisted): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(blacklisted);
    return this.http
      .patch<IBlacklisted>(`${this.resourceUrl}/${getBlacklistedIdentifier(blacklisted) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IBlacklisted>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBlacklisted[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addBlacklistedToCollectionIfMissing(
    blacklistedCollection: IBlacklisted[],
    ...blacklistedsToCheck: (IBlacklisted | null | undefined)[]
  ): IBlacklisted[] {
    const blacklisteds: IBlacklisted[] = blacklistedsToCheck.filter(isPresent);
    if (blacklisteds.length > 0) {
      const blacklistedCollectionIdentifiers = blacklistedCollection.map(blacklistedItem => getBlacklistedIdentifier(blacklistedItem)!);
      const blacklistedsToAdd = blacklisteds.filter(blacklistedItem => {
        const blacklistedIdentifier = getBlacklistedIdentifier(blacklistedItem);
        if (blacklistedIdentifier == null || blacklistedCollectionIdentifiers.includes(blacklistedIdentifier)) {
          return false;
        }
        blacklistedCollectionIdentifiers.push(blacklistedIdentifier);
        return true;
      });
      return [...blacklistedsToAdd, ...blacklistedCollection];
    }
    return blacklistedCollection;
  }

  protected convertDateFromClient(blacklisted: IBlacklisted): IBlacklisted {
    return Object.assign({}, blacklisted, {
      createdDate: blacklisted.createdDate?.isValid() ? blacklisted.createdDate.toJSON() : undefined,
      lastModifiedDate: blacklisted.lastModifiedDate?.isValid() ? blacklisted.lastModifiedDate.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdDate = res.body.createdDate ? dayjs(res.body.createdDate) : undefined;
      res.body.lastModifiedDate = res.body.lastModifiedDate ? dayjs(res.body.lastModifiedDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((blacklisted: IBlacklisted) => {
        blacklisted.createdDate = blacklisted.createdDate ? dayjs(blacklisted.createdDate) : undefined;
        blacklisted.lastModifiedDate = blacklisted.lastModifiedDate ? dayjs(blacklisted.lastModifiedDate) : undefined;
      });
    }
    return res;
  }
}
