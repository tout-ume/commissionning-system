import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IBlacklisted, Blacklisted } from '../blacklisted.model';

import { BlacklistedService } from './blacklisted.service';

describe('Blacklisted Service', () => {
  let service: BlacklistedService;
  let httpMock: HttpTestingController;
  let elemDefault: IBlacklisted;
  let expectedResult: IBlacklisted | IBlacklisted[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(BlacklistedService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      reason: 'AAAAAAA',
      blocked: false,
      createdBy: 'AAAAAAA',
      createdDate: currentDate,
      lastModifiedBy: 'AAAAAAA',
      lastModifiedDate: currentDate,
      spare1: 'AAAAAAA',
      spare2: 'AAAAAAA',
      spare3: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          createdDate: currentDate.format(DATE_TIME_FORMAT),
          lastModifiedDate: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Blacklisted', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          createdDate: currentDate.format(DATE_TIME_FORMAT),
          lastModifiedDate: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdDate: currentDate,
          lastModifiedDate: currentDate,
        },
        returnedFromService
      );

      service.create(new Blacklisted()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Blacklisted', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          reason: 'BBBBBB',
          blocked: true,
          createdBy: 'BBBBBB',
          createdDate: currentDate.format(DATE_TIME_FORMAT),
          lastModifiedBy: 'BBBBBB',
          lastModifiedDate: currentDate.format(DATE_TIME_FORMAT),
          spare1: 'BBBBBB',
          spare2: 'BBBBBB',
          spare3: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdDate: currentDate,
          lastModifiedDate: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Blacklisted', () => {
      const patchObject = Object.assign(
        {
          reason: 'BBBBBB',
          createdBy: 'BBBBBB',
        },
        new Blacklisted()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          createdDate: currentDate,
          lastModifiedDate: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Blacklisted', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          reason: 'BBBBBB',
          blocked: true,
          createdBy: 'BBBBBB',
          createdDate: currentDate.format(DATE_TIME_FORMAT),
          lastModifiedBy: 'BBBBBB',
          lastModifiedDate: currentDate.format(DATE_TIME_FORMAT),
          spare1: 'BBBBBB',
          spare2: 'BBBBBB',
          spare3: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdDate: currentDate,
          lastModifiedDate: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Blacklisted', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addBlacklistedToCollectionIfMissing', () => {
      it('should add a Blacklisted to an empty array', () => {
        const blacklisted: IBlacklisted = { id: 123 };
        expectedResult = service.addBlacklistedToCollectionIfMissing([], blacklisted);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(blacklisted);
      });

      it('should not add a Blacklisted to an array that contains it', () => {
        const blacklisted: IBlacklisted = { id: 123 };
        const blacklistedCollection: IBlacklisted[] = [
          {
            ...blacklisted,
          },
          { id: 456 },
        ];
        expectedResult = service.addBlacklistedToCollectionIfMissing(blacklistedCollection, blacklisted);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Blacklisted to an array that doesn't contain it", () => {
        const blacklisted: IBlacklisted = { id: 123 };
        const blacklistedCollection: IBlacklisted[] = [{ id: 456 }];
        expectedResult = service.addBlacklistedToCollectionIfMissing(blacklistedCollection, blacklisted);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(blacklisted);
      });

      it('should add only unique Blacklisted to an array', () => {
        const blacklistedArray: IBlacklisted[] = [{ id: 123 }, { id: 456 }, { id: 31818 }];
        const blacklistedCollection: IBlacklisted[] = [{ id: 123 }];
        expectedResult = service.addBlacklistedToCollectionIfMissing(blacklistedCollection, ...blacklistedArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const blacklisted: IBlacklisted = { id: 123 };
        const blacklisted2: IBlacklisted = { id: 456 };
        expectedResult = service.addBlacklistedToCollectionIfMissing([], blacklisted, blacklisted2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(blacklisted);
        expect(expectedResult).toContain(blacklisted2);
      });

      it('should accept null and undefined values', () => {
        const blacklisted: IBlacklisted = { id: 123 };
        expectedResult = service.addBlacklistedToCollectionIfMissing([], null, blacklisted, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(blacklisted);
      });

      it('should return initial array if no Blacklisted is added', () => {
        const blacklistedCollection: IBlacklisted[] = [{ id: 123 }];
        expectedResult = service.addBlacklistedToCollectionIfMissing(blacklistedCollection, undefined, null);
        expect(expectedResult).toEqual(blacklistedCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
