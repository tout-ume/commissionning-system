import dayjs from 'dayjs/esm';
import { ICommissioningPlan } from 'app/entities/commissioning-plan/commissioning-plan.model';
import { IPartner } from 'app/entities/partner/partner.model';

export interface IBlacklisted {
  id?: number;
  reason?: string | null;
  blocked?: boolean | null;
  createdBy?: string;
  createdDate?: dayjs.Dayjs | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: dayjs.Dayjs | null;
  spare1?: string | null;
  spare2?: string | null;
  spare3?: string | null;
  commissioningPlan?: ICommissioningPlan | null;
  partner?: IPartner | null;
}

export class Blacklisted implements IBlacklisted {
  constructor(
    public id?: number,
    public reason?: string | null,
    public blocked?: boolean | null,
    public createdBy?: string,
    public createdDate?: dayjs.Dayjs | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: dayjs.Dayjs | null,
    public spare1?: string | null,
    public spare2?: string | null,
    public spare3?: string | null,
    public commissioningPlan?: ICommissioningPlan | null,
    public partner?: IPartner | null
  ) {
    this.blocked = this.blocked ?? false;
  }
}

export function getBlacklistedIdentifier(blacklisted: IBlacklisted): number | undefined {
  return blacklisted.id;
}
