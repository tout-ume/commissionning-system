import dayjs from 'dayjs/esm';
import { ITerritory } from 'app/entities/territory/territory.model';
import { IConfigurationPlan } from 'app/entities/configuration-plan/configuration-plan.model';
import { IPartner } from 'app/entities/partner/partner.model';

export interface IZone {
  id?: number;
  code?: string;
  name?: string;
  description?: string | null;
  state?: string | null;
  zoneTypeId?: number | null;
  createdBy?: string;
  createdDate?: dayjs.Dayjs | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: dayjs.Dayjs | null;
  spare1?: string | null;
  spare2?: string | null;
  spare3?: string | null;
  territory?: ITerritory | null;
  configurationPlans?: IConfigurationPlan[] | null;
  partners?: IPartner[] | null;
}

export class Zone implements IZone {
  constructor(
    public id?: number,
    public code?: string,
    public name?: string,
    public description?: string | null,
    public state?: string | null,
    public zoneTypeId?: number | null,
    public createdBy?: string,
    public createdDate?: dayjs.Dayjs | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: dayjs.Dayjs | null,
    public spare1?: string | null,
    public spare2?: string | null,
    public spare3?: string | null,
    public territory?: ITerritory | null,
    public configurationPlans?: IConfigurationPlan[] | null,
    public partners?: IPartner[] | null
  ) {}
}

export function getZoneIdentifier(zone: IZone): number | undefined {
  return zone.id;
}
