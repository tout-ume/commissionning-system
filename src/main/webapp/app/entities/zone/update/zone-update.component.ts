import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IZone, Zone } from '../zone.model';
import { ZoneService } from '../service/zone.service';
import { ITerritory } from 'app/entities/territory/territory.model';
import { TerritoryService } from 'app/entities/territory/service/territory.service';

@Component({
  selector: 'jhi-zone-update',
  templateUrl: './zone-update.component.html',
})
export class ZoneUpdateComponent implements OnInit {
  isSaving = false;

  territoriesSharedCollection: ITerritory[] = [];

  editForm = this.fb.group({
    id: [],
    code: [null, [Validators.required]],
    name: [null, [Validators.required]],
    description: [],
    state: [],
    zoneTypeId: [],
    createdBy: [null, [Validators.required, Validators.maxLength(50)]],
    createdDate: [],
    lastModifiedBy: [null, [Validators.maxLength(50)]],
    lastModifiedDate: [],
    territory: [],
  });

  constructor(
    protected zoneService: ZoneService,
    protected territoryService: TerritoryService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ zone }) => {
      if (zone.id === undefined) {
        const today = dayjs().startOf('day');
        zone.createdDate = today;
        zone.lastModifiedDate = today;
      }

      this.updateForm(zone);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const zone = this.createFromForm();
    if (zone.id !== undefined) {
      this.subscribeToSaveResponse(this.zoneService.update(zone));
    } else {
      this.subscribeToSaveResponse(this.zoneService.create(zone));
    }
  }

  trackTerritoryById(_index: number, item: ITerritory): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IZone>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(zone: IZone): void {
    this.editForm.patchValue({
      id: zone.id,
      code: zone.code,
      name: zone.name,
      description: zone.description,
      state: zone.state,
      zoneTypeId: zone.zoneTypeId,
      createdBy: zone.createdBy,
      createdDate: zone.createdDate ? zone.createdDate.format(DATE_TIME_FORMAT) : null,
      lastModifiedBy: zone.lastModifiedBy,
      lastModifiedDate: zone.lastModifiedDate ? zone.lastModifiedDate.format(DATE_TIME_FORMAT) : null,
      territory: zone.territory,
    });

    this.territoriesSharedCollection = this.territoryService.addTerritoryToCollectionIfMissing(
      this.territoriesSharedCollection,
      zone.territory
    );
  }

  protected loadRelationshipsOptions(): void {
    this.territoryService
      .query()
      .pipe(map((res: HttpResponse<ITerritory[]>) => res.body ?? []))
      .pipe(
        map((territories: ITerritory[]) =>
          this.territoryService.addTerritoryToCollectionIfMissing(territories, this.editForm.get('territory')!.value)
        )
      )
      .subscribe((territories: ITerritory[]) => (this.territoriesSharedCollection = territories));
  }

  protected createFromForm(): IZone {
    return {
      ...new Zone(),
      id: this.editForm.get(['id'])!.value,
      code: this.editForm.get(['code'])!.value,
      name: this.editForm.get(['name'])!.value,
      description: this.editForm.get(['description'])!.value,
      state: this.editForm.get(['state'])!.value,
      zoneTypeId: this.editForm.get(['zoneTypeId'])!.value,
      createdBy: this.editForm.get(['createdBy'])!.value,
      createdDate: this.editForm.get(['createdDate'])!.value
        ? dayjs(this.editForm.get(['createdDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      lastModifiedBy: this.editForm.get(['lastModifiedBy'])!.value,
      lastModifiedDate: this.editForm.get(['lastModifiedDate'])!.value
        ? dayjs(this.editForm.get(['lastModifiedDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      territory: this.editForm.get(['territory'])!.value,
    };
  }
}
