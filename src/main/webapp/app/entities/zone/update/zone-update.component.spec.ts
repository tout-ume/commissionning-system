import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ZoneService } from '../service/zone.service';
import { IZone, Zone } from '../zone.model';
import { ITerritory } from 'app/entities/territory/territory.model';
import { TerritoryService } from 'app/entities/territory/service/territory.service';

import { ZoneUpdateComponent } from './zone-update.component';

describe('Zone Management Update Component', () => {
  let comp: ZoneUpdateComponent;
  let fixture: ComponentFixture<ZoneUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let zoneService: ZoneService;
  let territoryService: TerritoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [ZoneUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ZoneUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ZoneUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    zoneService = TestBed.inject(ZoneService);
    territoryService = TestBed.inject(TerritoryService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Territory query and add missing value', () => {
      const zone: IZone = { id: 456 };
      const territory: ITerritory = { id: 54160 };
      zone.territory = territory;

      const territoryCollection: ITerritory[] = [{ id: 6484 }];
      jest.spyOn(territoryService, 'query').mockReturnValue(of(new HttpResponse({ body: territoryCollection })));
      const additionalTerritories = [territory];
      const expectedCollection: ITerritory[] = [...additionalTerritories, ...territoryCollection];
      jest.spyOn(territoryService, 'addTerritoryToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ zone });
      comp.ngOnInit();

      expect(territoryService.query).toHaveBeenCalled();
      expect(territoryService.addTerritoryToCollectionIfMissing).toHaveBeenCalledWith(territoryCollection, ...additionalTerritories);
      expect(comp.territoriesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const zone: IZone = { id: 456 };
      const territory: ITerritory = { id: 24189 };
      zone.territory = territory;

      activatedRoute.data = of({ zone });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(zone));
      expect(comp.territoriesSharedCollection).toContain(territory);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Zone>>();
      const zone = { id: 123 };
      jest.spyOn(zoneService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ zone });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: zone }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(zoneService.update).toHaveBeenCalledWith(zone);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Zone>>();
      const zone = new Zone();
      jest.spyOn(zoneService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ zone });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: zone }));
      saveSubject.complete();

      // THEN
      expect(zoneService.create).toHaveBeenCalledWith(zone);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Zone>>();
      const zone = { id: 123 };
      jest.spyOn(zoneService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ zone });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(zoneService.update).toHaveBeenCalledWith(zone);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackTerritoryById', () => {
      it('Should return tracked Territory primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackTerritoryById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
