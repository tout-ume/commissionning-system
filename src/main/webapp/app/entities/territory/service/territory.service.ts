import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ITerritory, getTerritoryIdentifier } from '../territory.model';

export type EntityResponseType = HttpResponse<ITerritory>;
export type EntityArrayResponseType = HttpResponse<ITerritory[]>;

@Injectable({ providedIn: 'root' })
export class TerritoryService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/territories');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(territory: ITerritory): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(territory);
    return this.http
      .post<ITerritory>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(territory: ITerritory): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(territory);
    return this.http
      .put<ITerritory>(`${this.resourceUrl}/${getTerritoryIdentifier(territory) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(territory: ITerritory): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(territory);
    return this.http
      .patch<ITerritory>(`${this.resourceUrl}/${getTerritoryIdentifier(territory) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ITerritory>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ITerritory[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addTerritoryToCollectionIfMissing(
    territoryCollection: ITerritory[],
    ...territoriesToCheck: (ITerritory | null | undefined)[]
  ): ITerritory[] {
    const territories: ITerritory[] = territoriesToCheck.filter(isPresent);
    if (territories.length > 0) {
      const territoryCollectionIdentifiers = territoryCollection.map(territoryItem => getTerritoryIdentifier(territoryItem)!);
      const territoriesToAdd = territories.filter(territoryItem => {
        const territoryIdentifier = getTerritoryIdentifier(territoryItem);
        if (territoryIdentifier == null || territoryCollectionIdentifiers.includes(territoryIdentifier)) {
          return false;
        }
        territoryCollectionIdentifiers.push(territoryIdentifier);
        return true;
      });
      return [...territoriesToAdd, ...territoryCollection];
    }
    return territoryCollection;
  }

  protected convertDateFromClient(territory: ITerritory): ITerritory {
    return Object.assign({}, territory, {
      createdDate: territory.createdDate?.isValid() ? territory.createdDate.toJSON() : undefined,
      lastModifiedDate: territory.lastModifiedDate?.isValid() ? territory.lastModifiedDate.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createdDate = res.body.createdDate ? dayjs(res.body.createdDate) : undefined;
      res.body.lastModifiedDate = res.body.lastModifiedDate ? dayjs(res.body.lastModifiedDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((territory: ITerritory) => {
        territory.createdDate = territory.createdDate ? dayjs(territory.createdDate) : undefined;
        territory.lastModifiedDate = territory.lastModifiedDate ? dayjs(territory.lastModifiedDate) : undefined;
      });
    }
    return res;
  }
}
