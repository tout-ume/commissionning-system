import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { ITerritory, Territory } from '../territory.model';

import { TerritoryService } from './territory.service';

describe('Territory Service', () => {
  let service: TerritoryService;
  let httpMock: HttpTestingController;
  let elemDefault: ITerritory;
  let expectedResult: ITerritory | ITerritory[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(TerritoryService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      name: 'AAAAAAA',
      description: 'AAAAAAA',
      state: 'AAAAAAA',
      createdBy: 'AAAAAAA',
      createdDate: currentDate,
      lastModifiedBy: 'AAAAAAA',
      lastModifiedDate: currentDate,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          createdDate: currentDate.format(DATE_TIME_FORMAT),
          lastModifiedDate: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Territory', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          createdDate: currentDate.format(DATE_TIME_FORMAT),
          lastModifiedDate: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdDate: currentDate,
          lastModifiedDate: currentDate,
        },
        returnedFromService
      );

      service.create(new Territory()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Territory', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          description: 'BBBBBB',
          state: 'BBBBBB',
          createdBy: 'BBBBBB',
          createdDate: currentDate.format(DATE_TIME_FORMAT),
          lastModifiedBy: 'BBBBBB',
          lastModifiedDate: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdDate: currentDate,
          lastModifiedDate: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Territory', () => {
      const patchObject = Object.assign(
        {
          description: 'BBBBBB',
          state: 'BBBBBB',
          createdBy: 'BBBBBB',
          lastModifiedBy: 'BBBBBB',
          lastModifiedDate: currentDate.format(DATE_TIME_FORMAT),
        },
        new Territory()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          createdDate: currentDate,
          lastModifiedDate: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Territory', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          description: 'BBBBBB',
          state: 'BBBBBB',
          createdBy: 'BBBBBB',
          createdDate: currentDate.format(DATE_TIME_FORMAT),
          lastModifiedBy: 'BBBBBB',
          lastModifiedDate: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          createdDate: currentDate,
          lastModifiedDate: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Territory', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addTerritoryToCollectionIfMissing', () => {
      it('should add a Territory to an empty array', () => {
        const territory: ITerritory = { id: 123 };
        expectedResult = service.addTerritoryToCollectionIfMissing([], territory);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(territory);
      });

      it('should not add a Territory to an array that contains it', () => {
        const territory: ITerritory = { id: 123 };
        const territoryCollection: ITerritory[] = [
          {
            ...territory,
          },
          { id: 456 },
        ];
        expectedResult = service.addTerritoryToCollectionIfMissing(territoryCollection, territory);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Territory to an array that doesn't contain it", () => {
        const territory: ITerritory = { id: 123 };
        const territoryCollection: ITerritory[] = [{ id: 456 }];
        expectedResult = service.addTerritoryToCollectionIfMissing(territoryCollection, territory);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(territory);
      });

      it('should add only unique Territory to an array', () => {
        const territoryArray: ITerritory[] = [{ id: 123 }, { id: 456 }, { id: 34816 }];
        const territoryCollection: ITerritory[] = [{ id: 123 }];
        expectedResult = service.addTerritoryToCollectionIfMissing(territoryCollection, ...territoryArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const territory: ITerritory = { id: 123 };
        const territory2: ITerritory = { id: 456 };
        expectedResult = service.addTerritoryToCollectionIfMissing([], territory, territory2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(territory);
        expect(expectedResult).toContain(territory2);
      });

      it('should accept null and undefined values', () => {
        const territory: ITerritory = { id: 123 };
        expectedResult = service.addTerritoryToCollectionIfMissing([], null, territory, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(territory);
      });

      it('should return initial array if no Territory is added', () => {
        const territoryCollection: ITerritory[] = [{ id: 123 }];
        expectedResult = service.addTerritoryToCollectionIfMissing(territoryCollection, undefined, null);
        expect(expectedResult).toEqual(territoryCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
