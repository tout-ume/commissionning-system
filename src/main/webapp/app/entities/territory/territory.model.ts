import dayjs from 'dayjs/esm';
import { IZone } from 'app/entities/zone/zone.model';

export interface ITerritory {
  id?: number;
  name?: string;
  description?: string | null;
  state?: string | null;
  createdBy?: string;
  createdDate?: dayjs.Dayjs | null;
  lastModifiedBy?: string | null;
  lastModifiedDate?: dayjs.Dayjs | null;
  spare1?: string | null;
  spare2?: string | null;
  spare3?: string | null;
  zones?: IZone[] | null;
}

export class Territory implements ITerritory {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string | null,
    public state?: string | null,
    public createdBy?: string,
    public createdDate?: dayjs.Dayjs | null,
    public lastModifiedBy?: string | null,
    public lastModifiedDate?: dayjs.Dayjs | null,
    public spare1?: string | null,
    public spare2?: string | null,
    public spare3?: string | null,
    public zones?: IZone[] | null
  ) {}
}

export function getTerritoryIdentifier(territory: ITerritory): number | undefined {
  return territory.id;
}
