import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ITerritory } from '../territory.model';
import { TerritoryService } from '../service/territory.service';

@Component({
  templateUrl: './territory-delete-dialog.component.html',
})
export class TerritoryDeleteDialogComponent {
  territory?: ITerritory;

  constructor(protected territoryService: TerritoryService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.territoryService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
