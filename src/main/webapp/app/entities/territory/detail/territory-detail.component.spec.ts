import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { TerritoryDetailComponent } from './territory-detail.component';

describe('Territory Management Detail Component', () => {
  let comp: TerritoryDetailComponent;
  let fixture: ComponentFixture<TerritoryDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TerritoryDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ territory: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(TerritoryDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(TerritoryDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load territory on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.territory).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
