import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { TerritoryComponent } from '../list/territory.component';
import { TerritoryDetailComponent } from '../detail/territory-detail.component';
import { TerritoryUpdateComponent } from '../update/territory-update.component';
import { TerritoryRoutingResolveService } from './territory-routing-resolve.service';

const territoryRoute: Routes = [
  {
    path: '',
    component: TerritoryComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: TerritoryDetailComponent,
    resolve: {
      territory: TerritoryRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: TerritoryUpdateComponent,
    resolve: {
      territory: TerritoryRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: TerritoryUpdateComponent,
    resolve: {
      territory: TerritoryRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(territoryRoute)],
  exports: [RouterModule],
})
export class TerritoryRoutingModule {}
