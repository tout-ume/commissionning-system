import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ITerritory, Territory } from '../territory.model';
import { TerritoryService } from '../service/territory.service';

@Injectable({ providedIn: 'root' })
export class TerritoryRoutingResolveService implements Resolve<ITerritory> {
  constructor(protected service: TerritoryService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ITerritory> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((territory: HttpResponse<Territory>) => {
          if (territory.body) {
            return of(territory.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Territory());
  }
}
