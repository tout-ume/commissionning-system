import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { TerritoryComponent } from './list/territory.component';
import { TerritoryDetailComponent } from './detail/territory-detail.component';
import { TerritoryUpdateComponent } from './update/territory-update.component';
import { TerritoryDeleteDialogComponent } from './delete/territory-delete-dialog.component';
import { TerritoryRoutingModule } from './route/territory-routing.module';

@NgModule({
  imports: [SharedModule, TerritoryRoutingModule],
  declarations: [TerritoryComponent, TerritoryDetailComponent, TerritoryUpdateComponent, TerritoryDeleteDialogComponent],
  entryComponents: [TerritoryDeleteDialogComponent],
})
export class TerritoryModule {}
