import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { ITerritory, Territory } from '../territory.model';
import { TerritoryService } from '../service/territory.service';

@Component({
  selector: 'jhi-territory-update',
  templateUrl: './territory-update.component.html',
})
export class TerritoryUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    description: [],
    state: [],
    createdBy: [null, [Validators.required, Validators.maxLength(50)]],
    createdDate: [],
    lastModifiedBy: [null, [Validators.maxLength(50)]],
    lastModifiedDate: [],
  });

  constructor(protected territoryService: TerritoryService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ territory }) => {
      if (territory.id === undefined) {
        const today = dayjs().startOf('day');
        territory.createdDate = today;
        territory.lastModifiedDate = today;
      }

      this.updateForm(territory);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const territory = this.createFromForm();
    if (territory.id !== undefined) {
      this.subscribeToSaveResponse(this.territoryService.update(territory));
    } else {
      this.subscribeToSaveResponse(this.territoryService.create(territory));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITerritory>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(territory: ITerritory): void {
    this.editForm.patchValue({
      id: territory.id,
      name: territory.name,
      description: territory.description,
      state: territory.state,
      createdBy: territory.createdBy,
      createdDate: territory.createdDate ? territory.createdDate.format(DATE_TIME_FORMAT) : null,
      lastModifiedBy: territory.lastModifiedBy,
      lastModifiedDate: territory.lastModifiedDate ? territory.lastModifiedDate.format(DATE_TIME_FORMAT) : null,
    });
  }

  protected createFromForm(): ITerritory {
    return {
      ...new Territory(),
      id: this.editForm.get(['id'])!.value,
      name: this.editForm.get(['name'])!.value,
      description: this.editForm.get(['description'])!.value,
      state: this.editForm.get(['state'])!.value,
      createdBy: this.editForm.get(['createdBy'])!.value,
      createdDate: this.editForm.get(['createdDate'])!.value
        ? dayjs(this.editForm.get(['createdDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
      lastModifiedBy: this.editForm.get(['lastModifiedBy'])!.value,
      lastModifiedDate: this.editForm.get(['lastModifiedDate'])!.value
        ? dayjs(this.editForm.get(['lastModifiedDate'])!.value, DATE_TIME_FORMAT)
        : undefined,
    };
  }
}
