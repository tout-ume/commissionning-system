import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { TerritoryService } from '../service/territory.service';
import { ITerritory, Territory } from '../territory.model';

import { TerritoryUpdateComponent } from './territory-update.component';

describe('Territory Management Update Component', () => {
  let comp: TerritoryUpdateComponent;
  let fixture: ComponentFixture<TerritoryUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let territoryService: TerritoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [TerritoryUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(TerritoryUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(TerritoryUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    territoryService = TestBed.inject(TerritoryService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const territory: ITerritory = { id: 456 };

      activatedRoute.data = of({ territory });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(territory));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Territory>>();
      const territory = { id: 123 };
      jest.spyOn(territoryService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ territory });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: territory }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(territoryService.update).toHaveBeenCalledWith(territory);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Territory>>();
      const territory = new Territory();
      jest.spyOn(territoryService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ territory });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: territory }));
      saveSubject.complete();

      // THEN
      expect(territoryService.create).toHaveBeenCalledWith(territory);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Territory>>();
      const territory = { id: 123 };
      jest.spyOn(territoryService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ territory });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(territoryService.update).toHaveBeenCalledWith(territory);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
