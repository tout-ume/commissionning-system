import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CommissionDetailComponent } from './commission-detail.component';

describe('Commission Management Detail Component', () => {
  let comp: CommissionDetailComponent;
  let fixture: ComponentFixture<CommissionDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CommissionDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ commission: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(CommissionDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(CommissionDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load commission on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.commission).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
