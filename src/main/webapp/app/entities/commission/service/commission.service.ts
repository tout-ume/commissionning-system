import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICommission, getCommissionIdentifier, ICommissionExportFile } from '../commission.model';
import { ICommissionBulk } from '../../payment/commission-bulk.model';

export type EntityResponseType = HttpResponse<ICommission>;
export type EntityArrayResponseType = HttpResponse<ICommission[]>;
export type EntityArrayBulkResponseType = HttpResponse<ICommissionBulk[]>;

@Injectable({ providedIn: 'root' })
export class CommissionService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/commissions');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  getTopProfileCommissionsByPlan(planId: number): Observable<EntityArrayResponseType> {
    return this.http
      .get<ICommission[]>(`${this.resourceUrl}/top-profile-by-plan/${planId}`, { observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  getAllProfilesCommissionsByPlan(planId: number, req?: any): Observable<EntityArrayBulkResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICommissionBulk[]>(`${this.resourceUrl}/all-profiles-by-plan/${planId}`, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayBulkResponseType) => this.convertBulkCommissionsDateArrayFromServer(res)));
  }

  create(commission: ICommission): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(commission);
    return this.http
      .post<ICommission>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(commission: ICommission): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(commission);
    return this.http
      .put<ICommission>(`${this.resourceUrl}/${getCommissionIdentifier(commission) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(commission: ICommission): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(commission);
    return this.http
      .patch<ICommission>(`${this.resourceUrl}/${getCommissionIdentifier(commission) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<ICommission>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<ICommission[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCommissionToCollectionIfMissing(
    commissionCollection: ICommission[],
    ...commissionsToCheck: (ICommission | null | undefined)[]
  ): ICommission[] {
    const commissions: ICommission[] = commissionsToCheck.filter(isPresent);
    if (commissions.length > 0) {
      const commissionCollectionIdentifiers = commissionCollection.map(commissionItem => getCommissionIdentifier(commissionItem)!);
      const commissionsToAdd = commissions.filter(commissionItem => {
        const commissionIdentifier = getCommissionIdentifier(commissionItem);
        if (commissionIdentifier == null || commissionCollectionIdentifiers.includes(commissionIdentifier)) {
          return false;
        }
        commissionCollectionIdentifiers.push(commissionIdentifier);
        return true;
      });
      return [...commissionsToAdd, ...commissionCollection];
    }
    return commissionCollection;
  }

  mappingCommission(commissionsToExport: ICommission[]): ICommissionExportFile[] {
    return commissionsToExport.map(commission => {
      const data: ICommissionExportFile = {};
      data.id = commission.id;
      data.amount = commission.amount;
      data.calculatedAt = commission.calculatedAt;
      data.commissionPaymentStatus = commission.commissionPaymentStatus;
      data.senderMsisdn = commission.senderMsisdn;
      data.senderProfile = commission.senderProfile;
      data.serviceType = commission.serviceType;
      return data;
    });
  }

  protected convertDateFromClient(commission: ICommission): ICommission {
    return Object.assign({}, commission, {
      calculatedAt: commission.calculatedAt?.isValid() ? commission.calculatedAt.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.calculatedAt = res.body.calculatedAt ? dayjs(res.body.calculatedAt) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((commission: ICommission) => {
        commission.calculatedAt = commission.calculatedAt ? dayjs(commission.calculatedAt) : undefined;
        commission.calculationDate = commission.calculationDate ? dayjs(commission.calculationDate) : undefined;
      });
    }
    return res;
  }

  protected convertBulkCommissionsDateArrayFromServer(res: EntityArrayBulkResponseType): EntityArrayBulkResponseType {
    if (res.body) {
      res.body.forEach((commissionBulk: ICommissionBulk) => {
        commissionBulk.commissions?.forEach((commission: ICommission) => {
          commission.calculatedAt = commission.calculatedAt ? dayjs(commission.calculatedAt) : undefined;
        });
      });
    }
    return res;
  }
}
