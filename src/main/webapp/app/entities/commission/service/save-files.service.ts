import { Injectable } from '@angular/core';
import { saveAs } from 'file-saver';
import * as XLSX from 'xlsx';

const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';

@Injectable({
  providedIn: 'root',
})
export class SaveFilesService {
  exportToCsv(data: any[], fileName: string): void | boolean {
    try {
      // src : https://stackoverflow.com/a/51488124/10865170  npm i --save-dev @types/file-saver
      const header = Object.keys(data[0]);

      const csv = data.map(row => header.map(fieldName => (row[fieldName] !== null ? JSON.stringify(row[fieldName]) : '')).join(';'));
      csv.unshift(header.join(';'));
      const csvArray = csv.join('\r\n');

      const blob = new Blob([csvArray], { type: 'text/csv' });
      saveAs(blob, `${fileName}.csv`);
    } catch (error) {
      return false;
    }
  }

  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }
  public saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
    saveAs(data, fileName + '_export_' + new Date().toString() + EXCEL_EXTENSION);
  }
}
