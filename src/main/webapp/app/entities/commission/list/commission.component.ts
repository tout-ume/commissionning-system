import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ICommission, ICommissionExportFile } from '../commission.model';

import { ASC, DESC, ITEMS_PER_PAGE, SORT } from 'app/config/pagination.constants';
import { CommissionService } from '../service/commission.service';
import { CommissionDeleteDialogComponent } from '../delete/commission-delete-dialog.component';
import { ConfigurationPlanService } from '../../configuration-plan/service/configuration-plan.service';
import { IConfigurationPlan } from '../../configuration-plan/configuration-plan.model';

import { SaveFilesService } from '../service/save-files.service';
import { FormBuilder, Validators } from '@angular/forms';
import { DATE_TIME_FORMAT } from '../../../config/input.constants';
import dayjs from 'dayjs/esm';
import { IZone } from '../../zone/zone.model';

import { jsPDF } from 'jspdf';
import 'jspdf-autotable';

@Component({
  selector: 'jhi-commission',
  templateUrl: './commission.component.html',
  styleUrls: ['./commission.component.css'],
})
export class CommissionComponent implements OnInit {
  commissions!: ICommission[];
  isLoading = false;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page?: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;
  commissionPlanId!: number;

  isShowingTypeExportFile!: boolean;
  commissionExport!: ICommissionExportFile[];
  head: any;

  @ViewChild('pdfTable')
  pdfTable!: ElementRef;

  senderMsisdn!: string;
  filterForm = this.fb.group({
    msisdn: [null, [Validators.maxLength(9)]],
    profile: [null],
    dateStart: [null],
    dateEnd: [null],
    zone: [null],
  });

  profils: string[];
  allZones!: IZone[];
  allZonesName!: (string | undefined)[];

  constructor(
    protected commissionService: CommissionService,
    protected configurationPlanService: ConfigurationPlanService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected modalService: NgbModal,
    protected saveFilesService: SaveFilesService,
    protected fb: FormBuilder
  ) {
    this.profils = ['DP', 'DG', 'REV', 'PDV'];
  }

  loadPage(page?: number, dontNavigate?: boolean): void {
    this.isLoading = true;
    const pageToLoad: number = page ?? this.page ?? 1;

    const startDateFilterValue =
      this.filterForm.get('dateStart')!.value != null
        ? {
            'calculatedAt.greaterThanOrEqual': dayjs(
              dayjs(this.filterForm.get(['dateStart'])!.value)
                .set('hour', 0)
                .set('minute', 0)
                .set('second', 0),
              DATE_TIME_FORMAT
            ),
          }
        : undefined;

    const endDateFilterValue =
      this.filterForm.get('dateEnd')!.value != null
        ? {
            'calculatedAt.lessThanOrEqual': dayjs(
              dayjs(this.filterForm.get(['dateEnd'])!.value)
                .set('hour', 0)
                .set('minute', 0)
                .set('second', 0),
              DATE_TIME_FORMAT
            ),
          }
        : undefined;

    const msisdnFilterQuery =
      this.filterForm.get('msisdn')?.value != null ? { 'senderMsisdn.equals': this.filterForm.get('msisdn')?.value } : undefined;

    const profileFilterQuery =
      this.filterForm.get('profile')?.value != null ? { 'senderProfile.contains': this.filterForm.get('profile')?.value } : undefined;

    const zoneFilterQuery =
      this.filterForm.get('zone')?.value != null ? { 'configurationPlan.zones.contains': this.filterForm.get('zone')?.value } : [];

    this.configurationPlanService
      .query({
        'commissioningPlanId.equals': this.commissionPlanId,
      })
      .subscribe({
        next: (configsResult: HttpResponse<IConfigurationPlan[]>) => {
          const zoneTemp = configsResult.body
            ?.map(res => res.zones)
            .reduce((acc, item) => {
              if (acc && item) {
                acc = acc.concat(item);
              }
              return acc;
            }, []);
          this.allZones = Array.from(new Set(zoneTemp));
          const allZonesNameTemp = this.allZones.map(zone => zone.name);
          this.allZonesName = allZonesNameTemp;
          this.commissionService
            .query({
              page: pageToLoad - 1,
              size: this.itemsPerPage,
              sort: this.sort(),
              'configurationPlanId.in': configsResult.body?.map(item => item.id),
              ...msisdnFilterQuery,
              ...profileFilterQuery,
              ...startDateFilterValue,
              ...endDateFilterValue,
              ...zoneFilterQuery,
            })
            .subscribe({
              next: (res: HttpResponse<ICommission[]>) => {
                this.isLoading = false;
                this.onSuccess(res.body, res.headers, pageToLoad, !dontNavigate);
              },
              error: () => {
                this.isLoading = false;
                this.onError();
              },
            });
        },
        error: () => {
          this.isLoading = false;
          this.onError();
        },
      });
  }

  ngOnInit(): void {
    this.handleNavigation();
  }

  trackId(_index: number, item: ICommission): number {
    return item.id!;
  }

  delete(commission: ICommission): void {
    const modalRef = this.modalService.open(CommissionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.commission = commission;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadPage();
      }
    });
  }

  onChangeData(event: any): void {
    const { name, value } = event.target;
    if (!value) {
      this.filterForm.patchValue({ [name]: '' });
    }
  }

  onSearch(): void {
    this.loadPage(1, true);
  }

  exportFile(exportALl: string): void {
    this.downloadAllFile(exportALl);
  }

  exportAsXLSX(): void {
    const allCommissions = this.commissionService.mappingCommission(this.commissions);
    this.saveFilesService.exportAsExcelFile(allCommissions, 'commissions');
  }

  // https://codezup.com/angular-9-10-export-table-data-to-pdf-using-jspdf/
  exportAsPDF(): void {
    const doc = new jsPDF();
    this.head = [['ID', 'Amount', 'CalculatedAt', 'commissionPaymentStatus', 'senderMsisdn', 'senderProfil', 'serviceType']];
    this.commissionExport = this.commissionService.mappingCommission(this.commissions);
    doc.setFontSize(18);
    doc.text('Commissions calculées', 11, 8);
    doc.setFontSize(11);
    doc.setTextColor(100);
    doc.setHeaderFunction(this.head);

    (doc as any).autoTable({
      body: this.commissionExport,
      theme: 'plain',
    });

    // below line for Open PDF document in new tab
    // doc.output('dataurlnewwindow');

    // below line for Download PDF document
    doc.save('commissions_calculees' + new Date().toString() + '.pdf');
  }

  protected downloadAllFile(typeExport: string): void {
    if (typeExport === 'one') {
      const allCommissions = this.commissionService.mappingCommission(this.commissions);
      this.saveFilesService.exportToCsv(allCommissions, `commissions_page_result_${new Date().toString()}`);
    } else {
      this.commissionService
        .query({
          page: 0,
          size: this.itemsPerPage,
          sort: this.sort(),
        })
        .subscribe({
          next: (res: HttpResponse<ICommission[]>) => {
            this.isLoading = false;
            if (res.body) {
              const allCommissions = this.commissionService.mappingCommission(res.body);
              this.saveFilesService.exportToCsv(allCommissions, `commissions_search_result_${new Date().toString()}`);
            }
          },
          error: () => {
            this.onError();
          },
        });
    }
  }

  protected sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? ASC : DESC)];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected handleNavigation(): void {
    combineLatest([this.activatedRoute.data, this.activatedRoute.queryParamMap, this.activatedRoute.params]).subscribe(
      ([data, params, pathParam]) => {
        this.commissionPlanId = pathParam.id;
        const page = params.get('page');
        const pageNumber = +(page ?? 1);
        const sort = (params.get(SORT) ?? data['defaultSort']).split(',');
        const predicate = sort[0];
        const ascending = sort[1] === ASC;
        if (pageNumber !== this.page || predicate !== this.predicate || ascending !== this.ascending) {
          this.predicate = predicate;
          this.ascending = ascending;
          this.loadPage(pageNumber, true);
        }
      }
    );
  }

  protected onSuccess(data: ICommission[] | null, headers: HttpHeaders, page: number, navigate: boolean): void {
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    if (navigate) {
      this.router.navigate(['/commission', this.commissionPlanId, 'plan'], {
        queryParams: {
          page: this.page,
          size: this.itemsPerPage,
          sort: this.predicate + ',' + (this.ascending ? ASC : DESC),
        },
      });
    }
    this.commissions = data ?? [];
    this.ngbPaginationPage = this.page;
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page ?? 1;
  }
}
