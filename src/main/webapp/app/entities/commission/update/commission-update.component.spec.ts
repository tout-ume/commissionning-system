import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { CommissionService } from '../service/commission.service';
import { ICommission, Commission } from '../commission.model';
import { ICommissionAccount } from 'app/entities/commission-account/commission-account.model';
import { CommissionAccountService } from 'app/entities/commission-account/service/commission-account.service';
import { ITransactionOperation } from 'app/entities/transaction-operation/transaction-operation.model';
import { TransactionOperationService } from 'app/entities/transaction-operation/service/transaction-operation.service';
import { IConfigurationPlan } from 'app/entities/configuration-plan/configuration-plan.model';
import { ConfigurationPlanService } from 'app/entities/configuration-plan/service/configuration-plan.service';
import { IPayment } from 'app/entities/payment/payment.model';
import { PaymentService } from 'app/entities/payment/service/payment.service';

import { CommissionUpdateComponent } from './commission-update.component';

describe('Commission Management Update Component', () => {
  let comp: CommissionUpdateComponent;
  let fixture: ComponentFixture<CommissionUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let commissionService: CommissionService;
  let commissionAccountService: CommissionAccountService;
  let transactionOperationService: TransactionOperationService;
  let configurationPlanService: ConfigurationPlanService;
  let paymentService: PaymentService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [CommissionUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CommissionUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CommissionUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    commissionService = TestBed.inject(CommissionService);
    commissionAccountService = TestBed.inject(CommissionAccountService);
    transactionOperationService = TestBed.inject(TransactionOperationService);
    configurationPlanService = TestBed.inject(ConfigurationPlanService);
    paymentService = TestBed.inject(PaymentService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call CommissionAccount query and add missing value', () => {
      const commission: ICommission = { id: 456 };
      const commissionAccount: ICommissionAccount = { id: 1144 };
      commission.commissionAccount = commissionAccount;

      const commissionAccountCollection: ICommissionAccount[] = [{ id: 82201 }];
      jest.spyOn(commissionAccountService, 'query').mockReturnValue(of(new HttpResponse({ body: commissionAccountCollection })));
      const additionalCommissionAccounts = [commissionAccount];
      const expectedCollection: ICommissionAccount[] = [...additionalCommissionAccounts, ...commissionAccountCollection];
      jest.spyOn(commissionAccountService, 'addCommissionAccountToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ commission });
      comp.ngOnInit();

      expect(commissionAccountService.query).toHaveBeenCalled();
      expect(commissionAccountService.addCommissionAccountToCollectionIfMissing).toHaveBeenCalledWith(
        commissionAccountCollection,
        ...additionalCommissionAccounts
      );
      expect(comp.commissionAccountsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call TransactionOperation query and add missing value', () => {
      const commission: ICommission = { id: 456 };
      const transactionOperations: ITransactionOperation[] = [{ id: 54683 }];
      commission.transactionOperations = transactionOperations;

      const transactionOperationCollection: ITransactionOperation[] = [{ id: 1211 }];
      jest.spyOn(transactionOperationService, 'query').mockReturnValue(of(new HttpResponse({ body: transactionOperationCollection })));
      const additionalTransactionOperations = [...transactionOperations];
      const expectedCollection: ITransactionOperation[] = [...additionalTransactionOperations, ...transactionOperationCollection];
      jest.spyOn(transactionOperationService, 'addTransactionOperationToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ commission });
      comp.ngOnInit();

      expect(transactionOperationService.query).toHaveBeenCalled();
      expect(transactionOperationService.addTransactionOperationToCollectionIfMissing).toHaveBeenCalledWith(
        transactionOperationCollection,
        ...additionalTransactionOperations
      );
      expect(comp.transactionOperationsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call ConfigurationPlan query and add missing value', () => {
      const commission: ICommission = { id: 456 };
      const configurationPlan: IConfigurationPlan = { id: 89028 };
      commission.configurationPlan = configurationPlan;

      const configurationPlanCollection: IConfigurationPlan[] = [{ id: 69935 }];
      jest.spyOn(configurationPlanService, 'query').mockReturnValue(of(new HttpResponse({ body: configurationPlanCollection })));
      const additionalConfigurationPlans = [configurationPlan];
      const expectedCollection: IConfigurationPlan[] = [...additionalConfigurationPlans, ...configurationPlanCollection];
      jest.spyOn(configurationPlanService, 'addConfigurationPlanToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ commission });
      comp.ngOnInit();

      expect(configurationPlanService.query).toHaveBeenCalled();
      expect(configurationPlanService.addConfigurationPlanToCollectionIfMissing).toHaveBeenCalledWith(
        configurationPlanCollection,
        ...additionalConfigurationPlans
      );
      expect(comp.configurationPlansSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Payment query and add missing value', () => {
      const commission: ICommission = { id: 456 };
      const payment: IPayment = { id: 4715 };
      commission.payment = payment;

      const paymentCollection: IPayment[] = [{ id: 87617 }];
      jest.spyOn(paymentService, 'query').mockReturnValue(of(new HttpResponse({ body: paymentCollection })));
      const additionalPayments = [payment];
      const expectedCollection: IPayment[] = [...additionalPayments, ...paymentCollection];
      jest.spyOn(paymentService, 'addPaymentToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ commission });
      comp.ngOnInit();

      expect(paymentService.query).toHaveBeenCalled();
      expect(paymentService.addPaymentToCollectionIfMissing).toHaveBeenCalledWith(paymentCollection, ...additionalPayments);
      expect(comp.paymentsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const commission: ICommission = { id: 456 };
      const commissionAccount: ICommissionAccount = { id: 81296 };
      commission.commissionAccount = commissionAccount;
      const transactionOperations: ITransactionOperation = { id: 22885 };
      commission.transactionOperations = [transactionOperations];
      const configurationPlan: IConfigurationPlan = { id: 27614 };
      commission.configurationPlan = configurationPlan;
      const payment: IPayment = { id: 50856 };
      commission.payment = payment;

      activatedRoute.data = of({ commission });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(commission));
      expect(comp.commissionAccountsSharedCollection).toContain(commissionAccount);
      expect(comp.transactionOperationsSharedCollection).toContain(transactionOperations);
      expect(comp.configurationPlansSharedCollection).toContain(configurationPlan);
      expect(comp.paymentsSharedCollection).toContain(payment);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Commission>>();
      const commission = { id: 123 };
      jest.spyOn(commissionService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ commission });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: commission }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(commissionService.update).toHaveBeenCalledWith(commission);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Commission>>();
      const commission = new Commission();
      jest.spyOn(commissionService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ commission });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: commission }));
      saveSubject.complete();

      // THEN
      expect(commissionService.create).toHaveBeenCalledWith(commission);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Commission>>();
      const commission = { id: 123 };
      jest.spyOn(commissionService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ commission });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(commissionService.update).toHaveBeenCalledWith(commission);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackCommissionAccountById', () => {
      it('Should return tracked CommissionAccount primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCommissionAccountById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackTransactionOperationById', () => {
      it('Should return tracked TransactionOperation primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackTransactionOperationById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackConfigurationPlanById', () => {
      it('Should return tracked ConfigurationPlan primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackConfigurationPlanById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackPaymentById', () => {
      it('Should return tracked Payment primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackPaymentById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });

  describe('Getting selected relationships', () => {
    describe('getSelectedTransactionOperation', () => {
      it('Should return option if no TransactionOperation is selected', () => {
        const option = { id: 123 };
        const result = comp.getSelectedTransactionOperation(option);
        expect(result === option).toEqual(true);
      });

      it('Should return selected TransactionOperation for according option', () => {
        const option = { id: 123 };
        const selected = { id: 123 };
        const selected2 = { id: 456 };
        const result = comp.getSelectedTransactionOperation(option, [selected2, selected]);
        expect(result === selected).toEqual(true);
        expect(result === selected2).toEqual(false);
        expect(result === option).toEqual(false);
      });

      it('Should return option if this TransactionOperation is not selected', () => {
        const option = { id: 123 };
        const selected = { id: 456 };
        const result = comp.getSelectedTransactionOperation(option, [selected]);
        expect(result === option).toEqual(true);
        expect(result === selected).toEqual(false);
      });
    });
  });
});
