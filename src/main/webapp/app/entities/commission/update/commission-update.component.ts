import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { ICommission, Commission } from '../commission.model';
import { CommissionService } from '../service/commission.service';
import { ICommissionAccount } from 'app/entities/commission-account/commission-account.model';
import { CommissionAccountService } from 'app/entities/commission-account/service/commission-account.service';
import { ITransactionOperation } from 'app/entities/transaction-operation/transaction-operation.model';
import { TransactionOperationService } from 'app/entities/transaction-operation/service/transaction-operation.service';
import { IConfigurationPlan } from 'app/entities/configuration-plan/configuration-plan.model';
import { ConfigurationPlanService } from 'app/entities/configuration-plan/service/configuration-plan.service';
import { IPayment } from 'app/entities/payment/payment.model';
import { PaymentService } from 'app/entities/payment/service/payment.service';
import { CommissionPaymentStatus } from 'app/entities/enumerations/commission-payment-status.model';

@Component({
  selector: 'jhi-commission-update',
  templateUrl: './commission-update.component.html',
})
export class CommissionUpdateComponent implements OnInit {
  isSaving = false;
  commissionPaymentStatusValues = Object.keys(CommissionPaymentStatus);

  commissionAccountsSharedCollection: ICommissionAccount[] = [];
  transactionOperationsSharedCollection: ITransactionOperation[] = [];
  configurationPlansSharedCollection: IConfigurationPlan[] = [];
  paymentsSharedCollection: IPayment[] = [];

  editForm = this.fb.group({
    id: [],
    amount: [null, [Validators.required]],
    calculatedAt: [],
    senderMsisdn: [],
    senderProfile: [],
    serviceType: [],
    commissionPaymentStatus: [],
    commissionAccount: [],
    transactionOperations: [],
    configurationPlan: [],
    payment: [],
  });
  commissioningPlanTypeValues!: [];

  constructor(
    protected commissionService: CommissionService,
    protected commissionAccountService: CommissionAccountService,
    protected transactionOperationService: TransactionOperationService,
    protected configurationPlanService: ConfigurationPlanService,
    protected paymentService: PaymentService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ commission }) => {
      if (commission.id === undefined) {
        const today = dayjs().startOf('day');
        commission.calculatedAt = today;
      }

      this.updateForm(commission);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const commission = this.createFromForm();
    if (commission.id !== undefined) {
      this.subscribeToSaveResponse(this.commissionService.update(commission));
    } else {
      this.subscribeToSaveResponse(this.commissionService.create(commission));
    }
  }

  trackCommissionAccountById(_index: number, item: ICommissionAccount): number {
    return item.id!;
  }

  trackTransactionOperationById(_index: number, item: ITransactionOperation): number {
    return item.id!;
  }

  trackConfigurationPlanById(_index: number, item: IConfigurationPlan): number {
    return item.id!;
  }

  trackPaymentById(_index: number, item: IPayment): number {
    return item.id!;
  }

  getSelectedTransactionOperation(option: ITransactionOperation, selectedVals?: ITransactionOperation[]): ITransactionOperation {
    if (selectedVals) {
      for (const selectedVal of selectedVals) {
        if (option.id === selectedVal.id) {
          return selectedVal;
        }
      }
    }
    return option;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICommission>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(commission: ICommission): void {
    this.editForm.patchValue({
      id: commission.id,
      amount: commission.amount,
      calculatedAt: commission.calculatedAt ? commission.calculatedAt.format(DATE_TIME_FORMAT) : null,
      senderMsisdn: commission.senderMsisdn,
      senderProfile: commission.senderProfile,
      serviceType: commission.serviceType,
      commissionPaymentStatus: commission.commissionPaymentStatus,
      commissionAccount: commission.commissionAccount,
      transactionOperations: commission.transactionOperations,
      configurationPlan: commission.configurationPlan,
      payment: commission.payment,
    });

    this.commissionAccountsSharedCollection = this.commissionAccountService.addCommissionAccountToCollectionIfMissing(
      this.commissionAccountsSharedCollection,
      commission.commissionAccount
    );
    this.transactionOperationsSharedCollection = this.transactionOperationService.addTransactionOperationToCollectionIfMissing(
      this.transactionOperationsSharedCollection,
      ...(commission.transactionOperations ?? [])
    );
    this.configurationPlansSharedCollection = this.configurationPlanService.addConfigurationPlanToCollectionIfMissing(
      this.configurationPlansSharedCollection,
      commission.configurationPlan
    );
    this.paymentsSharedCollection = this.paymentService.addPaymentToCollectionIfMissing(this.paymentsSharedCollection, commission.payment);
  }

  protected loadRelationshipsOptions(): void {
    this.commissionAccountService
      .query()
      .pipe(map((res: HttpResponse<ICommissionAccount[]>) => res.body ?? []))
      .pipe(
        map((commissionAccounts: ICommissionAccount[]) =>
          this.commissionAccountService.addCommissionAccountToCollectionIfMissing(
            commissionAccounts,
            this.editForm.get('commissionAccount')!.value
          )
        )
      )
      .subscribe((commissionAccounts: ICommissionAccount[]) => (this.commissionAccountsSharedCollection = commissionAccounts));

    this.transactionOperationService
      .query()
      .pipe(map((res: HttpResponse<ITransactionOperation[]>) => res.body ?? []))
      .pipe(
        map((transactionOperations: ITransactionOperation[]) =>
          this.transactionOperationService.addTransactionOperationToCollectionIfMissing(
            transactionOperations,
            ...(this.editForm.get('transactionOperations')!.value ?? [])
          )
        )
      )
      .subscribe((transactionOperations: ITransactionOperation[]) => (this.transactionOperationsSharedCollection = transactionOperations));

    this.configurationPlanService
      .query()
      .pipe(map((res: HttpResponse<IConfigurationPlan[]>) => res.body ?? []))
      .pipe(
        map((configurationPlans: IConfigurationPlan[]) =>
          this.configurationPlanService.addConfigurationPlanToCollectionIfMissing(
            configurationPlans,
            this.editForm.get('configurationPlan')!.value
          )
        )
      )
      .subscribe((configurationPlans: IConfigurationPlan[]) => (this.configurationPlansSharedCollection = configurationPlans));

    this.paymentService
      .query()
      .pipe(map((res: HttpResponse<IPayment[]>) => res.body ?? []))
      .pipe(
        map((payments: IPayment[]) => this.paymentService.addPaymentToCollectionIfMissing(payments, this.editForm.get('payment')!.value))
      )
      .subscribe((payments: IPayment[]) => (this.paymentsSharedCollection = payments));
  }

  protected createFromForm(): ICommission {
    return {
      ...new Commission(),
      id: this.editForm.get(['id'])!.value,
      amount: this.editForm.get(['amount'])!.value,
      calculatedAt: this.editForm.get(['calculatedAt'])!.value
        ? dayjs(this.editForm.get(['calculatedAt'])!.value, DATE_TIME_FORMAT)
        : undefined,
      senderMsisdn: this.editForm.get(['senderMsisdn'])!.value,
      senderProfile: this.editForm.get(['senderProfile'])!.value,
      serviceType: this.editForm.get(['serviceType'])!.value,
      commissionPaymentStatus: this.editForm.get(['commissionPaymentStatus'])!.value,
      commissionAccount: this.editForm.get(['commissionAccount'])!.value,
      transactionOperations: this.editForm.get(['transactionOperations'])!.value,
      configurationPlan: this.editForm.get(['configurationPlan'])!.value,
      payment: this.editForm.get(['payment'])!.value,
    };
  }
}
