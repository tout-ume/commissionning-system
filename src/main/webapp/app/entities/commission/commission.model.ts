import dayjs from 'dayjs/esm';
import { ICommissionAccount } from 'app/entities/commission-account/commission-account.model';
import { ITransactionOperation } from 'app/entities/transaction-operation/transaction-operation.model';
import { IConfigurationPlan } from 'app/entities/configuration-plan/configuration-plan.model';
import { IPayment } from 'app/entities/payment/payment.model';
import { CommissionPaymentStatus } from 'app/entities/enumerations/commission-payment-status.model';
import { CommissioningPlanType } from 'app/entities/enumerations/commissioning-plan-type.model';

export interface ICommission {
  id?: number;
  amount?: number;
  calculatedAt?: dayjs.Dayjs | null;
  calculationDate?: dayjs.Dayjs | null;
  calculationShortDate?: dayjs.Dayjs | null;
  senderMsisdn?: string | null;
  senderProfile?: string | null;
  serviceType?: string | null;
  commissionPaymentStatus?: CommissionPaymentStatus | null;
  commissioningPlanType?: CommissioningPlanType | null;
  globalNetworkCommissionAmount?: number | null;
  transactionsAmount?: number | null;
  fraudAmount?: number | null;
  spare1?: string | null;
  spare2?: string | null;
  spare3?: string | null;
  commissionAccount?: ICommissionAccount | null;
  transactionOperations?: ITransactionOperation[] | null;
  configurationPlan?: IConfigurationPlan | null;
  payment?: IPayment | null;
}

export class Commission implements ICommission {
  constructor(
    public id?: number,
    public amount?: number,
    public calculatedAt?: dayjs.Dayjs | null,
    public calculationDate?: dayjs.Dayjs | null,
    public calculationShortDate?: dayjs.Dayjs | null,
    public senderMsisdn?: string | null,
    public senderProfile?: string | null,
    public serviceType?: string | null,
    public commissionPaymentStatus?: CommissionPaymentStatus | null,
    public commissioningPlanType?: CommissioningPlanType | null,
    public globalNetworkCommissionAmount?: number | null,
    public transactionsAmount?: number | null,
    public fraudAmount?: number | null,
    public spare1?: string | null,
    public spare2?: string | null,
    public spare3?: string | null,
    public commissionAccount?: ICommissionAccount | null,
    public transactionOperations?: ITransactionOperation[] | null,
    public configurationPlan?: IConfigurationPlan | null,
    public payment?: IPayment | null
  ) {}
}

// Interface use to export file
export interface ICommissionExportFile {
  id?: number;
  amount?: number;
  calculatedAt?: dayjs.Dayjs | null;
  senderMsisdn?: string | null;
  senderProfile?: string | null;
  serviceType?: string | null;
  commissionPaymentStatus?: CommissionPaymentStatus | null;
}

// Class use to export file
export class CommissionExportFile implements ICommissionExportFile {
  constructor(
    public id?: number,
    public amount?: number,
    public calculatedAt?: dayjs.Dayjs | null,
    public senderMsisdn?: string | null,
    public senderProfile?: string | null,
    public serviceType?: string | null,
    public commissionPaymentStatus?: CommissionPaymentStatus | null
  ) {}
}

export function getCommissionIdentifier(commission: ICommission): number | undefined {
  return commission.id;
}
