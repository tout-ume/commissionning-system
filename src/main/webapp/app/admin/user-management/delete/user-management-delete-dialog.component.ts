import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { User } from '../user-management.model';
import { UserManagementService } from '../service/user-management.service';

@Component({
  selector: 'jhi-user-mgmt-delete-dialog',
  templateUrl: './user-management-delete-dialog.component.html',
})
export class UserManagementDeleteDialogComponent {
  user?: User;
  isDeletion?: boolean;

  constructor(private userService: UserManagementService, private activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmOperation(login: string): void {
    this.isDeletion
      ? this.userService.delete(login).subscribe(() => {
          this.activeModal.close('deleted');
        })
      : this.userService.deactivate(login).subscribe(() => {
          this.activeModal.close('deactivated');
        });
  }
}
