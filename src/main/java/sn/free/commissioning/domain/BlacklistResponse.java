package sn.free.commissioning.domain;

import java.util.List;
import sn.free.commissioning.domain.enumeration.UploadStatus;
import sn.free.commissioning.service.dto.BlacklistedDTO;

public class BlacklistResponse {

    private List<BlacklistedDTO> content;
    private UploadStatus status;
    private String message;

    public BlacklistResponse(List<BlacklistedDTO> content, UploadStatus status, String message) {
        this.content = content;
        this.status = status;
        this.message = message;
    }

    public BlacklistResponse() {}

    public List<BlacklistedDTO> getContent() {
        return content;
    }

    public void setContent(List<BlacklistedDTO> content) {
        this.content = content;
    }

    public UploadStatus getStatus() {
        return status;
    }

    public void setStatus(UploadStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
