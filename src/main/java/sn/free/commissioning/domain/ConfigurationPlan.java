package sn.free.commissioning.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ConfigurationPlan.
 */
@Entity
@Table(name = "configuration_plan")
@NamedEntityGraph(
    name = "graph.ConfigurationPlan",
    attributeNodes = { @NamedAttributeNode("commissionTypes"), @NamedAttributeNode("zones") }
)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ConfigurationPlan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "spare_1")
    private String spare1;

    @Column(name = "spare_2")
    private String spare2;

    @Column(name = "spare_3")
    private String spare3;

    @OneToMany(mappedBy = "configurationPlan")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "configurationPlan" }, allowSetters = true)
    private Set<CommissionType> commissionTypes = new HashSet<>();

    @OneToMany(mappedBy = "configurationPlan")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "commissionAccount", "transactionOperations", "configurationPlan", "payment" }, allowSetters = true)
    private Set<Commission> commissions = new HashSet<>();

    @ManyToOne
    private PartnerProfile partnerProfile;

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "paymentFrequency", "calculusFrequency", "calculusPeriod", "paymentPeriod", "services", "configurationPlans" },
        allowSetters = true
    )
    private CommissioningPlan commissioningPlan;

    @ManyToMany
    @JoinTable(
        name = "rel_configuration_plan__zones",
        joinColumns = @JoinColumn(name = "configuration_plan_id"),
        inverseJoinColumns = @JoinColumn(name = "zones_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "territory", "configurationPlans", "partners" }, allowSetters = true)
    private Set<Zone> zones = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ConfigurationPlan id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSpare1() {
        return this.spare1;
    }

    public ConfigurationPlan spare1(String spare1) {
        this.setSpare1(spare1);
        return this;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return this.spare2;
    }

    public ConfigurationPlan spare2(String spare2) {
        this.setSpare2(spare2);
        return this;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return this.spare3;
    }

    public ConfigurationPlan spare3(String spare3) {
        this.setSpare3(spare3);
        return this;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public Set<CommissionType> getCommissionTypes() {
        return this.commissionTypes;
    }

    public void setCommissionTypes(Set<CommissionType> commissionTypes) {
        if (this.commissionTypes != null) {
            this.commissionTypes.forEach(i -> i.setConfigurationPlan(null));
        }
        if (commissionTypes != null) {
            commissionTypes.forEach(i -> i.setConfigurationPlan(this));
        }
        this.commissionTypes = commissionTypes;
    }

    public ConfigurationPlan commissionTypes(Set<CommissionType> commissionTypes) {
        this.setCommissionTypes(commissionTypes);
        return this;
    }

    public ConfigurationPlan addCommissionType(CommissionType commissionType) {
        this.commissionTypes.add(commissionType);
        commissionType.setConfigurationPlan(this);
        return this;
    }

    public ConfigurationPlan removeCommissionType(CommissionType commissionType) {
        this.commissionTypes.remove(commissionType);
        commissionType.setConfigurationPlan(null);
        return this;
    }

    public Set<Commission> getCommissions() {
        return this.commissions;
    }

    public void setCommissions(Set<Commission> commissions) {
        if (this.commissions != null) {
            this.commissions.forEach(i -> i.setConfigurationPlan(null));
        }
        if (commissions != null) {
            commissions.forEach(i -> i.setConfigurationPlan(this));
        }
        this.commissions = commissions;
    }

    public ConfigurationPlan commissions(Set<Commission> commissions) {
        this.setCommissions(commissions);
        return this;
    }

    public ConfigurationPlan addCommission(Commission commission) {
        this.commissions.add(commission);
        commission.setConfigurationPlan(this);
        return this;
    }

    public ConfigurationPlan removeCommission(Commission commission) {
        this.commissions.remove(commission);
        commission.setConfigurationPlan(null);
        return this;
    }

    public PartnerProfile getPartnerProfile() {
        return this.partnerProfile;
    }

    public void setPartnerProfile(PartnerProfile partnerProfile) {
        this.partnerProfile = partnerProfile;
    }

    public ConfigurationPlan partnerProfile(PartnerProfile partnerProfile) {
        this.setPartnerProfile(partnerProfile);
        return this;
    }

    public CommissioningPlan getCommissioningPlan() {
        return this.commissioningPlan;
    }

    public void setCommissioningPlan(CommissioningPlan commissioningPlan) {
        this.commissioningPlan = commissioningPlan;
    }

    public ConfigurationPlan commissioningPlan(CommissioningPlan commissioningPlan) {
        this.setCommissioningPlan(commissioningPlan);
        return this;
    }

    public Set<Zone> getZones() {
        return this.zones;
    }

    public void setZones(Set<Zone> zones) {
        this.zones = zones;
    }

    public ConfigurationPlan zones(Set<Zone> zones) {
        this.setZones(zones);
        return this;
    }

    public ConfigurationPlan addZones(Zone zone) {
        this.zones.add(zone);
        zone.getConfigurationPlans().add(this);
        return this;
    }

    public ConfigurationPlan removeZones(Zone zone) {
        this.zones.remove(zone);
        zone.getConfigurationPlans().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ConfigurationPlan)) {
            return false;
        }
        return id != null && id.equals(((ConfigurationPlan) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ConfigurationPlan{" +
            "id=" + getId() +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            "}";
    }
}
