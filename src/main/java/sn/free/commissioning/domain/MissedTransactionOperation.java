package sn.free.commissioning.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A MissedTransactionOperation.
 */
@Entity
@Table(name = "missed_transaction_operation")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MissedTransactionOperation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "subs_msisdn")
    private String subsMsisdn;

    @Column(name = "agent_msisdn")
    private String agentMsisdn;

    @Column(name = "type_transaction")
    private String typeTransaction;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "transaction_status")
    private String transactionStatus;

    @Column(name = "sender_zone")
    private String senderZone;

    @Column(name = "sender_profile")
    private String senderProfile;

    @Column(name = "code_territory")
    private String codeTerritory;

    @Column(name = "sub_type")
    private String subType;

    @Column(name = "operation_date")
    private Instant operationDate;

    @Column(name = "is_fraud")
    private Boolean isFraud;

    @Column(name = "tagged_at")
    private Instant taggedAt;

    @Column(name = "fraud_source")
    private String fraudSource;

    @Column(name = "comment")
    private String comment;

    @Column(name = "false_positive_detected_at")
    private Instant falsePositiveDetectedAt;

    @Column(name = "tid")
    private String tid;

    @Column(name = "parent_msisdn")
    private String parentMsisdn;

    @Column(name = "fct_dt")
    private String fctDt;

    @Column(name = "parent_id")
    private String parentId;

    @Column(name = "canceled_at")
    private Instant canceledAt;

    @Column(name = "canceled_id")
    private String canceledId;

    @Column(name = "product_id")
    private String productId;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public MissedTransactionOperation id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return this.amount;
    }

    public MissedTransactionOperation amount(Double amount) {
        this.setAmount(amount);
        return this;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getSubsMsisdn() {
        return this.subsMsisdn;
    }

    public MissedTransactionOperation subsMsisdn(String subsMsisdn) {
        this.setSubsMsisdn(subsMsisdn);
        return this;
    }

    public void setSubsMsisdn(String subsMsisdn) {
        this.subsMsisdn = subsMsisdn;
    }

    public String getAgentMsisdn() {
        return this.agentMsisdn;
    }

    public MissedTransactionOperation agentMsisdn(String agentMsisdn) {
        this.setAgentMsisdn(agentMsisdn);
        return this;
    }

    public void setAgentMsisdn(String agentMsisdn) {
        this.agentMsisdn = agentMsisdn;
    }

    public String getTypeTransaction() {
        return this.typeTransaction;
    }

    public MissedTransactionOperation typeTransaction(String typeTransaction) {
        this.setTypeTransaction(typeTransaction);
        return this;
    }

    public void setTypeTransaction(String typeTransaction) {
        this.typeTransaction = typeTransaction;
    }

    public Instant getCreatedAt() {
        return this.createdAt;
    }

    public MissedTransactionOperation createdAt(Instant createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getTransactionStatus() {
        return this.transactionStatus;
    }

    public MissedTransactionOperation transactionStatus(String transactionStatus) {
        this.setTransactionStatus(transactionStatus);
        return this;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getSenderZone() {
        return this.senderZone;
    }

    public MissedTransactionOperation senderZone(String senderZone) {
        this.setSenderZone(senderZone);
        return this;
    }

    public void setSenderZone(String senderZone) {
        this.senderZone = senderZone;
    }

    public String getSenderProfile() {
        return this.senderProfile;
    }

    public MissedTransactionOperation senderProfile(String senderProfile) {
        this.setSenderProfile(senderProfile);
        return this;
    }

    public void setSenderProfile(String senderProfile) {
        this.senderProfile = senderProfile;
    }

    public String getCodeTerritory() {
        return this.codeTerritory;
    }

    public MissedTransactionOperation codeTerritory(String codeTerritory) {
        this.setCodeTerritory(codeTerritory);
        return this;
    }

    public void setCodeTerritory(String codeTerritory) {
        this.codeTerritory = codeTerritory;
    }

    public String getSubType() {
        return this.subType;
    }

    public MissedTransactionOperation subType(String subType) {
        this.setSubType(subType);
        return this;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public Instant getOperationDate() {
        return this.operationDate;
    }

    public MissedTransactionOperation operationDate(Instant operationDate) {
        this.setOperationDate(operationDate);
        return this;
    }

    public void setOperationDate(Instant operationDate) {
        this.operationDate = operationDate;
    }

    public Boolean getIsFraud() {
        return this.isFraud;
    }

    public MissedTransactionOperation isFraud(Boolean isFraud) {
        this.setIsFraud(isFraud);
        return this;
    }

    public void setIsFraud(Boolean isFraud) {
        this.isFraud = isFraud;
    }

    public Instant getTaggedAt() {
        return this.taggedAt;
    }

    public MissedTransactionOperation taggedAt(Instant taggedAt) {
        this.setTaggedAt(taggedAt);
        return this;
    }

    public void setTaggedAt(Instant taggedAt) {
        this.taggedAt = taggedAt;
    }

    public String getFraudSource() {
        return this.fraudSource;
    }

    public MissedTransactionOperation fraudSource(String fraudSource) {
        this.setFraudSource(fraudSource);
        return this;
    }

    public void setFraudSource(String fraudSource) {
        this.fraudSource = fraudSource;
    }

    public String getComment() {
        return this.comment;
    }

    public MissedTransactionOperation comment(String comment) {
        this.setComment(comment);
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Instant getFalsePositiveDetectedAt() {
        return this.falsePositiveDetectedAt;
    }

    public MissedTransactionOperation falsePositiveDetectedAt(Instant falsePositiveDetectedAt) {
        this.setFalsePositiveDetectedAt(falsePositiveDetectedAt);
        return this;
    }

    public void setFalsePositiveDetectedAt(Instant falsePositiveDetectedAt) {
        this.falsePositiveDetectedAt = falsePositiveDetectedAt;
    }

    public String getTid() {
        return this.tid;
    }

    public MissedTransactionOperation tid(String tid) {
        this.setTid(tid);
        return this;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getParentMsisdn() {
        return this.parentMsisdn;
    }

    public MissedTransactionOperation parentMsisdn(String parentMsisdn) {
        this.setParentMsisdn(parentMsisdn);
        return this;
    }

    public void setParentMsisdn(String parentMsisdn) {
        this.parentMsisdn = parentMsisdn;
    }

    public String getFctDt() {
        return this.fctDt;
    }

    public MissedTransactionOperation fctDt(String fctDt) {
        this.setFctDt(fctDt);
        return this;
    }

    public void setFctDt(String fctDt) {
        this.fctDt = fctDt;
    }

    public String getParentId() {
        return this.parentId;
    }

    public MissedTransactionOperation parentId(String parentId) {
        this.setParentId(parentId);
        return this;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Instant getCanceledAt() {
        return this.canceledAt;
    }

    public MissedTransactionOperation canceledAt(Instant canceledAt) {
        this.setCanceledAt(canceledAt);
        return this;
    }

    public void setCanceledAt(Instant canceledAt) {
        this.canceledAt = canceledAt;
    }

    public String getCanceledId() {
        return this.canceledId;
    }

    public MissedTransactionOperation canceledId(String canceledId) {
        this.setCanceledId(canceledId);
        return this;
    }

    public void setCanceledId(String canceledId) {
        this.canceledId = canceledId;
    }

    public String getProductId() {
        return this.productId;
    }

    public MissedTransactionOperation productId(String productId) {
        this.setProductId(productId);
        return this;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MissedTransactionOperation)) {
            return false;
        }
        return id != null && id.equals(((MissedTransactionOperation) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MissedTransactionOperation{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", subsMsisdn='" + getSubsMsisdn() + "'" +
            ", agentMsisdn='" + getAgentMsisdn() + "'" +
            ", typeTransaction='" + getTypeTransaction() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", transactionStatus='" + getTransactionStatus() + "'" +
            ", senderZone='" + getSenderZone() + "'" +
            ", senderProfile='" + getSenderProfile() + "'" +
            ", codeTerritory='" + getCodeTerritory() + "'" +
            ", subType='" + getSubType() + "'" +
            ", operationDate='" + getOperationDate() + "'" +
            ", isFraud='" + getIsFraud() + "'" +
            ", taggedAt='" + getTaggedAt() + "'" +
            ", fraudSource='" + getFraudSource() + "'" +
            ", comment='" + getComment() + "'" +
            ", falsePositiveDetectedAt='" + getFalsePositiveDetectedAt() + "'" +
            ", tid='" + getTid() + "'" +
            ", parentMsisdn='" + getParentMsisdn() + "'" +
            ", fctDt='" + getFctDt() + "'" +
            ", parentId='" + getParentId() + "'" +
            ", canceledAt='" + getCanceledAt() + "'" +
            ", canceledId='" + getCanceledId() + "'" +
            ", productId='" + getProductId() + "'" +
            "}";
    }
}
