package sn.free.commissioning.domain.exceptions;

public class NoCommissionPlanException extends Exception {

    public NoCommissionPlanException(String message) {
        super(message);
    }

    public NoCommissionPlanException() {}
}
