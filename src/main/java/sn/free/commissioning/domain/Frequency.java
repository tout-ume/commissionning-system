package sn.free.commissioning.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import sn.free.commissioning.calculus_engine.listeners.CreateFrequencyListener;
import sn.free.commissioning.domain.enumeration.Cycle;
import sn.free.commissioning.domain.enumeration.FrequencyType;
import sn.free.commissioning.domain.enumeration.OperationType;
import sn.free.commissioning.domain.enumeration.PeriodOfOccurrence;

/**
 * A Frequency.
 */
@Entity
@EntityListeners(CreateFrequencyListener.class)
@Table(name = "frequency")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Frequency implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private FrequencyType type;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "operation_type", nullable = false)
    private OperationType operationType;

    @NotNull
    @Column(name = "execution_time", nullable = false)
    private Integer executionTime;

    @Min(value = 1)
    @Max(value = 7)
    @Column(name = "execution_week_day")
    private Integer executionWeekDay;

    @Min(value = 1)
    @Max(value = 28)
    @Column(name = "execution_month_day")
    private Integer executionMonthDay;

    @Min(value = 0)
    @Max(value = 31)
    @Column(name = "days_after")
    private Integer daysAfter;

    @NotNull
    @Size(max = 50)
    @Column(name = "created_by", length = 50, nullable = false)
    private String createdBy;

    @NotNull
    @Column(name = "created_date", nullable = false)
    private Instant createdDate;

    @Size(max = 50)
    @Column(name = "last_modified_by", length = 50)
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "cycle")
    private Cycle cycle;

    @Enumerated(EnumType.STRING)
    @Column(name = "period_of_occurrence")
    private PeriodOfOccurrence periodOfOccurrence;

    @Column(name = "number_of_cycle")
    private Integer numberOfCycle;

    @Column(name = "occurrence_by_period")
    private Integer occurrenceByPeriod;

    @Column(name = "dates_of_occurrence")
    private String datesOfOccurrence;

    @Column(name = "spare_1")
    private String spare1;

    @Column(name = "spare_2")
    private String spare2;

    @Column(name = "spare_3")
    private String spare3;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Frequency id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public Frequency name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FrequencyType getType() {
        return this.type;
    }

    public Frequency type(FrequencyType type) {
        this.setType(type);
        return this;
    }

    public void setType(FrequencyType type) {
        this.type = type;
    }

    public OperationType getOperationType() {
        return this.operationType;
    }

    public Frequency operationType(OperationType operationType) {
        this.setOperationType(operationType);
        return this;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public Integer getExecutionTime() {
        return this.executionTime;
    }

    public Frequency executionTime(Integer executionTime) {
        this.setExecutionTime(executionTime);
        return this;
    }

    public void setExecutionTime(Integer executionTime) {
        this.executionTime = executionTime;
    }

    public Integer getExecutionWeekDay() {
        return this.executionWeekDay;
    }

    public Frequency executionWeekDay(Integer executionWeekDay) {
        this.setExecutionWeekDay(executionWeekDay);
        return this;
    }

    public void setExecutionWeekDay(Integer executionWeekDay) {
        this.executionWeekDay = executionWeekDay;
    }

    public Integer getExecutionMonthDay() {
        return this.executionMonthDay;
    }

    public Frequency executionMonthDay(Integer executionMonthDay) {
        this.setExecutionMonthDay(executionMonthDay);
        return this;
    }

    public void setExecutionMonthDay(Integer executionMonthDay) {
        this.executionMonthDay = executionMonthDay;
    }

    public Integer getDaysAfter() {
        return this.daysAfter;
    }

    public Frequency daysAfter(Integer daysAfter) {
        this.setDaysAfter(daysAfter);
        return this;
    }

    public void setDaysAfter(Integer daysAfter) {
        this.daysAfter = daysAfter;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Frequency createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public Frequency createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public Frequency lastModifiedBy(String lastModifiedBy) {
        this.setLastModifiedBy(lastModifiedBy);
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return this.lastModifiedDate;
    }

    public Frequency lastModifiedDate(Instant lastModifiedDate) {
        this.setLastModifiedDate(lastModifiedDate);
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Cycle getCycle() {
        return this.cycle;
    }

    public Frequency cycle(Cycle cycle) {
        this.setCycle(cycle);
        return this;
    }

    public void setCycle(Cycle cycle) {
        this.cycle = cycle;
    }

    public PeriodOfOccurrence getPeriodOfOccurrence() {
        return this.periodOfOccurrence;
    }

    public Frequency periodOfOccurrence(PeriodOfOccurrence periodOfOccurrence) {
        this.setPeriodOfOccurrence(periodOfOccurrence);
        return this;
    }

    public void setPeriodOfOccurrence(PeriodOfOccurrence periodOfOccurrence) {
        this.periodOfOccurrence = periodOfOccurrence;
    }

    public Integer getNumberOfCycle() {
        return this.numberOfCycle;
    }

    public Frequency numberOfCycle(Integer numberOfCycle) {
        this.setNumberOfCycle(numberOfCycle);
        return this;
    }

    public void setNumberOfCycle(Integer numberOfCycle) {
        this.numberOfCycle = numberOfCycle;
    }

    public Integer getOccurrenceByPeriod() {
        return this.occurrenceByPeriod;
    }

    public Frequency occurrenceByPeriod(Integer occurrenceByPeriod) {
        this.setOccurrenceByPeriod(occurrenceByPeriod);
        return this;
    }

    public void setOccurrenceByPeriod(Integer occurrenceByPeriod) {
        this.occurrenceByPeriod = occurrenceByPeriod;
    }

    public String getDatesOfOccurrence() {
        return this.datesOfOccurrence;
    }

    public Frequency datesOfOccurrence(String datesOfOccurrence) {
        this.setDatesOfOccurrence(datesOfOccurrence);
        return this;
    }

    public void setDatesOfOccurrence(String datesOfOccurrence) {
        this.datesOfOccurrence = datesOfOccurrence;
    }

    public String getSpare1() {
        return this.spare1;
    }

    public Frequency spare1(String spare1) {
        this.setSpare1(spare1);
        return this;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return this.spare2;
    }

    public Frequency spare2(String spare2) {
        this.setSpare2(spare2);
        return this;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return this.spare3;
    }

    public Frequency spare3(String spare3) {
        this.setSpare3(spare3);
        return this;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Frequency)) {
            return false;
        }
        return id != null && id.equals(((Frequency) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Frequency{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", type='" + getType() + "'" +
            ", operationType='" + getOperationType() + "'" +
            ", executionTime=" + getExecutionTime() +
            ", executionWeekDay=" + getExecutionWeekDay() +
            ", executionMonthDay=" + getExecutionMonthDay() +
            ", daysAfter=" + getDaysAfter() +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", cycle='" + getCycle() + "'" +
            ", periodOfOccurrence='" + getPeriodOfOccurrence() + "'" +
            ", numberOfCycle=" + getNumberOfCycle() +
            ", occurrenceByPeriod=" + getOccurrenceByPeriod() +
            ", datesOfOccurrence='" + getDatesOfOccurrence() + "'" +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            "}";
    }
}
