package sn.free.commissioning.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A TransactionOperation.
 */
@Entity
@Table(name = "transaction_operation")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TransactionOperation implements Serializable {

    public static TransactionOperation fromMissedTransaction(MissedTransactionOperation missedTransactionOperation) {
        TransactionOperation transactionOpeartion = new TransactionOperation();
        transactionOpeartion.setAmount(missedTransactionOperation.getAmount());
        transactionOpeartion.setAgentMsisdn(missedTransactionOperation.getAgentMsisdn());
        transactionOpeartion.setComment(missedTransactionOperation.getComment());
        transactionOpeartion.setCanceledAt(missedTransactionOperation.getCanceledAt());
        transactionOpeartion.setCreatedAt(missedTransactionOperation.getCreatedAt());
        transactionOpeartion.setCodeTerritory(missedTransactionOperation.getCodeTerritory());
        transactionOpeartion.setCanceledId(missedTransactionOperation.getCanceledId());
        transactionOpeartion.setFalsePositiveDetectedAt(missedTransactionOperation.getFalsePositiveDetectedAt());
        transactionOpeartion.setFctDt(missedTransactionOperation.getFctDt());
        transactionOpeartion.setFraudSource(missedTransactionOperation.getFraudSource());
        transactionOpeartion.setIsFraud(missedTransactionOperation.getIsFraud());
        transactionOpeartion.setOperationDate(missedTransactionOperation.getOperationDate());
        transactionOpeartion.setParentId(missedTransactionOperation.getParentId());
        transactionOpeartion.setParentMsisdn(missedTransactionOperation.getParentMsisdn());
        transactionOpeartion.setProductId(missedTransactionOperation.getProductId());
        transactionOpeartion.setSenderProfile(missedTransactionOperation.getSenderProfile());
        transactionOpeartion.setSenderZone(missedTransactionOperation.getSenderZone());
        transactionOpeartion.setSubsMsisdn(missedTransactionOperation.getSubsMsisdn());
        transactionOpeartion.setSubType(missedTransactionOperation.getSubType());
        transactionOpeartion.setTaggedAt(missedTransactionOperation.getTaggedAt());
        transactionOpeartion.setTid(missedTransactionOperation.getTid());
        transactionOpeartion.setTransactionStatus(missedTransactionOperation.getTransactionStatus());
        transactionOpeartion.setTypeTransaction(missedTransactionOperation.getTypeTransaction());
        transactionOpeartion.setTid(missedTransactionOperation.getTid());
        return transactionOpeartion;
    }

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "subs_msisdn")
    private String subsMsisdn;

    @Column(name = "agent_msisdn")
    private String agentMsisdn;

    @Column(name = "type_transaction")
    private String typeTransaction;

    @Column(name = "created_at")
    private Instant createdAt;

    @Column(name = "transaction_status")
    private String transactionStatus;

    @Column(name = "sender_zone")
    private String senderZone;

    @Column(name = "sender_profile")
    private String senderProfile;

    @Column(name = "code_territory")
    private String codeTerritory;

    @Column(name = "sub_type")
    private String subType;

    @Column(name = "operation_short_date")
    private LocalDate operationShortDate;

    @Column(name = "operation_date")
    private Instant operationDate;

    @Column(name = "is_fraud")
    private Boolean isFraud;

    @Column(name = "tagged_at")
    private Instant taggedAt;

    @Column(name = "fraud_source")
    private String fraudSource;

    @Column(name = "comment")
    private String comment;

    @Column(name = "false_positive_detected_at")
    private Instant falsePositiveDetectedAt;

    @Column(name = "tid")
    private String tid;

    @Column(name = "parent_msisdn")
    private String parentMsisdn;

    @Column(name = "fct_dt")
    private String fctDt;

    @Column(name = "parent_id")
    private String parentId;

    @Column(name = "canceled_at")
    private Instant canceledAt;

    @Column(name = "canceled_id")
    private String canceledId;

    @Column(name = "product_id")
    private String productId;

    @ManyToMany(mappedBy = "transactionOperations")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "commissionAccount", "transactionOperations", "configurationPlan", "payment" }, allowSetters = true)
    private Set<Commission> commissions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public TransactionOperation id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return this.amount;
    }

    public TransactionOperation amount(Double amount) {
        this.setAmount(amount);
        return this;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getSubsMsisdn() {
        return this.subsMsisdn;
    }

    public TransactionOperation subsMsisdn(String subsMsisdn) {
        this.setSubsMsisdn(subsMsisdn);
        return this;
    }

    public void setSubsMsisdn(String subsMsisdn) {
        this.subsMsisdn = subsMsisdn;
    }

    public String getAgentMsisdn() {
        return this.agentMsisdn;
    }

    public TransactionOperation agentMsisdn(String agentMsisdn) {
        this.setAgentMsisdn(agentMsisdn);
        return this;
    }

    public void setAgentMsisdn(String agentMsisdn) {
        this.agentMsisdn = agentMsisdn;
    }

    public String getTypeTransaction() {
        return this.typeTransaction;
    }

    public TransactionOperation typeTransaction(String typeTransaction) {
        this.setTypeTransaction(typeTransaction);
        return this;
    }

    public void setTypeTransaction(String typeTransaction) {
        this.typeTransaction = typeTransaction;
    }

    public Instant getCreatedAt() {
        return this.createdAt;
    }

    public TransactionOperation createdAt(Instant createdAt) {
        this.setCreatedAt(createdAt);
        return this;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getTransactionStatus() {
        return this.transactionStatus;
    }

    public TransactionOperation transactionStatus(String transactionStatus) {
        this.setTransactionStatus(transactionStatus);
        return this;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getSenderZone() {
        return this.senderZone;
    }

    public TransactionOperation senderZone(String senderZone) {
        this.setSenderZone(senderZone);
        return this;
    }

    public void setSenderZone(String senderZone) {
        this.senderZone = senderZone;
    }

    public String getSenderProfile() {
        return this.senderProfile;
    }

    public TransactionOperation senderProfile(String senderProfile) {
        this.setSenderProfile(senderProfile);
        return this;
    }

    public void setSenderProfile(String senderProfile) {
        this.senderProfile = senderProfile;
    }

    public String getCodeTerritory() {
        return this.codeTerritory;
    }

    public TransactionOperation codeTerritory(String codeTerritory) {
        this.setCodeTerritory(codeTerritory);
        return this;
    }

    public void setCodeTerritory(String codeTerritory) {
        this.codeTerritory = codeTerritory;
    }

    public String getSubType() {
        return this.subType;
    }

    public TransactionOperation subType(String subType) {
        this.setSubType(subType);
        return this;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public LocalDate getOperationShortDate() {
        return this.operationShortDate;
    }

    public TransactionOperation operationShortDate(LocalDate operationShortDate) {
        this.setOperationShortDate(operationShortDate);
        return this;
    }

    public void setOperationShortDate(LocalDate operationShortDate) {
        this.operationShortDate = operationShortDate;
    }

    public Instant getOperationDate() {
        return this.operationDate;
    }

    public TransactionOperation operationDate(Instant operationDate) {
        this.setOperationDate(operationDate);
        return this;
    }

    public void setOperationDate(Instant operationDate) {
        this.operationDate = operationDate;
    }

    public Boolean getIsFraud() {
        return this.isFraud;
    }

    public TransactionOperation isFraud(Boolean isFraud) {
        this.setIsFraud(isFraud);
        return this;
    }

    public void setIsFraud(Boolean isFraud) {
        this.isFraud = isFraud;
    }

    public Instant getTaggedAt() {
        return this.taggedAt;
    }

    public TransactionOperation taggedAt(Instant taggedAt) {
        this.setTaggedAt(taggedAt);
        return this;
    }

    public void setTaggedAt(Instant taggedAt) {
        this.taggedAt = taggedAt;
    }

    public String getFraudSource() {
        return this.fraudSource;
    }

    public TransactionOperation fraudSource(String fraudSource) {
        this.setFraudSource(fraudSource);
        return this;
    }

    public void setFraudSource(String fraudSource) {
        this.fraudSource = fraudSource;
    }

    public String getComment() {
        return this.comment;
    }

    public TransactionOperation comment(String comment) {
        this.setComment(comment);
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Instant getFalsePositiveDetectedAt() {
        return this.falsePositiveDetectedAt;
    }

    public TransactionOperation falsePositiveDetectedAt(Instant falsePositiveDetectedAt) {
        this.setFalsePositiveDetectedAt(falsePositiveDetectedAt);
        return this;
    }

    public void setFalsePositiveDetectedAt(Instant falsePositiveDetectedAt) {
        this.falsePositiveDetectedAt = falsePositiveDetectedAt;
    }

    public String getTid() {
        return this.tid;
    }

    public TransactionOperation tid(String tid) {
        this.setTid(tid);
        return this;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getParentMsisdn() {
        return this.parentMsisdn;
    }

    public TransactionOperation parentMsisdn(String parentMsisdn) {
        this.setParentMsisdn(parentMsisdn);
        return this;
    }

    public void setParentMsisdn(String parentMsisdn) {
        this.parentMsisdn = parentMsisdn;
    }

    public String getFctDt() {
        return this.fctDt;
    }

    public TransactionOperation fctDt(String fctDt) {
        this.setFctDt(fctDt);
        return this;
    }

    public void setFctDt(String fctDt) {
        this.fctDt = fctDt;
    }

    public String getParentId() {
        return this.parentId;
    }

    public TransactionOperation parentId(String parentId) {
        this.setParentId(parentId);
        return this;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Instant getCanceledAt() {
        return this.canceledAt;
    }

    public TransactionOperation canceledAt(Instant canceledAt) {
        this.setCanceledAt(canceledAt);
        return this;
    }

    public void setCanceledAt(Instant canceledAt) {
        this.canceledAt = canceledAt;
    }

    public String getCanceledId() {
        return this.canceledId;
    }

    public TransactionOperation canceledId(String canceledId) {
        this.setCanceledId(canceledId);
        return this;
    }

    public void setCanceledId(String canceledId) {
        this.canceledId = canceledId;
    }

    public String getProductId() {
        return this.productId;
    }

    public TransactionOperation productId(String productId) {
        this.setProductId(productId);
        return this;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Set<Commission> getCommissions() {
        return this.commissions;
    }

    public void setCommissions(Set<Commission> commissions) {
        if (this.commissions != null) {
            this.commissions.forEach(i -> i.removeTransactionOperation(this));
        }
        if (commissions != null) {
            commissions.forEach(i -> i.addTransactionOperation(this));
        }
        this.commissions = commissions;
    }

    public TransactionOperation commissions(Set<Commission> commissions) {
        this.setCommissions(commissions);
        return this;
    }

    public TransactionOperation addCommissions(Commission commission) {
        this.commissions.add(commission);
        commission.getTransactionOperations().add(this);
        return this;
    }

    public TransactionOperation removeCommissions(Commission commission) {
        this.commissions.remove(commission);
        commission.getTransactionOperations().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof TransactionOperation)) {
            return false;
        }
        return id != null && id.equals(((TransactionOperation) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "TransactionOperation{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", subsMsisdn='" + getSubsMsisdn() + "'" +
            ", agentMsisdn='" + getAgentMsisdn() + "'" +
            ", typeTransaction='" + getTypeTransaction() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", transactionStatus='" + getTransactionStatus() + "'" +
            ", senderZone='" + getSenderZone() + "'" +
            ", senderProfile='" + getSenderProfile() + "'" +
            ", codeTerritory='" + getCodeTerritory() + "'" +
            ", subType='" + getSubType() + "'" +
            ", operationShortDate='" + getOperationShortDate() + "'" +
            ", operationDate='" + getOperationDate() + "'" +
            ", isFraud='" + getIsFraud() + "'" +
            ", taggedAt='" + getTaggedAt() + "'" +
            ", fraudSource='" + getFraudSource() + "'" +
            ", comment='" + getComment() + "'" +
            ", falsePositiveDetectedAt='" + getFalsePositiveDetectedAt() + "'" +
            ", tid='" + getTid() + "'" +
            ", parentMsisdn='" + getParentMsisdn() + "'" +
            ", fctDt='" + getFctDt() + "'" +
            ", parentId='" + getParentId() + "'" +
            ", canceledAt='" + getCanceledAt() + "'" +
            ", canceledId='" + getCanceledId() + "'" +
            ", productId='" + getProductId() + "'" +
            "}";
    }
}
