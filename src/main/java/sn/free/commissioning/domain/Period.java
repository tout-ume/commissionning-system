package sn.free.commissioning.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import sn.free.commissioning.domain.enumeration.FrequencyType;
import sn.free.commissioning.domain.enumeration.OperationType;

/**
 * A Period.
 */
@Entity
@Table(name = "period")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Period implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "month_day_from")
    private Integer monthDayFrom;

    @Column(name = "month_day_to")
    private Integer monthDayTo;

    @Column(name = "week_day_from")
    private Integer weekDayFrom;

    @Column(name = "week_day_to")
    private Integer weekDayTo;

    @Column(name = "day_hour_from")
    private Integer dayHourFrom;

    @Column(name = "day_hour_to")
    private Integer dayHourTo;

    @Column(name = "is_previous_day_hour_from")
    private Boolean isPreviousDayHourFrom;

    @Column(name = "is_previous_day_hour_to")
    private Boolean isPreviousDayHourTo;

    @Column(name = "is_previous_month_day_from")
    private Boolean isPreviousMonthDayFrom;

    @Column(name = "is_previous_month_day_to")
    private Boolean isPreviousMonthDayTo;

    @Column(name = "is_complete_day")
    private Boolean isCompleteDay;

    @Column(name = "is_complete_month")
    private Boolean isCompleteMonth;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "period_type", nullable = false)
    private FrequencyType periodType;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "operation_type", nullable = false)
    private OperationType operationType;

    @NotNull
    @Size(max = 50)
    @Column(name = "created_by", length = 50, nullable = false)
    private String createdBy;

    @NotNull
    @Column(name = "created_date", nullable = false)
    private Instant createdDate;

    @Size(max = 50)
    @Column(name = "last_modified_by", length = 50)
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Column(name = "days_or_dates_of_occurrence")
    private String daysOrDatesOfOccurrence;

    @Column(name = "spare_1")
    private String spare1;

    @Column(name = "spare_2")
    private String spare2;

    @Column(name = "spare_3")
    private String spare3;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Period id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMonthDayFrom() {
        return this.monthDayFrom;
    }

    public Period monthDayFrom(Integer monthDayFrom) {
        this.setMonthDayFrom(monthDayFrom);
        return this;
    }

    public void setMonthDayFrom(Integer monthDayFrom) {
        this.monthDayFrom = monthDayFrom;
    }

    public Integer getMonthDayTo() {
        return this.monthDayTo;
    }

    public Period monthDayTo(Integer monthDayTo) {
        this.setMonthDayTo(monthDayTo);
        return this;
    }

    public void setMonthDayTo(Integer monthDayTo) {
        this.monthDayTo = monthDayTo;
    }

    public Integer getWeekDayFrom() {
        return this.weekDayFrom;
    }

    public Period weekDayFrom(Integer weekDayFrom) {
        this.setWeekDayFrom(weekDayFrom);
        return this;
    }

    public void setWeekDayFrom(Integer weekDayFrom) {
        this.weekDayFrom = weekDayFrom;
    }

    public Integer getWeekDayTo() {
        return this.weekDayTo;
    }

    public Period weekDayTo(Integer weekDayTo) {
        this.setWeekDayTo(weekDayTo);
        return this;
    }

    public void setWeekDayTo(Integer weekDayTo) {
        this.weekDayTo = weekDayTo;
    }

    public Integer getDayHourFrom() {
        return this.dayHourFrom;
    }

    public Period dayHourFrom(Integer dayHourFrom) {
        this.setDayHourFrom(dayHourFrom);
        return this;
    }

    public void setDayHourFrom(Integer dayHourFrom) {
        this.dayHourFrom = dayHourFrom;
    }

    public Integer getDayHourTo() {
        return this.dayHourTo;
    }

    public Period dayHourTo(Integer dayHourTo) {
        this.setDayHourTo(dayHourTo);
        return this;
    }

    public void setDayHourTo(Integer dayHourTo) {
        this.dayHourTo = dayHourTo;
    }

    public Boolean getIsPreviousDayHourFrom() {
        return this.isPreviousDayHourFrom;
    }

    public Period isPreviousDayHourFrom(Boolean isPreviousDayHourFrom) {
        this.setIsPreviousDayHourFrom(isPreviousDayHourFrom);
        return this;
    }

    public void setIsPreviousDayHourFrom(Boolean isPreviousDayHourFrom) {
        this.isPreviousDayHourFrom = isPreviousDayHourFrom;
    }

    public Boolean getIsPreviousDayHourTo() {
        return this.isPreviousDayHourTo;
    }

    public Period isPreviousDayHourTo(Boolean isPreviousDayHourTo) {
        this.setIsPreviousDayHourTo(isPreviousDayHourTo);
        return this;
    }

    public void setIsPreviousDayHourTo(Boolean isPreviousDayHourTo) {
        this.isPreviousDayHourTo = isPreviousDayHourTo;
    }

    public Boolean getIsPreviousMonthDayFrom() {
        return this.isPreviousMonthDayFrom;
    }

    public Period isPreviousMonthDayFrom(Boolean isPreviousMonthDayFrom) {
        this.setIsPreviousMonthDayFrom(isPreviousMonthDayFrom);
        return this;
    }

    public void setIsPreviousMonthDayFrom(Boolean isPreviousMonthDayFrom) {
        this.isPreviousMonthDayFrom = isPreviousMonthDayFrom;
    }

    public Boolean getIsPreviousMonthDayTo() {
        return this.isPreviousMonthDayTo;
    }

    public Period isPreviousMonthDayTo(Boolean isPreviousMonthDayTo) {
        this.setIsPreviousMonthDayTo(isPreviousMonthDayTo);
        return this;
    }

    public void setIsPreviousMonthDayTo(Boolean isPreviousMonthDayTo) {
        this.isPreviousMonthDayTo = isPreviousMonthDayTo;
    }

    public Boolean getIsCompleteDay() {
        return this.isCompleteDay;
    }

    public Period isCompleteDay(Boolean isCompleteDay) {
        this.setIsCompleteDay(isCompleteDay);
        return this;
    }

    public void setIsCompleteDay(Boolean isCompleteDay) {
        this.isCompleteDay = isCompleteDay;
    }

    public Boolean getIsCompleteMonth() {
        return this.isCompleteMonth;
    }

    public Period isCompleteMonth(Boolean isCompleteMonth) {
        this.setIsCompleteMonth(isCompleteMonth);
        return this;
    }

    public void setIsCompleteMonth(Boolean isCompleteMonth) {
        this.isCompleteMonth = isCompleteMonth;
    }

    public FrequencyType getPeriodType() {
        return this.periodType;
    }

    public Period periodType(FrequencyType periodType) {
        this.setPeriodType(periodType);
        return this;
    }

    public void setPeriodType(FrequencyType periodType) {
        this.periodType = periodType;
    }

    public OperationType getOperationType() {
        return this.operationType;
    }

    public Period operationType(OperationType operationType) {
        this.setOperationType(operationType);
        return this;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Period createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public Period createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public Period lastModifiedBy(String lastModifiedBy) {
        this.setLastModifiedBy(lastModifiedBy);
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return this.lastModifiedDate;
    }

    public Period lastModifiedDate(Instant lastModifiedDate) {
        this.setLastModifiedDate(lastModifiedDate);
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getDaysOrDatesOfOccurrence() {
        return this.daysOrDatesOfOccurrence;
    }

    public Period daysOrDatesOfOccurrence(String daysOrDatesOfOccurrence) {
        this.setDaysOrDatesOfOccurrence(daysOrDatesOfOccurrence);
        return this;
    }

    public void setDaysOrDatesOfOccurrence(String daysOrDatesOfOccurrence) {
        this.daysOrDatesOfOccurrence = daysOrDatesOfOccurrence;
    }

    public String getSpare1() {
        return this.spare1;
    }

    public Period spare1(String spare1) {
        this.setSpare1(spare1);
        return this;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return this.spare2;
    }

    public Period spare2(String spare2) {
        this.setSpare2(spare2);
        return this;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return this.spare3;
    }

    public Period spare3(String spare3) {
        this.setSpare3(spare3);
        return this;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Period)) {
            return false;
        }
        return id != null && id.equals(((Period) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Period{" +
            "id=" + getId() +
            ", monthDayFrom=" + getMonthDayFrom() +
            ", monthDayTo=" + getMonthDayTo() +
            ", weekDayFrom=" + getWeekDayFrom() +
            ", weekDayTo=" + getWeekDayTo() +
            ", dayHourFrom=" + getDayHourFrom() +
            ", dayHourTo=" + getDayHourTo() +
            ", isPreviousDayHourFrom='" + getIsPreviousDayHourFrom() + "'" +
            ", isPreviousDayHourTo='" + getIsPreviousDayHourTo() + "'" +
            ", isPreviousMonthDayFrom='" + getIsPreviousMonthDayFrom() + "'" +
            ", isPreviousMonthDayTo='" + getIsPreviousMonthDayTo() + "'" +
            ", isCompleteDay='" + getIsCompleteDay() + "'" +
            ", isCompleteMonth='" + getIsCompleteMonth() + "'" +
            ", periodType='" + getPeriodType() + "'" +
            ", operationType='" + getOperationType() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", daysOrDatesOfOccurrence='" + getDaysOrDatesOfOccurrence() + "'" +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            "}";
    }
}
