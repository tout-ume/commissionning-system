package sn.free.commissioning.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import sn.free.commissioning.domain.enumeration.CommissionPaymentStatus;
import sn.free.commissioning.domain.enumeration.CommissioningPlanType;

/**
 * A Commission.
 */
@Entity
@Table(name = "commission")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Commission implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "amount", nullable = false)
    private Double amount;

    @Column(name = "calculated_at")
    private Instant calculatedAt;

    @Column(name = "calculation_date")
    private Instant calculationDate;

    @Column(name = "calculation_short_date")
    private LocalDate calculationShortDate;

    @Column(name = "sender_msisdn")
    private String senderMsisdn;

    @Column(name = "sender_profile")
    private String senderProfile;

    @Column(name = "service_type")
    private String serviceType;

    @Enumerated(EnumType.STRING)
    @Column(name = "commission_payment_status")
    private CommissionPaymentStatus commissionPaymentStatus;

    @Enumerated(EnumType.STRING)
    @Column(name = "commissioning_plan_type")
    private CommissioningPlanType commissioningPlanType;

    @Column(name = "global_network_commission_amount")
    private Double globalNetworkCommissionAmount;

    @Column(name = "transactions_amount")
    private Double transactionsAmount;

    @Column(name = "fraud_amount")
    private Double fraudAmount;

    @Column(name = "spare_1")
    private String spare1;

    @Column(name = "spare_2")
    private String spare2;

    @Column(name = "spare_3")
    private String spare3;

    @ManyToOne
    @JsonIgnoreProperties(value = { "partner", "commissions" }, allowSetters = true)
    private CommissionAccount commissionAccount;

    @ManyToMany
    @JoinTable(
        name = "rel_commission__transaction_operation",
        joinColumns = @JoinColumn(name = "commission_id"),
        inverseJoinColumns = @JoinColumn(name = "transaction_operation_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "commissions" }, allowSetters = true)
    private Set<TransactionOperation> transactionOperations = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "commissionTypes", "commissions", "partnerProfile", "commissioningPlan", "zones" }, allowSetters = true)
    private ConfigurationPlan configurationPlan;

    @ManyToOne
    @JsonIgnoreProperties(value = { "commissions" }, allowSetters = true)
    private Payment payment;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Commission id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return this.amount;
    }

    public Commission amount(Double amount) {
        this.setAmount(amount);
        return this;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Instant getCalculatedAt() {
        return this.calculatedAt;
    }

    public Commission calculatedAt(Instant calculatedAt) {
        this.setCalculatedAt(calculatedAt);
        return this;
    }

    public void setCalculatedAt(Instant calculatedAt) {
        this.calculatedAt = calculatedAt;
    }

    public Instant getCalculationDate() {
        return this.calculationDate;
    }

    public Commission calculationDate(Instant calculationDate) {
        this.setCalculationDate(calculationDate);
        return this;
    }

    public void setCalculationDate(Instant calculationDate) {
        this.calculationDate = calculationDate;
    }

    public LocalDate getCalculationShortDate() {
        return this.calculationShortDate;
    }

    public Commission calculationShortDate(LocalDate calculationShortDate) {
        this.setCalculationShortDate(calculationShortDate);
        return this;
    }

    public void setCalculationShortDate(LocalDate calculationShortDate) {
        this.calculationShortDate = calculationShortDate;
    }

    public String getSenderMsisdn() {
        return this.senderMsisdn;
    }

    public Commission senderMsisdn(String senderMsisdn) {
        this.setSenderMsisdn(senderMsisdn);
        return this;
    }

    public void setSenderMsisdn(String senderMsisdn) {
        this.senderMsisdn = senderMsisdn;
    }

    public String getSenderProfile() {
        return this.senderProfile;
    }

    public Commission senderProfile(String senderProfile) {
        this.setSenderProfile(senderProfile);
        return this;
    }

    public void setSenderProfile(String senderProfile) {
        this.senderProfile = senderProfile;
    }

    public String getServiceType() {
        return this.serviceType;
    }

    public Commission serviceType(String serviceType) {
        this.setServiceType(serviceType);
        return this;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public CommissionPaymentStatus getCommissionPaymentStatus() {
        return this.commissionPaymentStatus;
    }

    public Commission commissionPaymentStatus(CommissionPaymentStatus commissionPaymentStatus) {
        this.setCommissionPaymentStatus(commissionPaymentStatus);
        return this;
    }

    public void setCommissionPaymentStatus(CommissionPaymentStatus commissionPaymentStatus) {
        this.commissionPaymentStatus = commissionPaymentStatus;
    }

    public CommissioningPlanType getCommissioningPlanType() {
        return this.commissioningPlanType;
    }

    public Commission commissioningPlanType(CommissioningPlanType commissioningPlanType) {
        this.setCommissioningPlanType(commissioningPlanType);
        return this;
    }

    public void setCommissioningPlanType(CommissioningPlanType commissioningPlanType) {
        this.commissioningPlanType = commissioningPlanType;
    }

    public Double getGlobalNetworkCommissionAmount() {
        return this.globalNetworkCommissionAmount;
    }

    public Commission globalNetworkCommissionAmount(Double globalNetworkCommissionAmount) {
        this.setGlobalNetworkCommissionAmount(globalNetworkCommissionAmount);
        return this;
    }

    public void setGlobalNetworkCommissionAmount(Double globalNetworkCommissionAmount) {
        this.globalNetworkCommissionAmount = globalNetworkCommissionAmount;
    }

    public Double getTransactionsAmount() {
        return this.transactionsAmount;
    }

    public Commission transactionsAmount(Double transactionsAmount) {
        this.setTransactionsAmount(transactionsAmount);
        return this;
    }

    public void setTransactionsAmount(Double transactionsAmount) {
        this.transactionsAmount = transactionsAmount;
    }

    public Double getFraudAmount() {
        return this.fraudAmount;
    }

    public Commission fraudAmount(Double fraudAmount) {
        this.setFraudAmount(fraudAmount);
        return this;
    }

    public void setFraudAmount(Double fraudAmount) {
        this.fraudAmount = fraudAmount;
    }

    public String getSpare1() {
        return this.spare1;
    }

    public Commission spare1(String spare1) {
        this.setSpare1(spare1);
        return this;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return this.spare2;
    }

    public Commission spare2(String spare2) {
        this.setSpare2(spare2);
        return this;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return this.spare3;
    }

    public Commission spare3(String spare3) {
        this.setSpare3(spare3);
        return this;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public CommissionAccount getCommissionAccount() {
        return this.commissionAccount;
    }

    public void setCommissionAccount(CommissionAccount commissionAccount) {
        this.commissionAccount = commissionAccount;
    }

    public Commission commissionAccount(CommissionAccount commissionAccount) {
        this.setCommissionAccount(commissionAccount);
        return this;
    }

    public Set<TransactionOperation> getTransactionOperations() {
        return this.transactionOperations;
    }

    public void setTransactionOperations(Set<TransactionOperation> transactionOperations) {
        this.transactionOperations = transactionOperations;
    }

    public Commission transactionOperations(Set<TransactionOperation> transactionOperations) {
        this.setTransactionOperations(transactionOperations);
        return this;
    }

    public Commission addTransactionOperation(TransactionOperation transactionOperation) {
        this.transactionOperations.add(transactionOperation);
        transactionOperation.getCommissions().add(this);
        return this;
    }

    public Commission removeTransactionOperation(TransactionOperation transactionOperation) {
        this.transactionOperations.remove(transactionOperation);
        transactionOperation.getCommissions().remove(this);
        return this;
    }

    public ConfigurationPlan getConfigurationPlan() {
        return this.configurationPlan;
    }

    public void setConfigurationPlan(ConfigurationPlan configurationPlan) {
        this.configurationPlan = configurationPlan;
    }

    public Commission configurationPlan(ConfigurationPlan configurationPlan) {
        this.setConfigurationPlan(configurationPlan);
        return this;
    }

    public Payment getPayment() {
        return this.payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Commission payment(Payment payment) {
        this.setPayment(payment);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Commission)) {
            return false;
        }
        return id != null && id.equals(((Commission) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Commission{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", calculatedAt='" + getCalculatedAt() + "'" +
            ", calculationDate='" + getCalculationDate() + "'" +
            ", calculationShortDate='" + getCalculationShortDate() + "'" +
            ", senderMsisdn='" + getSenderMsisdn() + "'" +
            ", senderProfile='" + getSenderProfile() + "'" +
            ", serviceType='" + getServiceType() + "'" +
            ", commissionPaymentStatus='" + getCommissionPaymentStatus() + "'" +
            ", commissioningPlanType='" + getCommissioningPlanType() + "'" +
            ", configurationPlan='" + getConfigurationPlan() + "'" +
            ", globalNetworkCommissionAmount=" + getGlobalNetworkCommissionAmount() +
            ", transactionsAmount=" + getTransactionsAmount() +
            ", fraudAmount=" + getFraudAmount() +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            "}";
    }
}
