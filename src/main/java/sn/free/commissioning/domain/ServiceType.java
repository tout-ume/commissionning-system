package sn.free.commissioning.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Immutable;
import sn.free.commissioning.domain.enumeration.CommissioningPlanType;
import sn.free.commissioning.domain.enumeration.ServiceTypeStatus;

/**
 * A ServiceType.
 */
@Entity
@Immutable
@Table(name = "service_type")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class ServiceType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "code", nullable = false, unique = true)
    private String code;

    @Column(name = "description")
    private String description;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "state", nullable = false)
    private ServiceTypeStatus state;

    @NotNull
    @Column(name = "is_cos_related", nullable = false)
    private Boolean isCosRelated;

    @Enumerated(EnumType.STRING)
    @Column(name = "commissioning_plan_type")
    private CommissioningPlanType commissioningPlanType;

    @NotNull
    @Size(max = 50)
    @Column(name = "created_by", length = 50, nullable = false)
    private String createdBy;

    @Column(name = "created_date")
    private Instant createdDate;

    @Size(max = 50)
    @Column(name = "last_modified_by", length = 50)
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @ManyToMany(mappedBy = "services")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(
        value = { "paymentFrequency", "calculusFrequency", "calculusPeriod", "paymentPeriod", "services", "configurationPlans" },
        allowSetters = true
    )
    private Set<CommissioningPlan> commissionPlans = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ServiceType id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public ServiceType name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return this.code;
    }

    public ServiceType code(String code) {
        this.setCode(code);
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return this.description;
    }

    public ServiceType description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ServiceTypeStatus getState() {
        return this.state;
    }

    public ServiceType state(ServiceTypeStatus state) {
        this.setState(state);
        return this;
    }

    public void setState(ServiceTypeStatus state) {
        this.state = state;
    }

    public Boolean getIsCosRelated() {
        return this.isCosRelated;
    }

    public ServiceType isCosRelated(Boolean isCosRelated) {
        this.setIsCosRelated(isCosRelated);
        return this;
    }

    public void setIsCosRelated(Boolean isCosRelated) {
        this.isCosRelated = isCosRelated;
    }

    public CommissioningPlanType getCommissioningPlanType() {
        return this.commissioningPlanType;
    }

    public ServiceType commissioningPlanType(CommissioningPlanType commissioningPlanType) {
        this.setCommissioningPlanType(commissioningPlanType);
        return this;
    }

    public void setCommissioningPlanType(CommissioningPlanType commissioningPlanType) {
        this.commissioningPlanType = commissioningPlanType;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public ServiceType createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public ServiceType createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public ServiceType lastModifiedBy(String lastModifiedBy) {
        this.setLastModifiedBy(lastModifiedBy);
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return this.lastModifiedDate;
    }

    public ServiceType lastModifiedDate(Instant lastModifiedDate) {
        this.setLastModifiedDate(lastModifiedDate);
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Set<CommissioningPlan> getCommissionPlans() {
        return this.commissionPlans;
    }

    public void setCommissionPlans(Set<CommissioningPlan> commissioningPlans) {
        if (this.commissionPlans != null) {
            this.commissionPlans.forEach(i -> i.removeServices(this));
        }
        if (commissioningPlans != null) {
            commissioningPlans.forEach(i -> i.addServices(this));
        }
        this.commissionPlans = commissioningPlans;
    }

    public ServiceType commissionPlans(Set<CommissioningPlan> commissioningPlans) {
        this.setCommissionPlans(commissioningPlans);
        return this;
    }

    public ServiceType addCommissionPlan(CommissioningPlan commissioningPlan) {
        this.commissionPlans.add(commissioningPlan);
        commissioningPlan.getServices().add(this);
        return this;
    }

    public ServiceType removeCommissionPlan(CommissioningPlan commissioningPlan) {
        this.commissionPlans.remove(commissioningPlan);
        commissioningPlan.getServices().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ServiceType)) {
            return false;
        }
        return id != null && id.equals(((ServiceType) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ServiceType{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            ", description='" + getDescription() + "'" +
            ", state='" + getState() + "'" +
            ", isCosRelated='" + getIsCosRelated() + "'" +
            ", commissioningPlanType='" + getCommissioningPlanType() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            "}";
    }
}
