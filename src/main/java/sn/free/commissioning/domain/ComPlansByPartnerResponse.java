package sn.free.commissioning.domain;

import java.io.Serializable;
import java.util.List;
import sn.free.commissioning.domain.enumeration.UploadStatus;
import sn.free.commissioning.service.dto.CommissioningPlanDTO;

public class ComPlansByPartnerResponse implements Serializable {

    private UploadStatus status;
    private String message;
    private List<CommissioningPlanDTO> content;

    public ComPlansByPartnerResponse(UploadStatus status, String message, List<CommissioningPlanDTO> content) {
        this.status = status;
        this.message = message;
        this.content = content;
    }

    public ComPlansByPartnerResponse() {}

    public UploadStatus getStatus() {
        return status;
    }

    public void setStatus(UploadStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CommissioningPlanDTO> getContent() {
        return content;
    }

    public void setContent(List<CommissioningPlanDTO> content) {
        this.content = content;
    }
}
