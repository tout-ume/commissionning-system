package sn.free.commissioning.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * A CalculationExecutionTrace.
 */
@Entity
@Table(name = "calculation_execution_trace")
public class CalculationExecutionTrace implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "frequency_id", nullable = false)
    private Double frequencyId;

    @NotNull
    @Column(name = "execution_date", nullable = false)
    private Instant executionDate;

    @Column(name = "is_executed")
    private Boolean isExecuted;

    @Column(name = "execution_result")
    private String executionResult;

    @Column(name = "spare_1")
    private String spare1;

    @Column(name = "spare_2")
    private String spare2;

    @Column(name = "spare_3")
    private String spare3;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public CalculationExecutionTrace id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getFrequencyId() {
        return this.frequencyId;
    }

    public CalculationExecutionTrace frequencyId(Double frequencyId) {
        this.setFrequencyId(frequencyId);
        return this;
    }

    public void setFrequencyId(Double frequencyId) {
        this.frequencyId = frequencyId;
    }

    public Instant getExecutionDate() {
        return this.executionDate;
    }

    public CalculationExecutionTrace executionDate(Instant executionDate) {
        this.setExecutionDate(executionDate);
        return this;
    }

    public void setExecutionDate(Instant executionDate) {
        this.executionDate = executionDate;
    }

    public Boolean getIsExecuted() {
        return this.isExecuted;
    }

    public CalculationExecutionTrace isExecuted(Boolean isExecuted) {
        this.setIsExecuted(isExecuted);
        return this;
    }

    public void setIsExecuted(Boolean isExecuted) {
        this.isExecuted = isExecuted;
    }

    public String getExecutionResult() {
        return this.executionResult;
    }

    public CalculationExecutionTrace executionResult(String executionResult) {
        this.setExecutionResult(executionResult);
        return this;
    }

    public void setExecutionResult(String executionResult) {
        this.executionResult = executionResult;
    }

    public String getSpare1() {
        return this.spare1;
    }

    public CalculationExecutionTrace spare1(String spare1) {
        this.setSpare1(spare1);
        return this;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return this.spare2;
    }

    public CalculationExecutionTrace spare2(String spare2) {
        this.setSpare2(spare2);
        return this;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return this.spare3;
    }

    public CalculationExecutionTrace spare3(String spare3) {
        this.setSpare3(spare3);
        return this;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CalculationExecutionTrace)) {
            return false;
        }
        return id != null && id.equals(((CalculationExecutionTrace) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CalculationExecutionTrace{" +
            "id=" + getId() +
            ", frequencyId=" + getFrequencyId() +
            ", executionDate='" + getExecutionDate() + "'" +
            ", isExecuted='" + getIsExecuted() + "'" +
            ", executionResult='" + getExecutionResult() + "'" +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            "}";
    }
}
