package sn.free.commissioning.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import sn.free.commissioning.domain.enumeration.CommissioningPlanState;
import sn.free.commissioning.domain.enumeration.CommissioningPlanType;
import sn.free.commissioning.domain.enumeration.PaymentStrategy;

/**
 * A CommissioningPlan.
 */
@Entity
@Table(name = "commissioning_plan")
@NamedEntityGraph(
    name = "graph.CommissioningPlan",
    attributeNodes = {
        @NamedAttributeNode("services"),
        @NamedAttributeNode("calculusPeriod"),
        @NamedAttributeNode("paymentPeriod"),
        @NamedAttributeNode("calculusFrequency"),
        @NamedAttributeNode("paymentFrequency"),
        @NamedAttributeNode("configurationPlans"),
        @NamedAttributeNode(value = "configurationPlans", subgraph = "subgraph.configurationPlans"),
    },
    subgraphs = {
        @NamedSubgraph(
            name = "subgraph.configurationPlans",
            attributeNodes = { @NamedAttributeNode("zones"), @NamedAttributeNode("partnerProfile"), @NamedAttributeNode("commissionTypes") }
        ),
    }
)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CommissioningPlan implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @NotNull
    @Column(name = "begin_date", nullable = false)
    private Instant beginDate;

    @NotNull
    @Column(name = "end_date", nullable = false)
    private Instant endDate;

    @NotNull
    @Column(name = "creation_date", nullable = false)
    private Instant creationDate;

    @NotNull
    @Column(name = "created_by", nullable = false)
    private String createdBy;

    @Column(name = "archive_date")
    private Instant archiveDate;

    @Column(name = "archived_by")
    private String archivedBy;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_strategy")
    private PaymentStrategy paymentStrategy;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "state", nullable = false)
    private CommissioningPlanState state;

    @Enumerated(EnumType.STRING)
    @Column(name = "commissioning_plan_type")
    private CommissioningPlanType commissioningPlanType;

    @Column(name = "spare_1")
    private String spare1;

    @Column(name = "spare_2")
    private String spare2;

    @Column(name = "spare_3")
    private String spare3;

    @ManyToOne
    private Frequency paymentFrequency;

    @ManyToOne
    private Frequency calculusFrequency;

    @ManyToOne
    private Period calculusPeriod;

    @ManyToOne(cascade = CascadeType.ALL)
    private Period paymentPeriod;

    @ManyToMany
    @JoinTable(
        name = "rel_commissioning_plan__services",
        joinColumns = @JoinColumn(name = "commissioning_plan_id"),
        inverseJoinColumns = @JoinColumn(name = "services_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "commissionPlans" }, allowSetters = true)
    private Set<ServiceType> services = new HashSet<>();

    @OneToMany(mappedBy = "commissioningPlan")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "commissionTypes", "commissions", "partnerProfile", "commissioningPlan", "zones" }, allowSetters = true)
    private Set<ConfigurationPlan> configurationPlans = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public CommissioningPlan id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public CommissioningPlan name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getBeginDate() {
        return this.beginDate;
    }

    public CommissioningPlan beginDate(Instant beginDate) {
        this.setBeginDate(beginDate);
        return this;
    }

    public void setBeginDate(Instant beginDate) {
        this.beginDate = beginDate;
    }

    public Instant getEndDate() {
        return this.endDate;
    }

    public CommissioningPlan endDate(Instant endDate) {
        this.setEndDate(endDate);
        return this;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Instant getCreationDate() {
        return this.creationDate;
    }

    public CommissioningPlan creationDate(Instant creationDate) {
        this.setCreationDate(creationDate);
        return this;
    }

    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public CommissioningPlan createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getArchiveDate() {
        return this.archiveDate;
    }

    public CommissioningPlan archiveDate(Instant archiveDate) {
        this.setArchiveDate(archiveDate);
        return this;
    }

    public void setArchiveDate(Instant archiveDate) {
        this.archiveDate = archiveDate;
    }

    public String getArchivedBy() {
        return this.archivedBy;
    }

    public CommissioningPlan archivedBy(String archivedBy) {
        this.setArchivedBy(archivedBy);
        return this;
    }

    public void setArchivedBy(String archivedBy) {
        this.archivedBy = archivedBy;
    }

    public PaymentStrategy getPaymentStrategy() {
        return this.paymentStrategy;
    }

    public CommissioningPlan paymentStrategy(PaymentStrategy paymentStrategy) {
        this.setPaymentStrategy(paymentStrategy);
        return this;
    }

    public void setPaymentStrategy(PaymentStrategy paymentStrategy) {
        this.paymentStrategy = paymentStrategy;
    }

    public CommissioningPlanState getState() {
        return this.state;
    }

    public CommissioningPlan state(CommissioningPlanState state) {
        this.setState(state);
        return this;
    }

    public void setState(CommissioningPlanState state) {
        this.state = state;
    }

    public CommissioningPlanType getCommissioningPlanType() {
        return this.commissioningPlanType;
    }

    public CommissioningPlan commissioningPlanType(CommissioningPlanType commissioningPlanType) {
        this.setCommissioningPlanType(commissioningPlanType);
        return this;
    }

    public void setCommissioningPlanType(CommissioningPlanType commissioningPlanType) {
        this.commissioningPlanType = commissioningPlanType;
    }

    public String getSpare1() {
        return this.spare1;
    }

    public CommissioningPlan spare1(String spare1) {
        this.setSpare1(spare1);
        return this;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return this.spare2;
    }

    public CommissioningPlan spare2(String spare2) {
        this.setSpare2(spare2);
        return this;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return this.spare3;
    }

    public CommissioningPlan spare3(String spare3) {
        this.setSpare3(spare3);
        return this;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public Frequency getPaymentFrequency() {
        return this.paymentFrequency;
    }

    public void setPaymentFrequency(Frequency frequency) {
        this.paymentFrequency = frequency;
    }

    public CommissioningPlan paymentFrequency(Frequency frequency) {
        this.setPaymentFrequency(frequency);
        return this;
    }

    public Frequency getCalculusFrequency() {
        return this.calculusFrequency;
    }

    public void setCalculusFrequency(Frequency frequency) {
        this.calculusFrequency = frequency;
    }

    public CommissioningPlan calculusFrequency(Frequency frequency) {
        this.setCalculusFrequency(frequency);
        return this;
    }

    public Period getCalculusPeriod() {
        return this.calculusPeriod;
    }

    public void setCalculusPeriod(Period period) {
        this.calculusPeriod = period;
    }

    public CommissioningPlan calculusPeriod(Period period) {
        this.setCalculusPeriod(period);
        return this;
    }

    public Period getPaymentPeriod() {
        return this.paymentPeriod;
    }

    public void setPaymentPeriod(Period period) {
        this.paymentPeriod = period;
    }

    public CommissioningPlan paymentPeriod(Period period) {
        this.setPaymentPeriod(period);
        return this;
    }

    public Set<ServiceType> getServices() {
        return this.services;
    }

    public void setServices(Set<ServiceType> serviceTypes) {
        this.services = serviceTypes;
    }

    public CommissioningPlan services(Set<ServiceType> serviceTypes) {
        this.setServices(serviceTypes);
        return this;
    }

    public CommissioningPlan addServices(ServiceType serviceType) {
        this.services.add(serviceType);
        serviceType.getCommissionPlans().add(this);
        return this;
    }

    public CommissioningPlan removeServices(ServiceType serviceType) {
        this.services.remove(serviceType);
        serviceType.getCommissionPlans().remove(this);
        return this;
    }

    public Set<ConfigurationPlan> getConfigurationPlans() {
        return this.configurationPlans;
    }

    public void setConfigurationPlans(Set<ConfigurationPlan> configurationPlans) {
        if (this.configurationPlans != null) {
            this.configurationPlans.forEach(i -> i.setCommissioningPlan(null));
        }
        if (configurationPlans != null) {
            configurationPlans.forEach(i -> i.setCommissioningPlan(this));
        }
        this.configurationPlans = configurationPlans;
    }

    public CommissioningPlan configurationPlans(Set<ConfigurationPlan> configurationPlans) {
        this.setConfigurationPlans(configurationPlans);
        return this;
    }

    public CommissioningPlan addConfigurationPlan(ConfigurationPlan configurationPlan) {
        this.configurationPlans.add(configurationPlan);
        configurationPlan.setCommissioningPlan(this);
        return this;
    }

    public CommissioningPlan removeConfigurationPlan(ConfigurationPlan configurationPlan) {
        this.configurationPlans.remove(configurationPlan);
        configurationPlan.setCommissioningPlan(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommissioningPlan)) {
            return false;
        }
        return id != null && id.equals(((CommissioningPlan) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommissioningPlan{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", beginDate='" + getBeginDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", archiveDate='" + getArchiveDate() + "'" +
            ", archivedBy='" + getArchivedBy() + "'" +
            ", paymentStrategy='" + getPaymentStrategy() + "'" +
            ", state='" + getState() + "'" +
            ", commissioningPlanType='" + getCommissioningPlanType() + "'" +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            "}";
    }
}
