package sn.free.commissioning.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import sn.free.commissioning.domain.enumeration.PaymentStatus;

/**
 * A Payment.
 */
@Entity
@Table(name = "payment")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Payment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "amount")
    private Double amount;

    @Column(name = "sender_msisdn")
    private String senderMsisdn;

    @Column(name = "receiver_msisdn")
    private String receiverMsisdn;

    @Column(name = "receiver_profile")
    private String receiverProfile;

    @Column(name = "period_start_date")
    private Instant periodStartDate;

    @Column(name = "period_end_date")
    private Instant periodEndDate;

    @Column(name = "execution_duration")
    private Double executionDuration;

    @NotNull
    @Column(name = "created_by", nullable = false)
    private String createdBy;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "last_modified_by")
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_status")
    private PaymentStatus paymentStatus;

    @Column(name = "spare_1")
    private String spare1;

    @Column(name = "spare_2")
    private String spare2;

    @Column(name = "spare_3")
    private String spare3;

    @OneToMany(mappedBy = "payment")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "commissionAccount", "transactionOperations", "configurationPlan", "payment" }, allowSetters = true)
    private Set<Commission> commissions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Payment id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return this.amount;
    }

    public Payment amount(Double amount) {
        this.setAmount(amount);
        return this;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getSenderMsisdn() {
        return this.senderMsisdn;
    }

    public Payment senderMsisdn(String senderMsisdn) {
        this.setSenderMsisdn(senderMsisdn);
        return this;
    }

    public void setSenderMsisdn(String senderMsisdn) {
        this.senderMsisdn = senderMsisdn;
    }

    public String getReceiverMsisdn() {
        return this.receiverMsisdn;
    }

    public Payment receiverMsisdn(String receiverMsisdn) {
        this.setReceiverMsisdn(receiverMsisdn);
        return this;
    }

    public void setReceiverMsisdn(String receiverMsisdn) {
        this.receiverMsisdn = receiverMsisdn;
    }

    public String getReceiverProfile() {
        return this.receiverProfile;
    }

    public Payment receiverProfile(String receiverProfile) {
        this.setReceiverProfile(receiverProfile);
        return this;
    }

    public void setReceiverProfile(String receiverProfile) {
        this.receiverProfile = receiverProfile;
    }

    public Instant getPeriodStartDate() {
        return this.periodStartDate;
    }

    public Payment periodStartDate(Instant periodStartDate) {
        this.setPeriodStartDate(periodStartDate);
        return this;
    }

    public void setPeriodStartDate(Instant periodStartDate) {
        this.periodStartDate = periodStartDate;
    }

    public Instant getPeriodEndDate() {
        return this.periodEndDate;
    }

    public Payment periodEndDate(Instant periodEndDate) {
        this.setPeriodEndDate(periodEndDate);
        return this;
    }

    public void setPeriodEndDate(Instant periodEndDate) {
        this.periodEndDate = periodEndDate;
    }

    public Double getExecutionDuration() {
        return this.executionDuration;
    }

    public Payment executionDuration(Double executionDuration) {
        this.setExecutionDuration(executionDuration);
        return this;
    }

    public void setExecutionDuration(Double executionDuration) {
        this.executionDuration = executionDuration;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Payment createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public Payment createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public Payment lastModifiedBy(String lastModifiedBy) {
        this.setLastModifiedBy(lastModifiedBy);
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return this.lastModifiedDate;
    }

    public Payment lastModifiedDate(Instant lastModifiedDate) {
        this.setLastModifiedDate(lastModifiedDate);
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public PaymentStatus getPaymentStatus() {
        return this.paymentStatus;
    }

    public Payment paymentStatus(PaymentStatus paymentStatus) {
        this.setPaymentStatus(paymentStatus);
        return this;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getSpare1() {
        return this.spare1;
    }

    public Payment spare1(String spare1) {
        this.setSpare1(spare1);
        return this;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return this.spare2;
    }

    public Payment spare2(String spare2) {
        this.setSpare2(spare2);
        return this;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return this.spare3;
    }

    public Payment spare3(String spare3) {
        this.setSpare3(spare3);
        return this;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public Set<Commission> getCommissions() {
        return this.commissions;
    }

    public void setCommissions(Set<Commission> commissions) {
        if (this.commissions != null) {
            this.commissions.forEach(i -> i.setPayment(null));
        }
        if (commissions != null) {
            commissions.forEach(i -> i.setPayment(this));
        }
        this.commissions = commissions;
    }

    public Payment commissions(Set<Commission> commissions) {
        this.setCommissions(commissions);
        return this;
    }

    public Payment addCommission(Commission commission) {
        this.commissions.add(commission);
        commission.setPayment(this);
        return this;
    }

    public Payment removeCommission(Commission commission) {
        this.commissions.remove(commission);
        commission.setPayment(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Payment)) {
            return false;
        }
        return id != null && id.equals(((Payment) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Payment{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", senderMsisdn='" + getSenderMsisdn() + "'" +
            ", receiverMsisdn='" + getReceiverMsisdn() + "'" +
            ", receiverProfile='" + getReceiverProfile() + "'" +
            ", periodStartDate='" + getPeriodStartDate() + "'" +
            ", periodEndDate='" + getPeriodEndDate() + "'" +
            ", executionDuration=" + getExecutionDuration() +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", paymentStatus='" + getPaymentStatus() + "'" +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            "}";
    }
}
