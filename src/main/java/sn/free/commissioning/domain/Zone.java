package sn.free.commissioning.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Immutable;

/**
 * A Zone.
 */
@Entity
@Immutable
@Table(name = "zone_view")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Zone implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "code", nullable = false)
    private String code;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "state")
    private String state;

    @Column(name = "zone_type_id")
    private Long zoneTypeId;

    @NotNull
    @Size(max = 50)
    @Column(name = "created_by", length = 50, nullable = false)
    private String createdBy;

    @Column(name = "created_date")
    private Instant createdDate;

    @Size(max = 50)
    @Column(name = "last_modified_by", length = 50)
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @ManyToOne
    @JsonIgnoreProperties(value = { "zones" }, allowSetters = true)
    private Territory territory;

    @ManyToMany(mappedBy = "zones")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "commissionTypes", "commissions", "partnerProfile", "commissioningPlan", "zones" }, allowSetters = true)
    private Set<ConfigurationPlan> configurationPlans = new HashSet<>();

    @ManyToMany(mappedBy = "zones")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "blacklisteds", "children", "zones", "partner" }, allowSetters = true)
    private Set<Partner> partners = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Zone id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return this.code;
    }

    public Zone code(String code) {
        this.setCode(code);
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public Zone name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public Zone description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getState() {
        return this.state;
    }

    public Zone state(String state) {
        this.setState(state);
        return this;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getZoneTypeId() {
        return this.zoneTypeId;
    }

    public Zone zoneTypeId(Long zoneTypeId) {
        this.setZoneTypeId(zoneTypeId);
        return this;
    }

    public void setZoneTypeId(Long zoneTypeId) {
        this.zoneTypeId = zoneTypeId;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Zone createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public Zone createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public Zone lastModifiedBy(String lastModifiedBy) {
        this.setLastModifiedBy(lastModifiedBy);
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return this.lastModifiedDate;
    }

    public Zone lastModifiedDate(Instant lastModifiedDate) {
        this.setLastModifiedDate(lastModifiedDate);
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Territory getTerritory() {
        return this.territory;
    }

    public void setTerritory(Territory territory) {
        this.territory = territory;
    }

    public Zone territory(Territory territory) {
        this.setTerritory(territory);
        return this;
    }

    public Set<ConfigurationPlan> getConfigurationPlans() {
        return this.configurationPlans;
    }

    public void setConfigurationPlans(Set<ConfigurationPlan> configurationPlans) {
        if (this.configurationPlans != null) {
            this.configurationPlans.forEach(i -> i.removeZones(this));
        }
        if (configurationPlans != null) {
            configurationPlans.forEach(i -> i.addZones(this));
        }
        this.configurationPlans = configurationPlans;
    }

    public Zone configurationPlans(Set<ConfigurationPlan> configurationPlans) {
        this.setConfigurationPlans(configurationPlans);
        return this;
    }

    public Zone addConfigurationPlans(ConfigurationPlan configurationPlan) {
        this.configurationPlans.add(configurationPlan);
        configurationPlan.getZones().add(this);
        return this;
    }

    public Zone removeConfigurationPlans(ConfigurationPlan configurationPlan) {
        this.configurationPlans.remove(configurationPlan);
        configurationPlan.getZones().remove(this);
        return this;
    }

    public Set<Partner> getPartners() {
        return this.partners;
    }

    public void setPartners(Set<Partner> partners) {
        if (this.partners != null) {
            this.partners.forEach(i -> i.removeZone(this));
        }
        if (partners != null) {
            partners.forEach(i -> i.addZone(this));
        }
        this.partners = partners;
    }

    public Zone partners(Set<Partner> partners) {
        this.setPartners(partners);
        return this;
    }

    public Zone addPartner(Partner partner) {
        this.partners.add(partner);
        partner.getZones().add(this);
        return this;
    }

    public Zone removePartner(Partner partner) {
        this.partners.remove(partner);
        partner.getZones().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Zone)) {
            return false;
        }
        return id != null && id.equals(((Zone) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Zone{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", state='" + getState() + "'" +
            ", zoneTypeId=" + getZoneTypeId() +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            "}";
    }
}
