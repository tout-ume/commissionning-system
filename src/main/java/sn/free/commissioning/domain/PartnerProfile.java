package sn.free.commissioning.domain;

import java.io.Serializable;
import java.time.Instant;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Immutable;

/**
 * A PartnerProfile.
 */
@Entity
@Immutable
@Table(name = "partner_profile_view")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PartnerProfile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "code", nullable = false)
    private String code;

    @NotNull
    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "level")
    private Integer level;

    @Column(name = "parent_partner_profile_id")
    private Long parentPartnerProfileId;

    @NotNull
    @Size(max = 50)
    @Column(name = "created_by", length = 50, nullable = false)
    private String createdBy;

    @Column(name = "created_date")
    private Instant createdDate;

    @Size(max = 50)
    @Column(name = "last_modified_by", length = 50)
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Column(name = "can_have_multiple_zones")
    private Boolean canHaveMultipleZones;

    @Column(name = "zone_threshold")
    private Integer zoneThreshold;

    @Column(name = "sim_backup_threshold")
    private Integer simBackupThreshold;

    @Column(name = "can_do_profile_assignment")
    private Boolean canDoProfileAssignment;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public PartnerProfile id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return this.code;
    }

    public PartnerProfile code(String code) {
        this.setCode(code);
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return this.name;
    }

    public PartnerProfile name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public PartnerProfile description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getLevel() {
        return this.level;
    }

    public PartnerProfile level(Integer level) {
        this.setLevel(level);
        return this;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Long getParentPartnerProfileId() {
        return this.parentPartnerProfileId;
    }

    public PartnerProfile parentPartnerProfileId(Long parentPartnerProfileId) {
        this.setParentPartnerProfileId(parentPartnerProfileId);
        return this;
    }

    public void setParentPartnerProfileId(Long parentPartnerProfileId) {
        this.parentPartnerProfileId = parentPartnerProfileId;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public PartnerProfile createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public PartnerProfile createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public PartnerProfile lastModifiedBy(String lastModifiedBy) {
        this.setLastModifiedBy(lastModifiedBy);
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return this.lastModifiedDate;
    }

    public PartnerProfile lastModifiedDate(Instant lastModifiedDate) {
        this.setLastModifiedDate(lastModifiedDate);
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Boolean getCanHaveMultipleZones() {
        return this.canHaveMultipleZones;
    }

    public PartnerProfile canHaveMultipleZones(Boolean canHaveMultipleZones) {
        this.setCanHaveMultipleZones(canHaveMultipleZones);
        return this;
    }

    public void setCanHaveMultipleZones(Boolean canHaveMultipleZones) {
        this.canHaveMultipleZones = canHaveMultipleZones;
    }

    public Integer getZoneThreshold() {
        return this.zoneThreshold;
    }

    public PartnerProfile zoneThreshold(Integer zoneThreshold) {
        this.setZoneThreshold(zoneThreshold);
        return this;
    }

    public void setZoneThreshold(Integer zoneThreshold) {
        this.zoneThreshold = zoneThreshold;
    }

    public Integer getSimBackupThreshold() {
        return this.simBackupThreshold;
    }

    public PartnerProfile simBackupThreshold(Integer simBackupThreshold) {
        this.setSimBackupThreshold(simBackupThreshold);
        return this;
    }

    public void setSimBackupThreshold(Integer simBackupThreshold) {
        this.simBackupThreshold = simBackupThreshold;
    }

    public Boolean getCanDoProfileAssignment() {
        return this.canDoProfileAssignment;
    }

    public PartnerProfile canDoProfileAssignment(Boolean canDoProfileAssignment) {
        this.setCanDoProfileAssignment(canDoProfileAssignment);
        return this;
    }

    public void setCanDoProfileAssignment(Boolean canDoProfileAssignment) {
        this.canDoProfileAssignment = canDoProfileAssignment;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PartnerProfile)) {
            return false;
        }
        return id != null && id.equals(((PartnerProfile) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PartnerProfile{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", level=" + getLevel() +
            ", parentPartnerProfileId=" + getParentPartnerProfileId() +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", canHaveMultipleZones='" + getCanHaveMultipleZones() + "'" +
            ", zoneThreshold=" + getZoneThreshold() +
            ", simBackupThreshold=" + getSimBackupThreshold() +
            ", canDoProfileAssignment='" + getCanDoProfileAssignment() + "'" +
            "}";
    }
}
