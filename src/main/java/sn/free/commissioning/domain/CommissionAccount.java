package sn.free.commissioning.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A CommissionAccount.
 */
@Entity
@Table(name = "commission_account")
@NamedEntityGraph(name = "graph.CommissionAccount", attributeNodes = @NamedAttributeNode("commissions"))
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CommissionAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "calculated_balance")
    private Double calculatedBalance;

    @Column(name = "paid_balance")
    private Double paidBalance;

    @Column(name = "spare_1")
    private String spare1;

    @Column(name = "spare_2")
    private String spare2;

    @Column(name = "spare_3")
    private String spare3;

    @JsonIgnoreProperties(value = { "blacklisteds", "children", "zones", "partner" }, allowSetters = true)
    @OneToOne
    @JoinColumn(unique = true)
    private Partner partner;

    @OneToMany(mappedBy = "commissionAccount")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "commissionAccount", "transactionOperations", "configurationPlan", "payment" }, allowSetters = true)
    private Set<Commission> commissions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public CommissionAccount id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getCalculatedBalance() {
        return this.calculatedBalance;
    }

    public CommissionAccount calculatedBalance(Double calculatedBalance) {
        this.setCalculatedBalance(calculatedBalance);
        return this;
    }

    public void setCalculatedBalance(Double calculatedBalance) {
        this.calculatedBalance = calculatedBalance;
    }

    public Double getPaidBalance() {
        return this.paidBalance;
    }

    public CommissionAccount paidBalance(Double paidBalance) {
        this.setPaidBalance(paidBalance);
        return this;
    }

    public void setPaidBalance(Double paidBalance) {
        this.paidBalance = paidBalance;
    }

    public String getSpare1() {
        return this.spare1;
    }

    public CommissionAccount spare1(String spare1) {
        this.setSpare1(spare1);
        return this;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return this.spare2;
    }

    public CommissionAccount spare2(String spare2) {
        this.setSpare2(spare2);
        return this;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return this.spare3;
    }

    public CommissionAccount spare3(String spare3) {
        this.setSpare3(spare3);
        return this;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public Partner getPartner() {
        return this.partner;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    public CommissionAccount partner(Partner partner) {
        this.setPartner(partner);
        return this;
    }

    public Set<Commission> getCommissions() {
        return this.commissions;
    }

    public void setCommissions(Set<Commission> commissions) {
        if (this.commissions != null) {
            this.commissions.forEach(i -> i.setCommissionAccount(null));
        }
        if (commissions != null) {
            commissions.forEach(i -> i.setCommissionAccount(this));
        }
        this.commissions = commissions;
    }

    public CommissionAccount commissions(Set<Commission> commissions) {
        this.setCommissions(commissions);
        return this;
    }

    public CommissionAccount addCommission(Commission commission) {
        this.commissions.add(commission);
        commission.setCommissionAccount(this);
        return this;
    }

    public CommissionAccount removeCommission(Commission commission) {
        this.commissions.remove(commission);
        commission.setCommissionAccount(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommissionAccount)) {
            return false;
        }
        return id != null && id.equals(((CommissionAccount) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommissionAccount{" +
            "id=" + getId() +
            ", calculatedBalance=" + getCalculatedBalance() +
            ", paidBalance=" + getPaidBalance() +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            "}";
    }
}
