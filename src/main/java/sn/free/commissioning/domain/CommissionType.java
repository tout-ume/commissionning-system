package sn.free.commissioning.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import sn.free.commissioning.domain.enumeration.CommissionTypeType;
import sn.free.commissioning.domain.enumeration.PalierValueType;

/**
 * A CommissionType.
 */
@Entity
@Table(name = "commission_type")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class CommissionType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "min_value")
    private Double minValue;

    @Column(name = "max_value")
    private Double maxValue;

    @Column(name = "taux_value")
    private Double tauxValue;

    @Column(name = "palier_value")
    private Double palierValue;

    @Enumerated(EnumType.STRING)
    @Column(name = "commission_type_type")
    private CommissionTypeType commissionTypeType;

    @Enumerated(EnumType.STRING)
    @Column(name = "palier_value_type")
    private PalierValueType palierValueType;

    @Column(name = "is_infinity")
    private Boolean isInfinity;

    @Column(name = "spare_1")
    private String spare1;

    @Column(name = "spare_2")
    private String spare2;

    @Column(name = "spare_3")
    private String spare3;

    @ManyToOne
    @JsonIgnoreProperties(value = { "commissionTypes", "commissions", "partnerProfile", "commissioningPlan", "zones" }, allowSetters = true)
    private ConfigurationPlan configurationPlan;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public CommissionType id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getMinValue() {
        return this.minValue;
    }

    public CommissionType minValue(Double minValue) {
        this.setMinValue(minValue);
        return this;
    }

    public void setMinValue(Double minValue) {
        this.minValue = minValue;
    }

    public Double getMaxValue() {
        return this.maxValue;
    }

    public CommissionType maxValue(Double maxValue) {
        this.setMaxValue(maxValue);
        return this;
    }

    public void setMaxValue(Double maxValue) {
        this.maxValue = maxValue;
    }

    public Double getTauxValue() {
        return this.tauxValue;
    }

    public CommissionType tauxValue(Double tauxValue) {
        this.setTauxValue(tauxValue);
        return this;
    }

    public void setTauxValue(Double tauxValue) {
        this.tauxValue = tauxValue;
    }

    public Double getPalierValue() {
        return this.palierValue;
    }

    public CommissionType palierValue(Double palierValue) {
        this.setPalierValue(palierValue);
        return this;
    }

    public void setPalierValue(Double palierValue) {
        this.palierValue = palierValue;
    }

    public CommissionTypeType getCommissionTypeType() {
        return this.commissionTypeType;
    }

    public CommissionType commissionTypeType(CommissionTypeType commissionTypeType) {
        this.setCommissionTypeType(commissionTypeType);
        return this;
    }

    public void setCommissionTypeType(CommissionTypeType commissionTypeType) {
        this.commissionTypeType = commissionTypeType;
    }

    public PalierValueType getPalierValueType() {
        return this.palierValueType;
    }

    public CommissionType palierValueType(PalierValueType palierValueType) {
        this.setPalierValueType(palierValueType);
        return this;
    }

    public void setPalierValueType(PalierValueType palierValueType) {
        this.palierValueType = palierValueType;
    }

    public Boolean getIsInfinity() {
        return this.isInfinity;
    }

    public CommissionType isInfinity(Boolean isInfinity) {
        this.setIsInfinity(isInfinity);
        return this;
    }

    public void setIsInfinity(Boolean isInfinity) {
        this.isInfinity = isInfinity;
    }

    public String getSpare1() {
        return this.spare1;
    }

    public CommissionType spare1(String spare1) {
        this.setSpare1(spare1);
        return this;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return this.spare2;
    }

    public CommissionType spare2(String spare2) {
        this.setSpare2(spare2);
        return this;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return this.spare3;
    }

    public CommissionType spare3(String spare3) {
        this.setSpare3(spare3);
        return this;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public ConfigurationPlan getConfigurationPlan() {
        return this.configurationPlan;
    }

    public void setConfigurationPlan(ConfigurationPlan configurationPlan) {
        this.configurationPlan = configurationPlan;
    }

    public CommissionType configurationPlan(ConfigurationPlan configurationPlan) {
        this.setConfigurationPlan(configurationPlan);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommissionType)) {
            return false;
        }
        return id != null && id.equals(((CommissionType) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommissionType{" +
            "id=" + getId() +
            ", minValue=" + getMinValue() +
            ", maxValue=" + getMaxValue() +
            ", tauxValue=" + getTauxValue() +
            ", palierValue=" + getPalierValue() +
            ", commissionTypeType='" + getCommissionTypeType() + "'" +
            ", palierValueType='" + getPalierValueType() + "'" +
            ", isInfinity='" + getIsInfinity() + "'" +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            "}";
    }
}
