package sn.free.commissioning.domain;

import java.util.List;
import sn.free.commissioning.service.dto.CommissionDTO;
import sn.free.commissioning.service.dto.PartnerDTO;

public class PartnerCommissions {

    private PartnerDTO partner;
    private List<CommissionDTO> commissions;

    public PartnerDTO getPartner() {
        return partner;
    }

    public void setPartner(PartnerDTO partner) {
        this.partner = partner;
    }

    public List<CommissionDTO> getCommissions() {
        return commissions;
    }

    public void setCommissions(List<CommissionDTO> commissions) {
        this.commissions = commissions;
    }

    @Override
    public String toString() {
        return "PartnerCommissions{" + "partner=" + partner + ", commissions=" + commissions + '}';
    }
}
