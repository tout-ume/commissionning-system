package sn.free.commissioning.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Immutable;

/**
 * A Partner.
 */
@Entity
@Immutable
@Table(name = "partner_view")
@NamedEntityGraph(
    name = "graph.Partner",
    attributeNodes = {
        @NamedAttributeNode("zones"),
        @NamedAttributeNode("parent"),
        @NamedAttributeNode(value = "children", subgraph = "subgraph.children"),
    },
    subgraphs = {
        @NamedSubgraph(name = "subgraph.children", attributeNodes = { @NamedAttributeNode("parent"), @NamedAttributeNode("children") }),
    }
)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Partner implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "msisdn", nullable = false)
    private String msisdn;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "state")
    private String state;

    @Column(name = "partner_profile_id")
    private Long partnerProfileId;

    @ManyToOne
    @JoinColumn(name = "parent_id", nullable = true)
    @JsonIgnoreProperties(value = { "blacklisteds", "children", "zones", "partner" }, allowSetters = true)
    private Partner parent;

    @Column(name = "can_reset_pin")
    private Boolean canResetPin;

    @NotNull
    @Column(name = "created_by", nullable = false)
    private String createdBy;

    @Column(name = "created_date")
    private Instant createdDate;

    @Column(name = "last_modified_by")
    private String lastModifiedBy;

    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Column(name = "last_transaction_date")
    private Instant lastTransactionDate;

    @OneToMany(mappedBy = "partner")
    @JsonIgnoreProperties(value = { "commissioningPlan", "partner" }, allowSetters = true)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Blacklisted> blacklisteds = new HashSet<>();

    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL)
    @JsonIgnoreProperties(value = { "blacklisteds", "children", "zones", "partner" }, allowSetters = true)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Partner> children = new HashSet<>();

    @ManyToMany
    @JoinTable(
        name = "partner_zone_view",
        joinColumns = @JoinColumn(name = "partner_id"),
        inverseJoinColumns = @JoinColumn(name = "zone_id")
    )
    @JsonIgnoreProperties(value = { "territory", "configurationPlans", "partners" }, allowSetters = true)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<Zone> zones = new HashSet<>();

    //    @ManyToOne
    //    @JoinColumn(name = "parent_id", nullable = true)
    //    @JsonIgnoreProperties(value = { "blacklisteds", "children", "zones", "partner" }, allowSetters = true)
    //    private Partner partner;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Partner id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return this.msisdn;
    }

    public Partner msisdn(String msisdn) {
        this.setMsisdn(msisdn);
        return this;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getName() {
        return this.name;
    }

    public Partner name(String name) {
        this.setName(name);
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return this.surname;
    }

    public Partner surname(String surname) {
        this.setSurname(surname);
        return this;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getState() {
        return this.state;
    }

    public Partner state(String state) {
        this.setState(state);
        return this;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getPartnerProfileId() {
        return this.partnerProfileId;
    }

    public Partner partnerProfileId(Long partnerProfileId) {
        this.setPartnerProfileId(partnerProfileId);
        return this;
    }

    public void setPartnerProfileId(Long partnerProfileId) {
        this.partnerProfileId = partnerProfileId;
    }

    public Partner getParent() {
        return parent;
    }

    public void setParent(Partner parent) {
        this.parent = parent;
    }

    public Boolean getCanResetPin() {
        return this.canResetPin;
    }

    public Partner canResetPin(Boolean canResetPin) {
        this.setCanResetPin(canResetPin);
        return this;
    }

    public void setCanResetPin(Boolean canResetPin) {
        this.canResetPin = canResetPin;
    }

    public String getCreatedBy() {
        return this.createdBy;
    }

    public Partner createdBy(String createdBy) {
        this.setCreatedBy(createdBy);
        return this;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return this.createdDate;
    }

    public Partner createdDate(Instant createdDate) {
        this.setCreatedDate(createdDate);
        return this;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return this.lastModifiedBy;
    }

    public Partner lastModifiedBy(String lastModifiedBy) {
        this.setLastModifiedBy(lastModifiedBy);
        return this;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return this.lastModifiedDate;
    }

    public Partner lastModifiedDate(Instant lastModifiedDate) {
        this.setLastModifiedDate(lastModifiedDate);
        return this;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Instant getLastTransactionDate() {
        return this.lastTransactionDate;
    }

    public Partner lastTransactionDate(Instant lastTransactionDate) {
        this.setLastTransactionDate(lastTransactionDate);
        return this;
    }

    public void setLastTransactionDate(Instant lastTransactionDate) {
        this.lastTransactionDate = lastTransactionDate;
    }

    public Set<Blacklisted> getBlacklisteds() {
        return this.blacklisteds;
    }

    public void setBlacklisteds(Set<Blacklisted> blacklisteds) {
        if (this.blacklisteds != null) {
            this.blacklisteds.forEach(i -> i.setPartner(null));
        }
        if (blacklisteds != null) {
            blacklisteds.forEach(i -> i.setPartner(this));
        }
        this.blacklisteds = blacklisteds;
    }

    public Partner blacklisteds(Set<Blacklisted> blacklisteds) {
        this.setBlacklisteds(blacklisteds);
        return this;
    }

    public Partner addBlacklisted(Blacklisted blacklisted) {
        this.blacklisteds.add(blacklisted);
        blacklisted.setPartner(this);
        return this;
    }

    public Partner removeBlacklisted(Blacklisted blacklisted) {
        this.blacklisteds.remove(blacklisted);
        blacklisted.setPartner(null);
        return this;
    }

    public Set<Partner> getChildren() {
        return this.children;
    }

    public void setChildren(Set<Partner> partners) {
        if (this.children != null) {
            this.children.forEach(i -> i.setParent(null));
        }
        if (partners != null) {
            partners.forEach(i -> i.setParent(this));
        }
        this.children = partners;
    }

    public Partner children(Set<Partner> partners) {
        this.setChildren(partners);
        return this;
    }

    public Partner addChildren(Partner partner) {
        this.children.add(partner);
        partner.setParent(this);
        return this;
    }

    public Partner removeChildren(Partner partner) {
        this.children.remove(partner);
        partner.setParent(null);
        return this;
    }

    public Set<Zone> getZones() {
        return this.zones;
    }

    public void setZones(Set<Zone> zones) {
        this.zones = zones;
    }

    public Partner zones(Set<Zone> zones) {
        this.setZones(zones);
        return this;
    }

    public Partner addZone(Zone zone) {
        this.zones.add(zone);
        zone.getPartners().add(this);
        return this;
    }

    public Partner removeZone(Zone zone) {
        this.zones.remove(zone);
        zone.getPartners().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Partner)) {
            return false;
        }
        return id != null && id.equals(((Partner) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Partner{" +
            "id=" + getId() +
            ", msisdn='" + getMsisdn() + "'" +
            ", name='" + getName() + "'" +
            ", surname='" + getSurname() + "'" +
            ", state='" + getState() + "'" +
            ", partnerProfileId=" + getPartnerProfileId() +
//            ", parentId=" + getParentId() +
            ", canResetPin='" + getCanResetPin() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", lastTransactionDate='" + getLastTransactionDate() + "'" +
            "}";
    }
}
