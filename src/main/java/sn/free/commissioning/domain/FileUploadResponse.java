package sn.free.commissioning.domain;

import java.io.Serializable;
import sn.free.commissioning.domain.enumeration.UploadStatus;

public class FileUploadResponse implements Serializable {

    private UploadStatus status;
    private String message;

    public FileUploadResponse() {}

    public UploadStatus getStatus() {
        return status;
    }

    public void setStatus(UploadStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
