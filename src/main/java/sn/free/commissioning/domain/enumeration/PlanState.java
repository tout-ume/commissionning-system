package sn.free.commissioning.domain.enumeration;

/**
 * The PlanState enumeration.
 */
public enum PlanState {
    ARCHIVED,
    IN_USE,
}
