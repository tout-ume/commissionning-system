package sn.free.commissioning.domain.enumeration;

/**
 * The PaymentStatus enumeration.
 */
public enum PaymentStatus {
    FAILURE,
    SUCCESS,
}
