package sn.free.commissioning.domain.enumeration;

/**
 * The CommissioningPlanType enumeration.
 */
public enum CommissioningPlanType {
    MFS,
    MOBILE,
}
