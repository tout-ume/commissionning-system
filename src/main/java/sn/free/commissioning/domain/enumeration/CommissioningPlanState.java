package sn.free.commissioning.domain.enumeration;

/**
 * The CommissioningPlanState enumeration.
 */
public enum CommissioningPlanState {
    CREATED,
    ARCHIVED,
    IN_USE,
    DRAFT,
}
