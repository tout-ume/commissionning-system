package sn.free.commissioning.domain.enumeration;

/**
 * The ServiceTypeStatus enumeration.
 */
public enum ServiceTypeStatus {
    ACTIVATED,
    DEACTIVATED,
}
