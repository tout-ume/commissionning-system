package sn.free.commissioning.domain.enumeration;

/**
 * The FrequencyRevolution enumeration.
 */
public enum FrequencyRevolution {
    HOUR,
    HOURS_AFTER,
    DAY,
    DAYS_AFTER,
    WEEK,
    MONTH,
    YEAR,
}
