package sn.free.commissioning.domain.enumeration;

/**
 * The PalierValueType enumeration.
 */
public enum PalierValueType {
    TAUX,
    FIXE,
}
