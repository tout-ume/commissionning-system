package sn.free.commissioning.domain.enumeration;

/**
 * The CommissionPaymentStatus enumeration.
 */
public enum CommissionPaymentStatus {
    TO_BE_PAID,
    TO_BE_PAID_TO_PARTNER,
    PAID,
    FAILED,
    PENDING,
    BLOCKED,
}
