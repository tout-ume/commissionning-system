package sn.free.commissioning.domain.enumeration;

/**
 * The OperationType enumeration.
 */
public enum OperationType {
    CALCULUS,
    PAYMENT,
}
