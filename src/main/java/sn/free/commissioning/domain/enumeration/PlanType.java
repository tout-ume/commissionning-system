package sn.free.commissioning.domain.enumeration;

/**
 * The PlanType enumeration.
 */
public enum PlanType {
    TAUX,
    PALIER,
}
