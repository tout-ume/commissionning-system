package sn.free.commissioning.domain.enumeration;

/**
 * The Frequency enumeration.
 */
public enum Frequency {
    INSTANTLY,
    DAILY,
}
