package sn.free.commissioning.domain.enumeration;

/**
 * The PaymentStrategy enumeration.
 */
public enum PaymentStrategy {
    AUTOMATIC,
    MANUAL,
    SCHEDULED,
}
