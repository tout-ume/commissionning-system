package sn.free.commissioning.domain.enumeration;

/**
 * The FrequencyType enumeration.
 */
public enum FrequencyType {
    INSTANTLY,
    DAILY,
    WEEKLY,
    MONTHLY,
    QUARTERLY,
    SEMI_ANNUALLY,
    CYCLIC,
    RECCURRENT,
}
