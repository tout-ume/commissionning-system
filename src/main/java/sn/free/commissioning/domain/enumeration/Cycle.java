package sn.free.commissioning.domain.enumeration;

/**
 * The Cycle enumeration.
 */
public enum Cycle {
    DAYS,
    WEEKS,
    MONTHS,
}
