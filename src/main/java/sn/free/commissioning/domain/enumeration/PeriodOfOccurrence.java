package sn.free.commissioning.domain.enumeration;

/**
 * The PeriodOfOccurrence enumeration.
 */
public enum PeriodOfOccurrence {
    WEEK,
    MONTH,
}
