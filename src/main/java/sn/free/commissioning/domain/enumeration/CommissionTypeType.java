package sn.free.commissioning.domain.enumeration;

/**
 * The CommissionTypeType enumeration.
 */
public enum CommissionTypeType {
    PALIER,
    TAUX,
}
