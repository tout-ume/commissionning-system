package sn.free.commissioning.domain.enumeration;

public enum UploadStatus {
    SUCCESS,
    FAILURE,
}
