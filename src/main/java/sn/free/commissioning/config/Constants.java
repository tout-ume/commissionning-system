package sn.free.commissioning.config;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sn.free.commissioning.security.SecurityUtils;
import sn.free.commissioning.web.rest.AccountResource;

/**
 * Application constants.
 */
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^(?>[a-zA-Z0-9!$&*+=?^_`{|}~.-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*)|(?>[_.@A-Za-z0-9-]+)$";

    public static final String SYSTEM = "system";
    public static final String DEFAULT_LANGUAGE = "fr";

    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_API = "ROLE_API";
    public static final String ROLE_GTM_ADMIN = "ROLE_GTM_ADMIN";
    public static final String ROLE_GTM_AGENT = "ROLE_GTM_AGENT";
    public static final String ROLE_USER = "ROLE_USER";
    public static final String ROLE_INTERNAL = "ROLE_INTERNAL";
    public static final String ROLE_EXTERNAL = "ROLE_INTERNAL";

    private Constants() {}

    public static void logInfo(boolean isSuccessful, String getClassName, String description, String comment, HttpServletRequest request) {
        String userId = null;
        String userEmail = null;

        //Logger log = LoggerFactory.getLogger(AccountResource.class);
        Logger log = LoggerFactory.getLogger(getClassName);

        String ipSrc;

        if (request != null) {
            //String ipSrc = request.getHeader("X-Forwarded-For");
            ipSrc = request.getHeader("X-Custom-XFF");
        } else {
            ipSrc = "NA";
        }

        String message =
            "{" +
            "\"status\"" +
            ":" +
            "\"" +
            (isSuccessful ? "SUCCESS" : "FAILURE") +
            "\"," +
            "\"user_id\"" +
            ":" +
            "\"" +
            (userId == null ? "" : userId) +
            "\"," +
            "\"user_name\"" +
            ":" +
            "\"" +
            (SecurityUtils.getCurrentUserLogin().isPresent() ? SecurityUtils.getCurrentUserLogin().get() : "") +
            "\"," +
            "\"user_email\"" +
            ":" +
            "\"" +
            (userEmail == null ? "" : userEmail) +
            "\"," +
            "\"event_id\"" +
            ":" +
            "\"" +
            UUID.randomUUID() +
            "\"," +
            "\"description\"" +
            ":" +
            "\"" +
            description +
            "\"," +
            "\"comment\"" +
            ":" +
            "\"" +
            comment +
            "\"," +
            "\"date\"" +
            ":" +
            "\"" +
            (ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")) + ".000Z") +
            "\"," +
            //"\"date\"" + ":" + "\"" + (Instant.now().toString()) + "\"," +
            "\"ip_src\"" +
            ":" +
            "\"" +
            ipSrc +
            "\"" +
            "}";

        log.info(message);
    }

    public static void logInfoForService(String getClassName, String description, String comment) {
        // Logger log = LoggerFactory.getLogger(AccountResource.class);
        Logger log = LoggerFactory.getLogger(getClassName);

        String message =
            "{" +
            "\"description\"" +
            ":" +
            "\"" +
            description +
            "\"," +
            "\"comment\"" +
            ":" +
            "\"" +
            comment +
            "\"," +
            "\"date\"" +
            ":" +
            "\"" +
            (ZonedDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")) + ".000Z") +
            "\"," +
            "}";

        log.info(message);
    }
}
