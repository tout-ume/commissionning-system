package sn.free.commissioning.config;

import java.time.Duration;
import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;
import org.hibernate.cache.jcache.ConfigSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.info.GitProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.*;
import tech.jhipster.config.JHipsterProperties;
import tech.jhipster.config.cache.PrefixedKeyGenerator;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private GitProperties gitProperties;
    private BuildProperties buildProperties;
    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache = jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration =
            Eh107Configuration.fromEhcacheCacheConfiguration(
                CacheConfigurationBuilder
                    .newCacheConfigurationBuilder(Object.class, Object.class, ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                    .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                    .build()
            );
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, sn.free.commissioning.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, sn.free.commissioning.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, sn.free.commissioning.domain.User.class.getName());
            createCache(cm, sn.free.commissioning.domain.User.class.getName() + ".authorities");
            createCache(cm, sn.free.commissioning.domain.Authority.class.getName());
            createCache(cm, sn.free.commissioning.domain.AuthorityMirror.class.getName());
            createCache(cm, sn.free.commissioning.domain.Blacklisted.class.getName());
            createCache(cm, sn.free.commissioning.domain.Commission.class.getName());
            createCache(cm, sn.free.commissioning.domain.CommissionAccount.class.getName());
            createCache(cm, sn.free.commissioning.domain.CommissioningPlan.class.getName());
            createCache(cm, sn.free.commissioning.domain.CommissionType.class.getName());
            createCache(cm, sn.free.commissioning.domain.ConfigurationPlan.class.getName());
            createCache(cm, sn.free.commissioning.domain.Frequency.class.getName());
            createCache(cm, sn.free.commissioning.domain.Partner.class.getName());
            createCache(cm, sn.free.commissioning.domain.PartnerProfile.class.getName());
            createCache(cm, sn.free.commissioning.domain.Payment.class.getName());
            createCache(cm, sn.free.commissioning.domain.Period.class.getName());
            createCache(cm, sn.free.commissioning.domain.Role.class.getName());
            createCache(cm, sn.free.commissioning.domain.ServiceType.class.getName());
            createCache(cm, sn.free.commissioning.domain.Territory.class.getName());
            createCache(cm, sn.free.commissioning.domain.TransactionOperation.class.getName());
            createCache(cm, sn.free.commissioning.domain.Zone.class.getName());
            createCache(cm, sn.free.commissioning.domain.Territory.class.getName() + ".zones");
            createCache(cm, sn.free.commissioning.domain.Zone.class.getName() + ".configurationPlans");
            createCache(cm, sn.free.commissioning.domain.Zone.class.getName() + ".partners");
            createCache(cm, sn.free.commissioning.domain.Partner.class.getName() + ".blacklisteds");
            createCache(cm, sn.free.commissioning.domain.Partner.class.getName() + ".children");
            createCache(cm, sn.free.commissioning.domain.Partner.class.getName() + ".zones");
            createCache(cm, sn.free.commissioning.domain.CommissioningPlan.class.getName() + ".services");
            createCache(cm, sn.free.commissioning.domain.CommissioningPlan.class.getName() + ".configurationPlans");
            createCache(cm, sn.free.commissioning.domain.ServiceType.class.getName() + ".commissionPlans");
            createCache(cm, sn.free.commissioning.domain.ConfigurationPlan.class.getName() + ".commissionTypes");
            createCache(cm, sn.free.commissioning.domain.ConfigurationPlan.class.getName() + ".commissions");
            createCache(cm, sn.free.commissioning.domain.ConfigurationPlan.class.getName() + ".zones");
            createCache(cm, sn.free.commissioning.domain.Commission.class.getName() + ".transactionOperations");
            createCache(cm, sn.free.commissioning.domain.CommissionAccount.class.getName() + ".commissions");
            createCache(cm, sn.free.commissioning.domain.TransactionOperation.class.getName() + ".commissions");
            createCache(cm, sn.free.commissioning.domain.Payment.class.getName() + ".commissions");
            createCache(cm, sn.free.commissioning.domain.CalculationExecutionTrace.class.getName());
            createCache(cm, sn.free.commissioning.domain.Role.class.getName() + ".authorities");
            createCache(cm, sn.free.commissioning.domain.AuthorityMirror.class.getName() + ".roles");
            createCache(cm, sn.free.commissioning.domain.MissedTransactionOperation.class.getName());
            createCache(cm, sn.free.commissioning.domain.MissedTransactionOperationArchive.class.getName());
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache != null) {
            cache.clear();
        } else {
            cm.createCache(cacheName, jcacheConfiguration);
        }
    }

    @Autowired(required = false)
    public void setGitProperties(GitProperties gitProperties) {
        this.gitProperties = gitProperties;
    }

    @Autowired(required = false)
    public void setBuildProperties(BuildProperties buildProperties) {
        this.buildProperties = buildProperties;
    }

    @Bean
    public KeyGenerator keyGenerator() {
        return new PrefixedKeyGenerator(this.gitProperties, this.buildProperties);
    }
}
