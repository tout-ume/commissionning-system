package sn.free.commissioning.config;

import java.util.List;
import org.jobrunr.scheduling.BackgroundJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import sn.free.commissioning.calculus_engine.listeners.CreateFrequencyListener;
import sn.free.commissioning.calculus_engine.service.ReconciliationService;
import sn.free.commissioning.domain.Frequency;
import sn.free.commissioning.repository.FrequencyRepository;

@Component
public class JobReboot implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(JobReboot.class);
    private final FrequencyRepository frequencyRepository;
    private final ReconciliationService reconciliationService;
    private final CreateFrequencyListener createFrequencyListener;

    public JobReboot(
        FrequencyRepository frequencyRepository,
        ReconciliationService reconciliationService,
        CreateFrequencyListener createFrequencyListener
    ) {
        this.frequencyRepository = frequencyRepository;
        this.reconciliationService = reconciliationService;
        this.createFrequencyListener = createFrequencyListener;
    }

    @Override
    public void run(String... args) throws Exception {
        // TODO Auto-generated method stubo
        List<Frequency> frequencies = frequencyRepository.findAll();
        Constants.logInfoForService(
            String.valueOf(JobReboot.class),
            "====================== STARTING THE REBOT² ==========================",
            "====================== STARTING THE REBOT² =========================="
        );
        createFrequencyListener.run(frequencies);

        log.info(" =============== Creating Instant Reconciliation Job fds");
        // Runs every three (3) minutes
        String crontab = "*/3 * * * *";
        BackgroundJob.scheduleRecurrently("Instant-Reconciliation", crontab, reconciliationService::instantlyCommissionsReconciliation);
    }
}
