package sn.free.commissioning.calculus_engine.web.rest;

import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sn.free.commissioning.calculus_engine.service.AgentCommissionListService;
import sn.free.commissioning.calculus_engine.service.CommissionSimulationService;
import sn.free.commissioning.calculus_engine.service.dto.PartnerCommissionsDTO;

/**
 * REST controller for managing {@link sn.free.commissioning.domain.Commission}.
 */
@RestController
@RequestMapping("/api")
@Validated
public class PartnerCommissionListResource {

    private final Logger log = LoggerFactory.getLogger(PartnerCommissionListResource.class);

    private final AgentCommissionListService agentCommissionListService;
    private final CommissionSimulationService commissionSimulationService;

    public PartnerCommissionListResource(
        AgentCommissionListService agentCommissionListService,
        CommissionSimulationService commissionSimulationService
    ) {
        this.agentCommissionListService = agentCommissionListService;
        this.commissionSimulationService = commissionSimulationService;
    }

    public ResponseEntity<?> rateLimiterFallback(Exception e) {
        return new ResponseEntity<>("This service does not permit further requests", HttpStatus.TOO_MANY_REQUESTS);
    }

    @GetMapping("/commissions-list")
    // @RateLimiter(name = "commissionsHistory", fallbackMethod = "rateLimiterFallback")
    public ResponseEntity<?> createCommission(
        @RequestParam String msisdn,
        @RequestParam String frequency,
        @RequestParam(value = "limit", required = false) Integer limit,
        HttpServletRequest request
    ) throws URISyntaxException {
        log.info("========== Bonjour =========== ");
        List<PartnerCommissionsDTO> data = new ArrayList<PartnerCommissionsDTO>();
        Instant startDate = Instant.now().truncatedTo(ChronoUnit.DAYS).minus(1, ChronoUnit.DAYS);
        Instant endDate = Instant.now().truncatedTo(ChronoUnit.DAYS).minus(1, ChronoUnit.DAYS);
        Integer limitDays;
        ArrayList<String> acceptedFrequency = new ArrayList<>(Arrays.asList("daily", "monthly", "cyclic"));
        if (!acceptedFrequency.stream().anyMatch(frequency::equals)) {
            return ResponseEntity.badRequest().body("Frequency should be daily, monthly or cyclic");
        }
        switch (frequency) {
            case "daily":
                limitDays = limit == null ? 92 : limit;
                break;
            case "monthly":
                limitDays = limit == null ? 365 : limit * 30;
                break;
            case "cyclic":
                limitDays = limit == null ? 2 : limit * 2;
                break;
            default:
                limitDays = 30;
        }

        PartnerCommissionsDTO currentDayCommission = commissionSimulationService.simulate(msisdn, frequency);
        log.info(".................... BIZ");
        if (frequency.equals("daily") || frequency.equals("monthly")) {
            if (frequency.equals("monthly")) {
                endDate = Instant.parse(LocalDate.now().minusDays(1).with(TemporalAdjusters.firstDayOfMonth()).minusDays(1) + "T00:00:00Z");
            }
            startDate = endDate.minus(limitDays, ChronoUnit.DAYS);
            data = agentCommissionListService.getAgentCommissionsForPeriod(msisdn, frequency, startDate, endDate);
        } else {
            YearMonth current_month = YearMonth.from(LocalDate.parse(startDate.toString().split("T")[0]));
            log.info(".................... current_month = {}", current_month);
            YearMonth tmp_month = current_month.minusMonths(limitDays);
            while (!current_month.equals(tmp_month)) {
                log.info(".................... tmp_month = {}", tmp_month);
                Instant beginOfMonth = Instant.parse(tmp_month.atDay(1) + "T00:00:00Z");
                Instant middleOfMonth = Instant.parse(tmp_month.atDay(16) + "T00:00:00Z");

                YearMonth nextMonth = tmp_month.plusMonths(1);
                List<PartnerCommissionsDTO> first = agentCommissionListService.getAgentCommissionsForPeriod(
                    msisdn,
                    frequency,
                    beginOfMonth,
                    middleOfMonth
                );
                List<PartnerCommissionsDTO> second = agentCommissionListService.getAgentCommissionsForPeriod(
                    msisdn,
                    frequency,
                    middleOfMonth,
                    Instant.parse(nextMonth.atDay(1) + "T00:00:00Z")
                );
                data.addAll(first);
                data.addAll(second);
                tmp_month = nextMonth;
            }
        }
        Collections.sort(
            data,
            (s1, s2) -> {
                return s2.getStartdate().compareTo(s1.getStartdate());
            }
        );
        log.info("---------> CURRENT {}", currentDayCommission);
        log.info("---------> ALL {}", data);
        Map<String, List<PartnerCommissionsDTO>> response = new HashMap<String, List<PartnerCommissionsDTO>>();
        data.add(0, currentDayCommission);
        response.put("activities", data);
        return ResponseEntity.ok().body(response);
    }

    private List<String> getIntervalDate(LocalDate startDate, LocalDate endDate) {
        long numOfDaysBetween = Math.abs(ChronoUnit.DAYS.between(startDate, endDate));
        return IntStream
            .iterate(0, i -> i + 1)
            .limit(numOfDaysBetween)
            .mapToObj(i -> startDate.plusDays(i).toString() + " 00:00:00")
            .collect(Collectors.toList());
    }
}
