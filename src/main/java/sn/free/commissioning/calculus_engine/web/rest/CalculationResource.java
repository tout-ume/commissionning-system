package sn.free.commissioning.calculus_engine.web.rest;

import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.free.commissioning.calculus_engine.domain.CalculationResult;
import sn.free.commissioning.calculus_engine.domain.DailyCalculationInfo;
import sn.free.commissioning.calculus_engine.service.DeferredCalculusService;
import sn.free.commissioning.config.Constants;

/**
 * REST controller for managing Calculation.
 */
@RestController
@RequestMapping("/api")
public class CalculationResource {

    private final Logger log = LoggerFactory.getLogger(CalculationResource.class);

    private static final String CLASS_NAME = String.valueOf(CalculationResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DeferredCalculusService deferredCalculusService;

    public CalculationResource(DeferredCalculusService deferredCalculusService) {
        this.deferredCalculusService = deferredCalculusService;
    }

    /**
     * {@code POST  /calculation/calculate-for-days/{planId}} : Relaunch calculation for a list of days.
     *
     * @param dailyCalculationInfo the list of days for wich we need to recalculate.
     * @param planId the commissioning plan.
     * @return the {@link ResponseEntity} with status {@code 200} and with body the CalculationResult.
     */
    @PostMapping("/calculation/calculate-for-days/{planId}")
    public ResponseEntity<CalculationResult> createBlacklisted(
        @RequestBody DailyCalculationInfo dailyCalculationInfo,
        @PathVariable Long planId,
        HttpServletRequest request
    ) {
        log.info("REST request to calculate for days : {}, planId : {}", request, planId);
        Constants.logInfo(true, CLASS_NAME, "REST request to calculate for days", " REST request to calculate for days", request);
        deferredCalculusService.reCalculation(dailyCalculationInfo, planId);
        CalculationResult result = new CalculationResult();
        result.setMessage("Calcul terminé!");
        return ResponseEntity.ok().body(result);
    }
}
