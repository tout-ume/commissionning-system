package sn.free.commissioning.calculus_engine.domain.enumeration;

public enum InstantCalculationStatus {
    SUCCESS,
    FAILURE,
    NOT_PAID,
}
