package sn.free.commissioning.calculus_engine.domain;

import java.util.List;

public class NetworkInformations {

    private Double topProfileCommissionAmount;
    private Double globalCommissionAmount;
    private Double totalNetworkTransactionsAmount;
    private List<String> zones;

    public Double getTopProfileCommissionAmount() {
        return topProfileCommissionAmount;
    }

    public void setTopProfieCommissionAmount(Double topProfieCommissionAmount) {
        this.topProfileCommissionAmount = topProfieCommissionAmount;
    }

    public Double getGlobalCommissionAmount() {
        return globalCommissionAmount;
    }

    public void setGlobalCommissionAmount(Double globalCommissionAmount) {
        this.globalCommissionAmount = globalCommissionAmount;
    }

    public Double getTotalNetworkTransactionsAmount() {
        return totalNetworkTransactionsAmount;
    }

    public void setTotalNetworkTransactionsAmount(Double totalNetworkTransactionsAmount) {
        this.totalNetworkTransactionsAmount = totalNetworkTransactionsAmount;
    }

    public List<String> getZones() {
        return zones;
    }

    public void setZones(List<String> zones) {
        this.zones = zones;
    }

    @Override
    public String toString() {
        return (
            "NetworkInformations{" +
            "topProfieCommissionAmount=" +
            topProfileCommissionAmount +
            ", globalCommissionAmount=" +
            globalCommissionAmount +
            ", totalNetworkTransactionsAmount=" +
            totalNetworkTransactionsAmount +
            ", zones=" +
            zones +
            '}'
        );
    }
}
