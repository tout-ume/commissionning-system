package sn.free.commissioning.calculus_engine.domain;

import java.util.HashSet;
import java.util.Set;
import sn.free.commissioning.domain.ConfigurationPlan;
import sn.free.commissioning.domain.Partner;

public class PartnerNode {

    private Partner partner;
    private Set<ConfigurationPlan> configurationPlans = new HashSet<>();

    public Partner getPartner() {
        return partner;
    }

    public void setPartner(Partner partner) {
        this.partner = partner;
    }

    public Set<ConfigurationPlan> getConfigurationPlans() {
        return configurationPlans;
    }

    public void setConfigurationPlans(Set<ConfigurationPlan> configurationPlans) {
        this.configurationPlans = configurationPlans;
    }
}
