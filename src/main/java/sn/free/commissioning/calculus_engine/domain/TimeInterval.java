package sn.free.commissioning.calculus_engine.domain;

import java.time.Instant;

public class TimeInterval {

    private Instant startDate;
    private Instant endDate;
    private Instant calculationDate;

    public TimeInterval() {}

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Instant getCalculationDate() {
        return calculationDate;
    }

    public void setCalculationDate(Instant calculationDate) {
        this.calculationDate = calculationDate;
    }

    @Override
    public String toString() {
        return "TimeInterval{" + "startDate=" + startDate + ", endDate=" + endDate + ", calculationDate=" + calculationDate + '}';
    }
}
