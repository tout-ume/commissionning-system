package sn.free.commissioning.calculus_engine.domain;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class DailyCalculationInfo {

    private Long frequencyId;
    private List<Instant> days = new ArrayList<>();

    public Long getFrequencyId() {
        return frequencyId;
    }

    public void setFrequencyId(Long frequencyId) {
        this.frequencyId = frequencyId;
    }

    public List<Instant> getDays() {
        return days;
    }

    public void setDays(List<Instant> days) {
        this.days = days;
    }

    @Override
    public String toString() {
        return "DailyCalculationInfo{" + "frequencyId=" + frequencyId + ", days=" + days + '}';
    }
}
