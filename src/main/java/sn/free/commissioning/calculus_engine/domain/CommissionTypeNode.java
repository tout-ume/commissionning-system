package sn.free.commissioning.calculus_engine.domain;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import sn.free.commissioning.domain.enumeration.PalierValueType;

public class CommissionTypeNode {

    private Double amount;

    @Enumerated(EnumType.STRING)
    private PalierValueType palierValueType;

    public CommissionTypeNode() {}

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public PalierValueType getPalierValueType() {
        return palierValueType;
    }

    public void setPalierValueType(PalierValueType palierValueType) {
        this.palierValueType = palierValueType;
    }

    public CommissionTypeNode(Double amount, PalierValueType palierValueType) {
        this.amount = amount;
        this.palierValueType = palierValueType;
    }
}
