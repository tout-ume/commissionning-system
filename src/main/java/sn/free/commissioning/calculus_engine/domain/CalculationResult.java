package sn.free.commissioning.calculus_engine.domain;

public class CalculationResult {

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;
}
