package sn.free.commissioning.calculus_engine.domain;

import java.io.Serializable;

public class CurrentDayCommissionResponse implements Serializable {

    private String resultCode;

    private String resultMessage;

    private String message;

    private Double cashIn;

    private Double cashOut;

    private Double billPay;

    private Double totalJour;

    private Double airtimeAndBundle;

    private Double totalCom;

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Double getCashIn() {
        return cashIn;
    }

    public void setCashIn(Double cashIn) {
        this.cashIn = cashIn;
    }

    public Double getCashOut() {
        return cashOut;
    }

    public void setCashOut(Double cashOut) {
        this.cashOut = cashOut;
    }

    public Double getBillPay() {
        return billPay;
    }

    public void setBillPay(Double billPay) {
        this.billPay = billPay;
    }

    public Double getTotalJour() {
        return totalJour;
    }

    public void setTotalJour(Double totalJour) {
        this.totalJour = totalJour;
    }

    public Double getAirtimeAndBundle() {
        return airtimeAndBundle;
    }

    public void setAirtimeAndBundle(Double airtimeAndBundle) {
        this.airtimeAndBundle = airtimeAndBundle;
    }

    public Double getTotalCom() {
        return totalCom;
    }

    public void setTotalCom(Double totalCom) {
        this.totalCom = totalCom;
    }

    @Override
    public String toString() {
        return (
            "{" +
            " resultCode='" +
            getResultCode() +
            "'" +
            ", resultMessage='" +
            getResultMessage() +
            "'" +
            ", message='" +
            getMessage() +
            "'" +
            ", cashIn='" +
            getCashIn() +
            "'" +
            ", cashOut='" +
            getCashOut() +
            "'" +
            ", billPay='" +
            getBillPay() +
            "'" +
            ", totalJour='" +
            getTotalJour() +
            "'" +
            ", airtimeAndBundle='" +
            getAirtimeAndBundle() +
            "'" +
            ", totalCom='" +
            getTotalCom() +
            "'" +
            "}"
        );
    }
}
