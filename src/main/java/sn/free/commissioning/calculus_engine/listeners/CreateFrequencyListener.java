package sn.free.commissioning.calculus_engine.listeners;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import javax.persistence.PostPersist;
import javax.transaction.Transactional;
import org.jobrunr.scheduling.BackgroundJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import sn.free.commissioning.calculus_engine.service.DeferredCalculusService;
import sn.free.commissioning.config.Constants;
import sn.free.commissioning.domain.Frequency;
import sn.free.commissioning.domain.TransactionOperation;
import sn.free.commissioning.domain.enumeration.OperationType;
import sn.free.commissioning.domain.enumeration.PeriodOfOccurrence;
import sn.free.commissioning.service.TransactionOperationService;

@Service
public class CreateFrequencyListener implements Serializable {

    private final Logger log = LoggerFactory.getLogger(CreateFrequencyListener.class);

    private DeferredCalculusService deferredCalculusService;

    public CreateFrequencyListener(DeferredCalculusService deferredCalculusService) {
        this.deferredCalculusService = deferredCalculusService;
    }

    public CreateFrequencyListener() {}

    public void run(List<Frequency> frequencies) {
        frequencies
            .stream()
            .forEach(frequency -> {
                afterPersist(frequency);
            });
    }

    @Transactional
    @PostPersist
    public void afterPersist(Frequency frequency) {
        if (frequency.getOperationType() == OperationType.CALCULUS) {
            switch (frequency.getType()) {
                case DAILY:
                    {
                        Integer hour = frequency.getExecutionTime();
                        Constants.logInfoForService(
                            String.valueOf(CreateFrequencyListener.class),
                            " =============== l heure d execution is {}" + hour,
                            " =============== l heure d execution is {}" + hour
                        );

                        log.info(" =============== l heure d execution is {}", hour);
                        String crontab = "0 " + hour + " * * *";
                        // For testing purpose
                        //                        String crontab = "*/5 * * * *";
                        BackgroundJob.scheduleRecurrently(
                            "Calculating-" + frequency.getId(),
                            crontab,
                            () -> {
                                deferredCalculusService.calculate(frequency);
                            }
                        );

                        // Create a job for checking tagged transactions
                        BackgroundJob.scheduleRecurrently(
                            "checkingFraud-" + frequency.getId(),
                            crontab,
                            () -> {
                                deferredCalculusService.checkAndRelaunchCalculation(frequency.getId());
                            }
                        );
                        break;
                    }
                case WEEKLY:
                    {
                        Integer weekDay = frequency.getExecutionWeekDay();
                        String crontab = "0 0 * * " + weekDay;
                        log.info(" =============== jour d execution is {}", weekDay);
                        Constants.logInfoForService(
                            String.valueOf(CreateFrequencyListener.class),
                            " =============== jour d execution is {}" + weekDay,
                            "=============== jour d execution is {}" + weekDay
                        );

                        //             String crontab = "0 0 * * " + weekDay;
                        // For testing purpose
                        //             String crontab = "* * * * *";
                        BackgroundJob.scheduleRecurrently(
                            "Calculating-" + frequency.getId(),
                            crontab,
                            () -> {
                                deferredCalculusService.calculate(frequency);
                            }
                        );
                        break;
                    }
                case RECCURRENT:
                    {
                        if (frequency.getPeriodOfOccurrence().equals(PeriodOfOccurrence.MONTH)) {
                            log.info(" =============== les jours d execution sont {}", frequency.getDatesOfOccurrence());
                            Constants.logInfoForService(
                                String.valueOf(CreateFrequencyListener.class),
                                " =============== les jours d execution sont {}" + frequency.getDatesOfOccurrence(),
                                "=============== les jours d execution sont {}" + frequency.getDatesOfOccurrence()
                            );
                            Arrays
                                .asList(frequency.getDatesOfOccurrence().split(","))
                                .forEach(date -> {
                                    String crontab = "0 0 " + date + " * * ";
                                    //                                    String crontab = "*/5 * * * *";
                                    log.info(" =============== crontab is {}", crontab);
                                    BackgroundJob.scheduleRecurrently(
                                        "Calculating-" + frequency.getId(),
                                        crontab,
                                        () -> {
                                            deferredCalculusService.calculate(frequency);
                                        }
                                    );
                                });
                        }
                        break;
                    }
                default:
                    break;
            }
        }
    }
}
