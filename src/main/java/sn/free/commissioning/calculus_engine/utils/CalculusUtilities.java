package sn.free.commissioning.calculus_engine.utils;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sn.free.commissioning.calculus_engine.domain.TimeInterval;
import sn.free.commissioning.domain.Frequency;
import sn.free.commissioning.domain.Period;

public class CalculusUtilities {

    private static final Logger log = LoggerFactory.getLogger(CalculusUtilities.class);
    static int NUMBER_OF_DAYS_IN_A_WEEK = 7;

    public static TimeInterval buildTimeInterval(Period period, Instant calculationDate, Frequency frequency) {
        TimeInterval result = new TimeInterval();
        Instant dateStart = null, dateEnd = null;
        calculationDate = calculationDate == null ? Instant.now() : calculationDate;
        switch (period.getPeriodType()) {
            case DAILY:
                if (period.getIsCompleteDay()) {
                    // Starts at 00h and finishes at 23h59
                    dateStart = calculationDate.truncatedTo(ChronoUnit.DAYS);
                    //                    log.info("***************** DateStart : {}", dateStart);
                    // Plus 23 hours an d 59 miniutes (86340 seconds)
                    dateEnd = dateStart.plusSeconds(86340);
                    //                    log.info("***************** DateEnd : {}", dateEnd);
                } else {
                    // Set dateStart
                    if (period.getIsPreviousDayHourFrom() != null && period.getIsPreviousDayHourFrom()) {
                        // Substract 24h - period.getDayHourFrom() (in seconds) to start from the day before
                        dateStart = calculationDate.truncatedTo(ChronoUnit.DAYS).minusSeconds(86400 - (period.getDayHourFrom() * 3600));
                    } else {
                        // Add period.getDayHourFrom() (in seconds) to start from current day
                        dateStart = calculationDate.truncatedTo(ChronoUnit.DAYS).plusSeconds(period.getDayHourFrom() * 3600);
                    }
                    // Set dateEnd
                    if (period.getIsPreviousDayHourTo() != null && period.getIsPreviousDayHourTo()) {
                        // Substract 24h - period.getDayHourTo() (in seconds) to end the day before
                        dateEnd = calculationDate.truncatedTo(ChronoUnit.DAYS).minusSeconds(86400 - (period.getDayHourTo() * 3600));
                    } else {
                        // Add period.getDayHourTo() (in seconds) to end the current day
                        dateEnd = calculationDate.truncatedTo(ChronoUnit.DAYS).plusSeconds(period.getDayHourTo() * 3600);
                    }
                    //                    log.info("***************** DateStart : {}", dateStart);
                    //                    log.info("***************** DateEnd : {}", dateEnd);
                }
                break;
            case WEEKLY:
                // Starts at 00h (daysBeforeCalculationDate) days before calculationDate and finishes at 23h59 (daysBeforeCalculationDate) days before calculationDate
                // First let's calculate daysBeforeCalculationDate for startDate and endDate
                int daysBeforeCalculationDateForStartDate, daysBeforeCalculationDateForEndDate;
                if (Objects.equals(frequency.getExecutionWeekDay(), period.getWeekDayFrom())) {
                    daysBeforeCalculationDateForStartDate = NUMBER_OF_DAYS_IN_A_WEEK;
                } else {
                    daysBeforeCalculationDateForStartDate =
                        frequency.getExecutionWeekDay() < period.getWeekDayFrom()
                            ? (frequency.getExecutionWeekDay() - period.getWeekDayFrom()) + NUMBER_OF_DAYS_IN_A_WEEK
                            : frequency.getExecutionWeekDay() - period.getWeekDayFrom();
                }
                daysBeforeCalculationDateForEndDate =
                    frequency.getExecutionWeekDay() < period.getWeekDayTo()
                        ? (frequency.getExecutionWeekDay() - period.getWeekDayTo()) + NUMBER_OF_DAYS_IN_A_WEEK
                        : frequency.getExecutionWeekDay() - period.getWeekDayTo();
                log.info("***************** frequency.getExecutionWeekDay() : {}", frequency.getExecutionWeekDay());
                log.info("***************** daysBeforeCalculationDateForStartDate : {}", daysBeforeCalculationDateForStartDate);
                log.info("***************** daysBeforeCalculationDateForEndDate : {}", daysBeforeCalculationDateForEndDate);
                log.info("***************** calculationDate : {}", calculationDate);
                // Then let's calculate actual start and end dates
                // Calculation date minus daysBeforeCalculationDateForStartDate times 24 hours (86400 seconds)
                dateStart = calculationDate.truncatedTo(ChronoUnit.DAYS).minusSeconds(86400L * daysBeforeCalculationDateForStartDate);
                // Calculation date minus daysBeforeCalculationDateForEndDate times 24 hours (86400 seconds) plus 23 hours an d 59 miniutes (86340 seconds)
                dateEnd =
                    calculationDate
                        .truncatedTo(ChronoUnit.DAYS)
                        .minusSeconds(86400L * daysBeforeCalculationDateForEndDate)
                        .plusSeconds(86340);
                break;
            case MONTHLY:
                break;
            case QUARTERLY:
                break;
            case SEMI_ANNUALLY:
            default:
                break;
        }
        result.setStartDate(dateStart);
        result.setEndDate(dateEnd);
        //        result.setStartDate(Instant.parse("2022-09-18T00:00:00Z"));
        //        result.setEndDate(Instant.parse("2022-09-18T23:59:00Z"));
        result.setCalculationDate(calculationDate.truncatedTo(ChronoUnit.DAYS));
        log.info("***************** DateStart : {}", result.getStartDate());
        log.info("***************** DateEnd : {}", result.getEndDate());
        return result;
    }
}
