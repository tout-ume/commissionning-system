package sn.free.commissioning.calculus_engine.service.impl;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.calculus_engine.service.AgentCommissionListService;
import sn.free.commissioning.calculus_engine.service.dto.PartnerCommissionsDTO;
import sn.free.commissioning.calculus_engine.service.mapper.PartnerCommissionsMapper;
import sn.free.commissioning.domain.Commission;
import sn.free.commissioning.repository.CommissionRepository;

@Service
@Transactional
public class AgentCommissionListServiceImpl implements AgentCommissionListService {

    private final Logger log = LoggerFactory.getLogger(AgentCommissionListServiceImpl.class);

    private final CommissionRepository commissionRepository;

    private final PartnerCommissionsMapper partnerCommissionMapper;

    public AgentCommissionListServiceImpl(CommissionRepository commissionRepository, PartnerCommissionsMapper partnerCommissionMapper) {
        this.commissionRepository = commissionRepository;
        this.partnerCommissionMapper = partnerCommissionMapper;
    }

    @Override
    public List<PartnerCommissionsDTO> getAgentCommissionsForPeriod(String msisdn, String frequency, Instant dateStart, Instant dateEnd) {
        // TODO Auto-generated method stub
        List<Commission> commissions = commissionRepository.findBySenderMsisdnAndCalculationDateBetween(msisdn, dateStart, dateEnd);

        log.info("==================================================================================================");
        log.info("==================================================================================================");
        log.info("==================================================================================================");
        log.info("COMMISSIONS ==================================================================================");
        // log.info(".......... {}", commissions);
        log.info("==================================================================================================");
        log.info("INTERVAL DATE ::::::::::::::::::::::::::::: {} -> {}", dateStart, dateEnd);

        Map<String, List<Commission>> result = new HashMap<String, List<Commission>>();
        if (frequency.equals("daily")) {
            result = commissions.stream().collect(Collectors.groupingBy(commission -> commission.getCalculationDate().toString()));
        }
        if (frequency.equals("monthly") || frequency.equals("cyclic")) {
            result =
                commissions
                    .stream()
                    .collect(
                        Collectors.groupingBy(commission -> {
                            ZonedDateTime zdt = commission.getCalculationDate().atZone(ZoneId.of("Africa/Dakar"));
                            return YearMonth.from(zdt.toLocalDate()).toString();
                        })
                    );
        }

        log.info(" RESULT :::::::::::::::::::::::: {}", result.size());
        log.info(" SIZE HASHMAP :::::::::::::::::::::::: {}", result.size());
        return result
            .entrySet()
            .stream()
            .map(c -> partnerCommissionMapper.convert(c, frequency, dateStart, dateEnd))
            .collect(Collectors.toList());
    }
}
