package sn.free.commissioning.calculus_engine.service.impl;

import java.util.*;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.domain.PartnerProfile;
import sn.free.commissioning.repository.PartnerProfileRepository;
import sn.free.commissioning.repository.PartnerRepository;

@Service
public class PartnerFromTree {

    private final Logger log = LoggerFactory.getLogger(PartnerFromTree.class);

    @Value("#{'${cms.profileMapping.PDV}'.split(',')}")
    private List<String> pdvMappings;

    @Value("#{'${cms.profileMapping.REV}'.split(',')}")
    private List<String> revMappings;

    @Value("#{'${cms.profileMapping.DG}'.split(',')}")
    private List<String> dgMappings;

    @Value("#{'${cms.profileMapping.DP}'.split(',')}")
    private List<String> dpMappings;

    private Map<String, List<String>> mappings = new HashMap<String, List<String>>();

    private List<Partner> partners;
    private Map<String, List<Partner>> network = new HashMap<String, List<Partner>>();

    private List<Partner> dgList = new ArrayList<Partner>();
    private List<Partner> revList = new ArrayList<Partner>();
    private List<Partner> pdvList = new ArrayList<Partner>();

    private final PartnerProfileRepository profileRepository;
    private final PartnerRepository partnerRepository;

    public PartnerFromTree(PartnerProfileRepository profileRepository, PartnerRepository partnerRepository) {
        this.partnerRepository = partnerRepository;
        this.partners = new ArrayList<Partner>();
        this.profileRepository = profileRepository;
    }

    /**
     * Dans cette fonction on récupérer les configuration de mapping effectué sur le application.yml
     * et pour tous les mappings on les ajoute dans un objet un HashMap
     * Ce qui se passe :
     *   - on récupére le profile du partner,
     *   - on essaye de trouver dans les mappings qu'on a sur le quel il se trouve
     *   - une fois qu'on a trouvé là ou il se trouve, on récupere la clef de ce map
     *   - C'est cette clé là qu'on passe à notre switch au lieu du profile.getCode() directement
     *
     * Ceci permet si on a un mapping pour PDV: PDV,PDV6,PDV-LIGHT
     * et qu'on a un partner de profile PDV-LIGHT.
     * On trouvera dans quel map il se trouve et on prend la clef de ce map : PDV
     * qu'on va utilisé pour parourir le réseau.
     * @param partner
     * @return
     */
    public PartnerFromTree from(Partner partner) {
        log.info("======================= Partner from : {}", partner.getPartnerProfileId());
        log.info("||||||||||||||| IN FROM ||||||||||||||--------------");
        PartnerProfile profile = this.profileRepository.findById(partner.getPartnerProfileId()).get();
        mappings.put("PDV", pdvMappings);
        mappings.put("REV", revMappings);
        mappings.put("DG", dgMappings);
        mappings.put("DP", dpMappings);
        log.info("||||||||||||||| THE MAP ||||||||||||||-------------- {}", mappings);
        String profileCode = mappings.entrySet().stream().filter(m -> m.getValue().contains(profile.getCode())).findAny().get().getKey();

        log.info("--------------------------------------------------");
        log.info(".::::::::: , {}  -> {}", profileCode, partner.getMsisdn());

        switch (profileCode) {
            case "REV":
                this.getFromREV(partner);
                break;
            case "DP":
                this.getFromDP(partner);
                break;
            case "DG":
                this.getFromDG(partner);
                break;
            default:
                break;
        }
        log.info("::::::: DONE! FOR {} ({}) when have {} PDV", partner.getMsisdn(), profileCode, this.partners.size());
        return this;
    }

    public List<Partner> get() {
        log.info("||||||||||||||||||||||||||||| --- the number of PDV {}", this.partners.size());
        List<Partner> result = this.partners;
        this.partners = new ArrayList<Partner>();
        return result;
    }

    public List<Partner> get(String profile) {
        log.info("||||||||||||||| BEFORE GET BY PROFILE --------------> {} PDVs", network.get(profile).size());
        return network.get(profile);
    }

    public List<Partner> getWithParent(String profile, Partner parent) {
        log.info("||||||||||||||| BEFORE GET BY PROFILE --------------> {} PDVs", network.get(profile).size());
        return network
            .get(profile)
            .stream()
            .filter(partner -> partner.getParent().getId().equals(parent.getId()))
            .collect(Collectors.toList());
    }

    public void getFromREV(Partner partner) {
        Collection<Partner> children = partnerRepository.findByParent_Id(partner.getId());
        log.info("***************************** FROM REV {} has : {} PDV", partner.getMsisdn(), children.size());
        log.info("BEFORE we have {}", this.partners.size());
        this.partners.addAll(children);
    }

    public void getFromDG(Partner partner) {
        Collection<Partner> children = partnerRepository.findByParent_Id(partner.getId());
        log.info("********, {}", children);
        children
            .stream()
            .forEach(child -> {
                revList.add(child);
                log.info("***************************** FROM DG : {}", child);
                pdvList.addAll(partnerRepository.findByParent_Id(child.getId()));
                this.getFromREV(child);
            });
        network.put("REV", revList);
        network.put("PDV", pdvList);
    }

    public void getFromDP(Partner partner) {
        Set<Partner> children = partner.getChildren();
        children
            .stream()
            .forEach(child -> {
                log.info("***************************** FROM DP : {}", child);
                dgList.add(child);
                this.getFromDG(child);
            });
        network.put("DG", dgList);
    }

    public Map<String, List<Partner>> getNetwork() {
        return this.network;
    }
}
