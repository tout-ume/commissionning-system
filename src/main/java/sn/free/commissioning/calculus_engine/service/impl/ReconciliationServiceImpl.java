package sn.free.commissioning.calculus_engine.service.impl;

import static java.util.stream.Collectors.groupingBy;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import sn.free.commissioning.calculus_engine.service.CalculusService;
import sn.free.commissioning.calculus_engine.service.ReconciliationService;
import sn.free.commissioning.config.Constants;
import sn.free.commissioning.domain.MissedTransactionOperation;
import sn.free.commissioning.domain.MissedTransactionOperationArchive;
import sn.free.commissioning.domain.TransactionOperation;
import sn.free.commissioning.repository.CommissionRepository;
import sn.free.commissioning.repository.MissedTransactionOperationArchiveRepository;
import sn.free.commissioning.repository.MissedTransactionOperationRepository;
import sn.free.commissioning.repository.TransactionOperationRepository;
import sn.free.commissioning.service.mapper.TransactionOperationMapper;

@Service
public class ReconciliationServiceImpl implements ReconciliationService {

    private final CalculusService calculusService;
    private final CommissionRepository commissionRepository;
    private final TransactionOperationMapper transactionOperationMapper;
    private final TransactionOperationRepository transactionOperationRepository;
    private final MissedTransactionOperationRepository missedTransactionOperationRepository;
    private final MissedTransactionOperationArchiveRepository missedTransactionOperationArchiveRepository;

    public ReconciliationServiceImpl(
        CalculusService calculusService,
        CommissionRepository commissionRepository,
        TransactionOperationMapper transactionOperationMapper,
        TransactionOperationRepository transactionOperationRepository,
        MissedTransactionOperationRepository missedTransactionOperationRepository,
        MissedTransactionOperationArchiveRepository missedTransactionOperationArchiveRepository
    ) {
        this.calculusService = calculusService;
        this.commissionRepository = commissionRepository;
        this.transactionOperationMapper = transactionOperationMapper;
        this.transactionOperationRepository = transactionOperationRepository;
        this.missedTransactionOperationRepository = missedTransactionOperationRepository;
        this.missedTransactionOperationArchiveRepository = missedTransactionOperationArchiveRepository;
    }

    @Override
    public void instantlyCommissionsReconciliation() {
        List<MissedTransactionOperation> missedTransactions = missedTransactionOperationRepository.findAll();
        List<String> missedTrxTid = missedTransactions.stream().map(MissedTransactionOperation::getTid).collect(Collectors.toList());
        Map<String, List<TransactionOperation>> knownTransactions = transactionOperationRepository
            .findByTidIn(missedTrxTid)
            .stream()
            .collect(groupingBy(TransactionOperation::getTid));
        missedTransactions
            .stream()
            .filter(transaction -> (!knownTransactions.containsKey(transaction.getTid()) && transaction.getSenderZone() != null))
            .forEach(this::processReconciliation);
    }

    void processReconciliation(MissedTransactionOperation missedTransactionOperation) {
        if (commissionRepository.findBySpare1IgnoreCase(missedTransactionOperation.getTid()).isPresent()) {
            Constants.logInfoForService(
                String.valueOf(ReconciliationServiceImpl.class),
                "====================== Found commission for TRX " + missedTransactionOperation.getTid() + ", passing ...",
                "====================== Found commission for TRX " + missedTransactionOperation.getTid() + ", passing ..."
            );
            return;
        }
        missedTransactionOperation.setSenderZone(missedTransactionOperation.getSenderZone().split(",")[0]);
        MissedTransactionOperationArchive archive = MissedTransactionOperationArchive.fromMissedTransaction(missedTransactionOperation);
        TransactionOperation transactionOperation = transactionOperationRepository.save(
            TransactionOperation.fromMissedTransaction(missedTransactionOperation)
        );
        calculusService.performInstantCalculation(transactionOperationMapper.toDto(transactionOperation));
        missedTransactionOperationArchiveRepository.save(archive);
        missedTransactionOperationRepository.delete(missedTransactionOperation);
    }
}
