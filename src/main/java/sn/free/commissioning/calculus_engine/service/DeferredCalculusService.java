package sn.free.commissioning.calculus_engine.service;

import java.time.Instant;
import java.util.List;
import sn.free.commissioning.calculus_engine.domain.DailyCalculationInfo;
import sn.free.commissioning.domain.Frequency;
import sn.free.commissioning.domain.Partner;

public interface DeferredCalculusService {
    /**
     *  Calculate the deffered commissions from a given frequency
     *
     * @param frequency the frequency of calculation
     * @param calculationDate the date to consider when creating period interval
     * @param partners list of partners to provide when recalculating, otherwise should be empty
     */
    void calculate(Frequency frequency, Instant calculationDate, List<Partner> partners);

    /**
     *  Calculate the deffered commissions from a given frequency
     *
     * @param frequency the frequency of calculation
     */
    void calculate(Frequency frequency);

    /**
     *  Launch recalculation for a list of days
     *
     * @param dailyCalculationInfo the calculation infos
     * @param planId the commissioning plan identification
     */
    void reCalculation(DailyCalculationInfo dailyCalculationInfo, Long planId);

    /**
     *  Checks tagged transactions and relaunch calculation
     *
     * @param frequencyId the frequency id
     */
    void checkAndRelaunchCalculation(Long frequencyId);
}
