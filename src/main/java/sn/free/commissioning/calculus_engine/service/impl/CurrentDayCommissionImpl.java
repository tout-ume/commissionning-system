package sn.free.commissioning.calculus_engine.service.impl;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Objects;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import sn.free.commissioning.calculus_engine.domain.CurrentDayCommissionResponse;
import sn.free.commissioning.calculus_engine.service.CurrentDayCommission;

@Service
public class CurrentDayCommissionImpl implements CurrentDayCommission {

    private final Logger log = LoggerFactory.getLogger(CurrentDayCommissionImpl.class);

    @Autowired
    private WebClient webClient;

    @Override
    public CurrentDayCommissionResponse getCurrentDay(String msisdn, String frequency) {
        CurrentDayCommissionResponse commissionResponse = new CurrentDayCommissionResponse();

        switch (frequency) {
            case "cyclic":
                frequency = "cycle";
                break;
            case "daily":
                frequency = "day";
                break;
            case "monthly":
                frequency = "month";
                break;
            default:
                break;
        }
        String period = frequency;
        String commissionRequestResponse = webClient
            .get()
            .uri(uriBuilder ->
                uriBuilder
                    .path("getcommission")
                    .queryParam("msisdn", msisdn)
                    .queryParam("period", period)
                    .queryParam("sms", 0)
                    .queryParam("channel", "USSD")
                    .build()
            )
            .accept(MediaType.APPLICATION_XML)
            .retrieve()
            .bodyToMono(String.class)
            .block();

        log.info("******** GetBalanceResponse = {}", commissionRequestResponse);
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = null;
        Document document = null;
        try {
            db = dbf.newDocumentBuilder();
            assert commissionRequestResponse != null;
            document = db.parse(new InputSource(new StringReader(commissionRequestResponse)));
            NodeList nodeList = document.getElementsByTagName("param");
            for (int x = 0, size = nodeList.getLength(); x < size; x++) {
                String paramValue = getValue(nodeList, x);
                switch (nodeList.item(x).getAttributes().getNamedItem("name").getNodeValue()) {
                    case "resultCode":
                        commissionResponse.setResultCode(paramValue);
                        break;
                    case "resultMessage":
                        commissionResponse.setResultMessage(paramValue);
                        break;
                    case "cashout":
                        commissionResponse.setCashOut(Double.parseDouble(paramValue));
                        break;
                    case "cashin":
                        commissionResponse.setCashIn(Double.parseDouble(paramValue));
                        break;
                    case "billpay":
                        commissionResponse.setBillPay(Double.parseDouble(paramValue));
                        break;
                    case "total_com":
                        commissionResponse.setTotalCom(Double.parseDouble(paramValue));
                        break;
                    case "total_jour":
                        commissionResponse.setTotalJour(Double.parseDouble(paramValue));
                        break;
                    case "message":
                        commissionResponse.setMessage(paramValue);
                        break;
                    case "airtimeandbundles":
                        commissionResponse.setAirtimeAndBundle(Double.parseDouble(paramValue));
                        break;
                }
            }
            // log.info("******** balance value = {}", commissionResponse.getBalance());
        } catch (Exception e) {
            log.info("Error parsing XML response : {}", commissionRequestResponse);
        }
        return commissionResponse;
    }

    private String getValue(NodeList nodeList, int x) {
        return nodeList.item(x).getAttributes().getNamedItem("value").getNodeValue();
    }
}
