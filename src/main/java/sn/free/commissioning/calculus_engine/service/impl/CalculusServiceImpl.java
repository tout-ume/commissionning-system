package sn.free.commissioning.calculus_engine.service.impl;

import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import sn.free.commissioning.calculus_engine.domain.CommissionTypeNode;
import sn.free.commissioning.calculus_engine.domain.PartnerNode;
import sn.free.commissioning.calculus_engine.domain.enumeration.InstantCalculationStatus;
import sn.free.commissioning.calculus_engine.service.CalculusService;
import sn.free.commissioning.config.Constants;
import sn.free.commissioning.domain.*;
import sn.free.commissioning.domain.enumeration.CommissionPaymentStatus;
import sn.free.commissioning.domain.enumeration.CommissionTypeType;
import sn.free.commissioning.domain.enumeration.PalierValueType;
import sn.free.commissioning.payment_engine.domain.PaymentResult;
import sn.free.commissioning.payment_engine.domain.ennumeration.PaymentResultStatus;
import sn.free.commissioning.payment_engine.service.PaymentEngineService;
import sn.free.commissioning.repository.PartnerRepository;
import sn.free.commissioning.service.*;
import sn.free.commissioning.service.dto.TransactionOperationDTO;
import sn.free.commissioning.service.mapper.CommissionAccountMapper;
import sn.free.commissioning.service.mapper.TransactionOperationMapper;
import sn.free.commissioning.web.rest.TransactionOperationResource;

@Service
public class CalculusServiceImpl implements CalculusService {

    private final Logger log = LoggerFactory.getLogger(CalculusServiceImpl.class);
    private static final String CLASS_NAME = String.valueOf(TransactionOperationResource.class);
    private final CommissioningPlanService commissioningPlanService;
    private final PartnerService partnerService;
    private final CommissionAccountService commissionAccountService;
    private final PartnerRepository partnerRepository;
    private final CommissionService commissionService;
    private final PaymentEngineService paymentEngineService;
    private final TransactionOperationMapper transactionOperationMapper;
    private final CommissionAccountMapper commissionAccountMapper;
    private final PartnerProfileService partnerProfileService;

    CalculusServiceImpl(
        CommissioningPlanService commissioningPlanService,
        PartnerService partnerService,
        CommissionAccountService commissionAccountService,
        PartnerRepository partnerRepository,
        CommissionService commissionService,
        PaymentEngineService paymentEngineService,
        TransactionOperationMapper transactionOperationMapper,
        CommissionAccountMapper commissionAccountMapper,
        PartnerProfileService partnerProfileService
    ) {
        this.commissioningPlanService = commissioningPlanService;
        this.partnerService = partnerService;
        this.commissionAccountService = commissionAccountService;
        this.partnerRepository = partnerRepository;
        this.commissionService = commissionService;
        this.paymentEngineService = paymentEngineService;
        this.transactionOperationMapper = transactionOperationMapper;
        this.commissionAccountMapper = commissionAccountMapper;
        this.partnerProfileService = partnerProfileService;
    }

    @Override
    public Set<ConfigurationPlan> getPartnerConfigurationPlans(Partner partner, String zone, String serviceName, Instant transactionDate) {
        log.info("====================== Getting partner's configuration plans");
        Constants.logInfoForService(
            String.valueOf(CalculusServiceImpl.class),
            "Getting partner's configuration plans",
            " Getting partner's configuration plans"
        );
        log.info("====================== Partner : {}, Zone : {}, ServiceName : {}", partner.getMsisdn(), zone, serviceName);
        Constants.logInfoForService(
            String.valueOf(CalculusServiceImpl.class),
            "====================== Partner : {}, Zone : {}, ServiceName : {}" + partner.getMsisdn() + zone + serviceName,
            "====================== Partner : {}, Zone : {}, ServiceName : {}" + partner.getMsisdn() + zone + serviceName
        );

        List<CommissioningPlan> commissioningPlans;
        Set<ConfigurationPlan> configurationPlans = new HashSet<>();
        commissioningPlans = commissioningPlanService.getValidInstantlyPlansByServiceName(serviceName, transactionDate);
        if (!commissioningPlans.isEmpty()) {
            commissioningPlans.forEach(commissioningPlan -> {
                if (!commissioningPlan.getConfigurationPlans().isEmpty()) {
                    log.info("====================== Commission's config plans : {}", commissioningPlan.getConfigurationPlans());

                    commissioningPlan
                        .getConfigurationPlans()
                        .forEach(configurationPlan -> {
                            List<String> zonesNamesTemp = new ArrayList<>();
                            //                            log.info("====================== zones : {} ", configurationPlan.getZones());
                            if (!configurationPlan.getZones().isEmpty()) {
                                //                                log.info("====================== Setting zonesNamesTemp ");
                                configurationPlan
                                    .getZones()
                                    .forEach(zoneItem -> {
                                        zonesNamesTemp.add(zoneItem.getCode());
                                    });
                            }
                            //                            log.info(
                            //                                "====================== configurationPlan.getPartnerProfileId : {}, Partner_profile_id : {} ",
                            //                                configurationPlan.getPartnerProfile().getId(),
                            //                                partner.getPartnerProfileId()
                            //                            );
                            //                            log.info(
                            //                                "====================== configurationPlan.getPartnerProfileId == Partner_profile_id ?? : {} ",
                            //                                configurationPlan.getPartnerProfile().getId().equals(partner.getPartnerProfileId())
                            //                            );
                            //                            log.info("====================== zonesNamesTemp : {}, zone : {}", zonesNamesTemp, zone);
                            //                            log.info("====================== zonesNamesTemp contient zone ? : {}", zonesNamesTemp.contains(zone));
                            if (
                                configurationPlan.getPartnerProfile().getId().equals(partner.getPartnerProfileId()) &&
                                zonesNamesTemp.contains(zone)
                            ) {
                                log.info("====================== Macthing config plan : {}", configurationPlan);
                                Constants.logInfoForService(
                                    String.valueOf(CalculusServiceImpl.class),
                                    "====================== Macthing config plan : {} " + configurationPlan.getId(),
                                    "====================== Macthing config plan : {}  " + configurationPlan.getId()
                                );

                                configurationPlans.add(configurationPlan);
                            }
                        });
                }
            });
        } else {
            log.info("====================== No valid commission plans found");
            Constants.logInfoForService(
                String.valueOf(CalculusServiceImpl.class),
                "======================= No valid commission plans found ",
                "====================== No valid commission plans found  "
            );
        }
        return configurationPlans;
    }

    @Override
    public List<PartnerNode> buildPartnerNodeList(List<Partner> partnerTree, TransactionOperationDTO transactionOperationDTO) {
        log.info("====================== Building partnerNode list");
        Constants.logInfoForService(
            String.valueOf(CalculusServiceImpl.class),
            "====================== Building partnerNode list ",
            "====================== Building partnerNode list  "
        );

        List<PartnerNode> partnerNodes = new ArrayList<>();
        if (!partnerTree.isEmpty()) {
            partnerTree.forEach(partner -> {
                PartnerNode partnerNodeTemp = new PartnerNode();
                Set<ConfigurationPlan> configPlansToSet = getPartnerConfigurationPlans(
                    partner,
                    transactionOperationDTO.getSenderZone(),
                    transactionOperationDTO.getSubType(),
                    transactionOperationDTO.getOperationDate()
                );
                partnerNodeTemp.setPartner(partner);
                log.info("====================== Configuration plans to set : {}", configPlansToSet);
                partnerNodeTemp.setConfigurationPlans(configPlansToSet);
                partnerNodes.add(partnerNodeTemp);
            });
        }
        return partnerNodes;
    }

    // TODO : use filter instead forEach
    public CommissionTypeNode getPalierValue(List<CommissionType> commissionTypes, Double transactionAmount) {
        List<CommissionTypeNode> commissionTypeNodes = new ArrayList<>();
        if (!commissionTypes.isEmpty()) {
            log.info("====================== getPalierValue : CommissionTypes : {} ", commissionTypes);
            Constants.logInfoForService(
                String.valueOf(CalculusServiceImpl.class),
                "====================== getPalierValue  CommissionTypes ",
                "====================== getPalierValue CommissionTypes  "
            );

            commissionTypes.forEach(commissionType -> {
                if (commissionType != null) {
                    if (commissionType.getIsInfinity() != null && commissionType.getIsInfinity()) {
                        // Check if has MaxValue
                        if (transactionAmount >= commissionType.getMinValue()) {
                            CommissionTypeNode commissionTypeNodeTemp = new CommissionTypeNode(
                                commissionType.getPalierValue(),
                                commissionType.getPalierValueType()
                            );
                            commissionTypeNodes.add(commissionTypeNodeTemp);
                        }
                    } else if (transactionAmount >= commissionType.getMinValue() && transactionAmount <= commissionType.getMaxValue()) {
                        CommissionTypeNode commissionTypeNodeTemp = new CommissionTypeNode(
                            commissionType.getPalierValue(),
                            commissionType.getPalierValueType()
                        );
                        commissionTypeNodes.add(commissionTypeNodeTemp);
                    }
                }
            });
        }
        return commissionTypeNodes.get(0);
    }

    @Override
    public Double calculateCommission(ConfigurationPlan configurationPlan, Double transactionAmount) {
        AtomicReference<Double> commissionAmount = new AtomicReference<>(0.0);
        log.info("====================== Configuration plan : {}", configurationPlan.getId());
        Constants.logInfoForService(
            String.valueOf(CalculusServiceImpl.class),
            "====================== Configuration plan : {} " + configurationPlan.getId(),
            "====================== Configuration plan : {} " + configurationPlan.getId()
        );

        log.info("====================== Transaction amount : {}", transactionAmount);
        Constants.logInfoForService(
            String.valueOf(CalculusServiceImpl.class),
            "====================== Transaction amount : {} " + transactionAmount,
            "====================== Transaction amount : " + transactionAmount
        );

        // 1. liste commissionType
        List<CommissionType> commissionTypes;

        commissionTypes = new ArrayList<>(configurationPlan.getCommissionTypes());
        // TODO: what if we have accidentally two commission type of type taux ? So we should verify that it includes
        //  at least one commission type of type taux to go calculate the commission with taux
        if (!commissionTypes.isEmpty()) {
            if (commissionTypes.size() == 1 && commissionTypes.get(0).getCommissionTypeType().equals(CommissionTypeType.TAUX)) {
                log.info("====================== Configurated tauxValue : {}", commissionTypes.get(0).getTauxValue());
                return Math.floor(
                    commissionAmount.updateAndGet(v -> v + (commissionTypes.get(0).getTauxValue() * transactionAmount) / 100.0)
                );
            } else {
                log.info("******************Calcul Commission*****************************");
                CommissionTypeNode palierValue = getPalierValue(commissionTypes, transactionAmount);
                if (palierValue.getPalierValueType().equals(PalierValueType.TAUX)) {
                    commissionAmount.updateAndGet(v -> v + (palierValue.getAmount() * transactionAmount) / 100.0);
                } else {
                    commissionAmount.updateAndGet(v -> v + (palierValue.getAmount()));
                }
            }
        }
        return Math.floor(commissionAmount.get());
    }

    @Override
    public List<Partner> getAllPartnerbyConfigurationPlan(ConfigurationPlan configurationPlan) {
        List<Partner> partners = new ArrayList<>();
        if (configurationPlan != null) {
            PartnerProfile partnerProfile = configurationPlan.getPartnerProfile();
            List<Partner> partnersbyPartnerProfile = partnerRepository.findByPartnerProfileId(partnerProfile.getId());
            for (Partner partner : partnersbyPartnerProfile) {
                for (Zone zone : configurationPlan.getZones()) {
                    if (partner.getZones().contains(zone)) {
                        partners.add(partner);
                        break;
                    }
                }
            }
        }
        return partners;
    }

    @Override
    public Double sumTransactionOperationsAmount(List<TransactionOperation> transactionOperations) {
        Constants.logInfoForService(
            String.valueOf(CalculusServiceImpl.class),
            "====================== Function to sum list of transactions amounts  ",
            "===================== Function to sum list of transactions amounts "
        );
        return transactionOperations.stream().map(TransactionOperation::getAmount).reduce(0.0, Double::sum);
    }

    @Override
    public InstantCalculationStatus performInstantCalculation(TransactionOperationDTO transactionOperationDTO) {
        Commission commissionTemp = new Commission();
        //        1. Get the partner
        Partner partner = partnerService.findByMsisdn(transactionOperationDTO.getAgentMsisdn());
        if (partner == null) {
            log.info("======================= No partner found with msisdn: {}", transactionOperationDTO.getAgentMsisdn());
            Constants.logInfo(
                true,
                CLASS_NAME,
                " No partner found with msisdn: " + transactionOperationDTO.getAgentMsisdn(),
                " No partner found with msisdn:",
                null
            );
            return InstantCalculationStatus.FAILURE;
        } else {
            Set<ConfigurationPlan> configPlansToSet = getPartnerConfigurationPlans(
                partner,
                transactionOperationDTO.getSenderZone(),
                transactionOperationDTO.getSubType(),
                transactionOperationDTO.getOperationDate()
            );
            if (configPlansToSet.isEmpty()) {
                log.info("======================= No configuration plan");
                Constants.logInfo(
                    true,
                    CLASS_NAME,
                    "======================= No configuration plan",
                    "======================= No configuration plan",
                    null
                );
                return InstantCalculationStatus.FAILURE;
            } else {
                //        2. Calculate commission for each configuration
                configPlansToSet.forEach(configurationPlan -> {
                    CommissionAccount commissionAccount;
                    Optional<CommissionAccount> optionalCommissionAccount = commissionAccountService.findByPartner(partner.getId());
                    if (optionalCommissionAccount.isPresent()) {
                        commissionAccount = optionalCommissionAccount.get();
                        log.info("====================== Partner's Commission Account : {}", commissionAccount);
                        Constants.logInfo(true, CLASS_NAME, " Partner's Commission Account  ", "Partner's Commission Account ", null);
                    } else {
                        log.info("====================== Creating Commission Account");
                        commissionAccount = new CommissionAccount();
                        commissionAccount.setPartner(partner);
                        commissionAccount.setCalculatedBalance(0.0);
                        commissionAccount.setPaidBalance(0.0);
                        commissionAccount =
                            commissionAccountMapper.toEntity(
                                commissionAccountService.save(commissionAccountMapper.toDto(commissionAccount))
                            );
                        Constants.logInfo(true, CLASS_NAME, " Creating Commission Account  ", "Creating Commission Account ", null);
                    }
                    log.info("====================== Calculating commission for : {}", partner);
                    Constants.logInfo(
                        true,
                        CLASS_NAME,
                        "  Calculating commission for : {} " + partner,
                        "  Calculating commission for : {} " + partner,
                        null
                    );

                    commissionTemp.setAmount(calculateCommission(configurationPlan, transactionOperationDTO.getAmount()));
                    commissionTemp.setTransactionsAmount(transactionOperationDTO.getAmount());
                    commissionTemp.setCalculatedAt(Instant.now());
                    commissionTemp.setSenderMsisdn(partner.getMsisdn());
                    commissionTemp.setCommissionAccount(commissionAccount);
                    commissionTemp.setCommissionPaymentStatus(CommissionPaymentStatus.TO_BE_PAID);
                    commissionTemp.setSenderProfile(partnerProfileService.findOne(partner.getPartnerProfileId()).get().getCode());
                    commissionTemp.setConfigurationPlan(configurationPlan);
                    commissionTemp.setCommissioningPlanType(configurationPlan.getCommissioningPlan().getCommissioningPlanType());
                    commissionTemp.setServiceType(transactionOperationDTO.getSubType());
                    commissionTemp.setCalculationDate(Instant.now().truncatedTo(ChronoUnit.DAYS));
                    commissionTemp.setCalculationShortDate(LocalDate.now());
                    commissionTemp.setSpare1(transactionOperationDTO.getTid());
                    Set<TransactionOperation> transactionOperations = new HashSet<>();
                    transactionOperations.add(transactionOperationMapper.toEntity(transactionOperationDTO));
                    commissionTemp.setTransactionOperations(transactionOperations);
                    commissionAccount.setCalculatedBalance(commissionAccount.getCalculatedBalance() + commissionTemp.getAmount());
                    commissionAccount.getCommissions().add(commissionTemp);
                    commissionAccountService.update(commissionAccountMapper.toDto(commissionAccount));
                    log.info("====================== Partner's Commission Account After Calculation : {}", commissionAccount);
                    Constants.logInfo(
                        true,
                        CLASS_NAME,
                        "  Partner's Commission Account After Calculation  ",
                        " Partner's Commission Account After Calculation ",
                        null
                    );
                });

                //        3. Save the calculated commission
                log.info("====================== Saving commission : {}", commissionTemp);
                Constants.logInfo(true, CLASS_NAME, "  Saving commissions ", "Saving commissions ", null);
                Commission savedCommission = commissionService.save(commissionTemp);

                //        4. Perform Instant payment
                String paymentComment =
                    "COMMISSION@" + transactionOperationDTO.getSubType().toUpperCase() + "@" + transactionOperationDTO.getTid();
                log.info("======================= Performing payment for commission: {}", savedCommission);
                Constants.logInfo(true, CLASS_NAME, "  Performing payment for commission ", " Performing payment for commission ", null);
                PaymentResult paymentResult = paymentEngineService.performCommissionPayment(savedCommission, paymentComment, false);
                if (paymentResult.getStatus() == PaymentResultStatus.SUCCESS) {
                    //      Here we gotta make sure we fetch the commissionAccount from DB because commissions has different value of commissionAccount calculated and paid amounts
                    Optional<CommissionAccount> optionalCommissionAccount = commissionAccountService.findByPartner(
                        savedCommission.getCommissionAccount().getPartner().getId()
                    );
                    log.info("====================== CommissionAccount : {}", optionalCommissionAccount);
                    if (optionalCommissionAccount.isPresent()) {
                        CommissionAccount commissionAccount = optionalCommissionAccount.get();
                        commissionAccount.setPaidBalance(commissionAccount.getPaidBalance() + savedCommission.getAmount());
                        commissionAccountService.update(commissionAccountMapper.toDto(commissionAccount));
                        log.info("====================== Partner's Commission Account After Payment : {}", commissionAccount);
                        Constants.logInfo(
                            true,
                            CLASS_NAME,
                            "  Partner's Commission Account After Payment ",
                            "Partner's Commission Account After Payment ",
                            null
                        );
                    } else {
                        log.info(
                            "====================== No CommissionAccount for Partner : {}",
                            savedCommission.getCommissionAccount().getPartner()
                        );
                        Constants.logInfo(
                            true,
                            CLASS_NAME,
                            "====================== No CommissionAccount for Partner : " +
                            savedCommission.getCommissionAccount().getPartner(),
                            "====================== No CommissionAccount for Partner : " +
                            savedCommission.getCommissionAccount().getPartner(),
                            null
                        );
                    }
                }
                log.info("======================= Payment result: {}", paymentResult.getStatus().name());
                Constants.logInfo(
                    paymentResult.getStatus().name().equalsIgnoreCase("SUCCESS"),
                    CLASS_NAME,
                    "   Payment result is " + paymentResult.getStatus().name(),
                    " Payment result",
                    null
                );
                return paymentResult.getStatus().name().equalsIgnoreCase("SUCCESS")
                    ? InstantCalculationStatus.SUCCESS
                    : InstantCalculationStatus.NOT_PAID;
            }
        }
    }

    @Override
    public Double calculateTopProfileCommission(CommissioningPlan plan, ConfigurationPlan configuration, Double globalCommissionAmount) {
        List<ConfigurationPlan> hisNetworkConfigs = plan
            .getConfigurationPlans()
            .stream()
            .filter(config ->
                (
                    !Objects.equals(config.getId(), configuration.getId()) &&
                    !config.getPartnerProfile().getCode().equalsIgnoreCase("PDV/PC") &&
                    !config.getPartnerProfile().getCode().equalsIgnoreCase("PDV_SA") &&
                    !config.getPartnerProfile().getCode().equalsIgnoreCase("PDV")
                )
            )
            .collect(Collectors.toList());
        AtomicReference<Double> hisNetworkSharePercentage = new AtomicReference<>(0.0);
        // 1. liste commissionType
        List<CommissionType> commissionTypes = new ArrayList<>();
        hisNetworkConfigs.forEach(configurationPlan -> commissionTypes.addAll(configurationPlan.getCommissionTypes()));

        commissionTypes.forEach(commissionType -> {
            if (commissionType.getCommissionTypeType().equals(CommissionTypeType.TAUX)) {
                hisNetworkSharePercentage.updateAndGet(v -> v + commissionType.getTauxValue());
            }
        });

        Double result = globalCommissionAmount - ((hisNetworkSharePercentage.get() * globalCommissionAmount) / 100);
        log.info("====================== Global commission amount : {}, hisCommission : {}", globalCommissionAmount, result);
        Constants.logInfoForService(
            String.valueOf(CalculusServiceImpl.class),
            "====================== Global commission amount : " + globalCommissionAmount + ", hisCommission : " + result,
            "====================== Global commission amount : " + globalCommissionAmount + ", hisCommission : " + result
        );
        return result;
    }
}
