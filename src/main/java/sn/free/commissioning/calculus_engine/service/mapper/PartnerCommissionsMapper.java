package sn.free.commissioning.calculus_engine.service.mapper;

import java.time.Instant;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import org.mapstruct.Mapper;
import sn.free.commissioning.calculus_engine.service.dto.PartnerCommissionsDTO;
import sn.free.commissioning.domain.Commission;
import sn.free.commissioning.service.dto.CommissionDTO;

/**
 * Mapper for the entity {@link Commission} and its DTO {@link CommissionDTO}.
 */
@Mapper(componentModel = "spring")
public interface PartnerCommissionsMapper {
    default PartnerCommissionsDTO convert(
        Map.Entry<String, List<Commission>> commissions,
        String frequency,
        Instant startDateStr,
        Instant endDateStr
    ) {
        PartnerCommissionsDTO partnerCommissionsDTO = new PartnerCommissionsDTO();
        String strDate = commissions.getKey();
        Instant startDate = Instant.now();
        Instant endDate = Instant.now();

        if (frequency.equals("daily")) {
            startDate = Instant.parse(strDate);
            endDate = startDate;
        }
        if (frequency.equals("monthly")) {
            ZoneId zone = ZoneId.of("Africa/Dakar");
            strDate = strDate + "-01T00:00:00.00Z";
            startDate = Instant.parse(strDate);
            YearMonth month = YearMonth.from(startDate.atZone(zone).toLocalDate());
            endDate = Instant.from(month.atEndOfMonth().atStartOfDay(zone));
        }
        if (frequency.equals("cyclic")) {
            startDate = startDateStr;
            endDate = endDateStr;
        }
        partnerCommissionsDTO.setMfsDTOs(commissions);
        partnerCommissionsDTO.setMobileDTOs(commissions);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy").withZone(ZoneId.systemDefault());

        String startStr = formatter.format(startDate);
        String endStr = formatter.format(endDate);
        partnerCommissionsDTO.setStartdate(startStr);
        partnerCommissionsDTO.setEndate(endStr);

        return partnerCommissionsDTO;
    }
}
