package sn.free.commissioning.calculus_engine.service;

import java.time.Instant;
import java.util.List;
import java.util.Set;
import sn.free.commissioning.calculus_engine.domain.PartnerNode;
import sn.free.commissioning.calculus_engine.domain.enumeration.InstantCalculationStatus;
import sn.free.commissioning.domain.CommissioningPlan;
import sn.free.commissioning.domain.ConfigurationPlan;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.domain.TransactionOperation;
import sn.free.commissioning.service.dto.TransactionOperationDTO;

/**
 * Service Interface for managing Calculus.
 */
public interface CalculusService {
    /**
     * get partner configuration plans
     *
     * @param partner         the partner whom configurationPlans we want.
     * @param zone            the partner's zone.
     * @param serviceName     the transaction service type.
     * @param transactionDate the transaction's date.
     * @return a list of configuration plans.
     */
    Set<ConfigurationPlan> getPartnerConfigurationPlans(Partner partner, String zone, String serviceName, Instant transactionDate);

    /**
     * build partnerNode list
     *
     * @param partnerTree               the partner tree .
     * @param transactionOperationDTO   the transaction.
     * @return a list of partner node.
     */
    List<PartnerNode> buildPartnerNodeList(List<Partner> partnerTree, TransactionOperationDTO transactionOperationDTO);

    /**
     * calculate commission for a partnerNode
     *
     * @param configurationPlan   the partnerNode's configurationPlan.
     * @param transactionAmount   the transaction's amount.
     * @return the commission amount.
     */
    Double calculateCommission(ConfigurationPlan configurationPlan, Double transactionAmount);

    /**
     * get partner configuration plans by zone and partnerprofile
     *
     * @param configurationPlan   the configurationPlan.
     * @return  a list of Partner.
     */
    List<Partner> getAllPartnerbyConfigurationPlan(ConfigurationPlan configurationPlan);

    /**
     *  sum transaction operation list
     *
     * @param transactionOperations the list of transction operations.
     * @return  the amount of sum transaction operation list.
     */
    Double sumTransactionOperationsAmount(List<TransactionOperation> transactionOperations);

    /**
     * performs instant calculation for a transaction
     *
     * @param transaction the transaction.
     * @return the operation result.
     */
    InstantCalculationStatus performInstantCalculation(TransactionOperationDTO transaction);

    /**
     * calculate commission for a topProfile partner
     *
     * @param plan   the commissionning plan.
     * @param configuration   the topProfile configuration.
     * @param globalCommissionAmount   the partner's netWork global commission.
     * @return the partner's commission amount.
     */
    Double calculateTopProfileCommission(CommissioningPlan plan, ConfigurationPlan configuration, Double globalCommissionAmount);
}
