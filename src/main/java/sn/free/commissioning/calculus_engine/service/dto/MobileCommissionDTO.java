package sn.free.commissioning.calculus_engine.service.dto;

import java.io.Serializable;
import java.time.LocalDate;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

public class MobileCommissionDTO implements Serializable {

    private Double totalcom = 0.0;
    private Double airtime = 0.0;

    private Double bundle = 0.0;

    private Double totalvolume = 0.0;

    private Double totalvnormal = 0.0;
    private Double totalvfraude = 0.0;
    private Double totalregularise = 0.0;

    private Double comnormale = 0.0;
    private Double comfraude = 0.0;
    private Double comregularisee = 0.0;

    private String comstatus = "VALIDE";

    public Double getTotalcom() {
        return this.totalcom;
    }

    public void setTotalcom(Double totalcom) {
        this.totalcom = totalcom;
    }

    public Double getAirtime() {
        return this.airtime;
    }

    public void setAirtime(Double airtime) {
        this.airtime = airtime;
    }

    public Double getBundle() {
        return this.bundle;
    }

    public void setBundle(Double bundle) {
        this.bundle = bundle;
    }

    public Double getTotalvolume() {
        return this.totalvolume;
    }

    public void setTotalvolume(Double totalvolume) {
        this.totalvolume = totalvolume;
    }

    public Double getTotalvnormal() {
        return this.totalvnormal;
    }

    public void setTotalvnormal(Double totalvnormal) {
        this.totalvnormal = totalvnormal;
    }

    public Double getTotalvfraude() {
        return this.totalvfraude;
    }

    public void setTotalvfraude(Double totalvfraude) {
        this.totalvfraude = totalvfraude;
    }

    public Double getTotalregularise() {
        return this.totalregularise;
    }

    public void setTotalregularise(Double totalregularise) {
        this.totalregularise = totalregularise;
    }

    public Double getComfraude() {
        return this.comfraude;
    }

    public void setComfraude(Double comfraude) {
        this.comfraude = comfraude;
    }

    public Double getComregularisee() {
        return this.comregularisee;
    }

    public void setComregularisee(Double comregularisee) {
        this.comregularisee = comregularisee;
    }

    public String getComstatus() {
        return this.comstatus;
    }

    public void setComstatus(String comstatus) {
        this.comstatus = comstatus;
    }

    @Override
    public String toString() {
        return (
            "{" +
            " totalcom='" +
            getTotalcom() +
            "'" +
            ", airtime='" +
            getAirtime() +
            "'" +
            ", bundle='" +
            getBundle() +
            "'" +
            ", totalvolume='" +
            getTotalvolume() +
            "'" +
            ", totalvnormal='" +
            getTotalvnormal() +
            "'" +
            ", totalvfraude='" +
            getTotalvfraude() +
            "'" +
            ", totalregularise='" +
            getTotalregularise() +
            "'" +
            ", comnormal='" +
            getComnormale() +
            "'" +
            ", comfraude='" +
            getComfraude() +
            "'" +
            ", comregularisee='" +
            getComregularisee() +
            "'" +
            ", comstatus='" +
            getComstatus() +
            "'" +
            "}"
        );
    }

    public Double getComnormale() {
        return comnormale;
    }

    public void setComnormale(Double comnormale) {
        this.comnormale = comnormale;
    }
}
