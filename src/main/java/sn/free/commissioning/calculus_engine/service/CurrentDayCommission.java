package sn.free.commissioning.calculus_engine.service;

import sn.free.commissioning.calculus_engine.domain.CurrentDayCommissionResponse;

public interface CurrentDayCommission {
    CurrentDayCommissionResponse getCurrentDay(String msisdn, String frequency) throws Exception;
}
