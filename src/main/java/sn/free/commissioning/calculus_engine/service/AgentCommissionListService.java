package sn.free.commissioning.calculus_engine.service;

import java.time.Instant;
import java.util.Collection;
import java.util.List;
import sn.free.commissioning.calculus_engine.service.dto.PartnerCommissionsDTO;

public interface AgentCommissionListService {
    public List<PartnerCommissionsDTO> getAgentCommissionsForPeriod(String msisdn, String frequency, Instant dateStart, Instant dateEnd);
}
