package sn.free.commissioning.calculus_engine.service.impl;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import sn.free.commissioning.calculus_engine.domain.CurrentDayCommissionResponse;
import sn.free.commissioning.calculus_engine.service.CalculusService;
import sn.free.commissioning.calculus_engine.service.CommissionSimulationService;
import sn.free.commissioning.calculus_engine.service.CurrentDayCommission;
import sn.free.commissioning.calculus_engine.service.dto.MfsCommissionDTO;
import sn.free.commissioning.calculus_engine.service.dto.MobileCommissionDTO;
import sn.free.commissioning.calculus_engine.service.dto.PartnerCommissionsDTO;
import sn.free.commissioning.domain.CommissioningPlan;
import sn.free.commissioning.domain.ConfigurationPlan;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.domain.PartnerProfile;
import sn.free.commissioning.domain.Zone;
import sn.free.commissioning.domain.enumeration.CommissioningPlanState;
import sn.free.commissioning.domain.enumeration.CommissioningPlanType;
import sn.free.commissioning.repository.CommissioningPlanRepository;
import sn.free.commissioning.repository.ConfigurationPlanRepository;
import sn.free.commissioning.repository.PartnerProfileRepository;
import sn.free.commissioning.repository.PartnerRepository;
import sn.free.commissioning.repository.ZoneRepository;

@Service
public class CommissionSimulationServiceImpl implements CommissionSimulationService {

    private final Logger log = LoggerFactory.getLogger(CommissionSimulationServiceImpl.class);

    private final CurrentDayCommission currentDayCommission;
    private final CalculusService calculus;

    private final PartnerRepository partnerRepository;

    private final PartnerProfileRepository profileRepository;

    private final CommissioningPlanRepository commissioningPlanRepository;

    public CommissionSimulationServiceImpl(
        CurrentDayCommission currentDayCommission,
        CalculusService calculus,
        PartnerRepository partnerRepository,
        PartnerProfileRepository profileRepository,
        CommissioningPlanRepository commissioningPlanRepository
    ) {
        this.currentDayCommission = currentDayCommission;
        this.calculus = calculus;
        this.partnerRepository = partnerRepository;
        this.profileRepository = profileRepository;
        this.commissioningPlanRepository = commissioningPlanRepository;
    }

    @Override
    public PartnerCommissionsDTO simulate(String msisdn, String frequency) {
        PartnerCommissionsDTO partnerCommission = new PartnerCommissionsDTO();
        Instant startDate = Instant.now();
        Instant endDate = Instant.now().plusSeconds(86399);
        MfsCommissionDTO mfs = new MfsCommissionDTO();
        MobileCommissionDTO mobile = new MobileCommissionDTO();
        if (frequency.equals("monthly")) {
            startDate = Instant.parse(LocalDate.now().with(TemporalAdjusters.firstDayOfMonth()) + "T00:00:00Z");
            endDate = Instant.parse(LocalDate.now().with(TemporalAdjusters.lastDayOfMonth()) + "T23:59:59Z");
        }
        if (frequency.equals("cyclic")) {
            Instant middle = Instant.parse(LocalDate.now().withDayOfMonth(16) + "T00:00:00Z");
            if (startDate.isBefore(middle)) {
                startDate = Instant.parse(LocalDate.now().withDayOfMonth(1) + "T00:00:00Z");
                endDate = Instant.parse(LocalDate.now().withDayOfMonth(15) + "T23:59:59Z");
            } else {
                startDate = Instant.parse(LocalDate.now().withDayOfMonth(16) + "T00:00:00Z");
                endDate = Instant.parse(LocalDate.now().with(TemporalAdjusters.lastDayOfMonth()) + "T23:59:59Z");
            }
        }

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy").withZone(ZoneId.systemDefault());

        String startStr = formatter.format(startDate);
        String endStr = formatter.format(endDate);
        partnerCommission.setStartdate(startStr);
        partnerCommission.setEndate(endStr);
        try {
            CurrentDayCommissionResponse cdc = currentDayCommission.getCurrentDay(msisdn, frequency);
            mfs.setBillpayment(cdc.getBillPay());
            mfs.setCashin(cdc.getCashIn());
            mfs.setCashout(cdc.getCashOut());
            // mfs.setTotalCom(cdc.getTotalCom());
            mfs.setTotalcom(cdc.getTotalJour());
            mfs.setComnormale(cdc.getTotalCom());
            mfs.setComstatus("SOUS RESERVE");
            Double totalVolumeMfs = mfs.getCashin() + mfs.getCashout() + mfs.getBillpayment();
            Double totalComMfs = calculate(msisdn, CommissioningPlanType.MFS, totalVolumeMfs);
            mfs.setTotalcom(totalComMfs);
            mobile.setAirtime(cdc.getAirtimeAndBundle());
            mobile.setBundle(0.0);
            // mobile.setCashout(cdc.getCashOut());
            // mobile.setTotalCom(cdc.getTotalCom());
            mobile.setTotalcom(cdc.getAirtimeAndBundle());
            mobile.setComnormale(cdc.getAirtimeAndBundle());
            mobile.setComstatus("SOUS RESERVE");

            Double totalVolumeMobile = mobile.getAirtime() + mobile.getBundle();
            Double totalComMobile = calculate(msisdn, CommissioningPlanType.MOBILE, totalVolumeMobile);
            mobile.setTotalcom(totalComMobile);

            log.info("------------------>>>>>> {}", mobile.toString());
            partnerCommission.setFreemoney(mfs);
            partnerCommission.setMobile(mobile);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            mobile.setComstatus("SOUS RESERVE");
            mfs.setComstatus("SOUS RESERVE");
            partnerCommission.setFreemoney(mfs);
            partnerCommission.setMobile(mobile);
            e.printStackTrace();
        }
        return partnerCommission;
    }

    @Override
    public Double calculate(String msisdn, CommissioningPlanType planType, Double amount) {
        log.info("------------------>>>>>> Calculating for {}, amount: {}", msisdn, amount);
        Partner partner = partnerRepository.findByMsisdn(msisdn);
        PartnerProfile profile = profileRepository.findById(partner.getPartnerProfileId()).get();
        List<String> partnerZones = partner.getZones().stream().map(Zone::getCode).collect(Collectors.toList());
        List<CommissioningPlan> plans = commissioningPlanRepository.findByCommissioningPlanTypeAndState(
            planType,
            CommissioningPlanState.IN_USE
        );
        log.info("------------------>>>>>> Fetched plans : {}", plans);
        List<Double> commissions = new ArrayList<Double>();
        plans.forEach(plan -> {
            log.info("------------------>>>>>> Plan {}", plan.getName());
            // List<ConfigurationPlan> confs = plan.getConfigurationPlans()
            plan
                .getConfigurationPlans()
                .stream()
                .filter(cf -> {
                    List<String> zonePresent = cf
                        .getZones()
                        .stream()
                        .map(Zone::getCode)
                        .filter(zone -> {
                            return partnerZones.stream().anyMatch(zone::equals);
                        })
                        .collect(Collectors.toList());
                    return cf.getPartnerProfile().getCode().equals(profile.getCode()) && !zonePresent.isEmpty();
                })
                .forEach(config -> {
                    log.info("------------------>>>>>> Config for {}", config.getPartnerProfile().getCode());
                    Double commission = calculus.calculateCommission(config, amount);
                    commissions.add(commission);
                });
            // .collect(Collectors.toList());
        });
        return commissions.stream().reduce(0.0, (a, b) -> Double.sum(a, b));
    }
}
