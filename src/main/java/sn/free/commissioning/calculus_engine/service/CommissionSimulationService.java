package sn.free.commissioning.calculus_engine.service;

import sn.free.commissioning.calculus_engine.service.dto.PartnerCommissionsDTO;
import sn.free.commissioning.domain.enumeration.CommissioningPlanType;

public interface CommissionSimulationService {
    PartnerCommissionsDTO simulate(String msisdn, String frequency);

    Double calculate(String msisdn, CommissioningPlanType planType, Double amount);
}
