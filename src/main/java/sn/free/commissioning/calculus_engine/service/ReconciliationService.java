package sn.free.commissioning.calculus_engine.service;

import org.springframework.stereotype.Service;

public interface ReconciliationService {
    /**
     *  Calculate instantly commissions on transactions that failed to pass through API
     *
     */
    void instantlyCommissionsReconciliation();
}
