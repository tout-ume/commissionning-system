package sn.free.commissioning.calculus_engine.service.impl;

import static java.util.stream.Collectors.groupingBy;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import sn.free.commissioning.calculus_engine.domain.DailyCalculationInfo;
import sn.free.commissioning.calculus_engine.domain.NetworkInformations;
import sn.free.commissioning.calculus_engine.domain.TimeInterval;
import sn.free.commissioning.calculus_engine.service.CalculusService;
import sn.free.commissioning.calculus_engine.service.DeferredCalculusService;
import sn.free.commissioning.calculus_engine.utils.CalculusUtilities;
import sn.free.commissioning.config.Constants;
import sn.free.commissioning.domain.*;
import sn.free.commissioning.domain.enumeration.CommissionPaymentStatus;
import sn.free.commissioning.domain.enumeration.CommissioningPlanState;
import sn.free.commissioning.repository.*;
import sn.free.commissioning.service.CommissionAccountService;
import sn.free.commissioning.service.CommissioningPlanService;
import sn.free.commissioning.service.TransactionOperationService;
import sn.free.commissioning.service.dto.TransactionOperationDTO;
import sn.free.commissioning.service.mapper.CommissionAccountMapper;

@Service
public class DeferredCalculusServiceImpl implements DeferredCalculusService {

    private final CommissioningPlanService commissioningPlanService;
    private final PartnerRepository partnerRepository;
    private final TransactionOperationService transactionOperationService;
    private final CalculusService calculusService;
    private final FrequencyRepository frequencyRepository;
    private final CommissionRepository commissionRepository;
    private final CommissionAccountService commissionAccountService;
    private final CommissionAccountMapper commissionAccountMapper;
    private final PartnerFromTree partnerFromTree;
    private final Logger log = LoggerFactory.getLogger(DeferredCalculusServiceImpl.class);
    private final TransactionOperationRepository transactionOperationRepository;
    private final CommissioningPlanRepository commissioningPlanRepository;

    public DeferredCalculusServiceImpl(
        CommissioningPlanService commissioningPlanService,
        PartnerRepository partnerRepository,
        TransactionOperationService transactionOperationService,
        FrequencyRepository frequencyRepository,
        CommissionRepository commissionRepository,
        CalculusService calculusService,
        CommissionAccountService commissionAccountService,
        CommissionAccountMapper commissionAccountMapper,
        PartnerFromTree partnerFromTree,
        TransactionOperationRepository transactionOperationRepository,
        CommissioningPlanRepository commissioningPlanRepository
    ) {
        this.commissioningPlanService = commissioningPlanService;
        this.partnerRepository = partnerRepository;
        this.transactionOperationService = transactionOperationService;
        this.frequencyRepository = frequencyRepository;
        this.calculusService = calculusService;
        this.commissionRepository = commissionRepository;
        this.commissionAccountService = commissionAccountService;
        this.commissionAccountMapper = commissionAccountMapper;
        this.partnerFromTree = partnerFromTree;
        this.transactionOperationRepository = transactionOperationRepository;
        this.commissioningPlanRepository = commissioningPlanRepository;
    }

    private List<Partner> processConfiguration(ConfigurationPlan configuration, List<String> zones) {
        List<Partner> partners = partnerRepository.findByPartnerProfileIdAndZones_CodeIsIn(
            configuration.getPartnerProfile().getId(),
            zones
        );
        log.info("====================== Partners : {}", partners.stream().map(Partner::getMsisdn).collect(Collectors.toList()));
        return partners;
    }

    private List<Commission> processPartner(
        List<NetworkInformations> topProfilesNetworkInformations,
        Map<String, List<TransactionOperation>> transactionsByMsisdn,
        Partner partner,
        String partnerProfileCode,
        TimeInterval timeInterval,
        ConfigurationPlan configuration,
        CommissioningPlan plan
    ) {
        List<Commission> calculatedCommissions = new ArrayList<>();
        List<TransactionOperation> transactions = new ArrayList<>();
        AtomicReference<Double> commissionAmount = new AtomicReference<>(0.0);
        //  On vérifie dabord si le partner est un pdv
        boolean isLowLevelProfile =
            partnerProfileCode.equals("PDV/PC") || partnerProfileCode.equals("PDV") || partnerProfileCode.equals("PDV_SA");
        if (isLowLevelProfile) {
            transactions = transactionsByMsisdn.get(partner.getMsisdn());
        } else {
            List<Partner> tree = partnerFromTree.from(partner).get();
            log.info("====================== fetched tree's size : {}", tree.size());
            log.info(
                "====================== fetched tree for partner {} : {}",
                partner.getMsisdn(),
                tree.stream().map(Partner::getMsisdn).collect(Collectors.toList())
            );
            transactions =
                tree
                    .stream()
                    .filter(treeMember -> transactionsByMsisdn.get(treeMember.getMsisdn()) != null)
                    .flatMap(treeMember -> transactionsByMsisdn.get(treeMember.getMsisdn()).stream())
                    .collect(Collectors.toList());
        }
        if (transactions == null || transactions.isEmpty()) {
            // No transactions
            return new ArrayList<>();
        }
        log.info(
            "====================== fetched transactions for {} : {}",
            partner.getMsisdn(),
            transactions.stream().map(TransactionOperation::getId).collect(Collectors.toList())
        );
        Double transactionAmount = calculusService.sumTransactionOperationsAmount(transactions);
        if (!isLowLevelProfile) {
            topProfilesNetworkInformations
                .stream()
                .forEach(networkInformations -> {
                    if (
                        new HashSet<>(networkInformations.getZones())
                            .containsAll(partner.getZones().stream().map(Zone::getCode).collect(Collectors.toList()))
                    ) {
                        log.info(
                            "====================== Processing for partner {}, hisNetworkTransactionsAmount : {}, totalNetworkTransactionsAmount : {}",
                            partner.getMsisdn(),
                            transactionAmount,
                            networkInformations.getTotalNetworkTransactionsAmount()
                        );
                        commissionAmount.set(
                            (transactionAmount / networkInformations.getTotalNetworkTransactionsAmount()) *
                            calculusService.calculateCommission(configuration, networkInformations.getGlobalCommissionAmount())
                        );
                    }
                });
        } else {
            log.info("====================== calculusService.sumTransactionOperationsAmount : {}", transactionAmount);
            commissionAmount.set(calculusService.calculateCommission(configuration, transactionAmount));
        }
        CommissionAccount commissionAccount;
        Optional<CommissionAccount> optionalCommissionAccount = commissionAccountService.findByPartner(partner.getId());
        if (optionalCommissionAccount.isPresent()) {
            commissionAccount = optionalCommissionAccount.get();
            commissionAccount.setCalculatedBalance(commissionAccount.getCalculatedBalance() + commissionAmount.get());
            commissionAccountService.update(commissionAccountMapper.toDto(commissionAccount));
            log.info("====================== Partner's Commission Account : {}", commissionAccount);
        } else {
            log.info("====================== Creating Commission Account");
            commissionAccount = new CommissionAccount();
            commissionAccount.setPartner(partner);
            commissionAccount.setCalculatedBalance(commissionAmount.get());
            commissionAccount.setPaidBalance(0.0);
            commissionAccount =
                commissionAccountMapper.toEntity(commissionAccountService.save(commissionAccountMapper.toDto(commissionAccount)));
        }
        log.info("====================== Total Transaction Amount : {}", transactionAmount);
        Constants.logInfoForService(
            String.valueOf(DeferredCalculusServiceImpl.class),
            "====================== Total Transaction Amount :" + transactionAmount,
            "===================== Total Transaction Amount : " + transactionAmount
        );

        log.info("====================== Calculated amount : {}", commissionAmount);
        Constants.logInfoForService(
            String.valueOf(DeferredCalculusServiceImpl.class),
            "====================== Calculated amount : " + commissionAmount,
            "=====================  Calculated amount : " + commissionAmount
        );

        Commission commissionTemp;
        Optional<Commission> commission = commissionRepository.findBySenderMsisdnAndConfigurationPlan_IdAndCalculationShortDate(
            partner.getMsisdn(),
            configuration.getId(),
            timeInterval.getCalculationDate().atZone(ZoneOffset.UTC).toLocalDate()
        );

        if (commission.isPresent()) {
            log.info("====================== found commission when recalculating : {} ", commission.get());
            commissionTemp = commission.get();
            if (!commissionTemp.getAmount().equals(commissionAmount.get())) {
                double diff = commissionTemp.getAmount() - commissionAmount.get();
                commissionTemp.setFraudAmount(commissionTemp.getFraudAmount() + diff);
            }
            commissionTemp.setAmount(commissionAmount.get());
            commissionTemp.setCalculatedAt(Instant.now());
            commissionRepository.save(commissionTemp);
        } else {
            commissionTemp = new Commission();
            commissionTemp.setAmount(commissionAmount.get());
            commissionTemp.setFraudAmount(0.0);
            commissionTemp.setCalculatedAt(Instant.now());
            commissionTemp.setSenderMsisdn(partner.getMsisdn());
            commissionTemp.setSenderProfile(partnerProfileCode);
            commissionTemp.setCommissionAccount(commissionAccount);
            commissionTemp.setConfigurationPlan(configuration);
            commissionTemp.setServiceType(plan.getServices().stream().map(ServiceType::getName).collect(Collectors.toList()).toString());
            commissionTemp.setCommissionPaymentStatus(CommissionPaymentStatus.TO_BE_PAID);
            commissionTemp.setConfigurationPlan(configuration);
            commissionTemp.setCommissioningPlanType(plan.getCommissioningPlanType());
            Set<TransactionOperation> transactionOperations = new HashSet<TransactionOperation>(transactions);
            commissionTemp.setTransactionOperations(transactionOperations);
            // Double transactionAmount = calculusService.sumTransactionOperationsAmount(transactions);

            commissionTemp.setSpare3(String.valueOf(setSpare3Value(transactions)));
            commissionTemp.setTransactionsAmount(transactionAmount);
            commissionTemp.setCalculationDate(timeInterval.getCalculationDate());
            commissionTemp.setCalculationShortDate(timeInterval.getCalculationDate().atZone(ZoneId.of("Africa/Dakar")).toLocalDate());

            calculatedCommissions.add(commissionTemp);

            log.info("====================== Calculated commission : {}", commissionTemp);
        }
        return calculatedCommissions;
    }

    private String setSpare3Value(List<TransactionOperation> transactions) {
        List<Map<String, Object>> transactionByService = transactions
            .stream()
            .collect(groupingBy(TransactionOperation::getSubType))
            .entrySet()
            .stream()
            .map(c -> {
                Map<String, Object> serviceMap = new HashMap<String, Object>();
                Double total = calculusService.sumTransactionOperationsAmount(c.getValue());
                serviceMap.put("name", c.getKey());
                serviceMap.put("amount", total.toString());
                return serviceMap;
            })
            .collect(Collectors.toList());
        List<JSONObject> jsonObj = new ArrayList<JSONObject>();
        for (Map<String, Object> totalAmount : transactionByService) {
            JSONObject obj = new JSONObject(totalAmount);
            jsonObj.add(obj);
        }
        return new JSONArray(jsonObj).toString();
    }

    private NetworkInformations processTopProfilePartner(
        Partner partner,
        Map<String, List<TransactionOperation>> transactionsByMsisdn,
        NetworkInformations networkInformations,
        String partnerProfileCode,
        TimeInterval timeInterval,
        ConfigurationPlan configuration,
        CommissioningPlan plan
    ) {
        List<Commission> calculatedCommissions = new ArrayList<>();
        List<TransactionOperation> transactions;

        log.info("====================== processTopProfilePartner partner : {}", partner);
        List<Partner> tree = partnerFromTree.from(partner).get();
        log.info("====================== processTopProfilePartner fetched arborescence's : {}", tree.size());
        log.info("====================== processTopProfilePartner fetched partner : {}", partner);
        transactions =
            tree
                .stream()
                .filter(treeMember -> transactionsByMsisdn.get(treeMember.getMsisdn()) != null)
                .flatMap(treeMember -> transactionsByMsisdn.get(treeMember.getMsisdn()).stream())
                .collect(Collectors.toList());
        log.info(
            "====================== processTopProfilePartner fetched transactions : {}",
            transactions.stream().map(TransactionOperation::getId).collect(Collectors.toList())
        );
        if (transactions.isEmpty()) {
            // No transactions
            return networkInformations;
        }
        Double transactionAmount = calculusService.sumTransactionOperationsAmount(transactions);
        log.info("====================== processTopProfilePartner calculusService.sumTransactionOperationsAmount : {}", transactionAmount);
        Double globalCommissionAmount = calculusService.calculateCommission(configuration, transactionAmount);
        Double commissionAmount = calculusService.calculateTopProfileCommission(plan, configuration, globalCommissionAmount);

        networkInformations.setTotalNetworkTransactionsAmount(transactionAmount);
        networkInformations.setTopProfieCommissionAmount(commissionAmount);
        networkInformations.setGlobalCommissionAmount(globalCommissionAmount);
        CommissionAccount commissionAccount;
        Optional<CommissionAccount> optionalCommissionAccount = commissionAccountService.findByPartner(partner.getId());
        if (optionalCommissionAccount.isPresent()) {
            commissionAccount = optionalCommissionAccount.get();
            log.info("====================== processTopProfilePartner Partner's Commission Account : {}", commissionAccount);
            commissionAccount.setCalculatedBalance(commissionAccount.getCalculatedBalance() + commissionAmount);
            commissionAccountService.update(commissionAccountMapper.toDto(commissionAccount));
        } else {
            log.info("====================== processTopProfilePartner Creating Commission Account");
            commissionAccount = new CommissionAccount();
            commissionAccount.setPartner(partner);
            commissionAccount.setCalculatedBalance(commissionAmount);
            commissionAccount.setPaidBalance(0.0);
            commissionAccount =
                commissionAccountMapper.toEntity(commissionAccountService.save(commissionAccountMapper.toDto(commissionAccount)));
        }

        log.info("====================== processTopProfilePartner Total Transaction Amount : {}", transactionAmount);
        Constants.logInfoForService(
            String.valueOf(DeferredCalculusServiceImpl.class),
            "====================== processTopProfilePartner Total Transaction Amount :" + transactionAmount,
            "===================== processTopProfilePartner Total Transaction Amount : " + transactionAmount
        );

        log.info("====================== processTopProfilePartner Calculated commission amount : {}", commissionAmount);
        Constants.logInfoForService(
            String.valueOf(DeferredCalculusServiceImpl.class),
            "====================== processTopProfilePartner Calculated commission amount : " + commissionAmount,
            "===================== processTopProfilePartner Calculated commission amount : " + commissionAmount
        );

        Commission commissionTemp;
        Optional<Commission> commission = commissionRepository.findBySenderMsisdnAndConfigurationPlan_IdAndCalculationShortDate(
            partner.getMsisdn(),
            configuration.getId(),
            timeInterval.getCalculationDate().atZone(ZoneOffset.UTC).toLocalDate()
        );
        if (commission.isPresent()) {
            log.info("====================== processTopProfilePartner found commission when recalculating : {} ", commission);
            commissionTemp = commission.get();
            if (!commissionTemp.getAmount().equals(commissionAmount)) {
                double diff = commissionTemp.getAmount() - commissionAmount;
                commissionTemp.setFraudAmount(commissionTemp.getFraudAmount() + diff);
            }
            commissionTemp.setAmount(commissionAmount);
            commissionTemp.setGlobalNetworkCommissionAmount(globalCommissionAmount);
            commissionTemp.setCalculatedAt(Instant.now());
            commissionRepository.save(commissionTemp);
        } else {
            commissionTemp = new Commission();
            commissionTemp.setAmount(commissionAmount);
            commissionTemp.setFraudAmount(0.0);
            commissionTemp.setCalculatedAt(Instant.now());
            commissionTemp.setSenderMsisdn(partner.getMsisdn());
            commissionTemp.setSenderProfile(partnerProfileCode);
            commissionTemp.setCommissionAccount(commissionAccount);
            commissionTemp.setServiceType(plan.getServices().stream().map(ServiceType::getName).collect(Collectors.toList()).toString());
            commissionTemp.setCommissionPaymentStatus(CommissionPaymentStatus.TO_BE_PAID);
            commissionTemp.setConfigurationPlan(configuration);
            Set<TransactionOperation> transactionOperations = new HashSet<TransactionOperation>(transactions);
            commissionTemp.setTransactionOperations(transactionOperations);
            commissionTemp.setSpare3(String.valueOf(setSpare3Value(transactions)));
            commissionTemp.setTransactionsAmount(transactionAmount);
            commissionTemp.setCommissioningPlanType(plan.getCommissioningPlanType());
            commissionTemp.setGlobalNetworkCommissionAmount(globalCommissionAmount);
            commissionTemp.setCalculationDate(timeInterval.getCalculationDate());
            commissionTemp.setCalculationShortDate(timeInterval.getCalculationDate().atZone(ZoneId.of("Africa/Dakar")).toLocalDate());

            calculatedCommissions.add(commissionTemp);

            log.info("====================== processTopProfilePartner Calculated commission : {}", commissionTemp);

            log.info(
                "====================== processTopProfilePartner IDs Calculated commission : {}",
                calculatedCommissions.stream().map(Commission::getId).collect(Collectors.toList())
            );
            commissionRepository.saveAll(calculatedCommissions);
        }
        return networkInformations;
    }

    @Override
    public void calculate(Frequency frequency, Instant calculationDate, List<Partner> partnersForRecalculation) {
        Constants.logInfoForService(
            String.valueOf(DeferredCalculusServiceImpl.class),
            "====================== Calculating commission ",
            "====================== Calculating commission "
        );

        List<CommissioningPlan> plans = commissioningPlanService.getCommissionningPlansByFrequency(frequency);
        plans.forEach(plan -> {
            // Récuperons l'intervale de date de calcul pour le plan
            TimeInterval timeInterval = CalculusUtilities.buildTimeInterval(plan.getCalculusPeriod(), calculationDate, frequency);
            List<String> serviceTypes = plan.getServices().stream().map(ServiceType::getCode).collect(Collectors.toList());
            log.info("====================== CommissioningPlan : {}, ServiceTypes : {}", plan.getName(), serviceTypes);
            List<String> zones = plan
                .getConfigurationPlans()
                .stream()
                .flatMap(configurationPlan -> configurationPlan.getZones().stream().map(Zone::getCode))
                .distinct()
                .collect(Collectors.toList());
            log.info("====================== CommissioningPlan : {}, Zones : {}", plan.getName(), zones);
            List<String> profiles = plan
                .getConfigurationPlans()
                .stream()
                .map(configurationPlan -> configurationPlan.getPartnerProfile().getCode())
                .collect(Collectors.toList());
            log.info("====================== CommissioningPlan : {}, Profiles : {}", plan.getName(), profiles);
            List<TransactionOperation> planTransactions = transactionOperationRepository.findByIsFraudAndOperationDateBetweenAndSenderZoneInAndSubTypeIn(
                false,
                timeInterval.getStartDate(),
                timeInterval.getEndDate(),
                zones,
                serviceTypes
            );
            log.info("====================== CommissioningPlan : {}, transactions number : {}", plan.getName(), planTransactions.size());
            Map<String, List<TransactionOperation>> transactionsByMsisdn = planTransactions
                .stream()
                .collect(groupingBy(TransactionOperation::getAgentMsisdn));
            log.info(
                "====================== CommissioningPlan : {}, partners number : {}",
                plan.getName(),
                transactionsByMsisdn.keySet().size()
            );
            List<ConfigurationPlan> topProfileConfigurations = plan
                .getConfigurationPlans()
                .stream()
                .filter(configurationPlan -> configurationPlan.getPartnerProfile().getCode().equalsIgnoreCase("DP"))
                .collect(Collectors.toList());
            List<ConfigurationPlan> otherConfigurations = plan
                .getConfigurationPlans()
                .stream()
                .filter(configurationPlan -> !configurationPlan.getPartnerProfile().getCode().equalsIgnoreCase("DP"))
                .collect(Collectors.toList());

            List<NetworkInformations> topProfilesNetworkInformations = topProfileConfigurations
                .stream()
                .flatMap(configuration -> {
                    NetworkInformations networkInformations = new NetworkInformations();
                    String code = configuration.getPartnerProfile().getCode();
                    List<String> configZones = configuration.getZones().stream().map(Zone::getCode).collect(Collectors.toList());
                    networkInformations.setZones(configZones);
                    log.info("====================== Configuration Zones : {}", configZones);
                    log.info("====================== Configuration Profile : {}", configuration.getPartnerProfile().getCode());
                    return processConfiguration(configuration, configZones)
                        .stream()
                        .map(partner ->
                            processTopProfilePartner(
                                partner,
                                transactionsByMsisdn,
                                networkInformations,
                                code,
                                timeInterval,
                                configuration,
                                plan
                            )
                        );
                })
                .distinct()
                .collect(Collectors.toList());
            log.info("====================== Processing other profiles : {}", topProfilesNetworkInformations);
            log.info("====================== Processing other profiles for zones : {}", topProfilesNetworkInformations.get(0).getZones());
            commissionRepository.saveAll(
                otherConfigurations
                    .stream()
                    .flatMap(configuration -> {
                        String code = configuration.getPartnerProfile().getCode();
                        List<String> configZones = configuration.getZones().stream().map(Zone::getCode).collect(Collectors.toList());
                        log.info("====================== Configuration Zones : {}", configZones);
                        log.info("====================== Configuration Profile : {}", configuration.getPartnerProfile().getCode());
                        if (partnersForRecalculation.isEmpty()) {
                            return processConfiguration(configuration, configZones)
                                .stream()
                                .flatMap(partner ->
                                    processPartner(
                                        topProfilesNetworkInformations,
                                        transactionsByMsisdn,
                                        partner,
                                        code,
                                        timeInterval,
                                        configuration,
                                        plan
                                    )
                                        .stream()
                                );
                        } else {
                            log.info(
                                "====================== Recalculating for partners : {}",
                                partnersForRecalculation.stream().map(Partner::getMsisdn)
                            );
                            return partnersForRecalculation
                                .stream()
                                .flatMap(partner ->
                                    processPartner(
                                        topProfilesNetworkInformations,
                                        transactionsByMsisdn,
                                        partner,
                                        code,
                                        timeInterval,
                                        configuration,
                                        plan
                                    )
                                        .stream()
                                );
                        }
                    })
                    .collect(Collectors.toList())
            );
        });
    }

    @Override
    public void calculate(Frequency frequency) {
        calculate(frequency, Instant.now(), Collections.emptyList());
    }

    @Override
    public void reCalculation(DailyCalculationInfo dailyCalculationInfo, Long planId) {
        Optional<CommissioningPlan> plan = commissioningPlanRepository.findByIdAndState(planId, CommissioningPlanState.IN_USE);
        if (plan.isPresent()) {
            List<Zone> zonesToCheck = plan
                .get()
                .getConfigurationPlans()
                .stream()
                .flatMap(config -> config.getZones().stream())
                .collect(Collectors.toList());
            Optional<Frequency> frequency = frequencyRepository.findById(dailyCalculationInfo.getFrequencyId());
            if (frequency.isPresent()) {
                dailyCalculationInfo.getDays().sort(Comparator.naturalOrder());
                List<String> partnersMsisdn = transactionOperationRepository
                    .findByIsFraudAndOperationDateBetweenAndSenderZoneInAndSubTypeIn(
                        true,
                        dailyCalculationInfo.getDays().get(0),
                        dailyCalculationInfo.getDays().get(dailyCalculationInfo.getDays().size() - 1),
                        zonesToCheck.stream().map(Zone::getCode).distinct().collect(Collectors.toList()),
                        plan.get().getServices().stream().map(ServiceType::getCode).collect(Collectors.toList())
                    )
                    .stream()
                    .distinct()
                    .map(TransactionOperation::getAgentMsisdn)
                    .collect(Collectors.toList());
                if (partnersMsisdn.isEmpty()) {
                    log.info("Aucune transactions frauduleuses pour ces dates.");
                    // TODO: Remove this test purpose calculate() methode call
                    dailyCalculationInfo.getDays().stream().forEach(date -> calculate(frequency.get(), date, Collections.emptyList()));
                } else {
                    List<Partner> partners = partnerRepository.findByMsisdnIn(partnersMsisdn);
                    dailyCalculationInfo.getDays().stream().forEach(date -> calculate(frequency.get(), date, partners));
                }
            } else {
                log.info("La fréquence {} n'existe pas.", dailyCalculationInfo.getFrequencyId());
            }
        } else {
            log.info("Le plan {} n'existe pas ou n'est plus en vigueur.", planId);
        }
    }

    @Override
    public void checkAndRelaunchCalculation(Long frequencyId) {
        log.info("===================== Searching for tagged transactions ");
        List<TransactionOperationDTO> previousDayTaggedTransactions = transactionOperationService.getPreviousDayTaggedTransactions();
        List<Instant> datesToRecalculate = new ArrayList<>();
        if (!previousDayTaggedTransactions.isEmpty()) {
            previousDayTaggedTransactions.forEach(transactionOperation -> {
                Instant date = transactionOperation.getOperationDate().truncatedTo(ChronoUnit.DAYS);
                if (!datesToRecalculate.contains(date)) {
                    datesToRecalculate.add(date);
                }
            });
            DailyCalculationInfo calculationInfo = new DailyCalculationInfo();
            calculationInfo.setFrequencyId(frequencyId);
            calculationInfo.getDays().addAll(datesToRecalculate);
            log.info("===================== Relaunch calculation for {}", calculationInfo.getDays());
            List<CommissioningPlan> plans = commissioningPlanRepository.findByCalculusFrequency_Id(frequencyId);
            plans.stream().forEach(plan -> reCalculation(calculationInfo, plan.getId()));
        } else {
            log.info("===================== No transaction tagged in the last 24 hours ");
        }
    }
}
