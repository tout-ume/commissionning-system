package sn.free.commissioning.calculus_engine.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.time.Instant;
// import org.json.JSONObject;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.validation.constraints.*;
import lombok.Getter;
import lombok.Setter;
import org.json.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sn.free.commissioning.domain.Commission;
import sn.free.commissioning.domain.enumeration.CommissionPaymentStatus;
import sn.free.commissioning.domain.enumeration.CommissioningPlanType;

/**
 * A DTO for the {@link sn.free.commissioning.domain.Commission} entity.
 */

// TODO : On a juste modifier enddate à endate ... mais la bonne synthaxe est avec deux d. Portail doivent modifier leur signature après MVP
public class PartnerCommissionsDTO implements Serializable {

    private final Logger log = LoggerFactory.getLogger(PartnerCommissionsDTO.class);

    private String startdate;

    private String endate;

    private MfsCommissionDTO freemoney;

    private MobileCommissionDTO mobile;

    public MobileCommissionDTO getMobile() {
        return mobile;
    }

    public void setMobile(MobileCommissionDTO mobile) {
        this.mobile = mobile;
    }

    public MfsCommissionDTO getFreemoney() {
        return freemoney;
    }

    public void setFreemoney(MfsCommissionDTO freemoney) {
        this.freemoney = freemoney;
    }

    public String getStartdate() {
        return this.startdate;
    }

    public void setStartdate(String startStr) {
        this.startdate = startStr;
    }

    public String getEndate() {
        return endate;
    }

    public void setEndate(String endStr) {
        this.endate = endStr;
    }

    public Map<String, Double> getServiceCommisions(String spare3) {
        JSONArray jsonArray = new JSONArray(spare3);
        Map<String, Double> serviceCommissions = new HashMap<String, Double>();
        for (int i = 0; i < jsonArray.length(); i++) {
            String serviceName = jsonArray.getJSONObject(i).getString("name");
            Double amount = jsonArray.getJSONObject(i).getDouble("amount");
            serviceCommissions.put(serviceName, amount);
        }

        return serviceCommissions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PartnerCommissionsDTO)) {
            return false;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(startdate, endate, freemoney, mobile);
    }

    private Double sumServiceMap(Map<String, Double> a, Map<String, Double> b, String key) {
        Double value = b.containsKey(key) ? b.get(key) : 0.0;
        return a.get(key) + value;
    }

    public void setMfsDTOs(Entry<String, List<Commission>> commissions) {
        Supplier<Stream<Commission>> mfsCommissions = () ->
            commissions
                .getValue()
                .stream()
                .filter(commission -> {
                    return commission.getCommissioningPlanType().name().equals("MFS");
                });

        Map<String, Double> initialServices = new HashMap<String, Double>();
        initialServices.put("BILL PAYMENT", 0.0);
        initialServices.put("CASHIN", 0.0);
        initialServices.put("CASH OUT", 0.0);
        Double totalCom = mfsCommissions.get().map(Commission::getAmount).reduce(0.0, Double::sum);

        Map<String, Double> totalBySerive = mfsCommissions
            .get()
            .map(c -> {
                Map<String, Double> services = this.getServiceCommisions(c.getSpare3());
                return services;
            })
            .reduce(
                initialServices,
                (aggr, c) -> {
                    aggr.put("BILL PAYMENT", sumServiceMap(aggr, c, "BILL PAYMENT"));
                    aggr.put("CASHIN", sumServiceMap(aggr, c, "CASHIN"));
                    aggr.put("CASH OUT", sumServiceMap(aggr, c, "CASH OUT"));
                    return aggr;
                }
            );
        MfsCommissionDTO mfs = new MfsCommissionDTO();
        Double totalVolume = totalBySerive.get("BILL PAYMENT") + totalBySerive.get("CASHIN") + totalBySerive.get("CASH OUT");
        mfs.setBillpayment(totalBySerive.get("BILL PAYMENT"));
        mfs.setCashin(totalBySerive.get("CASHIN"));
        mfs.setCashout(totalBySerive.get("CASH OUT"));
        mfs.setTotalcom(totalCom);
        mfs.setTotalvolume(totalVolume);
        setFreemoney(mfs);
    }

    public void setMobileDTOs(Entry<String, List<Commission>> commissions) {
        Supplier<Stream<Commission>> mfsCommissions = () ->
            commissions.getValue().stream().filter(commission -> commission.getCommissioningPlanType().name().equals("Mobile"));

        Map<String, Double> initialServices = new HashMap<String, Double>();
        initialServices.put("AIRTIME", 0.0);
        initialServices.put("BUNDLE", 0.0);
        Double totalCom = mfsCommissions.get().map(Commission::getAmount).reduce(0.0, Double::sum);
        Map<String, Double> totalBySerive = mfsCommissions
            .get()
            .map(c -> {
                return this.getServiceCommisions(c.getSpare3());
            })
            .reduce(
                initialServices,
                (aggr, c) -> {
                    aggr.put("AIRTIME", sumServiceMap(aggr, c, "AIRTIME"));
                    aggr.put("BUNDLE", sumServiceMap(aggr, c, "BUNDLE"));
                    return aggr;
                }
            );
        MobileCommissionDTO gtm = new MobileCommissionDTO();
        Double totalVolume = totalBySerive.get("AIRTIME") + totalBySerive.get("BUNDLE");
        gtm.setAirtime(totalBySerive.get("AIRTIME"));
        gtm.setBundle(totalBySerive.get("BUNDLE"));
        gtm.setTotalcom(totalCom);
        gtm.setTotalvolume(totalVolume);
        setMobile(gtm);
    }

    @Override
    public String toString() {
        return (
            "{" +
            ", startDate='" +
            getStartdate() +
            "'" +
            ", endDate='" +
            getEndate() +
            "'" +
            ", freemoney='" +
            getFreemoney() +
            "'" +
            ", mobile='" +
            getMobile() +
            "'" +
            "}"
        );
    }
}
