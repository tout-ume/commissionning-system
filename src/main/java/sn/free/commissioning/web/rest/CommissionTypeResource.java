package sn.free.commissioning.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sn.free.commissioning.repository.CommissionTypeRepository;
import sn.free.commissioning.service.CommissionTypeQueryService;
import sn.free.commissioning.service.CommissionTypeService;
import sn.free.commissioning.service.criteria.CommissionTypeCriteria;
import sn.free.commissioning.service.dto.CommissionTypeDTO;
import sn.free.commissioning.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link sn.free.commissioning.domain.CommissionType}.
 */
@RestController
@RequestMapping("/api")
public class CommissionTypeResource {

    private final Logger log = LoggerFactory.getLogger(CommissionTypeResource.class);

    private static final String ENTITY_NAME = "commissionType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CommissionTypeService commissionTypeService;

    private final CommissionTypeRepository commissionTypeRepository;

    private final CommissionTypeQueryService commissionTypeQueryService;

    public CommissionTypeResource(
        CommissionTypeService commissionTypeService,
        CommissionTypeRepository commissionTypeRepository,
        CommissionTypeQueryService commissionTypeQueryService
    ) {
        this.commissionTypeService = commissionTypeService;
        this.commissionTypeRepository = commissionTypeRepository;
        this.commissionTypeQueryService = commissionTypeQueryService;
    }

    /**
     * {@code POST  /commission-types} : Create a new commissionType.
     *
     * @param commissionTypeDTO the commissionTypeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new commissionTypeDTO, or with status {@code 400 (Bad Request)} if the commissionType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/commission-types")
    public ResponseEntity<CommissionTypeDTO> createCommissionType(@RequestBody CommissionTypeDTO commissionTypeDTO)
        throws URISyntaxException {
        log.info("REST request to save CommissionType : {}", commissionTypeDTO);
        if (commissionTypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new commissionType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CommissionTypeDTO result = commissionTypeService.save(commissionTypeDTO);
        return ResponseEntity
            .created(new URI("/api/commission-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /commission-types/:id} : Updates an existing commissionType.
     *
     * @param id the id of the commissionTypeDTO to save.
     * @param commissionTypeDTO the commissionTypeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated commissionTypeDTO,
     * or with status {@code 400 (Bad Request)} if the commissionTypeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the commissionTypeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/commission-types/{id}")
    public ResponseEntity<CommissionTypeDTO> updateCommissionType(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CommissionTypeDTO commissionTypeDTO
    ) throws URISyntaxException {
        log.info("REST request to update CommissionType : {}, {}", id, commissionTypeDTO);
        if (commissionTypeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, commissionTypeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!commissionTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CommissionTypeDTO result = commissionTypeService.update(commissionTypeDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, commissionTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /commission-types/:id} : Partial updates given fields of an existing commissionType, field will ignore if it is null
     *
     * @param id the id of the commissionTypeDTO to save.
     * @param commissionTypeDTO the commissionTypeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated commissionTypeDTO,
     * or with status {@code 400 (Bad Request)} if the commissionTypeDTO is not valid,
     * or with status {@code 404 (Not Found)} if the commissionTypeDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the commissionTypeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/commission-types/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CommissionTypeDTO> partialUpdateCommissionType(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CommissionTypeDTO commissionTypeDTO
    ) throws URISyntaxException {
        log.info("REST request to partial update CommissionType partially : {}, {}", id, commissionTypeDTO);
        if (commissionTypeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, commissionTypeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!commissionTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CommissionTypeDTO> result = commissionTypeService.partialUpdate(commissionTypeDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, commissionTypeDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /commission-types} : get all the commissionTypes.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of commissionTypes in body.
     */
    @GetMapping("/commission-types")
    public ResponseEntity<List<CommissionTypeDTO>> getAllCommissionTypes(
        CommissionTypeCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.info("REST request to get CommissionTypes by criteria: {}", criteria);
        Page<CommissionTypeDTO> page = commissionTypeQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /commission-types/count} : count all the commissionTypes.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/commission-types/count")
    public ResponseEntity<Long> countCommissionTypes(CommissionTypeCriteria criteria) {
        log.info("REST request to count CommissionTypes by criteria: {}", criteria);
        return ResponseEntity.ok().body(commissionTypeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /commission-types/:id} : get the "id" commissionType.
     *
     * @param id the id of the commissionTypeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the commissionTypeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/commission-types/{id}")
    public ResponseEntity<CommissionTypeDTO> getCommissionType(@PathVariable Long id) {
        log.info("REST request to get CommissionType : {}", id);
        Optional<CommissionTypeDTO> commissionTypeDTO = commissionTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(commissionTypeDTO);
    }

    /**
     * {@code DELETE  /commission-types/:id} : delete the "id" commissionType.
     *
     * @param id the id of the commissionTypeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/commission-types/{id}")
    public ResponseEntity<Void> deleteCommissionType(@PathVariable Long id) {
        log.info("REST request to delete CommissionType : {}", id);
        commissionTypeService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
