package sn.free.commissioning.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.jobrunr.scheduling.BackgroundJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sn.free.commissioning.config.Constants;
import sn.free.commissioning.domain.ComPlansByPartnerResponse;
import sn.free.commissioning.domain.enumeration.UploadStatus;
import sn.free.commissioning.domain.exceptions.NoCommissionPlanException;
import sn.free.commissioning.domain.exceptions.PartnerNotFoundException;
import sn.free.commissioning.repository.CommissioningPlanRepository;
import sn.free.commissioning.service.CommissioningPlanQueryService;
import sn.free.commissioning.service.CommissioningPlanService;
import sn.free.commissioning.service.criteria.CommissioningPlanCriteria;
import sn.free.commissioning.service.dto.CommissioningPlanDTO;
import sn.free.commissioning.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link sn.free.commissioning.domain.CommissioningPlan}.
 */
@RestController
@RequestMapping("/api")
public class CommissioningPlanResource {

    private final Logger log = LoggerFactory.getLogger(CommissioningPlanResource.class);

    private static final String ENTITY_NAME = "commissioningPlan";
    private static final String CLASS_NAME = String.valueOf(CommissioningPlanResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CommissioningPlanService commissioningPlanService;

    private final CommissioningPlanRepository commissioningPlanRepository;

    private final CommissioningPlanQueryService commissioningPlanQueryService;

    public CommissioningPlanResource(
        CommissioningPlanService commissioningPlanService,
        CommissioningPlanRepository commissioningPlanRepository,
        CommissioningPlanQueryService commissioningPlanQueryService
    ) {
        this.commissioningPlanService = commissioningPlanService;
        this.commissioningPlanRepository = commissioningPlanRepository;
        this.commissioningPlanQueryService = commissioningPlanQueryService;
    }

    /**
     * {@code GET  /commissioning-plans/to-pay-today} : get commissioningPlans to pay today.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of commissioningPlans in body.
     */
    @GetMapping("/commissioning-plans/to-pay-today")
    public ResponseEntity<List<CommissioningPlanDTO>> getCommissioningPlansToPayToday(HttpServletRequest request) {
        log.info("REST request to get CommissioningPlans to pay today");
        List<CommissioningPlanDTO> plans = commissioningPlanService.getCommissioninPlansToPayToday();
        Constants.logInfo(
            true,
            CLASS_NAME,
            "Liste des plans de commissions à payer aujourd'hui",
            "Liste des plans de commissions à payer aujourd'hui, total : " + plans.size(),
            request
        );
        return ResponseEntity.ok().body(plans);
    }

    /**
     * {@code POST  /commissioning-plans} : Create a new commissioningPlan.
     *
     * @param commissioningPlanDTO the commissioningPlanDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new commissioningPlanDTO, or with status {@code 400 (Bad Request)} if the commissioningPlan has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/commissioning-plans")
    public ResponseEntity<CommissioningPlanDTO> createCommissioningPlan(
        @Valid @RequestBody CommissioningPlanDTO commissioningPlanDTO,
        HttpServletRequest request
    ) throws URISyntaxException {
        log.info("REST request to save CommissioningPlan : {}", commissioningPlanDTO);

        if (commissioningPlanDTO.getId() != null) {
            Constants.logInfo(
                true,
                CLASS_NAME,
                " A new commissioningPlan cannot already have an ID.",
                " A new commissioningPlan cannot already have an ID.",
                request
            );
            throw new BadRequestAlertException("A new commissioningPlan cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CommissioningPlanDTO result = commissioningPlanService.save(commissioningPlanDTO);
        Constants.logInfo(
            result != null,
            CLASS_NAME,
            "REST request to save CommissioningPlan .",
            result != null ? " CommissioningPlan crée avec succes avec comme nom: " + result.getName() : "Échec",
            request
        );
        return ResponseEntity
            .created(new URI("/api/commissioning-plans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /commissioning-plans/:id} : Updates an existing commissioningPlan.
     *
     * @param id the id of the commissioningPlanDTO to save.
     * @param commissioningPlanDTO the commissioningPlanDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated commissioningPlanDTO,
     * or with status {@code 400 (Bad Request)} if the commissioningPlanDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the commissioningPlanDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/commissioning-plans/{id}")
    public ResponseEntity<CommissioningPlanDTO> updateCommissioningPlan(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody CommissioningPlanDTO commissioningPlanDTO,
        HttpServletRequest request
    ) throws URISyntaxException {
        log.info("REST request to update CommissioningPlan : {}, {}", id, commissioningPlanDTO);
        Constants.logInfo(
            true,
            CLASS_NAME,
            " REST request to update CommissioningPlan.",
            " REST request to update CommissioningPlan.",
            request
        );

        if (commissioningPlanDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, commissioningPlanDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!commissioningPlanRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CommissioningPlanDTO result = commissioningPlanService.update(commissioningPlanDTO);
        Constants.logInfo(
            result != null,
            CLASS_NAME,
            "REST request to update a CommissioningPlan .",
            result != null ? " CommissioningPlan mise a jour avec succes avec comme nom: " + result.getName() : "Échec",
            request
        );
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, commissioningPlanDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /commissioning-plans/stop/:id} : Updates an existing commissioningPlan.
     *
     * @param id the id of the commissioningPlanDTO to save.
     * @param commissioningPlanDTO the commissioningPlanDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated commissioningPlanDTO,
     * or with status {@code 400 (Bad Request)} if the commissioningPlanDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the commissioningPlanDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/commissioning-plans/stop/{id}")
    public ResponseEntity<CommissioningPlanDTO> stopCommissioningPlan(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CommissioningPlanDTO commissioningPlanDTO,
        HttpServletRequest request
    ) throws URISyntaxException {
        if (commissioningPlanDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, commissioningPlanDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!commissioningPlanRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CommissioningPlanDTO result = commissioningPlanService.update(commissioningPlanDTO);

        Constants.logInfo(
            result != null,
            CLASS_NAME,
            "REST request to stop a CommissioningPlan .",
            result != null ? " CommissioningPlan stop avec succes avec comme nom: " + result.getName() : "Échec",
            request
        );

        Instant endDate = commissioningPlanDTO.getEndDate();
        BackgroundJob.schedule(
            endDate,
            () -> {
                commissioningPlanService.updateState(commissioningPlanDTO);
            }
        );

        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /commissioning-plans/:id} : Partial updates given fields of an existing commissioningPlan, field will ignore if it is null
     *
     * @param id the id of the commissioningPlanDTO to save.
     * @param commissioningPlanDTO the commissioningPlanDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated commissioningPlanDTO,
     * or with status {@code 400 (Bad Request)} if the commissioningPlanDTO is not valid,
     * or with status {@code 404 (Not Found)} if the commissioningPlanDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the commissioningPlanDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/commissioning-plans/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CommissioningPlanDTO> partialUpdateCommissioningPlan(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody CommissioningPlanDTO commissioningPlanDTO
    ) throws URISyntaxException {
        log.info("REST request to partial update CommissioningPlan partially : {}, {}", id, commissioningPlanDTO);
        if (commissioningPlanDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, commissioningPlanDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!commissioningPlanRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CommissioningPlanDTO> result = commissioningPlanService.partialUpdate(commissioningPlanDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, commissioningPlanDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /commissioning-plans} : get all the commissioningPlans.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of commissioningPlans in body.
     */
    @GetMapping("/commissioning-plans")
    public ResponseEntity<List<CommissioningPlanDTO>> getAllCommissioningPlans(
        CommissioningPlanCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        HttpServletRequest request
    ) {
        log.info("REST request to get CommissioningPlans by criteria: {}", criteria);
        Page<CommissioningPlanDTO> page = commissioningPlanQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        Constants.logInfo(
            true,
            CLASS_NAME,
            "Liste des plans de commissions",
            "Liste des plans de commissions, total : " +
            page.getTotalElements() +
            " - taille de la page : " +
            page.getSize() +
            " - numéro de la page : " +
            page.getNumber(),
            request
        );
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /commissioning-plans/count} : count all the commissioningPlans.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/commissioning-plans/count")
    public ResponseEntity<Long> countCommissioningPlans(CommissioningPlanCriteria criteria) {
        log.info("REST request to count CommissioningPlans by criteria: {}", criteria);
        return ResponseEntity.ok().body(commissioningPlanQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /commissioning-plans/:id} : get the "id" commissioningPlan.
     *
     * @param id the id of the commissioningPlanDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the commissioningPlanDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/commissioning-plans/{id}")
    public ResponseEntity<CommissioningPlanDTO> getCommissioningPlan(@PathVariable Long id, HttpServletRequest request) {
        log.info("REST request to get CommissioningPlan : {}", id);
        Constants.logInfo(true, CLASS_NAME, " REST request to get CommissioningPlan.", " REST request to get CommissioningPlan.", request);

        Optional<CommissioningPlanDTO> commissioningPlanDTO = commissioningPlanService.findOne(id);
        return ResponseUtil.wrapOrNotFound(commissioningPlanDTO);
    }

    /**
     * {@code DELETE  /commissioning-plans/:id} : delete the "id" commissioningPlan.
     *
     * @param id the id of the commissioningPlanDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/commissioning-plans/{id}")
    public ResponseEntity<Void> deleteCommissioningPlan(@PathVariable Long id) {
        log.info("REST request to delete CommissioningPlan : {}", id);
        commissioningPlanService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code GET  /commissioning-plans/by-partner?msisdn} : get the commissioningPlans by the "msisdn".
     *
     * @param msisdn the msisdn of the Partner.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the commissioningPlanDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/commissioning-plans/by-partner")
    public ResponseEntity<ComPlansByPartnerResponse> getCommissionningPlansByPartnerProfile(
        @RequestParam String msisdn,
        HttpServletRequest request
    ) {
        log.info("REST request to get CommissioningPlan by this msisdn : {} ", msisdn);
        Constants.logInfo(
            true,
            CLASS_NAME,
            "REST request to get CommissioningPlan by this partner: " + msisdn,
            " REST request to get CommissioningPlan by this partner: " + msisdn,
            request
        );

        ComPlansByPartnerResponse response = new ComPlansByPartnerResponse();
        List<CommissioningPlanDTO> result = null;
        try {
            result = commissioningPlanService.getCommissionningPlansByPartnerProfile(msisdn);
        } catch (PartnerNotFoundException e) {
            log.info("Pas de partner ayant ce msisdn");
            Constants.logInfo(true, CLASS_NAME, "Pas de partner ayant ce msisdn", " Pas de partner ayant ce msisdn.", request);

            response.setStatus(UploadStatus.FAILURE);
            response.setMessage("Pas de partner ayant ce msisdn");
            return ResponseEntity.badRequest().body(response);
        } catch (NoCommissionPlanException e) {
            log.info("Pas de plan de commission pour ce partenaire");
            Constants.logInfo(
                true,
                CLASS_NAME,
                "Pas de plan de commission pour ce partenaire",
                " Pas de plan de commission pour ce partenaire",
                request
            );

            response.setStatus(UploadStatus.FAILURE);
            response.setMessage("Pas de plan de commission pour ce partenaire");
            return ResponseEntity.badRequest().body(response);
        }
        log.info("Succès");
        Constants.logInfo(true, CLASS_NAME, "Succès", " Succès", request);

        response.setStatus(UploadStatus.SUCCESS);
        response.setMessage("Succès");
        response.setContent(result);
        return ResponseEntity.ok().body(response);
    }

    /**
     * {@code GET  /commissioning-plans/blocked-for/{msisdn}} : get the commissioningPlans blocked for a partner.
     *
     * @param msisdn the msisdn of the Partner.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the commissioningPlanDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/commissioning-plans/blocked-for/{msisdn}")
    public ResponseEntity<List<CommissioningPlanDTO>> getCommissionningPlansByPartnerAndBlockedIsTrue(
        @PathVariable String msisdn,
        HttpServletRequest request
    ) {
        log.info("REST request to get CommissioningPlan by this msisdn : {} ", msisdn);
        Constants.logInfo(
            true,
            CLASS_NAME,
            "REST request to get CommissioningPlan blocked by this partner : " + msisdn,
            "REST request to get CommissioningPlan blocked by this partner : " + msisdn,
            request
        );

        List<CommissioningPlanDTO> result = commissioningPlanService.getCommissioningPlansAndBlockedIsTrue(msisdn);
        return ResponseEntity.ok().body(result);
    }
}
