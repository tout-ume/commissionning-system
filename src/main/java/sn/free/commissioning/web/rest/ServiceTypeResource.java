package sn.free.commissioning.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sn.free.commissioning.config.Constants;
import sn.free.commissioning.repository.ServiceTypeRepository;
import sn.free.commissioning.service.ServiceTypeQueryService;
import sn.free.commissioning.service.ServiceTypeService;
import sn.free.commissioning.service.criteria.ServiceTypeCriteria;
import sn.free.commissioning.service.dto.ServiceTypeDTO;
import sn.free.commissioning.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link sn.free.commissioning.domain.ServiceType}.
 */
@RestController
@RequestMapping("/api")
public class ServiceTypeResource {

    private final Logger log = LoggerFactory.getLogger(ServiceTypeResource.class);

    private static final String ENTITY_NAME = "serviceType";
    private static final String CLASS_NAME = String.valueOf(ServiceTypeResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ServiceTypeService serviceTypeService;

    private final ServiceTypeRepository serviceTypeRepository;

    private final ServiceTypeQueryService serviceTypeQueryService;

    public ServiceTypeResource(
        ServiceTypeService serviceTypeService,
        ServiceTypeRepository serviceTypeRepository,
        ServiceTypeQueryService serviceTypeQueryService
    ) {
        this.serviceTypeService = serviceTypeService;
        this.serviceTypeRepository = serviceTypeRepository;
        this.serviceTypeQueryService = serviceTypeQueryService;
    }

    /**
     * {@code POST  /service-types} : Create a new serviceType.
     *
     * @param serviceTypeDTO the serviceTypeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new serviceTypeDTO, or with status {@code 400 (Bad Request)} if the serviceType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/service-types")
    public ResponseEntity<ServiceTypeDTO> createServiceType(@Valid @RequestBody ServiceTypeDTO serviceTypeDTO, HttpServletRequest request)
        throws URISyntaxException {
        log.debug("REST request to save ServiceType : {}", serviceTypeDTO);
        if (serviceTypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new serviceType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ServiceTypeDTO result = serviceTypeService.save(serviceTypeDTO);
        Constants.logInfo(true, CLASS_NAME, "REST request to save ServiceType ", "REST request to save ServiceType ", request);

        return ResponseEntity
            .created(new URI("/api/service-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /service-types/:id} : Updates an existing serviceType.
     *
     * @param id the id of the serviceTypeDTO to save.
     * @param serviceTypeDTO the serviceTypeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated serviceTypeDTO,
     * or with status {@code 400 (Bad Request)} if the serviceTypeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the serviceTypeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/service-types/{id}")
    public ResponseEntity<ServiceTypeDTO> updateServiceType(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ServiceTypeDTO serviceTypeDTO,
        HttpServletRequest request
    ) throws URISyntaxException {
        log.debug("REST request to update ServiceType : {}, {}", id, serviceTypeDTO);
        if (serviceTypeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, serviceTypeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!serviceTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ServiceTypeDTO result = serviceTypeService.update(serviceTypeDTO);
        Constants.logInfo(
            result != null,
            CLASS_NAME,
            "REST request to update ServiceType.",
            result != null ? " REST request to update ServiceType avec succes : " + result.getId() : "Échec",
            request
        );
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, serviceTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /service-types/:id} : Partial updates given fields of an existing serviceType, field will ignore if it is null
     *
     * @param id the id of the serviceTypeDTO to save.
     * @param serviceTypeDTO the serviceTypeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated serviceTypeDTO,
     * or with status {@code 400 (Bad Request)} if the serviceTypeDTO is not valid,
     * or with status {@code 404 (Not Found)} if the serviceTypeDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the serviceTypeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/service-types/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ServiceTypeDTO> partialUpdateServiceType(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ServiceTypeDTO serviceTypeDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ServiceType partially : {}, {}", id, serviceTypeDTO);
        if (serviceTypeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, serviceTypeDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!serviceTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ServiceTypeDTO> result = serviceTypeService.partialUpdate(serviceTypeDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, serviceTypeDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /service-types} : get all the serviceTypes.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of serviceTypes in body.
     */
    @GetMapping("/service-types")
    public ResponseEntity<List<ServiceTypeDTO>> getAllServiceTypes(
        ServiceTypeCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        HttpServletRequest request
    ) {
        log.debug("REST request to get ServiceTypes by criteria: {}", criteria);
        Page<ServiceTypeDTO> page = serviceTypeQueryService.findByCriteria(criteria, pageable);
        Constants.logInfo(
            true,
            CLASS_NAME,
            "REST request to get ServiceTypes",
            "REST request to get ServiceTypes, total : " +
            page.getTotalElements() +
            " - taille de la page : " +
            page.getSize() +
            " - numéro de la page : " +
            page.getNumber(),
            request
        );
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /service-types/count} : count all the serviceTypes.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/service-types/count")
    public ResponseEntity<Long> countServiceTypes(ServiceTypeCriteria criteria) {
        log.debug("REST request to count ServiceTypes by criteria: {}", criteria);
        return ResponseEntity.ok().body(serviceTypeQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /service-types/:id} : get the "id" serviceType.
     *
     * @param id the id of the serviceTypeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the serviceTypeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/service-types/{id}")
    public ResponseEntity<ServiceTypeDTO> getServiceType(@PathVariable Long id, HttpServletRequest request) {
        log.debug("REST request to get ServiceType : {}", id);
        Optional<ServiceTypeDTO> serviceTypeDTO = serviceTypeService.findOne(id);
        Constants.logInfo(
            serviceTypeDTO.isPresent(),
            CLASS_NAME,
            " REST request to get a ServiceType ",
            serviceTypeDTO.map(dto -> "REST request to get a ServiceType :  : " + dto.getName()).orElse("ÉCHEC"),
            request
        );
        return ResponseUtil.wrapOrNotFound(serviceTypeDTO);
    }

    /**
     * {@code DELETE  /service-types/:id} : delete the "id" serviceType.
     *
     * @param id the id of the serviceTypeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/service-types/{id}")
    public ResponseEntity<Void> deleteServiceType(@PathVariable Long id, HttpServletRequest request) {
        log.debug("REST request to delete ServiceType : {}", id);
        Constants.logInfo(
            true,
            CLASS_NAME,
            " REST request to delete ServiceType ",
            "REST request to delete ServiceType  by id" + id,
            request
        );
        serviceTypeService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
