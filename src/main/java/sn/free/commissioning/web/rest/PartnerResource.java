package sn.free.commissioning.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sn.free.commissioning.calculus_engine.service.impl.PartnerFromTree;
import sn.free.commissioning.config.Constants;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.repository.PartnerRepository;
import sn.free.commissioning.service.PartnerQueryService;
import sn.free.commissioning.service.PartnerService;
import sn.free.commissioning.service.criteria.PartnerCriteria;
import sn.free.commissioning.service.dto.PartnerDTO;
import sn.free.commissioning.service.dto.PartnerForPaymentDTO;
import sn.free.commissioning.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link sn.free.commissioning.domain.Partner}.
 */
@RestController
@RequestMapping("/api")
public class PartnerResource {

    private final Logger log = LoggerFactory.getLogger(PartnerResource.class);

    private static final String ENTITY_NAME = "partner";
    private static final String CLASS_NAME = String.valueOf(PartnerResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PartnerService partnerService;

    private final PartnerRepository partnerRepository;

    private final PartnerQueryService partnerQueryService;

    private final PartnerFromTree partnerFromFree;

    public PartnerResource(
        PartnerService partnerService,
        PartnerRepository partnerRepository,
        PartnerFromTree partnerFromFree,
        PartnerQueryService partnerQueryService
    ) {
        this.partnerService = partnerService;
        this.partnerRepository = partnerRepository;
        this.partnerQueryService = partnerQueryService;
        this.partnerFromFree = partnerFromFree;
    }

    /**
     * {@code POST  /partners} : Create a new partner.
     *
     * @param partnerDTO the partnerDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new partnerDTO, or with status {@code 400 (Bad Request)} if the partner has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/partners")
    public ResponseEntity<PartnerDTO> createPartner(@Valid @RequestBody PartnerDTO partnerDTO, HttpServletRequest request)
        throws URISyntaxException {
        log.debug("REST request to save Partner : {}", partnerDTO);
        if (partnerDTO.getId() != null) {
            throw new BadRequestAlertException("A new partner cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PartnerDTO result = partnerService.save(partnerDTO);
        Constants.logInfo(
            result != null,
            CLASS_NAME,
            "REST request to save Partner.",
            result != null ? " REST request to save Partner avec succes : " + result.getId() : "Échec",
            request
        );
        return ResponseEntity
            .created(new URI("/api/partners/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /partners/:id} : Updates an existing partner.
     *
     * @param id the id of the partnerDTO to save.
     * @param partnerDTO the partnerDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated partnerDTO,
     * or with status {@code 400 (Bad Request)} if the partnerDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the partnerDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/partners/{id}")
    public ResponseEntity<PartnerDTO> updatePartner(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody PartnerDTO partnerDTO,
        HttpServletRequest request
    ) throws URISyntaxException {
        log.debug("REST request to update Partner : {}, {}", id, partnerDTO);
        if (partnerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, partnerDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!partnerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PartnerDTO result = partnerService.update(partnerDTO);

        Constants.logInfo(
            result != null,
            CLASS_NAME,
            "REST request to update Partner.",
            result != null ? " REST request to update Partner avec succes : " + result.getId() : "Échec",
            request
        );
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, partnerDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /partners/:id} : Partial updates given fields of an existing partner, field will ignore if it is null
     *
     * @param id the id of the partnerDTO to save.
     * @param partnerDTO the partnerDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated partnerDTO,
     * or with status {@code 400 (Bad Request)} if the partnerDTO is not valid,
     * or with status {@code 404 (Not Found)} if the partnerDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the partnerDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/partners/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<PartnerDTO> partialUpdatePartner(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody PartnerDTO partnerDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Partner partially : {}, {}", id, partnerDTO);
        if (partnerDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, partnerDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!partnerRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PartnerDTO> result = partnerService.partialUpdate(partnerDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, partnerDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /partners} : get all the partners.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of partners in body.
     */
    @GetMapping("/partners")
    public ResponseEntity<List<PartnerDTO>> getAllPartners(
        PartnerCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        HttpServletRequest request
    ) {
        Partner partner = this.partnerRepository.findByMsisdn("+221761001000");
        log.debug("REST childrens: {}", partner.getChildren());

        List<Partner> partners = this.partnerFromFree.from(partner).get();

        log.debug("GET CHILDREN: []", partners);
        System.out.println("===========================================");
        System.out.println("===========================================");
        System.out.println("===========================================");
        System.out.println(partners);
        log.debug("REST request to get Partners by criteria: {}", criteria);
        Page<PartnerDTO> page = partnerQueryService.findByCriteria(criteria, pageable);
        Constants.logInfo(
            true,
            CLASS_NAME,
            "Liste des partners",
            "Liste des partners, total : " +
            page.getTotalElements() +
            " - taille de la page : " +
            page.getSize() +
            " - numéro de la page : " +
            page.getNumber(),
            request
        );
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /partners/getDPNetwork/{msisdn}} : get the DP network (dg || rev) with their unpaid commissions.
     *
     * @param msisdn the DP whom network to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of partners, each with his unpaid commissions (PartnerCommissions), in body.
     */
    @GetMapping("/partners/get-dp-network/{msisdn}")
    public ResponseEntity<List<PartnerForPaymentDTO>> getDPNetwork(
        @PathVariable String msisdn,
        @RequestParam(value = "codeProfile", required = false) Optional<String> codeProfile,
        HttpServletRequest request
    ) {
        Constants.logInfo(
            true,
            CLASS_NAME,
            " Get the DP network (dg, rev) with their unpaid commissions.",
            " Get the DP network (dg, rev) with their unpaid commissions.  ",
            request
        );
        List<PartnerForPaymentDTO> nodes = partnerService.getDPNetwork(msisdn);
        if (!codeProfile.isPresent()) return ResponseEntity.ok().body(nodes);
        List<PartnerForPaymentDTO> partnerCommissions = partnerService.getDPNetworkFilter(nodes, codeProfile.get());
        return ResponseEntity.ok().body(partnerCommissions);
    }

    /**
     * {@code GET  /partners/count} : count all the partners.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/partners/count")
    public ResponseEntity<Long> countPartners(PartnerCriteria criteria) {
        log.debug("REST request to count Partners by criteria: {}", criteria);
        return ResponseEntity.ok().body(partnerQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /partners/:id} : get the "id" partner.
     *
     * @param id the id of the partnerDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the partnerDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/partners/{id}")
    public ResponseEntity<PartnerDTO> getPartner(@PathVariable Long id, HttpServletRequest request) {
        log.debug("REST request to get Partner : {}", id);

        Optional<PartnerDTO> partnerDTO = partnerService.findOne(id);
        Constants.logInfo(
            partnerDTO.isPresent(),
            CLASS_NAME,
            " REST request to get a Partner ",
            partnerDTO.map(dto -> "Partenaire trouvé : " + dto.getName()).orElse("ÉCHEC"),
            request
        );
        return ResponseUtil.wrapOrNotFound(partnerDTO);
    }

    /**
     * {@code DELETE  /partners/:id} : delete the "id" partner.
     *
     * @param id the id of the partnerDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/partners/{id}")
    public ResponseEntity<Void> deletePartner(@PathVariable Long id, HttpServletRequest request) {
        log.debug("REST request to delete Partner : {}", id);
        Constants.logInfo(true, CLASS_NAME, " REST request to get a Partner ", "REST request to delete Partner by id" + id, request);
        partnerService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
