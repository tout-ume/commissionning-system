package sn.free.commissioning.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sn.free.commissioning.calculus_engine.service.CalculusService;
import sn.free.commissioning.config.Constants;
import sn.free.commissioning.domain.ConfigurationPlan;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.repository.ConfigurationPlanRepository;
import sn.free.commissioning.service.ConfigurationPlanQueryService;
import sn.free.commissioning.service.ConfigurationPlanService;
import sn.free.commissioning.service.criteria.ConfigurationPlanCriteria;
import sn.free.commissioning.service.dto.ConfigurationPlanDTO;
import sn.free.commissioning.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link sn.free.commissioning.domain.ConfigurationPlan}.
 */
@RestController
@RequestMapping("/api")
public class ConfigurationPlanResource {

    private final Logger log = LoggerFactory.getLogger(ConfigurationPlanResource.class);

    private static final String ENTITY_NAME = "configurationPlan";
    private static final String CLASS_NAME = String.valueOf(ConfigurationPlanResource.class);

    private final CalculusService calculusService;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ConfigurationPlanService configurationPlanService;

    private final ConfigurationPlanRepository configurationPlanRepository;

    private final ConfigurationPlanQueryService configurationPlanQueryService;

    public ConfigurationPlanResource(
        CalculusService calculusService,
        ConfigurationPlanService configurationPlanService,
        ConfigurationPlanRepository configurationPlanRepository,
        ConfigurationPlanQueryService configurationPlanQueryService
    ) {
        this.calculusService = calculusService;
        this.configurationPlanService = configurationPlanService;
        this.configurationPlanRepository = configurationPlanRepository;
        this.configurationPlanQueryService = configurationPlanQueryService;
    }

    /**
     * {@code POST  /configuration-plans} : Create a new configurationPlan.
     *
     * @param configurationPlanDTO the configurationPlanDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new configurationPlanDTO, or with status {@code 400 (Bad Request)} if the configurationPlan has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/configuration-plans")
    public ResponseEntity<ConfigurationPlanDTO> createConfigurationPlan(
        @RequestBody ConfigurationPlanDTO configurationPlanDTO,
        HttpServletRequest request
    ) throws URISyntaxException {
        log.debug("REST request to save ConfigurationPlan : {}", configurationPlanDTO);
        Constants.logInfo(
            true,
            CLASS_NAME,
            " REST request to save ConfigurationPlan ",
            " REST request to save ConfigurationPlan.",
            request
        );

        if (configurationPlanDTO.getId() != null) {
            throw new BadRequestAlertException("A new configurationPlan cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ConfigurationPlanDTO result = configurationPlanService.save(configurationPlanDTO);
        return ResponseEntity
            .created(new URI("/api/configuration-plans/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /configuration-plans/:id} : Updates an existing configurationPlan.
     *
     * @param id the id of the configurationPlanDTO to save.
     * @param configurationPlanDTO the configurationPlanDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated configurationPlanDTO,
     * or with status {@code 400 (Bad Request)} if the configurationPlanDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the configurationPlanDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/configuration-plans/{id}")
    public ResponseEntity<ConfigurationPlanDTO> updateConfigurationPlan(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ConfigurationPlanDTO configurationPlanDTO,
        HttpServletRequest request
    ) throws URISyntaxException {
        log.debug("REST request to update ConfigurationPlan : {}, {}", id, configurationPlanDTO);
        Constants.logInfo(
            true,
            CLASS_NAME,
            " REST request to update ConfigurationPlan ",
            " REST request to update ConfigurationPlan.",
            request
        );
        if (configurationPlanDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, configurationPlanDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!configurationPlanRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ConfigurationPlanDTO result = configurationPlanService.update(configurationPlanDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, configurationPlanDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /configuration-plans/:id} : Partial updates given fields of an existing configurationPlan, field will ignore if it is null
     *
     * @param id the id of the configurationPlanDTO to save.
     * @param configurationPlanDTO the configurationPlanDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated configurationPlanDTO,
     * or with status {@code 400 (Bad Request)} if the configurationPlanDTO is not valid,
     * or with status {@code 404 (Not Found)} if the configurationPlanDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the configurationPlanDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/configuration-plans/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ConfigurationPlanDTO> partialUpdateConfigurationPlan(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody ConfigurationPlanDTO configurationPlanDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update ConfigurationPlan partially : {}, {}", id, configurationPlanDTO);
        if (configurationPlanDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, configurationPlanDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!configurationPlanRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ConfigurationPlanDTO> result = configurationPlanService.partialUpdate(configurationPlanDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, configurationPlanDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /configuration-plans} : get all the configurationPlans.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of configurationPlans in body.
     */
    @GetMapping("/configuration-plans")
    public ResponseEntity<List<ConfigurationPlanDTO>> getAllConfigurationPlans(
        ConfigurationPlanCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        HttpServletRequest request
    ) {
        log.debug("REST request to get ConfigurationPlans by criteria: {}", criteria);
        Page<ConfigurationPlanDTO> page = configurationPlanQueryService.findByCriteria(criteria, pageable);
        Constants.logInfo(
            true,
            CLASS_NAME,
            "REST request to get all ConfigurationPlans",
            "REST request to get all ConfigurationPlans, total : " +
            page.getTotalElements() +
            " - taille de la page : " +
            page.getSize() +
            " - numéro de la page : " +
            page.getNumber(),
            request
        );
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /configuration-plans/count} : count all the configurationPlans.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/configuration-plans/count")
    public ResponseEntity<Long> countConfigurationPlans(ConfigurationPlanCriteria criteria) {
        log.debug("REST request to count ConfigurationPlans by criteria: {}", criteria);
        return ResponseEntity.ok().body(configurationPlanQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /configuration-plans/:id} : get the "id" configurationPlan.
     *
     * @param id the id of the configurationPlanDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the configurationPlanDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/configuration-plans/{id}")
    public ResponseEntity<ConfigurationPlanDTO> getConfigurationPlan(@PathVariable Long id, HttpServletRequest request) {
        log.debug("REST request to get ConfigurationPlan : {}", id);
        Optional<ConfigurationPlanDTO> configurationPlanDTO = configurationPlanService.findOne(id);
        Constants.logInfo(
            configurationPlanDTO.isPresent(),
            CLASS_NAME,
            " REST request to get ConfigurationPlan ",
            " REST request to get ConfigurationPlan.",
            request
        );

        return ResponseUtil.wrapOrNotFound(configurationPlanDTO);
    }

    /**
     * {@code DELETE  /configuration-plans/:id} : delete the "id" configurationPlan.
     *
     * @param id the id of the configurationPlanDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/configuration-plans/{id}")
    public ResponseEntity<Void> deleteConfigurationPlan(@PathVariable Long id, HttpServletRequest request) {
        log.debug("REST request to delete ConfigurationPlan : {}", id);
        configurationPlanService.delete(id);
        Constants.logInfo(
            true,
            CLASS_NAME,
            " REST request to delete ConfigurationPlan.",
            " REST request to delete ConfigurationPlan.",
            request
        );

        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code GET  /configuration-plans/} : get all partener by  the configurationPlan.
     *
     * @param configurationPlan   the configurationPlan.
     * @return  a list of Partner.
     */
    List<Partner> getAllPartnerbyConfigurationPlan(ConfigurationPlan configurationPlan) {
        return calculusService.getAllPartnerbyConfigurationPlan(configurationPlan);
    }
}
