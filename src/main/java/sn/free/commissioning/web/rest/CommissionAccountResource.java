package sn.free.commissioning.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sn.free.commissioning.config.Constants;
import sn.free.commissioning.repository.CommissionAccountRepository;
import sn.free.commissioning.service.CommissionAccountQueryService;
import sn.free.commissioning.service.CommissionAccountService;
import sn.free.commissioning.service.criteria.CommissionAccountCriteria;
import sn.free.commissioning.service.dto.CommissionAccountDTO;
import sn.free.commissioning.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link sn.free.commissioning.domain.CommissionAccount}.
 */
@RestController
@RequestMapping("/api")
public class CommissionAccountResource {

    private final Logger log = LoggerFactory.getLogger(CommissionAccountResource.class);

    private static final String ENTITY_NAME = "commissionAccount";
    private static final String CLASS_NAME = String.valueOf(CommissionAccountResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CommissionAccountService commissionAccountService;

    private final CommissionAccountRepository commissionAccountRepository;

    private final CommissionAccountQueryService commissionAccountQueryService;

    public CommissionAccountResource(
        CommissionAccountService commissionAccountService,
        CommissionAccountRepository commissionAccountRepository,
        CommissionAccountQueryService commissionAccountQueryService
    ) {
        this.commissionAccountService = commissionAccountService;
        this.commissionAccountRepository = commissionAccountRepository;
        this.commissionAccountQueryService = commissionAccountQueryService;
    }

    /**
     * {@code POST  /commission-accounts} : Create a new commissionAccount.
     *
     * @param commissionAccountDTO the commissionAccountDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new commissionAccountDTO, or with status {@code 400 (Bad Request)} if the commissionAccount has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/commission-accounts")
    public ResponseEntity<CommissionAccountDTO> createCommissionAccount(
        @RequestBody CommissionAccountDTO commissionAccountDTO,
        HttpServletRequest request
    ) throws URISyntaxException {
        log.debug("REST request to save CommissionAccount : {}", commissionAccountDTO);
        Constants.logInfo(
            true,
            CLASS_NAME,
            " REST request to save CommissionAccount.",
            " REST request to save CommissionAccount .",
            request
        );

        if (commissionAccountDTO.getId() != null) {
            throw new BadRequestAlertException("A new commissionAccount cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CommissionAccountDTO result = commissionAccountService.save(commissionAccountDTO);
        return ResponseEntity
            .created(new URI("/api/commission-accounts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /commission-accounts/:id} : Updates an existing commissionAccount.
     *
     * @param id the id of the commissionAccountDTO to save.
     * @param commissionAccountDTO the commissionAccountDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated commissionAccountDTO,
     * or with status {@code 400 (Bad Request)} if the commissionAccountDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the commissionAccountDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/commission-accounts/{id}")
    public ResponseEntity<CommissionAccountDTO> updateCommissionAccount(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CommissionAccountDTO commissionAccountDTO,
        HttpServletRequest request
    ) throws URISyntaxException {
        log.debug("REST request to update CommissionAccount : {}, {}", id, commissionAccountDTO);
        Constants.logInfo(
            true,
            CLASS_NAME,
            " REST request to update a CommissionAccount.",
            " REST request to  update a CommissionAccount .",
            request
        );

        if (commissionAccountDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, commissionAccountDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!commissionAccountRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CommissionAccountDTO result = commissionAccountService.update(commissionAccountDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, commissionAccountDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /commission-accounts/:id} : Partial updates given fields of an existing commissionAccount, field will ignore if it is null
     *
     * @param id the id of the commissionAccountDTO to save.
     * @param commissionAccountDTO the commissionAccountDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated commissionAccountDTO,
     * or with status {@code 400 (Bad Request)} if the commissionAccountDTO is not valid,
     * or with status {@code 404 (Not Found)} if the commissionAccountDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the commissionAccountDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/commission-accounts/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CommissionAccountDTO> partialUpdateCommissionAccount(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody CommissionAccountDTO commissionAccountDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update CommissionAccount partially : {}, {}", id, commissionAccountDTO);
        if (commissionAccountDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, commissionAccountDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!commissionAccountRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CommissionAccountDTO> result = commissionAccountService.partialUpdate(commissionAccountDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, commissionAccountDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /commission-accounts} : get all the commissionAccounts.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of commissionAccounts in body.
     */
    @GetMapping("/commission-accounts")
    public ResponseEntity<List<CommissionAccountDTO>> getAllCommissionAccounts(
        CommissionAccountCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        HttpServletRequest request
    ) {
        log.debug("REST request to get CommissionAccounts by criteria: {}", criteria);
        Constants.logInfo(
            true,
            CLASS_NAME,
            " REST request to get CommissionAccounts.",
            " REST request to get CommissionAccounts.",
            request
        );

        Page<CommissionAccountDTO> page = commissionAccountQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /commission-accounts/count} : count all the commissionAccounts.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/commission-accounts/count")
    public ResponseEntity<Long> countCommissionAccounts(CommissionAccountCriteria criteria) {
        log.debug("REST request to count CommissionAccounts by criteria: {}", criteria);
        return ResponseEntity.ok().body(commissionAccountQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /commission-accounts/:id} : get the "id" commissionAccount.
     *
     * @param id the id of the commissionAccountDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the commissionAccountDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/commission-accounts/{id}")
    public ResponseEntity<CommissionAccountDTO> getCommissionAccount(@PathVariable Long id, HttpServletRequest request) {
        log.debug("REST request to get CommissionAccount : {}", id);
        Constants.logInfo(true, CLASS_NAME, " REST request to get CommissionAccount.", " REST request to get CommissionAccount.", request);

        Optional<CommissionAccountDTO> commissionAccountDTO = commissionAccountService.findOne(id);
        return ResponseUtil.wrapOrNotFound(commissionAccountDTO);
    }

    /**
     * {@code DELETE  /commission-accounts/:id} : delete the "id" commissionAccount.
     *
     * @param id the id of the commissionAccountDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/commission-accounts/{id}")
    public ResponseEntity<Void> deleteCommissionAccount(@PathVariable Long id, HttpServletRequest request) {
        log.debug("REST request to delete CommissionAccount : {}", id);
        Constants.logInfo(
            true,
            CLASS_NAME,
            " REST request to delete CommissionAccount.",
            " REST request to delete CommissionAccount .",
            request
        );

        commissionAccountService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
