package sn.free.commissioning.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sn.free.commissioning.config.Constants;
import sn.free.commissioning.repository.CommissionRepository;
import sn.free.commissioning.service.CommissionQueryService;
import sn.free.commissioning.service.CommissionService;
import sn.free.commissioning.service.criteria.CommissionCriteria;
import sn.free.commissioning.service.criteria.CommissioningPlanCriteria;
import sn.free.commissioning.service.dto.CommissionBulkPaymentDTO;
import sn.free.commissioning.service.dto.CommissionDTO;
import sn.free.commissioning.service.dto.CommissioningPlanDTO;
import sn.free.commissioning.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link sn.free.commissioning.domain.Commission}.
 */
@RestController
@RequestMapping("/api")
public class CommissionResource {

    private final Logger log = LoggerFactory.getLogger(CommissionResource.class);

    private static final String ENTITY_NAME = "commission";
    private static final String CLASS_NAME = String.valueOf(CommissionResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CommissionService commissionService;

    private final CommissionRepository commissionRepository;

    private final CommissionQueryService commissionQueryService;

    public CommissionResource(
        CommissionService commissionService,
        CommissionRepository commissionRepository,
        CommissionQueryService commissionQueryService
    ) {
        this.commissionService = commissionService;
        this.commissionRepository = commissionRepository;
        this.commissionQueryService = commissionQueryService;
    }

    /**
     * {@code GET  /commissions/all-profiles-by-plan/:id} : get the all profiles commissions by commissioningPlan.
     *
     * @param pageable the pagination information.
     * @param planId the commissioning plan's id .
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the list of commissionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/commissions/all-profiles-by-plan/{planId}")
    public ResponseEntity<?> getAllProfilesCommissionsByPlan(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        @PathVariable Long planId,
        HttpServletRequest request
    ) {
        log.debug("REST request to get all profiles Commissions for plan : {}", planId);
        Page<CommissionBulkPaymentDTO> page = commissionService.findAllProfilesCommissionsByPlan(planId, pageable);
        if (page.isEmpty()) {
            return ResponseEntity.status(480).body("Ce plan n'as pas de fréquence de paiement");
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        Constants.logInfo(
            !page.isEmpty(),
            CLASS_NAME,
            "REST request to get all profiles Commissions for plan : " + planId,
            "REST request to get all profiles Commissions for plan : " +
            planId +
            ", Total : " +
            page.getTotalElements() +
            " - taille de la page : " +
            page.getSize() +
            " - numéro de la page : " +
            page.getNumber(),
            request
        );
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /commissions/top-profile-by-plan/:id} : get the top profile (DP) commissions by commissioningPlan.
     *
     * @param planId the commissioning plan's id .
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the list of commissionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/commissions/top-profile-by-plan/{planId}")
    public ResponseEntity<List<CommissionDTO>> getTopProfileCommissions(@PathVariable Long planId, HttpServletRequest request) {
        log.debug("REST request to get top profile (DP) Commissions for plan : {}", planId);
        List<CommissionDTO> commissionDTO = commissionService.findTopProfileCommissionsByPlan(planId);
        Constants.logInfo(
            !commissionDTO.isEmpty(),
            CLASS_NAME,
            "REST request to get top profile (DP) Commissions for plan : " + planId,
            "REST request to get top profile (DP) Commissions for plan : " + planId,
            request
        );

        return ResponseEntity.ok().body(commissionDTO);
    }

    /**
     * {@code POST  /commissions} : Create a new commission.
     *
     * @param commissionDTO the commissionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new commissionDTO, or with status {@code 400 (Bad Request)} if the commission has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/commissions")
    public ResponseEntity<CommissionDTO> createCommission(@Valid @RequestBody CommissionDTO commissionDTO, HttpServletRequest request)
        throws URISyntaxException {
        log.debug("REST request to save Commission : {}", commissionDTO);
        Constants.logInfo(true, CLASS_NAME, " REST request to save Commission", " REST request to save Commission.", request);

        if (commissionDTO.getId() != null) {
            throw new BadRequestAlertException("A new commission cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CommissionDTO result = commissionService.save(commissionDTO);
        return ResponseEntity
            .created(new URI("/api/commissions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /commissions/:id} : Updates an existing commission.
     *
     * @param id the id of the commissionDTO to save.
     * @param commissionDTO the commissionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated commissionDTO,
     * or with status {@code 400 (Bad Request)} if the commissionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the commissionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/commissions/{id}")
    public ResponseEntity<CommissionDTO> updateCommission(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody CommissionDTO commissionDTO,
        HttpServletRequest request
    ) throws URISyntaxException {
        log.debug("REST request to update Commission : {}, {}", id, commissionDTO);
        Constants.logInfo(true, CLASS_NAME, " REST request to update Commission", " REST request to update Commission.", request);

        if (commissionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, commissionDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!commissionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CommissionDTO result = commissionService.update(commissionDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, commissionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /commissions/:id} : Partial updates given fields of an existing commission, field will ignore if it is null
     *
     * @param id the id of the commissionDTO to save.
     * @param commissionDTO the commissionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated commissionDTO,
     * or with status {@code 400 (Bad Request)} if the commissionDTO is not valid,
     * or with status {@code 404 (Not Found)} if the commissionDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the commissionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/commissions/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CommissionDTO> partialUpdateCommission(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody CommissionDTO commissionDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Commission partially : {}, {}", id, commissionDTO);
        if (commissionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, commissionDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!commissionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CommissionDTO> result = commissionService.partialUpdate(commissionDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, commissionDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /commissions} : get all the commissions.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of commissions in body.
     */
    @GetMapping("/commissions")
    public ResponseEntity<List<CommissionDTO>> getAllCommissions(
        CommissionCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        HttpServletRequest request
    ) {
        log.info("REST request to get Commissions by criteria: {}", criteria);
        Constants.logInfo(true, CLASS_NAME, " REST request to get Commissions ", " REST request to get Commissions.", request);

        Page<CommissionDTO> page = commissionQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /commissions/count} : count all the commissions.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/commissions/count")
    public ResponseEntity<Long> countCommissions(CommissionCriteria criteria) {
        log.debug("REST request to count Commissions by criteria: {}", criteria);
        return ResponseEntity.ok().body(commissionQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /commissions/:id} : get the "id" commission.
     *
     * @param id the id of the commissionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the commissionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/commissions/{id}")
    public ResponseEntity<CommissionDTO> getCommission(@PathVariable Long id, HttpServletRequest request) {
        log.debug("REST request to get Commission : {}", id);
        Optional<CommissionDTO> commissionDTO = commissionService.findOne(id);
        Constants.logInfo(
            commissionDTO.isPresent(),
            CLASS_NAME,
            " REST request to get Commissions ",
            " REST request to get Commissions.",
            request
        );

        return ResponseUtil.wrapOrNotFound(commissionDTO);
    }

    /**
     * {@code DELETE  /commissions/:id} : delete the "id" commission.
     *
     * @param id the id of the commissionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/commissions/{id}")
    public ResponseEntity<Void> deleteCommission(@PathVariable Long id, HttpServletRequest request) {
        log.debug("REST request to delete Commission : {}", id);
        Constants.logInfo(true, CLASS_NAME, " REST request to delete Commission ", " REST request to delete Commission.", request);

        commissionService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
