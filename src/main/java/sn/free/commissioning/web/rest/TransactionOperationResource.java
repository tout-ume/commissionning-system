package sn.free.commissioning.web.rest;

import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import java.net.URISyntaxException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sn.free.commissioning.calculus_engine.domain.enumeration.InstantCalculationStatus;
import sn.free.commissioning.calculus_engine.service.CalculusService;
import sn.free.commissioning.config.Constants;
import sn.free.commissioning.domain.*;
import sn.free.commissioning.domain.enumeration.FrequencyType;
import sn.free.commissioning.repository.TransactionOperationRepository;
import sn.free.commissioning.service.*;
import sn.free.commissioning.service.criteria.TransactionOperationCriteria;
import sn.free.commissioning.service.dto.TransactionOperationDTO;
import sn.free.commissioning.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link sn.free.commissioning.domain.TransactionOperation}.
 */
@RestController
@RequestMapping("/api")
public class TransactionOperationResource {

    private final Logger log = LoggerFactory.getLogger(TransactionOperationResource.class);

    private static final String ENTITY_NAME = "transactionOperation";
    private static final String CLASS_NAME = String.valueOf(TransactionOperationResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TransactionOperationService transactionOperationService;

    private final TransactionOperationRepository transactionOperationRepository;

    private final TransactionOperationQueryService transactionOperationQueryService;
    private final CalculusService calculusService;

    public TransactionOperationResource(
        TransactionOperationService transactionOperationService,
        TransactionOperationRepository transactionOperationRepository,
        TransactionOperationQueryService transactionOperationQueryService,
        CalculusService calculusService
    ) {
        this.transactionOperationService = transactionOperationService;
        this.transactionOperationRepository = transactionOperationRepository;
        this.transactionOperationQueryService = transactionOperationQueryService;
        this.calculusService = calculusService;
    }

    @GetMapping("/transaction-operations-by-partner")
    public ResponseEntity<List<TransactionOperation>> getAllTransactionOperationsByPartner() {
        Period period = new Period();
        period.setPeriodType(FrequencyType.DAILY);
        period.setDayHourFrom(3);
        period.setDayHourTo(15);
        period.setIsCompleteDay(false);
        period.setIsPreviousDayHourFrom(true);

        Partner partner = new Partner();
        partner.setMsisdn("+221761002106");
        log.info("REST request to get TransactionOperations by partner");

        //        List<TransactionOperation> result = transactionOperationService.getPartnerTransactions(partner, period);
        //        return ResponseEntity.ok().body(result);
        return ResponseEntity.ok().body(null);
    }

    public ResponseEntity<?> rateLimiterFallback(Exception e) {
        return new ResponseEntity<>("This service does not permit further requests", HttpStatus.TOO_MANY_REQUESTS);
    }

    /**
     * {@code POST  /transaction-operations} : Create a new transactionOperation.
     *
     * @param transactionOperationDTO the transactionOperationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new transactionOperationDTO, or with status {@code 400 (Bad Request)} if the transactionOperation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/transaction-operations")
    @RateLimiter(name = "transactionListener", fallbackMethod = "rateLimiterFallback")
    public ResponseEntity<?> createTransactionOperation(
        @RequestBody TransactionOperationDTO transactionOperationDTO,
        HttpServletRequest request
    ) throws URISyntaxException {
        if (transactionOperationDTO.getId() != null) {
            throw new BadRequestAlertException("A new transactionOperation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        // fetch only one zone from the trx senderZone list
        transactionOperationDTO.setSenderZone(transactionOperationDTO.getSenderZone().split(",")[0]);

        TransactionOperationDTO savedTransactionOperationDTO = transactionOperationService.save(transactionOperationDTO);

        log.info("REST request to save TransactionOperation : {}", savedTransactionOperationDTO);
        Constants.logInfo(
            true,
            CLASS_NAME,
            "REST request to save a new  TransactionOperation  ",
            "Initiation d'une sauvegarde de TransactionOperation ",
            request
        );
        calculate(savedTransactionOperationDTO)
            .thenAccept(data -> {
                log.info("================================= Instant Calculation Result : {}", data);
                Constants.logInfo(
                    !data.equals(InstantCalculationStatus.FAILURE),
                    CLASS_NAME,
                    "================================= Instant Calculation Result : " + data,
                    "================================= Instant Calculation Result : " + data,
                    null
                );
            });
        return ResponseEntity.ok().build();
    }

    private CompletableFuture<?> calculate(TransactionOperationDTO transaction) {
        return CompletableFuture.supplyAsync(() -> calculusService.performInstantCalculation(transaction));
    }

    /**
     * {@code PUT  /transaction-operations/:id} : Updates an existing transactionOperation.
     *
     * @param id the id of the transactionOperationDTO to save.
     * @param transactionOperationDTO the transactionOperationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated transactionOperationDTO,
     * or with status {@code 400 (Bad Request)} if the transactionOperationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the transactionOperationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/transaction-operations/{id}")
    public ResponseEntity<TransactionOperationDTO> updateTransactionOperation(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody TransactionOperationDTO transactionOperationDTO,
        HttpServletRequest request
    ) throws URISyntaxException {
        log.info("REST request to update TransactionOperation : {}, {}", id, transactionOperationDTO);
        if (transactionOperationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, transactionOperationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!transactionOperationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        TransactionOperationDTO result = transactionOperationService.update(transactionOperationDTO);
        Constants.logInfo(
            result != null,
            CLASS_NAME,
            "REST request to update TransactionOperation.",
            result != null ? " REST request to update TransactionOperation avec succes : " + result.getId() : "Échec",
            request
        );
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, transactionOperationDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /transaction-operations/:id} : Partial updates given fields of an existing transactionOperation, field will ignore if it is null
     *
     * @param id the id of the transactionOperationDTO to save.
     * @param transactionOperationDTO the transactionOperationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated transactionOperationDTO,
     * or with status {@code 400 (Bad Request)} if the transactionOperationDTO is not valid,
     * or with status {@code 404 (Not Found)} if the transactionOperationDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the transactionOperationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/transaction-operations/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<TransactionOperationDTO> partialUpdateTransactionOperation(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody TransactionOperationDTO transactionOperationDTO
    ) throws URISyntaxException {
        log.info("REST request to partial update TransactionOperation partially : {}, {}", id, transactionOperationDTO);
        if (transactionOperationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, transactionOperationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!transactionOperationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<TransactionOperationDTO> result = transactionOperationService.partialUpdate(transactionOperationDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, transactionOperationDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /transaction-operations} : get all the transactionOperations.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of transactionOperations in body.
     */
    @GetMapping("/transaction-operations")
    public ResponseEntity<List<TransactionOperationDTO>> getAllTransactionOperations(
        TransactionOperationCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        HttpServletRequest request
    ) {
        log.info("REST request to get TransactionOperations by criteria: {}", criteria);
        Page<TransactionOperationDTO> page = transactionOperationQueryService.findByCriteria(criteria, pageable);
        Constants.logInfo(
            true,
            CLASS_NAME,
            "REST request to get all TransactionOperations",
            "REST request to get all TransactionOperations, total : " +
            page.getTotalElements() +
            " - taille de la page : " +
            page.getSize() +
            " - numéro de la page : " +
            page.getNumber(),
            request
        );
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /transaction-operations/count} : count all the transactionOperations.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/transaction-operations/count")
    public ResponseEntity<Long> countTransactionOperations(TransactionOperationCriteria criteria) {
        log.info("REST request to count TransactionOperations by criteria: {}", criteria);
        return ResponseEntity.ok().body(transactionOperationQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /transaction-operations/:id} : get the "id" transactionOperation.
     *
     * @param id the id of the transactionOperationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the transactionOperationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/transaction-operations/{id}")
    public ResponseEntity<TransactionOperationDTO> getTransactionOperation(@PathVariable Long id, HttpServletRequest request) {
        log.info("REST request to get TransactionOperation : {}", id);
        Optional<TransactionOperationDTO> transactionOperationDTO = transactionOperationService.findOne(id);
        Constants.logInfo(
            transactionOperationDTO.isPresent(),
            CLASS_NAME,
            " REST request to get a TransactionOperation ",
            transactionOperationDTO.map(dto -> "REST request to get a TransactionOperation with ID : " + dto.getId()).orElse("ÉCHEC"),
            request
        );
        return ResponseUtil.wrapOrNotFound(transactionOperationDTO);
    }

    /**
     * {@code DELETE  /transaction-operations/:id} : delete the "id" transactionOperation.
     *
     * @param id the id of the transactionOperationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/transaction-operations/{id}")
    public ResponseEntity<Void> deleteTransactionOperation(@PathVariable Long id, HttpServletRequest request) {
        log.info("REST request to delete TransactionOperation : {}", id);
        Constants.logInfo(
            true,
            CLASS_NAME,
            " REST request  to delete TransactionOperation  ",
            "REST request to delete TransactionOperation  by id" + id,
            request
        );
        transactionOperationService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }

    /**
     * {@code GET  /transaction-operations/bymsisdn?senderMsisdn=pdv} : get list transactionOperations where senderMsisdn is pdv.
     *
     * @param senderMsisdn the sendermsisdn of the transactionOperation.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of transactionOperations in body.
     */
    @GetMapping("/transaction-operations/bymsisdn?senderMsisdn=")
    public ResponseEntity<List<TransactionOperationDTO>> getTransactionOperationsbyPDV(
        @RequestParam(defaultValue = "pdv") String senderMsisdn,
        HttpServletRequest request
    ) {
        log.info("REST request to get TransactionOperations by senderMsisdn: {}", senderMsisdn);
        Constants.logInfo(
            true,
            CLASS_NAME,
            "  REST request to get TransactionOperations by senderMsisdn: {} " + senderMsisdn,
            "REST request to get TransactionOperations by senderMsisdn: {} " + senderMsisdn,
            request
        );

        // List<TransactionOperationDTO> transactionOperations= transactionOperationService.findBySenderMsisdnIgnoreCase(senderMsisdn);
        return ResponseEntity.ok().body(transactionOperationService.findBySenderMsisdnIgnoreCase(senderMsisdn));
    }

    /**
     * {@code GET  /transaction-operations/sum-amount} : get sum amount of the transactionOperations.
     *
     * @param transactionOperations the list of the transactionOperation.
     * @return the total amount.
     */
    @GetMapping("/transaction-operations/sum-amount")
    public Double sumTransactionOperationsAmount(
        @RequestBody List<TransactionOperation> transactionOperations,
        HttpServletRequest request
    ) {
        log.info("REST request to get the total amount of a list transaction operations: {}", transactionOperations);
        Constants.logInfo(
            true,
            CLASS_NAME,
            "  REST request to get the total amount of a list transaction operations ",
            "REST request to get the total amount of a list transaction operations ",
            request
        );

        return calculusService.sumTransactionOperationsAmount(transactionOperations);
    }
}
