/**
 * View Models used by Spring MVC REST controllers.
 */
package sn.free.commissioning.web.rest.vm;
