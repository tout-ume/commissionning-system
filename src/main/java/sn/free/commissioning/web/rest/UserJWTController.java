package sn.free.commissioning.web.rest;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.ldap.authentication.ad.ActiveDirectoryLdapAuthenticationProvider;
import org.springframework.security.ldap.userdetails.LdapUserDetails;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import sn.free.commissioning.config.Constants;
import sn.free.commissioning.domain.Authority;
import sn.free.commissioning.domain.User;
import sn.free.commissioning.security.jwt.JWTFilter;
import sn.free.commissioning.security.jwt.TokenProvider;
import sn.free.commissioning.service.UserService;
import sn.free.commissioning.service.dto.UserDTO;
import sn.free.commissioning.web.rest.vm.LoginVM;

/**
 * Controller to authenticate users.
 */
@RestController
@RequestMapping("/api")
public class UserJWTController {

    Logger log = LoggerFactory.getLogger(UserJWTController.class);

    private final TokenProvider tokenProvider;

    private static final String CLASS_NAME = String.valueOf(UserJWTController.class);

    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    private final UserService userService;

    private final ActiveDirectoryLdapAuthenticationProvider adAuthProvider;

    public UserJWTController(
        TokenProvider tokenProvider,
        AuthenticationManagerBuilder authenticationManagerBuilder,
        UserService userService,
        ActiveDirectoryLdapAuthenticationProvider adAuthProvider,
        UserDetailsService userDetailsService
    ) throws Exception {
        this.tokenProvider = tokenProvider;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.userService = userService;
        this.authenticationManagerBuilder.userDetailsService(userDetailsService);
        this.adAuthProvider = adAuthProvider;
        adAuthProvider.setSearchFilter("(&(objectClass=user)(userPrincipalName={0}))");
        this.authenticationManagerBuilder.authenticationProvider(this.adAuthProvider);
    }

    @PostMapping("/authenticate")
    @Transactional
    public ResponseEntity<JWTToken> authorize(@Valid @RequestBody LoginVM loginVM, HttpServletRequest request) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
            loginVM.getUsername(),
            loginVM.getPassword()
        );

        // Check if user exist before and then authenticate them with AD
        if (!userService.findByLogin(loginVM.getUsername()).isPresent()) {
            Constants.logInfo(false, CLASS_NAME, "Tentative de connexion", "Interdite", request);
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }

        Authentication authentication = authenticationManagerBuilder.getObject().authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        if (authentication == null) {
            Constants.logInfo(false, CLASS_NAME, "Tentative de connexion", "Interdite", request);

            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        Optional<User> userOptional = this.userService.getUserWithAuthoritiesByLogin(loginVM.getUsername());
        userOptional.get().setLastAuthenticationDate(Instant.now().minusSeconds(1));
        User authenticatedUser = userService.updateUser(userOptional.get()).get();
        log.info("USER {}", authenticatedUser);
        //        Set<String> roles = userOptional.get().getAuthorities().stream().map(Authority::getName).collect(Collectors.toSet());
        //        Set<String> actions = userService.getUserActions(roles);
        //        log.debug("actions User res: {}", actions);
        // Remember me is not used any more
        String jwt = tokenProvider.createToken(authentication, loginVM.getUsername().equalsIgnoreCase("admin"));
        //        String jwt = tokenProvider.createToken(authentication, roles, actions);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTFilter.AUTHORIZATION_HEADER, "Bearer " + jwt);
        Constants.logInfo(true, CLASS_NAME, "Connexion de l'utilisateur", "nom d'utilisateur : " + authentication.getName(), request);
        return new ResponseEntity<>(new JWTToken(jwt), httpHeaders, HttpStatus.OK);
    }

    /**
     * Object to return as body in JWT Authentication.
     */
    static class JWTToken {

        private String idToken;

        JWTToken(String idToken) {
            this.idToken = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return idToken;
        }

        void setIdToken(String idToken) {
            this.idToken = idToken;
        }
    }
}
