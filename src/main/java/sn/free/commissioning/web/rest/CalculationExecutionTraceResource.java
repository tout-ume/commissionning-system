package sn.free.commissioning.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sn.free.commissioning.repository.CalculationExecutionTraceRepository;
import sn.free.commissioning.service.CalculationExecutionTraceQueryService;
import sn.free.commissioning.service.CalculationExecutionTraceService;
import sn.free.commissioning.service.criteria.CalculationExecutionTraceCriteria;
import sn.free.commissioning.service.dto.CalculationExecutionTraceDTO;
import sn.free.commissioning.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link sn.free.commissioning.domain.CalculationExecutionTrace}.
 */
@RestController
@RequestMapping("/api")
public class CalculationExecutionTraceResource {

    private final Logger log = LoggerFactory.getLogger(CalculationExecutionTraceResource.class);

    private static final String ENTITY_NAME = "calculationExecutionTrace";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CalculationExecutionTraceService calculationExecutionTraceService;

    private final CalculationExecutionTraceRepository calculationExecutionTraceRepository;

    private final CalculationExecutionTraceQueryService calculationExecutionTraceQueryService;

    public CalculationExecutionTraceResource(
        CalculationExecutionTraceService calculationExecutionTraceService,
        CalculationExecutionTraceRepository calculationExecutionTraceRepository,
        CalculationExecutionTraceQueryService calculationExecutionTraceQueryService
    ) {
        this.calculationExecutionTraceService = calculationExecutionTraceService;
        this.calculationExecutionTraceRepository = calculationExecutionTraceRepository;
        this.calculationExecutionTraceQueryService = calculationExecutionTraceQueryService;
    }

    /**
     * {@code POST  /calculation-execution-traces} : Create a new calculationExecutionTrace.
     *
     * @param calculationExecutionTraceDTO the calculationExecutionTraceDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new calculationExecutionTraceDTO, or with status {@code 400 (Bad Request)} if the calculationExecutionTrace has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/calculation-execution-traces")
    public ResponseEntity<CalculationExecutionTraceDTO> createCalculationExecutionTrace(
        @Valid @RequestBody CalculationExecutionTraceDTO calculationExecutionTraceDTO
    ) throws URISyntaxException {
        log.debug("REST request to save CalculationExecutionTrace : {}", calculationExecutionTraceDTO);
        if (calculationExecutionTraceDTO.getId() != null) {
            throw new BadRequestAlertException("A new calculationExecutionTrace cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CalculationExecutionTraceDTO result = calculationExecutionTraceService.save(calculationExecutionTraceDTO);
        return ResponseEntity
            .created(new URI("/api/calculation-execution-traces/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /calculation-execution-traces/:id} : Updates an existing calculationExecutionTrace.
     *
     * @param id the id of the calculationExecutionTraceDTO to save.
     * @param calculationExecutionTraceDTO the calculationExecutionTraceDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated calculationExecutionTraceDTO,
     * or with status {@code 400 (Bad Request)} if the calculationExecutionTraceDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the calculationExecutionTraceDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/calculation-execution-traces/{id}")
    public ResponseEntity<CalculationExecutionTraceDTO> updateCalculationExecutionTrace(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody CalculationExecutionTraceDTO calculationExecutionTraceDTO
    ) throws URISyntaxException {
        log.debug("REST request to update CalculationExecutionTrace : {}, {}", id, calculationExecutionTraceDTO);
        if (calculationExecutionTraceDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, calculationExecutionTraceDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!calculationExecutionTraceRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        CalculationExecutionTraceDTO result = calculationExecutionTraceService.update(calculationExecutionTraceDTO);
        return ResponseEntity
            .ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, calculationExecutionTraceDTO.getId().toString())
            )
            .body(result);
    }

    /**
     * {@code PATCH  /calculation-execution-traces/:id} : Partial updates given fields of an existing calculationExecutionTrace, field will ignore if it is null
     *
     * @param id the id of the calculationExecutionTraceDTO to save.
     * @param calculationExecutionTraceDTO the calculationExecutionTraceDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated calculationExecutionTraceDTO,
     * or with status {@code 400 (Bad Request)} if the calculationExecutionTraceDTO is not valid,
     * or with status {@code 404 (Not Found)} if the calculationExecutionTraceDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the calculationExecutionTraceDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/calculation-execution-traces/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<CalculationExecutionTraceDTO> partialUpdateCalculationExecutionTrace(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody CalculationExecutionTraceDTO calculationExecutionTraceDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update CalculationExecutionTrace partially : {}, {}", id, calculationExecutionTraceDTO);
        if (calculationExecutionTraceDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, calculationExecutionTraceDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!calculationExecutionTraceRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<CalculationExecutionTraceDTO> result = calculationExecutionTraceService.partialUpdate(calculationExecutionTraceDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, calculationExecutionTraceDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /calculation-execution-traces} : get all the calculationExecutionTraces.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of calculationExecutionTraces in body.
     */
    @GetMapping("/calculation-execution-traces")
    public ResponseEntity<List<CalculationExecutionTraceDTO>> getAllCalculationExecutionTraces(
        CalculationExecutionTraceCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get CalculationExecutionTraces by criteria: {}", criteria);
        Page<CalculationExecutionTraceDTO> page = calculationExecutionTraceQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /calculation-execution-traces/count} : count all the calculationExecutionTraces.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/calculation-execution-traces/count")
    public ResponseEntity<Long> countCalculationExecutionTraces(CalculationExecutionTraceCriteria criteria) {
        log.debug("REST request to count CalculationExecutionTraces by criteria: {}", criteria);
        return ResponseEntity.ok().body(calculationExecutionTraceQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /calculation-execution-traces/:id} : get the "id" calculationExecutionTrace.
     *
     * @param id the id of the calculationExecutionTraceDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the calculationExecutionTraceDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/calculation-execution-traces/{id}")
    public ResponseEntity<CalculationExecutionTraceDTO> getCalculationExecutionTrace(@PathVariable Long id) {
        log.debug("REST request to get CalculationExecutionTrace : {}", id);
        Optional<CalculationExecutionTraceDTO> calculationExecutionTraceDTO = calculationExecutionTraceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(calculationExecutionTraceDTO);
    }

    /**
     * {@code DELETE  /calculation-execution-traces/:id} : delete the "id" calculationExecutionTrace.
     *
     * @param id the id of the calculationExecutionTraceDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/calculation-execution-traces/{id}")
    public ResponseEntity<Void> deleteCalculationExecutionTrace(@PathVariable Long id) {
        log.debug("REST request to delete CalculationExecutionTrace : {}", id);
        calculationExecutionTraceService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
