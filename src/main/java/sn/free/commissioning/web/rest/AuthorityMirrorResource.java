package sn.free.commissioning.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sn.free.commissioning.repository.AuthorityMirrorRepository;
import sn.free.commissioning.service.AuthorityMirrorQueryService;
import sn.free.commissioning.service.AuthorityMirrorService;
import sn.free.commissioning.service.criteria.AuthorityMirrorCriteria;
import sn.free.commissioning.service.dto.AuthorityMirrorDTO;
import sn.free.commissioning.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link sn.free.commissioning.domain.AuthorityMirror}.
 */
@RestController
@RequestMapping("/api")
public class AuthorityMirrorResource {

    private final Logger log = LoggerFactory.getLogger(AuthorityMirrorResource.class);

    private static final String ENTITY_NAME = "authorityMirror";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AuthorityMirrorService authorityMirrorService;

    private final AuthorityMirrorRepository authorityMirrorRepository;

    private final AuthorityMirrorQueryService authorityMirrorQueryService;

    public AuthorityMirrorResource(
        AuthorityMirrorService authorityMirrorService,
        AuthorityMirrorRepository authorityMirrorRepository,
        AuthorityMirrorQueryService authorityMirrorQueryService
    ) {
        this.authorityMirrorService = authorityMirrorService;
        this.authorityMirrorRepository = authorityMirrorRepository;
        this.authorityMirrorQueryService = authorityMirrorQueryService;
    }

    /**
     * {@code POST  /authority-mirrors} : Create a new authorityMirror.
     *
     * @param authorityMirrorDTO the authorityMirrorDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new authorityMirrorDTO, or with status {@code 400 (Bad Request)} if the authorityMirror has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/authority-mirrors")
    public ResponseEntity<AuthorityMirrorDTO> createAuthorityMirror(@Valid @RequestBody AuthorityMirrorDTO authorityMirrorDTO)
        throws URISyntaxException {
        log.debug("REST request to save AuthorityMirror : {}", authorityMirrorDTO);
        if (authorityMirrorDTO.getId() != null) {
            throw new BadRequestAlertException("A new authorityMirror cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AuthorityMirrorDTO result = authorityMirrorService.save(authorityMirrorDTO);
        return ResponseEntity
            .created(new URI("/api/authority-mirrors/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /authority-mirrors/:id} : Updates an existing authorityMirror.
     *
     * @param id the id of the authorityMirrorDTO to save.
     * @param authorityMirrorDTO the authorityMirrorDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated authorityMirrorDTO,
     * or with status {@code 400 (Bad Request)} if the authorityMirrorDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the authorityMirrorDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/authority-mirrors/{id}")
    public ResponseEntity<AuthorityMirrorDTO> updateAuthorityMirror(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody AuthorityMirrorDTO authorityMirrorDTO
    ) throws URISyntaxException {
        log.debug("REST request to update AuthorityMirror : {}, {}", id, authorityMirrorDTO);
        if (authorityMirrorDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, authorityMirrorDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!authorityMirrorRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        AuthorityMirrorDTO result = authorityMirrorService.update(authorityMirrorDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, authorityMirrorDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /authority-mirrors/:id} : Partial updates given fields of an existing authorityMirror, field will ignore if it is null
     *
     * @param id the id of the authorityMirrorDTO to save.
     * @param authorityMirrorDTO the authorityMirrorDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated authorityMirrorDTO,
     * or with status {@code 400 (Bad Request)} if the authorityMirrorDTO is not valid,
     * or with status {@code 404 (Not Found)} if the authorityMirrorDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the authorityMirrorDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/authority-mirrors/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<AuthorityMirrorDTO> partialUpdateAuthorityMirror(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody AuthorityMirrorDTO authorityMirrorDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update AuthorityMirror partially : {}, {}", id, authorityMirrorDTO);
        if (authorityMirrorDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, authorityMirrorDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!authorityMirrorRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<AuthorityMirrorDTO> result = authorityMirrorService.partialUpdate(authorityMirrorDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, authorityMirrorDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /authority-mirrors} : get all the authorityMirrors.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of authorityMirrors in body.
     */
    @GetMapping("/authority-mirrors")
    public ResponseEntity<List<AuthorityMirrorDTO>> getAllAuthorityMirrors(
        AuthorityMirrorCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get AuthorityMirrors by criteria: {}", criteria);
        Page<AuthorityMirrorDTO> page = authorityMirrorQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /authority-mirrors/count} : count all the authorityMirrors.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/authority-mirrors/count")
    public ResponseEntity<Long> countAuthorityMirrors(AuthorityMirrorCriteria criteria) {
        log.debug("REST request to count AuthorityMirrors by criteria: {}", criteria);
        return ResponseEntity.ok().body(authorityMirrorQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /authority-mirrors/:id} : get the "id" authorityMirror.
     *
     * @param id the id of the authorityMirrorDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the authorityMirrorDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/authority-mirrors/{id}")
    public ResponseEntity<AuthorityMirrorDTO> getAuthorityMirror(@PathVariable Long id) {
        log.debug("REST request to get AuthorityMirror : {}", id);
        Optional<AuthorityMirrorDTO> authorityMirrorDTO = authorityMirrorService.findOne(id);
        return ResponseUtil.wrapOrNotFound(authorityMirrorDTO);
    }

    /**
     * {@code DELETE  /authority-mirrors/:id} : delete the "id" authorityMirror.
     *
     * @param id the id of the authorityMirrorDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/authority-mirrors/{id}")
    public ResponseEntity<Void> deleteAuthorityMirror(@PathVariable Long id) {
        log.debug("REST request to delete AuthorityMirror : {}", id);
        authorityMirrorService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
