package sn.free.commissioning.web.rest;

import com.opencsv.CSVReader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sn.free.commissioning.config.Constants;
import sn.free.commissioning.domain.BlacklistResponse;
import sn.free.commissioning.domain.FileUploadResponse;
import sn.free.commissioning.domain.enumeration.UploadStatus;
import sn.free.commissioning.repository.BlacklistedRepository;
import sn.free.commissioning.service.BlacklistedQueryService;
import sn.free.commissioning.service.BlacklistedService;
import sn.free.commissioning.service.criteria.BlacklistedCriteria;
import sn.free.commissioning.service.dto.BlacklistedDTO;
import sn.free.commissioning.service.dto.CommissioningPlanDTO;
import sn.free.commissioning.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link sn.free.commissioning.domain.Blacklisted}.
 */
@RestController
@RequestMapping("/api")
public class BlacklistedResource {

    private final Logger log = LoggerFactory.getLogger(BlacklistedResource.class);

    private static final String ENTITY_NAME = "blacklisted";
    private static final String CLASS_NAME = String.valueOf(BlacklistedResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BlacklistedService blacklistedService;

    private final BlacklistedRepository blacklistedRepository;

    private final BlacklistedQueryService blacklistedQueryService;

    private List<String[]> fileData = new ArrayList<>();

    public BlacklistedResource(
        BlacklistedService blacklistedService,
        BlacklistedRepository blacklistedRepository,
        BlacklistedQueryService blacklistedQueryService
    ) {
        this.blacklistedService = blacklistedService;
        this.blacklistedRepository = blacklistedRepository;
        this.blacklistedQueryService = blacklistedQueryService;
    }

    /**
     * {@code POST  /blacklists/upload-partners-to-block} : Upload a CSV file that contains the list of partners to block msisdn.
     *
     * @param file the file uploaded.
     * @return the {@link ResponseEntity} with status {@code 200} and with body a message, or with status {@code 400 (Bad Request)} if the file structure is incorrect.
     */
    @PostMapping("/blacklists/upload-partners-to-block")
    public ResponseEntity<FileUploadResponse> uploadCSVPartnersToBlock(
        @RequestParam("file") MultipartFile file,
        HttpServletRequest request
    ) {
        FileUploadResponse response = new FileUploadResponse();
        Constants.logInfo(true, CLASS_NAME, "Upload File CSV", " REST request to uploadFileCSV", request);

        if (!file.isEmpty()) {
            // parse CSV file to create a list of `User` objects
            try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
                try (CSVReader csvReader = new CSVReader(reader)) {
                    String[] line;
                    // Exclude the first line (wich is the header) and check number of columns
                    if ((line = csvReader.readNext()) != null && line.length == 2) {
                        // If there is no header
                        if (!line[0].equalsIgnoreCase("Msisdn") && !line[0].equalsIgnoreCase("Numéro")) {
                            fileData.add(line);
                        }
                        while ((line = csvReader.readNext()) != null) {
                            log.info("********************************** {}", Arrays.toString(line));
                            fileData.add(line);
                        }
                    } else {
                        response.setStatus(UploadStatus.FAILURE);
                        response.setMessage("Veuillez vérifier la structure du fichier (CSV séparation ',' attendu)");
                        Constants.logInfo(
                            true,
                            String.valueOf(BlacklistedResource.class),
                            "Uploader un fichier CSV",
                            " Format fichier incorrect : " + " Veuillez vérifier la structure du fichier (CSV séparation ',' attendu)",
                            request
                        );
                        return ResponseEntity.badRequest().body(response);
                    }
                }
                response.setStatus(UploadStatus.SUCCESS);
                response.setMessage("Fichier reçu");
                Constants.logInfo(true, CLASS_NAME, "Uploader un fichier CSV", " Fichier bien uploader", request);
            } catch (Exception ex) {
                response.setStatus(UploadStatus.FAILURE);
                response.setMessage("Une erreur est survenue durant le traitement du fichier CSV. message: " + ex.getMessage());
                Constants.logInfo(
                    true,
                    String.valueOf(BlacklistedResource.class),
                    "Uploader un fichier CSV",
                    " Une erreur est survenue durant le traitement du fichier CSV. message ",
                    request
                );
                return ResponseEntity.badRequest().body(response);
            }
        } else {
            response.setStatus(UploadStatus.FAILURE);
            response.setMessage("No file received");
            Constants.logInfo(true, CLASS_NAME, "Uploader un fichier CSV", " Fichier non recu ", request);
            return ResponseEntity.badRequest().body(response);
        }
        return ResponseEntity.ok().body(response);
    }

    /**
     * {@code POST  /blacklists/block-list} : send the commission plans list to block.
     *
     * @param commissioningPlanDTOList the list of commission plans to create.
     * @return the {@link ResponseEntity} with status {@code 200} and with body a message.
     */
    @PostMapping("/blacklists/block-list")
    public ResponseEntity<BlacklistResponse> commissionPlansToBlock(
        @RequestBody List<CommissioningPlanDTO> commissioningPlanDTOList,
        HttpServletRequest request
    ) {
        Constants.logInfo(
            true,
            CLASS_NAME,
            "REST request to upload commissionPlans to block",
            " REST request to upload commissionPlans to block",
            request
        );

        BlacklistResponse response = new BlacklistResponse();
        HashMap<String, String> partnerReason = new HashMap<>();
        fileData.forEach(line -> {
            partnerReason.put(line[0], line[1]);
        });
        List<BlacklistedDTO> result = blacklistedService.create(
            partnerReason,
            commissioningPlanDTOList.stream().map(CommissioningPlanDTO::getId).collect(Collectors.toList())
        );
        if (result == null) {
            response.setStatus(UploadStatus.FAILURE);
            response.setMessage("Aucun partenaire trouvé.");
            Constants.logInfo(true, CLASS_NAME, "Aucun partenaire trouvé", " Aucun partenaire trouvé", request);
        } else if (result.isEmpty()) {
            response.setStatus(UploadStatus.FAILURE);
            response.setMessage("Cette configuration a déjà été enregistrée.");
            Constants.logInfo(
                true,
                CLASS_NAME,
                "Cette configuration a déjà été enregistrée.",
                " Cette configuration a déjà été enregistrée.",
                request
            );
        } else {
            response.setContent(result);
            response.setStatus(UploadStatus.SUCCESS);
            response.setMessage("Les partenaires ont été bloqués.");
            Constants.logInfo(true, CLASS_NAME, "Les partenaires ont été bloqués.", " Les partenaires ont été bloqués.", request);
        }
        return ResponseEntity.ok().body(response);
    }

    /**
     * {@code POST  /blacklists/block-one?msisdn} : send the commission plans list to block and the partner's msisdn.
     *
     * @param commissioningPlanDTOList the list of commission plans to create.
     * @return the {@link ResponseEntity} with status {@code 200} and with body a message.
     */
    @PostMapping("/blacklists/block-one")
    public ResponseEntity<BlacklistResponse> commissionPlansToBlockAndMsisdn(
        @RequestBody List<CommissioningPlanDTO> commissioningPlanDTOList,
        @RequestParam String msisdn,
        @RequestParam String reason,
        HttpServletRequest request
    ) {
        BlacklistResponse response = new BlacklistResponse();
        HashMap<String, String> partnerReason = new HashMap<>();
        partnerReason.put(msisdn, reason);
        List<BlacklistedDTO> result = blacklistedService.create(
            partnerReason,
            commissioningPlanDTOList.stream().map(CommissioningPlanDTO::getId).collect(Collectors.toList())
        );
        if (result != null) {
            response.setContent(result);
            response.setStatus(UploadStatus.SUCCESS);
            response.setMessage("Partenaire bloqué avec succès!");
            Constants.logInfo(true, CLASS_NAME, "Les partenaires ont été bloqués.", " Les partenaires ont été bloqués.", request);
        } else {
            response.setStatus(UploadStatus.FAILURE);
            response.setMessage("Aucun partenaire trouvé.");

            Constants.logInfo(
                true,
                String.valueOf(BlacklistedResource.class),
                "Aucun partenaire trouvé.",
                " Aucun partenaire trouvé.",
                request
            );
        }
        return ResponseEntity.ok().body(response);
    }

    /**
     * {@code POST  /blacklisteds} : Create a new blacklisted.
     *
     * @param blacklistedDTO the blacklistedDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new blacklistedDTO, or with status {@code 400 (Bad Request)} if the blacklisted has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/blacklisteds")
    public ResponseEntity<BlacklistedDTO> createBlacklisted(@Valid @RequestBody BlacklistedDTO blacklistedDTO, HttpServletRequest request)
        throws URISyntaxException {
        Constants.logInfo(true, CLASS_NAME, "REST request to save Blacklisted : {}", " REST request to save Blacklisted.", request);

        if (blacklistedDTO.getId() != null) {
            throw new BadRequestAlertException("A new blacklisted cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BlacklistedDTO result = blacklistedService.save(blacklistedDTO);
        return ResponseEntity
            .created(new URI("/api/blacklisteds/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code POST  /blacklists/unblock-one/?msisdn} : Unblock a partner for a list of blocked commission plans.
     *
     * @param commissioningPlans the commissioningPlanDTO to update.
     * @param msisdn the commissioningPlanDTO to update.
     */
    @PostMapping("/blacklists/unblock-one")
    public ResponseEntity<BlacklistResponse> unblockOnePartner(
        @RequestBody List<CommissioningPlanDTO> commissioningPlans,
        @RequestParam String msisdn,
        HttpServletRequest request
    ) {
        log.info("REST request to unblock the partner of msisdn: {}  and commissioningPlans : {}", msisdn, commissioningPlans);
        Constants.logInfo(
            true,
            CLASS_NAME,
            "REST request to unblock the partner of msisdn and commissioningPlans",
            " REST request to unblock the partner of msisdn and commissioningPlans.",
            request
        );

        BlacklistResponse response = new BlacklistResponse();
        List<Long> result = blacklistedService.unblockAListOfCommissioningPlansForPartner(msisdn, commissioningPlans);
        if (!result.isEmpty()) {
            response.setStatus(UploadStatus.SUCCESS);
            response.setMessage("Partenaire débloqué avec succès!");

            Constants.logInfo(true, CLASS_NAME, "Partenaire débloqué avec succès!", " Partenaire débloqué avec succès!", request);
        } else {
            response.setStatus(UploadStatus.FAILURE);
            response.setMessage("Une erreur est survenue, veuillez réesssayer!");

            Constants.logInfo(
                true,
                CLASS_NAME,
                "Une erreur est survenue, veuillez réesssayer!",
                " Une erreur est survenue, veuillez réesssayer!",
                request
            );
        }
        return ResponseEntity.ok().body(response);
    }

    /**
     * {@code PUT  /blacklisteds/:id} : Updates an existing blacklisted.
     *
     * @param id the id of the blacklistedDTO to save.
     * @param blacklistedDTO the blacklistedDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated blacklistedDTO,
     * or with status {@code 400 (Bad Request)} if the blacklistedDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the blacklistedDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/blacklisteds/{id}")
    public ResponseEntity<BlacklistedDTO> updateBlacklisted(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody BlacklistedDTO blacklistedDTO
    ) throws URISyntaxException {
        if (blacklistedDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, blacklistedDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!blacklistedRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        BlacklistedDTO result = blacklistedService.update(blacklistedDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, blacklistedDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /blacklisteds/:id} : Partial updates given fields of an existing blacklisted, field will ignore if it is null
     *
     * @param id the id of the blacklistedDTO to save.
     * @param blacklistedDTO the blacklistedDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated blacklistedDTO,
     * or with status {@code 400 (Bad Request)} if the blacklistedDTO is not valid,
     * or with status {@code 404 (Not Found)} if the blacklistedDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the blacklistedDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/blacklisteds/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<BlacklistedDTO> partialUpdateBlacklisted(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody BlacklistedDTO blacklistedDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Blacklisted partially : {}, {}", id, blacklistedDTO);
        if (blacklistedDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, blacklistedDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!blacklistedRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<BlacklistedDTO> result = blacklistedService.partialUpdate(blacklistedDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, blacklistedDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /blacklisteds} : get all the blacklisteds.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of blacklisteds in body.
     */
    @GetMapping("/blacklisteds")
    public ResponseEntity<List<BlacklistedDTO>> getAllBlacklisteds(
        BlacklistedCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        HttpServletRequest request
    ) {
        log.debug("REST request to get Blacklisteds by criteria: {}", criteria);
        Page<BlacklistedDTO> page = blacklistedQueryService.findByCriteria(criteria, pageable);
        Constants.logInfo(
            true,
            CLASS_NAME,
            "Liste des blacklisteds commissions",
            "Liste des blacklisteds, total : " +
            page.getTotalElements() +
            " - taille de la page : " +
            page.getSize() +
            " - numéro de la page : " +
            page.getNumber(),
            request
        );
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /blacklisteds/count} : count all the blacklisteds.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/blacklisteds/count")
    public ResponseEntity<Long> countBlacklisteds(BlacklistedCriteria criteria) {
        log.debug("REST request to count Blacklisteds by criteria: {}", criteria);
        return ResponseEntity.ok().body(blacklistedQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /blacklisteds/:id} : get the "id" blacklisted.
     *
     * @param id the id of the blacklistedDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the blacklistedDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/blacklisteds/{id}")
    public ResponseEntity<BlacklistedDTO> getBlacklisted(@PathVariable Long id, HttpServletRequest request) {
        Optional<BlacklistedDTO> blacklistedDTO = blacklistedService.findOne(id);
        Constants.logInfo(
            blacklistedDTO.isPresent(),
            CLASS_NAME,
            "REST request to get Blacklisted ",
            "REST request to get Blacklisted ",
            request
        );
        return ResponseUtil.wrapOrNotFound(blacklistedDTO);
    }

    /**
     * {@code DELETE  /blacklisteds/:id} : delete the "id" blacklisted.
     *
     * @param id the id of the blacklistedDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/blacklisteds/{id}")
    public ResponseEntity<Void> deleteBlacklisted(@PathVariable Long id, HttpServletRequest request) {
        Constants.logInfo(true, CLASS_NAME, "REST request to delete Blacklisted ", "REST request to delete Blacklisted ", request);
        blacklistedService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
