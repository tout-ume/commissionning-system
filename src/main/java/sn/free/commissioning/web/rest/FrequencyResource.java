package sn.free.commissioning.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sn.free.commissioning.calculus_engine.service.DeferredCalculusService;
import sn.free.commissioning.config.Constants;
import sn.free.commissioning.domain.Frequency;
import sn.free.commissioning.repository.FrequencyRepository;
import sn.free.commissioning.service.FrequencyQueryService;
import sn.free.commissioning.service.FrequencyService;
import sn.free.commissioning.service.criteria.FrequencyCriteria;
import sn.free.commissioning.service.dto.FrequencyDTO;
import sn.free.commissioning.service.mapper.FrequencyMapper;
import sn.free.commissioning.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link sn.free.commissioning.domain.Frequency}.
 */
@RestController
@RequestMapping("/api")
public class FrequencyResource {

    private final Logger log = LoggerFactory.getLogger(FrequencyResource.class);

    private static final String ENTITY_NAME = "frequency";
    private static final String CLASS_NAME = String.valueOf(FrequencyResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FrequencyService frequencyService;

    private final FrequencyRepository frequencyRepository;

    private final FrequencyQueryService frequencyQueryService;
    private final FrequencyMapper frequencyMapper;

    private final DeferredCalculusService deferredCalculusService;

    public FrequencyResource(
        FrequencyService frequencyService,
        FrequencyRepository frequencyRepository,
        FrequencyQueryService frequencyQueryService,
        FrequencyMapper frequencyMapper,
        DeferredCalculusService deferredCalculusService
    ) {
        this.frequencyService = frequencyService;
        this.frequencyRepository = frequencyRepository;
        this.frequencyQueryService = frequencyQueryService;
        this.frequencyMapper = frequencyMapper;
        this.deferredCalculusService = deferredCalculusService;
    }

    /**
     * {@code POST  /frequencies} : Create a new frequency.
     *
     * @param frequencyDTO the frequencyDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new frequencyDTO, or with status {@code 400 (Bad Request)} if the frequency has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/frequencies")
    public ResponseEntity<FrequencyDTO> createFrequency(@Valid @RequestBody FrequencyDTO frequencyDTO, HttpServletRequest request)
        throws URISyntaxException {
        log.info("REST request to save Frequency : {}", frequencyDTO);

        if (frequencyDTO.getId() != null) {
            throw new BadRequestAlertException("A new frequency cannot already have an ID", ENTITY_NAME, "idexists");
        }
        // Check if the same frequency exists
        if (frequencyService.findByNameAndOperation(frequencyDTO.getName(), frequencyDTO.getOperationType()).isPresent()) {
            throw new BadRequestAlertException("This frequency already exists!", ENTITY_NAME, "frequencyexists");
        }
        FrequencyDTO result = frequencyService.save(frequencyDTO);
        Constants.logInfo(
            result != null,
            CLASS_NAME,
            "REST request to save Frequency.",
            result != null ? " Frequency crée avec succes avec comme nom: " + result.getName() : "Échec",
            request
        );

        return ResponseEntity
            .created(new URI("/api/frequencies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /frequencies/:id} : Updates an existing frequency.
     *
     * @param id the id of the frequencyDTO to save.
     * @param frequencyDTO the frequencyDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated frequencyDTO,
     * or with status {@code 400 (Bad Request)} if the frequencyDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the frequencyDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/frequencies/{id}")
    public ResponseEntity<FrequencyDTO> updateFrequency(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody FrequencyDTO frequencyDTO,
        HttpServletRequest request
    ) throws URISyntaxException {
        log.info("REST request to update Frequency : {}, {}", id, frequencyDTO);
        if (frequencyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, frequencyDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!frequencyRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        FrequencyDTO result = frequencyService.update(frequencyDTO);
        Constants.logInfo(
            result != null,
            CLASS_NAME,
            "REST request to update Frequency .",
            result != null ? " REST request to update Frequency : " + result.getName() : "Échec",
            request
        );
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, frequencyDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /frequencies/:id} : Partial updates given fields of an existing frequency, field will ignore if it is null
     *
     * @param id the id of the frequencyDTO to save.
     * @param frequencyDTO the frequencyDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated frequencyDTO,
     * or with status {@code 400 (Bad Request)} if the frequencyDTO is not valid,
     * or with status {@code 404 (Not Found)} if the frequencyDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the frequencyDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/frequencies/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<FrequencyDTO> partialUpdateFrequency(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody FrequencyDTO frequencyDTO
    ) throws URISyntaxException {
        log.info("REST request to partial update Frequency partially : {}, {}", id, frequencyDTO);
        if (frequencyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, frequencyDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!frequencyRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<FrequencyDTO> result = frequencyService.partialUpdate(frequencyDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, frequencyDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /frequencies} : get all the frequencies.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of frequencies in body.
     */
    @GetMapping("/frequencies")
    public ResponseEntity<List<FrequencyDTO>> getAllFrequencies(
        FrequencyCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        HttpServletRequest request
    ) {
        log.info("REST request to get Frequencies by criteria: {}", criteria);
        Page<FrequencyDTO> page = frequencyQueryService.findByCriteria(criteria, pageable);
        Constants.logInfo(
            true,
            CLASS_NAME,
            "REST request to get all Frequencies",
            "REST request to get all Frequencies, total : " +
            page.getTotalElements() +
            " - taille de la page : " +
            page.getSize() +
            " - numéro de la page : " +
            page.getNumber(),
            request
        );
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /frequencies/count} : count all the frequencies.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/frequencies/count")
    public ResponseEntity<Long> countFrequencies(FrequencyCriteria criteria) {
        log.info("REST request to count Frequencies by criteria: {}", criteria);
        return ResponseEntity.ok().body(frequencyQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /frequencies/:id} : get the "id" frequency.
     *
     * @param id the id of the frequencyDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the frequencyDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/frequencies/{id}")
    public ResponseEntity<FrequencyDTO> getFrequency(@PathVariable Long id, HttpServletRequest request) {
        log.info("REST request to get Frequency : {}", id);
        Optional<FrequencyDTO> frequencyDTO = frequencyService.findOne(id);
        Constants.logInfo(
            frequencyDTO.isPresent(),
            CLASS_NAME,
            " REST request to get Frequency.",
            "REST request to get Frequency.",
            request
        );

        return ResponseUtil.wrapOrNotFound(frequencyDTO);
    }

    /**
     * {@code GET  /frequencies/:id} : get the "id" frequency.
     *
     * @param id the id of the frequencyDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the frequencyDTO, or with status {@code 404 (Not Found)}.
     */
    @PostMapping("/frequencies/calculDiffere/{id}")
    public ResponseEntity<FrequencyDTO> calculateCommissionFrequency(@PathVariable Long id) {
        log.info("REST request to get Frequency : {}", id);
        Frequency frequency = frequencyRepository.findById(id).get();
        deferredCalculusService.calculate(frequency);
        FrequencyDTO frequencyDTO = frequencyMapper.toDto(frequency);
        return ResponseEntity.ok().body(frequencyDTO);
    }

    /**
     * {@code DELETE  /frequencies/:id} : delete the "id" frequency.
     *
     * @param id the id of the frequencyDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/frequencies/{id}")
    public ResponseEntity<Void> deleteFrequency(@PathVariable Long id, HttpServletRequest request) {
        log.info("REST request to delete Frequency : {}", id);
        Constants.logInfo(true, CLASS_NAME, " REST request to delete Frequency.", " REST request to delete Frequency.", request);

        frequencyService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
