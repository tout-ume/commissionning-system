package sn.free.commissioning.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sn.free.commissioning.config.Constants;
import sn.free.commissioning.repository.TerritoryRepository;
import sn.free.commissioning.service.TerritoryQueryService;
import sn.free.commissioning.service.TerritoryService;
import sn.free.commissioning.service.criteria.TerritoryCriteria;
import sn.free.commissioning.service.dto.TerritoryDTO;
import sn.free.commissioning.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link sn.free.commissioning.domain.Territory}.
 */
@RestController
@RequestMapping("/api")
public class TerritoryResource {

    private final Logger log = LoggerFactory.getLogger(TerritoryResource.class);

    private static final String ENTITY_NAME = "territory";
    private static final String CLASS_NAME = String.valueOf(TerritoryResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TerritoryService territoryService;

    private final TerritoryRepository territoryRepository;

    private final TerritoryQueryService territoryQueryService;

    public TerritoryResource(
        TerritoryService territoryService,
        TerritoryRepository territoryRepository,
        TerritoryQueryService territoryQueryService
    ) {
        this.territoryService = territoryService;
        this.territoryRepository = territoryRepository;
        this.territoryQueryService = territoryQueryService;
    }

    /**
     * {@code POST  /territories} : Create a new territory.
     *
     * @param territoryDTO the territoryDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new territoryDTO, or with status {@code 400 (Bad Request)} if the territory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/territories")
    public ResponseEntity<TerritoryDTO> createTerritory(@Valid @RequestBody TerritoryDTO territoryDTO, HttpServletRequest request)
        throws URISyntaxException {
        log.debug("REST request to save Territory : {}", territoryDTO);
        if (territoryDTO.getId() != null) {
            throw new BadRequestAlertException("A new territory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TerritoryDTO result = territoryService.save(territoryDTO);
        Constants.logInfo(true, CLASS_NAME, "REST request to save Territory ", "REST request to save Territory ", request);

        return ResponseEntity
            .created(new URI("/api/territories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /territories/:id} : Updates an existing territory.
     *
     * @param id the id of the territoryDTO to save.
     * @param territoryDTO the territoryDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated territoryDTO,
     * or with status {@code 400 (Bad Request)} if the territoryDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the territoryDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/territories/{id}")
    public ResponseEntity<TerritoryDTO> updateTerritory(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody TerritoryDTO territoryDTO,
        HttpServletRequest request
    ) throws URISyntaxException {
        log.debug("REST request to update Territory : {}, {}", id, territoryDTO);
        if (territoryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, territoryDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!territoryRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        TerritoryDTO result = territoryService.update(territoryDTO);
        Constants.logInfo(
            result != null,
            CLASS_NAME,
            "REST request to update Taux.",
            result != null ? " REST request to update Taux avec succes : " + result.getId() : "Échec",
            request
        );

        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, territoryDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /territories/:id} : Partial updates given fields of an existing territory, field will ignore if it is null
     *
     * @param id the id of the territoryDTO to save.
     * @param territoryDTO the territoryDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated territoryDTO,
     * or with status {@code 400 (Bad Request)} if the territoryDTO is not valid,
     * or with status {@code 404 (Not Found)} if the territoryDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the territoryDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/territories/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<TerritoryDTO> partialUpdateTerritory(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody TerritoryDTO territoryDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Territory partially : {}, {}", id, territoryDTO);
        if (territoryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, territoryDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!territoryRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<TerritoryDTO> result = territoryService.partialUpdate(territoryDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, territoryDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /territories} : get all the territories.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of territories in body.
     */
    @GetMapping("/territories")
    public ResponseEntity<List<TerritoryDTO>> getAllTerritories(
        TerritoryCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        HttpServletRequest request
    ) {
        log.debug("REST request to get Territories by criteria: {}", criteria);
        Page<TerritoryDTO> page = territoryQueryService.findByCriteria(criteria, pageable);
        Constants.logInfo(
            true,
            CLASS_NAME,
            "REST request to get all Territories",
            "REST request to get all Territories, total : " +
            page.getTotalElements() +
            " - taille de la page : " +
            page.getSize() +
            " - numéro de la page : " +
            page.getNumber(),
            request
        );
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /territories/count} : count all the territories.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/territories/count")
    public ResponseEntity<Long> countTerritories(TerritoryCriteria criteria) {
        log.debug("REST request to count Territories by criteria: {}", criteria);
        return ResponseEntity.ok().body(territoryQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /territories/:id} : get the "id" territory.
     *
     * @param id the id of the territoryDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the territoryDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/territories/{id}")
    public ResponseEntity<TerritoryDTO> getTerritory(@PathVariable Long id, HttpServletRequest request) {
        log.debug("REST request to get Territory : {}", id);
        Optional<TerritoryDTO> territoryDTO = territoryService.findOne(id);
        Constants.logInfo(
            territoryDTO.isPresent(),
            CLASS_NAME,
            " REST request to get a Territory ",
            territoryDTO.map(dto -> "REST request to get a Territory with ID : " + dto.getId()).orElse("ÉCHEC"),
            request
        );
        return ResponseUtil.wrapOrNotFound(territoryDTO);
    }

    /**
     * {@code DELETE  /territories/:id} : delete the "id" territory.
     *
     * @param id the id of the territoryDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/territories/{id}")
    public ResponseEntity<Void> deleteTerritory(@PathVariable Long id, HttpServletRequest request) {
        log.debug("REST request to delete Territory : {}", id);
        Constants.logInfo(
            true,
            CLASS_NAME,
            " REST request  to delete Territory  ",
            "REST request to delete Territory  by id" + id,
            request
        );
        territoryService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
