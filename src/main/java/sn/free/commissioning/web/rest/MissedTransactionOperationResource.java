package sn.free.commissioning.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sn.free.commissioning.repository.MissedTransactionOperationRepository;
import sn.free.commissioning.service.MissedTransactionOperationQueryService;
import sn.free.commissioning.service.MissedTransactionOperationService;
import sn.free.commissioning.service.criteria.MissedTransactionOperationCriteria;
import sn.free.commissioning.service.dto.MissedTransactionOperationDTO;
import sn.free.commissioning.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link sn.free.commissioning.domain.MissedTransactionOperation}.
 */
@RestController
@RequestMapping("/api")
public class MissedTransactionOperationResource {

    private final Logger log = LoggerFactory.getLogger(MissedTransactionOperationResource.class);

    private static final String ENTITY_NAME = "missedTransactionOperation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MissedTransactionOperationService missedTransactionOperationService;

    private final MissedTransactionOperationRepository missedTransactionOperationRepository;

    private final MissedTransactionOperationQueryService missedTransactionOperationQueryService;

    public MissedTransactionOperationResource(
        MissedTransactionOperationService missedTransactionOperationService,
        MissedTransactionOperationRepository missedTransactionOperationRepository,
        MissedTransactionOperationQueryService missedTransactionOperationQueryService
    ) {
        this.missedTransactionOperationService = missedTransactionOperationService;
        this.missedTransactionOperationRepository = missedTransactionOperationRepository;
        this.missedTransactionOperationQueryService = missedTransactionOperationQueryService;
    }

    /**
     * {@code POST  /missed-transaction-operations} : Create a new missedTransactionOperation.
     *
     * @param missedTransactionOperationDTO the missedTransactionOperationDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new missedTransactionOperationDTO, or with status {@code 400 (Bad Request)} if the missedTransactionOperation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/missed-transaction-operations")
    public ResponseEntity<MissedTransactionOperationDTO> createMissedTransactionOperation(
        @RequestBody MissedTransactionOperationDTO missedTransactionOperationDTO
    ) throws URISyntaxException {
        log.debug("REST request to save MissedTransactionOperation : {}", missedTransactionOperationDTO);
        if (missedTransactionOperationDTO.getId() != null) {
            throw new BadRequestAlertException("A new missedTransactionOperation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MissedTransactionOperationDTO result = missedTransactionOperationService.save(missedTransactionOperationDTO);
        return ResponseEntity
            .created(new URI("/api/missed-transaction-operations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /missed-transaction-operations/:id} : Updates an existing missedTransactionOperation.
     *
     * @param id the id of the missedTransactionOperationDTO to save.
     * @param missedTransactionOperationDTO the missedTransactionOperationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated missedTransactionOperationDTO,
     * or with status {@code 400 (Bad Request)} if the missedTransactionOperationDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the missedTransactionOperationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/missed-transaction-operations/{id}")
    public ResponseEntity<MissedTransactionOperationDTO> updateMissedTransactionOperation(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody MissedTransactionOperationDTO missedTransactionOperationDTO
    ) throws URISyntaxException {
        log.debug("REST request to update MissedTransactionOperation : {}, {}", id, missedTransactionOperationDTO);
        if (missedTransactionOperationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, missedTransactionOperationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!missedTransactionOperationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        MissedTransactionOperationDTO result = missedTransactionOperationService.update(missedTransactionOperationDTO);
        return ResponseEntity
            .ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, missedTransactionOperationDTO.getId().toString())
            )
            .body(result);
    }

    /**
     * {@code PATCH  /missed-transaction-operations/:id} : Partial updates given fields of an existing missedTransactionOperation, field will ignore if it is null
     *
     * @param id the id of the missedTransactionOperationDTO to save.
     * @param missedTransactionOperationDTO the missedTransactionOperationDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated missedTransactionOperationDTO,
     * or with status {@code 400 (Bad Request)} if the missedTransactionOperationDTO is not valid,
     * or with status {@code 404 (Not Found)} if the missedTransactionOperationDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the missedTransactionOperationDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/missed-transaction-operations/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<MissedTransactionOperationDTO> partialUpdateMissedTransactionOperation(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody MissedTransactionOperationDTO missedTransactionOperationDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update MissedTransactionOperation partially : {}, {}", id, missedTransactionOperationDTO);
        if (missedTransactionOperationDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, missedTransactionOperationDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!missedTransactionOperationRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<MissedTransactionOperationDTO> result = missedTransactionOperationService.partialUpdate(missedTransactionOperationDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, missedTransactionOperationDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /missed-transaction-operations} : get all the missedTransactionOperations.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of missedTransactionOperations in body.
     */
    @GetMapping("/missed-transaction-operations")
    public ResponseEntity<List<MissedTransactionOperationDTO>> getAllMissedTransactionOperations(
        MissedTransactionOperationCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get MissedTransactionOperations by criteria: {}", criteria);
        Page<MissedTransactionOperationDTO> page = missedTransactionOperationQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /missed-transaction-operations/count} : count all the missedTransactionOperations.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/missed-transaction-operations/count")
    public ResponseEntity<Long> countMissedTransactionOperations(MissedTransactionOperationCriteria criteria) {
        log.debug("REST request to count MissedTransactionOperations by criteria: {}", criteria);
        return ResponseEntity.ok().body(missedTransactionOperationQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /missed-transaction-operations/:id} : get the "id" missedTransactionOperation.
     *
     * @param id the id of the missedTransactionOperationDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the missedTransactionOperationDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/missed-transaction-operations/{id}")
    public ResponseEntity<MissedTransactionOperationDTO> getMissedTransactionOperation(@PathVariable Long id) {
        log.debug("REST request to get MissedTransactionOperation : {}", id);
        Optional<MissedTransactionOperationDTO> missedTransactionOperationDTO = missedTransactionOperationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(missedTransactionOperationDTO);
    }

    /**
     * {@code DELETE  /missed-transaction-operations/:id} : delete the "id" missedTransactionOperation.
     *
     * @param id the id of the missedTransactionOperationDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/missed-transaction-operations/{id}")
    public ResponseEntity<Void> deleteMissedTransactionOperation(@PathVariable Long id) {
        log.debug("REST request to delete MissedTransactionOperation : {}", id);
        missedTransactionOperationService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
