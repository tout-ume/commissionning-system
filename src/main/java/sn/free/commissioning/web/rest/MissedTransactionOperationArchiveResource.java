package sn.free.commissioning.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sn.free.commissioning.repository.MissedTransactionOperationArchiveRepository;
import sn.free.commissioning.service.MissedTransactionOperationArchiveQueryService;
import sn.free.commissioning.service.MissedTransactionOperationArchiveService;
import sn.free.commissioning.service.criteria.MissedTransactionOperationArchiveCriteria;
import sn.free.commissioning.service.dto.MissedTransactionOperationArchiveDTO;
import sn.free.commissioning.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link sn.free.commissioning.domain.MissedTransactionOperationArchive}.
 */
@RestController
@RequestMapping("/api")
public class MissedTransactionOperationArchiveResource {

    private final Logger log = LoggerFactory.getLogger(MissedTransactionOperationArchiveResource.class);

    private static final String ENTITY_NAME = "missedTransactionOperationArchive";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MissedTransactionOperationArchiveService missedTransactionOperationArchiveService;

    private final MissedTransactionOperationArchiveRepository missedTransactionOperationArchiveRepository;

    private final MissedTransactionOperationArchiveQueryService missedTransactionOperationArchiveQueryService;

    public MissedTransactionOperationArchiveResource(
        MissedTransactionOperationArchiveService missedTransactionOperationArchiveService,
        MissedTransactionOperationArchiveRepository missedTransactionOperationArchiveRepository,
        MissedTransactionOperationArchiveQueryService missedTransactionOperationArchiveQueryService
    ) {
        this.missedTransactionOperationArchiveService = missedTransactionOperationArchiveService;
        this.missedTransactionOperationArchiveRepository = missedTransactionOperationArchiveRepository;
        this.missedTransactionOperationArchiveQueryService = missedTransactionOperationArchiveQueryService;
    }

    /**
     * {@code POST  /missed-transaction-operation-archives} : Create a new missedTransactionOperationArchive.
     *
     * @param missedTransactionOperationArchiveDTO the missedTransactionOperationArchiveDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new missedTransactionOperationArchiveDTO, or with status {@code 400 (Bad Request)} if the missedTransactionOperationArchive has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/missed-transaction-operation-archives")
    public ResponseEntity<MissedTransactionOperationArchiveDTO> createMissedTransactionOperationArchive(
        @RequestBody MissedTransactionOperationArchiveDTO missedTransactionOperationArchiveDTO
    ) throws URISyntaxException {
        log.debug("REST request to save MissedTransactionOperationArchive : {}", missedTransactionOperationArchiveDTO);
        if (missedTransactionOperationArchiveDTO.getId() != null) {
            throw new BadRequestAlertException(
                "A new missedTransactionOperationArchive cannot already have an ID",
                ENTITY_NAME,
                "idexists"
            );
        }
        MissedTransactionOperationArchiveDTO result = missedTransactionOperationArchiveService.save(missedTransactionOperationArchiveDTO);
        return ResponseEntity
            .created(new URI("/api/missed-transaction-operation-archives/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /missed-transaction-operation-archives/:id} : Updates an existing missedTransactionOperationArchive.
     *
     * @param id the id of the missedTransactionOperationArchiveDTO to save.
     * @param missedTransactionOperationArchiveDTO the missedTransactionOperationArchiveDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated missedTransactionOperationArchiveDTO,
     * or with status {@code 400 (Bad Request)} if the missedTransactionOperationArchiveDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the missedTransactionOperationArchiveDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/missed-transaction-operation-archives/{id}")
    public ResponseEntity<MissedTransactionOperationArchiveDTO> updateMissedTransactionOperationArchive(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody MissedTransactionOperationArchiveDTO missedTransactionOperationArchiveDTO
    ) throws URISyntaxException {
        log.debug("REST request to update MissedTransactionOperationArchive : {}, {}", id, missedTransactionOperationArchiveDTO);
        if (missedTransactionOperationArchiveDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, missedTransactionOperationArchiveDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!missedTransactionOperationArchiveRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        MissedTransactionOperationArchiveDTO result = missedTransactionOperationArchiveService.update(missedTransactionOperationArchiveDTO);
        return ResponseEntity
            .ok()
            .headers(
                HeaderUtil.createEntityUpdateAlert(
                    applicationName,
                    false,
                    ENTITY_NAME,
                    missedTransactionOperationArchiveDTO.getId().toString()
                )
            )
            .body(result);
    }

    /**
     * {@code PATCH  /missed-transaction-operation-archives/:id} : Partial updates given fields of an existing missedTransactionOperationArchive, field will ignore if it is null
     *
     * @param id the id of the missedTransactionOperationArchiveDTO to save.
     * @param missedTransactionOperationArchiveDTO the missedTransactionOperationArchiveDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated missedTransactionOperationArchiveDTO,
     * or with status {@code 400 (Bad Request)} if the missedTransactionOperationArchiveDTO is not valid,
     * or with status {@code 404 (Not Found)} if the missedTransactionOperationArchiveDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the missedTransactionOperationArchiveDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/missed-transaction-operation-archives/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<MissedTransactionOperationArchiveDTO> partialUpdateMissedTransactionOperationArchive(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody MissedTransactionOperationArchiveDTO missedTransactionOperationArchiveDTO
    ) throws URISyntaxException {
        log.debug(
            "REST request to partial update MissedTransactionOperationArchive partially : {}, {}",
            id,
            missedTransactionOperationArchiveDTO
        );
        if (missedTransactionOperationArchiveDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, missedTransactionOperationArchiveDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!missedTransactionOperationArchiveRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<MissedTransactionOperationArchiveDTO> result = missedTransactionOperationArchiveService.partialUpdate(
            missedTransactionOperationArchiveDTO
        );

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, missedTransactionOperationArchiveDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /missed-transaction-operation-archives} : get all the missedTransactionOperationArchives.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of missedTransactionOperationArchives in body.
     */
    @GetMapping("/missed-transaction-operation-archives")
    public ResponseEntity<List<MissedTransactionOperationArchiveDTO>> getAllMissedTransactionOperationArchives(
        MissedTransactionOperationArchiveCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable
    ) {
        log.debug("REST request to get MissedTransactionOperationArchives by criteria: {}", criteria);
        Page<MissedTransactionOperationArchiveDTO> page = missedTransactionOperationArchiveQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /missed-transaction-operation-archives/count} : count all the missedTransactionOperationArchives.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/missed-transaction-operation-archives/count")
    public ResponseEntity<Long> countMissedTransactionOperationArchives(MissedTransactionOperationArchiveCriteria criteria) {
        log.debug("REST request to count MissedTransactionOperationArchives by criteria: {}", criteria);
        return ResponseEntity.ok().body(missedTransactionOperationArchiveQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /missed-transaction-operation-archives/:id} : get the "id" missedTransactionOperationArchive.
     *
     * @param id the id of the missedTransactionOperationArchiveDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the missedTransactionOperationArchiveDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/missed-transaction-operation-archives/{id}")
    public ResponseEntity<MissedTransactionOperationArchiveDTO> getMissedTransactionOperationArchive(@PathVariable Long id) {
        log.debug("REST request to get MissedTransactionOperationArchive : {}", id);
        Optional<MissedTransactionOperationArchiveDTO> missedTransactionOperationArchiveDTO = missedTransactionOperationArchiveService.findOne(
            id
        );
        return ResponseUtil.wrapOrNotFound(missedTransactionOperationArchiveDTO);
    }

    /**
     * {@code DELETE  /missed-transaction-operation-archives/:id} : delete the "id" missedTransactionOperationArchive.
     *
     * @param id the id of the missedTransactionOperationArchiveDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/missed-transaction-operation-archives/{id}")
    public ResponseEntity<Void> deleteMissedTransactionOperationArchive(@PathVariable Long id) {
        log.debug("REST request to delete MissedTransactionOperationArchive : {}", id);
        missedTransactionOperationArchiveService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
