package sn.free.commissioning.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import sn.free.commissioning.config.Constants;
import sn.free.commissioning.repository.PartnerProfileRepository;
import sn.free.commissioning.service.PartnerProfileQueryService;
import sn.free.commissioning.service.PartnerProfileService;
import sn.free.commissioning.service.criteria.PartnerProfileCriteria;
import sn.free.commissioning.service.dto.PartnerProfileDTO;
import sn.free.commissioning.web.rest.errors.BadRequestAlertException;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link sn.free.commissioning.domain.PartnerProfile}.
 */
@RestController
@RequestMapping("/api")
public class PartnerProfileResource {

    private final Logger log = LoggerFactory.getLogger(PartnerProfileResource.class);

    private static final String ENTITY_NAME = "partnerProfile";
    private static final String CLASS_NAME = String.valueOf(PartnerProfileResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PartnerProfileService partnerProfileService;

    private final PartnerProfileRepository partnerProfileRepository;

    private final PartnerProfileQueryService partnerProfileQueryService;

    public PartnerProfileResource(
        PartnerProfileService partnerProfileService,
        PartnerProfileRepository partnerProfileRepository,
        PartnerProfileQueryService partnerProfileQueryService
    ) {
        this.partnerProfileService = partnerProfileService;
        this.partnerProfileRepository = partnerProfileRepository;
        this.partnerProfileQueryService = partnerProfileQueryService;
    }

    /**
     * {@code POST  /partner-profiles} : Create a new partnerProfile.
     *
     * @param partnerProfileDTO the partnerProfileDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new partnerProfileDTO, or with status {@code 400 (Bad Request)} if the partnerProfile has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/partner-profiles")
    public ResponseEntity<PartnerProfileDTO> createPartnerProfile(
        @Valid @RequestBody PartnerProfileDTO partnerProfileDTO,
        HttpServletRequest request
    ) throws URISyntaxException {
        log.debug("REST request to save PartnerProfile : {}", partnerProfileDTO);

        if (partnerProfileDTO.getId() != null) {
            throw new BadRequestAlertException("A new partnerProfile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PartnerProfileDTO result = partnerProfileService.save(partnerProfileDTO);
        Constants.logInfo(
            result != null,
            CLASS_NAME,
            "REST request to save PartnerProfile.",
            result != null ? " REST request to save PartnerProfile: " + result.getName() : "Échec",
            request
        );
        return ResponseEntity
            .created(new URI("/api/partner-profiles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /partner-profiles/:id} : Updates an existing partnerProfile.
     *
     * @param id the id of the partnerProfileDTO to save.
     * @param partnerProfileDTO the partnerProfileDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated partnerProfileDTO,
     * or with status {@code 400 (Bad Request)} if the partnerProfileDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the partnerProfileDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/partner-profiles/{id}")
    public ResponseEntity<PartnerProfileDTO> updatePartnerProfile(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody PartnerProfileDTO partnerProfileDTO,
        HttpServletRequest request
    ) throws URISyntaxException {
        log.debug("REST request to update PartnerProfile : {}, {}", id, partnerProfileDTO);
        if (partnerProfileDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, partnerProfileDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!partnerProfileRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PartnerProfileDTO result = partnerProfileService.update(partnerProfileDTO);
        Constants.logInfo(
            result != null,
            CLASS_NAME,
            "REST request to update PartnerProfile.",
            result != null ? " REST request to update PartnerProfile: " + result.getName() : "Échec",
            request
        );
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, partnerProfileDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /partner-profiles/:id} : Partial updates given fields of an existing partnerProfile, field will ignore if it is null
     *
     * @param id the id of the partnerProfileDTO to save.
     * @param partnerProfileDTO the partnerProfileDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated partnerProfileDTO,
     * or with status {@code 400 (Bad Request)} if the partnerProfileDTO is not valid,
     * or with status {@code 404 (Not Found)} if the partnerProfileDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the partnerProfileDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/partner-profiles/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<PartnerProfileDTO> partialUpdatePartnerProfile(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody PartnerProfileDTO partnerProfileDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update PartnerProfile partially : {}, {}", id, partnerProfileDTO);
        if (partnerProfileDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, partnerProfileDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!partnerProfileRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PartnerProfileDTO> result = partnerProfileService.partialUpdate(partnerProfileDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, partnerProfileDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /partner-profiles} : get all the partnerProfiles.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of partnerProfiles in body.
     */
    @GetMapping("/partner-profiles")
    public ResponseEntity<List<PartnerProfileDTO>> getAllPartnerProfiles(
        PartnerProfileCriteria criteria,
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        HttpServletRequest request
    ) {
        log.debug("REST request to get PartnerProfiles by criteria: {}", criteria);
        Page<PartnerProfileDTO> page = partnerProfileQueryService.findByCriteria(criteria, pageable);
        Constants.logInfo(
            true,
            CLASS_NAME,
            "REST request to get all PartnerProfiles",
            "REST request to get all PartnerProfiles, total : " +
            page.getTotalElements() +
            " - taille de la page : " +
            page.getSize() +
            " - numéro de la page : " +
            page.getNumber(),
            request
        );
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /partner-profiles/count} : count all the partnerProfiles.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/partner-profiles/count")
    public ResponseEntity<Long> countPartnerProfiles(PartnerProfileCriteria criteria) {
        log.debug("REST request to count PartnerProfiles by criteria: {}", criteria);
        return ResponseEntity.ok().body(partnerProfileQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /partner-profiles/:id} : get the "id" partnerProfile.
     *
     * @param id the id of the partnerProfileDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the partnerProfileDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/partner-profiles/{id}")
    public ResponseEntity<PartnerProfileDTO> getPartnerProfile(@PathVariable Long id, HttpServletRequest request) {
        log.debug("REST request to get PartnerProfile : {}", id);
        Optional<PartnerProfileDTO> partnerProfileDTO = partnerProfileService.findOne(id);
        Constants.logInfo(
            partnerProfileDTO.isPresent(),
            CLASS_NAME,
            " REST request to get a PartnerProfile ",
            partnerProfileDTO.map(dto -> "Partenaire trouvé : " + dto.getName() + dto.getCode()).orElse("ÉCHEC"),
            request
        );

        return ResponseUtil.wrapOrNotFound(partnerProfileDTO);
    }

    /**
     * {@code DELETE  /partner-profiles/:id} : delete the "id" partnerProfile.
     *
     * @param id the id of the partnerProfileDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/partner-profiles/{id}")
    public ResponseEntity<Void> deletePartnerProfile(@PathVariable Long id, HttpServletRequest request) {
        log.debug("REST request to delete PartnerProfile : {}", id);
        Constants.logInfo(
            true,
            CLASS_NAME,
            " REST request to delete PartnerProfile by id" + id,
            "REST request to delete PartnerProfile by id: " + id,
            request
        );

        partnerProfileService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
