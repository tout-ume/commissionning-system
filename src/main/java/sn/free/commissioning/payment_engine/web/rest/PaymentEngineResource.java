package sn.free.commissioning.payment_engine.web.rest;

import java.util.List;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sn.free.commissioning.payment_engine.domain.*;
import sn.free.commissioning.payment_engine.service.PaymentEngineService;
import sn.free.commissioning.service.dto.CommissionBulkPaymentDTO;
import sn.free.commissioning.service.mapper.CommissionMapper;

@RestController
@RequestMapping("/api")
public class PaymentEngineResource {

    private final Logger log = LoggerFactory.getLogger(PaymentEngineResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PaymentEngineService paymentEngineService;

    private final CommissionMapper commissionMapper;

    public PaymentEngineResource(PaymentEngineService paymentEngineService, CommissionMapper commissionMapper) {
        this.paymentEngineService = paymentEngineService;
        this.commissionMapper = commissionMapper;
    }

    /**
     * {@code POST  /payment/pay-dp-network} : Pay DP Network commissions.
     *
     * @param networkPaymentRequest the list of partners and their commissions to pay.
     * @return the {@link ResponseEntity} with status {@code 200 (Success)} and with body the networkPaymentResponse, or with status {@code 400 (Bad Request)} if networkPaymentRequest is null.
     */
    @PostMapping("/payment/pay-dp-network")
    public ResponseEntity<NetworkPaymentResponse> performNetworkPayment(@Valid @RequestBody NetworkPaymentRequest networkPaymentRequest) {
        log.debug(
            "REST request to pay: {}, from wallet: {}",
            networkPaymentRequest.getPartnerCommissions(),
            networkPaymentRequest.getDpWallet()
        );
        NetworkPaymentResponse response = paymentEngineService.performDpNetworkPayment(
            networkPaymentRequest.getPartnerCommissions(),
            networkPaymentRequest.getDpWallet(),
            networkPaymentRequest.getDpWalletPIN()
        );
        return ResponseEntity.ok().body(response);
    }

    /**
     * {@code POST  /payment/pay-top-profile} : Pay the top profile (DP) his commissions and his network's (DG & RV) commissions.
     *
     * @param commissionIds the list of commissions to pay ids.
     * @return the {@link ResponseEntity} with status {@code 200 (Success)} and with body the TopProfilePaymentResponse, or with status {@code 400 (Bad Request)} if TopProfilePaymentResponse is null.
     */
    @PostMapping("/payment/pay-top-profile")
    public ResponseEntity<List<TopProfilePaymentResponse>> performTopProfilePayment(@RequestBody List<Long> commissionIds) {
        log.debug("REST request to pay top profile commissions: {}", commissionIds);
        List<TopProfilePaymentResponse> response = paymentEngineService.performTopProfilePayment(commissionIds);
        return ResponseEntity.ok().body(response);
    }

    /**
     * {@code POST  /payment/bulk-payment} : Pay a list of partners all their commissions.
     *
     * @param commissionBulkPaymentList the list of commissions to pay.
     * @return the {@link ResponseEntity} with status {@code 200 (Success)} and with body a list of commissionBulkPaymentList, or with status {@code 400 (Bad Request)}.
     */
    @PostMapping("/payment/bulk-payment")
    public ResponseEntity<List<CommissionBulkPaymentDTO>> performBulkPayment(
        @RequestBody List<CommissionBulkPaymentDTO> commissionBulkPaymentList
    ) {
        log.debug("REST request to pay partner commissions: {}", commissionBulkPaymentList);

        commissionBulkPaymentList.forEach(item -> {
            item.setPaymentResult(
                paymentEngineService.performCommissionListPaymentFromOperation(commissionMapper.toEntity(item.getCommissions()))
            );
        });

        return ResponseEntity.ok().body(commissionBulkPaymentList);
    }

    /**
     * {@code GET  /payment/operation-balance} : Get the operations account balance.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (Success)} and with body the amount, or with status {@code 400 (Bad Request)}.
     */
    @GetMapping("/payment/operation-balance")
    public ResponseEntity<String> getOperationsBalance() {
        log.debug("REST request to get operation account balance");
        Double response = null;
        try {
            response = paymentEngineService.getOperationBalance();
            return ResponseEntity.ok().body(response.toString());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
