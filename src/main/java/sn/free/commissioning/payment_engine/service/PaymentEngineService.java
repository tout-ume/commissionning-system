package sn.free.commissioning.payment_engine.service;

import java.util.List;
import sn.free.commissioning.domain.Commission;
import sn.free.commissioning.domain.PartnerCommissions;
import sn.free.commissioning.payment_engine.domain.*;

public interface PaymentEngineService {
    /**
     * checks if the wallet's (account) balance is suffiscient for a transfer of the indicated amount
     *
     * @param msisdn         the account to check
     * @param amount         the amount to check
     *
     * @return wether or not the amount is available
     */
    Boolean getFundAvailability(String msisdn, Double amount) throws Exception;

    /**
     * transfers money from a wallet (account) to another
     *
     * @param senderMsisdn         the sender's account
     * @param senderPIN            the sender's pin
     * @param receiverMsisdn       the receiver's account
     * @param amount               the amount to transfer
     * @param comment              the transfer remarks
     *
     * @return transferResponse from the payment server
     */
    TransferResponse performTransfer(String senderMsisdn, String senderPIN, String receiverMsisdn, Double amount, String comment);

    /**
     * performs payment for a specific commission
     *
     * @param commission         the commission to pay
     * @param commentForInstantPayment         the comment for instant commission payment
     * @param isTopProfileCommission        wether or not the commission belongs to a top profile
     * @return the payment process result
     */
    PaymentResult performCommissionPayment(Commission commission, String commentForInstantPayment, Boolean isTopProfileCommission);

    /**
     * performs payment for a list of commissions
     *
     * @param commissions         the commissions to pay
     * @return the payment process result
     */
    PaymentResult performCommissionListPaymentFromOperation(List<Commission> commissions);

    /**
     * performs payment for a DP's network
     *
     * @param partnerCommissions         the partners with their commissions to pay
     * @param dpWallet         the DP's wallet to use for the payment
     * @return the payment process result
     */
    NetworkPaymentResponse performDpNetworkPayment(List<PaymentPartnerCommissions> partnerCommissions, String dpWallet, String dpWalletPIN);

    /**
     * performs topProfile commissions payment for a commissioning plan
     *
     * @param commissionIds    the commissions to pay ids
     * @return the payment process result
     */
    List<TopProfilePaymentResponse> performTopProfilePayment(List<Long> commissionIds);

    /**
     * get operations account balance
     *
     * @return the balance amount
     */
    Double getOperationBalance() throws Exception;
}
