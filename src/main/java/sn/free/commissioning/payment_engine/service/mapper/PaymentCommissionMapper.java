package sn.free.commissioning.payment_engine.service.mapper;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.*;
import org.springframework.stereotype.Component;
import sn.free.commissioning.domain.Commission;
import sn.free.commissioning.domain.CommissionAccount;
import sn.free.commissioning.domain.ConfigurationPlan;
import sn.free.commissioning.domain.Payment;
import sn.free.commissioning.payment_engine.service.dto.CommissionDTO;
import sn.free.commissioning.service.mapper.EntityMapper;

/**
 * Mapper for the entity {@link Commission} and its DTO {@link CommissionDTO}.
 */
@Mapper(componentModel = "spring")
public interface PaymentCommissionMapper extends EntityMapper<CommissionDTO, Commission> {
    @Mapping(target = "removeTransactionOperation", ignore = true)
    Commission toEntity(CommissionDTO commissionDTO);

    List<Commission> toEntity(List<CommissionDTO> commissionDTO);
}
