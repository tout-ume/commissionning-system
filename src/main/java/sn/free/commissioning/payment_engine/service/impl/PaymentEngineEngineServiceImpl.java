package sn.free.commissioning.payment_engine.service.impl;

import java.io.StringReader;
import java.time.*;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import sn.free.commissioning.domain.*;
import sn.free.commissioning.domain.Period;
import sn.free.commissioning.domain.enumeration.CommissionPaymentStatus;
import sn.free.commissioning.domain.enumeration.FrequencyType;
import sn.free.commissioning.domain.enumeration.PaymentStatus;
import sn.free.commissioning.domain.enumeration.PeriodOfOccurrence;
import sn.free.commissioning.payment_engine.domain.*;
import sn.free.commissioning.payment_engine.domain.ennumeration.PaymentResultStatus;
import sn.free.commissioning.payment_engine.service.PaymentEngineService;
import sn.free.commissioning.payment_engine.service.dto.CommissionDTO;
import sn.free.commissioning.payment_engine.service.mapper.PaymentCommissionMapper;
import sn.free.commissioning.repository.*;
import sn.free.commissioning.security.SecurityUtils;
import sn.free.commissioning.service.CommissionService;
import sn.free.commissioning.service.mapper.CommissionMapper;

@Service
public class PaymentEngineEngineServiceImpl implements PaymentEngineService {

    @Value("${mfs.transferRequest.initiator}")
    private String initiator;

    @Value("${mfs.transferRequest.senderIdType}")
    private String senderIdType;

    @Value("${mfs.transferRequest.receiverIdType}")
    private String receiverIdType;

    @Value("${mfs.transferRequest.bearerCode}")
    private String bearerCode;

    @Value("${mfs.transferRequest.language}")
    private String language;

    private final Logger log = LoggerFactory.getLogger(PaymentEngineEngineServiceImpl.class);

    @Autowired
    private WebClient webClient;

    @Value("${mfs.freePayment.wallet.msisdn}")
    private String freePaymentWallet;

    @Value("${mfs.freePayment.wallet.pin}")
    private String freePaymentPin;

    private final PaymentCommissionMapper paymentCommissionMapper;
    private final CommissionMapper commissionMapper;
    private final CommissionRepository commissionRepository;
    private final CommissionService commissionService;
    private final PaymentRepository paymentRepository;

    public PaymentEngineEngineServiceImpl(
        PaymentCommissionMapper paymentCommissionMapper,
        CommissionMapper commissionMapper,
        CommissionRepository commissionRepository,
        CommissionService commissionService,
        PaymentRepository paymentRepository
    ) {
        this.paymentCommissionMapper = paymentCommissionMapper;
        this.commissionMapper = commissionMapper;
        this.commissionRepository = commissionRepository;
        this.commissionService = commissionService;
        this.paymentRepository = paymentRepository;
    }

    @Override
    public Boolean getFundAvailability(String msisdn, Double amount) throws Exception {
        GetBalanceResponse balanceResponse = getWalletBalance(msisdn);

        if (Objects.equals(balanceResponse.getResultCode(), "200")) {
            return balanceResponse.getBalance() >= amount;
        } else {
            log.info("Something went wrong, server response = {}", balanceResponse);
            throw new Exception("Something went wrong. Message : " + balanceResponse.getResultMessage());
        }
    }

    private GetBalanceResponse getWalletBalance(String msisdn) {
        GetBalanceResponse balanceResponse = new GetBalanceResponse();
        String balanceRequestResponse = webClient
            .get()
            .uri(uriBuilder -> uriBuilder.path("getBalanceNoPIN").queryParam("msisdn", msisdn).build())
            .accept(MediaType.APPLICATION_XML)
            .retrieve()
            .bodyToMono(String.class)
            .block();

        log.info("******** GetBalanceResponse = {}", balanceRequestResponse);
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = null;
        Document document = null;
        try {
            db = dbf.newDocumentBuilder();
            assert balanceRequestResponse != null;
            document = db.parse(new InputSource(new StringReader(balanceRequestResponse)));
            NodeList nodeList = document.getElementsByTagName("param");
            for (int x = 0, size = nodeList.getLength(); x < size; x++) {
                //                if (Objects.equals(nodeList.item(x).getAttributes().getNamedItem("name").getNodeValue(), "balance")) {
                switch (nodeList.item(x).getAttributes().getNamedItem("name").getNodeValue()) {
                    case "balance":
                        if (!nodeList.item(x).getAttributes().getNamedItem("value").getNodeValue().isEmpty()) {
                            balanceResponse.setBalance(
                                Double.parseDouble(nodeList.item(x).getAttributes().getNamedItem("value").getNodeValue())
                            );
                        }
                        break;
                    case "resultMessage":
                        balanceResponse.setResultMessage(nodeList.item(x).getAttributes().getNamedItem("value").getNodeValue());
                        break;
                    case "TransactionId":
                        balanceResponse.setTransactionId(nodeList.item(x).getAttributes().getNamedItem("value").getNodeValue());
                        break;
                    case "resultCode":
                        balanceResponse.setResultCode(nodeList.item(x).getAttributes().getNamedItem("value").getNodeValue());
                        break;
                    case "transactionStatus":
                        balanceResponse.setTransactionStatus(nodeList.item(x).getAttributes().getNamedItem("value").getNodeValue());
                        break;
                }
            }
            log.info("******** balance value = {}", balanceResponse.getBalance());
            return balanceResponse;
        } catch (Exception e) {
            log.info("Error parsing XML response : {}", balanceRequestResponse);
            throw new RuntimeException(e);
        }
    }

    @Override
    public TransferResponse performTransfer(String senderMsisdn, String senderPIN, String receiverMsisdn, Double amount, String comment) {
        TransferResponse transferResponse = new TransferResponse();
        String transferRequestResponse = webClient
            .get()
            .uri(uriBuilder ->
                uriBuilder
                    .path("float-transfer")
                    .queryParam("initiator", initiator)
                    .queryParam("transactionAmount", String.format("%.2f", amount))
                    .queryParam("receiverIdType", receiverIdType)
                    .queryParam("bearerCode", bearerCode)
                    .queryParam("senderPIN", senderPIN)
                    .queryParam("language", language)
                    .queryParam("senderIdType", senderIdType)
                    .queryParam("receiverIdValue", receiverMsisdn)
                    .queryParam("senderIdValue", senderMsisdn)
                    .queryParam("remarks", comment)
                    .build()
            )
            .accept(MediaType.APPLICATION_XML)
            .retrieve()
            .bodyToMono(String.class)
            .block();
        log.info("******** transferRequestResponse = {}", transferRequestResponse);

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = null;
        Document document = null;
        try {
            db = dbf.newDocumentBuilder();
            assert transferRequestResponse != null;
            document = db.parse(new InputSource(new StringReader(transferRequestResponse)));
            NodeList nodeList = document.getElementsByTagName("param");
            for (int x = 0, size = nodeList.getLength(); x < size; x++) {
                switch (nodeList.item(x).getAttributes().getNamedItem("name").getNodeValue()) {
                    case "resultMessage":
                        transferResponse.setResultMessage(nodeList.item(x).getAttributes().getNamedItem("value").getNodeValue());
                        break;
                    case "TransactionId":
                        transferResponse.setTransactionId(nodeList.item(x).getAttributes().getNamedItem("value").getNodeValue());
                        break;
                    case "resultCode":
                        transferResponse.setResultCode(nodeList.item(x).getAttributes().getNamedItem("value").getNodeValue());
                        break;
                    case "transactionStatus":
                        transferResponse.setTransactionStatus(nodeList.item(x).getAttributes().getNamedItem("value").getNodeValue());
                        break;
                }
            }
            log.info("******** resultCode value = {}", transferResponse.getResultCode());
        } catch (Exception e) {
            log.info("Error parsing XML response : {}", transferRequestResponse);
            throw new RuntimeException(e);
        }
        return transferResponse;
    }

    @Override
    public PaymentResult performCommissionPayment(Commission commission, String commentForInstantPayment, Boolean isTopProfileCommission) {
        String comment;
        if (commentForInstantPayment.equals("")) {
            comment = buildPaymentComment(commission);
        } else {
            comment = commentForInstantPayment;
        }
        PaymentResult result = new PaymentResult(PaymentResultStatus.FAILURE, "");
        // 1. Check if commission status is TO_BE_PAID
        if (commission.getCommissionPaymentStatus() != CommissionPaymentStatus.TO_BE_PAID) {
            log.info("La commission {} n'est pas à payer", commission);
            result.setMessage("La commission " + commission.getId() + " n'est pas à payer");
            return result;
        }
        // 2. Update status to PENDING
        commission.setCommissionPaymentStatus(CommissionPaymentStatus.PENDING);
        // 3. Get fund availability
        try {
            boolean isFundAvailable = getFundAvailability(
                freePaymentWallet,
                commission.getAmount()
                //                isTopProfileCommission ? commission.getGlobalNetworkCommissionAmount() : commission.getAmount()
            );
            // 3.1 If fund available
            if (isFundAvailable) {
                log.info(
                    "Paiement en cours.... Montant : {}, Bénéficiaire : {}",
                    isTopProfileCommission ? commission.getGlobalNetworkCommissionAmount() : commission.getAmount(),
                    commission.getSenderMsisdn()
                );
                //  4. Preceed with transfer
                TransferResponse transferResponse = performTransfer(
                    freePaymentWallet,
                    freePaymentPin,
                    commission.getSenderMsisdn(),
                    //                    isTopProfileCommission ? commission.getGlobalNetworkCommissionAmount() : commission.getAmount()
                    commission.getAmount(),
                    comment
                );
                Payment payment = new Payment();
                payment.setAmount(isTopProfileCommission ? commission.getGlobalNetworkCommissionAmount() : commission.getAmount());
                payment.setReceiverMsisdn(commission.getSenderMsisdn());
                payment.setCreatedBy(
                    SecurityUtils.getCurrentUserLogin().isPresent() ? SecurityUtils.getCurrentUserLogin().get() : "SYSTEM"
                );
                payment.setReceiverProfile(commission.getSenderProfile());
                payment.setSenderMsisdn(freePaymentWallet);
                // 4.1 If transfer okay, Update status to PAID, return success
                if (Objects.equals(transferResponse.getResultCode(), "SUCCEEDED")) {
                    commission.setCommissionPaymentStatus(CommissionPaymentStatus.PAID);
                    if (isTopProfileCommission) {
                        // Set network commissions payment status to to_be_paid_to_partner
                        commissionService.changeTopProfileNetworkCommissionsStatus(
                            commission,
                            CommissionPaymentStatus.TO_BE_PAID_TO_PARTNER
                        );
                    }
                    log.info(
                        "Le paiement de la commission {} est effectué avec succès: {}",
                        commission,
                        transferResponse.getResultMessage()
                    );
                    result.setStatus(PaymentResultStatus.SUCCESS);
                    result.setMessage(
                        "Le paiement de la commission " +
                        commission.getId() +
                        " est effectué avec succès: " +
                        transferResponse.getResultMessage()
                    );
                    payment.setPaymentStatus(PaymentStatus.SUCCESS);
                    payment.setCreatedDate(Instant.now());
                    log.info("===================== Saving payment {}", payment);
                    Payment savedPayment = paymentRepository.save(payment);
                    commission.setPayment(savedPayment);
                    commissionRepository.save(commission);
                }
                // 4.2 Else update status to FAILED and return error (error.message)
                else {
                    payment.setPaymentStatus(PaymentStatus.FAILURE);
                    payment.setCreatedDate(Instant.now());
                    log.info("===================== Saving payment {}", payment);
                    Payment savedPayment = paymentRepository.save(payment);
                    commission.setPayment(savedPayment);
                    commission.setCommissionPaymentStatus(CommissionPaymentStatus.FAILED);
                    commissionRepository.save(commission);
                    log.info("Le paiement de la commission {} a échoué: {}", commission, transferResponse.getResultMessage());
                    result.setMessage(
                        "Le paiement de la commission " + commission.getId() + " a échoué: " + transferResponse.getResultMessage()
                    );
                }
            }
            // 3.2 Else update status to FAILED and return error ('insuffiscient fund')
            else {
                log.info("Le solde du wallet de paiement ({}) est insuffisant", freePaymentWallet);
                commission.setCommissionPaymentStatus(CommissionPaymentStatus.FAILED);
                commissionRepository.save(commission);
                result.setMessage("Le solde du wallet de paiement (" + freePaymentWallet + ") est insuffisant");
            }
            return result;
        } catch (Exception e) {
            log.info("Le paiement de la commission {} a échoué: {}", commission, e.getMessage());
            commission.setCommissionPaymentStatus(CommissionPaymentStatus.FAILED);
            commissionRepository.save(commission);
            result.setMessage("Le paiement de la commission " + commission.getId() + " a échoué: " + e.getMessage());
            return result;
        }
    }

    @Override
    public PaymentResult performCommissionListPaymentFromOperation(List<Commission> commissions) {
        return performCommissionListPayment(commissions, freePaymentWallet, freePaymentPin);
    }

    private List<Commission> changeCommissionPaymentStatus(
        List<Commission> commissions,
        CommissionPaymentStatus status,
        Boolean checkPaymentStatus
    ) {
        List<Commission> result = new ArrayList<>();
        commissions.forEach(commission -> {
            List<CommissionPaymentStatus> statusToCheck = new ArrayList<>();
            statusToCheck.add(CommissionPaymentStatus.TO_BE_PAID);
            statusToCheck.add(CommissionPaymentStatus.TO_BE_PAID_TO_PARTNER);
            // Check if commission status is TO_BE_PAID or TO_BE_PAID_TO_PARTNER
            if (checkPaymentStatus && !statusToCheck.contains(commission.getCommissionPaymentStatus())) {
                log.info("La commission {} n'est pas à payer", commission);
            } else {
                // Update status
                commission.setCommissionPaymentStatus(status);
                result.add(commission);
            }
        });
        log.info("Les commissions après changement de status du paiement: {}", result);
        return result;
    }

    private Double sumCommissionsAmounts(List<Commission> commissions) {
        Double result = 0.0;
        for (Commission commission : commissions) {
            result += commission.getAmount();
        }
        log.info("Le total du montant a payer est: {}", result);
        return result;
    }

    private PaymentResult performCommissionListPayment(List<Commission> commissions, String walletSource, String walletSourcePIN) {
        PaymentResult result = new PaymentResult(PaymentResultStatus.FAILURE, "");
        // Change commissions status to PENDING
        List<Commission> commissionsToPay = new ArrayList<>(
            changeCommissionPaymentStatus(commissions, CommissionPaymentStatus.PENDING, true)
        );
        if (commissionsToPay.isEmpty()) {
            log.info("Aucune des commissions n'est à payer!");
            result.setMessage("Aucune des commissions n'est à payer!");
            return result;
        }
        // Get amount total
        Double totalAmount = sumCommissionsAmounts(commissionsToPay);
        String comment = buildPaymentComment(commissions.get(0));
        // Assuming fundDisponibilty has already been checked
        try {
            String partnerMsisdn = commissions.get(0).getSenderMsisdn().startsWith("+221")
                ? commissions.get(0).getSenderMsisdn().substring(4)
                : commissions.get(0).getSenderMsisdn();
            log.info("Partner being paid msisdn: {}", partnerMsisdn);
            //  Preceed with transfer
            TransferResponse transferResponse = performTransfer(walletSource, walletSourcePIN, partnerMsisdn, totalAmount, comment);
            Payment payment = new Payment();
            payment.setAmount(totalAmount);
            payment.setReceiverMsisdn(partnerMsisdn);
            payment.setCreatedBy(SecurityUtils.getCurrentUserLogin().get());
            payment.setSpare2(commissions.get(0).getSenderProfile());
            payment.setSenderMsisdn(walletSource);
            // If transfer okay, Update status to PAID, return success
            if (Objects.equals(transferResponse.getResultCode(), "SUCCEEDED")) {
                payment.setPaymentStatus(PaymentStatus.SUCCESS);
                payment.setSpare1(String.valueOf(Instant.now()));
                changeCommissionPaymentStatus(commissions, CommissionPaymentStatus.PAID, false);
                log.info("Le paiement des commissions {} est effectué avec succès: {}", commissions, transferResponse.getResultMessage());
                result.setStatus(PaymentResultStatus.SUCCESS);
                result.setMessage(
                    "Le paiement des commissions " + commissions + " est effectué avec succès: " + transferResponse.getResultMessage()
                );
                log.info("===================== Saving payment {}", payment);
                Payment savedPayment = paymentRepository.save(payment);
                commissions.forEach(item -> item.setPayment(savedPayment));
                commissionRepository.saveAll(commissions);
            }
            // Else update status to FAILED and return error (error.message)
            else {
                payment.setPaymentStatus(PaymentStatus.FAILURE);
                payment.setSpare1(String.valueOf(Instant.now()));
                changeCommissionPaymentStatus(commissions, CommissionPaymentStatus.FAILED, false);
                log.info("Le paiement des commissions {} a échoué: {}", commissions, transferResponse.getResultMessage());
                result.setMessage("Le paiement des commissions " + commissions + " a échoué: " + transferResponse.getResultMessage());
                log.info("===================== Saving payment {}", payment);
                Payment savedPayment = paymentRepository.save(payment);
                commissions.forEach(item -> item.setPayment(savedPayment));
                commissionRepository.saveAll(commissions);
            }
            return result;
        } catch (Exception e) {
            log.info("Le paiement des commissions {} a échoué: {}", commissions, e.getMessage());
            result.setMessage("Le paiement des commissions " + commissions + " a échoué: " + e.getMessage());
            return result;
        }
    }

    @Override
    public NetworkPaymentResponse performDpNetworkPayment(
        List<PaymentPartnerCommissions> partnerCommissions,
        String dpWallet,
        String dpWalletPIN
    ) {
        NetworkPaymentResponse response = new NetworkPaymentResponse();
        List<Commission> allNetworkCommissions = new ArrayList<>();
        for (PaymentPartnerCommissions partnerCommission : partnerCommissions) {
            allNetworkCommissions.addAll(paymentCommissionMapper.toEntity(partnerCommission.getCommissions()));
            log.info("Les commissions à payer {} ", allNetworkCommissions);
        }
        try {
            if (getFundAvailability(dpWallet, sumCommissionsAmounts(allNetworkCommissions))) {
                // If DP's payment wallet has enough fund, preceed with payment
                for (PaymentPartnerCommissions partnerCommission : partnerCommissions) {
                    PaymentResult result = performCommissionListPayment(
                        commissionMapper.toEntity(
                            commissionService.findByIdIn(
                                partnerCommission.getCommissions().stream().map(CommissionDTO::getId).collect(Collectors.toList())
                            )
                        ),
                        dpWallet,
                        dpWalletPIN
                    );
                    if (result.getStatus() == PaymentResultStatus.SUCCESS) {
                        partnerCommission
                            .getCommissions()
                            .forEach(item -> {
                                paymentCommissionMapper.toEntity(item).setCommissionPaymentStatus(CommissionPaymentStatus.PAID);
                            });
                    } else {
                        partnerCommission
                            .getCommissions()
                            .forEach(item -> {
                                paymentCommissionMapper.toEntity(item).setCommissionPaymentStatus(CommissionPaymentStatus.FAILED);
                            });
                    }
                }
                response.setPaymentResult(
                    new PaymentResult(PaymentResultStatus.SUCCESS, "Le paiement des commissions est effectué avec succès.")
                );
                response.setPartnerCommissions(partnerCommissions);
            } else {
                log.info("Le solde du wallet de paiement ({}) est insuffisant pour le paiement des commissions.", dpWallet);
                changeCommissionPaymentStatus(allNetworkCommissions, CommissionPaymentStatus.FAILED, false);
                response.setPartnerCommissions(new ArrayList<>());
                response.setPaymentResult(
                    new PaymentResult(
                        PaymentResultStatus.FAILURE,
                        "Le solde du wallet de (" + dpWallet + ") est insuffisant pour le paiement des commissions."
                    )
                );
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        log.info("************ NetworkPaymentResponse: {}", response);
        return response;
    }

    @Override
    public List<TopProfilePaymentResponse> performTopProfilePayment(List<Long> commissionIds) {
        List<TopProfilePaymentResponse> result = new ArrayList<>();
        commissionRepository
            .findByIdIn(commissionIds)
            .forEach(commission -> {
                TopProfilePaymentResponse response = new TopProfilePaymentResponse();
                response.setCommission(commission);
                response.setResult(performCommissionPayment(commission, "", true));
                result.add(response);
            });
        return result;
    }

    @Override
    public Double getOperationBalance() throws Exception {
        GetBalanceResponse balanceResponse = getWalletBalance(freePaymentWallet);

        if (Objects.equals(balanceResponse.getResultCode(), "200")) {
            return balanceResponse.getBalance();
        } else {
            log.info("Something went wrong, server response = {}", balanceResponse);
            throw new Exception("Something went wrong. Message : " + balanceResponse.getResultMessage());
        }
    }

    private String commissionTypeValue(String profile) {
        switch (profile) {
            case "DP":
                return "DISTRIBUTEURS_PARTENAIRES@";
            case "MD":
                return "MARKET_DEVELOPPER@";
            case "DG":
                return "GROSSISTES@";
            default:
                return "KIOSQUES@";
        }
    }

    private String commissionPeriodValue(Long commissionId) {
        Commission commission = commissionRepository.findById(commissionId).get();
        int today = LocalDate.now().getDayOfMonth();
        int month = LocalDate.now().getMonthValue();
        int year = LocalDate.now().getYear();
        String oneDay = today + "/" + month + "/" + year + "@" + today + "/" + month + "/" + year;
        // Case of Instantly calculated commission
        if (commission.getConfigurationPlan().getCommissioningPlan().getCalculusFrequency().getType().equals(FrequencyType.INSTANTLY)) {
            return oneDay;
        }
        Period paymentPeriod = commission.getConfigurationPlan().getCommissioningPlan().getPaymentPeriod();
        Frequency paymentFrequency = commission.getConfigurationPlan().getCommissioningPlan().getPaymentFrequency();
        switch (paymentPeriod.getPeriodType()) {
            case RECCURRENT:
                String[] days = paymentPeriod.getDaysOrDatesOfOccurrence().split(",");
                if (paymentFrequency.getPeriodOfOccurrence() == PeriodOfOccurrence.MONTH) {
                    for (int i = 0; i < days.length; i++) {
                        if ((i == days.length - 1) && today > Integer.parseInt(days[i])) {
                            return (
                                days[i] +
                                "/" +
                                month +
                                "/" +
                                year +
                                "@" +
                                days[0] +
                                "/" +
                                (month < 12 ? month + 1 + "/" + year : 1 + "/" + (year + 1))
                            );
                        } else if (Integer.parseInt(days[i + 1]) >= today && Integer.parseInt(days[i]) < today) {
                            return days[i] + "/" + month + "/" + year + "@" + days[i + 1] + "/" + month + "/" + year;
                        }
                    }
                } else if (paymentFrequency.getPeriodOfOccurrence() == PeriodOfOccurrence.WEEK) {
                    int todayWeekDay = LocalDate.now().getDayOfWeek().getValue();
                    for (int i = 0; i < days.length; i++) {
                        if (Integer.parseInt(days[i + 1]) >= todayWeekDay && Integer.parseInt(days[i]) < todayWeekDay) {
                            return (
                                today -
                                (todayWeekDay - Integer.parseInt(days[i])) +
                                "/" +
                                month +
                                "/" +
                                year +
                                "@" +
                                (today - (Integer.parseInt(days[i + 1]) - todayWeekDay)) +
                                "/" +
                                month +
                                "/" +
                                year
                            );
                        }
                    }
                }
            case DAILY:
                return oneDay;
            case INSTANTLY:
                return oneDay;
            default:
                return "";
        }
    }

    private String buildPaymentComment(Commission commission) {
        String paymentComment = "COMMISSION@";
        paymentComment += commissionTypeValue(commission.getSenderProfile());
        paymentComment += commissionPeriodValue(commission.getId());
        return paymentComment;
    }
}
