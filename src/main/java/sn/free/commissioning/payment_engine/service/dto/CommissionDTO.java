package sn.free.commissioning.payment_engine.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;
import sn.free.commissioning.domain.enumeration.CommissionPaymentStatus;

/**
 * A DTO for the {@link sn.free.commissioning.domain.Commission} entity.
 */
public class CommissionDTO implements Serializable {

    private Long id;

    @NotNull
    private Double amount;

    private Instant calculatedAt;
    private CommissionPaymentStatus commissionPaymentStatus;

    public CommissionPaymentStatus getCommissionPaymentStatus() {
        return commissionPaymentStatus;
    }

    public void setCommissionPaymentStatus(CommissionPaymentStatus commissionPaymentStatus) {
        this.commissionPaymentStatus = commissionPaymentStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Instant getCalculatedAt() {
        return calculatedAt;
    }

    public void setCalculatedAt(Instant calculatedAt) {
        this.calculatedAt = calculatedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommissionDTO)) {
            return false;
        }

        CommissionDTO commissionDTO = (CommissionDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, commissionDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    @Override
    public String toString() {
        return (
            "CommissionDTO{" +
            "id=" +
            id +
            ", amount=" +
            amount +
            ", calculatedAt=" +
            calculatedAt +
            ", commissionPaymentStatus=" +
            commissionPaymentStatus +
            '}'
        );
    }
}
