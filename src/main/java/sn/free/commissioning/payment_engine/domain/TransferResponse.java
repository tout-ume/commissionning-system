package sn.free.commissioning.payment_engine.domain;

public class TransferResponse {

    private String resultMessage;
    private String transactionId;
    private String resultCode;
    private String transactionStatus;

    public TransferResponse(String resultMessage, String transactionId, String resultCode, String transactionStatus) {
        this.resultMessage = resultMessage;
        this.transactionId = transactionId;
        this.resultCode = resultCode;
        this.transactionStatus = transactionStatus;
    }

    public TransferResponse() {}

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    @Override
    public String toString() {
        return (
            "TransferResponse{" +
            "resultMessage='" +
            resultMessage +
            '\'' +
            ", transactionId='" +
            transactionId +
            '\'' +
            ", resultCode='" +
            resultCode +
            '\'' +
            ", transactionStatus='" +
            transactionStatus +
            '\'' +
            '}'
        );
    }
}
