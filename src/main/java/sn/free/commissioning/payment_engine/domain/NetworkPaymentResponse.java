package sn.free.commissioning.payment_engine.domain;

import java.io.Serializable;
import java.util.List;
import sn.free.commissioning.domain.PartnerCommissions;

public class NetworkPaymentResponse implements Serializable {

    private PaymentResult paymentResult;
    private List<PaymentPartnerCommissions> partnerCommissions;

    public NetworkPaymentResponse() {}

    public NetworkPaymentResponse(PaymentResult paymentResult, List<PaymentPartnerCommissions> partnerCommissions) {
        this.paymentResult = paymentResult;
        this.partnerCommissions = partnerCommissions;
    }

    public PaymentResult getPaymentResult() {
        return paymentResult;
    }

    public void setPaymentResult(PaymentResult paymentResult) {
        this.paymentResult = paymentResult;
    }

    public List<PaymentPartnerCommissions> getPartnerCommissions() {
        return partnerCommissions;
    }

    public void setPartnerCommissions(List<PaymentPartnerCommissions> partnerCommissions) {
        this.partnerCommissions = partnerCommissions;
    }

    @Override
    public String toString() {
        return "NetworkPaymentResponse{" + "paymentResult=" + paymentResult + ", partnerCommissions=" + partnerCommissions + '}';
    }
}
