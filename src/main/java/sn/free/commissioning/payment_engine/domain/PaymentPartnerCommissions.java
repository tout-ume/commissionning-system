package sn.free.commissioning.payment_engine.domain;

import java.util.List;
import sn.free.commissioning.payment_engine.service.dto.CommissionDTO;

public class PaymentPartnerCommissions {

    private String partnerMsisdn;
    private List<CommissionDTO> commissions;

    public String getPartnerMString() {
        return partnerMsisdn;
    }

    public void setPartnerMsisdn(String partnerMsisdn) {
        this.partnerMsisdn = partnerMsisdn;
    }

    public List<CommissionDTO> getCommissions() {
        return commissions;
    }

    public void setCommissions(List<CommissionDTO> commissions) {
        this.commissions = commissions;
    }

    @Override
    public String toString() {
        return "PartnerCommissions{" + "partnerMsisdn=" + partnerMsisdn + ", commissions=" + commissions + '}';
    }
}
