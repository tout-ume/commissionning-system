package sn.free.commissioning.payment_engine.domain;

public class GetBalanceResponse {

    private String resultMessage;
    private Double balance;
    private String transactionId;
    private String resultCode;
    private String transactionStatus;

    public GetBalanceResponse(String resultMessage, Double balance, String transactionId, String resultCode, String transactionStatus) {
        this.resultMessage = resultMessage;
        this.balance = balance;
        this.transactionId = transactionId;
        this.resultCode = resultCode;
        this.transactionStatus = transactionStatus;
    }

    public GetBalanceResponse() {}

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getResultCode() {
        return resultCode;
    }

    public void setResultCode(String resultCode) {
        this.resultCode = resultCode;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    @Override
    public String toString() {
        return (
            "GetBalanceResponse{" +
            "resultMessage='" +
            resultMessage +
            '\'' +
            ", balance='" +
            balance +
            '\'' +
            ", transactionId='" +
            transactionId +
            '\'' +
            ", resultCode='" +
            resultCode +
            '\'' +
            ", transactionStatus='" +
            transactionStatus +
            '\'' +
            '}'
        );
    }
}
