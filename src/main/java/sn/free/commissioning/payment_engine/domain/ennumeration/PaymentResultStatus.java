package sn.free.commissioning.payment_engine.domain.ennumeration;

public enum PaymentResultStatus {
    SUCCESS,
    FAILURE,
}
