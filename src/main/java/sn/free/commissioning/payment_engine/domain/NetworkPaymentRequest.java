package sn.free.commissioning.payment_engine.domain;

import java.io.Serializable;
import java.util.List;
import sn.free.commissioning.payment_engine.domain.PaymentPartnerCommissions;

public class NetworkPaymentRequest implements Serializable {

    private String dpWallet;
    private String dpWalletPIN;
    private List<PaymentPartnerCommissions> partnerCommissions;

    public NetworkPaymentRequest() {}

    public NetworkPaymentRequest(String dpWallet, String dpWalletPIN, List<PaymentPartnerCommissions> partnerCommissions) {
        this.dpWallet = dpWallet;
        this.dpWalletPIN = dpWalletPIN;
        this.partnerCommissions = partnerCommissions;
    }

    public String getDpWallet() {
        return dpWallet;
    }

    public void setDpWallet(String dpWallet) {
        this.dpWallet = dpWallet;
    }

    public String getDpWalletPIN() {
        return dpWalletPIN;
    }

    public void setDpWalletPIN(String dpWalletPIN) {
        this.dpWalletPIN = dpWalletPIN;
    }

    public List<PaymentPartnerCommissions> getPartnerCommissions() {
        return partnerCommissions;
    }

    public void setPartnerCommissions(List<PaymentPartnerCommissions> partnerCommissions) {
        this.partnerCommissions = partnerCommissions;
    }

    @Override
    public String toString() {
        return (
            "NetworkPaymentRequest{" +
            "dpWallet='" +
            dpWallet +
            '\'' +
            ", dpWalletPIN='" +
            dpWalletPIN +
            '\'' +
            ", partnerCommissions=" +
            partnerCommissions +
            '}'
        );
    }
}
