package sn.free.commissioning.payment_engine.domain;

import sn.free.commissioning.payment_engine.domain.ennumeration.PaymentResultStatus;

public class PaymentResult {

    private PaymentResultStatus status;
    private String message;

    public PaymentResult(PaymentResultStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public PaymentResultStatus getStatus() {
        return status;
    }

    public void setStatus(PaymentResultStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
