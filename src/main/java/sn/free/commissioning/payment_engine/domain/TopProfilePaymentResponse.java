package sn.free.commissioning.payment_engine.domain;

import java.io.Serializable;
import sn.free.commissioning.domain.Commission;

public class TopProfilePaymentResponse implements Serializable {

    private PaymentResult result;
    private Commission commission;

    public PaymentResult getResult() {
        return result;
    }

    public void setResult(PaymentResult result) {
        this.result = result;
    }

    public Commission getCommission() {
        return commission;
    }

    public void setCommission(Commission commission) {
        this.commission = commission;
    }

    @Override
    public String toString() {
        return "TopProfilePaymentResponse{" + "result=" + result + ", commission=" + commission + '}';
    }
}
