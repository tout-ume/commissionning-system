package sn.free.commissioning.security;

import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import sn.free.commissioning.config.Constants;
import sn.free.commissioning.service.MailService;
import sn.free.commissioning.web.rest.TransactionOperationResource;

@Service
public class DiskManagerService {

    private static final Logger log = LoggerFactory.getLogger(DiskManagerService.class);

    private static final String CLASS_NAME = String.valueOf(DiskManagerService.class);
    private final MailService mailService;

    public DiskManagerService(MailService mailService) {
        this.mailService = mailService;
    }

    @Scheduled(cron = "0 2 3 1/1 * ?")
    public void availableSpaceMonitor() {
        log.debug("Checking available disk space");

        long availableUsableDiskSpace = new File("/").getFreeSpace();
        long totalDisSpace = new File("/").getTotalSpace();

        long availableSpacePercentage = (availableUsableDiskSpace * 100) / totalDisSpace;

        if (availableSpacePercentage <= 5) {
            mailService.sendSpaceMonitoringMail(totalDisSpace, availableUsableDiskSpace, availableSpacePercentage);
        }

        Constants.logInfo(
            true,
            CLASS_NAME,
            "Monitoring available disk space",
            "Space available : " + availableUsableDiskSpace + " sur " + totalDisSpace + " soit " + availableSpacePercentage + "%",
            null
        );
    }
}
