package sn.free.commissioning.exception;

public enum ErrorCodes {
    ENTITY_NOT_FOUND(1000),
    ENTITY_NOT_VALID(1001),
    ENTITY_ALREADY_IN_USE(1002);

    private int code;

    ErrorCodes(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
