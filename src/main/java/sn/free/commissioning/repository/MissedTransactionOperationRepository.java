package sn.free.commissioning.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.free.commissioning.domain.MissedTransactionOperation;

/**
 * Spring Data SQL repository for the MissedTransactionOperation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MissedTransactionOperationRepository
    extends JpaRepository<MissedTransactionOperation, Long>, JpaSpecificationExecutor<MissedTransactionOperation> {}
