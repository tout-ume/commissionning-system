package sn.free.commissioning.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import sn.free.commissioning.domain.Commission;

public interface CommissionRepositoryWithBagRelationships {
    Optional<Commission> fetchBagRelationships(Optional<Commission> commission);

    List<Commission> fetchBagRelationships(List<Commission> commissions);

    Page<Commission> fetchBagRelationships(Page<Commission> commissions);
}
