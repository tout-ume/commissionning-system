package sn.free.commissioning.repository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.annotations.QueryHints;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import sn.free.commissioning.domain.Zone;

/**
 * Utility repository to load bag relationships based on https://vladmihalcea.com/hibernate-multiplebagfetchexception/
 */
public class ZoneRepositoryWithBagRelationshipsImpl implements ZoneRepositoryWithBagRelationships {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Zone> fetchBagRelationships(Optional<Zone> zone) {
        return zone.map(this::fetchPartners);
    }

    @Override
    public Page<Zone> fetchBagRelationships(Page<Zone> zones) {
        return new PageImpl<>(fetchBagRelationships(zones.getContent()), zones.getPageable(), zones.getTotalElements());
    }

    @Override
    public List<Zone> fetchBagRelationships(List<Zone> zones) {
        return Optional.of(zones).map(this::fetchPartners).orElse(Collections.emptyList());
    }

    Zone fetchPartners(Zone result) {
        return entityManager
            .createQuery("select zone from Zone zone left join fetch zone.partners where zone is :zone", Zone.class)
            .setParameter("zone", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<Zone> fetchPartners(List<Zone> zones) {
        return entityManager
            .createQuery("select distinct zone from Zone zone left join fetch zone.partners where zone in :zones", Zone.class)
            .setParameter("zones", zones)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
    }
}
