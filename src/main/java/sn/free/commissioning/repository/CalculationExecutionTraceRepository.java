package sn.free.commissioning.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.free.commissioning.domain.CalculationExecutionTrace;

/**
 * Spring Data SQL repository for the CalculationExecutionTrace entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CalculationExecutionTraceRepository
    extends JpaRepository<CalculationExecutionTrace, Long>, JpaSpecificationExecutor<CalculationExecutionTrace> {}
