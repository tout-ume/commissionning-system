package sn.free.commissioning.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.free.commissioning.domain.Frequency;
import sn.free.commissioning.domain.enumeration.FrequencyType;
import sn.free.commissioning.domain.enumeration.OperationType;

/**
 * Spring Data SQL repository for the Frequency entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FrequencyRepository extends JpaRepository<Frequency, Long>, JpaSpecificationExecutor<Frequency> {
    Frequency findByType(FrequencyType instantly);
    Optional<Frequency> findByName(String name);

    Optional<Frequency> findByNameAndOperationType(String name, OperationType operationType);
}
