package sn.free.commissioning.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import sn.free.commissioning.domain.Partner;

public interface PartnerRepositoryWithBagRelationships {
    Optional<Partner> fetchBagRelationships(Optional<Partner> partner);

    List<Partner> fetchBagRelationships(List<Partner> partners);

    Page<Partner> fetchBagRelationships(Page<Partner> partners);
}
