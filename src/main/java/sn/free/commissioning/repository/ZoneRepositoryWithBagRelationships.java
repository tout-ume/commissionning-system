package sn.free.commissioning.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import sn.free.commissioning.domain.Zone;

public interface ZoneRepositoryWithBagRelationships {
    Optional<Zone> fetchBagRelationships(Optional<Zone> zone);

    List<Zone> fetchBagRelationships(List<Zone> zones);

    Page<Zone> fetchBagRelationships(Page<Zone> zones);
}
