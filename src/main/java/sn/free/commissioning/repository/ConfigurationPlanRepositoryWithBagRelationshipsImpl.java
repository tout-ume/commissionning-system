package sn.free.commissioning.repository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.annotations.QueryHints;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import sn.free.commissioning.domain.ConfigurationPlan;

/**
 * Utility repository to load bag relationships based on https://vladmihalcea.com/hibernate-multiplebagfetchexception/
 */
public class ConfigurationPlanRepositoryWithBagRelationshipsImpl implements ConfigurationPlanRepositoryWithBagRelationships {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<ConfigurationPlan> fetchBagRelationships(Optional<ConfigurationPlan> configurationPlan) {
        return configurationPlan.map(this::fetchZones);
    }

    @Override
    public Page<ConfigurationPlan> fetchBagRelationships(Page<ConfigurationPlan> configurationPlans) {
        return new PageImpl<>(
            fetchBagRelationships(configurationPlans.getContent()),
            configurationPlans.getPageable(),
            configurationPlans.getTotalElements()
        );
    }

    @Override
    public List<ConfigurationPlan> fetchBagRelationships(List<ConfigurationPlan> configurationPlans) {
        return Optional.of(configurationPlans).map(this::fetchZones).orElse(Collections.emptyList());
    }

    ConfigurationPlan fetchZones(ConfigurationPlan result) {
        return entityManager
            .createQuery(
                "select configurationPlan from ConfigurationPlan configurationPlan left join fetch configurationPlan.zones where configurationPlan is :configurationPlan",
                ConfigurationPlan.class
            )
            .setParameter("configurationPlan", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<ConfigurationPlan> fetchZones(List<ConfigurationPlan> configurationPlans) {
        return entityManager
            .createQuery(
                "select distinct configurationPlan from ConfigurationPlan configurationPlan left join fetch configurationPlan.zones where configurationPlan in :configurationPlans",
                ConfigurationPlan.class
            )
            .setParameter("configurationPlans", configurationPlans)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
    }
}
