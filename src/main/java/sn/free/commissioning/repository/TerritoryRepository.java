package sn.free.commissioning.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.free.commissioning.domain.Territory;

/**
 * Spring Data SQL repository for the Territory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TerritoryRepository extends JpaRepository<Territory, Long>, JpaSpecificationExecutor<Territory> {}
