package sn.free.commissioning.repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sn.free.commissioning.domain.Partner;

/**
 * Spring Data SQL repository for the Partner entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PartnerRepository
    extends PartnerRepositoryWithBagRelationships, JpaRepository<Partner, Long>, JpaSpecificationExecutor<Partner> {
    @EntityGraph(value = "graph.Partner", type = EntityGraph.EntityGraphType.LOAD)
    Partner findByMsisdn(String msisdn);

    default Optional<Partner> findOneWithEagerRelationships(Long id) {
        return this.fetchBagRelationships(this.findById(id));
    }

    default List<Partner> findAllWithEagerRelationships() {
        return this.fetchBagRelationships(this.findAll());
    }

    default Page<Partner> findAllWithEagerRelationships(Pageable pageable) {
        return this.fetchBagRelationships(this.findAll(pageable));
    }

    @Query("SELECT p, pp.code as profile FROM Partner p INNER JOIN PartnerProfile pp ON pp.id = p.partnerProfileId WHERE pp.code = :code")
    List<Partner> findByPartnerProfile(@Param("code") String code);

    List<Partner> findByPartnerProfileId(Long partnerProfileId);

    @EntityGraph(value = "graph.Partner", type = EntityGraph.EntityGraphType.LOAD)
    List<Partner> findByPartnerProfileIdAndZones_CodeIsIn(Long partnerProfileId, Collection<String> codes);

    List<Partner> findByMsisdnIn(Collection<String> msisdns);

    List<Partner> findByParent_Id(Long id);
}
