package sn.free.commissioning.repository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.annotations.QueryHints;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import sn.free.commissioning.domain.Partner;

/**
 * Utility repository to load bag relationships based on https://vladmihalcea.com/hibernate-multiplebagfetchexception/
 */
public class PartnerRepositoryWithBagRelationshipsImpl implements PartnerRepositoryWithBagRelationships {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Partner> fetchBagRelationships(Optional<Partner> partner) {
        return partner.map(this::fetchZones);
    }

    @Override
    public Page<Partner> fetchBagRelationships(Page<Partner> partners) {
        return new PageImpl<>(fetchBagRelationships(partners.getContent()), partners.getPageable(), partners.getTotalElements());
    }

    @Override
    public List<Partner> fetchBagRelationships(List<Partner> partners) {
        return Optional.of(partners).map(this::fetchZones).orElse(Collections.emptyList());
    }

    Partner fetchZones(Partner result) {
        return entityManager
            .createQuery("select partner from Partner partner left join fetch partner.zones where partner is :partner", Partner.class)
            .setParameter("partner", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<Partner> fetchZones(List<Partner> partners) {
        return entityManager
            .createQuery(
                "select distinct partner from Partner partner left join fetch partner.zones where partner in :partners",
                Partner.class
            )
            .setParameter("partners", partners)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
    }
}
