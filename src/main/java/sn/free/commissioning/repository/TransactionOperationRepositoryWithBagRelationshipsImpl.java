package sn.free.commissioning.repository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.annotations.QueryHints;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import sn.free.commissioning.domain.TransactionOperation;

/**
 * Utility repository to load bag relationships based on https://vladmihalcea.com/hibernate-multiplebagfetchexception/
 */
public class TransactionOperationRepositoryWithBagRelationshipsImpl implements TransactionOperationRepositoryWithBagRelationships {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<TransactionOperation> fetchBagRelationships(Optional<TransactionOperation> transactionOperation) {
        return transactionOperation.map(this::fetchCommissions);
    }

    @Override
    public Page<TransactionOperation> fetchBagRelationships(Page<TransactionOperation> transactionOperations) {
        return new PageImpl<>(
            fetchBagRelationships(transactionOperations.getContent()),
            transactionOperations.getPageable(),
            transactionOperations.getTotalElements()
        );
    }

    @Override
    public List<TransactionOperation> fetchBagRelationships(List<TransactionOperation> transactionOperations) {
        return Optional.of(transactionOperations).map(this::fetchCommissions).orElse(Collections.emptyList());
    }

    TransactionOperation fetchCommissions(TransactionOperation result) {
        return entityManager
            .createQuery(
                "select transactionOperation from TransactionOperation transactionOperation left join fetch transactionOperation.commissions where transactionOperation is :transactionOperation",
                TransactionOperation.class
            )
            .setParameter("transactionOperation", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<TransactionOperation> fetchCommissions(List<TransactionOperation> transactionOperations) {
        return entityManager
            .createQuery(
                "select distinct transactionOperation from TransactionOperation transactionOperation left join fetch transactionOperation.commissions where transactionOperation in :transactionOperations",
                TransactionOperation.class
            )
            .setParameter("transactionOperations", transactionOperations)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
    }
}
