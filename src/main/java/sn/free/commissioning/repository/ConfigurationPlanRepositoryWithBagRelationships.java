package sn.free.commissioning.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import sn.free.commissioning.domain.ConfigurationPlan;

public interface ConfigurationPlanRepositoryWithBagRelationships {
    Optional<ConfigurationPlan> fetchBagRelationships(Optional<ConfigurationPlan> configurationPlan);

    List<ConfigurationPlan> fetchBagRelationships(List<ConfigurationPlan> configurationPlans);

    Page<ConfigurationPlan> fetchBagRelationships(Page<ConfigurationPlan> configurationPlans);
}
