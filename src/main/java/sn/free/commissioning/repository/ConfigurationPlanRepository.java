package sn.free.commissioning.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sn.free.commissioning.domain.ConfigurationPlan;

/**
 * Spring Data SQL repository for the ConfigurationPlan entity.
 */
@Repository
public interface ConfigurationPlanRepository
    extends
        ConfigurationPlanRepositoryWithBagRelationships,
        JpaRepository<ConfigurationPlan, Long>,
        JpaSpecificationExecutor<ConfigurationPlan> {
    default Optional<ConfigurationPlan> findOneWithEagerRelationships(Long id) {
        return this.fetchBagRelationships(this.findById(id));
    }

    default List<ConfigurationPlan> findAllWithEagerRelationships() {
        return this.fetchBagRelationships(this.findAll());
    }

    default Page<ConfigurationPlan> findAllWithEagerRelationships(Pageable pageable) {
        return this.fetchBagRelationships(this.findAll(pageable));
    }

    List<ConfigurationPlan> findByPartnerProfile_Id(Long id);

    List<ConfigurationPlan> findByCommissioningPlan_Id(Long id);

    List<ConfigurationPlan> findByPartnerProfile_Code(String code);
}
