package sn.free.commissioning.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.free.commissioning.domain.CommissionType;

/**
 * Spring Data SQL repository for the CommissionType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CommissionTypeRepository extends JpaRepository<CommissionType, Long>, JpaSpecificationExecutor<CommissionType> {}
