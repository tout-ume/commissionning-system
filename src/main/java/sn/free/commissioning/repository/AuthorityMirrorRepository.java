package sn.free.commissioning.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.free.commissioning.domain.AuthorityMirror;

/**
 * Spring Data SQL repository for the AuthorityMirror entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AuthorityMirrorRepository extends JpaRepository<AuthorityMirror, Long>, JpaSpecificationExecutor<AuthorityMirror> {}
