package sn.free.commissioning.repository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.annotations.QueryHints;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import sn.free.commissioning.domain.CommissioningPlan;

/**
 * Utility repository to load bag relationships based on https://vladmihalcea.com/hibernate-multiplebagfetchexception/
 */
public class CommissioningPlanRepositoryWithBagRelationshipsImpl implements CommissioningPlanRepositoryWithBagRelationships {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<CommissioningPlan> fetchBagRelationships(Optional<CommissioningPlan> commissioningPlan) {
        return commissioningPlan.map(this::fetchServices);
    }

    @Override
    public Page<CommissioningPlan> fetchBagRelationships(Page<CommissioningPlan> commissioningPlans) {
        return new PageImpl<>(
            fetchBagRelationships(commissioningPlans.getContent()),
            commissioningPlans.getPageable(),
            commissioningPlans.getTotalElements()
        );
    }

    @Override
    public List<CommissioningPlan> fetchBagRelationships(List<CommissioningPlan> commissioningPlans) {
        return Optional.of(commissioningPlans).map(this::fetchServices).orElse(Collections.emptyList());
    }

    CommissioningPlan fetchServices(CommissioningPlan result) {
        return entityManager
            .createQuery(
                "select commissioningPlan from CommissioningPlan commissioningPlan left join fetch commissioningPlan.services where commissioningPlan is :commissioningPlan",
                CommissioningPlan.class
            )
            .setParameter("commissioningPlan", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<CommissioningPlan> fetchServices(List<CommissioningPlan> commissioningPlans) {
        return entityManager
            .createQuery(
                "select distinct commissioningPlan from CommissioningPlan commissioningPlan left join fetch commissioningPlan.services where commissioningPlan in :commissioningPlans",
                CommissioningPlan.class
            )
            .setParameter("commissioningPlans", commissioningPlans)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
    }
}
