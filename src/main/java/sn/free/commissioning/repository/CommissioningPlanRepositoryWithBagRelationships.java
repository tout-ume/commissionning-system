package sn.free.commissioning.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import sn.free.commissioning.domain.CommissioningPlan;

public interface CommissioningPlanRepositoryWithBagRelationships {
    Optional<CommissioningPlan> fetchBagRelationships(Optional<CommissioningPlan> commissioningPlan);

    List<CommissioningPlan> fetchBagRelationships(List<CommissioningPlan> commissioningPlans);

    Page<CommissioningPlan> fetchBagRelationships(Page<CommissioningPlan> commissioningPlans);
}
