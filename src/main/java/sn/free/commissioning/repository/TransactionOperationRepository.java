package sn.free.commissioning.repository;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sn.free.commissioning.domain.TransactionOperation;

/**
 * Spring Data SQL repository for the TransactionOperation entity.
 */
@Repository
public interface TransactionOperationRepository
    extends
        TransactionOperationRepositoryWithBagRelationships,
        JpaRepository<TransactionOperation, Long>,
        JpaSpecificationExecutor<TransactionOperation> {
    default Optional<TransactionOperation> findOneWithEagerRelationships(Long id) {
        return this.fetchBagRelationships(this.findById(id));
    }

    default List<TransactionOperation> findAllWithEagerRelationships() {
        return this.fetchBagRelationships(this.findAll());
    }

    default Page<TransactionOperation> findAllWithEagerRelationships(Pageable pageable) {
        return this.fetchBagRelationships(this.findAll(pageable));
    }

    List<TransactionOperation> findByAgentMsisdnIgnoreCaseAndOperationDateIsBetweenAndSubTypeIsInAndSenderZoneIsInAndIsFraud(
        String agentMsisdn,
        Instant operationDateStart,
        Instant operationDateEnd,
        Collection<String> subTypes,
        Collection<String> senderZones,
        Boolean isFraud
    );

    List<TransactionOperation> findByAgentMsisdnIgnoreCase(String agentMsisdn);

    @Query("SELECT to FROM TransactionOperation to WHERE to.agentMsisdn IN :msisdns AND to.operationDate BETWEEN :startDate AND :endDate")
    List<TransactionOperation> findWithMsisdns(
        @Param("msisdns") List<String> msisdns,
        @Param("startDate") LocalDateTime startDate,
        @Param("endDate") LocalDateTime endDate
    );

    List<TransactionOperation> findByIsFraudAndTaggedAtIsGreaterThanEqual(Boolean isFraud, Instant taggedAt);

    List<TransactionOperation> findByIsFraudAndOperationDateBetweenAndSenderZoneInAndSubTypeIn(
        Boolean isFraud,
        Instant operationDateStart,
        Instant operationDateEnd,
        Collection<String> senderZones,
        Collection<String> subTypes
    );

    List<TransactionOperation> findByTidIn(Collection<String> tids);
}
