package sn.free.commissioning.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sn.free.commissioning.domain.Zone;

/**
 * Spring Data SQL repository for the Zone entity.
 */
@Repository
public interface ZoneRepository extends ZoneRepositoryWithBagRelationships, JpaRepository<Zone, Long>, JpaSpecificationExecutor<Zone> {
    default Optional<Zone> findOneWithEagerRelationships(Long id) {
        return this.fetchBagRelationships(this.findById(id));
    }

    default List<Zone> findAllWithEagerRelationships() {
        return this.fetchBagRelationships(this.findAll());
    }

    default Page<Zone> findAllWithEagerRelationships(Pageable pageable) {
        return this.fetchBagRelationships(this.findAll(pageable));
    }
}
