package sn.free.commissioning.repository;

import java.util.Collection;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.free.commissioning.domain.Blacklisted;

/**
 * Spring Data SQL repository for the Blacklisted entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BlacklistedRepository extends JpaRepository<Blacklisted, Long>, JpaSpecificationExecutor<Blacklisted> {
    List<Blacklisted> findByCommissioningPlan_IdInAndPartner_MsisdnInAndBlockedIs(
        Collection<Long> ids,
        Collection<String> msisdns,
        Boolean blocked
    );
    List<Blacklisted> findByCommissioningPlan_Id(Long id);

    List<Blacklisted> findByPartner_Id(Long id);

    Blacklisted findByPartner_IdAndCommissioningPlan_IdAndBlockedIsTrue(Long partnerID, Long commissioningPlanID);
    List<Blacklisted> findByPartner_MsisdnAndBlockedIsTrue(String msisdn);
}
