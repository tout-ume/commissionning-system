package sn.free.commissioning.repository;

import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sn.free.commissioning.domain.CommissioningPlan;
import sn.free.commissioning.domain.Frequency;
import sn.free.commissioning.domain.enumeration.CommissioningPlanState;
import sn.free.commissioning.domain.enumeration.CommissioningPlanType;
import sn.free.commissioning.domain.enumeration.FrequencyType;
import sn.free.commissioning.domain.enumeration.PaymentStrategy;
import sn.free.commissioning.service.dto.CommissioningPlanDTO;

/**
 * Spring Data SQL repository for the CommissioningPlan entity.
 */
@Repository
public interface CommissioningPlanRepository
    extends
        CommissioningPlanRepositoryWithBagRelationships,
        JpaRepository<CommissioningPlan, Long>,
        JpaSpecificationExecutor<CommissioningPlan> {
    default Optional<CommissioningPlan> findOneWithEagerRelationships(Long id) {
        return this.fetchBagRelationships(this.findById(id));
    }

    default List<CommissioningPlan> findAllWithEagerRelationships() {
        return this.fetchBagRelationships(this.findAll());
    }

    default Page<CommissioningPlan> findAllWithEagerRelationships(Pageable pageable) {
        return this.fetchBagRelationships(this.findAll(pageable));
    }

    List<CommissioningPlan> findByServices_NameIgnoreCase(String name);

    @EntityGraph(value = "graph.CommissioningPlan")
    List<CommissioningPlan> findByServices_NameIgnoreCaseAndStateAndCalculusFrequency_TypeEqualsAndBeginDateIsLessThanAndEndDateGreaterThan(
        String name,
        CommissioningPlanState state,
        FrequencyType frequencyType,
        Instant beginDate,
        Instant endDate
    );

    @EntityGraph(value = "graph.CommissioningPlan")
    List<CommissioningPlan> findByCalculusFrequency_Id(Long id);

    List<CommissioningPlan> findByIdIn(Collection<Long> ids);

    @EntityGraph(value = "graph.CommissioningPlan")
    List<CommissioningPlan> findByCommissioningPlanTypeAndState(CommissioningPlanType type, CommissioningPlanState state);

    @EntityGraph(value = "graph.CommissioningPlan")
    List<CommissioningPlan> findByPaymentStrategyAndState(PaymentStrategy paymentStrategy, CommissioningPlanState state);

    @EntityGraph(value = "graph.CommissioningPlan")
    Optional<CommissioningPlan> findByIdAndState(Long id, CommissioningPlanState state);
}
