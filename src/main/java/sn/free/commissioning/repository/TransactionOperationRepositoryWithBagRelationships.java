package sn.free.commissioning.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import sn.free.commissioning.domain.TransactionOperation;

public interface TransactionOperationRepositoryWithBagRelationships {
    Optional<TransactionOperation> fetchBagRelationships(Optional<TransactionOperation> transactionOperation);

    List<TransactionOperation> fetchBagRelationships(List<TransactionOperation> transactionOperations);

    Page<TransactionOperation> fetchBagRelationships(Page<TransactionOperation> transactionOperations);
}
