package sn.free.commissioning.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.free.commissioning.domain.CommissionAccount;
import sn.free.commissioning.domain.Partner;

/**
 * Spring Data SQL repository for the CommissionAccount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CommissionAccountRepository extends JpaRepository<CommissionAccount, Long>, JpaSpecificationExecutor<CommissionAccount> {
    @EntityGraph(value = "graph.CommissionAccount")
    Optional<CommissionAccount> findByPartner(Partner partner);

    @EntityGraph(value = "graph.CommissionAccount")
    Optional<CommissionAccount> findByPartner_Id(Long id);
}
