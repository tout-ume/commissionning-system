package sn.free.commissioning.repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.free.commissioning.domain.MissedTransactionOperationArchive;

/**
 * Spring Data SQL repository for the MissedTransactionOperationArchive entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MissedTransactionOperationArchiveRepository
    extends JpaRepository<MissedTransactionOperationArchive, Long>, JpaSpecificationExecutor<MissedTransactionOperationArchive> {}
