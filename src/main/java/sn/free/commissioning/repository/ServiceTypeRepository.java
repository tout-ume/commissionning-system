package sn.free.commissioning.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.free.commissioning.domain.PartnerProfile;
import sn.free.commissioning.domain.ServiceType;

/**
 * Spring Data SQL repository for the ServiceType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ServiceTypeRepository extends JpaRepository<ServiceType, Long>, JpaSpecificationExecutor<ServiceType> {
    ServiceType findOneByCode(String code);

    Optional<ServiceType> findByCode(String string);
}
