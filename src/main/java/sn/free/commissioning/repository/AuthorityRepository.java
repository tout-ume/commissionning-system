package sn.free.commissioning.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import sn.free.commissioning.domain.Authority;

/**
 * Spring Data JPA repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
    Optional<Authority> findFirstByNameIsIgnoreCase(String name);
}
