package sn.free.commissioning.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.free.commissioning.domain.PartnerProfile;

/**
 * Spring Data SQL repository for the PartnerProfile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PartnerProfileRepository extends JpaRepository<PartnerProfile, Long>, JpaSpecificationExecutor<PartnerProfile> {
    Optional<PartnerProfile> findByCode(String code);
}
