package sn.free.commissioning.repository;

import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.free.commissioning.domain.Payment;
import sn.free.commissioning.domain.enumeration.PaymentStatus;

/**
 * Spring Data SQL repository for the Payment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long>, JpaSpecificationExecutor<Payment> {
    List<Payment> findBySenderMsisdnAndPaymentStatus(String senderMsisdn, PaymentStatus paymentStatus);
}
