package sn.free.commissioning.repository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.annotations.QueryHints;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import sn.free.commissioning.domain.Commission;

/**
 * Utility repository to load bag relationships based on https://vladmihalcea.com/hibernate-multiplebagfetchexception/
 */
public class CommissionRepositoryWithBagRelationshipsImpl implements CommissionRepositoryWithBagRelationships {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Commission> fetchBagRelationships(Optional<Commission> commission) {
        return commission.map(this::fetchTransactionOperations);
    }

    @Override
    public Page<Commission> fetchBagRelationships(Page<Commission> commissions) {
        return new PageImpl<>(fetchBagRelationships(commissions.getContent()), commissions.getPageable(), commissions.getTotalElements());
    }

    @Override
    public List<Commission> fetchBagRelationships(List<Commission> commissions) {
        return Optional.of(commissions).map(this::fetchTransactionOperations).orElse(Collections.emptyList());
    }

    Commission fetchTransactionOperations(Commission result) {
        return entityManager
            .createQuery(
                "select commission from Commission commission left join fetch commission.transactionOperations where commission is :commission",
                Commission.class
            )
            .setParameter("commission", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<Commission> fetchTransactionOperations(List<Commission> commissions) {
        return entityManager
            .createQuery(
                "select distinct commission from Commission commission left join fetch commission.transactionOperations where commission in :commissions",
                Commission.class
            )
            .setParameter("commissions", commissions)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
    }
}
