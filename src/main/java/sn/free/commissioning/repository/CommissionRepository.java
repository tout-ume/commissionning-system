package sn.free.commissioning.repository;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;
import sn.free.commissioning.domain.Commission;
import sn.free.commissioning.domain.enumeration.CommissionPaymentStatus;

/**
 * Spring Data SQL repository for the Commission entity.
 */
@Repository
public interface CommissionRepository
    extends CommissionRepositoryWithBagRelationships, JpaRepository<Commission, Long>, JpaSpecificationExecutor<Commission> {
    Optional<Commission> findBySenderMsisdnAndConfigurationPlan_IdAndCalculationShortDate(
        String senderMsisdn,
        Long id,
        LocalDate calculationShortDate
    );

    default Optional<Commission> findOneWithEagerRelationships(Long id) {
        return this.fetchBagRelationships(this.findById(id));
    }

    List<Commission> findBySenderMsisdnAndSpare1In(String msisdn, Collection<String> dates);

    List<Commission> findBySenderMsisdnAndCalculationDateBetween(
        String senderMsisdn,
        Instant calculationDateStart,
        Instant calculationDateEnd
    );

    List<Commission> findBySenderMsisdn(String msisdn);

    default List<Commission> findAllWithEagerRelationships() {
        return this.fetchBagRelationships(this.findAll());
    }

    default Page<Commission> findAllWithEagerRelationships(Pageable pageable) {
        return this.fetchBagRelationships(this.findAll(pageable));
    }

    List<Commission> findByCommissionPaymentStatusAndSenderMsisdn(CommissionPaymentStatus commissionPaymentStatus, String senderMsisdn);

    List<Commission> findByConfigurationPlan_IdInAndSenderProfileAndCommissionPaymentStatusIn(
        Collection<Long> ids,
        String senderProfile,
        Collection<CommissionPaymentStatus> commissionPaymentStatuses
    );

    List<Commission> findByConfigurationPlan_IdInAndCommissionPaymentStatusInAndCalculationDateBetween(
        Collection<Long> ids,
        Collection<CommissionPaymentStatus> commissionPaymentStatuses,
        Instant calculationDateStart,
        Instant calculationDateEnd
    );
    List<Commission> findByIdIn(Collection<Long> ids);

    List<Commission> findByCommissionPaymentStatusAndSenderProfileNotInAndConfigurationPlan_CommissioningPlan_Id(
        CommissionPaymentStatus commissionPaymentStatus,
        Collection<String> senderProfiles,
        Long id
    );
    Optional<Commission> findBySpare1IgnoreCase(String tid);
}
