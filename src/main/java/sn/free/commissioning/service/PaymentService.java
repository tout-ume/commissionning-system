package sn.free.commissioning.service;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.commissioning.service.dto.PaymentDTO;
import sn.free.commissioning.service.dto.PaymentHistoricDTO;

/**
 * Service Interface for managing {@link sn.free.commissioning.domain.Payment}.
 */
public interface PaymentService {
    /**
     * Save a payment.
     *
     * @param paymentDTO the entity to save.
     * @return the persisted entity.
     */
    PaymentDTO save(PaymentDTO paymentDTO);

    /**
     * Updates a payment.
     *
     * @param paymentDTO the entity to update.
     * @return the persisted entity.
     */
    PaymentDTO update(PaymentDTO paymentDTO);

    /**
     * Partially updates a payment.
     *
     * @param paymentDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PaymentDTO> partialUpdate(PaymentDTO paymentDTO);

    /**
     * Get all the payments.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PaymentDTO> findAll(Pageable pageable);

    /**
     * Get the "id" payment.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PaymentDTO> findOne(Long id);

    /**
     * Delete the "id" payment.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * get a DP's network (DG and Rev) with their payment success.
     *
     * @param msisdn the DP's msisdn.
     * @return the list of the DP's network partners with their payment.
     */
    List<PaymentHistoricDTO> getDPNetworkPaymentSuccess(String msisdn);

    /**
     * get a DP's network (DG and Rev) with their payment success.
     *
     * @param paymentHistoricDTOList all payment success of the DP.
     * @param  codeProfile the code profile of the partner use two filter the result.
     * @return the list of the DP's network partners with their payment.
     */
    List<PaymentHistoricDTO> getDPNetworkPaymentSuccessFilter(List<PaymentHistoricDTO> paymentHistoricDTOList, String codeProfile);
}
