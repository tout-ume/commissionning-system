package sn.free.commissioning.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.*; // for static metamodels
import sn.free.commissioning.domain.CommissionType;
import sn.free.commissioning.repository.CommissionTypeRepository;
import sn.free.commissioning.service.criteria.CommissionTypeCriteria;
import sn.free.commissioning.service.dto.CommissionTypeDTO;
import sn.free.commissioning.service.mapper.CommissionTypeMapper;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link CommissionType} entities in the database.
 * The main input is a {@link CommissionTypeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CommissionTypeDTO} or a {@link Page} of {@link CommissionTypeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CommissionTypeQueryService extends QueryService<CommissionType> {

    private final Logger log = LoggerFactory.getLogger(CommissionTypeQueryService.class);

    private final CommissionTypeRepository commissionTypeRepository;

    private final CommissionTypeMapper commissionTypeMapper;

    public CommissionTypeQueryService(CommissionTypeRepository commissionTypeRepository, CommissionTypeMapper commissionTypeMapper) {
        this.commissionTypeRepository = commissionTypeRepository;
        this.commissionTypeMapper = commissionTypeMapper;
    }

    /**
     * Return a {@link List} of {@link CommissionTypeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CommissionTypeDTO> findByCriteria(CommissionTypeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CommissionType> specification = createSpecification(criteria);
        return commissionTypeMapper.toDto(commissionTypeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CommissionTypeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CommissionTypeDTO> findByCriteria(CommissionTypeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CommissionType> specification = createSpecification(criteria);
        return commissionTypeRepository.findAll(specification, page).map(commissionTypeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CommissionTypeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CommissionType> specification = createSpecification(criteria);
        return commissionTypeRepository.count(specification);
    }

    /**
     * Function to convert {@link CommissionTypeCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<CommissionType> createSpecification(CommissionTypeCriteria criteria) {
        Specification<CommissionType> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), CommissionType_.id));
            }
            if (criteria.getMinValue() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMinValue(), CommissionType_.minValue));
            }
            if (criteria.getMaxValue() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMaxValue(), CommissionType_.maxValue));
            }
            if (criteria.getTauxValue() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTauxValue(), CommissionType_.tauxValue));
            }
            if (criteria.getPalierValue() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPalierValue(), CommissionType_.palierValue));
            }
            if (criteria.getCommissionTypeType() != null) {
                specification = specification.and(buildSpecification(criteria.getCommissionTypeType(), CommissionType_.commissionTypeType));
            }
            if (criteria.getPalierValueType() != null) {
                specification = specification.and(buildSpecification(criteria.getPalierValueType(), CommissionType_.palierValueType));
            }
            if (criteria.getIsInfinity() != null) {
                specification = specification.and(buildSpecification(criteria.getIsInfinity(), CommissionType_.isInfinity));
            }
            if (criteria.getSpare1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare1(), CommissionType_.spare1));
            }
            if (criteria.getSpare2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare2(), CommissionType_.spare2));
            }
            if (criteria.getSpare3() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare3(), CommissionType_.spare3));
            }
            if (criteria.getConfigurationPlanId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getConfigurationPlanId(),
                            root -> root.join(CommissionType_.configurationPlan, JoinType.LEFT).get(ConfigurationPlan_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
