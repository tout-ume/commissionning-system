package sn.free.commissioning.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.*; // for static metamodels
import sn.free.commissioning.domain.CommissionAccount;
import sn.free.commissioning.repository.CommissionAccountRepository;
import sn.free.commissioning.service.criteria.CommissionAccountCriteria;
import sn.free.commissioning.service.dto.CommissionAccountDTO;
import sn.free.commissioning.service.mapper.CommissionAccountMapper;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link CommissionAccount} entities in the database.
 * The main input is a {@link CommissionAccountCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CommissionAccountDTO} or a {@link Page} of {@link CommissionAccountDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CommissionAccountQueryService extends QueryService<CommissionAccount> {

    private final Logger log = LoggerFactory.getLogger(CommissionAccountQueryService.class);

    private final CommissionAccountRepository commissionAccountRepository;

    private final CommissionAccountMapper commissionAccountMapper;

    public CommissionAccountQueryService(
        CommissionAccountRepository commissionAccountRepository,
        CommissionAccountMapper commissionAccountMapper
    ) {
        this.commissionAccountRepository = commissionAccountRepository;
        this.commissionAccountMapper = commissionAccountMapper;
    }

    /**
     * Return a {@link List} of {@link CommissionAccountDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CommissionAccountDTO> findByCriteria(CommissionAccountCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CommissionAccount> specification = createSpecification(criteria);
        return commissionAccountMapper.toDto(commissionAccountRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CommissionAccountDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CommissionAccountDTO> findByCriteria(CommissionAccountCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CommissionAccount> specification = createSpecification(criteria);
        return commissionAccountRepository.findAll(specification, page).map(commissionAccountMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CommissionAccountCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CommissionAccount> specification = createSpecification(criteria);
        return commissionAccountRepository.count(specification);
    }

    /**
     * Function to convert {@link CommissionAccountCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<CommissionAccount> createSpecification(CommissionAccountCriteria criteria) {
        Specification<CommissionAccount> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), CommissionAccount_.id));
            }
            if (criteria.getCalculatedBalance() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getCalculatedBalance(), CommissionAccount_.calculatedBalance));
            }
            if (criteria.getPaidBalance() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPaidBalance(), CommissionAccount_.paidBalance));
            }
            if (criteria.getSpare1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare1(), CommissionAccount_.spare1));
            }
            if (criteria.getSpare2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare2(), CommissionAccount_.spare2));
            }
            if (criteria.getSpare3() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare3(), CommissionAccount_.spare3));
            }
            if (criteria.getPartnerId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getPartnerId(),
                            root -> root.join(CommissionAccount_.partner, JoinType.LEFT).get(Partner_.id)
                        )
                    );
            }
            if (criteria.getCommissionId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getCommissionId(),
                            root -> root.join(CommissionAccount_.commissions, JoinType.LEFT).get(Commission_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
