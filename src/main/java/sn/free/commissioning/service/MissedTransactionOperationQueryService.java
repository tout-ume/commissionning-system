package sn.free.commissioning.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.*; // for static metamodels
import sn.free.commissioning.domain.MissedTransactionOperation;
import sn.free.commissioning.repository.MissedTransactionOperationRepository;
import sn.free.commissioning.service.criteria.MissedTransactionOperationCriteria;
import sn.free.commissioning.service.dto.MissedTransactionOperationDTO;
import sn.free.commissioning.service.mapper.MissedTransactionOperationMapper;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link MissedTransactionOperation} entities in the database.
 * The main input is a {@link MissedTransactionOperationCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MissedTransactionOperationDTO} or a {@link Page} of {@link MissedTransactionOperationDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MissedTransactionOperationQueryService extends QueryService<MissedTransactionOperation> {

    private final Logger log = LoggerFactory.getLogger(MissedTransactionOperationQueryService.class);

    private final MissedTransactionOperationRepository missedTransactionOperationRepository;

    private final MissedTransactionOperationMapper missedTransactionOperationMapper;

    public MissedTransactionOperationQueryService(
        MissedTransactionOperationRepository missedTransactionOperationRepository,
        MissedTransactionOperationMapper missedTransactionOperationMapper
    ) {
        this.missedTransactionOperationRepository = missedTransactionOperationRepository;
        this.missedTransactionOperationMapper = missedTransactionOperationMapper;
    }

    /**
     * Return a {@link List} of {@link MissedTransactionOperationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MissedTransactionOperationDTO> findByCriteria(MissedTransactionOperationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<MissedTransactionOperation> specification = createSpecification(criteria);
        return missedTransactionOperationMapper.toDto(missedTransactionOperationRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link MissedTransactionOperationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MissedTransactionOperationDTO> findByCriteria(MissedTransactionOperationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<MissedTransactionOperation> specification = createSpecification(criteria);
        return missedTransactionOperationRepository.findAll(specification, page).map(missedTransactionOperationMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MissedTransactionOperationCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<MissedTransactionOperation> specification = createSpecification(criteria);
        return missedTransactionOperationRepository.count(specification);
    }

    /**
     * Function to convert {@link MissedTransactionOperationCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<MissedTransactionOperation> createSpecification(MissedTransactionOperationCriteria criteria) {
        Specification<MissedTransactionOperation> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), MissedTransactionOperation_.id));
            }
            if (criteria.getAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAmount(), MissedTransactionOperation_.amount));
            }
            if (criteria.getSubsMsisdn() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getSubsMsisdn(), MissedTransactionOperation_.subsMsisdn));
            }
            if (criteria.getAgentMsisdn() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getAgentMsisdn(), MissedTransactionOperation_.agentMsisdn));
            }
            if (criteria.getTypeTransaction() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getTypeTransaction(), MissedTransactionOperation_.typeTransaction));
            }
            if (criteria.getCreatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedAt(), MissedTransactionOperation_.createdAt));
            }
            if (criteria.getTransactionStatus() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getTransactionStatus(), MissedTransactionOperation_.transactionStatus)
                    );
            }
            if (criteria.getSenderZone() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getSenderZone(), MissedTransactionOperation_.senderZone));
            }
            if (criteria.getSenderProfile() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getSenderProfile(), MissedTransactionOperation_.senderProfile));
            }
            if (criteria.getCodeTerritory() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getCodeTerritory(), MissedTransactionOperation_.codeTerritory));
            }
            if (criteria.getSubType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubType(), MissedTransactionOperation_.subType));
            }
            if (criteria.getOperationDate() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getOperationDate(), MissedTransactionOperation_.operationDate));
            }
            if (criteria.getIsFraud() != null) {
                specification = specification.and(buildSpecification(criteria.getIsFraud(), MissedTransactionOperation_.isFraud));
            }
            if (criteria.getTaggedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTaggedAt(), MissedTransactionOperation_.taggedAt));
            }
            if (criteria.getFraudSource() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getFraudSource(), MissedTransactionOperation_.fraudSource));
            }
            if (criteria.getComment() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComment(), MissedTransactionOperation_.comment));
            }
            if (criteria.getFalsePositiveDetectedAt() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getFalsePositiveDetectedAt(), MissedTransactionOperation_.falsePositiveDetectedAt)
                    );
            }
            if (criteria.getTid() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTid(), MissedTransactionOperation_.tid));
            }
            if (criteria.getParentMsisdn() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getParentMsisdn(), MissedTransactionOperation_.parentMsisdn));
            }
            if (criteria.getFctDt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFctDt(), MissedTransactionOperation_.fctDt));
            }
            if (criteria.getParentId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getParentId(), MissedTransactionOperation_.parentId));
            }
            if (criteria.getCanceledAt() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getCanceledAt(), MissedTransactionOperation_.canceledAt));
            }
            if (criteria.getCanceledId() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getCanceledId(), MissedTransactionOperation_.canceledId));
            }
            if (criteria.getProductId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProductId(), MissedTransactionOperation_.productId));
            }
        }
        return specification;
    }
}
