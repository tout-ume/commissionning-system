package sn.free.commissioning.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.commissioning.service.dto.CommissionTypeDTO;

/**
 * Service Interface for managing {@link sn.free.commissioning.domain.CommissionType}.
 */
public interface CommissionTypeService {
    /**
     * Save a commissionType.
     *
     * @param commissionTypeDTO the entity to save.
     * @return the persisted entity.
     */
    CommissionTypeDTO save(CommissionTypeDTO commissionTypeDTO);

    /**
     * Updates a commissionType.
     *
     * @param commissionTypeDTO the entity to update.
     * @return the persisted entity.
     */
    CommissionTypeDTO update(CommissionTypeDTO commissionTypeDTO);

    /**
     * Partially updates a commissionType.
     *
     * @param commissionTypeDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<CommissionTypeDTO> partialUpdate(CommissionTypeDTO commissionTypeDTO);

    /**
     * Get all the commissionTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CommissionTypeDTO> findAll(Pageable pageable);

    /**
     * Get the "id" commissionType.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CommissionTypeDTO> findOne(Long id);

    /**
     * Delete the "id" commissionType.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
