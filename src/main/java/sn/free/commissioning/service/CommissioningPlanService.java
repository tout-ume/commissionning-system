package sn.free.commissioning.service;

import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.commissioning.domain.CommissioningPlan;
import sn.free.commissioning.domain.Frequency;
import sn.free.commissioning.domain.exceptions.NoCommissionPlanException;
import sn.free.commissioning.domain.exceptions.PartnerNotFoundException;
import sn.free.commissioning.service.dto.CommissioningPlanDTO;

/**
 * Service Interface for managing {@link sn.free.commissioning.domain.CommissioningPlan}.
 */
public interface CommissioningPlanService {
    /**
     * Save a commissioningPlan.
     *
     * @param commissioningPlanDTO the entity to save.
     * @return the persisted entity.
     */
    CommissioningPlanDTO save(CommissioningPlanDTO commissioningPlanDTO);

    /**
     * Updates a commissioningPlan.
     *
     * @param commissioningPlanDTO the entity to update.
     * @return the persisted entity.
     */
    CommissioningPlanDTO update(CommissioningPlanDTO commissioningPlanDTO);

    /**
     * Updates the state of a commissioningPlan.
     *
     * @param commissioningPlanDTO the entity to update.
     * @return the persisted entity.
     */
    CommissioningPlanDTO updateState(CommissioningPlanDTO commissioningPlanDTO);

    /**
     * Partially updates a commissioningPlan.
     *
     * @param commissioningPlanDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<CommissioningPlanDTO> partialUpdate(CommissioningPlanDTO commissioningPlanDTO);

    /**
     * Get all the commissioningPlans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CommissioningPlanDTO> findAll(Pageable pageable);

    /**
     * Get all the commissioningPlans with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CommissioningPlanDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" commissioningPlan.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CommissioningPlanDTO> findOne(Long id);

    /**
     * Delete the "id" commissioningPlan.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<CommissioningPlan> findByServices_Name(String name);

    /**
     * Get all commissioningPlans that include a service name, are IN_USE and rolling.
     *
     * @param serviceName the transaction service type to search.
     * @param transactionDate the transaction date.
     */
    List<CommissioningPlan> getValidInstantlyPlansByServiceName(String serviceName, Instant transactionDate);

    /**
     * Get all commissioningPlans  by frequency.
     *
     * @param frequency .
     * @return  a list of commissionning plan
     */
    List<CommissioningPlan> getCommissionningPlansByFrequency(Frequency frequency);

    /**
     * Get all commissioningPlans  by msisdn of partner .
     *
     * @param msisdn .
     * @return  a list of commissionning plan
     */
    List<CommissioningPlanDTO> getCommissionningPlansByPartnerProfile(String msisdn)
        throws PartnerNotFoundException, NoCommissionPlanException;

    /**
     * Get the  commissioningPlans by a list commissioningPlanIdList .
     *
     * @param ids the list id of the entity.
     * @return a list of commissionning plan.
     */
    List<CommissioningPlanDTO> findByIdIn(Collection<Long> ids);

    /**
     * Get a commissioningPlans blocked of a Partner given .
     *
     * @param msisdn of the partner.
     * @return a list of commissionning plan blocked.
     */
    List<CommissioningPlanDTO> getCommissioningPlansAndBlockedIsTrue(String msisdn);

    /**
     * Get the commissioningPlans to pay today.
     *
     * @return a list of commissionning plan.
     */
    List<CommissioningPlanDTO> getCommissioninPlansToPayToday();
}
