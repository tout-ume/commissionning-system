package sn.free.commissioning.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.commissioning.service.dto.TerritoryDTO;

/**
 * Service Interface for managing {@link sn.free.commissioning.domain.Territory}.
 */
public interface TerritoryService {
    /**
     * Save a territory.
     *
     * @param territoryDTO the entity to save.
     * @return the persisted entity.
     */
    TerritoryDTO save(TerritoryDTO territoryDTO);

    /**
     * Updates a territory.
     *
     * @param territoryDTO the entity to update.
     * @return the persisted entity.
     */
    TerritoryDTO update(TerritoryDTO territoryDTO);

    /**
     * Partially updates a territory.
     *
     * @param territoryDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<TerritoryDTO> partialUpdate(TerritoryDTO territoryDTO);

    /**
     * Get all the territories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TerritoryDTO> findAll(Pageable pageable);

    /**
     * Get the "id" territory.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TerritoryDTO> findOne(Long id);

    /**
     * Delete the "id" territory.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
