package sn.free.commissioning.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.commissioning.service.dto.AuthorityMirrorDTO;

/**
 * Service Interface for managing {@link sn.free.commissioning.domain.AuthorityMirror}.
 */
public interface AuthorityMirrorService {
    /**
     * Save a authorityMirror.
     *
     * @param authorityMirrorDTO the entity to save.
     * @return the persisted entity.
     */
    AuthorityMirrorDTO save(AuthorityMirrorDTO authorityMirrorDTO);

    /**
     * Updates a authorityMirror.
     *
     * @param authorityMirrorDTO the entity to update.
     * @return the persisted entity.
     */
    AuthorityMirrorDTO update(AuthorityMirrorDTO authorityMirrorDTO);

    /**
     * Partially updates a authorityMirror.
     *
     * @param authorityMirrorDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<AuthorityMirrorDTO> partialUpdate(AuthorityMirrorDTO authorityMirrorDTO);

    /**
     * Get all the authorityMirrors.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AuthorityMirrorDTO> findAll(Pageable pageable);

    /**
     * Get the "id" authorityMirror.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AuthorityMirrorDTO> findOne(Long id);

    /**
     * Delete the "id" authorityMirror.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
