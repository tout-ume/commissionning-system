package sn.free.commissioning.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.*; // for static metamodels
import sn.free.commissioning.domain.CommissioningPlan;
import sn.free.commissioning.repository.CommissioningPlanRepository;
import sn.free.commissioning.service.criteria.CommissioningPlanCriteria;
import sn.free.commissioning.service.dto.CommissioningPlanDTO;
import sn.free.commissioning.service.mapper.CommissioningPlanMapper;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link CommissioningPlan} entities in the database.
 * The main input is a {@link CommissioningPlanCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CommissioningPlanDTO} or a {@link Page} of {@link CommissioningPlanDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CommissioningPlanQueryService extends QueryService<CommissioningPlan> {

    private final Logger log = LoggerFactory.getLogger(CommissioningPlanQueryService.class);

    private final CommissioningPlanRepository commissioningPlanRepository;

    private final CommissioningPlanMapper commissioningPlanMapper;

    public CommissioningPlanQueryService(
        CommissioningPlanRepository commissioningPlanRepository,
        CommissioningPlanMapper commissioningPlanMapper
    ) {
        this.commissioningPlanRepository = commissioningPlanRepository;
        this.commissioningPlanMapper = commissioningPlanMapper;
    }

    /**
     * Return a {@link List} of {@link CommissioningPlanDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CommissioningPlanDTO> findByCriteria(CommissioningPlanCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CommissioningPlan> specification = createSpecification(criteria);
        return commissioningPlanMapper.toDto(commissioningPlanRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CommissioningPlanDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CommissioningPlanDTO> findByCriteria(CommissioningPlanCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CommissioningPlan> specification = createSpecification(criteria);
        return commissioningPlanRepository.findAll(specification, page).map(commissioningPlanMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CommissioningPlanCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CommissioningPlan> specification = createSpecification(criteria);
        return commissioningPlanRepository.count(specification);
    }

    /**
     * Function to convert {@link CommissioningPlanCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<CommissioningPlan> createSpecification(CommissioningPlanCriteria criteria) {
        Specification<CommissioningPlan> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), CommissioningPlan_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), CommissioningPlan_.name));
            }
            if (criteria.getBeginDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getBeginDate(), CommissioningPlan_.beginDate));
            }
            if (criteria.getEndDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEndDate(), CommissioningPlan_.endDate));
            }
            if (criteria.getCreationDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreationDate(), CommissioningPlan_.creationDate));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), CommissioningPlan_.createdBy));
            }
            if (criteria.getArchiveDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getArchiveDate(), CommissioningPlan_.archiveDate));
            }
            if (criteria.getArchivedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getArchivedBy(), CommissioningPlan_.archivedBy));
            }
            if (criteria.getPaymentStrategy() != null) {
                specification = specification.and(buildSpecification(criteria.getPaymentStrategy(), CommissioningPlan_.paymentStrategy));
            }
            if (criteria.getState() != null) {
                specification = specification.and(buildSpecification(criteria.getState(), CommissioningPlan_.state));
            }
            if (criteria.getCommissioningPlanType() != null) {
                specification =
                    specification.and(buildSpecification(criteria.getCommissioningPlanType(), CommissioningPlan_.commissioningPlanType));
            }
            if (criteria.getSpare1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare1(), CommissioningPlan_.spare1));
            }
            if (criteria.getSpare2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare2(), CommissioningPlan_.spare2));
            }
            if (criteria.getSpare3() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare3(), CommissioningPlan_.spare3));
            }
            if (criteria.getPaymentFrequencyId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getPaymentFrequencyId(),
                            root -> root.join(CommissioningPlan_.paymentFrequency, JoinType.LEFT).get(Frequency_.id)
                        )
                    );
            }
            if (criteria.getCalculusFrequencyId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getCalculusFrequencyId(),
                            root -> root.join(CommissioningPlan_.calculusFrequency, JoinType.LEFT).get(Frequency_.id)
                        )
                    );
            }
            if (criteria.getCalculusPeriodId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getCalculusPeriodId(),
                            root -> root.join(CommissioningPlan_.calculusPeriod, JoinType.LEFT).get(Period_.id)
                        )
                    );
            }
            if (criteria.getPaymentPeriodId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getPaymentPeriodId(),
                            root -> root.join(CommissioningPlan_.paymentPeriod, JoinType.LEFT).get(Period_.id)
                        )
                    );
            }
            if (criteria.getServicesId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getServicesId(),
                            root -> root.join(CommissioningPlan_.services, JoinType.LEFT).get(ServiceType_.id)
                        )
                    );
            }
            if (criteria.getConfigurationPlanId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getConfigurationPlanId(),
                            root -> root.join(CommissioningPlan_.configurationPlans, JoinType.LEFT).get(ConfigurationPlan_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
