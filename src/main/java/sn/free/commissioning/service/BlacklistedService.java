package sn.free.commissioning.service;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.commissioning.service.dto.BlacklistedDTO;
import sn.free.commissioning.service.dto.CommissioningPlanDTO;

/**
 * Service Interface for managing {@link sn.free.commissioning.domain.Blacklisted}.
 */
public interface BlacklistedService {
    /**
     * Save a blacklisted.
     *
     * @param blacklistedDTO the entity to save.
     * @return the persisted entity.
     */
    BlacklistedDTO save(BlacklistedDTO blacklistedDTO);

    /**
     * Save a blacklisted.
     *
     * @param partnerReason the msisdn of partner to blacklist and the reason why.
     * @param commissioningPlanIDList the commissioningPlan list to blacklist.
     * @return the persisted entity.
     */
    List<BlacklistedDTO> create(HashMap<String, String> partnerReason, List<Long> commissioningPlanIDList);

    /**
     * Unblock a partner for all CommisssionningPlan.
     *
     * @param msisdn the msisdn of partner to unblacklisted.
     */
    void unblockAPartner(String msisdn);

    /**
     * Unblacklisted a partner for a CommisssionningPlan.
     *
     * @param msisdn the msisdn of partner to unblock.
     * @param commissioningPlanID the commissioningPlanID of partner to unblock.
     */
    Long unblockOneCommissioningPlan(String msisdn, Long commissioningPlanID);

    /**
     * Unblock a partner for a list of CommisssionningPlan.
     *
     * @param msisdn the msisdn of partner to unblock.
     * @param commissioningPlanDTOS the list of commissioningPlans to unblock.
     */
    List<Long> unblockAListOfCommissioningPlansForPartner(String msisdn, List<CommissioningPlanDTO> commissioningPlanDTOS);

    /**
     * Updates a blacklisted.
     *
     * @param blacklistedDTO the entity to update.
     * @return the persisted entity.
     */
    BlacklistedDTO update(BlacklistedDTO blacklistedDTO);

    /**
     * Partially updates a blacklisted.
     *
     * @param blacklistedDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<BlacklistedDTO> partialUpdate(BlacklistedDTO blacklistedDTO);

    /**
     * Get all the blacklisteds.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BlacklistedDTO> findAll(Pageable pageable);

    /**
     * Get the "id" blacklisted.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BlacklistedDTO> findOne(Long id);

    /**
     * Delete the "id" blacklisted.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Get blacklisteds blocked(true) of a partner .
     *
     * @param msisdn the msisdn of the partner.
     * @return the list of Blacklisted.
     */

    List<BlacklistedDTO> findByPartner_MsisdnAndBlockedIsTrue(String msisdn);
}
