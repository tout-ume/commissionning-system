package sn.free.commissioning.service;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.service.dto.CommissioningPlanDTO;
import sn.free.commissioning.service.dto.PartnerDTO;
import sn.free.commissioning.service.dto.PartnerForPaymentDTO;

/**
 * Service Interface for managing {@link sn.free.commissioning.domain.Partner}.
 */
public interface PartnerService {
    /**
     * Save a partner.
     *
     * @param partnerDTO the entity to save.
     * @return the persisted entity.
     */
    PartnerDTO save(PartnerDTO partnerDTO);

    /**
     * Updates a partner.
     *
     * @param partnerDTO the entity to update.
     * @return the persisted entity.
     */
    PartnerDTO update(PartnerDTO partnerDTO);

    /**
     * Partially updates a partner.
     *
     * @param partnerDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PartnerDTO> partialUpdate(PartnerDTO partnerDTO);

    /**
     * Get all the partners.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PartnerDTO> findAll(Pageable pageable);

    /**
     * Get all the partners with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PartnerDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" partner.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PartnerDTO> findOne(Long id);

    /**
     * Delete the "id" partner.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<Partner> getPartnerTree(String msisdn, List<Partner> partners);

    /**
     * get a DP's network (DG and Rev) with their commissions to be paid.
     *
     * @param msisdn the DP's msisdn.
     * @return the list of the DP's network partners with their commissions.
     */
    List<PartnerForPaymentDTO> getDPNetwork(String msisdn);

    /**
     * get a DP's network (DG or Rev) with their commissions to be paid.
     *
     * @param codeProfile the code Partners.
     * @param partnerForPaymentDTOList DP Network(DG and REV).
     * @return the list of the DP's network partners containing the codeProfile with their commissions.
     */
    List<PartnerForPaymentDTO> getDPNetworkFilter(List<PartnerForPaymentDTO> partnerForPaymentDTOList, String codeProfile);

    /**
     * check if the partner profile is configuring in the CommisssioningPlan .
     *
     * @param partnerDTO the Partner.
     * @param commissioningPlanDTO the CommissioningPlan.
     * @return the list of the DP's network partners with their commissions.
     */
    boolean checkIfPartnerProfileISInCommissioningPlan(PartnerDTO partnerDTO, CommissioningPlanDTO commissioningPlanDTO);

    /**
     * FInd a partner by msisdn.
     *
     * @param msisdn the partner's msisdn.
     * @return the entity.
     */
    Partner findByMsisdn(String msisdn);
}
