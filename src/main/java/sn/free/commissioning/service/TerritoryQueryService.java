package sn.free.commissioning.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.*; // for static metamodels
import sn.free.commissioning.domain.Territory;
import sn.free.commissioning.repository.TerritoryRepository;
import sn.free.commissioning.service.criteria.TerritoryCriteria;
import sn.free.commissioning.service.dto.TerritoryDTO;
import sn.free.commissioning.service.mapper.TerritoryMapper;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Territory} entities in the database.
 * The main input is a {@link TerritoryCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TerritoryDTO} or a {@link Page} of {@link TerritoryDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TerritoryQueryService extends QueryService<Territory> {

    private final Logger log = LoggerFactory.getLogger(TerritoryQueryService.class);

    private final TerritoryRepository territoryRepository;

    private final TerritoryMapper territoryMapper;

    public TerritoryQueryService(TerritoryRepository territoryRepository, TerritoryMapper territoryMapper) {
        this.territoryRepository = territoryRepository;
        this.territoryMapper = territoryMapper;
    }

    /**
     * Return a {@link List} of {@link TerritoryDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TerritoryDTO> findByCriteria(TerritoryCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Territory> specification = createSpecification(criteria);
        return territoryMapper.toDto(territoryRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TerritoryDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TerritoryDTO> findByCriteria(TerritoryCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Territory> specification = createSpecification(criteria);
        return territoryRepository.findAll(specification, page).map(territoryMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TerritoryCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Territory> specification = createSpecification(criteria);
        return territoryRepository.count(specification);
    }

    /**
     * Function to convert {@link TerritoryCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Territory> createSpecification(TerritoryCriteria criteria) {
        Specification<Territory> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Territory_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Territory_.name));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Territory_.description));
            }
            if (criteria.getState() != null) {
                specification = specification.and(buildStringSpecification(criteria.getState(), Territory_.state));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), Territory_.createdBy));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), Territory_.createdDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), Territory_.lastModifiedBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), Territory_.lastModifiedDate));
            }
            if (criteria.getZoneId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getZoneId(), root -> root.join(Territory_.zones, JoinType.LEFT).get(Zone_.id))
                    );
            }
        }
        return specification;
    }
}
