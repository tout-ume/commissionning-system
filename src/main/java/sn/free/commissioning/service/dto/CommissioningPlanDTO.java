package sn.free.commissioning.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;
import sn.free.commissioning.domain.enumeration.CommissioningPlanState;
import sn.free.commissioning.domain.enumeration.CommissioningPlanType;
import sn.free.commissioning.domain.enumeration.PaymentStrategy;

/**
 * A DTO for the {@link sn.free.commissioning.domain.CommissioningPlan} entity.
 */
public class CommissioningPlanDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Instant beginDate;

    @NotNull
    private Instant endDate;

    @NotNull
    private Instant creationDate;

    @NotNull
    private String createdBy;

    private Instant archiveDate;

    private String archivedBy;

    private PaymentStrategy paymentStrategy;

    @NotNull
    private CommissioningPlanState state;

    private CommissioningPlanType commissioningPlanType;

    private String spare1;

    private String spare2;

    private String spare3;

    private FrequencyDTO paymentFrequency;

    private FrequencyDTO calculusFrequency;

    private PeriodDTO calculusPeriod;

    private PeriodDTO paymentPeriod;

    private Set<ServiceTypeDTO> services = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Instant getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Instant beginDate) {
        this.beginDate = beginDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Instant getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getArchiveDate() {
        return archiveDate;
    }

    public void setArchiveDate(Instant archiveDate) {
        this.archiveDate = archiveDate;
    }

    public String getArchivedBy() {
        return archivedBy;
    }

    public void setArchivedBy(String archivedBy) {
        this.archivedBy = archivedBy;
    }

    public PaymentStrategy getPaymentStrategy() {
        return paymentStrategy;
    }

    public void setPaymentStrategy(PaymentStrategy paymentStrategy) {
        this.paymentStrategy = paymentStrategy;
    }

    public CommissioningPlanState getState() {
        return state;
    }

    public void setState(CommissioningPlanState state) {
        this.state = state;
    }

    public CommissioningPlanType getCommissioningPlanType() {
        return commissioningPlanType;
    }

    public void setCommissioningPlanType(CommissioningPlanType commissioningPlanType) {
        this.commissioningPlanType = commissioningPlanType;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return spare3;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public FrequencyDTO getPaymentFrequency() {
        return paymentFrequency;
    }

    public void setPaymentFrequency(FrequencyDTO paymentFrequency) {
        this.paymentFrequency = paymentFrequency;
    }

    public FrequencyDTO getCalculusFrequency() {
        return calculusFrequency;
    }

    public void setCalculusFrequency(FrequencyDTO calculusFrequency) {
        this.calculusFrequency = calculusFrequency;
    }

    public PeriodDTO getCalculusPeriod() {
        return calculusPeriod;
    }

    public void setCalculusPeriod(PeriodDTO calculusPeriod) {
        this.calculusPeriod = calculusPeriod;
    }

    public PeriodDTO getPaymentPeriod() {
        return paymentPeriod;
    }

    public void setPaymentPeriod(PeriodDTO paymentPeriod) {
        this.paymentPeriod = paymentPeriod;
    }

    public Set<ServiceTypeDTO> getServices() {
        return services;
    }

    private Set<ConfigurationPlanDTO> configurationPlans = new HashSet<>();

    public void setServices(Set<ServiceTypeDTO> services) {
        this.services = services;
    }

    public void setConfigurationPlans(Set<ConfigurationPlanDTO> configurationPlans) {
        this.configurationPlans = configurationPlans;
    }

    public Set<ConfigurationPlanDTO> getConfigurationPlans() {
        return configurationPlans;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommissioningPlanDTO)) {
            return false;
        }

        CommissioningPlanDTO commissioningPlanDTO = (CommissioningPlanDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, commissioningPlanDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommissioningPlanDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", beginDate='" + getBeginDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", creationDate='" + getCreationDate() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", archiveDate='" + getArchiveDate() + "'" +
            ", archivedBy='" + getArchivedBy() + "'" +
            ", paymentStrategy='" + getPaymentStrategy() + "'" +
            ", configurationPlans='" + getConfigurationPlans() + "'" +
            ", state='" + getState() + "'" +
            ", commissioningPlanType='" + getCommissioningPlanType() + "'" +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            ", paymentFrequency=" + getPaymentFrequency() +
            ", calculusFrequency=" + getCalculusFrequency() +
            ", calculusPeriod=" + getCalculusPeriod() +
            ", paymentPeriod=" + getPaymentPeriod() +
            ", services=" + getServices() +
            "}";
    }
}
