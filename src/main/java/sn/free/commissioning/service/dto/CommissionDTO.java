package sn.free.commissioning.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;
import sn.free.commissioning.domain.enumeration.CommissionPaymentStatus;
import sn.free.commissioning.domain.enumeration.CommissioningPlanType;

/**
 * A DTO for the {@link sn.free.commissioning.domain.Commission} entity.
 */
public class CommissionDTO implements Serializable {

    private Long id;

    @NotNull
    private Double amount;

    private Instant calculatedAt;

    private Instant calculationDate;

    private LocalDate calculationShortDate;

    private String senderMsisdn;

    private String senderProfile;

    private String serviceType;

    private CommissionPaymentStatus commissionPaymentStatus;

    private CommissioningPlanType commissioningPlanType;

    private Double globalNetworkCommissionAmount;

    private Double transactionsAmount;

    private Double fraudAmount;

    private String spare1;

    private String spare2;

    private String spare3;

    private CommissionAccountDTO commissionAccount;

    private Set<TransactionOperationDTO> transactionOperations = new HashSet<>();

    private ConfigurationPlanDTO configurationPlan;

    private PaymentDTO payment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Instant getCalculatedAt() {
        return calculatedAt;
    }

    public void setCalculatedAt(Instant calculatedAt) {
        this.calculatedAt = calculatedAt;
    }

    public Instant getCalculationDate() {
        return calculationDate;
    }

    public void setCalculationDate(Instant calculationDate) {
        this.calculationDate = calculationDate;
    }

    public LocalDate getCalculationShortDate() {
        return calculationShortDate;
    }

    public void setCalculationShortDate(LocalDate calculationShortDate) {
        this.calculationShortDate = calculationShortDate;
    }

    public String getSenderMsisdn() {
        return senderMsisdn;
    }

    public void setSenderMsisdn(String senderMsisdn) {
        this.senderMsisdn = senderMsisdn;
    }

    public String getSenderProfile() {
        return senderProfile;
    }

    public void setSenderProfile(String senderProfile) {
        this.senderProfile = senderProfile;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public CommissionPaymentStatus getCommissionPaymentStatus() {
        return commissionPaymentStatus;
    }

    public void setCommissionPaymentStatus(CommissionPaymentStatus commissionPaymentStatus) {
        this.commissionPaymentStatus = commissionPaymentStatus;
    }

    public CommissioningPlanType getCommissioningPlanType() {
        return commissioningPlanType;
    }

    public void setCommissioningPlanType(CommissioningPlanType commissioningPlanType) {
        this.commissioningPlanType = commissioningPlanType;
    }

    public Double getGlobalNetworkCommissionAmount() {
        return globalNetworkCommissionAmount;
    }

    public void setGlobalNetworkCommissionAmount(Double globalNetworkCommissionAmount) {
        this.globalNetworkCommissionAmount = globalNetworkCommissionAmount;
    }

    public Double getTransactionsAmount() {
        return transactionsAmount;
    }

    public void setTransactionsAmount(Double transactionsAmount) {
        this.transactionsAmount = transactionsAmount;
    }

    public Double getFraudAmount() {
        return fraudAmount;
    }

    public void setFraudAmount(Double fraudAmount) {
        this.fraudAmount = fraudAmount;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return spare3;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public CommissionAccountDTO getCommissionAccount() {
        return commissionAccount;
    }

    public void setCommissionAccount(CommissionAccountDTO commissionAccount) {
        this.commissionAccount = commissionAccount;
    }

    public Set<TransactionOperationDTO> getTransactionOperations() {
        return transactionOperations;
    }

    public void setTransactionOperations(Set<TransactionOperationDTO> transactionOperations) {
        this.transactionOperations = transactionOperations;
    }

    public ConfigurationPlanDTO getConfigurationPlan() {
        return configurationPlan;
    }

    public void setConfigurationPlan(ConfigurationPlanDTO configurationPlan) {
        this.configurationPlan = configurationPlan;
    }

    public PaymentDTO getPayment() {
        return payment;
    }

    public void setPayment(PaymentDTO payment) {
        this.payment = payment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommissionDTO)) {
            return false;
        }

        CommissionDTO commissionDTO = (CommissionDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, commissionDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommissionDTO{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", calculatedAt='" + getCalculatedAt() + "'" +
            ", calculationDate='" + getCalculationDate() + "'" +
            ", calculationShortDate='" + getCalculationShortDate() + "'" +
            ", senderMsisdn='" + getSenderMsisdn() + "'" +
            ", senderProfile='" + getSenderProfile() + "'" +
            ", serviceType='" + getServiceType() + "'" +
            ", commissionPaymentStatus='" + getCommissionPaymentStatus() + "'" +
            ", commissioningPlanType='" + getCommissioningPlanType() + "'" +
            ", globalNetworkCommissionAmount=" + getGlobalNetworkCommissionAmount() +
            ", transactionsAmount=" + getTransactionsAmount() +
            ", fraudAmount=" + getFraudAmount() +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            ", commissionAccount=" + getCommissionAccount() +
            ", transactionOperations=" + getTransactionOperations() +
            ", configurationPlan=" + getConfigurationPlan() +
            ", payment=" + getPayment() +
            "}";
    }
}
