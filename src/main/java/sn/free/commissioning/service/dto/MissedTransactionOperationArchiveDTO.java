package sn.free.commissioning.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DTO for the {@link sn.free.commissioning.domain.MissedTransactionOperationArchive} entity.
 */
public class MissedTransactionOperationArchiveDTO implements Serializable {

    private Long id;

    private Double amount;

    private String subsMsisdn;

    private String agentMsisdn;

    private String typeTransaction;

    private Instant createdAt;

    private String transactionStatus;

    private String senderZone;

    private String senderProfile;

    private String codeTerritory;

    private String subType;

    private Instant operationDate;

    private Boolean isFraud;

    private Instant taggedAt;

    private String fraudSource;

    private String comment;

    private Instant falsePositiveDetectedAt;

    private String tid;

    private String parentMsisdn;

    private String fctDt;

    private String parentId;

    private Instant canceledAt;

    private String canceledId;

    private String productId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getSubsMsisdn() {
        return subsMsisdn;
    }

    public void setSubsMsisdn(String subsMsisdn) {
        this.subsMsisdn = subsMsisdn;
    }

    public String getAgentMsisdn() {
        return agentMsisdn;
    }

    public void setAgentMsisdn(String agentMsisdn) {
        this.agentMsisdn = agentMsisdn;
    }

    public String getTypeTransaction() {
        return typeTransaction;
    }

    public void setTypeTransaction(String typeTransaction) {
        this.typeTransaction = typeTransaction;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getTransactionStatus() {
        return transactionStatus;
    }

    public void setTransactionStatus(String transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public String getSenderZone() {
        return senderZone;
    }

    public void setSenderZone(String senderZone) {
        this.senderZone = senderZone;
    }

    public String getSenderProfile() {
        return senderProfile;
    }

    public void setSenderProfile(String senderProfile) {
        this.senderProfile = senderProfile;
    }

    public String getCodeTerritory() {
        return codeTerritory;
    }

    public void setCodeTerritory(String codeTerritory) {
        this.codeTerritory = codeTerritory;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public Instant getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Instant operationDate) {
        this.operationDate = operationDate;
    }

    public Boolean getIsFraud() {
        return isFraud;
    }

    public void setIsFraud(Boolean isFraud) {
        this.isFraud = isFraud;
    }

    public Instant getTaggedAt() {
        return taggedAt;
    }

    public void setTaggedAt(Instant taggedAt) {
        this.taggedAt = taggedAt;
    }

    public String getFraudSource() {
        return fraudSource;
    }

    public void setFraudSource(String fraudSource) {
        this.fraudSource = fraudSource;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Instant getFalsePositiveDetectedAt() {
        return falsePositiveDetectedAt;
    }

    public void setFalsePositiveDetectedAt(Instant falsePositiveDetectedAt) {
        this.falsePositiveDetectedAt = falsePositiveDetectedAt;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getParentMsisdn() {
        return parentMsisdn;
    }

    public void setParentMsisdn(String parentMsisdn) {
        this.parentMsisdn = parentMsisdn;
    }

    public String getFctDt() {
        return fctDt;
    }

    public void setFctDt(String fctDt) {
        this.fctDt = fctDt;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Instant getCanceledAt() {
        return canceledAt;
    }

    public void setCanceledAt(Instant canceledAt) {
        this.canceledAt = canceledAt;
    }

    public String getCanceledId() {
        return canceledId;
    }

    public void setCanceledId(String canceledId) {
        this.canceledId = canceledId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MissedTransactionOperationArchiveDTO)) {
            return false;
        }

        MissedTransactionOperationArchiveDTO missedTransactionOperationArchiveDTO = (MissedTransactionOperationArchiveDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, missedTransactionOperationArchiveDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MissedTransactionOperationArchiveDTO{" +
            "id=" + getId() +
            ", amount=" + getAmount() +
            ", subsMsisdn='" + getSubsMsisdn() + "'" +
            ", agentMsisdn='" + getAgentMsisdn() + "'" +
            ", typeTransaction='" + getTypeTransaction() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", transactionStatus='" + getTransactionStatus() + "'" +
            ", senderZone='" + getSenderZone() + "'" +
            ", senderProfile='" + getSenderProfile() + "'" +
            ", codeTerritory='" + getCodeTerritory() + "'" +
            ", subType='" + getSubType() + "'" +
            ", operationDate='" + getOperationDate() + "'" +
            ", isFraud='" + getIsFraud() + "'" +
            ", taggedAt='" + getTaggedAt() + "'" +
            ", fraudSource='" + getFraudSource() + "'" +
            ", comment='" + getComment() + "'" +
            ", falsePositiveDetectedAt='" + getFalsePositiveDetectedAt() + "'" +
            ", tid='" + getTid() + "'" +
            ", parentMsisdn='" + getParentMsisdn() + "'" +
            ", fctDt='" + getFctDt() + "'" +
            ", parentId='" + getParentId() + "'" +
            ", canceledAt='" + getCanceledAt() + "'" +
            ", canceledId='" + getCanceledId() + "'" +
            ", productId='" + getProductId() + "'" +
            "}";
    }
}
