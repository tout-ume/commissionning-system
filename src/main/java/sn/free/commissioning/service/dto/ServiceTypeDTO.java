package sn.free.commissioning.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;
import sn.free.commissioning.domain.enumeration.CommissioningPlanType;
import sn.free.commissioning.domain.enumeration.ServiceTypeStatus;

/**
 * A DTO for the {@link sn.free.commissioning.domain.ServiceType} entity.
 */
public class ServiceTypeDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String code;

    private String description;

    @NotNull
    private ServiceTypeStatus state;

    @NotNull
    private Boolean isCosRelated;

    private CommissioningPlanType commissioningPlanType;

    @NotNull
    @Size(max = 50)
    private String createdBy;

    private Instant createdDate;

    @Size(max = 50)
    private String lastModifiedBy;

    private Instant lastModifiedDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ServiceTypeStatus getState() {
        return state;
    }

    public void setState(ServiceTypeStatus state) {
        this.state = state;
    }

    public Boolean getIsCosRelated() {
        return isCosRelated;
    }

    public void setIsCosRelated(Boolean isCosRelated) {
        this.isCosRelated = isCosRelated;
    }

    public CommissioningPlanType getCommissioningPlanType() {
        return commissioningPlanType;
    }

    public void setCommissioningPlanType(CommissioningPlanType commissioningPlanType) {
        this.commissioningPlanType = commissioningPlanType;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ServiceTypeDTO)) {
            return false;
        }

        ServiceTypeDTO serviceTypeDTO = (ServiceTypeDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, serviceTypeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ServiceTypeDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", code='" + getCode() + "'" +
            ", description='" + getDescription() + "'" +
            ", state='" + getState() + "'" +
            ", isCosRelated='" + getIsCosRelated() + "'" +
            ", commissioningPlanType='" + getCommissioningPlanType() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            "}";
    }
}
