package sn.free.commissioning.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link sn.free.commissioning.domain.CalculationExecutionTrace} entity.
 */
public class CalculationExecutionTraceDTO implements Serializable {

    private Long id;

    @NotNull
    private Double frequencyId;

    @NotNull
    private Instant executionDate;

    private Boolean isExecuted;

    private String executionResult;

    private String spare1;

    private String spare2;

    private String spare3;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getFrequencyId() {
        return frequencyId;
    }

    public void setFrequencyId(Double frequencyId) {
        this.frequencyId = frequencyId;
    }

    public Instant getExecutionDate() {
        return executionDate;
    }

    public void setExecutionDate(Instant executionDate) {
        this.executionDate = executionDate;
    }

    public Boolean getIsExecuted() {
        return isExecuted;
    }

    public void setIsExecuted(Boolean isExecuted) {
        this.isExecuted = isExecuted;
    }

    public String getExecutionResult() {
        return executionResult;
    }

    public void setExecutionResult(String executionResult) {
        this.executionResult = executionResult;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return spare3;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CalculationExecutionTraceDTO)) {
            return false;
        }

        CalculationExecutionTraceDTO calculationExecutionTraceDTO = (CalculationExecutionTraceDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, calculationExecutionTraceDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CalculationExecutionTraceDTO{" +
            "id=" + getId() +
            ", frequencyId=" + getFrequencyId() +
            ", executionDate='" + getExecutionDate() + "'" +
            ", isExecuted='" + getIsExecuted() + "'" +
            ", executionResult='" + getExecutionResult() + "'" +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            "}";
    }
}
