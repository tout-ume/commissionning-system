package sn.free.commissioning.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;
import sn.free.commissioning.domain.enumeration.FrequencyType;
import sn.free.commissioning.domain.enumeration.OperationType;

/**
 * A DTO for the {@link sn.free.commissioning.domain.Period} entity.
 */
public class PeriodDTO implements Serializable {

    private Long id;

    private Integer monthDayFrom;

    private Integer monthDayTo;

    private Integer weekDayFrom;

    private Integer weekDayTo;

    private Integer dayHourFrom;

    private Integer dayHourTo;

    private Boolean isPreviousDayHourFrom;

    private Boolean isPreviousDayHourTo;

    private Boolean isPreviousMonthDayFrom;

    private Boolean isPreviousMonthDayTo;

    private Boolean isCompleteDay;

    private Boolean isCompleteMonth;

    @NotNull
    private FrequencyType periodType;

    @NotNull
    private OperationType operationType;

    @NotNull
    @Size(max = 50)
    private String createdBy;

    @NotNull
    private Instant createdDate;

    @Size(max = 50)
    private String lastModifiedBy;

    private Instant lastModifiedDate;

    private String daysOrDatesOfOccurrence;

    private String spare1;

    private String spare2;

    private String spare3;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMonthDayFrom() {
        return monthDayFrom;
    }

    public void setMonthDayFrom(Integer monthDayFrom) {
        this.monthDayFrom = monthDayFrom;
    }

    public Integer getMonthDayTo() {
        return monthDayTo;
    }

    public void setMonthDayTo(Integer monthDayTo) {
        this.monthDayTo = monthDayTo;
    }

    public Integer getWeekDayFrom() {
        return weekDayFrom;
    }

    public void setWeekDayFrom(Integer weekDayFrom) {
        this.weekDayFrom = weekDayFrom;
    }

    public Integer getWeekDayTo() {
        return weekDayTo;
    }

    public void setWeekDayTo(Integer weekDayTo) {
        this.weekDayTo = weekDayTo;
    }

    public Integer getDayHourFrom() {
        return dayHourFrom;
    }

    public void setDayHourFrom(Integer dayHourFrom) {
        this.dayHourFrom = dayHourFrom;
    }

    public Integer getDayHourTo() {
        return dayHourTo;
    }

    public void setDayHourTo(Integer dayHourTo) {
        this.dayHourTo = dayHourTo;
    }

    public Boolean getIsPreviousDayHourFrom() {
        return isPreviousDayHourFrom;
    }

    public void setIsPreviousDayHourFrom(Boolean isPreviousDayHourFrom) {
        this.isPreviousDayHourFrom = isPreviousDayHourFrom;
    }

    public Boolean getIsPreviousDayHourTo() {
        return isPreviousDayHourTo;
    }

    public void setIsPreviousDayHourTo(Boolean isPreviousDayHourTo) {
        this.isPreviousDayHourTo = isPreviousDayHourTo;
    }

    public Boolean getIsPreviousMonthDayFrom() {
        return isPreviousMonthDayFrom;
    }

    public void setIsPreviousMonthDayFrom(Boolean isPreviousMonthDayFrom) {
        this.isPreviousMonthDayFrom = isPreviousMonthDayFrom;
    }

    public Boolean getIsPreviousMonthDayTo() {
        return isPreviousMonthDayTo;
    }

    public void setIsPreviousMonthDayTo(Boolean isPreviousMonthDayTo) {
        this.isPreviousMonthDayTo = isPreviousMonthDayTo;
    }

    public Boolean getIsCompleteDay() {
        return isCompleteDay;
    }

    public void setIsCompleteDay(Boolean isCompleteDay) {
        this.isCompleteDay = isCompleteDay;
    }

    public Boolean getIsCompleteMonth() {
        return isCompleteMonth;
    }

    public void setIsCompleteMonth(Boolean isCompleteMonth) {
        this.isCompleteMonth = isCompleteMonth;
    }

    public FrequencyType getPeriodType() {
        return periodType;
    }

    public void setPeriodType(FrequencyType periodType) {
        this.periodType = periodType;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getDaysOrDatesOfOccurrence() {
        return daysOrDatesOfOccurrence;
    }

    public void setDaysOrDatesOfOccurrence(String daysOrDatesOfOccurrence) {
        this.daysOrDatesOfOccurrence = daysOrDatesOfOccurrence;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return spare3;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PeriodDTO)) {
            return false;
        }

        PeriodDTO periodDTO = (PeriodDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, periodDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PeriodDTO{" +
            "id=" + getId() +
            ", monthDayFrom=" + getMonthDayFrom() +
            ", monthDayTo=" + getMonthDayTo() +
            ", weekDayFrom=" + getWeekDayFrom() +
            ", weekDayTo=" + getWeekDayTo() +
            ", dayHourFrom=" + getDayHourFrom() +
            ", dayHourTo=" + getDayHourTo() +
            ", isPreviousDayHourFrom='" + getIsPreviousDayHourFrom() + "'" +
            ", isPreviousDayHourTo='" + getIsPreviousDayHourTo() + "'" +
            ", isPreviousMonthDayFrom='" + getIsPreviousMonthDayFrom() + "'" +
            ", isPreviousMonthDayTo='" + getIsPreviousMonthDayTo() + "'" +
            ", isCompleteDay='" + getIsCompleteDay() + "'" +
            ", isCompleteMonth='" + getIsCompleteMonth() + "'" +
            ", periodType='" + getPeriodType() + "'" +
            ", operationType='" + getOperationType() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", daysOrDatesOfOccurrence='" + getDaysOrDatesOfOccurrence() + "'" +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            "}";
    }
}
