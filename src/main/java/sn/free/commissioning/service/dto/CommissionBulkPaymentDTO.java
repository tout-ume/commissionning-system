package sn.free.commissioning.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.*;
import javax.validation.constraints.*;
import sn.free.commissioning.domain.enumeration.CommissionPaymentStatus;
import sn.free.commissioning.domain.enumeration.CommissioningPlanType;
import sn.free.commissioning.payment_engine.domain.PaymentResult;

/**
 * A DTO for Commission bulk payment.
 */
public class CommissionBulkPaymentDTO implements Serializable {

    private String partnerMsisdn;

    private String partnerProfile;

    private Double totalAmount;

    private PaymentResult paymentResult;

    private CommissioningPlanType commissioningPlanType;

    private List<CommissionDTO> commissions = new ArrayList<>();

    public String getPartnerMsisdn() {
        return partnerMsisdn;
    }

    public void setPartnerMsisdn(String partnerMsisdn) {
        this.partnerMsisdn = partnerMsisdn;
    }

    public String getPartnerProfile() {
        return partnerProfile;
    }

    public void setPartnerProfile(String partnerProfile) {
        this.partnerProfile = partnerProfile;
    }

    public CommissioningPlanType getCommissioningPlanType() {
        return commissioningPlanType;
    }

    public void setCommissioningPlanType(CommissioningPlanType commissioningPlanType) {
        this.commissioningPlanType = commissioningPlanType;
    }

    public List<CommissionDTO> getCommissions() {
        return commissions;
    }

    public void setCommissions(List<CommissionDTO> commissions) {
        this.commissions = commissions;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public PaymentResult getPaymentResult() {
        return paymentResult;
    }

    public void setPaymentResult(PaymentResult paymentResult) {
        this.paymentResult = paymentResult;
    }

    @Override
    public String toString() {
        return (
            "CommissionBulkPaymentDTO{" +
            "partnerMsisdn='" +
            partnerMsisdn +
            '\'' +
            ", partnerProfile='" +
            partnerProfile +
            '\'' +
            ", totalAmount=" +
            totalAmount +
            ", paymentResult=" +
            paymentResult +
            ", commissioningPlanType=" +
            commissioningPlanType +
            ", commissions=" +
            commissions +
            '}'
        );
    }
}
