package sn.free.commissioning.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.time.Instant;
import javax.validation.constraints.NotNull;
import sn.free.commissioning.domain.enumeration.CommissionPaymentStatus;
import sn.free.commissioning.domain.enumeration.CommissioningPlanType;

/**
 * A DTO BIS for the {@link sn.free.commissioning.domain.Commission} entity, used to return the list of commission of a partner.
 */

public class CommissionForPaymentDTO implements Serializable {

    @JsonIgnore
    private Long id;

    @NotNull
    private Double amount;

    private Instant calculatedAt;

    private CommissioningPlanType commissioningPlanType;

    public CommissioningPlanType getCommissioningPlanType() {
        return commissioningPlanType;
    }

    public void setCommissioningPlanType(CommissioningPlanType commissioningPlanType) {
        this.commissioningPlanType = commissioningPlanType;
    }

    public CommissionForPaymentDTO() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Instant getCalculatedAt() {
        return calculatedAt;
    }

    public void setCalculatedAt(Instant calculatedAt) {
        this.calculatedAt = calculatedAt;
    }

    public CommissionPaymentStatus getCommissionPaymentStatus() {
        return commissionPaymentStatus;
    }

    public void setCommissionPaymentStatus(CommissionPaymentStatus commissionPaymentStatus) {
        this.commissionPaymentStatus = commissionPaymentStatus;
    }

    private CommissionPaymentStatus commissionPaymentStatus;

    @Override
    public String toString() {
        return (
            "CommissionForPaymentDTO{" +
            "id=" +
            id +
            ", amount=" +
            amount +
            ", calculatedAt=" +
            calculatedAt +
            ", commissioningPlanType=" +
            commissioningPlanType +
            ", commissionPaymentStatus=" +
            commissionPaymentStatus +
            '}'
        );
    }
}
