package sn.free.commissioning.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link sn.free.commissioning.domain.PartnerProfile} entity.
 */
public class PartnerProfileDTO implements Serializable {

    private Long id;

    @NotNull
    private String code;

    @NotNull
    private String name;

    private String description;

    private Integer level;

    private Long parentPartnerProfileId;

    @NotNull
    @Size(max = 50)
    private String createdBy;

    private Instant createdDate;

    @Size(max = 50)
    private String lastModifiedBy;

    private Instant lastModifiedDate;

    private Boolean canHaveMultipleZones;

    private Integer zoneThreshold;

    private Integer simBackupThreshold;

    private Boolean canDoProfileAssignment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public Long getParentPartnerProfileId() {
        return parentPartnerProfileId;
    }

    public void setParentPartnerProfileId(Long parentPartnerProfileId) {
        this.parentPartnerProfileId = parentPartnerProfileId;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Boolean getCanHaveMultipleZones() {
        return canHaveMultipleZones;
    }

    public void setCanHaveMultipleZones(Boolean canHaveMultipleZones) {
        this.canHaveMultipleZones = canHaveMultipleZones;
    }

    public Integer getZoneThreshold() {
        return zoneThreshold;
    }

    public void setZoneThreshold(Integer zoneThreshold) {
        this.zoneThreshold = zoneThreshold;
    }

    public Integer getSimBackupThreshold() {
        return simBackupThreshold;
    }

    public void setSimBackupThreshold(Integer simBackupThreshold) {
        this.simBackupThreshold = simBackupThreshold;
    }

    public Boolean getCanDoProfileAssignment() {
        return canDoProfileAssignment;
    }

    public void setCanDoProfileAssignment(Boolean canDoProfileAssignment) {
        this.canDoProfileAssignment = canDoProfileAssignment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PartnerProfileDTO)) {
            return false;
        }

        PartnerProfileDTO partnerProfileDTO = (PartnerProfileDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, partnerProfileDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PartnerProfileDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", level=" + getLevel() +
            ", parentPartnerProfileId=" + getParentPartnerProfileId() +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", canHaveMultipleZones='" + getCanHaveMultipleZones() + "'" +
            ", zoneThreshold=" + getZoneThreshold() +
            ", simBackupThreshold=" + getSimBackupThreshold() +
            ", canDoProfileAssignment='" + getCanDoProfileAssignment() + "'" +
            "}";
    }
}
