package sn.free.commissioning.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.validation.constraints.NotNull;
import lombok.*;

/**
 * A DTO BIS for the {@link sn.free.commissioning.domain.Partner} entity, used to return the list of commission and the partner.
 */

@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PartnerForPaymentDTO implements Serializable {

    private Long id;

    @NotNull
    private String msisdn;

    @NotNull
    private String codeProfile;

    private String name;

    private String surname;

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCodeProfile() {
        return codeProfile;
    }

    public void setCodeProfile(String codeProfile) {
        this.codeProfile = codeProfile;
    }

    private Set<CommissionForPaymentDTO> commissionForPaymentDTOS = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Set<CommissionForPaymentDTO> getCommissionForPaymentDTOS() {
        return commissionForPaymentDTOS;
    }

    public void setCommissionForPaymentDTOS(Set<CommissionForPaymentDTO> commissionForPaymentDTOS) {
        this.commissionForPaymentDTOS = commissionForPaymentDTOS;
    }
}
