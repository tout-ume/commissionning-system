package sn.free.commissioning.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link sn.free.commissioning.domain.CommissionAccount} entity.
 */
public class CommissionAccountDTO implements Serializable {

    private Long id;

    private Double calculatedBalance;

    private Double paidBalance;

    private String spare1;

    private String spare2;

    private String spare3;

    private PartnerDTO partner;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getCalculatedBalance() {
        return calculatedBalance;
    }

    public void setCalculatedBalance(Double calculatedBalance) {
        this.calculatedBalance = calculatedBalance;
    }

    public Double getPaidBalance() {
        return paidBalance;
    }

    public void setPaidBalance(Double paidBalance) {
        this.paidBalance = paidBalance;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return spare3;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public PartnerDTO getPartner() {
        return partner;
    }

    public void setPartner(PartnerDTO partner) {
        this.partner = partner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommissionAccountDTO)) {
            return false;
        }

        CommissionAccountDTO commissionAccountDTO = (CommissionAccountDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, commissionAccountDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommissionAccountDTO{" +
            "id=" + getId() +
            ", calculatedBalance=" + getCalculatedBalance() +
            ", paidBalance=" + getPaidBalance() +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            ", partner=" + getPartner() +
            "}";
    }
}
