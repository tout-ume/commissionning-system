package sn.free.commissioning.service.dto;

import java.io.Serializable;
import java.util.Objects;
import sn.free.commissioning.domain.enumeration.CommissionTypeType;
import sn.free.commissioning.domain.enumeration.PalierValueType;

/**
 * A DTO for the {@link sn.free.commissioning.domain.CommissionType} entity.
 */
public class CommissionTypeDTO implements Serializable {

    private Long id;

    private Double minValue;

    private Double maxValue;

    private Double tauxValue;

    private Double palierValue;

    private CommissionTypeType commissionTypeType;

    private PalierValueType palierValueType;

    private Boolean isInfinity;

    private String spare1;

    private String spare2;

    private String spare3;

    //    private ConfigurationPlanDTO configurationPlan;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getMinValue() {
        return minValue;
    }

    public void setMinValue(Double minValue) {
        this.minValue = minValue;
    }

    public Double getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(Double maxValue) {
        this.maxValue = maxValue;
    }

    public Double getTauxValue() {
        return tauxValue;
    }

    public void setTauxValue(Double tauxValue) {
        this.tauxValue = tauxValue;
    }

    public Double getPalierValue() {
        return palierValue;
    }

    public void setPalierValue(Double palierValue) {
        this.palierValue = palierValue;
    }

    public CommissionTypeType getCommissionTypeType() {
        return commissionTypeType;
    }

    public void setCommissionTypeType(CommissionTypeType commissionTypeType) {
        this.commissionTypeType = commissionTypeType;
    }

    public PalierValueType getPalierValueType() {
        return palierValueType;
    }

    public void setPalierValueType(PalierValueType palierValueType) {
        this.palierValueType = palierValueType;
    }

    public Boolean getIsInfinity() {
        return isInfinity;
    }

    public void setIsInfinity(Boolean isInfinity) {
        this.isInfinity = isInfinity;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return spare3;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    //    public ConfigurationPlanDTO getConfigurationPlan() {
    //        return configurationPlan;
    //    }
    //
    //    public void setConfigurationPlan(ConfigurationPlanDTO configurationPlan) {
    //        this.configurationPlan = configurationPlan;
    //    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CommissionTypeDTO)) {
            return false;
        }

        CommissionTypeDTO commissionTypeDTO = (CommissionTypeDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, commissionTypeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommissionTypeDTO{" +
            "id=" + getId() +
            ", minValue=" + getMinValue() +
            ", maxValue=" + getMaxValue() +
            ", tauxValue=" + getTauxValue() +
            ", palierValue=" + getPalierValue() +
            ", commissionTypeType='" + getCommissionTypeType() + "'" +
            ", palierValueType='" + getPalierValueType() + "'" +
            ", isInfinity='" + getIsInfinity() + "'" +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
//            ", configurationPlan=" + getConfigurationPlan() +
            "}";
    }
}
