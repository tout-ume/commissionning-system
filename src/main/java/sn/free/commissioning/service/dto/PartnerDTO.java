package sn.free.commissioning.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;
import sn.free.commissioning.domain.Partner;

/**
 * A DTO for the {@link sn.free.commissioning.domain.Partner} entity.
 */
public class PartnerDTO implements Serializable {

    private Long id;

    @NotNull
    private String msisdn;

    private String name;

    private String surname;

    private String state;

    private Long partnerProfileId;

    private PartnerDTO parent;

    private Boolean canResetPin;

    @NotNull
    private String createdBy;

    private Instant createdDate;

    private String lastModifiedBy;

    private Instant lastModifiedDate;

    private Instant lastTransactionDate;

    private Set<ZoneDTO> zones = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getPartnerProfileId() {
        return partnerProfileId;
    }

    public void setPartnerProfileId(Long partnerProfileId) {
        this.partnerProfileId = partnerProfileId;
    }

    public Boolean getCanResetPin() {
        return canResetPin;
    }

    public void setCanResetPin(Boolean canResetPin) {
        this.canResetPin = canResetPin;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Instant getLastTransactionDate() {
        return lastTransactionDate;
    }

    public void setLastTransactionDate(Instant lastTransactionDate) {
        this.lastTransactionDate = lastTransactionDate;
    }

    public Set<ZoneDTO> getZones() {
        return zones;
    }

    public void setZones(Set<ZoneDTO> zones) {
        this.zones = zones;
    }

    public PartnerDTO getParent() {
        return parent;
    }

    public void setParent(PartnerDTO parent) {
        this.parent = parent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PartnerDTO)) {
            return false;
        }

        PartnerDTO partnerDTO = (PartnerDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, partnerDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PartnerDTO{" +
            "id=" + getId() +
            ", msisdn='" + getMsisdn() + "'" +
            ", name='" + getName() + "'" +
            ", surname='" + getSurname() + "'" +
            ", state='" + getState() + "'" +
            ", partnerProfileId=" + getPartnerProfileId() +
            ", parent=" + getParent() +
            ", canResetPin='" + getCanResetPin() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", lastTransactionDate='" + getLastTransactionDate() + "'" +
            ", zones=" + getZones() +
            "}";
    }
}
