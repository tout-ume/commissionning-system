package sn.free.commissioning.service.dto;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import javax.validation.constraints.*;
import sn.free.commissioning.domain.enumeration.Cycle;
import sn.free.commissioning.domain.enumeration.FrequencyType;
import sn.free.commissioning.domain.enumeration.OperationType;
import sn.free.commissioning.domain.enumeration.PeriodOfOccurrence;

/**
 * A DTO for the {@link sn.free.commissioning.domain.Frequency} entity.
 */
public class FrequencyDTO implements Serializable {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private FrequencyType type;

    @NotNull
    private OperationType operationType;

    @NotNull
    private Integer executionTime;

    @Min(value = 1)
    @Max(value = 7)
    private Integer executionWeekDay;

    @Min(value = 1)
    @Max(value = 28)
    private Integer executionMonthDay;

    @Min(value = 0)
    @Max(value = 31)
    private Integer daysAfter;

    @NotNull
    @Size(max = 50)
    private String createdBy;

    @NotNull
    private Instant createdDate;

    @Size(max = 50)
    private String lastModifiedBy;

    private Instant lastModifiedDate;

    private Cycle cycle;

    private PeriodOfOccurrence periodOfOccurrence;

    private Integer numberOfCycle;

    private Integer occurrenceByPeriod;

    private String datesOfOccurrence;

    private String spare1;

    private String spare2;

    private String spare3;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FrequencyType getType() {
        return type;
    }

    public void setType(FrequencyType type) {
        this.type = type;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public Integer getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(Integer executionTime) {
        this.executionTime = executionTime;
    }

    public Integer getExecutionWeekDay() {
        return executionWeekDay;
    }

    public void setExecutionWeekDay(Integer executionWeekDay) {
        this.executionWeekDay = executionWeekDay;
    }

    public Integer getExecutionMonthDay() {
        return executionMonthDay;
    }

    public void setExecutionMonthDay(Integer executionMonthDay) {
        this.executionMonthDay = executionMonthDay;
    }

    public Integer getDaysAfter() {
        return daysAfter;
    }

    public void setDaysAfter(Integer daysAfter) {
        this.daysAfter = daysAfter;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Instant getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(Instant lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Cycle getCycle() {
        return cycle;
    }

    public void setCycle(Cycle cycle) {
        this.cycle = cycle;
    }

    public PeriodOfOccurrence getPeriodOfOccurrence() {
        return periodOfOccurrence;
    }

    public void setPeriodOfOccurrence(PeriodOfOccurrence periodOfOccurrence) {
        this.periodOfOccurrence = periodOfOccurrence;
    }

    public Integer getNumberOfCycle() {
        return numberOfCycle;
    }

    public void setNumberOfCycle(Integer numberOfCycle) {
        this.numberOfCycle = numberOfCycle;
    }

    public Integer getOccurrenceByPeriod() {
        return occurrenceByPeriod;
    }

    public void setOccurrenceByPeriod(Integer occurrenceByPeriod) {
        this.occurrenceByPeriod = occurrenceByPeriod;
    }

    public String getDatesOfOccurrence() {
        return datesOfOccurrence;
    }

    public void setDatesOfOccurrence(String datesOfOccurrence) {
        this.datesOfOccurrence = datesOfOccurrence;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return spare3;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FrequencyDTO)) {
            return false;
        }

        FrequencyDTO frequencyDTO = (FrequencyDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, frequencyDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FrequencyDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", type='" + getType() + "'" +
            ", operationType='" + getOperationType() + "'" +
            ", executionTime=" + getExecutionTime() +
            ", executionWeekDay=" + getExecutionWeekDay() +
            ", executionMonthDay=" + getExecutionMonthDay() +
            ", daysAfter=" + getDaysAfter() +
            ", createdBy='" + getCreatedBy() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", lastModifiedBy='" + getLastModifiedBy() + "'" +
            ", lastModifiedDate='" + getLastModifiedDate() + "'" +
            ", cycle='" + getCycle() + "'" +
            ", periodOfOccurrence='" + getPeriodOfOccurrence() + "'" +
            ", numberOfCycle=" + getNumberOfCycle() +
            ", occurrenceByPeriod=" + getOccurrenceByPeriod() +
            ", datesOfOccurrence='" + getDatesOfOccurrence() + "'" +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            "}";
    }
}
