package sn.free.commissioning.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import sn.free.commissioning.domain.enumeration.PaymentStatus;

public class PaymentHistoricDTO implements Serializable {

    @JsonIgnore
    private Long id;

    private String receiverMsisdn;

    private String name;

    private String surname;

    private String codeProfile; // TODO Add field in the constructor of parameter

    private Double amount;

    private Instant periodStartDate;

    private Instant createdDate;

    private PaymentStatus paymentStatus;

    private Set<CommissionForPaymentDTO> commissionForPaymentDTOS = new HashSet<>();

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PaymentHistoricDTO() {}

    public PaymentHistoricDTO(
        Long id,
        String receiverMsisdn,
        String codeProfile,
        Double amount,
        Instant periodStartDate,
        Instant createdDate,
        PaymentStatus paymentStatus,
        Set<CommissionForPaymentDTO> commissionForPaymentDTOS,
        String name,
        String surname
    ) {
        this.id = id;
        this.receiverMsisdn = receiverMsisdn;
        this.codeProfile = codeProfile;
        this.amount = amount;
        this.periodStartDate = periodStartDate;
        this.createdDate = createdDate;
        this.paymentStatus = paymentStatus;
        this.commissionForPaymentDTOS = commissionForPaymentDTOS;
        this.name = name;
        this.surname = surname;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReceiverMsisdn() {
        return receiverMsisdn;
    }

    public void setReceiverMsisdn(String receiverMsisdn) {
        this.receiverMsisdn = receiverMsisdn;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCodeProfile() {
        return codeProfile;
    }

    public void setCodeProfile(String codeProfile) {
        this.codeProfile = codeProfile;
    }

    public Instant getPeriodStartDate() {
        return periodStartDate;
    }

    public void setPeriodStartDate(Instant periodStartDate) {
        this.periodStartDate = periodStartDate;
    }

    public Instant getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Instant createdDate) {
        this.createdDate = createdDate;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Set<CommissionForPaymentDTO> getCommissionForPaymentDTOS() {
        return commissionForPaymentDTOS;
    }

    public void setCommissionForPaymentDTOS(Set<CommissionForPaymentDTO> commissionForPaymentDTOS) {
        this.commissionForPaymentDTOS = commissionForPaymentDTOS;
    }
}
