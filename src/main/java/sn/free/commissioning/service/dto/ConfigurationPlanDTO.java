package sn.free.commissioning.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the {@link sn.free.commissioning.domain.ConfigurationPlan} entity.
 */
public class ConfigurationPlanDTO implements Serializable {

    private Long id;

    private Set<CommissionTypeDTO> commissionTypes = new HashSet<>();

    private String spare1;

    private String spare2;

    private String spare3;

    private PartnerProfileDTO partnerProfile;

    private CommissioningPlanDTO commissioningPlan;

    private Set<ZoneDTO> zones = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<CommissionTypeDTO> getCommissionTypes() {
        return commissionTypes;
    }

    public void setCommissionTypes(Set<CommissionTypeDTO> commissionTypes) {
        this.commissionTypes = commissionTypes;
    }

    public PartnerProfileDTO getPartnerProfile() {
        return partnerProfile;
    }

    public void setPartnerProfile(PartnerProfileDTO partnerProfile) {
        this.partnerProfile = partnerProfile;
    }

    public CommissioningPlanDTO getCommissioningPlan() {
        return commissioningPlan;
    }

    public void setCommissioningPlan(CommissioningPlanDTO commissioningPlan) {
        this.commissioningPlan = commissioningPlan;
    }

    public String getSpare1() {
        return spare1;
    }

    public void setSpare1(String spare1) {
        this.spare1 = spare1;
    }

    public String getSpare2() {
        return spare2;
    }

    public void setSpare2(String spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return spare3;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }

    public Set<ZoneDTO> getZones() {
        return zones;
    }

    public void setZones(Set<ZoneDTO> zones) {
        this.zones = zones;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ConfigurationPlanDTO)) {
            return false;
        }

        ConfigurationPlanDTO configurationPlanDTO = (ConfigurationPlanDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, configurationPlanDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ConfigurationPlanDTO{" +
            "id=" + getId() +
            ", commissionTypes=" + getCommissionTypes() +
            ", spare1='" + getSpare1() + "'" +
            ", spare2='" + getSpare2() + "'" +
            ", spare3='" + getSpare3() + "'" +
            ", partnerProfile=" + getPartnerProfile() +
            ", commissioningPlan=" + getCommissioningPlan() +
            ", zones=" + getZones() +
            "}";
    }
}
