package sn.free.commissioning.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.commissioning.service.dto.ServiceTypeDTO;

/**
 * Service Interface for managing {@link sn.free.commissioning.domain.ServiceType}.
 */
public interface ServiceTypeService {
    /**
     * Save a serviceType.
     *
     * @param serviceTypeDTO the entity to save.
     * @return the persisted entity.
     */
    ServiceTypeDTO save(ServiceTypeDTO serviceTypeDTO);

    /**
     * Updates a serviceType.
     *
     * @param serviceTypeDTO the entity to update.
     * @return the persisted entity.
     */
    ServiceTypeDTO update(ServiceTypeDTO serviceTypeDTO);

    /**
     * Partially updates a serviceType.
     *
     * @param serviceTypeDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ServiceTypeDTO> partialUpdate(ServiceTypeDTO serviceTypeDTO);

    /**
     * Get all the serviceTypes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ServiceTypeDTO> findAll(Pageable pageable);

    /**
     * Get the "id" serviceType.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ServiceTypeDTO> findOne(Long id);

    /**
     * Delete the "id" serviceType.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
