package sn.free.commissioning.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.*; // for static metamodels
import sn.free.commissioning.domain.Commission;
import sn.free.commissioning.repository.CommissionRepository;
import sn.free.commissioning.service.criteria.CommissionCriteria;
import sn.free.commissioning.service.dto.CommissionDTO;
import sn.free.commissioning.service.mapper.CommissionMapper;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Commission} entities in the database.
 * The main input is a {@link CommissionCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CommissionDTO} or a {@link Page} of {@link CommissionDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CommissionQueryService extends QueryService<Commission> {

    private final Logger log = LoggerFactory.getLogger(CommissionQueryService.class);

    private final CommissionRepository commissionRepository;

    private final CommissionMapper commissionMapper;

    public CommissionQueryService(CommissionRepository commissionRepository, CommissionMapper commissionMapper) {
        this.commissionRepository = commissionRepository;
        this.commissionMapper = commissionMapper;
    }

    /**
     * Return a {@link List} of {@link CommissionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CommissionDTO> findByCriteria(CommissionCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Commission> specification = createSpecification(criteria);
        return commissionMapper.toDto(commissionRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CommissionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CommissionDTO> findByCriteria(CommissionCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Commission> specification = createSpecification(criteria);
        return commissionRepository.findAll(specification, page).map(commissionMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CommissionCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Commission> specification = createSpecification(criteria);
        return commissionRepository.count(specification);
    }

    /**
     * Function to convert {@link CommissionCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Commission> createSpecification(CommissionCriteria criteria) {
        Specification<Commission> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Commission_.id));
            }
            if (criteria.getAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAmount(), Commission_.amount));
            }
            if (criteria.getCalculatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCalculatedAt(), Commission_.calculatedAt));
            }
            if (criteria.getCalculationDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCalculationDate(), Commission_.calculationDate));
            }
            if (criteria.getCalculationShortDate() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getCalculationShortDate(), Commission_.calculationShortDate));
            }
            if (criteria.getSenderMsisdn() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSenderMsisdn(), Commission_.senderMsisdn));
            }
            if (criteria.getSenderProfile() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSenderProfile(), Commission_.senderProfile));
            }
            if (criteria.getServiceType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getServiceType(), Commission_.serviceType));
            }
            if (criteria.getCommissionPaymentStatus() != null) {
                specification =
                    specification.and(buildSpecification(criteria.getCommissionPaymentStatus(), Commission_.commissionPaymentStatus));
            }
            if (criteria.getCommissioningPlanType() != null) {
                specification =
                    specification.and(buildSpecification(criteria.getCommissioningPlanType(), Commission_.commissioningPlanType));
            }
            if (criteria.getGlobalNetworkCommissionAmount() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getGlobalNetworkCommissionAmount(), Commission_.globalNetworkCommissionAmount)
                    );
            }
            if (criteria.getTransactionsAmount() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getTransactionsAmount(), Commission_.transactionsAmount));
            }
            if (criteria.getFraudAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFraudAmount(), Commission_.fraudAmount));
            }
            if (criteria.getSpare1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare1(), Commission_.spare1));
            }
            if (criteria.getSpare2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare2(), Commission_.spare2));
            }
            if (criteria.getSpare3() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare3(), Commission_.spare3));
            }
            if (criteria.getCommissionAccountId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getCommissionAccountId(),
                            root -> root.join(Commission_.commissionAccount, JoinType.LEFT).get(CommissionAccount_.id)
                        )
                    );
            }
            if (criteria.getTransactionOperationId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getTransactionOperationId(),
                            root -> root.join(Commission_.transactionOperations, JoinType.LEFT).get(TransactionOperation_.id)
                        )
                    );
            }
            if (criteria.getConfigurationPlanId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getConfigurationPlanId(),
                            root -> root.join(Commission_.configurationPlan, JoinType.LEFT).get(ConfigurationPlan_.id)
                        )
                    );
            }
            if (criteria.getPaymentId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getPaymentId(), root -> root.join(Commission_.payment, JoinType.LEFT).get(Payment_.id))
                    );
            }
        }
        return specification;
    }
}
