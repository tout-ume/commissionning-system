package sn.free.commissioning.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.commissioning.service.dto.PartnerProfileDTO;

/**
 * Service Interface for managing {@link sn.free.commissioning.domain.PartnerProfile}.
 */
public interface PartnerProfileService {
    /**
     * Save a partnerProfile.
     *
     * @param partnerProfileDTO the entity to save.
     * @return the persisted entity.
     */
    PartnerProfileDTO save(PartnerProfileDTO partnerProfileDTO);

    /**
     * Updates a partnerProfile.
     *
     * @param partnerProfileDTO the entity to update.
     * @return the persisted entity.
     */
    PartnerProfileDTO update(PartnerProfileDTO partnerProfileDTO);

    /**
     * Partially updates a partnerProfile.
     *
     * @param partnerProfileDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PartnerProfileDTO> partialUpdate(PartnerProfileDTO partnerProfileDTO);

    /**
     * Get all the partnerProfiles.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PartnerProfileDTO> findAll(Pageable pageable);

    /**
     * Get the "id" partnerProfile.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PartnerProfileDTO> findOne(Long id);

    /**
     * Delete the "id" partnerProfile.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
