package sn.free.commissioning.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.commissioning.service.dto.MissedTransactionOperationDTO;

/**
 * Service Interface for managing {@link sn.free.commissioning.domain.MissedTransactionOperation}.
 */
public interface MissedTransactionOperationService {
    /**
     * Save a missedTransactionOperation.
     *
     * @param missedTransactionOperationDTO the entity to save.
     * @return the persisted entity.
     */
    MissedTransactionOperationDTO save(MissedTransactionOperationDTO missedTransactionOperationDTO);

    /**
     * Updates a missedTransactionOperation.
     *
     * @param missedTransactionOperationDTO the entity to update.
     * @return the persisted entity.
     */
    MissedTransactionOperationDTO update(MissedTransactionOperationDTO missedTransactionOperationDTO);

    /**
     * Partially updates a missedTransactionOperation.
     *
     * @param missedTransactionOperationDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<MissedTransactionOperationDTO> partialUpdate(MissedTransactionOperationDTO missedTransactionOperationDTO);

    /**
     * Get all the missedTransactionOperations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MissedTransactionOperationDTO> findAll(Pageable pageable);

    /**
     * Get the "id" missedTransactionOperation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MissedTransactionOperationDTO> findOne(Long id);

    /**
     * Delete the "id" missedTransactionOperation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
