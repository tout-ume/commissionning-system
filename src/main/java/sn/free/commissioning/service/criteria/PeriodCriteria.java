package sn.free.commissioning.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import sn.free.commissioning.domain.enumeration.FrequencyType;
import sn.free.commissioning.domain.enumeration.OperationType;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link sn.free.commissioning.domain.Period} entity. This class is used
 * in {@link sn.free.commissioning.web.rest.PeriodResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /periods?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
public class PeriodCriteria implements Serializable, Criteria {

    /**
     * Class for filtering FrequencyType
     */
    public static class FrequencyTypeFilter extends Filter<FrequencyType> {

        public FrequencyTypeFilter() {}

        public FrequencyTypeFilter(FrequencyTypeFilter filter) {
            super(filter);
        }

        @Override
        public FrequencyTypeFilter copy() {
            return new FrequencyTypeFilter(this);
        }
    }

    /**
     * Class for filtering OperationType
     */
    public static class OperationTypeFilter extends Filter<OperationType> {

        public OperationTypeFilter() {}

        public OperationTypeFilter(OperationTypeFilter filter) {
            super(filter);
        }

        @Override
        public OperationTypeFilter copy() {
            return new OperationTypeFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter monthDayFrom;

    private IntegerFilter monthDayTo;

    private IntegerFilter weekDayFrom;

    private IntegerFilter weekDayTo;

    private IntegerFilter dayHourFrom;

    private IntegerFilter dayHourTo;

    private BooleanFilter isPreviousDayHourFrom;

    private BooleanFilter isPreviousDayHourTo;

    private BooleanFilter isPreviousMonthDayFrom;

    private BooleanFilter isPreviousMonthDayTo;

    private BooleanFilter isCompleteDay;

    private BooleanFilter isCompleteMonth;

    private FrequencyTypeFilter periodType;

    private OperationTypeFilter operationType;

    private StringFilter createdBy;

    private InstantFilter createdDate;

    private StringFilter lastModifiedBy;

    private InstantFilter lastModifiedDate;

    private StringFilter daysOrDatesOfOccurrence;

    private StringFilter spare1;

    private StringFilter spare2;

    private StringFilter spare3;

    private Boolean distinct;

    public PeriodCriteria() {}

    public PeriodCriteria(PeriodCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.monthDayFrom = other.monthDayFrom == null ? null : other.monthDayFrom.copy();
        this.monthDayTo = other.monthDayTo == null ? null : other.monthDayTo.copy();
        this.weekDayFrom = other.weekDayFrom == null ? null : other.weekDayFrom.copy();
        this.weekDayTo = other.weekDayTo == null ? null : other.weekDayTo.copy();
        this.dayHourFrom = other.dayHourFrom == null ? null : other.dayHourFrom.copy();
        this.dayHourTo = other.dayHourTo == null ? null : other.dayHourTo.copy();
        this.isPreviousDayHourFrom = other.isPreviousDayHourFrom == null ? null : other.isPreviousDayHourFrom.copy();
        this.isPreviousDayHourTo = other.isPreviousDayHourTo == null ? null : other.isPreviousDayHourTo.copy();
        this.isPreviousMonthDayFrom = other.isPreviousMonthDayFrom == null ? null : other.isPreviousMonthDayFrom.copy();
        this.isPreviousMonthDayTo = other.isPreviousMonthDayTo == null ? null : other.isPreviousMonthDayTo.copy();
        this.isCompleteDay = other.isCompleteDay == null ? null : other.isCompleteDay.copy();
        this.isCompleteMonth = other.isCompleteMonth == null ? null : other.isCompleteMonth.copy();
        this.periodType = other.periodType == null ? null : other.periodType.copy();
        this.operationType = other.operationType == null ? null : other.operationType.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.daysOrDatesOfOccurrence = other.daysOrDatesOfOccurrence == null ? null : other.daysOrDatesOfOccurrence.copy();
        this.spare1 = other.spare1 == null ? null : other.spare1.copy();
        this.spare2 = other.spare2 == null ? null : other.spare2.copy();
        this.spare3 = other.spare3 == null ? null : other.spare3.copy();
        this.distinct = other.distinct;
    }

    @Override
    public PeriodCriteria copy() {
        return new PeriodCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getMonthDayFrom() {
        return monthDayFrom;
    }

    public IntegerFilter monthDayFrom() {
        if (monthDayFrom == null) {
            monthDayFrom = new IntegerFilter();
        }
        return monthDayFrom;
    }

    public void setMonthDayFrom(IntegerFilter monthDayFrom) {
        this.monthDayFrom = monthDayFrom;
    }

    public IntegerFilter getMonthDayTo() {
        return monthDayTo;
    }

    public IntegerFilter monthDayTo() {
        if (monthDayTo == null) {
            monthDayTo = new IntegerFilter();
        }
        return monthDayTo;
    }

    public void setMonthDayTo(IntegerFilter monthDayTo) {
        this.monthDayTo = monthDayTo;
    }

    public IntegerFilter getWeekDayFrom() {
        return weekDayFrom;
    }

    public IntegerFilter weekDayFrom() {
        if (weekDayFrom == null) {
            weekDayFrom = new IntegerFilter();
        }
        return weekDayFrom;
    }

    public void setWeekDayFrom(IntegerFilter weekDayFrom) {
        this.weekDayFrom = weekDayFrom;
    }

    public IntegerFilter getWeekDayTo() {
        return weekDayTo;
    }

    public IntegerFilter weekDayTo() {
        if (weekDayTo == null) {
            weekDayTo = new IntegerFilter();
        }
        return weekDayTo;
    }

    public void setWeekDayTo(IntegerFilter weekDayTo) {
        this.weekDayTo = weekDayTo;
    }

    public IntegerFilter getDayHourFrom() {
        return dayHourFrom;
    }

    public IntegerFilter dayHourFrom() {
        if (dayHourFrom == null) {
            dayHourFrom = new IntegerFilter();
        }
        return dayHourFrom;
    }

    public void setDayHourFrom(IntegerFilter dayHourFrom) {
        this.dayHourFrom = dayHourFrom;
    }

    public IntegerFilter getDayHourTo() {
        return dayHourTo;
    }

    public IntegerFilter dayHourTo() {
        if (dayHourTo == null) {
            dayHourTo = new IntegerFilter();
        }
        return dayHourTo;
    }

    public void setDayHourTo(IntegerFilter dayHourTo) {
        this.dayHourTo = dayHourTo;
    }

    public BooleanFilter getIsPreviousDayHourFrom() {
        return isPreviousDayHourFrom;
    }

    public BooleanFilter isPreviousDayHourFrom() {
        if (isPreviousDayHourFrom == null) {
            isPreviousDayHourFrom = new BooleanFilter();
        }
        return isPreviousDayHourFrom;
    }

    public void setIsPreviousDayHourFrom(BooleanFilter isPreviousDayHourFrom) {
        this.isPreviousDayHourFrom = isPreviousDayHourFrom;
    }

    public BooleanFilter getIsPreviousDayHourTo() {
        return isPreviousDayHourTo;
    }

    public BooleanFilter isPreviousDayHourTo() {
        if (isPreviousDayHourTo == null) {
            isPreviousDayHourTo = new BooleanFilter();
        }
        return isPreviousDayHourTo;
    }

    public void setIsPreviousDayHourTo(BooleanFilter isPreviousDayHourTo) {
        this.isPreviousDayHourTo = isPreviousDayHourTo;
    }

    public BooleanFilter getIsPreviousMonthDayFrom() {
        return isPreviousMonthDayFrom;
    }

    public BooleanFilter isPreviousMonthDayFrom() {
        if (isPreviousMonthDayFrom == null) {
            isPreviousMonthDayFrom = new BooleanFilter();
        }
        return isPreviousMonthDayFrom;
    }

    public void setIsPreviousMonthDayFrom(BooleanFilter isPreviousMonthDayFrom) {
        this.isPreviousMonthDayFrom = isPreviousMonthDayFrom;
    }

    public BooleanFilter getIsPreviousMonthDayTo() {
        return isPreviousMonthDayTo;
    }

    public BooleanFilter isPreviousMonthDayTo() {
        if (isPreviousMonthDayTo == null) {
            isPreviousMonthDayTo = new BooleanFilter();
        }
        return isPreviousMonthDayTo;
    }

    public void setIsPreviousMonthDayTo(BooleanFilter isPreviousMonthDayTo) {
        this.isPreviousMonthDayTo = isPreviousMonthDayTo;
    }

    public BooleanFilter getIsCompleteDay() {
        return isCompleteDay;
    }

    public BooleanFilter isCompleteDay() {
        if (isCompleteDay == null) {
            isCompleteDay = new BooleanFilter();
        }
        return isCompleteDay;
    }

    public void setIsCompleteDay(BooleanFilter isCompleteDay) {
        this.isCompleteDay = isCompleteDay;
    }

    public BooleanFilter getIsCompleteMonth() {
        return isCompleteMonth;
    }

    public BooleanFilter isCompleteMonth() {
        if (isCompleteMonth == null) {
            isCompleteMonth = new BooleanFilter();
        }
        return isCompleteMonth;
    }

    public void setIsCompleteMonth(BooleanFilter isCompleteMonth) {
        this.isCompleteMonth = isCompleteMonth;
    }

    public FrequencyTypeFilter getPeriodType() {
        return periodType;
    }

    public FrequencyTypeFilter periodType() {
        if (periodType == null) {
            periodType = new FrequencyTypeFilter();
        }
        return periodType;
    }

    public void setPeriodType(FrequencyTypeFilter periodType) {
        this.periodType = periodType;
    }

    public OperationTypeFilter getOperationType() {
        return operationType;
    }

    public OperationTypeFilter operationType() {
        if (operationType == null) {
            operationType = new OperationTypeFilter();
        }
        return operationType;
    }

    public void setOperationType(OperationTypeFilter operationType) {
        this.operationType = operationType;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public StringFilter createdBy() {
        if (createdBy == null) {
            createdBy = new StringFilter();
        }
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public InstantFilter createdDate() {
        if (createdDate == null) {
            createdDate = new InstantFilter();
        }
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public StringFilter lastModifiedBy() {
        if (lastModifiedBy == null) {
            lastModifiedBy = new StringFilter();
        }
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public InstantFilter lastModifiedDate() {
        if (lastModifiedDate == null) {
            lastModifiedDate = new InstantFilter();
        }
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public StringFilter getDaysOrDatesOfOccurrence() {
        return daysOrDatesOfOccurrence;
    }

    public StringFilter daysOrDatesOfOccurrence() {
        if (daysOrDatesOfOccurrence == null) {
            daysOrDatesOfOccurrence = new StringFilter();
        }
        return daysOrDatesOfOccurrence;
    }

    public void setDaysOrDatesOfOccurrence(StringFilter daysOrDatesOfOccurrence) {
        this.daysOrDatesOfOccurrence = daysOrDatesOfOccurrence;
    }

    public StringFilter getSpare1() {
        return spare1;
    }

    public StringFilter spare1() {
        if (spare1 == null) {
            spare1 = new StringFilter();
        }
        return spare1;
    }

    public void setSpare1(StringFilter spare1) {
        this.spare1 = spare1;
    }

    public StringFilter getSpare2() {
        return spare2;
    }

    public StringFilter spare2() {
        if (spare2 == null) {
            spare2 = new StringFilter();
        }
        return spare2;
    }

    public void setSpare2(StringFilter spare2) {
        this.spare2 = spare2;
    }

    public StringFilter getSpare3() {
        return spare3;
    }

    public StringFilter spare3() {
        if (spare3 == null) {
            spare3 = new StringFilter();
        }
        return spare3;
    }

    public void setSpare3(StringFilter spare3) {
        this.spare3 = spare3;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PeriodCriteria that = (PeriodCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(monthDayFrom, that.monthDayFrom) &&
            Objects.equals(monthDayTo, that.monthDayTo) &&
            Objects.equals(weekDayFrom, that.weekDayFrom) &&
            Objects.equals(weekDayTo, that.weekDayTo) &&
            Objects.equals(dayHourFrom, that.dayHourFrom) &&
            Objects.equals(dayHourTo, that.dayHourTo) &&
            Objects.equals(isPreviousDayHourFrom, that.isPreviousDayHourFrom) &&
            Objects.equals(isPreviousDayHourTo, that.isPreviousDayHourTo) &&
            Objects.equals(isPreviousMonthDayFrom, that.isPreviousMonthDayFrom) &&
            Objects.equals(isPreviousMonthDayTo, that.isPreviousMonthDayTo) &&
            Objects.equals(isCompleteDay, that.isCompleteDay) &&
            Objects.equals(isCompleteMonth, that.isCompleteMonth) &&
            Objects.equals(periodType, that.periodType) &&
            Objects.equals(operationType, that.operationType) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(daysOrDatesOfOccurrence, that.daysOrDatesOfOccurrence) &&
            Objects.equals(spare1, that.spare1) &&
            Objects.equals(spare2, that.spare2) &&
            Objects.equals(spare3, that.spare3) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            monthDayFrom,
            monthDayTo,
            weekDayFrom,
            weekDayTo,
            dayHourFrom,
            dayHourTo,
            isPreviousDayHourFrom,
            isPreviousDayHourTo,
            isPreviousMonthDayFrom,
            isPreviousMonthDayTo,
            isCompleteDay,
            isCompleteMonth,
            periodType,
            operationType,
            createdBy,
            createdDate,
            lastModifiedBy,
            lastModifiedDate,
            daysOrDatesOfOccurrence,
            spare1,
            spare2,
            spare3,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PeriodCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (monthDayFrom != null ? "monthDayFrom=" + monthDayFrom + ", " : "") +
            (monthDayTo != null ? "monthDayTo=" + monthDayTo + ", " : "") +
            (weekDayFrom != null ? "weekDayFrom=" + weekDayFrom + ", " : "") +
            (weekDayTo != null ? "weekDayTo=" + weekDayTo + ", " : "") +
            (dayHourFrom != null ? "dayHourFrom=" + dayHourFrom + ", " : "") +
            (dayHourTo != null ? "dayHourTo=" + dayHourTo + ", " : "") +
            (isPreviousDayHourFrom != null ? "isPreviousDayHourFrom=" + isPreviousDayHourFrom + ", " : "") +
            (isPreviousDayHourTo != null ? "isPreviousDayHourTo=" + isPreviousDayHourTo + ", " : "") +
            (isPreviousMonthDayFrom != null ? "isPreviousMonthDayFrom=" + isPreviousMonthDayFrom + ", " : "") +
            (isPreviousMonthDayTo != null ? "isPreviousMonthDayTo=" + isPreviousMonthDayTo + ", " : "") +
            (isCompleteDay != null ? "isCompleteDay=" + isCompleteDay + ", " : "") +
            (isCompleteMonth != null ? "isCompleteMonth=" + isCompleteMonth + ", " : "") +
            (periodType != null ? "periodType=" + periodType + ", " : "") +
            (operationType != null ? "operationType=" + operationType + ", " : "") +
            (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
            (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
            (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
            (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
            (daysOrDatesOfOccurrence != null ? "daysOrDatesOfOccurrence=" + daysOrDatesOfOccurrence + ", " : "") +
            (spare1 != null ? "spare1=" + spare1 + ", " : "") +
            (spare2 != null ? "spare2=" + spare2 + ", " : "") +
            (spare3 != null ? "spare3=" + spare3 + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
