package sn.free.commissioning.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import sn.free.commissioning.domain.enumeration.CommissionTypeType;
import sn.free.commissioning.domain.enumeration.PalierValueType;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link sn.free.commissioning.domain.CommissionType} entity. This class is used
 * in {@link sn.free.commissioning.web.rest.CommissionTypeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /commission-types?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
public class CommissionTypeCriteria implements Serializable, Criteria {

    /**
     * Class for filtering CommissionTypeType
     */
    public static class CommissionTypeTypeFilter extends Filter<CommissionTypeType> {

        public CommissionTypeTypeFilter() {}

        public CommissionTypeTypeFilter(CommissionTypeTypeFilter filter) {
            super(filter);
        }

        @Override
        public CommissionTypeTypeFilter copy() {
            return new CommissionTypeTypeFilter(this);
        }
    }

    /**
     * Class for filtering PalierValueType
     */
    public static class PalierValueTypeFilter extends Filter<PalierValueType> {

        public PalierValueTypeFilter() {}

        public PalierValueTypeFilter(PalierValueTypeFilter filter) {
            super(filter);
        }

        @Override
        public PalierValueTypeFilter copy() {
            return new PalierValueTypeFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private DoubleFilter minValue;

    private DoubleFilter maxValue;

    private DoubleFilter tauxValue;

    private DoubleFilter palierValue;

    private CommissionTypeTypeFilter commissionTypeType;

    private PalierValueTypeFilter palierValueType;

    private BooleanFilter isInfinity;

    private StringFilter spare1;

    private StringFilter spare2;

    private StringFilter spare3;

    private LongFilter configurationPlanId;

    private Boolean distinct;

    public CommissionTypeCriteria() {}

    public CommissionTypeCriteria(CommissionTypeCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.minValue = other.minValue == null ? null : other.minValue.copy();
        this.maxValue = other.maxValue == null ? null : other.maxValue.copy();
        this.tauxValue = other.tauxValue == null ? null : other.tauxValue.copy();
        this.palierValue = other.palierValue == null ? null : other.palierValue.copy();
        this.commissionTypeType = other.commissionTypeType == null ? null : other.commissionTypeType.copy();
        this.palierValueType = other.palierValueType == null ? null : other.palierValueType.copy();
        this.isInfinity = other.isInfinity == null ? null : other.isInfinity.copy();
        this.spare1 = other.spare1 == null ? null : other.spare1.copy();
        this.spare2 = other.spare2 == null ? null : other.spare2.copy();
        this.spare3 = other.spare3 == null ? null : other.spare3.copy();
        this.configurationPlanId = other.configurationPlanId == null ? null : other.configurationPlanId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public CommissionTypeCriteria copy() {
        return new CommissionTypeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public DoubleFilter getMinValue() {
        return minValue;
    }

    public DoubleFilter minValue() {
        if (minValue == null) {
            minValue = new DoubleFilter();
        }
        return minValue;
    }

    public void setMinValue(DoubleFilter minValue) {
        this.minValue = minValue;
    }

    public DoubleFilter getMaxValue() {
        return maxValue;
    }

    public DoubleFilter maxValue() {
        if (maxValue == null) {
            maxValue = new DoubleFilter();
        }
        return maxValue;
    }

    public void setMaxValue(DoubleFilter maxValue) {
        this.maxValue = maxValue;
    }

    public DoubleFilter getTauxValue() {
        return tauxValue;
    }

    public DoubleFilter tauxValue() {
        if (tauxValue == null) {
            tauxValue = new DoubleFilter();
        }
        return tauxValue;
    }

    public void setTauxValue(DoubleFilter tauxValue) {
        this.tauxValue = tauxValue;
    }

    public DoubleFilter getPalierValue() {
        return palierValue;
    }

    public DoubleFilter palierValue() {
        if (palierValue == null) {
            palierValue = new DoubleFilter();
        }
        return palierValue;
    }

    public void setPalierValue(DoubleFilter palierValue) {
        this.palierValue = palierValue;
    }

    public CommissionTypeTypeFilter getCommissionTypeType() {
        return commissionTypeType;
    }

    public CommissionTypeTypeFilter commissionTypeType() {
        if (commissionTypeType == null) {
            commissionTypeType = new CommissionTypeTypeFilter();
        }
        return commissionTypeType;
    }

    public void setCommissionTypeType(CommissionTypeTypeFilter commissionTypeType) {
        this.commissionTypeType = commissionTypeType;
    }

    public PalierValueTypeFilter getPalierValueType() {
        return palierValueType;
    }

    public PalierValueTypeFilter palierValueType() {
        if (palierValueType == null) {
            palierValueType = new PalierValueTypeFilter();
        }
        return palierValueType;
    }

    public void setPalierValueType(PalierValueTypeFilter palierValueType) {
        this.palierValueType = palierValueType;
    }

    public BooleanFilter getIsInfinity() {
        return isInfinity;
    }

    public BooleanFilter isInfinity() {
        if (isInfinity == null) {
            isInfinity = new BooleanFilter();
        }
        return isInfinity;
    }

    public void setIsInfinity(BooleanFilter isInfinity) {
        this.isInfinity = isInfinity;
    }

    public StringFilter getSpare1() {
        return spare1;
    }

    public StringFilter spare1() {
        if (spare1 == null) {
            spare1 = new StringFilter();
        }
        return spare1;
    }

    public void setSpare1(StringFilter spare1) {
        this.spare1 = spare1;
    }

    public StringFilter getSpare2() {
        return spare2;
    }

    public StringFilter spare2() {
        if (spare2 == null) {
            spare2 = new StringFilter();
        }
        return spare2;
    }

    public void setSpare2(StringFilter spare2) {
        this.spare2 = spare2;
    }

    public StringFilter getSpare3() {
        return spare3;
    }

    public StringFilter spare3() {
        if (spare3 == null) {
            spare3 = new StringFilter();
        }
        return spare3;
    }

    public void setSpare3(StringFilter spare3) {
        this.spare3 = spare3;
    }

    public LongFilter getConfigurationPlanId() {
        return configurationPlanId;
    }

    public LongFilter configurationPlanId() {
        if (configurationPlanId == null) {
            configurationPlanId = new LongFilter();
        }
        return configurationPlanId;
    }

    public void setConfigurationPlanId(LongFilter configurationPlanId) {
        this.configurationPlanId = configurationPlanId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CommissionTypeCriteria that = (CommissionTypeCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(minValue, that.minValue) &&
            Objects.equals(maxValue, that.maxValue) &&
            Objects.equals(tauxValue, that.tauxValue) &&
            Objects.equals(palierValue, that.palierValue) &&
            Objects.equals(commissionTypeType, that.commissionTypeType) &&
            Objects.equals(palierValueType, that.palierValueType) &&
            Objects.equals(isInfinity, that.isInfinity) &&
            Objects.equals(spare1, that.spare1) &&
            Objects.equals(spare2, that.spare2) &&
            Objects.equals(spare3, that.spare3) &&
            Objects.equals(configurationPlanId, that.configurationPlanId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            minValue,
            maxValue,
            tauxValue,
            palierValue,
            commissionTypeType,
            palierValueType,
            isInfinity,
            spare1,
            spare2,
            spare3,
            configurationPlanId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommissionTypeCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (minValue != null ? "minValue=" + minValue + ", " : "") +
            (maxValue != null ? "maxValue=" + maxValue + ", " : "") +
            (tauxValue != null ? "tauxValue=" + tauxValue + ", " : "") +
            (palierValue != null ? "palierValue=" + palierValue + ", " : "") +
            (commissionTypeType != null ? "commissionTypeType=" + commissionTypeType + ", " : "") +
            (palierValueType != null ? "palierValueType=" + palierValueType + ", " : "") +
            (isInfinity != null ? "isInfinity=" + isInfinity + ", " : "") +
            (spare1 != null ? "spare1=" + spare1 + ", " : "") +
            (spare2 != null ? "spare2=" + spare2 + ", " : "") +
            (spare3 != null ? "spare3=" + spare3 + ", " : "") +
            (configurationPlanId != null ? "configurationPlanId=" + configurationPlanId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
