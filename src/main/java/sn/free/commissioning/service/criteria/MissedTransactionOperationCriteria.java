package sn.free.commissioning.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link sn.free.commissioning.domain.MissedTransactionOperation} entity. This class is used
 * in {@link sn.free.commissioning.web.rest.MissedTransactionOperationResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /missed-transaction-operations?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
public class MissedTransactionOperationCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private DoubleFilter amount;

    private StringFilter subsMsisdn;

    private StringFilter agentMsisdn;

    private StringFilter typeTransaction;

    private InstantFilter createdAt;

    private StringFilter transactionStatus;

    private StringFilter senderZone;

    private StringFilter senderProfile;

    private StringFilter codeTerritory;

    private StringFilter subType;

    private InstantFilter operationDate;

    private BooleanFilter isFraud;

    private InstantFilter taggedAt;

    private StringFilter fraudSource;

    private StringFilter comment;

    private InstantFilter falsePositiveDetectedAt;

    private StringFilter tid;

    private StringFilter parentMsisdn;

    private StringFilter fctDt;

    private StringFilter parentId;

    private InstantFilter canceledAt;

    private StringFilter canceledId;

    private StringFilter productId;

    private Boolean distinct;

    public MissedTransactionOperationCriteria() {}

    public MissedTransactionOperationCriteria(MissedTransactionOperationCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.amount = other.amount == null ? null : other.amount.copy();
        this.subsMsisdn = other.subsMsisdn == null ? null : other.subsMsisdn.copy();
        this.agentMsisdn = other.agentMsisdn == null ? null : other.agentMsisdn.copy();
        this.typeTransaction = other.typeTransaction == null ? null : other.typeTransaction.copy();
        this.createdAt = other.createdAt == null ? null : other.createdAt.copy();
        this.transactionStatus = other.transactionStatus == null ? null : other.transactionStatus.copy();
        this.senderZone = other.senderZone == null ? null : other.senderZone.copy();
        this.senderProfile = other.senderProfile == null ? null : other.senderProfile.copy();
        this.codeTerritory = other.codeTerritory == null ? null : other.codeTerritory.copy();
        this.subType = other.subType == null ? null : other.subType.copy();
        this.operationDate = other.operationDate == null ? null : other.operationDate.copy();
        this.isFraud = other.isFraud == null ? null : other.isFraud.copy();
        this.taggedAt = other.taggedAt == null ? null : other.taggedAt.copy();
        this.fraudSource = other.fraudSource == null ? null : other.fraudSource.copy();
        this.comment = other.comment == null ? null : other.comment.copy();
        this.falsePositiveDetectedAt = other.falsePositiveDetectedAt == null ? null : other.falsePositiveDetectedAt.copy();
        this.tid = other.tid == null ? null : other.tid.copy();
        this.parentMsisdn = other.parentMsisdn == null ? null : other.parentMsisdn.copy();
        this.fctDt = other.fctDt == null ? null : other.fctDt.copy();
        this.parentId = other.parentId == null ? null : other.parentId.copy();
        this.canceledAt = other.canceledAt == null ? null : other.canceledAt.copy();
        this.canceledId = other.canceledId == null ? null : other.canceledId.copy();
        this.productId = other.productId == null ? null : other.productId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public MissedTransactionOperationCriteria copy() {
        return new MissedTransactionOperationCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public DoubleFilter getAmount() {
        return amount;
    }

    public DoubleFilter amount() {
        if (amount == null) {
            amount = new DoubleFilter();
        }
        return amount;
    }

    public void setAmount(DoubleFilter amount) {
        this.amount = amount;
    }

    public StringFilter getSubsMsisdn() {
        return subsMsisdn;
    }

    public StringFilter subsMsisdn() {
        if (subsMsisdn == null) {
            subsMsisdn = new StringFilter();
        }
        return subsMsisdn;
    }

    public void setSubsMsisdn(StringFilter subsMsisdn) {
        this.subsMsisdn = subsMsisdn;
    }

    public StringFilter getAgentMsisdn() {
        return agentMsisdn;
    }

    public StringFilter agentMsisdn() {
        if (agentMsisdn == null) {
            agentMsisdn = new StringFilter();
        }
        return agentMsisdn;
    }

    public void setAgentMsisdn(StringFilter agentMsisdn) {
        this.agentMsisdn = agentMsisdn;
    }

    public StringFilter getTypeTransaction() {
        return typeTransaction;
    }

    public StringFilter typeTransaction() {
        if (typeTransaction == null) {
            typeTransaction = new StringFilter();
        }
        return typeTransaction;
    }

    public void setTypeTransaction(StringFilter typeTransaction) {
        this.typeTransaction = typeTransaction;
    }

    public InstantFilter getCreatedAt() {
        return createdAt;
    }

    public InstantFilter createdAt() {
        if (createdAt == null) {
            createdAt = new InstantFilter();
        }
        return createdAt;
    }

    public void setCreatedAt(InstantFilter createdAt) {
        this.createdAt = createdAt;
    }

    public StringFilter getTransactionStatus() {
        return transactionStatus;
    }

    public StringFilter transactionStatus() {
        if (transactionStatus == null) {
            transactionStatus = new StringFilter();
        }
        return transactionStatus;
    }

    public void setTransactionStatus(StringFilter transactionStatus) {
        this.transactionStatus = transactionStatus;
    }

    public StringFilter getSenderZone() {
        return senderZone;
    }

    public StringFilter senderZone() {
        if (senderZone == null) {
            senderZone = new StringFilter();
        }
        return senderZone;
    }

    public void setSenderZone(StringFilter senderZone) {
        this.senderZone = senderZone;
    }

    public StringFilter getSenderProfile() {
        return senderProfile;
    }

    public StringFilter senderProfile() {
        if (senderProfile == null) {
            senderProfile = new StringFilter();
        }
        return senderProfile;
    }

    public void setSenderProfile(StringFilter senderProfile) {
        this.senderProfile = senderProfile;
    }

    public StringFilter getCodeTerritory() {
        return codeTerritory;
    }

    public StringFilter codeTerritory() {
        if (codeTerritory == null) {
            codeTerritory = new StringFilter();
        }
        return codeTerritory;
    }

    public void setCodeTerritory(StringFilter codeTerritory) {
        this.codeTerritory = codeTerritory;
    }

    public StringFilter getSubType() {
        return subType;
    }

    public StringFilter subType() {
        if (subType == null) {
            subType = new StringFilter();
        }
        return subType;
    }

    public void setSubType(StringFilter subType) {
        this.subType = subType;
    }

    public InstantFilter getOperationDate() {
        return operationDate;
    }

    public InstantFilter operationDate() {
        if (operationDate == null) {
            operationDate = new InstantFilter();
        }
        return operationDate;
    }

    public void setOperationDate(InstantFilter operationDate) {
        this.operationDate = operationDate;
    }

    public BooleanFilter getIsFraud() {
        return isFraud;
    }

    public BooleanFilter isFraud() {
        if (isFraud == null) {
            isFraud = new BooleanFilter();
        }
        return isFraud;
    }

    public void setIsFraud(BooleanFilter isFraud) {
        this.isFraud = isFraud;
    }

    public InstantFilter getTaggedAt() {
        return taggedAt;
    }

    public InstantFilter taggedAt() {
        if (taggedAt == null) {
            taggedAt = new InstantFilter();
        }
        return taggedAt;
    }

    public void setTaggedAt(InstantFilter taggedAt) {
        this.taggedAt = taggedAt;
    }

    public StringFilter getFraudSource() {
        return fraudSource;
    }

    public StringFilter fraudSource() {
        if (fraudSource == null) {
            fraudSource = new StringFilter();
        }
        return fraudSource;
    }

    public void setFraudSource(StringFilter fraudSource) {
        this.fraudSource = fraudSource;
    }

    public StringFilter getComment() {
        return comment;
    }

    public StringFilter comment() {
        if (comment == null) {
            comment = new StringFilter();
        }
        return comment;
    }

    public void setComment(StringFilter comment) {
        this.comment = comment;
    }

    public InstantFilter getFalsePositiveDetectedAt() {
        return falsePositiveDetectedAt;
    }

    public InstantFilter falsePositiveDetectedAt() {
        if (falsePositiveDetectedAt == null) {
            falsePositiveDetectedAt = new InstantFilter();
        }
        return falsePositiveDetectedAt;
    }

    public void setFalsePositiveDetectedAt(InstantFilter falsePositiveDetectedAt) {
        this.falsePositiveDetectedAt = falsePositiveDetectedAt;
    }

    public StringFilter getTid() {
        return tid;
    }

    public StringFilter tid() {
        if (tid == null) {
            tid = new StringFilter();
        }
        return tid;
    }

    public void setTid(StringFilter tid) {
        this.tid = tid;
    }

    public StringFilter getParentMsisdn() {
        return parentMsisdn;
    }

    public StringFilter parentMsisdn() {
        if (parentMsisdn == null) {
            parentMsisdn = new StringFilter();
        }
        return parentMsisdn;
    }

    public void setParentMsisdn(StringFilter parentMsisdn) {
        this.parentMsisdn = parentMsisdn;
    }

    public StringFilter getFctDt() {
        return fctDt;
    }

    public StringFilter fctDt() {
        if (fctDt == null) {
            fctDt = new StringFilter();
        }
        return fctDt;
    }

    public void setFctDt(StringFilter fctDt) {
        this.fctDt = fctDt;
    }

    public StringFilter getParentId() {
        return parentId;
    }

    public StringFilter parentId() {
        if (parentId == null) {
            parentId = new StringFilter();
        }
        return parentId;
    }

    public void setParentId(StringFilter parentId) {
        this.parentId = parentId;
    }

    public InstantFilter getCanceledAt() {
        return canceledAt;
    }

    public InstantFilter canceledAt() {
        if (canceledAt == null) {
            canceledAt = new InstantFilter();
        }
        return canceledAt;
    }

    public void setCanceledAt(InstantFilter canceledAt) {
        this.canceledAt = canceledAt;
    }

    public StringFilter getCanceledId() {
        return canceledId;
    }

    public StringFilter canceledId() {
        if (canceledId == null) {
            canceledId = new StringFilter();
        }
        return canceledId;
    }

    public void setCanceledId(StringFilter canceledId) {
        this.canceledId = canceledId;
    }

    public StringFilter getProductId() {
        return productId;
    }

    public StringFilter productId() {
        if (productId == null) {
            productId = new StringFilter();
        }
        return productId;
    }

    public void setProductId(StringFilter productId) {
        this.productId = productId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final MissedTransactionOperationCriteria that = (MissedTransactionOperationCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(amount, that.amount) &&
            Objects.equals(subsMsisdn, that.subsMsisdn) &&
            Objects.equals(agentMsisdn, that.agentMsisdn) &&
            Objects.equals(typeTransaction, that.typeTransaction) &&
            Objects.equals(createdAt, that.createdAt) &&
            Objects.equals(transactionStatus, that.transactionStatus) &&
            Objects.equals(senderZone, that.senderZone) &&
            Objects.equals(senderProfile, that.senderProfile) &&
            Objects.equals(codeTerritory, that.codeTerritory) &&
            Objects.equals(subType, that.subType) &&
            Objects.equals(operationDate, that.operationDate) &&
            Objects.equals(isFraud, that.isFraud) &&
            Objects.equals(taggedAt, that.taggedAt) &&
            Objects.equals(fraudSource, that.fraudSource) &&
            Objects.equals(comment, that.comment) &&
            Objects.equals(falsePositiveDetectedAt, that.falsePositiveDetectedAt) &&
            Objects.equals(tid, that.tid) &&
            Objects.equals(parentMsisdn, that.parentMsisdn) &&
            Objects.equals(fctDt, that.fctDt) &&
            Objects.equals(parentId, that.parentId) &&
            Objects.equals(canceledAt, that.canceledAt) &&
            Objects.equals(canceledId, that.canceledId) &&
            Objects.equals(productId, that.productId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            amount,
            subsMsisdn,
            agentMsisdn,
            typeTransaction,
            createdAt,
            transactionStatus,
            senderZone,
            senderProfile,
            codeTerritory,
            subType,
            operationDate,
            isFraud,
            taggedAt,
            fraudSource,
            comment,
            falsePositiveDetectedAt,
            tid,
            parentMsisdn,
            fctDt,
            parentId,
            canceledAt,
            canceledId,
            productId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MissedTransactionOperationCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (amount != null ? "amount=" + amount + ", " : "") +
            (subsMsisdn != null ? "subsMsisdn=" + subsMsisdn + ", " : "") +
            (agentMsisdn != null ? "agentMsisdn=" + agentMsisdn + ", " : "") +
            (typeTransaction != null ? "typeTransaction=" + typeTransaction + ", " : "") +
            (createdAt != null ? "createdAt=" + createdAt + ", " : "") +
            (transactionStatus != null ? "transactionStatus=" + transactionStatus + ", " : "") +
            (senderZone != null ? "senderZone=" + senderZone + ", " : "") +
            (senderProfile != null ? "senderProfile=" + senderProfile + ", " : "") +
            (codeTerritory != null ? "codeTerritory=" + codeTerritory + ", " : "") +
            (subType != null ? "subType=" + subType + ", " : "") +
            (operationDate != null ? "operationDate=" + operationDate + ", " : "") +
            (isFraud != null ? "isFraud=" + isFraud + ", " : "") +
            (taggedAt != null ? "taggedAt=" + taggedAt + ", " : "") +
            (fraudSource != null ? "fraudSource=" + fraudSource + ", " : "") +
            (comment != null ? "comment=" + comment + ", " : "") +
            (falsePositiveDetectedAt != null ? "falsePositiveDetectedAt=" + falsePositiveDetectedAt + ", " : "") +
            (tid != null ? "tid=" + tid + ", " : "") +
            (parentMsisdn != null ? "parentMsisdn=" + parentMsisdn + ", " : "") +
            (fctDt != null ? "fctDt=" + fctDt + ", " : "") +
            (parentId != null ? "parentId=" + parentId + ", " : "") +
            (canceledAt != null ? "canceledAt=" + canceledAt + ", " : "") +
            (canceledId != null ? "canceledId=" + canceledId + ", " : "") +
            (productId != null ? "productId=" + productId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
