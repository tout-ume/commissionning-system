package sn.free.commissioning.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link sn.free.commissioning.domain.CalculationExecutionTrace} entity. This class is used
 * in {@link sn.free.commissioning.web.rest.CalculationExecutionTraceResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /calculation-execution-traces?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
public class CalculationExecutionTraceCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private DoubleFilter frequencyId;

    private InstantFilter executionDate;

    private BooleanFilter isExecuted;

    private StringFilter executionResult;

    private StringFilter spare1;

    private StringFilter spare2;

    private StringFilter spare3;

    private Boolean distinct;

    public CalculationExecutionTraceCriteria() {}

    public CalculationExecutionTraceCriteria(CalculationExecutionTraceCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.frequencyId = other.frequencyId == null ? null : other.frequencyId.copy();
        this.executionDate = other.executionDate == null ? null : other.executionDate.copy();
        this.isExecuted = other.isExecuted == null ? null : other.isExecuted.copy();
        this.executionResult = other.executionResult == null ? null : other.executionResult.copy();
        this.spare1 = other.spare1 == null ? null : other.spare1.copy();
        this.spare2 = other.spare2 == null ? null : other.spare2.copy();
        this.spare3 = other.spare3 == null ? null : other.spare3.copy();
        this.distinct = other.distinct;
    }

    @Override
    public CalculationExecutionTraceCriteria copy() {
        return new CalculationExecutionTraceCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public DoubleFilter getFrequencyId() {
        return frequencyId;
    }

    public DoubleFilter frequencyId() {
        if (frequencyId == null) {
            frequencyId = new DoubleFilter();
        }
        return frequencyId;
    }

    public void setFrequencyId(DoubleFilter frequencyId) {
        this.frequencyId = frequencyId;
    }

    public InstantFilter getExecutionDate() {
        return executionDate;
    }

    public InstantFilter executionDate() {
        if (executionDate == null) {
            executionDate = new InstantFilter();
        }
        return executionDate;
    }

    public void setExecutionDate(InstantFilter executionDate) {
        this.executionDate = executionDate;
    }

    public BooleanFilter getIsExecuted() {
        return isExecuted;
    }

    public BooleanFilter isExecuted() {
        if (isExecuted == null) {
            isExecuted = new BooleanFilter();
        }
        return isExecuted;
    }

    public void setIsExecuted(BooleanFilter isExecuted) {
        this.isExecuted = isExecuted;
    }

    public StringFilter getExecutionResult() {
        return executionResult;
    }

    public StringFilter executionResult() {
        if (executionResult == null) {
            executionResult = new StringFilter();
        }
        return executionResult;
    }

    public void setExecutionResult(StringFilter executionResult) {
        this.executionResult = executionResult;
    }

    public StringFilter getSpare1() {
        return spare1;
    }

    public StringFilter spare1() {
        if (spare1 == null) {
            spare1 = new StringFilter();
        }
        return spare1;
    }

    public void setSpare1(StringFilter spare1) {
        this.spare1 = spare1;
    }

    public StringFilter getSpare2() {
        return spare2;
    }

    public StringFilter spare2() {
        if (spare2 == null) {
            spare2 = new StringFilter();
        }
        return spare2;
    }

    public void setSpare2(StringFilter spare2) {
        this.spare2 = spare2;
    }

    public StringFilter getSpare3() {
        return spare3;
    }

    public StringFilter spare3() {
        if (spare3 == null) {
            spare3 = new StringFilter();
        }
        return spare3;
    }

    public void setSpare3(StringFilter spare3) {
        this.spare3 = spare3;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CalculationExecutionTraceCriteria that = (CalculationExecutionTraceCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(frequencyId, that.frequencyId) &&
            Objects.equals(executionDate, that.executionDate) &&
            Objects.equals(isExecuted, that.isExecuted) &&
            Objects.equals(executionResult, that.executionResult) &&
            Objects.equals(spare1, that.spare1) &&
            Objects.equals(spare2, that.spare2) &&
            Objects.equals(spare3, that.spare3) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, frequencyId, executionDate, isExecuted, executionResult, spare1, spare2, spare3, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CalculationExecutionTraceCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (frequencyId != null ? "frequencyId=" + frequencyId + ", " : "") +
            (executionDate != null ? "executionDate=" + executionDate + ", " : "") +
            (isExecuted != null ? "isExecuted=" + isExecuted + ", " : "") +
            (executionResult != null ? "executionResult=" + executionResult + ", " : "") +
            (spare1 != null ? "spare1=" + spare1 + ", " : "") +
            (spare2 != null ? "spare2=" + spare2 + ", " : "") +
            (spare3 != null ? "spare3=" + spare3 + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
