package sn.free.commissioning.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link sn.free.commissioning.domain.PartnerProfile} entity. This class is used
 * in {@link sn.free.commissioning.web.rest.PartnerProfileResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /partner-profiles?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
public class PartnerProfileCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter code;

    private StringFilter name;

    private StringFilter description;

    private IntegerFilter level;

    private LongFilter parentPartnerProfileId;

    private StringFilter createdBy;

    private InstantFilter createdDate;

    private StringFilter lastModifiedBy;

    private InstantFilter lastModifiedDate;

    private BooleanFilter canHaveMultipleZones;

    private IntegerFilter zoneThreshold;

    private IntegerFilter simBackupThreshold;

    private BooleanFilter canDoProfileAssignment;

    private Boolean distinct;

    public PartnerProfileCriteria() {}

    public PartnerProfileCriteria(PartnerProfileCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.level = other.level == null ? null : other.level.copy();
        this.parentPartnerProfileId = other.parentPartnerProfileId == null ? null : other.parentPartnerProfileId.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.canHaveMultipleZones = other.canHaveMultipleZones == null ? null : other.canHaveMultipleZones.copy();
        this.zoneThreshold = other.zoneThreshold == null ? null : other.zoneThreshold.copy();
        this.simBackupThreshold = other.simBackupThreshold == null ? null : other.simBackupThreshold.copy();
        this.canDoProfileAssignment = other.canDoProfileAssignment == null ? null : other.canDoProfileAssignment.copy();
        this.distinct = other.distinct;
    }

    @Override
    public PartnerProfileCriteria copy() {
        return new PartnerProfileCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getCode() {
        return code;
    }

    public StringFilter code() {
        if (code == null) {
            code = new StringFilter();
        }
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getDescription() {
        return description;
    }

    public StringFilter description() {
        if (description == null) {
            description = new StringFilter();
        }
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public IntegerFilter getLevel() {
        return level;
    }

    public IntegerFilter level() {
        if (level == null) {
            level = new IntegerFilter();
        }
        return level;
    }

    public void setLevel(IntegerFilter level) {
        this.level = level;
    }

    public LongFilter getParentPartnerProfileId() {
        return parentPartnerProfileId;
    }

    public LongFilter parentPartnerProfileId() {
        if (parentPartnerProfileId == null) {
            parentPartnerProfileId = new LongFilter();
        }
        return parentPartnerProfileId;
    }

    public void setParentPartnerProfileId(LongFilter parentPartnerProfileId) {
        this.parentPartnerProfileId = parentPartnerProfileId;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public StringFilter createdBy() {
        if (createdBy == null) {
            createdBy = new StringFilter();
        }
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public InstantFilter createdDate() {
        if (createdDate == null) {
            createdDate = new InstantFilter();
        }
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public StringFilter lastModifiedBy() {
        if (lastModifiedBy == null) {
            lastModifiedBy = new StringFilter();
        }
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public InstantFilter lastModifiedDate() {
        if (lastModifiedDate == null) {
            lastModifiedDate = new InstantFilter();
        }
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public BooleanFilter getCanHaveMultipleZones() {
        return canHaveMultipleZones;
    }

    public BooleanFilter canHaveMultipleZones() {
        if (canHaveMultipleZones == null) {
            canHaveMultipleZones = new BooleanFilter();
        }
        return canHaveMultipleZones;
    }

    public void setCanHaveMultipleZones(BooleanFilter canHaveMultipleZones) {
        this.canHaveMultipleZones = canHaveMultipleZones;
    }

    public IntegerFilter getZoneThreshold() {
        return zoneThreshold;
    }

    public IntegerFilter zoneThreshold() {
        if (zoneThreshold == null) {
            zoneThreshold = new IntegerFilter();
        }
        return zoneThreshold;
    }

    public void setZoneThreshold(IntegerFilter zoneThreshold) {
        this.zoneThreshold = zoneThreshold;
    }

    public IntegerFilter getSimBackupThreshold() {
        return simBackupThreshold;
    }

    public IntegerFilter simBackupThreshold() {
        if (simBackupThreshold == null) {
            simBackupThreshold = new IntegerFilter();
        }
        return simBackupThreshold;
    }

    public void setSimBackupThreshold(IntegerFilter simBackupThreshold) {
        this.simBackupThreshold = simBackupThreshold;
    }

    public BooleanFilter getCanDoProfileAssignment() {
        return canDoProfileAssignment;
    }

    public BooleanFilter canDoProfileAssignment() {
        if (canDoProfileAssignment == null) {
            canDoProfileAssignment = new BooleanFilter();
        }
        return canDoProfileAssignment;
    }

    public void setCanDoProfileAssignment(BooleanFilter canDoProfileAssignment) {
        this.canDoProfileAssignment = canDoProfileAssignment;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PartnerProfileCriteria that = (PartnerProfileCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(code, that.code) &&
            Objects.equals(name, that.name) &&
            Objects.equals(description, that.description) &&
            Objects.equals(level, that.level) &&
            Objects.equals(parentPartnerProfileId, that.parentPartnerProfileId) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(canHaveMultipleZones, that.canHaveMultipleZones) &&
            Objects.equals(zoneThreshold, that.zoneThreshold) &&
            Objects.equals(simBackupThreshold, that.simBackupThreshold) &&
            Objects.equals(canDoProfileAssignment, that.canDoProfileAssignment) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            code,
            name,
            description,
            level,
            parentPartnerProfileId,
            createdBy,
            createdDate,
            lastModifiedBy,
            lastModifiedDate,
            canHaveMultipleZones,
            zoneThreshold,
            simBackupThreshold,
            canDoProfileAssignment,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PartnerProfileCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (code != null ? "code=" + code + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (description != null ? "description=" + description + ", " : "") +
            (level != null ? "level=" + level + ", " : "") +
            (parentPartnerProfileId != null ? "parentPartnerProfileId=" + parentPartnerProfileId + ", " : "") +
            (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
            (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
            (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
            (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
            (canHaveMultipleZones != null ? "canHaveMultipleZones=" + canHaveMultipleZones + ", " : "") +
            (zoneThreshold != null ? "zoneThreshold=" + zoneThreshold + ", " : "") +
            (simBackupThreshold != null ? "simBackupThreshold=" + simBackupThreshold + ", " : "") +
            (canDoProfileAssignment != null ? "canDoProfileAssignment=" + canDoProfileAssignment + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
