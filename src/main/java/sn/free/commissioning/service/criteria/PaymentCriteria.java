package sn.free.commissioning.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import sn.free.commissioning.domain.enumeration.PaymentStatus;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link sn.free.commissioning.domain.Payment} entity. This class is used
 * in {@link sn.free.commissioning.web.rest.PaymentResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /payments?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
public class PaymentCriteria implements Serializable, Criteria {

    /**
     * Class for filtering PaymentStatus
     */
    public static class PaymentStatusFilter extends Filter<PaymentStatus> {

        public PaymentStatusFilter() {}

        public PaymentStatusFilter(PaymentStatusFilter filter) {
            super(filter);
        }

        @Override
        public PaymentStatusFilter copy() {
            return new PaymentStatusFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private DoubleFilter amount;

    private StringFilter senderMsisdn;

    private StringFilter receiverMsisdn;

    private StringFilter receiverProfile;

    private InstantFilter periodStartDate;

    private InstantFilter periodEndDate;

    private DoubleFilter executionDuration;

    private StringFilter createdBy;

    private InstantFilter createdDate;

    private StringFilter lastModifiedBy;

    private InstantFilter lastModifiedDate;

    private PaymentStatusFilter paymentStatus;

    private StringFilter spare1;

    private StringFilter spare2;

    private StringFilter spare3;

    private LongFilter commissionId;

    private Boolean distinct;

    public PaymentCriteria() {}

    public PaymentCriteria(PaymentCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.amount = other.amount == null ? null : other.amount.copy();
        this.senderMsisdn = other.senderMsisdn == null ? null : other.senderMsisdn.copy();
        this.receiverMsisdn = other.receiverMsisdn == null ? null : other.receiverMsisdn.copy();
        this.receiverProfile = other.receiverProfile == null ? null : other.receiverProfile.copy();
        this.periodStartDate = other.periodStartDate == null ? null : other.periodStartDate.copy();
        this.periodEndDate = other.periodEndDate == null ? null : other.periodEndDate.copy();
        this.executionDuration = other.executionDuration == null ? null : other.executionDuration.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.paymentStatus = other.paymentStatus == null ? null : other.paymentStatus.copy();
        this.spare1 = other.spare1 == null ? null : other.spare1.copy();
        this.spare2 = other.spare2 == null ? null : other.spare2.copy();
        this.spare3 = other.spare3 == null ? null : other.spare3.copy();
        this.commissionId = other.commissionId == null ? null : other.commissionId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public PaymentCriteria copy() {
        return new PaymentCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public DoubleFilter getAmount() {
        return amount;
    }

    public DoubleFilter amount() {
        if (amount == null) {
            amount = new DoubleFilter();
        }
        return amount;
    }

    public void setAmount(DoubleFilter amount) {
        this.amount = amount;
    }

    public StringFilter getSenderMsisdn() {
        return senderMsisdn;
    }

    public StringFilter senderMsisdn() {
        if (senderMsisdn == null) {
            senderMsisdn = new StringFilter();
        }
        return senderMsisdn;
    }

    public void setSenderMsisdn(StringFilter senderMsisdn) {
        this.senderMsisdn = senderMsisdn;
    }

    public StringFilter getReceiverMsisdn() {
        return receiverMsisdn;
    }

    public StringFilter receiverMsisdn() {
        if (receiverMsisdn == null) {
            receiverMsisdn = new StringFilter();
        }
        return receiverMsisdn;
    }

    public void setReceiverMsisdn(StringFilter receiverMsisdn) {
        this.receiverMsisdn = receiverMsisdn;
    }

    public StringFilter getReceiverProfile() {
        return receiverProfile;
    }

    public StringFilter receiverProfile() {
        if (receiverProfile == null) {
            receiverProfile = new StringFilter();
        }
        return receiverProfile;
    }

    public void setReceiverProfile(StringFilter receiverProfile) {
        this.receiverProfile = receiverProfile;
    }

    public InstantFilter getPeriodStartDate() {
        return periodStartDate;
    }

    public InstantFilter periodStartDate() {
        if (periodStartDate == null) {
            periodStartDate = new InstantFilter();
        }
        return periodStartDate;
    }

    public void setPeriodStartDate(InstantFilter periodStartDate) {
        this.periodStartDate = periodStartDate;
    }

    public InstantFilter getPeriodEndDate() {
        return periodEndDate;
    }

    public InstantFilter periodEndDate() {
        if (periodEndDate == null) {
            periodEndDate = new InstantFilter();
        }
        return periodEndDate;
    }

    public void setPeriodEndDate(InstantFilter periodEndDate) {
        this.periodEndDate = periodEndDate;
    }

    public DoubleFilter getExecutionDuration() {
        return executionDuration;
    }

    public DoubleFilter executionDuration() {
        if (executionDuration == null) {
            executionDuration = new DoubleFilter();
        }
        return executionDuration;
    }

    public void setExecutionDuration(DoubleFilter executionDuration) {
        this.executionDuration = executionDuration;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public StringFilter createdBy() {
        if (createdBy == null) {
            createdBy = new StringFilter();
        }
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public InstantFilter createdDate() {
        if (createdDate == null) {
            createdDate = new InstantFilter();
        }
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public StringFilter lastModifiedBy() {
        if (lastModifiedBy == null) {
            lastModifiedBy = new StringFilter();
        }
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public InstantFilter lastModifiedDate() {
        if (lastModifiedDate == null) {
            lastModifiedDate = new InstantFilter();
        }
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public PaymentStatusFilter getPaymentStatus() {
        return paymentStatus;
    }

    public PaymentStatusFilter paymentStatus() {
        if (paymentStatus == null) {
            paymentStatus = new PaymentStatusFilter();
        }
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatusFilter paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public StringFilter getSpare1() {
        return spare1;
    }

    public StringFilter spare1() {
        if (spare1 == null) {
            spare1 = new StringFilter();
        }
        return spare1;
    }

    public void setSpare1(StringFilter spare1) {
        this.spare1 = spare1;
    }

    public StringFilter getSpare2() {
        return spare2;
    }

    public StringFilter spare2() {
        if (spare2 == null) {
            spare2 = new StringFilter();
        }
        return spare2;
    }

    public void setSpare2(StringFilter spare2) {
        this.spare2 = spare2;
    }

    public StringFilter getSpare3() {
        return spare3;
    }

    public StringFilter spare3() {
        if (spare3 == null) {
            spare3 = new StringFilter();
        }
        return spare3;
    }

    public void setSpare3(StringFilter spare3) {
        this.spare3 = spare3;
    }

    public LongFilter getCommissionId() {
        return commissionId;
    }

    public LongFilter commissionId() {
        if (commissionId == null) {
            commissionId = new LongFilter();
        }
        return commissionId;
    }

    public void setCommissionId(LongFilter commissionId) {
        this.commissionId = commissionId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PaymentCriteria that = (PaymentCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(amount, that.amount) &&
            Objects.equals(senderMsisdn, that.senderMsisdn) &&
            Objects.equals(receiverMsisdn, that.receiverMsisdn) &&
            Objects.equals(receiverProfile, that.receiverProfile) &&
            Objects.equals(periodStartDate, that.periodStartDate) &&
            Objects.equals(periodEndDate, that.periodEndDate) &&
            Objects.equals(executionDuration, that.executionDuration) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(paymentStatus, that.paymentStatus) &&
            Objects.equals(spare1, that.spare1) &&
            Objects.equals(spare2, that.spare2) &&
            Objects.equals(spare3, that.spare3) &&
            Objects.equals(commissionId, that.commissionId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            amount,
            senderMsisdn,
            receiverMsisdn,
            receiverProfile,
            periodStartDate,
            periodEndDate,
            executionDuration,
            createdBy,
            createdDate,
            lastModifiedBy,
            lastModifiedDate,
            paymentStatus,
            spare1,
            spare2,
            spare3,
            commissionId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PaymentCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (amount != null ? "amount=" + amount + ", " : "") +
            (senderMsisdn != null ? "senderMsisdn=" + senderMsisdn + ", " : "") +
            (receiverMsisdn != null ? "receiverMsisdn=" + receiverMsisdn + ", " : "") +
            (receiverProfile != null ? "receiverProfile=" + receiverProfile + ", " : "") +
            (periodStartDate != null ? "periodStartDate=" + periodStartDate + ", " : "") +
            (periodEndDate != null ? "periodEndDate=" + periodEndDate + ", " : "") +
            (executionDuration != null ? "executionDuration=" + executionDuration + ", " : "") +
            (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
            (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
            (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
            (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
            (paymentStatus != null ? "paymentStatus=" + paymentStatus + ", " : "") +
            (spare1 != null ? "spare1=" + spare1 + ", " : "") +
            (spare2 != null ? "spare2=" + spare2 + ", " : "") +
            (spare3 != null ? "spare3=" + spare3 + ", " : "") +
            (commissionId != null ? "commissionId=" + commissionId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
