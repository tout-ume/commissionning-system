package sn.free.commissioning.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import sn.free.commissioning.domain.enumeration.CommissioningPlanState;
import sn.free.commissioning.domain.enumeration.CommissioningPlanType;
import sn.free.commissioning.domain.enumeration.PaymentStrategy;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link sn.free.commissioning.domain.CommissioningPlan} entity. This class is used
 * in {@link sn.free.commissioning.web.rest.CommissioningPlanResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /commissioning-plans?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
public class CommissioningPlanCriteria implements Serializable, Criteria {

    /**
     * Class for filtering PaymentStrategy
     */
    public static class PaymentStrategyFilter extends Filter<PaymentStrategy> {

        public PaymentStrategyFilter() {}

        public PaymentStrategyFilter(PaymentStrategyFilter filter) {
            super(filter);
        }

        @Override
        public PaymentStrategyFilter copy() {
            return new PaymentStrategyFilter(this);
        }
    }

    /**
     * Class for filtering CommissioningPlanState
     */
    public static class CommissioningPlanStateFilter extends Filter<CommissioningPlanState> {

        public CommissioningPlanStateFilter() {}

        public CommissioningPlanStateFilter(CommissioningPlanStateFilter filter) {
            super(filter);
        }

        @Override
        public CommissioningPlanStateFilter copy() {
            return new CommissioningPlanStateFilter(this);
        }
    }

    /**
     * Class for filtering CommissioningPlanType
     */
    public static class CommissioningPlanTypeFilter extends Filter<CommissioningPlanType> {

        public CommissioningPlanTypeFilter() {}

        public CommissioningPlanTypeFilter(CommissioningPlanTypeFilter filter) {
            super(filter);
        }

        @Override
        public CommissioningPlanTypeFilter copy() {
            return new CommissioningPlanTypeFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private InstantFilter beginDate;

    private InstantFilter endDate;

    private InstantFilter creationDate;

    private StringFilter createdBy;

    private InstantFilter archiveDate;

    private StringFilter archivedBy;

    private PaymentStrategyFilter paymentStrategy;

    private CommissioningPlanStateFilter state;

    private CommissioningPlanTypeFilter commissioningPlanType;

    private StringFilter spare1;

    private StringFilter spare2;

    private StringFilter spare3;

    private LongFilter paymentFrequencyId;

    private LongFilter calculusFrequencyId;

    private LongFilter calculusPeriodId;

    private LongFilter paymentPeriodId;

    private LongFilter servicesId;

    private LongFilter configurationPlanId;

    private Boolean distinct;

    public CommissioningPlanCriteria() {}

    public CommissioningPlanCriteria(CommissioningPlanCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.beginDate = other.beginDate == null ? null : other.beginDate.copy();
        this.endDate = other.endDate == null ? null : other.endDate.copy();
        this.creationDate = other.creationDate == null ? null : other.creationDate.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.archiveDate = other.archiveDate == null ? null : other.archiveDate.copy();
        this.archivedBy = other.archivedBy == null ? null : other.archivedBy.copy();
        this.paymentStrategy = other.paymentStrategy == null ? null : other.paymentStrategy.copy();
        this.state = other.state == null ? null : other.state.copy();
        this.commissioningPlanType = other.commissioningPlanType == null ? null : other.commissioningPlanType.copy();
        this.spare1 = other.spare1 == null ? null : other.spare1.copy();
        this.spare2 = other.spare2 == null ? null : other.spare2.copy();
        this.spare3 = other.spare3 == null ? null : other.spare3.copy();
        this.paymentFrequencyId = other.paymentFrequencyId == null ? null : other.paymentFrequencyId.copy();
        this.calculusFrequencyId = other.calculusFrequencyId == null ? null : other.calculusFrequencyId.copy();
        this.calculusPeriodId = other.calculusPeriodId == null ? null : other.calculusPeriodId.copy();
        this.paymentPeriodId = other.paymentPeriodId == null ? null : other.paymentPeriodId.copy();
        this.servicesId = other.servicesId == null ? null : other.servicesId.copy();
        this.configurationPlanId = other.configurationPlanId == null ? null : other.configurationPlanId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public CommissioningPlanCriteria copy() {
        return new CommissioningPlanCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public InstantFilter getBeginDate() {
        return beginDate;
    }

    public InstantFilter beginDate() {
        if (beginDate == null) {
            beginDate = new InstantFilter();
        }
        return beginDate;
    }

    public void setBeginDate(InstantFilter beginDate) {
        this.beginDate = beginDate;
    }

    public InstantFilter getEndDate() {
        return endDate;
    }

    public InstantFilter endDate() {
        if (endDate == null) {
            endDate = new InstantFilter();
        }
        return endDate;
    }

    public void setEndDate(InstantFilter endDate) {
        this.endDate = endDate;
    }

    public InstantFilter getCreationDate() {
        return creationDate;
    }

    public InstantFilter creationDate() {
        if (creationDate == null) {
            creationDate = new InstantFilter();
        }
        return creationDate;
    }

    public void setCreationDate(InstantFilter creationDate) {
        this.creationDate = creationDate;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public StringFilter createdBy() {
        if (createdBy == null) {
            createdBy = new StringFilter();
        }
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getArchiveDate() {
        return archiveDate;
    }

    public InstantFilter archiveDate() {
        if (archiveDate == null) {
            archiveDate = new InstantFilter();
        }
        return archiveDate;
    }

    public void setArchiveDate(InstantFilter archiveDate) {
        this.archiveDate = archiveDate;
    }

    public StringFilter getArchivedBy() {
        return archivedBy;
    }

    public StringFilter archivedBy() {
        if (archivedBy == null) {
            archivedBy = new StringFilter();
        }
        return archivedBy;
    }

    public void setArchivedBy(StringFilter archivedBy) {
        this.archivedBy = archivedBy;
    }

    public PaymentStrategyFilter getPaymentStrategy() {
        return paymentStrategy;
    }

    public PaymentStrategyFilter paymentStrategy() {
        if (paymentStrategy == null) {
            paymentStrategy = new PaymentStrategyFilter();
        }
        return paymentStrategy;
    }

    public void setPaymentStrategy(PaymentStrategyFilter paymentStrategy) {
        this.paymentStrategy = paymentStrategy;
    }

    public CommissioningPlanStateFilter getState() {
        return state;
    }

    public CommissioningPlanStateFilter state() {
        if (state == null) {
            state = new CommissioningPlanStateFilter();
        }
        return state;
    }

    public void setState(CommissioningPlanStateFilter state) {
        this.state = state;
    }

    public CommissioningPlanTypeFilter getCommissioningPlanType() {
        return commissioningPlanType;
    }

    public CommissioningPlanTypeFilter commissioningPlanType() {
        if (commissioningPlanType == null) {
            commissioningPlanType = new CommissioningPlanTypeFilter();
        }
        return commissioningPlanType;
    }

    public void setCommissioningPlanType(CommissioningPlanTypeFilter commissioningPlanType) {
        this.commissioningPlanType = commissioningPlanType;
    }

    public StringFilter getSpare1() {
        return spare1;
    }

    public StringFilter spare1() {
        if (spare1 == null) {
            spare1 = new StringFilter();
        }
        return spare1;
    }

    public void setSpare1(StringFilter spare1) {
        this.spare1 = spare1;
    }

    public StringFilter getSpare2() {
        return spare2;
    }

    public StringFilter spare2() {
        if (spare2 == null) {
            spare2 = new StringFilter();
        }
        return spare2;
    }

    public void setSpare2(StringFilter spare2) {
        this.spare2 = spare2;
    }

    public StringFilter getSpare3() {
        return spare3;
    }

    public StringFilter spare3() {
        if (spare3 == null) {
            spare3 = new StringFilter();
        }
        return spare3;
    }

    public void setSpare3(StringFilter spare3) {
        this.spare3 = spare3;
    }

    public LongFilter getPaymentFrequencyId() {
        return paymentFrequencyId;
    }

    public LongFilter paymentFrequencyId() {
        if (paymentFrequencyId == null) {
            paymentFrequencyId = new LongFilter();
        }
        return paymentFrequencyId;
    }

    public void setPaymentFrequencyId(LongFilter paymentFrequencyId) {
        this.paymentFrequencyId = paymentFrequencyId;
    }

    public LongFilter getCalculusFrequencyId() {
        return calculusFrequencyId;
    }

    public LongFilter calculusFrequencyId() {
        if (calculusFrequencyId == null) {
            calculusFrequencyId = new LongFilter();
        }
        return calculusFrequencyId;
    }

    public void setCalculusFrequencyId(LongFilter calculusFrequencyId) {
        this.calculusFrequencyId = calculusFrequencyId;
    }

    public LongFilter getCalculusPeriodId() {
        return calculusPeriodId;
    }

    public LongFilter calculusPeriodId() {
        if (calculusPeriodId == null) {
            calculusPeriodId = new LongFilter();
        }
        return calculusPeriodId;
    }

    public void setCalculusPeriodId(LongFilter calculusPeriodId) {
        this.calculusPeriodId = calculusPeriodId;
    }

    public LongFilter getPaymentPeriodId() {
        return paymentPeriodId;
    }

    public LongFilter paymentPeriodId() {
        if (paymentPeriodId == null) {
            paymentPeriodId = new LongFilter();
        }
        return paymentPeriodId;
    }

    public void setPaymentPeriodId(LongFilter paymentPeriodId) {
        this.paymentPeriodId = paymentPeriodId;
    }

    public LongFilter getServicesId() {
        return servicesId;
    }

    public LongFilter servicesId() {
        if (servicesId == null) {
            servicesId = new LongFilter();
        }
        return servicesId;
    }

    public void setServicesId(LongFilter servicesId) {
        this.servicesId = servicesId;
    }

    public LongFilter getConfigurationPlanId() {
        return configurationPlanId;
    }

    public LongFilter configurationPlanId() {
        if (configurationPlanId == null) {
            configurationPlanId = new LongFilter();
        }
        return configurationPlanId;
    }

    public void setConfigurationPlanId(LongFilter configurationPlanId) {
        this.configurationPlanId = configurationPlanId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CommissioningPlanCriteria that = (CommissioningPlanCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(beginDate, that.beginDate) &&
            Objects.equals(endDate, that.endDate) &&
            Objects.equals(creationDate, that.creationDate) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(archiveDate, that.archiveDate) &&
            Objects.equals(archivedBy, that.archivedBy) &&
            Objects.equals(paymentStrategy, that.paymentStrategy) &&
            Objects.equals(state, that.state) &&
            Objects.equals(commissioningPlanType, that.commissioningPlanType) &&
            Objects.equals(spare1, that.spare1) &&
            Objects.equals(spare2, that.spare2) &&
            Objects.equals(spare3, that.spare3) &&
            Objects.equals(paymentFrequencyId, that.paymentFrequencyId) &&
            Objects.equals(calculusFrequencyId, that.calculusFrequencyId) &&
            Objects.equals(calculusPeriodId, that.calculusPeriodId) &&
            Objects.equals(paymentPeriodId, that.paymentPeriodId) &&
            Objects.equals(servicesId, that.servicesId) &&
            Objects.equals(configurationPlanId, that.configurationPlanId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            name,
            beginDate,
            endDate,
            creationDate,
            createdBy,
            archiveDate,
            archivedBy,
            paymentStrategy,
            state,
            commissioningPlanType,
            spare1,
            spare2,
            spare3,
            paymentFrequencyId,
            calculusFrequencyId,
            calculusPeriodId,
            paymentPeriodId,
            servicesId,
            configurationPlanId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommissioningPlanCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (beginDate != null ? "beginDate=" + beginDate + ", " : "") +
            (endDate != null ? "endDate=" + endDate + ", " : "") +
            (creationDate != null ? "creationDate=" + creationDate + ", " : "") +
            (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
            (archiveDate != null ? "archiveDate=" + archiveDate + ", " : "") +
            (archivedBy != null ? "archivedBy=" + archivedBy + ", " : "") +
            (paymentStrategy != null ? "paymentStrategy=" + paymentStrategy + ", " : "") +
            (state != null ? "state=" + state + ", " : "") +
            (commissioningPlanType != null ? "commissioningPlanType=" + commissioningPlanType + ", " : "") +
            (spare1 != null ? "spare1=" + spare1 + ", " : "") +
            (spare2 != null ? "spare2=" + spare2 + ", " : "") +
            (spare3 != null ? "spare3=" + spare3 + ", " : "") +
            (paymentFrequencyId != null ? "paymentFrequencyId=" + paymentFrequencyId + ", " : "") +
            (calculusFrequencyId != null ? "calculusFrequencyId=" + calculusFrequencyId + ", " : "") +
            (calculusPeriodId != null ? "calculusPeriodId=" + calculusPeriodId + ", " : "") +
            (paymentPeriodId != null ? "paymentPeriodId=" + paymentPeriodId + ", " : "") +
            (servicesId != null ? "servicesId=" + servicesId + ", " : "") +
            (configurationPlanId != null ? "configurationPlanId=" + configurationPlanId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
