package sn.free.commissioning.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link sn.free.commissioning.domain.ConfigurationPlan} entity. This class is used
 * in {@link sn.free.commissioning.web.rest.ConfigurationPlanResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /configuration-plans?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
public class ConfigurationPlanCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter spare1;

    private StringFilter spare2;

    private StringFilter spare3;

    private LongFilter commissionTypeId;

    private LongFilter commissionId;

    private LongFilter partnerProfileId;

    private LongFilter commissioningPlanId;

    private LongFilter zonesId;

    private Boolean distinct;

    public ConfigurationPlanCriteria() {}

    public ConfigurationPlanCriteria(ConfigurationPlanCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.spare1 = other.spare1 == null ? null : other.spare1.copy();
        this.spare2 = other.spare2 == null ? null : other.spare2.copy();
        this.spare3 = other.spare3 == null ? null : other.spare3.copy();
        this.commissionTypeId = other.commissionTypeId == null ? null : other.commissionTypeId.copy();
        this.commissionId = other.commissionId == null ? null : other.commissionId.copy();
        this.partnerProfileId = other.partnerProfileId == null ? null : other.partnerProfileId.copy();
        this.commissioningPlanId = other.commissioningPlanId == null ? null : other.commissioningPlanId.copy();
        this.zonesId = other.zonesId == null ? null : other.zonesId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public ConfigurationPlanCriteria copy() {
        return new ConfigurationPlanCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getSpare1() {
        return spare1;
    }

    public StringFilter spare1() {
        if (spare1 == null) {
            spare1 = new StringFilter();
        }
        return spare1;
    }

    public void setSpare1(StringFilter spare1) {
        this.spare1 = spare1;
    }

    public StringFilter getSpare2() {
        return spare2;
    }

    public StringFilter spare2() {
        if (spare2 == null) {
            spare2 = new StringFilter();
        }
        return spare2;
    }

    public void setSpare2(StringFilter spare2) {
        this.spare2 = spare2;
    }

    public StringFilter getSpare3() {
        return spare3;
    }

    public StringFilter spare3() {
        if (spare3 == null) {
            spare3 = new StringFilter();
        }
        return spare3;
    }

    public void setSpare3(StringFilter spare3) {
        this.spare3 = spare3;
    }

    public LongFilter getCommissionTypeId() {
        return commissionTypeId;
    }

    public LongFilter commissionTypeId() {
        if (commissionTypeId == null) {
            commissionTypeId = new LongFilter();
        }
        return commissionTypeId;
    }

    public void setCommissionTypeId(LongFilter commissionTypeId) {
        this.commissionTypeId = commissionTypeId;
    }

    public LongFilter getCommissionId() {
        return commissionId;
    }

    public LongFilter commissionId() {
        if (commissionId == null) {
            commissionId = new LongFilter();
        }
        return commissionId;
    }

    public void setCommissionId(LongFilter commissionId) {
        this.commissionId = commissionId;
    }

    public LongFilter getPartnerProfileId() {
        return partnerProfileId;
    }

    public LongFilter partnerProfileId() {
        if (partnerProfileId == null) {
            partnerProfileId = new LongFilter();
        }
        return partnerProfileId;
    }

    public void setPartnerProfileId(LongFilter partnerProfileId) {
        this.partnerProfileId = partnerProfileId;
    }

    public LongFilter getCommissioningPlanId() {
        return commissioningPlanId;
    }

    public LongFilter commissioningPlanId() {
        if (commissioningPlanId == null) {
            commissioningPlanId = new LongFilter();
        }
        return commissioningPlanId;
    }

    public void setCommissioningPlanId(LongFilter commissioningPlanId) {
        this.commissioningPlanId = commissioningPlanId;
    }

    public LongFilter getZonesId() {
        return zonesId;
    }

    public LongFilter zonesId() {
        if (zonesId == null) {
            zonesId = new LongFilter();
        }
        return zonesId;
    }

    public void setZonesId(LongFilter zonesId) {
        this.zonesId = zonesId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ConfigurationPlanCriteria that = (ConfigurationPlanCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(spare1, that.spare1) &&
            Objects.equals(spare2, that.spare2) &&
            Objects.equals(spare3, that.spare3) &&
            Objects.equals(commissionTypeId, that.commissionTypeId) &&
            Objects.equals(commissionId, that.commissionId) &&
            Objects.equals(partnerProfileId, that.partnerProfileId) &&
            Objects.equals(commissioningPlanId, that.commissioningPlanId) &&
            Objects.equals(zonesId, that.zonesId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            spare1,
            spare2,
            spare3,
            commissionTypeId,
            commissionId,
            partnerProfileId,
            commissioningPlanId,
            zonesId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ConfigurationPlanCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (spare1 != null ? "spare1=" + spare1 + ", " : "") +
            (spare2 != null ? "spare2=" + spare2 + ", " : "") +
            (spare3 != null ? "spare3=" + spare3 + ", " : "") +
            (commissionTypeId != null ? "commissionTypeId=" + commissionTypeId + ", " : "") +
            (commissionId != null ? "commissionId=" + commissionId + ", " : "") +
            (partnerProfileId != null ? "partnerProfileId=" + partnerProfileId + ", " : "") +
            (commissioningPlanId != null ? "commissioningPlanId=" + commissioningPlanId + ", " : "") +
            (zonesId != null ? "zonesId=" + zonesId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
