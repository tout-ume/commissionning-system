package sn.free.commissioning.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import sn.free.commissioning.domain.enumeration.CommissioningPlanType;
import sn.free.commissioning.domain.enumeration.ServiceTypeStatus;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link sn.free.commissioning.domain.ServiceType} entity. This class is used
 * in {@link sn.free.commissioning.web.rest.ServiceTypeResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /service-types?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
public class ServiceTypeCriteria implements Serializable, Criteria {

    /**
     * Class for filtering ServiceTypeStatus
     */
    public static class ServiceTypeStatusFilter extends Filter<ServiceTypeStatus> {

        public ServiceTypeStatusFilter() {}

        public ServiceTypeStatusFilter(ServiceTypeStatusFilter filter) {
            super(filter);
        }

        @Override
        public ServiceTypeStatusFilter copy() {
            return new ServiceTypeStatusFilter(this);
        }
    }

    /**
     * Class for filtering CommissioningPlanType
     */
    public static class CommissioningPlanTypeFilter extends Filter<CommissioningPlanType> {

        public CommissioningPlanTypeFilter() {}

        public CommissioningPlanTypeFilter(CommissioningPlanTypeFilter filter) {
            super(filter);
        }

        @Override
        public CommissioningPlanTypeFilter copy() {
            return new CommissioningPlanTypeFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private StringFilter code;

    private StringFilter description;

    private ServiceTypeStatusFilter state;

    private BooleanFilter isCosRelated;

    private CommissioningPlanTypeFilter commissioningPlanType;

    private StringFilter createdBy;

    private InstantFilter createdDate;

    private StringFilter lastModifiedBy;

    private InstantFilter lastModifiedDate;

    private LongFilter commissionPlanId;

    private Boolean distinct;

    public ServiceTypeCriteria() {}

    public ServiceTypeCriteria(ServiceTypeCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.code = other.code == null ? null : other.code.copy();
        this.description = other.description == null ? null : other.description.copy();
        this.state = other.state == null ? null : other.state.copy();
        this.isCosRelated = other.isCosRelated == null ? null : other.isCosRelated.copy();
        this.commissioningPlanType = other.commissioningPlanType == null ? null : other.commissioningPlanType.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.commissionPlanId = other.commissionPlanId == null ? null : other.commissionPlanId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public ServiceTypeCriteria copy() {
        return new ServiceTypeCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getCode() {
        return code;
    }

    public StringFilter code() {
        if (code == null) {
            code = new StringFilter();
        }
        return code;
    }

    public void setCode(StringFilter code) {
        this.code = code;
    }

    public StringFilter getDescription() {
        return description;
    }

    public StringFilter description() {
        if (description == null) {
            description = new StringFilter();
        }
        return description;
    }

    public void setDescription(StringFilter description) {
        this.description = description;
    }

    public ServiceTypeStatusFilter getState() {
        return state;
    }

    public ServiceTypeStatusFilter state() {
        if (state == null) {
            state = new ServiceTypeStatusFilter();
        }
        return state;
    }

    public void setState(ServiceTypeStatusFilter state) {
        this.state = state;
    }

    public BooleanFilter getIsCosRelated() {
        return isCosRelated;
    }

    public BooleanFilter isCosRelated() {
        if (isCosRelated == null) {
            isCosRelated = new BooleanFilter();
        }
        return isCosRelated;
    }

    public void setIsCosRelated(BooleanFilter isCosRelated) {
        this.isCosRelated = isCosRelated;
    }

    public CommissioningPlanTypeFilter getCommissioningPlanType() {
        return commissioningPlanType;
    }

    public CommissioningPlanTypeFilter commissioningPlanType() {
        if (commissioningPlanType == null) {
            commissioningPlanType = new CommissioningPlanTypeFilter();
        }
        return commissioningPlanType;
    }

    public void setCommissioningPlanType(CommissioningPlanTypeFilter commissioningPlanType) {
        this.commissioningPlanType = commissioningPlanType;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public StringFilter createdBy() {
        if (createdBy == null) {
            createdBy = new StringFilter();
        }
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public InstantFilter createdDate() {
        if (createdDate == null) {
            createdDate = new InstantFilter();
        }
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public StringFilter lastModifiedBy() {
        if (lastModifiedBy == null) {
            lastModifiedBy = new StringFilter();
        }
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public InstantFilter lastModifiedDate() {
        if (lastModifiedDate == null) {
            lastModifiedDate = new InstantFilter();
        }
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public LongFilter getCommissionPlanId() {
        return commissionPlanId;
    }

    public LongFilter commissionPlanId() {
        if (commissionPlanId == null) {
            commissionPlanId = new LongFilter();
        }
        return commissionPlanId;
    }

    public void setCommissionPlanId(LongFilter commissionPlanId) {
        this.commissionPlanId = commissionPlanId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final ServiceTypeCriteria that = (ServiceTypeCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(code, that.code) &&
            Objects.equals(description, that.description) &&
            Objects.equals(state, that.state) &&
            Objects.equals(isCosRelated, that.isCosRelated) &&
            Objects.equals(commissioningPlanType, that.commissioningPlanType) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(commissionPlanId, that.commissionPlanId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            name,
            code,
            description,
            state,
            isCosRelated,
            commissioningPlanType,
            createdBy,
            createdDate,
            lastModifiedBy,
            lastModifiedDate,
            commissionPlanId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ServiceTypeCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (code != null ? "code=" + code + ", " : "") +
            (description != null ? "description=" + description + ", " : "") +
            (state != null ? "state=" + state + ", " : "") +
            (isCosRelated != null ? "isCosRelated=" + isCosRelated + ", " : "") +
            (commissioningPlanType != null ? "commissioningPlanType=" + commissioningPlanType + ", " : "") +
            (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
            (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
            (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
            (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
            (commissionPlanId != null ? "commissionPlanId=" + commissionPlanId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
