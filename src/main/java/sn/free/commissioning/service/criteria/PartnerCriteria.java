package sn.free.commissioning.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link sn.free.commissioning.domain.Partner} entity. This class is used
 * in {@link sn.free.commissioning.web.rest.PartnerResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /partners?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
public class PartnerCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter msisdn;

    private StringFilter name;

    private StringFilter surname;

    private StringFilter state;

    private LongFilter partnerProfileId;

    private LongFilter parentId;

    private BooleanFilter canResetPin;

    private StringFilter createdBy;

    private InstantFilter createdDate;

    private StringFilter lastModifiedBy;

    private InstantFilter lastModifiedDate;

    private InstantFilter lastTransactionDate;

    private LongFilter blacklistedId;

    private LongFilter childrenId;

    private LongFilter zoneId;

    private LongFilter partnerId;

    private Boolean distinct;

    public PartnerCriteria() {}

    public PartnerCriteria(PartnerCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.msisdn = other.msisdn == null ? null : other.msisdn.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.surname = other.surname == null ? null : other.surname.copy();
        this.state = other.state == null ? null : other.state.copy();
        this.partnerProfileId = other.partnerProfileId == null ? null : other.partnerProfileId.copy();
        this.parentId = other.parentId == null ? null : other.parentId.copy();
        this.canResetPin = other.canResetPin == null ? null : other.canResetPin.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.lastTransactionDate = other.lastTransactionDate == null ? null : other.lastTransactionDate.copy();
        this.blacklistedId = other.blacklistedId == null ? null : other.blacklistedId.copy();
        this.childrenId = other.childrenId == null ? null : other.childrenId.copy();
        this.zoneId = other.zoneId == null ? null : other.zoneId.copy();
        this.partnerId = other.partnerId == null ? null : other.partnerId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public PartnerCriteria copy() {
        return new PartnerCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getMsisdn() {
        return msisdn;
    }

    public StringFilter msisdn() {
        if (msisdn == null) {
            msisdn = new StringFilter();
        }
        return msisdn;
    }

    public void setMsisdn(StringFilter msisdn) {
        this.msisdn = msisdn;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getSurname() {
        return surname;
    }

    public StringFilter surname() {
        if (surname == null) {
            surname = new StringFilter();
        }
        return surname;
    }

    public void setSurname(StringFilter surname) {
        this.surname = surname;
    }

    public StringFilter getState() {
        return state;
    }

    public StringFilter state() {
        if (state == null) {
            state = new StringFilter();
        }
        return state;
    }

    public void setState(StringFilter state) {
        this.state = state;
    }

    public LongFilter getPartnerProfileId() {
        return partnerProfileId;
    }

    public LongFilter partnerProfileId() {
        if (partnerProfileId == null) {
            partnerProfileId = new LongFilter();
        }
        return partnerProfileId;
    }

    public void setPartnerProfileId(LongFilter partnerProfileId) {
        this.partnerProfileId = partnerProfileId;
    }

    public LongFilter getParentId() {
        return parentId;
    }

    public LongFilter parentId() {
        if (parentId == null) {
            parentId = new LongFilter();
        }
        return parentId;
    }

    public void setParentId(LongFilter parentId) {
        this.parentId = parentId;
    }

    public BooleanFilter getCanResetPin() {
        return canResetPin;
    }

    public BooleanFilter canResetPin() {
        if (canResetPin == null) {
            canResetPin = new BooleanFilter();
        }
        return canResetPin;
    }

    public void setCanResetPin(BooleanFilter canResetPin) {
        this.canResetPin = canResetPin;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public StringFilter createdBy() {
        if (createdBy == null) {
            createdBy = new StringFilter();
        }
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public InstantFilter createdDate() {
        if (createdDate == null) {
            createdDate = new InstantFilter();
        }
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public StringFilter lastModifiedBy() {
        if (lastModifiedBy == null) {
            lastModifiedBy = new StringFilter();
        }
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public InstantFilter lastModifiedDate() {
        if (lastModifiedDate == null) {
            lastModifiedDate = new InstantFilter();
        }
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public InstantFilter getLastTransactionDate() {
        return lastTransactionDate;
    }

    public InstantFilter lastTransactionDate() {
        if (lastTransactionDate == null) {
            lastTransactionDate = new InstantFilter();
        }
        return lastTransactionDate;
    }

    public void setLastTransactionDate(InstantFilter lastTransactionDate) {
        this.lastTransactionDate = lastTransactionDate;
    }

    public LongFilter getBlacklistedId() {
        return blacklistedId;
    }

    public LongFilter blacklistedId() {
        if (blacklistedId == null) {
            blacklistedId = new LongFilter();
        }
        return blacklistedId;
    }

    public void setBlacklistedId(LongFilter blacklistedId) {
        this.blacklistedId = blacklistedId;
    }

    public LongFilter getChildrenId() {
        return childrenId;
    }

    public LongFilter childrenId() {
        if (childrenId == null) {
            childrenId = new LongFilter();
        }
        return childrenId;
    }

    public void setChildrenId(LongFilter childrenId) {
        this.childrenId = childrenId;
    }

    public LongFilter getZoneId() {
        return zoneId;
    }

    public LongFilter zoneId() {
        if (zoneId == null) {
            zoneId = new LongFilter();
        }
        return zoneId;
    }

    public void setZoneId(LongFilter zoneId) {
        this.zoneId = zoneId;
    }

    public LongFilter getPartnerId() {
        return partnerId;
    }

    public LongFilter partnerId() {
        if (partnerId == null) {
            partnerId = new LongFilter();
        }
        return partnerId;
    }

    public void setPartnerId(LongFilter partnerId) {
        this.partnerId = partnerId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PartnerCriteria that = (PartnerCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(msisdn, that.msisdn) &&
            Objects.equals(name, that.name) &&
            Objects.equals(surname, that.surname) &&
            Objects.equals(state, that.state) &&
            Objects.equals(partnerProfileId, that.partnerProfileId) &&
            Objects.equals(parentId, that.parentId) &&
            Objects.equals(canResetPin, that.canResetPin) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(lastTransactionDate, that.lastTransactionDate) &&
            Objects.equals(blacklistedId, that.blacklistedId) &&
            Objects.equals(childrenId, that.childrenId) &&
            Objects.equals(zoneId, that.zoneId) &&
            Objects.equals(partnerId, that.partnerId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            msisdn,
            name,
            surname,
            state,
            partnerProfileId,
            parentId,
            canResetPin,
            createdBy,
            createdDate,
            lastModifiedBy,
            lastModifiedDate,
            lastTransactionDate,
            blacklistedId,
            childrenId,
            zoneId,
            partnerId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PartnerCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (msisdn != null ? "msisdn=" + msisdn + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (surname != null ? "surname=" + surname + ", " : "") +
            (state != null ? "state=" + state + ", " : "") +
            (partnerProfileId != null ? "partnerProfileId=" + partnerProfileId + ", " : "") +
            (parentId != null ? "parentId=" + parentId + ", " : "") +
            (canResetPin != null ? "canResetPin=" + canResetPin + ", " : "") +
            (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
            (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
            (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
            (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
            (lastTransactionDate != null ? "lastTransactionDate=" + lastTransactionDate + ", " : "") +
            (blacklistedId != null ? "blacklistedId=" + blacklistedId + ", " : "") +
            (childrenId != null ? "childrenId=" + childrenId + ", " : "") +
            (zoneId != null ? "zoneId=" + zoneId + ", " : "") +
            (partnerId != null ? "partnerId=" + partnerId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
