package sn.free.commissioning.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link sn.free.commissioning.domain.CommissionAccount} entity. This class is used
 * in {@link sn.free.commissioning.web.rest.CommissionAccountResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /commission-accounts?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
public class CommissionAccountCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private DoubleFilter calculatedBalance;

    private DoubleFilter paidBalance;

    private StringFilter spare1;

    private StringFilter spare2;

    private StringFilter spare3;

    private LongFilter partnerId;

    private LongFilter commissionId;

    private Boolean distinct;

    public CommissionAccountCriteria() {}

    public CommissionAccountCriteria(CommissionAccountCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.calculatedBalance = other.calculatedBalance == null ? null : other.calculatedBalance.copy();
        this.paidBalance = other.paidBalance == null ? null : other.paidBalance.copy();
        this.spare1 = other.spare1 == null ? null : other.spare1.copy();
        this.spare2 = other.spare2 == null ? null : other.spare2.copy();
        this.spare3 = other.spare3 == null ? null : other.spare3.copy();
        this.partnerId = other.partnerId == null ? null : other.partnerId.copy();
        this.commissionId = other.commissionId == null ? null : other.commissionId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public CommissionAccountCriteria copy() {
        return new CommissionAccountCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public DoubleFilter getCalculatedBalance() {
        return calculatedBalance;
    }

    public DoubleFilter calculatedBalance() {
        if (calculatedBalance == null) {
            calculatedBalance = new DoubleFilter();
        }
        return calculatedBalance;
    }

    public void setCalculatedBalance(DoubleFilter calculatedBalance) {
        this.calculatedBalance = calculatedBalance;
    }

    public DoubleFilter getPaidBalance() {
        return paidBalance;
    }

    public DoubleFilter paidBalance() {
        if (paidBalance == null) {
            paidBalance = new DoubleFilter();
        }
        return paidBalance;
    }

    public void setPaidBalance(DoubleFilter paidBalance) {
        this.paidBalance = paidBalance;
    }

    public StringFilter getSpare1() {
        return spare1;
    }

    public StringFilter spare1() {
        if (spare1 == null) {
            spare1 = new StringFilter();
        }
        return spare1;
    }

    public void setSpare1(StringFilter spare1) {
        this.spare1 = spare1;
    }

    public StringFilter getSpare2() {
        return spare2;
    }

    public StringFilter spare2() {
        if (spare2 == null) {
            spare2 = new StringFilter();
        }
        return spare2;
    }

    public void setSpare2(StringFilter spare2) {
        this.spare2 = spare2;
    }

    public StringFilter getSpare3() {
        return spare3;
    }

    public StringFilter spare3() {
        if (spare3 == null) {
            spare3 = new StringFilter();
        }
        return spare3;
    }

    public void setSpare3(StringFilter spare3) {
        this.spare3 = spare3;
    }

    public LongFilter getPartnerId() {
        return partnerId;
    }

    public LongFilter partnerId() {
        if (partnerId == null) {
            partnerId = new LongFilter();
        }
        return partnerId;
    }

    public void setPartnerId(LongFilter partnerId) {
        this.partnerId = partnerId;
    }

    public LongFilter getCommissionId() {
        return commissionId;
    }

    public LongFilter commissionId() {
        if (commissionId == null) {
            commissionId = new LongFilter();
        }
        return commissionId;
    }

    public void setCommissionId(LongFilter commissionId) {
        this.commissionId = commissionId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CommissionAccountCriteria that = (CommissionAccountCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(calculatedBalance, that.calculatedBalance) &&
            Objects.equals(paidBalance, that.paidBalance) &&
            Objects.equals(spare1, that.spare1) &&
            Objects.equals(spare2, that.spare2) &&
            Objects.equals(spare3, that.spare3) &&
            Objects.equals(partnerId, that.partnerId) &&
            Objects.equals(commissionId, that.commissionId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, calculatedBalance, paidBalance, spare1, spare2, spare3, partnerId, commissionId, distinct);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommissionAccountCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (calculatedBalance != null ? "calculatedBalance=" + calculatedBalance + ", " : "") +
            (paidBalance != null ? "paidBalance=" + paidBalance + ", " : "") +
            (spare1 != null ? "spare1=" + spare1 + ", " : "") +
            (spare2 != null ? "spare2=" + spare2 + ", " : "") +
            (spare3 != null ? "spare3=" + spare3 + ", " : "") +
            (partnerId != null ? "partnerId=" + partnerId + ", " : "") +
            (commissionId != null ? "commissionId=" + commissionId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
