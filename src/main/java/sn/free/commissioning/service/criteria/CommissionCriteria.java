package sn.free.commissioning.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import sn.free.commissioning.domain.enumeration.CommissionPaymentStatus;
import sn.free.commissioning.domain.enumeration.CommissioningPlanType;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LocalDateFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link sn.free.commissioning.domain.Commission} entity. This class is used
 * in {@link sn.free.commissioning.web.rest.CommissionResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /commissions?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
public class CommissionCriteria implements Serializable, Criteria {

    /**
     * Class for filtering CommissionPaymentStatus
     */
    public static class CommissionPaymentStatusFilter extends Filter<CommissionPaymentStatus> {

        public CommissionPaymentStatusFilter() {}

        public CommissionPaymentStatusFilter(CommissionPaymentStatusFilter filter) {
            super(filter);
        }

        @Override
        public CommissionPaymentStatusFilter copy() {
            return new CommissionPaymentStatusFilter(this);
        }
    }

    /**
     * Class for filtering CommissioningPlanType
     */
    public static class CommissioningPlanTypeFilter extends Filter<CommissioningPlanType> {

        public CommissioningPlanTypeFilter() {}

        public CommissioningPlanTypeFilter(CommissioningPlanTypeFilter filter) {
            super(filter);
        }

        @Override
        public CommissioningPlanTypeFilter copy() {
            return new CommissioningPlanTypeFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private DoubleFilter amount;

    private InstantFilter calculatedAt;

    private InstantFilter calculationDate;

    private LocalDateFilter calculationShortDate;

    private StringFilter senderMsisdn;

    private StringFilter senderProfile;

    private StringFilter serviceType;

    private CommissionPaymentStatusFilter commissionPaymentStatus;

    private CommissioningPlanTypeFilter commissioningPlanType;

    private DoubleFilter globalNetworkCommissionAmount;

    private DoubleFilter transactionsAmount;

    private DoubleFilter fraudAmount;

    private StringFilter spare1;

    private StringFilter spare2;

    private StringFilter spare3;

    private LongFilter commissionAccountId;

    private LongFilter transactionOperationId;

    private LongFilter configurationPlanId;

    private LongFilter paymentId;

    private Boolean distinct;

    public CommissionCriteria() {}

    public CommissionCriteria(CommissionCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.amount = other.amount == null ? null : other.amount.copy();
        this.calculatedAt = other.calculatedAt == null ? null : other.calculatedAt.copy();
        this.calculationDate = other.calculationDate == null ? null : other.calculationDate.copy();
        this.calculationShortDate = other.calculationShortDate == null ? null : other.calculationShortDate.copy();
        this.senderMsisdn = other.senderMsisdn == null ? null : other.senderMsisdn.copy();
        this.senderProfile = other.senderProfile == null ? null : other.senderProfile.copy();
        this.serviceType = other.serviceType == null ? null : other.serviceType.copy();
        this.commissionPaymentStatus = other.commissionPaymentStatus == null ? null : other.commissionPaymentStatus.copy();
        this.commissioningPlanType = other.commissioningPlanType == null ? null : other.commissioningPlanType.copy();
        this.globalNetworkCommissionAmount =
            other.globalNetworkCommissionAmount == null ? null : other.globalNetworkCommissionAmount.copy();
        this.transactionsAmount = other.transactionsAmount == null ? null : other.transactionsAmount.copy();
        this.fraudAmount = other.fraudAmount == null ? null : other.fraudAmount.copy();
        this.spare1 = other.spare1 == null ? null : other.spare1.copy();
        this.spare2 = other.spare2 == null ? null : other.spare2.copy();
        this.spare3 = other.spare3 == null ? null : other.spare3.copy();
        this.commissionAccountId = other.commissionAccountId == null ? null : other.commissionAccountId.copy();
        this.transactionOperationId = other.transactionOperationId == null ? null : other.transactionOperationId.copy();
        this.configurationPlanId = other.configurationPlanId == null ? null : other.configurationPlanId.copy();
        this.paymentId = other.paymentId == null ? null : other.paymentId.copy();
        this.distinct = other.distinct;
    }

    @Override
    public CommissionCriteria copy() {
        return new CommissionCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public DoubleFilter getAmount() {
        return amount;
    }

    public DoubleFilter amount() {
        if (amount == null) {
            amount = new DoubleFilter();
        }
        return amount;
    }

    public void setAmount(DoubleFilter amount) {
        this.amount = amount;
    }

    public InstantFilter getCalculatedAt() {
        return calculatedAt;
    }

    public InstantFilter calculatedAt() {
        if (calculatedAt == null) {
            calculatedAt = new InstantFilter();
        }
        return calculatedAt;
    }

    public void setCalculatedAt(InstantFilter calculatedAt) {
        this.calculatedAt = calculatedAt;
    }

    public InstantFilter getCalculationDate() {
        return calculationDate;
    }

    public InstantFilter calculationDate() {
        if (calculationDate == null) {
            calculationDate = new InstantFilter();
        }
        return calculationDate;
    }

    public void setCalculationDate(InstantFilter calculationDate) {
        this.calculationDate = calculationDate;
    }

    public LocalDateFilter getCalculationShortDate() {
        return calculationShortDate;
    }

    public LocalDateFilter calculationShortDate() {
        if (calculationShortDate == null) {
            calculationShortDate = new LocalDateFilter();
        }
        return calculationShortDate;
    }

    public void setCalculationShortDate(LocalDateFilter calculationShortDate) {
        this.calculationShortDate = calculationShortDate;
    }

    public StringFilter getSenderMsisdn() {
        return senderMsisdn;
    }

    public StringFilter senderMsisdn() {
        if (senderMsisdn == null) {
            senderMsisdn = new StringFilter();
        }
        return senderMsisdn;
    }

    public void setSenderMsisdn(StringFilter senderMsisdn) {
        this.senderMsisdn = senderMsisdn;
    }

    public StringFilter getSenderProfile() {
        return senderProfile;
    }

    public StringFilter senderProfile() {
        if (senderProfile == null) {
            senderProfile = new StringFilter();
        }
        return senderProfile;
    }

    public void setSenderProfile(StringFilter senderProfile) {
        this.senderProfile = senderProfile;
    }

    public StringFilter getServiceType() {
        return serviceType;
    }

    public StringFilter serviceType() {
        if (serviceType == null) {
            serviceType = new StringFilter();
        }
        return serviceType;
    }

    public void setServiceType(StringFilter serviceType) {
        this.serviceType = serviceType;
    }

    public CommissionPaymentStatusFilter getCommissionPaymentStatus() {
        return commissionPaymentStatus;
    }

    public CommissionPaymentStatusFilter commissionPaymentStatus() {
        if (commissionPaymentStatus == null) {
            commissionPaymentStatus = new CommissionPaymentStatusFilter();
        }
        return commissionPaymentStatus;
    }

    public void setCommissionPaymentStatus(CommissionPaymentStatusFilter commissionPaymentStatus) {
        this.commissionPaymentStatus = commissionPaymentStatus;
    }

    public CommissioningPlanTypeFilter getCommissioningPlanType() {
        return commissioningPlanType;
    }

    public CommissioningPlanTypeFilter commissioningPlanType() {
        if (commissioningPlanType == null) {
            commissioningPlanType = new CommissioningPlanTypeFilter();
        }
        return commissioningPlanType;
    }

    public void setCommissioningPlanType(CommissioningPlanTypeFilter commissioningPlanType) {
        this.commissioningPlanType = commissioningPlanType;
    }

    public DoubleFilter getGlobalNetworkCommissionAmount() {
        return globalNetworkCommissionAmount;
    }

    public DoubleFilter globalNetworkCommissionAmount() {
        if (globalNetworkCommissionAmount == null) {
            globalNetworkCommissionAmount = new DoubleFilter();
        }
        return globalNetworkCommissionAmount;
    }

    public void setGlobalNetworkCommissionAmount(DoubleFilter globalNetworkCommissionAmount) {
        this.globalNetworkCommissionAmount = globalNetworkCommissionAmount;
    }

    public DoubleFilter getTransactionsAmount() {
        return transactionsAmount;
    }

    public DoubleFilter transactionsAmount() {
        if (transactionsAmount == null) {
            transactionsAmount = new DoubleFilter();
        }
        return transactionsAmount;
    }

    public void setTransactionsAmount(DoubleFilter transactionsAmount) {
        this.transactionsAmount = transactionsAmount;
    }

    public DoubleFilter getFraudAmount() {
        return fraudAmount;
    }

    public DoubleFilter fraudAmount() {
        if (fraudAmount == null) {
            fraudAmount = new DoubleFilter();
        }
        return fraudAmount;
    }

    public void setFraudAmount(DoubleFilter fraudAmount) {
        this.fraudAmount = fraudAmount;
    }

    public StringFilter getSpare1() {
        return spare1;
    }

    public StringFilter spare1() {
        if (spare1 == null) {
            spare1 = new StringFilter();
        }
        return spare1;
    }

    public void setSpare1(StringFilter spare1) {
        this.spare1 = spare1;
    }

    public StringFilter getSpare2() {
        return spare2;
    }

    public StringFilter spare2() {
        if (spare2 == null) {
            spare2 = new StringFilter();
        }
        return spare2;
    }

    public void setSpare2(StringFilter spare2) {
        this.spare2 = spare2;
    }

    public StringFilter getSpare3() {
        return spare3;
    }

    public StringFilter spare3() {
        if (spare3 == null) {
            spare3 = new StringFilter();
        }
        return spare3;
    }

    public void setSpare3(StringFilter spare3) {
        this.spare3 = spare3;
    }

    public LongFilter getCommissionAccountId() {
        return commissionAccountId;
    }

    public LongFilter commissionAccountId() {
        if (commissionAccountId == null) {
            commissionAccountId = new LongFilter();
        }
        return commissionAccountId;
    }

    public void setCommissionAccountId(LongFilter commissionAccountId) {
        this.commissionAccountId = commissionAccountId;
    }

    public LongFilter getTransactionOperationId() {
        return transactionOperationId;
    }

    public LongFilter transactionOperationId() {
        if (transactionOperationId == null) {
            transactionOperationId = new LongFilter();
        }
        return transactionOperationId;
    }

    public void setTransactionOperationId(LongFilter transactionOperationId) {
        this.transactionOperationId = transactionOperationId;
    }

    public LongFilter getConfigurationPlanId() {
        return configurationPlanId;
    }

    public LongFilter configurationPlanId() {
        if (configurationPlanId == null) {
            configurationPlanId = new LongFilter();
        }
        return configurationPlanId;
    }

    public void setConfigurationPlanId(LongFilter configurationPlanId) {
        this.configurationPlanId = configurationPlanId;
    }

    public LongFilter getPaymentId() {
        return paymentId;
    }

    public LongFilter paymentId() {
        if (paymentId == null) {
            paymentId = new LongFilter();
        }
        return paymentId;
    }

    public void setPaymentId(LongFilter paymentId) {
        this.paymentId = paymentId;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CommissionCriteria that = (CommissionCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(amount, that.amount) &&
            Objects.equals(calculatedAt, that.calculatedAt) &&
            Objects.equals(calculationDate, that.calculationDate) &&
            Objects.equals(calculationShortDate, that.calculationShortDate) &&
            Objects.equals(senderMsisdn, that.senderMsisdn) &&
            Objects.equals(senderProfile, that.senderProfile) &&
            Objects.equals(serviceType, that.serviceType) &&
            Objects.equals(commissionPaymentStatus, that.commissionPaymentStatus) &&
            Objects.equals(commissioningPlanType, that.commissioningPlanType) &&
            Objects.equals(globalNetworkCommissionAmount, that.globalNetworkCommissionAmount) &&
            Objects.equals(transactionsAmount, that.transactionsAmount) &&
            Objects.equals(fraudAmount, that.fraudAmount) &&
            Objects.equals(spare1, that.spare1) &&
            Objects.equals(spare2, that.spare2) &&
            Objects.equals(spare3, that.spare3) &&
            Objects.equals(commissionAccountId, that.commissionAccountId) &&
            Objects.equals(transactionOperationId, that.transactionOperationId) &&
            Objects.equals(configurationPlanId, that.configurationPlanId) &&
            Objects.equals(paymentId, that.paymentId) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            amount,
            calculatedAt,
            calculationDate,
            calculationShortDate,
            senderMsisdn,
            senderProfile,
            serviceType,
            commissionPaymentStatus,
            commissioningPlanType,
            globalNetworkCommissionAmount,
            transactionsAmount,
            fraudAmount,
            spare1,
            spare2,
            spare3,
            commissionAccountId,
            transactionOperationId,
            configurationPlanId,
            paymentId,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CommissionCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (amount != null ? "amount=" + amount + ", " : "") +
            (calculatedAt != null ? "calculatedAt=" + calculatedAt + ", " : "") +
            (calculationDate != null ? "calculationDate=" + calculationDate + ", " : "") +
            (calculationShortDate != null ? "calculationShortDate=" + calculationShortDate + ", " : "") +
            (senderMsisdn != null ? "senderMsisdn=" + senderMsisdn + ", " : "") +
            (senderProfile != null ? "senderProfile=" + senderProfile + ", " : "") +
            (serviceType != null ? "serviceType=" + serviceType + ", " : "") +
            (commissionPaymentStatus != null ? "commissionPaymentStatus=" + commissionPaymentStatus + ", " : "") +
            (commissioningPlanType != null ? "commissioningPlanType=" + commissioningPlanType + ", " : "") +
            (globalNetworkCommissionAmount != null ? "globalNetworkCommissionAmount=" + globalNetworkCommissionAmount + ", " : "") +
            (transactionsAmount != null ? "transactionsAmount=" + transactionsAmount + ", " : "") +
            (fraudAmount != null ? "fraudAmount=" + fraudAmount + ", " : "") +
            (spare1 != null ? "spare1=" + spare1 + ", " : "") +
            (spare2 != null ? "spare2=" + spare2 + ", " : "") +
            (spare3 != null ? "spare3=" + spare3 + ", " : "") +
            (commissionAccountId != null ? "commissionAccountId=" + commissionAccountId + ", " : "") +
            (transactionOperationId != null ? "transactionOperationId=" + transactionOperationId + ", " : "") +
            (configurationPlanId != null ? "configurationPlanId=" + configurationPlanId + ", " : "") +
            (paymentId != null ? "paymentId=" + paymentId + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
