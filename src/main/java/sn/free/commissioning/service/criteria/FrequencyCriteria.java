package sn.free.commissioning.service.criteria;

import java.io.Serializable;
import java.util.Objects;
import org.springdoc.api.annotations.ParameterObject;
import sn.free.commissioning.domain.enumeration.Cycle;
import sn.free.commissioning.domain.enumeration.FrequencyType;
import sn.free.commissioning.domain.enumeration.OperationType;
import sn.free.commissioning.domain.enumeration.PeriodOfOccurrence;
import tech.jhipster.service.Criteria;
import tech.jhipster.service.filter.BooleanFilter;
import tech.jhipster.service.filter.DoubleFilter;
import tech.jhipster.service.filter.Filter;
import tech.jhipster.service.filter.FloatFilter;
import tech.jhipster.service.filter.InstantFilter;
import tech.jhipster.service.filter.IntegerFilter;
import tech.jhipster.service.filter.LongFilter;
import tech.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link sn.free.commissioning.domain.Frequency} entity. This class is used
 * in {@link sn.free.commissioning.web.rest.FrequencyResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /frequencies?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
@ParameterObject
public class FrequencyCriteria implements Serializable, Criteria {

    /**
     * Class for filtering FrequencyType
     */
    public static class FrequencyTypeFilter extends Filter<FrequencyType> {

        public FrequencyTypeFilter() {}

        public FrequencyTypeFilter(FrequencyTypeFilter filter) {
            super(filter);
        }

        @Override
        public FrequencyTypeFilter copy() {
            return new FrequencyTypeFilter(this);
        }
    }

    /**
     * Class for filtering OperationType
     */
    public static class OperationTypeFilter extends Filter<OperationType> {

        public OperationTypeFilter() {}

        public OperationTypeFilter(OperationTypeFilter filter) {
            super(filter);
        }

        @Override
        public OperationTypeFilter copy() {
            return new OperationTypeFilter(this);
        }
    }

    /**
     * Class for filtering Cycle
     */
    public static class CycleFilter extends Filter<Cycle> {

        public CycleFilter() {}

        public CycleFilter(CycleFilter filter) {
            super(filter);
        }

        @Override
        public CycleFilter copy() {
            return new CycleFilter(this);
        }
    }

    /**
     * Class for filtering PeriodOfOccurrence
     */
    public static class PeriodOfOccurrenceFilter extends Filter<PeriodOfOccurrence> {

        public PeriodOfOccurrenceFilter() {}

        public PeriodOfOccurrenceFilter(PeriodOfOccurrenceFilter filter) {
            super(filter);
        }

        @Override
        public PeriodOfOccurrenceFilter copy() {
            return new PeriodOfOccurrenceFilter(this);
        }
    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter name;

    private FrequencyTypeFilter type;

    private OperationTypeFilter operationType;

    private IntegerFilter executionTime;

    private IntegerFilter executionWeekDay;

    private IntegerFilter executionMonthDay;

    private IntegerFilter daysAfter;

    private StringFilter createdBy;

    private InstantFilter createdDate;

    private StringFilter lastModifiedBy;

    private InstantFilter lastModifiedDate;

    private CycleFilter cycle;

    private PeriodOfOccurrenceFilter periodOfOccurrence;

    private IntegerFilter numberOfCycle;

    private IntegerFilter occurrenceByPeriod;

    private StringFilter datesOfOccurrence;

    private StringFilter spare1;

    private StringFilter spare2;

    private StringFilter spare3;

    private Boolean distinct;

    public FrequencyCriteria() {}

    public FrequencyCriteria(FrequencyCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.name = other.name == null ? null : other.name.copy();
        this.type = other.type == null ? null : other.type.copy();
        this.operationType = other.operationType == null ? null : other.operationType.copy();
        this.executionTime = other.executionTime == null ? null : other.executionTime.copy();
        this.executionWeekDay = other.executionWeekDay == null ? null : other.executionWeekDay.copy();
        this.executionMonthDay = other.executionMonthDay == null ? null : other.executionMonthDay.copy();
        this.daysAfter = other.daysAfter == null ? null : other.daysAfter.copy();
        this.createdBy = other.createdBy == null ? null : other.createdBy.copy();
        this.createdDate = other.createdDate == null ? null : other.createdDate.copy();
        this.lastModifiedBy = other.lastModifiedBy == null ? null : other.lastModifiedBy.copy();
        this.lastModifiedDate = other.lastModifiedDate == null ? null : other.lastModifiedDate.copy();
        this.cycle = other.cycle == null ? null : other.cycle.copy();
        this.periodOfOccurrence = other.periodOfOccurrence == null ? null : other.periodOfOccurrence.copy();
        this.numberOfCycle = other.numberOfCycle == null ? null : other.numberOfCycle.copy();
        this.occurrenceByPeriod = other.occurrenceByPeriod == null ? null : other.occurrenceByPeriod.copy();
        this.datesOfOccurrence = other.datesOfOccurrence == null ? null : other.datesOfOccurrence.copy();
        this.spare1 = other.spare1 == null ? null : other.spare1.copy();
        this.spare2 = other.spare2 == null ? null : other.spare2.copy();
        this.spare3 = other.spare3 == null ? null : other.spare3.copy();
        this.distinct = other.distinct;
    }

    @Override
    public FrequencyCriteria copy() {
        return new FrequencyCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public LongFilter id() {
        if (id == null) {
            id = new LongFilter();
        }
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public StringFilter name() {
        if (name == null) {
            name = new StringFilter();
        }
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public FrequencyTypeFilter getType() {
        return type;
    }

    public FrequencyTypeFilter type() {
        if (type == null) {
            type = new FrequencyTypeFilter();
        }
        return type;
    }

    public void setType(FrequencyTypeFilter type) {
        this.type = type;
    }

    public OperationTypeFilter getOperationType() {
        return operationType;
    }

    public OperationTypeFilter operationType() {
        if (operationType == null) {
            operationType = new OperationTypeFilter();
        }
        return operationType;
    }

    public void setOperationType(OperationTypeFilter operationType) {
        this.operationType = operationType;
    }

    public IntegerFilter getExecutionTime() {
        return executionTime;
    }

    public IntegerFilter executionTime() {
        if (executionTime == null) {
            executionTime = new IntegerFilter();
        }
        return executionTime;
    }

    public void setExecutionTime(IntegerFilter executionTime) {
        this.executionTime = executionTime;
    }

    public IntegerFilter getExecutionWeekDay() {
        return executionWeekDay;
    }

    public IntegerFilter executionWeekDay() {
        if (executionWeekDay == null) {
            executionWeekDay = new IntegerFilter();
        }
        return executionWeekDay;
    }

    public void setExecutionWeekDay(IntegerFilter executionWeekDay) {
        this.executionWeekDay = executionWeekDay;
    }

    public IntegerFilter getExecutionMonthDay() {
        return executionMonthDay;
    }

    public IntegerFilter executionMonthDay() {
        if (executionMonthDay == null) {
            executionMonthDay = new IntegerFilter();
        }
        return executionMonthDay;
    }

    public void setExecutionMonthDay(IntegerFilter executionMonthDay) {
        this.executionMonthDay = executionMonthDay;
    }

    public IntegerFilter getDaysAfter() {
        return daysAfter;
    }

    public IntegerFilter daysAfter() {
        if (daysAfter == null) {
            daysAfter = new IntegerFilter();
        }
        return daysAfter;
    }

    public void setDaysAfter(IntegerFilter daysAfter) {
        this.daysAfter = daysAfter;
    }

    public StringFilter getCreatedBy() {
        return createdBy;
    }

    public StringFilter createdBy() {
        if (createdBy == null) {
            createdBy = new StringFilter();
        }
        return createdBy;
    }

    public void setCreatedBy(StringFilter createdBy) {
        this.createdBy = createdBy;
    }

    public InstantFilter getCreatedDate() {
        return createdDate;
    }

    public InstantFilter createdDate() {
        if (createdDate == null) {
            createdDate = new InstantFilter();
        }
        return createdDate;
    }

    public void setCreatedDate(InstantFilter createdDate) {
        this.createdDate = createdDate;
    }

    public StringFilter getLastModifiedBy() {
        return lastModifiedBy;
    }

    public StringFilter lastModifiedBy() {
        if (lastModifiedBy == null) {
            lastModifiedBy = new StringFilter();
        }
        return lastModifiedBy;
    }

    public void setLastModifiedBy(StringFilter lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public InstantFilter getLastModifiedDate() {
        return lastModifiedDate;
    }

    public InstantFilter lastModifiedDate() {
        if (lastModifiedDate == null) {
            lastModifiedDate = new InstantFilter();
        }
        return lastModifiedDate;
    }

    public void setLastModifiedDate(InstantFilter lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public CycleFilter getCycle() {
        return cycle;
    }

    public CycleFilter cycle() {
        if (cycle == null) {
            cycle = new CycleFilter();
        }
        return cycle;
    }

    public void setCycle(CycleFilter cycle) {
        this.cycle = cycle;
    }

    public PeriodOfOccurrenceFilter getPeriodOfOccurrence() {
        return periodOfOccurrence;
    }

    public PeriodOfOccurrenceFilter periodOfOccurrence() {
        if (periodOfOccurrence == null) {
            periodOfOccurrence = new PeriodOfOccurrenceFilter();
        }
        return periodOfOccurrence;
    }

    public void setPeriodOfOccurrence(PeriodOfOccurrenceFilter periodOfOccurrence) {
        this.periodOfOccurrence = periodOfOccurrence;
    }

    public IntegerFilter getNumberOfCycle() {
        return numberOfCycle;
    }

    public IntegerFilter numberOfCycle() {
        if (numberOfCycle == null) {
            numberOfCycle = new IntegerFilter();
        }
        return numberOfCycle;
    }

    public void setNumberOfCycle(IntegerFilter numberOfCycle) {
        this.numberOfCycle = numberOfCycle;
    }

    public IntegerFilter getOccurrenceByPeriod() {
        return occurrenceByPeriod;
    }

    public IntegerFilter occurrenceByPeriod() {
        if (occurrenceByPeriod == null) {
            occurrenceByPeriod = new IntegerFilter();
        }
        return occurrenceByPeriod;
    }

    public void setOccurrenceByPeriod(IntegerFilter occurrenceByPeriod) {
        this.occurrenceByPeriod = occurrenceByPeriod;
    }

    public StringFilter getDatesOfOccurrence() {
        return datesOfOccurrence;
    }

    public StringFilter datesOfOccurrence() {
        if (datesOfOccurrence == null) {
            datesOfOccurrence = new StringFilter();
        }
        return datesOfOccurrence;
    }

    public void setDatesOfOccurrence(StringFilter datesOfOccurrence) {
        this.datesOfOccurrence = datesOfOccurrence;
    }

    public StringFilter getSpare1() {
        return spare1;
    }

    public StringFilter spare1() {
        if (spare1 == null) {
            spare1 = new StringFilter();
        }
        return spare1;
    }

    public void setSpare1(StringFilter spare1) {
        this.spare1 = spare1;
    }

    public StringFilter getSpare2() {
        return spare2;
    }

    public StringFilter spare2() {
        if (spare2 == null) {
            spare2 = new StringFilter();
        }
        return spare2;
    }

    public void setSpare2(StringFilter spare2) {
        this.spare2 = spare2;
    }

    public StringFilter getSpare3() {
        return spare3;
    }

    public StringFilter spare3() {
        if (spare3 == null) {
            spare3 = new StringFilter();
        }
        return spare3;
    }

    public void setSpare3(StringFilter spare3) {
        this.spare3 = spare3;
    }

    public Boolean getDistinct() {
        return distinct;
    }

    public void setDistinct(Boolean distinct) {
        this.distinct = distinct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final FrequencyCriteria that = (FrequencyCriteria) o;
        return (
            Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(type, that.type) &&
            Objects.equals(operationType, that.operationType) &&
            Objects.equals(executionTime, that.executionTime) &&
            Objects.equals(executionWeekDay, that.executionWeekDay) &&
            Objects.equals(executionMonthDay, that.executionMonthDay) &&
            Objects.equals(daysAfter, that.daysAfter) &&
            Objects.equals(createdBy, that.createdBy) &&
            Objects.equals(createdDate, that.createdDate) &&
            Objects.equals(lastModifiedBy, that.lastModifiedBy) &&
            Objects.equals(lastModifiedDate, that.lastModifiedDate) &&
            Objects.equals(cycle, that.cycle) &&
            Objects.equals(periodOfOccurrence, that.periodOfOccurrence) &&
            Objects.equals(numberOfCycle, that.numberOfCycle) &&
            Objects.equals(occurrenceByPeriod, that.occurrenceByPeriod) &&
            Objects.equals(datesOfOccurrence, that.datesOfOccurrence) &&
            Objects.equals(spare1, that.spare1) &&
            Objects.equals(spare2, that.spare2) &&
            Objects.equals(spare3, that.spare3) &&
            Objects.equals(distinct, that.distinct)
        );
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            id,
            name,
            type,
            operationType,
            executionTime,
            executionWeekDay,
            executionMonthDay,
            daysAfter,
            createdBy,
            createdDate,
            lastModifiedBy,
            lastModifiedDate,
            cycle,
            periodOfOccurrence,
            numberOfCycle,
            occurrenceByPeriod,
            datesOfOccurrence,
            spare1,
            spare2,
            spare3,
            distinct
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FrequencyCriteria{" +
            (id != null ? "id=" + id + ", " : "") +
            (name != null ? "name=" + name + ", " : "") +
            (type != null ? "type=" + type + ", " : "") +
            (operationType != null ? "operationType=" + operationType + ", " : "") +
            (executionTime != null ? "executionTime=" + executionTime + ", " : "") +
            (executionWeekDay != null ? "executionWeekDay=" + executionWeekDay + ", " : "") +
            (executionMonthDay != null ? "executionMonthDay=" + executionMonthDay + ", " : "") +
            (daysAfter != null ? "daysAfter=" + daysAfter + ", " : "") +
            (createdBy != null ? "createdBy=" + createdBy + ", " : "") +
            (createdDate != null ? "createdDate=" + createdDate + ", " : "") +
            (lastModifiedBy != null ? "lastModifiedBy=" + lastModifiedBy + ", " : "") +
            (lastModifiedDate != null ? "lastModifiedDate=" + lastModifiedDate + ", " : "") +
            (cycle != null ? "cycle=" + cycle + ", " : "") +
            (periodOfOccurrence != null ? "periodOfOccurrence=" + periodOfOccurrence + ", " : "") +
            (numberOfCycle != null ? "numberOfCycle=" + numberOfCycle + ", " : "") +
            (occurrenceByPeriod != null ? "occurrenceByPeriod=" + occurrenceByPeriod + ", " : "") +
            (datesOfOccurrence != null ? "datesOfOccurrence=" + datesOfOccurrence + ", " : "") +
            (spare1 != null ? "spare1=" + spare1 + ", " : "") +
            (spare2 != null ? "spare2=" + spare2 + ", " : "") +
            (spare3 != null ? "spare3=" + spare3 + ", " : "") +
            (distinct != null ? "distinct=" + distinct + ", " : "") +
            "}";
    }
}
