package sn.free.commissioning.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.*; // for static metamodels
import sn.free.commissioning.domain.PartnerProfile;
import sn.free.commissioning.repository.PartnerProfileRepository;
import sn.free.commissioning.service.criteria.PartnerProfileCriteria;
import sn.free.commissioning.service.dto.PartnerProfileDTO;
import sn.free.commissioning.service.mapper.PartnerProfileMapper;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link PartnerProfile} entities in the database.
 * The main input is a {@link PartnerProfileCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PartnerProfileDTO} or a {@link Page} of {@link PartnerProfileDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PartnerProfileQueryService extends QueryService<PartnerProfile> {

    private final Logger log = LoggerFactory.getLogger(PartnerProfileQueryService.class);

    private final PartnerProfileRepository partnerProfileRepository;

    private final PartnerProfileMapper partnerProfileMapper;

    public PartnerProfileQueryService(PartnerProfileRepository partnerProfileRepository, PartnerProfileMapper partnerProfileMapper) {
        this.partnerProfileRepository = partnerProfileRepository;
        this.partnerProfileMapper = partnerProfileMapper;
    }

    /**
     * Return a {@link List} of {@link PartnerProfileDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PartnerProfileDTO> findByCriteria(PartnerProfileCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<PartnerProfile> specification = createSpecification(criteria);
        return partnerProfileMapper.toDto(partnerProfileRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PartnerProfileDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PartnerProfileDTO> findByCriteria(PartnerProfileCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<PartnerProfile> specification = createSpecification(criteria);
        return partnerProfileRepository.findAll(specification, page).map(partnerProfileMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PartnerProfileCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<PartnerProfile> specification = createSpecification(criteria);
        return partnerProfileRepository.count(specification);
    }

    /**
     * Function to convert {@link PartnerProfileCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<PartnerProfile> createSpecification(PartnerProfileCriteria criteria) {
        Specification<PartnerProfile> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), PartnerProfile_.id));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), PartnerProfile_.code));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), PartnerProfile_.name));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), PartnerProfile_.description));
            }
            if (criteria.getLevel() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLevel(), PartnerProfile_.level));
            }
            if (criteria.getParentPartnerProfileId() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getParentPartnerProfileId(), PartnerProfile_.parentPartnerProfileId)
                    );
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), PartnerProfile_.createdBy));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), PartnerProfile_.createdDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), PartnerProfile_.lastModifiedBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), PartnerProfile_.lastModifiedDate));
            }
            if (criteria.getCanHaveMultipleZones() != null) {
                specification =
                    specification.and(buildSpecification(criteria.getCanHaveMultipleZones(), PartnerProfile_.canHaveMultipleZones));
            }
            if (criteria.getZoneThreshold() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getZoneThreshold(), PartnerProfile_.zoneThreshold));
            }
            if (criteria.getSimBackupThreshold() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getSimBackupThreshold(), PartnerProfile_.simBackupThreshold));
            }
            if (criteria.getCanDoProfileAssignment() != null) {
                specification =
                    specification.and(buildSpecification(criteria.getCanDoProfileAssignment(), PartnerProfile_.canDoProfileAssignment));
            }
        }
        return specification;
    }
}
