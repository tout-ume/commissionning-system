package sn.free.commissioning.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.commissioning.domain.ConfigurationPlan;
import sn.free.commissioning.service.dto.ConfigurationPlanDTO;

/**
 * Service Interface for managing {@link sn.free.commissioning.domain.ConfigurationPlan}.
 */
public interface ConfigurationPlanService {
    /**
     * Save a configurationPlan.
     *
     * @param configurationPlanDTO the entity to save.
     * @return the persisted entity.
     */
    ConfigurationPlanDTO save(ConfigurationPlanDTO configurationPlanDTO);

    /**
     * Save a list of configurationPlan.
     *
     * @param configurationPlans the entity to save.
     * @return the list persisted entity.
     */
    List<ConfigurationPlan> saveAll(Set<ConfigurationPlan> configurationPlans);

    /**
     * Updates a configurationPlan.
     *
     * @param configurationPlanDTO the entity to update.
     * @return the persisted entity.
     */
    ConfigurationPlanDTO update(ConfigurationPlanDTO configurationPlanDTO);

    /**
     * Partially updates a configurationPlan.
     *
     * @param configurationPlanDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<ConfigurationPlanDTO> partialUpdate(ConfigurationPlanDTO configurationPlanDTO);

    /**
     * Get all the configurationPlans.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ConfigurationPlanDTO> findAll(Pageable pageable);

    /**
     * Get all the configurationPlans with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ConfigurationPlanDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" configurationPlan.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ConfigurationPlanDTO> findOne(Long id);

    /**
     * Delete the "id" configurationPlan.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Get configurationPlans by commissioningPlanId.
     *
     * @param id the commissioningPlanId.
     * @return the list of entities.
     */
    List<ConfigurationPlan> findByCommissioningPlan(Long id);
    /* List<ConfigurationPlan> findByPartnerProfile(Long id);

    double getTaux();*/

    /**
     * Get configurationPlans by code partnerprofile.
     *
     * @param code the code of partnerprofile.
     * @return the list of entities.
     */
    List<ConfigurationPlanDTO> findByPartnerProfile_Code(String code);
}
