package sn.free.commissioning.service;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.commissioning.domain.Commission;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.domain.enumeration.CommissionPaymentStatus;
import sn.free.commissioning.service.dto.CommissionBulkPaymentDTO;
import sn.free.commissioning.service.dto.CommissionDTO;

/**
 * Service Interface for managing {@link sn.free.commissioning.domain.Commission}.
 */
public interface CommissionService {
    /**
     * Save a commission.
     *
     * @param commissionDTO the entity to save.
     * @return the persisted entity.
     */
    CommissionDTO save(CommissionDTO commissionDTO);

    /**
     * Save a commission.
     *
     * @param commission the entity to save.
     * @return the persisted entity.
     */
    Commission save(Commission commission);

    /**
     * Updates a commission.
     *
     * @param commissionDTO the entity to update.
     * @return the persisted entity.
     */
    CommissionDTO update(CommissionDTO commissionDTO);

    /**
     * Partially updates a commission.
     *
     * @param commissionDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<CommissionDTO> partialUpdate(CommissionDTO commissionDTO);

    /**
     * Get all the commissions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CommissionDTO> findAll(Pageable pageable);

    /**
     * Get all the commissions with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CommissionDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" commission.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CommissionDTO> findOne(Long id);

    /**
     * Delete the "id" commission.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * @param msisdn
     * @return
     */
    List<CommissionDTO> findBySenderMsidn(String msisdn);

    /**
     * Get commissions to be paid for a partner.
     *
     * @param msisdn the partner's msisdn.
     * @return the list of commissions.
     */
    List<CommissionDTO> findToBePaidBySenderMsisdn(String msisdn);

    /**
     * Get top profile commissions for a plan.
     *
     * @param planId the plan's id.
     * @return the list of commissions.
     */
    List<CommissionDTO> findTopProfileCommissionsByPlan(Long planId);

    /**
     * Get all profiles commissions for a plan.
     *
     * @param planId the plan's id.
     * @param pageable the pagination information.
     * @return the list of commissions.
     */
    Page<CommissionBulkPaymentDTO> findAllProfilesCommissionsByPlan(Long planId, Pageable pageable);

    /**
     * Get commissions to be paid to partner by again free.
     *
     * @param msisdn the partner's msisdn.
     * @return the list of commissions.
     */
    List<CommissionDTO> findToBePaidToPartnerBySenderMsisdn(String msisdn);

    /**
     * Get commissions by list of ids.
     *
     * @param ids commission ids.
     * @return the list of commissions.
     */
    List<CommissionDTO> findByIdIn(List<Long> ids);

    /**
     * Change top profile's network commissions status after top profile (DP) payment.
     *
     * @param commission the top profile commission.
     * @param status the status to define.
     */
    void changeTopProfileNetworkCommissionsStatus(Commission commission, CommissionPaymentStatus status);
}
