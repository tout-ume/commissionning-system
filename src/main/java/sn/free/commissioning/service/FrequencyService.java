package sn.free.commissioning.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.commissioning.domain.enumeration.FrequencyType;
import sn.free.commissioning.domain.enumeration.OperationType;
import sn.free.commissioning.service.dto.FrequencyDTO;

/**
 * Service Interface for managing {@link sn.free.commissioning.domain.Frequency}.
 */
public interface FrequencyService {
    /**
     * Save a frequency.
     *
     * @param frequencyDTO the entity to save.
     * @return the persisted entity.
     */
    FrequencyDTO save(FrequencyDTO frequencyDTO);

    /**
     * Updates a frequency.
     *
     * @param frequencyDTO the entity to update.
     * @return the persisted entity.
     */
    FrequencyDTO update(FrequencyDTO frequencyDTO);

    /**
     * Partially updates a frequency.
     *
     * @param frequencyDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<FrequencyDTO> partialUpdate(FrequencyDTO frequencyDTO);

    /**
     * Get all the frequencies.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<FrequencyDTO> findAll(Pageable pageable);

    /**
     * Get the "id" frequency.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<FrequencyDTO> findOne(Long id);

    /**
     * Get the "name" frequency.
     *
     * @param name the name field of the entity.
     * @return the entity.
     */
    Optional<FrequencyDTO> findByName(String name);

    /**
     * Get the "name" and "operationType frequency.
     *
     * @param name the name field of the entity.
     * @param operationType the name field of the entity.
     * @return the entity.
     */
    Optional<FrequencyDTO> findByNameAndOperation(String name, OperationType operationType);

    /**
     * Delete the "id" frequency.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
