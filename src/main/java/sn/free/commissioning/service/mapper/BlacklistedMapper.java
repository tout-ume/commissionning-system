package sn.free.commissioning.service.mapper;

import org.mapstruct.*;
import sn.free.commissioning.domain.Blacklisted;
import sn.free.commissioning.domain.CommissioningPlan;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.service.dto.BlacklistedDTO;
import sn.free.commissioning.service.dto.CommissioningPlanDTO;
import sn.free.commissioning.service.dto.PartnerDTO;

/**
 * Mapper for the entity {@link Blacklisted} and its DTO {@link BlacklistedDTO}.
 */
@Mapper(componentModel = "spring")
public interface BlacklistedMapper extends EntityMapper<BlacklistedDTO, Blacklisted> {
    @Mapping(target = "commissioningPlan", source = "commissioningPlan", qualifiedByName = "commissioningPlanId")
    @Mapping(target = "partner", source = "partner", qualifiedByName = "partnerId")
    BlacklistedDTO toDto(Blacklisted s);

    @Named("commissioningPlanId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CommissioningPlanDTO toDtoCommissioningPlanId(CommissioningPlan commissioningPlan);

    @Named("partnerId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PartnerDTO toDtoPartnerId(Partner partner);
}
