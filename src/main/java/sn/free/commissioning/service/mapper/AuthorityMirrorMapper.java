package sn.free.commissioning.service.mapper;

import org.mapstruct.*;
import sn.free.commissioning.domain.AuthorityMirror;
import sn.free.commissioning.service.dto.AuthorityMirrorDTO;

/**
 * Mapper for the entity {@link AuthorityMirror} and its DTO {@link AuthorityMirrorDTO}.
 */
@Mapper(componentModel = "spring")
public interface AuthorityMirrorMapper extends EntityMapper<AuthorityMirrorDTO, AuthorityMirror> {}
