package sn.free.commissioning.service.mapper;

import java.util.List;
import java.util.Set;
import org.mapstruct.Mapper;
import sn.free.commissioning.service.dto.CommissionDTO;
import sn.free.commissioning.service.dto.CommissionForPaymentDTO;

/**
 * Mapper for the DTO {@link CommissionDTO} and its DTO BIS {@link CommissionDTO}.
 */
@Mapper(componentModel = "spring")
public interface CommissionForPaymentMapper extends EntityMapper<CommissionForPaymentDTO, CommissionDTO> {
    CommissionForPaymentDTO toDto(CommissionDTO commissionDTO);

    List<CommissionForPaymentDTO> toDto(List<CommissionDTO> commissionDTOs);
}
