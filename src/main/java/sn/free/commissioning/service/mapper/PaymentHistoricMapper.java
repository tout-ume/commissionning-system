package sn.free.commissioning.service.mapper;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.Mapper;
import sn.free.commissioning.domain.Commission;
import sn.free.commissioning.domain.Payment;
import sn.free.commissioning.service.dto.CommissionDTO;
import sn.free.commissioning.service.dto.CommissionForPaymentDTO;
import sn.free.commissioning.service.dto.PaymentHistoricDTO;

/**
 * Mapper for the entity {@link Payment} and its DTO {@link PaymentHistoricDTO}.
 */
@Mapper(componentModel = "spring", uses = { CommissionForPaymentDTO.class })
public interface PaymentHistoricMapper extends EntityMapper<PaymentHistoricDTO, Payment> {
    PaymentHistoricDTO toDto(Payment payment);

    List<PaymentHistoricDTO> toDto(List<Payment> payments);
    CommissionForPaymentDTO toDtoCommissionId(CommissionDTO commissionDTO);

    default Set<CommissionForPaymentDTO> toDtoCommissionIdSet(Set<CommissionDTO> commissionDTOS) {
        return commissionDTOS.stream().map(this::toDtoCommissionId).collect(Collectors.toSet());
    }
}
