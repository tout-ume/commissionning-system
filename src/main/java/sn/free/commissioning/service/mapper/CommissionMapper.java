package sn.free.commissioning.service.mapper;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.*;
import sn.free.commissioning.domain.Commission;
import sn.free.commissioning.domain.CommissionAccount;
import sn.free.commissioning.domain.ConfigurationPlan;
import sn.free.commissioning.domain.Payment;
import sn.free.commissioning.domain.TransactionOperation;
import sn.free.commissioning.service.dto.CommissionAccountDTO;
import sn.free.commissioning.service.dto.CommissionDTO;
import sn.free.commissioning.service.dto.ConfigurationPlanDTO;
import sn.free.commissioning.service.dto.PaymentDTO;
import sn.free.commissioning.service.dto.TransactionOperationDTO;

/**
 * Mapper for the entity {@link Commission} and its DTO {@link CommissionDTO}.
 */
@Mapper(componentModel = "spring")
public interface CommissionMapper extends EntityMapper<CommissionDTO, Commission> {
    @Mapping(target = "commissionAccount", ignore = true, source = "commissionAccount", qualifiedByName = "commissionAccountId")
    @Mapping(
        target = "transactionOperations",
        ignore = true,
        source = "transactionOperations",
        qualifiedByName = "transactionOperationIdSet"
    )
    @Mapping(target = "configurationPlan", ignore = true, source = "configurationPlan", qualifiedByName = "configurationPlanId")
    @Mapping(target = "payment", ignore = true, source = "payment", qualifiedByName = "paymentId")
    CommissionDTO toDto(Commission s);

    @Mapping(target = "removeTransactionOperation", ignore = true)
    Commission toEntity(CommissionDTO commissionDTO);

    List<Commission> toEntity(List<CommissionDTO> commissionDTO);

    @Named("commissionAccountId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CommissionAccountDTO toDtoCommissionAccountId(CommissionAccount commissionAccount);

    @Named("transactionOperationId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    TransactionOperationDTO toDtoTransactionOperationId(TransactionOperation transactionOperation);

    @Named("transactionOperationIdSet")
    default Set<TransactionOperationDTO> toDtoTransactionOperationIdSet(Set<TransactionOperation> transactionOperation) {
        return transactionOperation.stream().map(this::toDtoTransactionOperationId).collect(Collectors.toSet());
    }

    @Named("configurationPlanId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ConfigurationPlanDTO toDtoConfigurationPlanId(ConfigurationPlan configurationPlan);

    @Named("paymentId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PaymentDTO toDtoPaymentId(Payment payment);
}
