package sn.free.commissioning.service.mapper;

import org.mapstruct.*;
import sn.free.commissioning.domain.PartnerProfile;
import sn.free.commissioning.service.dto.PartnerProfileDTO;

/**
 * Mapper for the entity {@link PartnerProfile} and its DTO {@link PartnerProfileDTO}.
 */
@Mapper(componentModel = "spring")
public interface PartnerProfileMapper extends EntityMapper<PartnerProfileDTO, PartnerProfile> {}
