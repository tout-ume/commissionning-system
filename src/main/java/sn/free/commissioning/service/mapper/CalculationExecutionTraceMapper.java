package sn.free.commissioning.service.mapper;

import org.mapstruct.*;
import sn.free.commissioning.domain.CalculationExecutionTrace;
import sn.free.commissioning.service.dto.CalculationExecutionTraceDTO;

/**
 * Mapper for the entity {@link CalculationExecutionTrace} and its DTO {@link CalculationExecutionTraceDTO}.
 */
@Mapper(componentModel = "spring")
public interface CalculationExecutionTraceMapper extends EntityMapper<CalculationExecutionTraceDTO, CalculationExecutionTrace> {}
