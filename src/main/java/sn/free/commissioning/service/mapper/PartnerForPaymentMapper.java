package sn.free.commissioning.service.mapper;

import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.Mapper;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.service.dto.*;

@Mapper(componentModel = "spring")
public interface PartnerForPaymentMapper extends EntityMapper<PartnerForPaymentDTO, Partner> {
    PartnerForPaymentDTO toDto(Partner partner);
    CommissionForPaymentDTO toDtoCommissionId(CommissionDTO commissionDTO);

    default Set<CommissionForPaymentDTO> toDtoCommissionIdSet(Set<CommissionDTO> commissionDTOS) {
        return commissionDTOS.stream().map(this::toDtoCommissionId).collect(Collectors.toSet());
    }
}
