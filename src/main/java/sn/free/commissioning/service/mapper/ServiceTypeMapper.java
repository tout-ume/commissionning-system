package sn.free.commissioning.service.mapper;

import org.mapstruct.*;
import sn.free.commissioning.domain.ServiceType;
import sn.free.commissioning.service.dto.ServiceTypeDTO;

/**
 * Mapper for the entity {@link ServiceType} and its DTO {@link ServiceTypeDTO}.
 */
@Mapper(componentModel = "spring")
public interface ServiceTypeMapper extends EntityMapper<ServiceTypeDTO, ServiceType> {}
