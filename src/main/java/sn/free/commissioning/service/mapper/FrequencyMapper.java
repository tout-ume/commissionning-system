package sn.free.commissioning.service.mapper;

import org.mapstruct.*;
import sn.free.commissioning.domain.Frequency;
import sn.free.commissioning.service.dto.FrequencyDTO;

/**
 * Mapper for the entity {@link Frequency} and its DTO {@link FrequencyDTO}.
 */
@Mapper(componentModel = "spring")
public interface FrequencyMapper extends EntityMapper<FrequencyDTO, Frequency> {}
