package sn.free.commissioning.service.mapper;

import org.mapstruct.*;
import sn.free.commissioning.domain.CommissionAccount;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.service.dto.CommissionAccountDTO;
import sn.free.commissioning.service.dto.PartnerDTO;

/**
 * Mapper for the entity {@link CommissionAccount} and its DTO {@link CommissionAccountDTO}.
 */
@Mapper(componentModel = "spring")
public interface CommissionAccountMapper extends EntityMapper<CommissionAccountDTO, CommissionAccount> {
    @Mapping(target = "partner", source = "partner", qualifiedByName = "partnerId")
    CommissionAccountDTO toDto(CommissionAccount s);

    @Named("partnerId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PartnerDTO toDtoPartnerId(Partner partner);
}
