package sn.free.commissioning.service.mapper;

import org.mapstruct.*;
import sn.free.commissioning.domain.MissedTransactionOperation;
import sn.free.commissioning.service.dto.MissedTransactionOperationDTO;

/**
 * Mapper for the entity {@link MissedTransactionOperation} and its DTO {@link MissedTransactionOperationDTO}.
 */
@Mapper(componentModel = "spring")
public interface MissedTransactionOperationMapper extends EntityMapper<MissedTransactionOperationDTO, MissedTransactionOperation> {}
