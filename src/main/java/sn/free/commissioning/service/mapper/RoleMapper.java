package sn.free.commissioning.service.mapper;

import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.*;
import sn.free.commissioning.domain.AuthorityMirror;
import sn.free.commissioning.domain.Role;
import sn.free.commissioning.service.dto.AuthorityMirrorDTO;
import sn.free.commissioning.service.dto.RoleDTO;

/**
 * Mapper for the entity {@link Role} and its DTO {@link RoleDTO}.
 */
@Mapper(componentModel = "spring")
public interface RoleMapper extends EntityMapper<RoleDTO, Role> {
    @Mapping(target = "authorities", source = "authorities", qualifiedByName = "authorityMirrorIdSet")
    RoleDTO toDto(Role s);

    @Mapping(target = "removeAuthorities", ignore = true)
    Role toEntity(RoleDTO roleDTO);

    @Named("authorityMirrorId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    AuthorityMirrorDTO toDtoAuthorityMirrorId(AuthorityMirror authorityMirror);

    @Named("authorityMirrorIdSet")
    default Set<AuthorityMirrorDTO> toDtoAuthorityMirrorIdSet(Set<AuthorityMirror> authorityMirror) {
        return authorityMirror.stream().map(this::toDtoAuthorityMirrorId).collect(Collectors.toSet());
    }
}
