package sn.free.commissioning.service.mapper;

import org.mapstruct.*;
import sn.free.commissioning.domain.MissedTransactionOperationArchive;
import sn.free.commissioning.service.dto.MissedTransactionOperationArchiveDTO;

/**
 * Mapper for the entity {@link MissedTransactionOperationArchive} and its DTO {@link MissedTransactionOperationArchiveDTO}.
 */
@Mapper(componentModel = "spring")
public interface MissedTransactionOperationArchiveMapper
    extends EntityMapper<MissedTransactionOperationArchiveDTO, MissedTransactionOperationArchive> {}
