package sn.free.commissioning.service.mapper;

import org.mapstruct.*;
import sn.free.commissioning.domain.TransactionOperation;
import sn.free.commissioning.service.dto.TransactionOperationDTO;

/**
 * Mapper for the entity {@link TransactionOperation} and its DTO {@link TransactionOperationDTO}.
 */
@Mapper(componentModel = "spring")
public interface TransactionOperationMapper extends EntityMapper<TransactionOperationDTO, TransactionOperation> {}
