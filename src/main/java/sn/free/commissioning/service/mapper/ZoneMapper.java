package sn.free.commissioning.service.mapper;

import org.mapstruct.*;
import sn.free.commissioning.domain.Territory;
import sn.free.commissioning.domain.Zone;
import sn.free.commissioning.service.dto.TerritoryDTO;
import sn.free.commissioning.service.dto.ZoneDTO;

/**
 * Mapper for the entity {@link Zone} and its DTO {@link ZoneDTO}.
 */
@Mapper(componentModel = "spring")
public interface ZoneMapper extends EntityMapper<ZoneDTO, Zone> {
    @Mapping(target = "territory", source = "territory", qualifiedByName = "territoryId")
    ZoneDTO toDto(Zone s);

    @Named("territoryId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    TerritoryDTO toDtoTerritoryId(Territory territory);
}
