package sn.free.commissioning.service.mapper;

import org.mapstruct.*;
import sn.free.commissioning.domain.CommissionType;
import sn.free.commissioning.domain.ConfigurationPlan;
import sn.free.commissioning.service.dto.CommissionTypeDTO;
import sn.free.commissioning.service.dto.ConfigurationPlanDTO;

/**
 * Mapper for the entity {@link CommissionType} and its DTO {@link CommissionTypeDTO}.
 */
@Mapper(componentModel = "spring")
public interface CommissionTypeMapper extends EntityMapper<CommissionTypeDTO, CommissionType> {
    //    @Mapping(target = "configurationPlan", ignore = true)

    @Named("configurationPlanId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ConfigurationPlanDTO toDtoConfigurationPlanId(ConfigurationPlan configurationPlan);

    CommissionTypeDTO toDto(CommissionType s);
}
