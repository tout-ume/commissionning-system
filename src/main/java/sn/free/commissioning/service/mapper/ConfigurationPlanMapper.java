package sn.free.commissioning.service.mapper;

import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.*;
import sn.free.commissioning.domain.CommissioningPlan;
import sn.free.commissioning.domain.ConfigurationPlan;
import sn.free.commissioning.domain.PartnerProfile;
import sn.free.commissioning.domain.Zone;
import sn.free.commissioning.service.dto.CommissioningPlanDTO;
import sn.free.commissioning.service.dto.ConfigurationPlanDTO;
import sn.free.commissioning.service.dto.PartnerProfileDTO;
import sn.free.commissioning.service.dto.ZoneDTO;

/**
 * Mapper for the entity {@link ConfigurationPlan} and its DTO {@link ConfigurationPlanDTO}.
 */
@Mapper(componentModel = "spring")
public interface ConfigurationPlanMapper extends EntityMapper<ConfigurationPlanDTO, ConfigurationPlan> {
    //    @Mapping(target = "partnerProfile", source = "partnerProfile", qualifiedByName = "partnerProfileId")
    @Mapping(target = "commissioningPlan", source = "commissioningPlan", qualifiedByName = "commissioningPlanId")
    //    @Mapping(target = "zones", source = "zones", qualifiedByName = "zoneIdSet")
    ConfigurationPlanDTO toDto(ConfigurationPlan s);

    @Mapping(target = "removeZones", ignore = true)
    ConfigurationPlan toEntity(ConfigurationPlanDTO configurationPlanDTO);

    @Named("partnerProfileId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PartnerProfileDTO toDtoPartnerProfileId(PartnerProfile partnerProfile);

    @Named("commissioningPlanId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CommissioningPlanDTO toDtoCommissioningPlanId(CommissioningPlan commissioningPlan);

    @Named("zoneId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ZoneDTO toDtoZoneId(Zone zone);

    @Named("zoneIdSet")
    default Set<ZoneDTO> toDtoZoneIdSet(Set<Zone> zone) {
        return zone.stream().map(this::toDtoZoneId).collect(Collectors.toSet());
    }
}
