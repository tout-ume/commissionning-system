package sn.free.commissioning.service.mapper;

import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.*;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.domain.Zone;
import sn.free.commissioning.service.dto.PartnerDTO;
import sn.free.commissioning.service.dto.ZoneDTO;

/**
 * Mapper for the entity {@link Partner} and its DTO {@link PartnerDTO}.
 */
@Mapper(componentModel = "spring")
public interface PartnerMapper extends EntityMapper<PartnerDTO, Partner> {
    @Mapping(target = "zones", source = "zones", qualifiedByName = "zoneIdSet")
    @Mapping(target = "parent", source = "parent", qualifiedByName = "partnerId")
    PartnerDTO toDto(Partner s);

    @Mapping(target = "removeZone", ignore = true)
    Partner toEntity(PartnerDTO partnerDTO);

    @Named("partnerId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PartnerDTO toDtoPartnerId(Partner partner);

    @Named("zoneId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ZoneDTO toDtoZoneId(Zone zone);

    @Named("zoneIdSet")
    default Set<ZoneDTO> toDtoZoneIdSet(Set<Zone> zone) {
        return zone.stream().map(this::toDtoZoneId).collect(Collectors.toSet());
    }
}
