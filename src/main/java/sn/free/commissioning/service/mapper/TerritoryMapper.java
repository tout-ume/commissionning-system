package sn.free.commissioning.service.mapper;

import org.mapstruct.*;
import sn.free.commissioning.domain.Territory;
import sn.free.commissioning.service.dto.TerritoryDTO;

/**
 * Mapper for the entity {@link Territory} and its DTO {@link TerritoryDTO}.
 */
@Mapper(componentModel = "spring")
public interface TerritoryMapper extends EntityMapper<TerritoryDTO, Territory> {}
