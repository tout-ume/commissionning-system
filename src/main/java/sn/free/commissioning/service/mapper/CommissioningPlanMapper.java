package sn.free.commissioning.service.mapper;

import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.*;
import sn.free.commissioning.domain.CommissioningPlan;
import sn.free.commissioning.domain.Frequency;
import sn.free.commissioning.domain.Period;
import sn.free.commissioning.domain.ServiceType;
import sn.free.commissioning.service.dto.CommissioningPlanDTO;
import sn.free.commissioning.service.dto.FrequencyDTO;
import sn.free.commissioning.service.dto.PeriodDTO;
import sn.free.commissioning.service.dto.ServiceTypeDTO;

/**
 * Mapper for the entity {@link CommissioningPlan} and its DTO {@link CommissioningPlanDTO}.
 */
@Mapper(componentModel = "spring")
public interface CommissioningPlanMapper extends EntityMapper<CommissioningPlanDTO, CommissioningPlan> {
    //    @Mapping(target = "paymentFrequency", source = "paymentFrequency", qualifiedByName = "frequencyId")
    //    @Mapping(target = "calculusFrequency", source = "calculusFrequency", qualifiedByName = "frequencyId")
    //    @Mapping(target = "calculusPeriod", source = "calculusPeriod", qualifiedByName = "periodId")
    //    @Mapping(target = "paymentPeriod", source = "paymentPeriod", qualifiedByName = "periodId")
    //    @Mapping(target = "services", source = "services", qualifiedByName = "serviceTypeIdSet")
    @Mapping(target = "configurationPlans", ignore = true)
    CommissioningPlanDTO toDto(CommissioningPlan s);

    @Mapping(target = "removeServices", ignore = true)
    CommissioningPlan toEntity(CommissioningPlanDTO commissioningPlanDTO);

    @Named("frequencyId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    FrequencyDTO toDtoFrequencyId(Frequency frequency);

    @Named("periodId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PeriodDTO toDtoPeriodId(Period period);

    @Named("serviceTypeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ServiceTypeDTO toDtoServiceTypeId(ServiceType serviceType);

    @Named("serviceTypeIdSet")
    default Set<ServiceTypeDTO> toDtoServiceTypeIdSet(Set<ServiceType> serviceType) {
        return serviceType.stream().map(this::toDtoServiceTypeId).collect(Collectors.toSet());
    }
}
