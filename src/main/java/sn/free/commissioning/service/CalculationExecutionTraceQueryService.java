package sn.free.commissioning.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.*; // for static metamodels
import sn.free.commissioning.domain.CalculationExecutionTrace;
import sn.free.commissioning.repository.CalculationExecutionTraceRepository;
import sn.free.commissioning.service.criteria.CalculationExecutionTraceCriteria;
import sn.free.commissioning.service.dto.CalculationExecutionTraceDTO;
import sn.free.commissioning.service.mapper.CalculationExecutionTraceMapper;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link CalculationExecutionTrace} entities in the database.
 * The main input is a {@link CalculationExecutionTraceCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CalculationExecutionTraceDTO} or a {@link Page} of {@link CalculationExecutionTraceDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CalculationExecutionTraceQueryService extends QueryService<CalculationExecutionTrace> {

    private final Logger log = LoggerFactory.getLogger(CalculationExecutionTraceQueryService.class);

    private final CalculationExecutionTraceRepository calculationExecutionTraceRepository;

    private final CalculationExecutionTraceMapper calculationExecutionTraceMapper;

    public CalculationExecutionTraceQueryService(
        CalculationExecutionTraceRepository calculationExecutionTraceRepository,
        CalculationExecutionTraceMapper calculationExecutionTraceMapper
    ) {
        this.calculationExecutionTraceRepository = calculationExecutionTraceRepository;
        this.calculationExecutionTraceMapper = calculationExecutionTraceMapper;
    }

    /**
     * Return a {@link List} of {@link CalculationExecutionTraceDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CalculationExecutionTraceDTO> findByCriteria(CalculationExecutionTraceCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CalculationExecutionTrace> specification = createSpecification(criteria);
        return calculationExecutionTraceMapper.toDto(calculationExecutionTraceRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link CalculationExecutionTraceDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CalculationExecutionTraceDTO> findByCriteria(CalculationExecutionTraceCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CalculationExecutionTrace> specification = createSpecification(criteria);
        return calculationExecutionTraceRepository.findAll(specification, page).map(calculationExecutionTraceMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CalculationExecutionTraceCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CalculationExecutionTrace> specification = createSpecification(criteria);
        return calculationExecutionTraceRepository.count(specification);
    }

    /**
     * Function to convert {@link CalculationExecutionTraceCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<CalculationExecutionTrace> createSpecification(CalculationExecutionTraceCriteria criteria) {
        Specification<CalculationExecutionTrace> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), CalculationExecutionTrace_.id));
            }
            if (criteria.getFrequencyId() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getFrequencyId(), CalculationExecutionTrace_.frequencyId));
            }
            if (criteria.getExecutionDate() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getExecutionDate(), CalculationExecutionTrace_.executionDate));
            }
            if (criteria.getIsExecuted() != null) {
                specification = specification.and(buildSpecification(criteria.getIsExecuted(), CalculationExecutionTrace_.isExecuted));
            }
            if (criteria.getExecutionResult() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getExecutionResult(), CalculationExecutionTrace_.executionResult));
            }
            if (criteria.getSpare1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare1(), CalculationExecutionTrace_.spare1));
            }
            if (criteria.getSpare2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare2(), CalculationExecutionTrace_.spare2));
            }
            if (criteria.getSpare3() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare3(), CalculationExecutionTrace_.spare3));
            }
        }
        return specification;
    }
}
