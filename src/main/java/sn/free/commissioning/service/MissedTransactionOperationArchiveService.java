package sn.free.commissioning.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.commissioning.service.dto.MissedTransactionOperationArchiveDTO;

/**
 * Service Interface for managing {@link sn.free.commissioning.domain.MissedTransactionOperationArchive}.
 */
public interface MissedTransactionOperationArchiveService {
    /**
     * Save a missedTransactionOperationArchive.
     *
     * @param missedTransactionOperationArchiveDTO the entity to save.
     * @return the persisted entity.
     */
    MissedTransactionOperationArchiveDTO save(MissedTransactionOperationArchiveDTO missedTransactionOperationArchiveDTO);

    /**
     * Updates a missedTransactionOperationArchive.
     *
     * @param missedTransactionOperationArchiveDTO the entity to update.
     * @return the persisted entity.
     */
    MissedTransactionOperationArchiveDTO update(MissedTransactionOperationArchiveDTO missedTransactionOperationArchiveDTO);

    /**
     * Partially updates a missedTransactionOperationArchive.
     *
     * @param missedTransactionOperationArchiveDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<MissedTransactionOperationArchiveDTO> partialUpdate(MissedTransactionOperationArchiveDTO missedTransactionOperationArchiveDTO);

    /**
     * Get all the missedTransactionOperationArchives.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MissedTransactionOperationArchiveDTO> findAll(Pageable pageable);

    /**
     * Get the "id" missedTransactionOperationArchive.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MissedTransactionOperationArchiveDTO> findOne(Long id);

    /**
     * Delete the "id" missedTransactionOperationArchive.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
