package sn.free.commissioning.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.*; // for static metamodels
import sn.free.commissioning.domain.ConfigurationPlan;
import sn.free.commissioning.repository.ConfigurationPlanRepository;
import sn.free.commissioning.service.criteria.ConfigurationPlanCriteria;
import sn.free.commissioning.service.dto.ConfigurationPlanDTO;
import sn.free.commissioning.service.mapper.ConfigurationPlanMapper;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link ConfigurationPlan} entities in the database.
 * The main input is a {@link ConfigurationPlanCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ConfigurationPlanDTO} or a {@link Page} of {@link ConfigurationPlanDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ConfigurationPlanQueryService extends QueryService<ConfigurationPlan> {

    private final Logger log = LoggerFactory.getLogger(ConfigurationPlanQueryService.class);

    private final ConfigurationPlanRepository configurationPlanRepository;

    private final ConfigurationPlanMapper configurationPlanMapper;

    public ConfigurationPlanQueryService(
        ConfigurationPlanRepository configurationPlanRepository,
        ConfigurationPlanMapper configurationPlanMapper
    ) {
        this.configurationPlanRepository = configurationPlanRepository;
        this.configurationPlanMapper = configurationPlanMapper;
    }

    /**
     * Return a {@link List} of {@link ConfigurationPlanDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ConfigurationPlanDTO> findByCriteria(ConfigurationPlanCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ConfigurationPlan> specification = createSpecification(criteria);
        return configurationPlanMapper.toDto(configurationPlanRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ConfigurationPlanDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ConfigurationPlanDTO> findByCriteria(ConfigurationPlanCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ConfigurationPlan> specification = createSpecification(criteria);
        return configurationPlanRepository.findAll(specification, page).map(configurationPlanMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ConfigurationPlanCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ConfigurationPlan> specification = createSpecification(criteria);
        return configurationPlanRepository.count(specification);
    }

    /**
     * Function to convert {@link ConfigurationPlanCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ConfigurationPlan> createSpecification(ConfigurationPlanCriteria criteria) {
        Specification<ConfigurationPlan> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ConfigurationPlan_.id));
            }
            if (criteria.getSpare1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare1(), ConfigurationPlan_.spare1));
            }
            if (criteria.getSpare2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare2(), ConfigurationPlan_.spare2));
            }
            if (criteria.getSpare3() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare3(), ConfigurationPlan_.spare3));
            }
            if (criteria.getCommissionTypeId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getCommissionTypeId(),
                            root -> root.join(ConfigurationPlan_.commissionTypes, JoinType.LEFT).get(CommissionType_.id)
                        )
                    );
            }
            if (criteria.getCommissionId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getCommissionId(),
                            root -> root.join(ConfigurationPlan_.commissions, JoinType.LEFT).get(Commission_.id)
                        )
                    );
            }
            if (criteria.getPartnerProfileId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getPartnerProfileId(),
                            root -> root.join(ConfigurationPlan_.partnerProfile, JoinType.LEFT).get(PartnerProfile_.id)
                        )
                    );
            }
            if (criteria.getCommissioningPlanId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getCommissioningPlanId(),
                            root -> root.join(ConfigurationPlan_.commissioningPlan, JoinType.LEFT).get(CommissioningPlan_.id)
                        )
                    );
            }
            if (criteria.getZonesId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getZonesId(), root -> root.join(ConfigurationPlan_.zones, JoinType.LEFT).get(Zone_.id))
                    );
            }
        }
        return specification;
    }
}
