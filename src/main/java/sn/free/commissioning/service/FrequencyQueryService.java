package sn.free.commissioning.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.*; // for static metamodels
import sn.free.commissioning.domain.Frequency;
import sn.free.commissioning.repository.FrequencyRepository;
import sn.free.commissioning.service.criteria.FrequencyCriteria;
import sn.free.commissioning.service.dto.FrequencyDTO;
import sn.free.commissioning.service.mapper.FrequencyMapper;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Frequency} entities in the database.
 * The main input is a {@link FrequencyCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link FrequencyDTO} or a {@link Page} of {@link FrequencyDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class FrequencyQueryService extends QueryService<Frequency> {

    private final Logger log = LoggerFactory.getLogger(FrequencyQueryService.class);

    private final FrequencyRepository frequencyRepository;

    private final FrequencyMapper frequencyMapper;

    public FrequencyQueryService(FrequencyRepository frequencyRepository, FrequencyMapper frequencyMapper) {
        this.frequencyRepository = frequencyRepository;
        this.frequencyMapper = frequencyMapper;
    }

    /**
     * Return a {@link List} of {@link FrequencyDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<FrequencyDTO> findByCriteria(FrequencyCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Frequency> specification = createSpecification(criteria);
        return frequencyMapper.toDto(frequencyRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link FrequencyDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<FrequencyDTO> findByCriteria(FrequencyCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Frequency> specification = createSpecification(criteria);
        return frequencyRepository.findAll(specification, page).map(frequencyMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(FrequencyCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Frequency> specification = createSpecification(criteria);
        return frequencyRepository.count(specification);
    }

    /**
     * Function to convert {@link FrequencyCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Frequency> createSpecification(FrequencyCriteria criteria) {
        Specification<Frequency> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Frequency_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Frequency_.name));
            }
            if (criteria.getType() != null) {
                specification = specification.and(buildSpecification(criteria.getType(), Frequency_.type));
            }
            if (criteria.getOperationType() != null) {
                specification = specification.and(buildSpecification(criteria.getOperationType(), Frequency_.operationType));
            }
            if (criteria.getExecutionTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getExecutionTime(), Frequency_.executionTime));
            }
            if (criteria.getExecutionWeekDay() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getExecutionWeekDay(), Frequency_.executionWeekDay));
            }
            if (criteria.getExecutionMonthDay() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getExecutionMonthDay(), Frequency_.executionMonthDay));
            }
            if (criteria.getDaysAfter() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDaysAfter(), Frequency_.daysAfter));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), Frequency_.createdBy));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), Frequency_.createdDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), Frequency_.lastModifiedBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), Frequency_.lastModifiedDate));
            }
            if (criteria.getCycle() != null) {
                specification = specification.and(buildSpecification(criteria.getCycle(), Frequency_.cycle));
            }
            if (criteria.getPeriodOfOccurrence() != null) {
                specification = specification.and(buildSpecification(criteria.getPeriodOfOccurrence(), Frequency_.periodOfOccurrence));
            }
            if (criteria.getNumberOfCycle() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNumberOfCycle(), Frequency_.numberOfCycle));
            }
            if (criteria.getOccurrenceByPeriod() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getOccurrenceByPeriod(), Frequency_.occurrenceByPeriod));
            }
            if (criteria.getDatesOfOccurrence() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDatesOfOccurrence(), Frequency_.datesOfOccurrence));
            }
            if (criteria.getSpare1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare1(), Frequency_.spare1));
            }
            if (criteria.getSpare2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare2(), Frequency_.spare2));
            }
            if (criteria.getSpare3() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare3(), Frequency_.spare3));
            }
        }
        return specification;
    }
}
