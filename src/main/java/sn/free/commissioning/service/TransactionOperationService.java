package sn.free.commissioning.service;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.commissioning.calculus_engine.domain.TimeInterval;
import sn.free.commissioning.domain.Frequency;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.domain.Period;
import sn.free.commissioning.domain.TransactionOperation;
import sn.free.commissioning.service.dto.TransactionOperationDTO;

/**
 * Service Interface for managing {@link sn.free.commissioning.domain.TransactionOperation}.
 */
public interface TransactionOperationService {
    /**
     * Save a transactionOperation.
     *
     * @param transactionOperationDTO the entity to save.
     * @return the persisted entity.
     */
    TransactionOperationDTO save(TransactionOperationDTO transactionOperationDTO);

    /**
     * Updates a transactionOperation.
     *
     * @param transactionOperationDTO the entity to update.
     * @return the persisted entity.
     */
    TransactionOperationDTO update(TransactionOperationDTO transactionOperationDTO);

    /**
     * Partially updates a transactionOperation.
     *
     * @param transactionOperationDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<TransactionOperationDTO> partialUpdate(TransactionOperationDTO transactionOperationDTO);

    /**
     * Get all the transactionOperations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TransactionOperationDTO> findAll(Pageable pageable);

    /**
     * Get all the transactionOperations for a partner performed in a given period.
     *
     * @param partner the partner informations.
     * @param serviceTypes the list of serviceTypes to consider.
     * @param zones the list of zones to consider.
     * @param timeInterval the timeInterval to consider.
     * @return the list of entities.
     */
    public List<TransactionOperation> getPartnerTransactions(
        Partner partner,
        List<String> serviceTypes,
        List<String> zones,
        TimeInterval timeInterval
    );

    /**
     * Get all the transactionOperations with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<TransactionOperationDTO> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" transactionOperation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<TransactionOperationDTO> findOne(Long id);

    /**
     * Delete the "id" transactionOperation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Get a partner's transactionOperations list
     *
     * @param senderMsisdn partner's msisdn
     * @return the list of entities.
     */
    List<TransactionOperationDTO> findBySenderMsisdnIgnoreCase(String senderMsisdn);

    /**
     * Get previous day tagged transactionOperations list
     *
     * @return the list of transactionOperations.
     */
    List<TransactionOperationDTO> getPreviousDayTaggedTransactions();
}
