package sn.free.commissioning.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.commissioning.domain.CommissionAccount;
import sn.free.commissioning.service.dto.CommissionAccountDTO;

/**
 * Service Interface for managing {@link sn.free.commissioning.domain.CommissionAccount}.
 */
public interface CommissionAccountService {
    /**
     * Save a commissionAccount.
     *
     * @param commissionAccountDTO the entity to save.
     * @return the persisted entity.
     */
    CommissionAccountDTO save(CommissionAccountDTO commissionAccountDTO);

    /**
     * Updates a commissionAccount.
     *
     * @param commissionAccountDTO the entity to update.
     * @return the persisted entity.
     */
    CommissionAccountDTO update(CommissionAccountDTO commissionAccountDTO);

    /**
     * Partially updates a commissionAccount.
     *
     * @param commissionAccountDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<CommissionAccountDTO> partialUpdate(CommissionAccountDTO commissionAccountDTO);

    /**
     * Get all the commissionAccounts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CommissionAccountDTO> findAll(Pageable pageable);

    /**
     * Get the "id" commissionAccount.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CommissionAccountDTO> findOne(Long id);

    /**
     * Delete the "id" commissionAccount.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Get a partner's commissionAccount.
     *
     * @param partnerId the commissionAccount owner's id.
     * @return the entity.
     */
    Optional<CommissionAccount> findByPartner(Long partnerId);
}
