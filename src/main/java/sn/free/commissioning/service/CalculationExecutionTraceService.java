package sn.free.commissioning.service;

import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import sn.free.commissioning.service.dto.CalculationExecutionTraceDTO;

/**
 * Service Interface for managing {@link sn.free.commissioning.domain.CalculationExecutionTrace}.
 */
public interface CalculationExecutionTraceService {
    /**
     * Save a calculationExecutionTrace.
     *
     * @param calculationExecutionTraceDTO the entity to save.
     * @return the persisted entity.
     */
    CalculationExecutionTraceDTO save(CalculationExecutionTraceDTO calculationExecutionTraceDTO);

    /**
     * Updates a calculationExecutionTrace.
     *
     * @param calculationExecutionTraceDTO the entity to update.
     * @return the persisted entity.
     */
    CalculationExecutionTraceDTO update(CalculationExecutionTraceDTO calculationExecutionTraceDTO);

    /**
     * Partially updates a calculationExecutionTrace.
     *
     * @param calculationExecutionTraceDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<CalculationExecutionTraceDTO> partialUpdate(CalculationExecutionTraceDTO calculationExecutionTraceDTO);

    /**
     * Get all the calculationExecutionTraces.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CalculationExecutionTraceDTO> findAll(Pageable pageable);

    /**
     * Get the "id" calculationExecutionTrace.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CalculationExecutionTraceDTO> findOne(Long id);

    /**
     * Delete the "id" calculationExecutionTrace.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
