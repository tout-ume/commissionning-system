package sn.free.commissioning.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.*; // for static metamodels
import sn.free.commissioning.domain.Blacklisted;
import sn.free.commissioning.repository.BlacklistedRepository;
import sn.free.commissioning.service.criteria.BlacklistedCriteria;
import sn.free.commissioning.service.dto.BlacklistedDTO;
import sn.free.commissioning.service.mapper.BlacklistedMapper;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Blacklisted} entities in the database.
 * The main input is a {@link BlacklistedCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link BlacklistedDTO} or a {@link Page} of {@link BlacklistedDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BlacklistedQueryService extends QueryService<Blacklisted> {

    private final Logger log = LoggerFactory.getLogger(BlacklistedQueryService.class);

    private final BlacklistedRepository blacklistedRepository;

    private final BlacklistedMapper blacklistedMapper;

    public BlacklistedQueryService(BlacklistedRepository blacklistedRepository, BlacklistedMapper blacklistedMapper) {
        this.blacklistedRepository = blacklistedRepository;
        this.blacklistedMapper = blacklistedMapper;
    }

    /**
     * Return a {@link List} of {@link BlacklistedDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<BlacklistedDTO> findByCriteria(BlacklistedCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Blacklisted> specification = createSpecification(criteria);
        return blacklistedMapper.toDto(blacklistedRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link BlacklistedDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<BlacklistedDTO> findByCriteria(BlacklistedCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Blacklisted> specification = createSpecification(criteria);
        return blacklistedRepository.findAll(specification, page).map(blacklistedMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BlacklistedCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Blacklisted> specification = createSpecification(criteria);
        return blacklistedRepository.count(specification);
    }

    /**
     * Function to convert {@link BlacklistedCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Blacklisted> createSpecification(BlacklistedCriteria criteria) {
        Specification<Blacklisted> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Blacklisted_.id));
            }
            if (criteria.getReason() != null) {
                specification = specification.and(buildStringSpecification(criteria.getReason(), Blacklisted_.reason));
            }
            if (criteria.getBlocked() != null) {
                specification = specification.and(buildSpecification(criteria.getBlocked(), Blacklisted_.blocked));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), Blacklisted_.createdBy));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), Blacklisted_.createdDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), Blacklisted_.lastModifiedBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), Blacklisted_.lastModifiedDate));
            }
            if (criteria.getSpare1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare1(), Blacklisted_.spare1));
            }
            if (criteria.getSpare2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare2(), Blacklisted_.spare2));
            }
            if (criteria.getSpare3() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare3(), Blacklisted_.spare3));
            }
            if (criteria.getCommissioningPlanId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getCommissioningPlanId(),
                            root -> root.join(Blacklisted_.commissioningPlan, JoinType.LEFT).get(CommissioningPlan_.id)
                        )
                    );
            }
            if (criteria.getPartnerId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getPartnerId(), root -> root.join(Blacklisted_.partner, JoinType.LEFT).get(Partner_.id))
                    );
            }
        }
        return specification;
    }
}
