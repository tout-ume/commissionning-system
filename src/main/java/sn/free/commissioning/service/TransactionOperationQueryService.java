package sn.free.commissioning.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.*; // for static metamodels
import sn.free.commissioning.domain.TransactionOperation;
import sn.free.commissioning.repository.TransactionOperationRepository;
import sn.free.commissioning.service.criteria.TransactionOperationCriteria;
import sn.free.commissioning.service.dto.TransactionOperationDTO;
import sn.free.commissioning.service.mapper.TransactionOperationMapper;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link TransactionOperation} entities in the database.
 * The main input is a {@link TransactionOperationCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link TransactionOperationDTO} or a {@link Page} of {@link TransactionOperationDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TransactionOperationQueryService extends QueryService<TransactionOperation> {

    private final Logger log = LoggerFactory.getLogger(TransactionOperationQueryService.class);

    private final TransactionOperationRepository transactionOperationRepository;

    private final TransactionOperationMapper transactionOperationMapper;

    public TransactionOperationQueryService(
        TransactionOperationRepository transactionOperationRepository,
        TransactionOperationMapper transactionOperationMapper
    ) {
        this.transactionOperationRepository = transactionOperationRepository;
        this.transactionOperationMapper = transactionOperationMapper;
    }

    /**
     * Return a {@link List} of {@link TransactionOperationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<TransactionOperationDTO> findByCriteria(TransactionOperationCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<TransactionOperation> specification = createSpecification(criteria);
        return transactionOperationMapper.toDto(transactionOperationRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link TransactionOperationDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<TransactionOperationDTO> findByCriteria(TransactionOperationCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<TransactionOperation> specification = createSpecification(criteria);
        return transactionOperationRepository.findAll(specification, page).map(transactionOperationMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TransactionOperationCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<TransactionOperation> specification = createSpecification(criteria);
        return transactionOperationRepository.count(specification);
    }

    /**
     * Function to convert {@link TransactionOperationCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<TransactionOperation> createSpecification(TransactionOperationCriteria criteria) {
        Specification<TransactionOperation> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), TransactionOperation_.id));
            }
            if (criteria.getAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAmount(), TransactionOperation_.amount));
            }
            if (criteria.getSubsMsisdn() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubsMsisdn(), TransactionOperation_.subsMsisdn));
            }
            if (criteria.getAgentMsisdn() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAgentMsisdn(), TransactionOperation_.agentMsisdn));
            }
            if (criteria.getTypeTransaction() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getTypeTransaction(), TransactionOperation_.typeTransaction));
            }
            if (criteria.getCreatedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedAt(), TransactionOperation_.createdAt));
            }
            if (criteria.getTransactionStatus() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getTransactionStatus(), TransactionOperation_.transactionStatus));
            }
            if (criteria.getSenderZone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSenderZone(), TransactionOperation_.senderZone));
            }
            if (criteria.getSenderProfile() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getSenderProfile(), TransactionOperation_.senderProfile));
            }
            if (criteria.getCodeTerritory() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getCodeTerritory(), TransactionOperation_.codeTerritory));
            }
            if (criteria.getSubType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSubType(), TransactionOperation_.subType));
            }
            if (criteria.getOperationShortDate() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getOperationShortDate(), TransactionOperation_.operationShortDate));
            }
            if (criteria.getOperationDate() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getOperationDate(), TransactionOperation_.operationDate));
            }
            if (criteria.getIsFraud() != null) {
                specification = specification.and(buildSpecification(criteria.getIsFraud(), TransactionOperation_.isFraud));
            }
            if (criteria.getTaggedAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTaggedAt(), TransactionOperation_.taggedAt));
            }
            if (criteria.getFraudSource() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFraudSource(), TransactionOperation_.fraudSource));
            }
            if (criteria.getComment() != null) {
                specification = specification.and(buildStringSpecification(criteria.getComment(), TransactionOperation_.comment));
            }
            if (criteria.getFalsePositiveDetectedAt() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getFalsePositiveDetectedAt(), TransactionOperation_.falsePositiveDetectedAt)
                    );
            }
            if (criteria.getTid() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTid(), TransactionOperation_.tid));
            }
            if (criteria.getParentMsisdn() != null) {
                specification = specification.and(buildStringSpecification(criteria.getParentMsisdn(), TransactionOperation_.parentMsisdn));
            }
            if (criteria.getFctDt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFctDt(), TransactionOperation_.fctDt));
            }
            if (criteria.getParentId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getParentId(), TransactionOperation_.parentId));
            }
            if (criteria.getCanceledAt() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCanceledAt(), TransactionOperation_.canceledAt));
            }
            if (criteria.getCanceledId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCanceledId(), TransactionOperation_.canceledId));
            }
            if (criteria.getProductId() != null) {
                specification = specification.and(buildStringSpecification(criteria.getProductId(), TransactionOperation_.productId));
            }
            if (criteria.getCommissionsId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getCommissionsId(),
                            root -> root.join(TransactionOperation_.commissions, JoinType.LEFT).get(Commission_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
