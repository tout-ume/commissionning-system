package sn.free.commissioning.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.*; // for static metamodels
import sn.free.commissioning.domain.Period;
import sn.free.commissioning.repository.PeriodRepository;
import sn.free.commissioning.service.criteria.PeriodCriteria;
import sn.free.commissioning.service.dto.PeriodDTO;
import sn.free.commissioning.service.mapper.PeriodMapper;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Period} entities in the database.
 * The main input is a {@link PeriodCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PeriodDTO} or a {@link Page} of {@link PeriodDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PeriodQueryService extends QueryService<Period> {

    private final Logger log = LoggerFactory.getLogger(PeriodQueryService.class);

    private final PeriodRepository periodRepository;

    private final PeriodMapper periodMapper;

    public PeriodQueryService(PeriodRepository periodRepository, PeriodMapper periodMapper) {
        this.periodRepository = periodRepository;
        this.periodMapper = periodMapper;
    }

    /**
     * Return a {@link List} of {@link PeriodDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PeriodDTO> findByCriteria(PeriodCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Period> specification = createSpecification(criteria);
        return periodMapper.toDto(periodRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PeriodDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PeriodDTO> findByCriteria(PeriodCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Period> specification = createSpecification(criteria);
        return periodRepository.findAll(specification, page).map(periodMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PeriodCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Period> specification = createSpecification(criteria);
        return periodRepository.count(specification);
    }

    /**
     * Function to convert {@link PeriodCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Period> createSpecification(PeriodCriteria criteria) {
        Specification<Period> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Period_.id));
            }
            if (criteria.getMonthDayFrom() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMonthDayFrom(), Period_.monthDayFrom));
            }
            if (criteria.getMonthDayTo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMonthDayTo(), Period_.monthDayTo));
            }
            if (criteria.getWeekDayFrom() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getWeekDayFrom(), Period_.weekDayFrom));
            }
            if (criteria.getWeekDayTo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getWeekDayTo(), Period_.weekDayTo));
            }
            if (criteria.getDayHourFrom() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDayHourFrom(), Period_.dayHourFrom));
            }
            if (criteria.getDayHourTo() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getDayHourTo(), Period_.dayHourTo));
            }
            if (criteria.getIsPreviousDayHourFrom() != null) {
                specification = specification.and(buildSpecification(criteria.getIsPreviousDayHourFrom(), Period_.isPreviousDayHourFrom));
            }
            if (criteria.getIsPreviousDayHourTo() != null) {
                specification = specification.and(buildSpecification(criteria.getIsPreviousDayHourTo(), Period_.isPreviousDayHourTo));
            }
            if (criteria.getIsPreviousMonthDayFrom() != null) {
                specification = specification.and(buildSpecification(criteria.getIsPreviousMonthDayFrom(), Period_.isPreviousMonthDayFrom));
            }
            if (criteria.getIsPreviousMonthDayTo() != null) {
                specification = specification.and(buildSpecification(criteria.getIsPreviousMonthDayTo(), Period_.isPreviousMonthDayTo));
            }
            if (criteria.getIsCompleteDay() != null) {
                specification = specification.and(buildSpecification(criteria.getIsCompleteDay(), Period_.isCompleteDay));
            }
            if (criteria.getIsCompleteMonth() != null) {
                specification = specification.and(buildSpecification(criteria.getIsCompleteMonth(), Period_.isCompleteMonth));
            }
            if (criteria.getPeriodType() != null) {
                specification = specification.and(buildSpecification(criteria.getPeriodType(), Period_.periodType));
            }
            if (criteria.getOperationType() != null) {
                specification = specification.and(buildSpecification(criteria.getOperationType(), Period_.operationType));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), Period_.createdBy));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), Period_.createdDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), Period_.lastModifiedBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), Period_.lastModifiedDate));
            }
            if (criteria.getDaysOrDatesOfOccurrence() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getDaysOrDatesOfOccurrence(), Period_.daysOrDatesOfOccurrence));
            }
            if (criteria.getSpare1() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare1(), Period_.spare1));
            }
            if (criteria.getSpare2() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare2(), Period_.spare2));
            }
            if (criteria.getSpare3() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSpare3(), Period_.spare3));
            }
        }
        return specification;
    }
}
