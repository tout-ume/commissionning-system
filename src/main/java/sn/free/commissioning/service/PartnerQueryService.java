package sn.free.commissioning.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.*; // for static metamodels
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.repository.PartnerRepository;
import sn.free.commissioning.service.criteria.PartnerCriteria;
import sn.free.commissioning.service.dto.PartnerDTO;
import sn.free.commissioning.service.mapper.PartnerMapper;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link Partner} entities in the database.
 * The main input is a {@link PartnerCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PartnerDTO} or a {@link Page} of {@link PartnerDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PartnerQueryService extends QueryService<Partner> {

    private final Logger log = LoggerFactory.getLogger(PartnerQueryService.class);

    private final PartnerRepository partnerRepository;

    private final PartnerMapper partnerMapper;

    public PartnerQueryService(PartnerRepository partnerRepository, PartnerMapper partnerMapper) {
        this.partnerRepository = partnerRepository;
        this.partnerMapper = partnerMapper;
    }

    /**
     * Return a {@link List} of {@link PartnerDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PartnerDTO> findByCriteria(PartnerCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Partner> specification = createSpecification(criteria);
        return partnerMapper.toDto(partnerRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PartnerDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PartnerDTO> findByCriteria(PartnerCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Partner> specification = createSpecification(criteria);
        return partnerRepository.findAll(specification, page).map(partnerMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PartnerCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Partner> specification = createSpecification(criteria);
        return partnerRepository.count(specification);
    }

    /**
     * Function to convert {@link PartnerCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<Partner> createSpecification(PartnerCriteria criteria) {
        Specification<Partner> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), Partner_.id));
            }
            if (criteria.getMsisdn() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMsisdn(), Partner_.msisdn));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Partner_.name));
            }
            if (criteria.getSurname() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSurname(), Partner_.surname));
            }
            if (criteria.getState() != null) {
                specification = specification.and(buildStringSpecification(criteria.getState(), Partner_.state));
            }
            if (criteria.getPartnerProfileId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getPartnerProfileId(), Partner_.partnerProfileId));
            }
            /*if (criteria.getParentId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getParentId(), Partner_.parentId));
            }*/
            if (criteria.getCanResetPin() != null) {
                specification = specification.and(buildSpecification(criteria.getCanResetPin(), Partner_.canResetPin));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), Partner_.createdBy));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), Partner_.createdDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), Partner_.lastModifiedBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), Partner_.lastModifiedDate));
            }
            if (criteria.getLastTransactionDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastTransactionDate(), Partner_.lastTransactionDate));
            }
            if (criteria.getBlacklistedId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getBlacklistedId(),
                            root -> root.join(Partner_.blacklisteds, JoinType.LEFT).get(Blacklisted_.id)
                        )
                    );
            }
            if (criteria.getChildrenId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getChildrenId(), root -> root.join(Partner_.children, JoinType.LEFT).get(Partner_.id))
                    );
            }
            if (criteria.getZoneId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getZoneId(), root -> root.join(Partner_.zones, JoinType.LEFT).get(Zone_.id))
                    );
            }
            if (criteria.getPartnerId() != null) {
                specification =
                    specification.and(
                        buildSpecification(criteria.getPartnerId(), root -> root.join(Partner_.parent, JoinType.LEFT).get(Partner_.id))
                    );
            }
        }
        return specification;
    }
}
