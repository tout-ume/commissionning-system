package sn.free.commissioning.service.impl;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.domain.Payment;
import sn.free.commissioning.domain.enumeration.PaymentStatus;
import sn.free.commissioning.repository.PartnerRepository;
import sn.free.commissioning.repository.PaymentRepository;
import sn.free.commissioning.service.PartnerProfileService;
import sn.free.commissioning.service.PaymentService;
import sn.free.commissioning.service.dto.*;
import sn.free.commissioning.service.mapper.CommissionForPaymentMapper;
import sn.free.commissioning.service.mapper.CommissionMapper;
import sn.free.commissioning.service.mapper.PaymentHistoricMapper;
import sn.free.commissioning.service.mapper.PaymentMapper;

/**
 * Service Implementation for managing {@link Payment}.
 */
@Service
@Transactional
public class PaymentServiceImpl implements PaymentService {

    private final Logger log = LoggerFactory.getLogger(PaymentServiceImpl.class);

    private final PaymentRepository paymentRepository;

    private final PaymentMapper paymentMapper;

    private final PartnerRepository partnerRepository;

    private final PartnerProfileService partnerProfileService;

    private final PaymentHistoricMapper paymentHistoricMapper;
    private final CommissionForPaymentMapper commissionForPaymentMapper;

    private final CommissionMapper commissionMapper;

    public PaymentServiceImpl(
        PaymentRepository paymentRepository,
        PaymentMapper paymentMapper,
        PartnerRepository partnerRepository,
        PartnerProfileService partnerProfileService,
        PaymentHistoricMapper paymentHistoricMapper,
        CommissionForPaymentMapper commissionForPaymentMapper,
        CommissionMapper commissionMapper
    ) {
        this.paymentRepository = paymentRepository;
        this.paymentMapper = paymentMapper;
        this.partnerRepository = partnerRepository;
        this.partnerProfileService = partnerProfileService;
        this.paymentHistoricMapper = paymentHistoricMapper;
        this.commissionForPaymentMapper = commissionForPaymentMapper;
        this.commissionMapper = commissionMapper;
    }

    @Override
    public PaymentDTO save(PaymentDTO paymentDTO) {
        log.debug("Request to save Payment : {}", paymentDTO);
        Payment payment = paymentMapper.toEntity(paymentDTO);
        payment = paymentRepository.save(payment);
        return paymentMapper.toDto(payment);
    }

    @Override
    public PaymentDTO update(PaymentDTO paymentDTO) {
        log.debug("Request to save Payment : {}", paymentDTO);
        Payment payment = paymentMapper.toEntity(paymentDTO);
        payment = paymentRepository.save(payment);
        return paymentMapper.toDto(payment);
    }

    @Override
    public Optional<PaymentDTO> partialUpdate(PaymentDTO paymentDTO) {
        log.debug("Request to partially update Payment : {}", paymentDTO);

        return paymentRepository
            .findById(paymentDTO.getId())
            .map(existingPayment -> {
                paymentMapper.partialUpdate(existingPayment, paymentDTO);

                return existingPayment;
            })
            .map(paymentRepository::save)
            .map(paymentMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PaymentDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Payments");
        return paymentRepository.findAll(pageable).map(paymentMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PaymentDTO> findOne(Long id) {
        log.debug("Request to get Payment : {}", id);
        return paymentRepository.findById(id).map(paymentMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Payment : {}", id);
        paymentRepository.deleteById(id);
    }

    @Override
    public List<PaymentHistoricDTO> getDPNetworkPaymentSuccess(String msisdn) {
        Partner partner = partnerRepository.findByMsisdn(msisdn);
        if (partner != null) {
            if (
                partnerProfileService.findOne(partner.getPartnerProfileId()).isPresent() &&
                !Objects.equals(partnerProfileService.findOne(partner.getPartnerProfileId()).get().getCode(), "DP")
            ) {
                return null;
            }
            List<PaymentHistoricDTO> paymentHistoricDTOS = new ArrayList<>();
            List<Payment> payments = paymentRepository.findBySenderMsisdnAndPaymentStatus(msisdn, PaymentStatus.SUCCESS);

            paymentHistoricDTOS =
                payments
                    .stream()
                    .map(payment -> {
                        Set<CommissionForPaymentDTO> commissionForPaymentDTOSet = new HashSet<>();
                        payment
                            .getCommissions()
                            .stream()
                            .forEach(commission -> {
                                CommissionForPaymentDTO commissionForPaymentDTO = commissionForPaymentMapper.toDto(
                                    commissionMapper.toDto(commission)
                                );
                                commissionForPaymentDTOSet.add(commissionForPaymentDTO);
                            });
                        PaymentHistoricDTO paymentHistoricDTO = new PaymentHistoricDTO();
                        Partner partner1 = partnerRepository.findByMsisdn(payment.getReceiverMsisdn());
                        paymentHistoricDTO.setId(payment.getId());
                        paymentHistoricDTO.setPaymentStatus(payment.getPaymentStatus());
                        paymentHistoricDTO.setAmount(payment.getAmount());
                        paymentHistoricDTO.setCodeProfile(payment.getSpare2());
                        paymentHistoricDTO.setCreatedDate(Instant.parse(payment.getSpare1()));
                        paymentHistoricDTO.setReceiverMsisdn(payment.getReceiverMsisdn());
                        paymentHistoricDTO.setPeriodStartDate(payment.getPeriodStartDate());
                        paymentHistoricDTO.setName(partner1.getName());
                        paymentHistoricDTO.setSurname(partner1.getSurname());
                        paymentHistoricDTO.setCommissionForPaymentDTOS(commissionForPaymentDTOSet);
                        return paymentHistoricDTO;
                    })
                    .collect(Collectors.toList());

            return paymentHistoricDTOS;
        }
        return new ArrayList<>();
    }

    @Override
    public List<PaymentHistoricDTO> getDPNetworkPaymentSuccessFilter(List<PaymentHistoricDTO> paymentHistoricDTOList, String codeProfile) {
        if (!paymentHistoricDTOList.isEmpty()) {
            return paymentHistoricDTOList
                .stream()
                .filter(paymentHistoricDTO -> {
                    log.info("-------------> DTO profile {}", paymentHistoricDTO.getCodeProfile());
                    return paymentHistoricDTO.getCodeProfile().equals(codeProfile);
                })
                .collect(Collectors.toList());
        }
        return paymentHistoricDTOList;
    }
}
