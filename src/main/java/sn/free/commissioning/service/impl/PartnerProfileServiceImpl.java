package sn.free.commissioning.service.impl;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.PartnerProfile;
import sn.free.commissioning.repository.PartnerProfileRepository;
import sn.free.commissioning.service.PartnerProfileService;
import sn.free.commissioning.service.dto.PartnerProfileDTO;
import sn.free.commissioning.service.mapper.PartnerProfileMapper;

/**
 * Service Implementation for managing {@link PartnerProfile}.
 */
@Service
@Transactional
public class PartnerProfileServiceImpl implements PartnerProfileService {

    private final Logger log = LoggerFactory.getLogger(PartnerProfileServiceImpl.class);

    private final PartnerProfileRepository partnerProfileRepository;

    private final PartnerProfileMapper partnerProfileMapper;

    public PartnerProfileServiceImpl(PartnerProfileRepository partnerProfileRepository, PartnerProfileMapper partnerProfileMapper) {
        this.partnerProfileRepository = partnerProfileRepository;
        this.partnerProfileMapper = partnerProfileMapper;
    }

    @Override
    public PartnerProfileDTO save(PartnerProfileDTO partnerProfileDTO) {
        log.debug("Request to save PartnerProfile : {}", partnerProfileDTO);
        PartnerProfile partnerProfile = partnerProfileMapper.toEntity(partnerProfileDTO);
        partnerProfile = partnerProfileRepository.save(partnerProfile);
        return partnerProfileMapper.toDto(partnerProfile);
    }

    @Override
    public PartnerProfileDTO update(PartnerProfileDTO partnerProfileDTO) {
        log.debug("Request to save PartnerProfile : {}", partnerProfileDTO);
        PartnerProfile partnerProfile = partnerProfileMapper.toEntity(partnerProfileDTO);
        partnerProfile = partnerProfileRepository.save(partnerProfile);
        return partnerProfileMapper.toDto(partnerProfile);
    }

    @Override
    public Optional<PartnerProfileDTO> partialUpdate(PartnerProfileDTO partnerProfileDTO) {
        log.debug("Request to partially update PartnerProfile : {}", partnerProfileDTO);

        return partnerProfileRepository
            .findById(partnerProfileDTO.getId())
            .map(existingPartnerProfile -> {
                partnerProfileMapper.partialUpdate(existingPartnerProfile, partnerProfileDTO);

                return existingPartnerProfile;
            })
            .map(partnerProfileRepository::save)
            .map(partnerProfileMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PartnerProfileDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PartnerProfiles");
        return partnerProfileRepository.findAll(pageable).map(partnerProfileMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PartnerProfileDTO> findOne(Long id) {
        log.debug("Request to get PartnerProfile : {}", id);
        return partnerProfileRepository.findById(id).map(partnerProfileMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PartnerProfile : {}", id);
        partnerProfileRepository.deleteById(id);
    }
}
