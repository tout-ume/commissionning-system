package sn.free.commissioning.service.impl;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.MissedTransactionOperation;
import sn.free.commissioning.repository.MissedTransactionOperationRepository;
import sn.free.commissioning.service.MissedTransactionOperationService;
import sn.free.commissioning.service.dto.MissedTransactionOperationDTO;
import sn.free.commissioning.service.mapper.MissedTransactionOperationMapper;

/**
 * Service Implementation for managing {@link MissedTransactionOperation}.
 */
@Service
@Transactional
public class MissedTransactionOperationServiceImpl implements MissedTransactionOperationService {

    private final Logger log = LoggerFactory.getLogger(MissedTransactionOperationServiceImpl.class);

    private final MissedTransactionOperationRepository missedTransactionOperationRepository;

    private final MissedTransactionOperationMapper missedTransactionOperationMapper;

    public MissedTransactionOperationServiceImpl(
        MissedTransactionOperationRepository missedTransactionOperationRepository,
        MissedTransactionOperationMapper missedTransactionOperationMapper
    ) {
        this.missedTransactionOperationRepository = missedTransactionOperationRepository;
        this.missedTransactionOperationMapper = missedTransactionOperationMapper;
    }

    @Override
    public MissedTransactionOperationDTO save(MissedTransactionOperationDTO missedTransactionOperationDTO) {
        log.debug("Request to save MissedTransactionOperation : {}", missedTransactionOperationDTO);
        MissedTransactionOperation missedTransactionOperation = missedTransactionOperationMapper.toEntity(missedTransactionOperationDTO);
        missedTransactionOperation = missedTransactionOperationRepository.save(missedTransactionOperation);
        return missedTransactionOperationMapper.toDto(missedTransactionOperation);
    }

    @Override
    public MissedTransactionOperationDTO update(MissedTransactionOperationDTO missedTransactionOperationDTO) {
        log.debug("Request to save MissedTransactionOperation : {}", missedTransactionOperationDTO);
        MissedTransactionOperation missedTransactionOperation = missedTransactionOperationMapper.toEntity(missedTransactionOperationDTO);
        missedTransactionOperation = missedTransactionOperationRepository.save(missedTransactionOperation);
        return missedTransactionOperationMapper.toDto(missedTransactionOperation);
    }

    @Override
    public Optional<MissedTransactionOperationDTO> partialUpdate(MissedTransactionOperationDTO missedTransactionOperationDTO) {
        log.debug("Request to partially update MissedTransactionOperation : {}", missedTransactionOperationDTO);

        return missedTransactionOperationRepository
            .findById(missedTransactionOperationDTO.getId())
            .map(existingMissedTransactionOperation -> {
                missedTransactionOperationMapper.partialUpdate(existingMissedTransactionOperation, missedTransactionOperationDTO);

                return existingMissedTransactionOperation;
            })
            .map(missedTransactionOperationRepository::save)
            .map(missedTransactionOperationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MissedTransactionOperationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MissedTransactionOperations");
        return missedTransactionOperationRepository.findAll(pageable).map(missedTransactionOperationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<MissedTransactionOperationDTO> findOne(Long id) {
        log.debug("Request to get MissedTransactionOperation : {}", id);
        return missedTransactionOperationRepository.findById(id).map(missedTransactionOperationMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete MissedTransactionOperation : {}", id);
        missedTransactionOperationRepository.deleteById(id);
    }
}
