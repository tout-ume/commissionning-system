package sn.free.commissioning.service.impl;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.Frequency;
import sn.free.commissioning.domain.enumeration.FrequencyType;
import sn.free.commissioning.domain.enumeration.OperationType;
import sn.free.commissioning.repository.FrequencyRepository;
import sn.free.commissioning.service.FrequencyService;
import sn.free.commissioning.service.dto.FrequencyDTO;
import sn.free.commissioning.service.mapper.FrequencyMapper;

/**
 * Service Implementation for managing {@link Frequency}.
 */
@Service
@Transactional
public class FrequencyServiceImpl implements FrequencyService {

    private final Logger log = LoggerFactory.getLogger(FrequencyServiceImpl.class);

    private final FrequencyRepository frequencyRepository;

    private final FrequencyMapper frequencyMapper;

    public FrequencyServiceImpl(FrequencyRepository frequencyRepository, FrequencyMapper frequencyMapper) {
        this.frequencyRepository = frequencyRepository;
        this.frequencyMapper = frequencyMapper;
    }

    @Override
    public FrequencyDTO save(FrequencyDTO frequencyDTO) {
        log.info("Request to save Frequency : {}", frequencyDTO);
        Frequency frequency = frequencyMapper.toEntity(frequencyDTO);
        frequency = frequencyRepository.save(frequency);
        return frequencyMapper.toDto(frequency);
    }

    @Override
    public FrequencyDTO update(FrequencyDTO frequencyDTO) {
        log.info("Request to save Frequency : {}", frequencyDTO);
        Frequency frequency = frequencyMapper.toEntity(frequencyDTO);
        frequency = frequencyRepository.save(frequency);
        return frequencyMapper.toDto(frequency);
    }

    @Override
    public Optional<FrequencyDTO> partialUpdate(FrequencyDTO frequencyDTO) {
        log.info("Request to partially update Frequency : {}", frequencyDTO);

        return frequencyRepository
            .findById(frequencyDTO.getId())
            .map(existingFrequency -> {
                frequencyMapper.partialUpdate(existingFrequency, frequencyDTO);

                return existingFrequency;
            })
            .map(frequencyRepository::save)
            .map(frequencyMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<FrequencyDTO> findAll(Pageable pageable) {
        log.info("Request to get all Frequencies");
        return frequencyRepository.findAll(pageable).map(frequencyMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<FrequencyDTO> findOne(Long id) {
        log.info("Request to get Frequency : {}", id);
        return frequencyRepository.findById(id).map(frequencyMapper::toDto);
    }

    @Override
    public Optional<FrequencyDTO> findByName(String name) {
        log.info("Request to get Frequency : {}", name);
        return frequencyRepository.findByName(name).map(frequencyMapper::toDto);
    }

    @Override
    public Optional<FrequencyDTO> findByNameAndOperation(String name, OperationType operationType) {
        log.info("Request to get Frequency : {}, {}", name, operationType);
        return frequencyRepository.findByNameAndOperationType(name, operationType).map(frequencyMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.info("Request to delete Frequency : {}", id);
        frequencyRepository.deleteById(id);
    }
}
