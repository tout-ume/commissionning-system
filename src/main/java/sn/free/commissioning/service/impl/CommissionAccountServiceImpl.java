package sn.free.commissioning.service.impl;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.CommissionAccount;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.repository.CommissionAccountRepository;
import sn.free.commissioning.service.CommissionAccountService;
import sn.free.commissioning.service.dto.CommissionAccountDTO;
import sn.free.commissioning.service.mapper.CommissionAccountMapper;

/**
 * Service Implementation for managing {@link CommissionAccount}.
 */
@Service
@Transactional
public class CommissionAccountServiceImpl implements CommissionAccountService {

    private final Logger log = LoggerFactory.getLogger(CommissionAccountServiceImpl.class);

    private final CommissionAccountRepository commissionAccountRepository;

    private final CommissionAccountMapper commissionAccountMapper;

    public CommissionAccountServiceImpl(
        CommissionAccountRepository commissionAccountRepository,
        CommissionAccountMapper commissionAccountMapper
    ) {
        this.commissionAccountRepository = commissionAccountRepository;
        this.commissionAccountMapper = commissionAccountMapper;
    }

    @Override
    public CommissionAccountDTO save(CommissionAccountDTO commissionAccountDTO) {
        log.info("Request to save CommissionAccount : {}", commissionAccountDTO);
        CommissionAccount commissionAccount = commissionAccountMapper.toEntity(commissionAccountDTO);
        commissionAccount = commissionAccountRepository.save(commissionAccount);
        return commissionAccountMapper.toDto(commissionAccount);
    }

    @Override
    public CommissionAccountDTO update(CommissionAccountDTO commissionAccountDTO) {
        log.info("Request to save CommissionAccount : {}", commissionAccountDTO);
        CommissionAccount commissionAccount = commissionAccountMapper.toEntity(commissionAccountDTO);
        commissionAccount = commissionAccountRepository.save(commissionAccount);
        return commissionAccountMapper.toDto(commissionAccount);
    }

    @Override
    public Optional<CommissionAccountDTO> partialUpdate(CommissionAccountDTO commissionAccountDTO) {
        log.info("Request to partially update CommissionAccount : {}", commissionAccountDTO);

        return commissionAccountRepository
            .findById(commissionAccountDTO.getId())
            .map(existingCommissionAccount -> {
                commissionAccountMapper.partialUpdate(existingCommissionAccount, commissionAccountDTO);

                return existingCommissionAccount;
            })
            .map(commissionAccountRepository::save)
            .map(commissionAccountMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CommissionAccountDTO> findAll(Pageable pageable) {
        log.info("Request to get all CommissionAccounts");
        return commissionAccountRepository.findAll(pageable).map(commissionAccountMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CommissionAccountDTO> findOne(Long id) {
        log.info("Request to get CommissionAccount : {}", id);
        return commissionAccountRepository.findById(id).map(commissionAccountMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.info("Request to delete CommissionAccount : {}", id);
        commissionAccountRepository.deleteById(id);
    }

    @Override
    public Optional<CommissionAccount> findByPartner(Long partnerId) {
        log.info("Request to get the CommissionAccount of partner : {}", partnerId);
        return commissionAccountRepository.findByPartner_Id(partnerId);
    }
}
