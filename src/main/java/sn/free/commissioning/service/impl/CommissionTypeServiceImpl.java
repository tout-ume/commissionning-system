package sn.free.commissioning.service.impl;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.CommissionType;
import sn.free.commissioning.repository.CommissionTypeRepository;
import sn.free.commissioning.service.CommissionTypeService;
import sn.free.commissioning.service.dto.CommissionTypeDTO;
import sn.free.commissioning.service.mapper.CommissionTypeMapper;

/**
 * Service Implementation for managing {@link CommissionType}.
 */
@Service
@Transactional
public class CommissionTypeServiceImpl implements CommissionTypeService {

    private final Logger log = LoggerFactory.getLogger(CommissionTypeServiceImpl.class);

    private final CommissionTypeRepository commissionTypeRepository;

    private final CommissionTypeMapper commissionTypeMapper;

    public CommissionTypeServiceImpl(CommissionTypeRepository commissionTypeRepository, CommissionTypeMapper commissionTypeMapper) {
        this.commissionTypeRepository = commissionTypeRepository;
        this.commissionTypeMapper = commissionTypeMapper;
    }

    @Override
    public CommissionTypeDTO save(CommissionTypeDTO commissionTypeDTO) {
        log.info("Request to save CommissionType : {}", commissionTypeDTO);
        CommissionType commissionType = commissionTypeMapper.toEntity(commissionTypeDTO);
        commissionType = commissionTypeRepository.save(commissionType);
        return commissionTypeMapper.toDto(commissionType);
    }

    @Override
    public CommissionTypeDTO update(CommissionTypeDTO commissionTypeDTO) {
        log.info("Request to save CommissionType : {}", commissionTypeDTO);
        CommissionType commissionType = commissionTypeMapper.toEntity(commissionTypeDTO);
        commissionType = commissionTypeRepository.save(commissionType);
        return commissionTypeMapper.toDto(commissionType);
    }

    @Override
    public Optional<CommissionTypeDTO> partialUpdate(CommissionTypeDTO commissionTypeDTO) {
        log.info("Request to partially update CommissionType : {}", commissionTypeDTO);

        return commissionTypeRepository
            .findById(commissionTypeDTO.getId())
            .map(existingCommissionType -> {
                commissionTypeMapper.partialUpdate(existingCommissionType, commissionTypeDTO);

                return existingCommissionType;
            })
            .map(commissionTypeRepository::save)
            .map(commissionTypeMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CommissionTypeDTO> findAll(Pageable pageable) {
        log.info("Request to get all CommissionTypes");
        return commissionTypeRepository.findAll(pageable).map(commissionTypeMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CommissionTypeDTO> findOne(Long id) {
        log.info("Request to get CommissionType : {}", id);
        return commissionTypeRepository.findById(id).map(commissionTypeMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.info("Request to delete CommissionType : {}", id);
        commissionTypeRepository.deleteById(id);
    }
}
