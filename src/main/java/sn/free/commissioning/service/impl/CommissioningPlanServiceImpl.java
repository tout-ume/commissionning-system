package sn.free.commissioning.service.impl;

import java.time.Instant;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalField;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.jobrunr.scheduling.BackgroundJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.CommissioningPlan;
import sn.free.commissioning.domain.Frequency;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.domain.Period;
import sn.free.commissioning.domain.enumeration.CommissioningPlanState;
import sn.free.commissioning.domain.enumeration.FrequencyType;
import sn.free.commissioning.domain.enumeration.PaymentStrategy;
import sn.free.commissioning.domain.exceptions.NoCommissionPlanException;
import sn.free.commissioning.domain.exceptions.PartnerNotFoundException;
import sn.free.commissioning.repository.CommissioningPlanRepository;
import sn.free.commissioning.repository.ConfigurationPlanRepository;
import sn.free.commissioning.repository.PartnerRepository;
import sn.free.commissioning.repository.PeriodRepository;
import sn.free.commissioning.service.*;
import sn.free.commissioning.service.dto.BlacklistedDTO;
import sn.free.commissioning.service.dto.CommissioningPlanDTO;
import sn.free.commissioning.service.dto.ConfigurationPlanDTO;
import sn.free.commissioning.service.mapper.CommissioningPlanMapper;
import sn.free.commissioning.service.mapper.PeriodMapper;

/**
 * Service Implementation for managing {@link CommissioningPlan}.
 */
@Service
@Transactional
public class CommissioningPlanServiceImpl implements CommissioningPlanService {

    private final Logger log = LoggerFactory.getLogger(CommissioningPlanServiceImpl.class);

    private final CommissioningPlanRepository commissioningPlanRepository;

    private final PeriodRepository periodRepository;

    private final CommissioningPlanMapper commissioningPlanMapper;

    private final PeriodMapper periodMapper;

    private final ConfigurationPlanService configurationPlanService;

    private final FrequencyService frequencyService;

    private final PartnerRepository partnerRepository;

    private final PartnerProfileService partnerProfileService;

    private final BlacklistedService blacklistedService;

    public CommissioningPlanServiceImpl(
        CommissioningPlanRepository commissioningPlanRepository,
        PeriodRepository periodRepository,
        CommissioningPlanMapper commissioningPlanMapper,
        PeriodMapper periodMapper,
        ConfigurationPlanService configurationPlanService,
        FrequencyService frequencyService,
        PartnerRepository partnerRepository,
        PartnerProfileService partnerProfileService,
        BlacklistedService blacklistedService
    ) {
        this.commissioningPlanRepository = commissioningPlanRepository;
        this.periodRepository = periodRepository;
        this.commissioningPlanMapper = commissioningPlanMapper;
        this.periodMapper = periodMapper;
        this.configurationPlanService = configurationPlanService;
        this.frequencyService = frequencyService;
        this.partnerRepository = partnerRepository;
        this.partnerProfileService = partnerProfileService;
        this.blacklistedService = blacklistedService;
    }

    @Override
    public CommissioningPlanDTO save(CommissioningPlanDTO commissioningPlanDTO) {
        log.info("Request to save CommissioningPlan : {}", commissioningPlanDTO);
        List<Period> periods = new ArrayList<>();
        if (commissioningPlanDTO.getCalculusPeriod() != null) {
            periods.add(periodMapper.toEntity(commissioningPlanDTO.getCalculusPeriod()));
        }
        if (commissioningPlanDTO.getPaymentPeriod() != null) {
            periods.add(periodMapper.toEntity(commissioningPlanDTO.getPaymentPeriod()));
        }
        if (!periods.isEmpty()) {
            periods = periodRepository.saveAll(periods);
        }
        CommissioningPlan commissioningPlan = commissioningPlanMapper.toEntity(commissioningPlanDTO);
        if (commissioningPlanDTO.getCalculusPeriod() != null) {
            commissioningPlan.setCalculusPeriod(periods.get(0));
        }
        if (commissioningPlanDTO.getPaymentPeriod() != null) {
            commissioningPlan.setPaymentPeriod(periods.get(1));
        }
        CommissioningPlan finalCommissioningPlan = commissioningPlanRepository.save(commissioningPlan);
        //        configurationPlanRepository.saveAll(commissioningPlan.getConfigurationPlans().stream().map(configurationPlan -> configurationPlan.setCommissioningPlan(finalCommissioningPlan)));
        finalCommissioningPlan
            .getConfigurationPlans()
            .forEach(configurationPlan -> {
                configurationPlan.setCommissioningPlan(finalCommissioningPlan);
            });
        configurationPlanService.saveAll(finalCommissioningPlan.getConfigurationPlans());
        return commissioningPlanMapper.toDto(commissioningPlan);
    }

    @Override
    public CommissioningPlanDTO update(CommissioningPlanDTO commissioningPlanDTO) {
        log.info("Request to save CommissioningPlan : {}", commissioningPlanDTO);
        log.info(" =============== l heure d execution is {}", commissioningPlanDTO.getEndDate());
        CommissioningPlan commissioningPlan = commissioningPlanMapper.toEntity(commissioningPlanDTO);
        commissioningPlan = commissioningPlanRepository.save(commissioningPlan);
        return commissioningPlanMapper.toDto(commissioningPlan);
    }

    @Override
    public CommissioningPlanDTO updateState(CommissioningPlanDTO commissioningPlanDTO) {
        log.info("Request to update state of CommissioningPlan : {}", commissioningPlanDTO);
        commissioningPlanDTO.setState(CommissioningPlanState.ARCHIVED);
        CommissioningPlan commissioningPlan = commissioningPlanMapper.toEntity(commissioningPlanDTO);
        commissioningPlan = commissioningPlanRepository.save(commissioningPlan);
        return commissioningPlanMapper.toDto(commissioningPlan);
    }

    @Override
    public Optional<CommissioningPlanDTO> partialUpdate(CommissioningPlanDTO commissioningPlanDTO) {
        log.info("Request to partially update CommissioningPlan : {}", commissioningPlanDTO);

        return commissioningPlanRepository
            .findById(commissioningPlanDTO.getId())
            .map(existingCommissioningPlan -> {
                commissioningPlanMapper.partialUpdate(existingCommissioningPlan, commissioningPlanDTO);

                return existingCommissioningPlan;
            })
            .map(commissioningPlanRepository::save)
            .map(commissioningPlanMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CommissioningPlanDTO> findAll(Pageable pageable) {
        log.info("Request to get all CommissioningPlans");
        return commissioningPlanRepository.findAll(pageable).map(commissioningPlanMapper::toDto);
    }

    public Page<CommissioningPlanDTO> findAllWithEagerRelationships(Pageable pageable) {
        return commissioningPlanRepository.findAllWithEagerRelationships(pageable).map(commissioningPlanMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CommissioningPlanDTO> findOne(Long id) {
        log.info("Request to get CommissioningPlan : {}", id);
        return commissioningPlanRepository.findOneWithEagerRelationships(id).map(commissioningPlanMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.info("Request to delete CommissioningPlan : {}", id);
        commissioningPlanRepository.deleteById(id);
    }

    @Override
    public List<CommissioningPlan> findByServices_Name(String name) {
        return commissioningPlanRepository.findByServices_NameIgnoreCase(name);
    }

    @Override
    public List<CommissioningPlan> getValidInstantlyPlansByServiceName(String serviceName, Instant transactionDate) {
        return commissioningPlanRepository.findByServices_NameIgnoreCaseAndStateAndCalculusFrequency_TypeEqualsAndBeginDateIsLessThanAndEndDateGreaterThan(
            serviceName,
            CommissioningPlanState.IN_USE,
            FrequencyType.INSTANTLY,
            transactionDate,
            transactionDate
        );
    }

    @Override
    public List<CommissioningPlan> getCommissionningPlansByFrequency(Frequency frequency) {
        return commissioningPlanRepository.findByCalculusFrequency_Id(frequency.getId());
    }

    @Override
    public List<CommissioningPlanDTO> findByIdIn(Collection<Long> ids) {
        return commissioningPlanRepository.findByIdIn(ids).stream().map(commissioningPlanMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<CommissioningPlanDTO> getCommissionningPlansByPartnerProfile(String msisdn)
        throws PartnerNotFoundException, NoCommissionPlanException {
        Partner partner = partnerRepository.findByMsisdn(msisdn);
        List<Long> commissioningPlanIdList = new ArrayList<>();
        if (partner != null) {
            String code = partnerProfileService.findOne(partner.getPartnerProfileId()).get().getCode();
            List<ConfigurationPlanDTO> configurationPlansByCodePartner = configurationPlanService.findByPartnerProfile_Code(code);
            for (ConfigurationPlanDTO configurationPlanDTO : configurationPlansByCodePartner) {
                if (!commissioningPlanIdList.contains(configurationPlanDTO.getCommissioningPlan().getId())) {
                    commissioningPlanIdList.add(configurationPlanDTO.getCommissioningPlan().getId());
                }
            }
            if (commissioningPlanIdList.isEmpty()) {
                throw new NoCommissionPlanException("Pas de commissioning plan pour ce partner!");
            }
            return findByIdIn(commissioningPlanIdList);
        } else {
            throw new PartnerNotFoundException();
        }
    }

    @Override
    public List<CommissioningPlanDTO> getCommissioningPlansAndBlockedIsTrue(String msisdn) {
        List<Long> commissioningPlanIdList = new ArrayList<>();
        List<BlacklistedDTO> blacklisteds = blacklistedService.findByPartner_MsisdnAndBlockedIsTrue(msisdn);
        if (!blacklisteds.isEmpty()) {
            for (BlacklistedDTO blacklistedDTO : blacklisteds) {
                if (!commissioningPlanIdList.contains(blacklistedDTO.getCommissioningPlan().getId())) {
                    commissioningPlanIdList.add(blacklistedDTO.getCommissioningPlan().getId());
                }
            }
        }
        return findByIdIn(commissioningPlanIdList);
    }

    @Override
    public List<CommissioningPlanDTO> getCommissioninPlansToPayToday() {
        log.info("Request to get CommissioningPlans to pay today");
        return commissioningPlanMapper.toDto(
            commissioningPlanRepository.findByPaymentStrategyAndState(PaymentStrategy.SCHEDULED, CommissioningPlanState.IN_USE)
        );
    }
}
