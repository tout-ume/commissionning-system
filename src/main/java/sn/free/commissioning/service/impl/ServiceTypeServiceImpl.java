package sn.free.commissioning.service.impl;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.ServiceType;
import sn.free.commissioning.repository.ServiceTypeRepository;
import sn.free.commissioning.service.ServiceTypeService;
import sn.free.commissioning.service.dto.ServiceTypeDTO;
import sn.free.commissioning.service.mapper.ServiceTypeMapper;

/**
 * Service Implementation for managing {@link ServiceType}.
 */
@Service
@Transactional
public class ServiceTypeServiceImpl implements ServiceTypeService {

    private final Logger log = LoggerFactory.getLogger(ServiceTypeServiceImpl.class);

    private final ServiceTypeRepository serviceTypeRepository;

    private final ServiceTypeMapper serviceTypeMapper;

    public ServiceTypeServiceImpl(ServiceTypeRepository serviceTypeRepository, ServiceTypeMapper serviceTypeMapper) {
        this.serviceTypeRepository = serviceTypeRepository;
        this.serviceTypeMapper = serviceTypeMapper;
    }

    @Override
    public ServiceTypeDTO save(ServiceTypeDTO serviceTypeDTO) {
        log.debug("Request to save ServiceType : {}", serviceTypeDTO);
        ServiceType serviceType = serviceTypeMapper.toEntity(serviceTypeDTO);
        serviceType = serviceTypeRepository.save(serviceType);
        return serviceTypeMapper.toDto(serviceType);
    }

    @Override
    public ServiceTypeDTO update(ServiceTypeDTO serviceTypeDTO) {
        log.debug("Request to save ServiceType : {}", serviceTypeDTO);
        ServiceType serviceType = serviceTypeMapper.toEntity(serviceTypeDTO);
        serviceType = serviceTypeRepository.save(serviceType);
        return serviceTypeMapper.toDto(serviceType);
    }

    @Override
    public Optional<ServiceTypeDTO> partialUpdate(ServiceTypeDTO serviceTypeDTO) {
        log.debug("Request to partially update ServiceType : {}", serviceTypeDTO);

        return serviceTypeRepository
            .findById(serviceTypeDTO.getId())
            .map(existingServiceType -> {
                serviceTypeMapper.partialUpdate(existingServiceType, serviceTypeDTO);

                return existingServiceType;
            })
            .map(serviceTypeRepository::save)
            .map(serviceTypeMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ServiceTypeDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ServiceTypes");
        return serviceTypeRepository.findAll(pageable).map(serviceTypeMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ServiceTypeDTO> findOne(Long id) {
        log.debug("Request to get ServiceType : {}", id);
        return serviceTypeRepository.findById(id).map(serviceTypeMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ServiceType : {}", id);
        serviceTypeRepository.deleteById(id);
    }
}
