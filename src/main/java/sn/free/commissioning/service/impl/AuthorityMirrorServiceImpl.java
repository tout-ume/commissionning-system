package sn.free.commissioning.service.impl;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.AuthorityMirror;
import sn.free.commissioning.repository.AuthorityMirrorRepository;
import sn.free.commissioning.service.AuthorityMirrorService;
import sn.free.commissioning.service.dto.AuthorityMirrorDTO;
import sn.free.commissioning.service.mapper.AuthorityMirrorMapper;

/**
 * Service Implementation for managing {@link AuthorityMirror}.
 */
@Service
@Transactional
public class AuthorityMirrorServiceImpl implements AuthorityMirrorService {

    private final Logger log = LoggerFactory.getLogger(AuthorityMirrorServiceImpl.class);

    private final AuthorityMirrorRepository authorityMirrorRepository;

    private final AuthorityMirrorMapper authorityMirrorMapper;

    public AuthorityMirrorServiceImpl(AuthorityMirrorRepository authorityMirrorRepository, AuthorityMirrorMapper authorityMirrorMapper) {
        this.authorityMirrorRepository = authorityMirrorRepository;
        this.authorityMirrorMapper = authorityMirrorMapper;
    }

    @Override
    public AuthorityMirrorDTO save(AuthorityMirrorDTO authorityMirrorDTO) {
        log.debug("Request to save AuthorityMirror : {}", authorityMirrorDTO);
        AuthorityMirror authorityMirror = authorityMirrorMapper.toEntity(authorityMirrorDTO);
        authorityMirror = authorityMirrorRepository.save(authorityMirror);
        return authorityMirrorMapper.toDto(authorityMirror);
    }

    @Override
    public AuthorityMirrorDTO update(AuthorityMirrorDTO authorityMirrorDTO) {
        log.debug("Request to save AuthorityMirror : {}", authorityMirrorDTO);
        AuthorityMirror authorityMirror = authorityMirrorMapper.toEntity(authorityMirrorDTO);
        authorityMirror = authorityMirrorRepository.save(authorityMirror);
        return authorityMirrorMapper.toDto(authorityMirror);
    }

    @Override
    public Optional<AuthorityMirrorDTO> partialUpdate(AuthorityMirrorDTO authorityMirrorDTO) {
        log.debug("Request to partially update AuthorityMirror : {}", authorityMirrorDTO);

        return authorityMirrorRepository
            .findById(authorityMirrorDTO.getId())
            .map(existingAuthorityMirror -> {
                authorityMirrorMapper.partialUpdate(existingAuthorityMirror, authorityMirrorDTO);

                return existingAuthorityMirror;
            })
            .map(authorityMirrorRepository::save)
            .map(authorityMirrorMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<AuthorityMirrorDTO> findAll(Pageable pageable) {
        log.debug("Request to get all AuthorityMirrors");
        return authorityMirrorRepository.findAll(pageable).map(authorityMirrorMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<AuthorityMirrorDTO> findOne(Long id) {
        log.debug("Request to get AuthorityMirror : {}", id);
        return authorityMirrorRepository.findById(id).map(authorityMirrorMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete AuthorityMirror : {}", id);
        authorityMirrorRepository.deleteById(id);
    }
}
