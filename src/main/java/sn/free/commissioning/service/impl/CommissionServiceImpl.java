package sn.free.commissioning.service.impl;

import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.calculus_engine.domain.TimeInterval;
import sn.free.commissioning.domain.Commission;
import sn.free.commissioning.domain.CommissioningPlan;
import sn.free.commissioning.domain.ConfigurationPlan;
import sn.free.commissioning.domain.enumeration.CommissionPaymentStatus;
import sn.free.commissioning.repository.CommissionRepository;
import sn.free.commissioning.repository.CommissioningPlanRepository;
import sn.free.commissioning.service.CommissionService;
import sn.free.commissioning.service.dto.CommissionBulkPaymentDTO;
import sn.free.commissioning.service.dto.CommissionDTO;
import sn.free.commissioning.service.mapper.CommissionMapper;

/**
 * Service Implementation for managing {@link Commission}.
 */
@Service
@Transactional
public class CommissionServiceImpl implements CommissionService {

    private final Logger log = LoggerFactory.getLogger(CommissionServiceImpl.class);

    private final CommissionRepository commissionRepository;

    private final CommissioningPlanRepository commissioningPlanRepository;

    private final CommissionMapper commissionMapper;

    public CommissionServiceImpl(
        CommissionRepository commissionRepository,
        CommissioningPlanRepository commissioningPlanRepository,
        CommissionMapper commissionMapper
    ) {
        this.commissionRepository = commissionRepository;
        this.commissioningPlanRepository = commissioningPlanRepository;
        this.commissionMapper = commissionMapper;
    }

    @Override
    public CommissionDTO save(CommissionDTO commissionDTO) {
        log.debug("Request to save Commission : {}", commissionDTO);
        Commission commission = commissionMapper.toEntity(commissionDTO);
        commission = commissionRepository.save(commission);
        return commissionMapper.toDto(commission);
    }

    @Override
    public Commission save(Commission commission) {
        log.debug("Request to save Commission : {}", commission);
        return commissionRepository.save(commission);
    }

    @Override
    public CommissionDTO update(CommissionDTO commissionDTO) {
        log.debug("Request to save Commission : {}", commissionDTO);
        Commission commission = commissionMapper.toEntity(commissionDTO);
        commission = commissionRepository.save(commission);
        return commissionMapper.toDto(commission);
    }

    @Override
    public Optional<CommissionDTO> partialUpdate(CommissionDTO commissionDTO) {
        log.debug("Request to partially update Commission : {}", commissionDTO);

        return commissionRepository
            .findById(commissionDTO.getId())
            .map(existingCommission -> {
                commissionMapper.partialUpdate(existingCommission, commissionDTO);

                return existingCommission;
            })
            .map(commissionRepository::save)
            .map(commissionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CommissionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Commissions");
        return commissionRepository.findAll(pageable).map(commissionMapper::toDto);
    }

    public Page<CommissionDTO> findAllWithEagerRelationships(Pageable pageable) {
        return commissionRepository.findAllWithEagerRelationships(pageable).map(commissionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CommissionDTO> findOne(Long id) {
        log.debug("Request to get Commission : {}", id);
        return commissionRepository.findOneWithEagerRelationships(id).map(commissionMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Commission : {}", id);
        commissionRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public List<CommissionDTO> findBySenderMsidn(String msisdn) {
        log.info("Request to get all Commissions");
        return commissionRepository.findBySenderMsisdn(msisdn).stream().map(commissionMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public List<CommissionDTO> findToBePaidBySenderMsisdn(String msisdn) {
        log.info("Request to get all TO_BE_PAID Commissions for partner: {}", msisdn);
        return commissionRepository
            .findByCommissionPaymentStatusAndSenderMsisdn(CommissionPaymentStatus.TO_BE_PAID, msisdn)
            .stream()
            .map(commissionMapper::toDto)
            .collect(Collectors.toList());
    }

    @Override
    public List<CommissionDTO> findToBePaidToPartnerBySenderMsisdn(String msisdn) {
        log.info("Request to get all TO_BE_PAID_PARTNER Commissions for partner: {}", msisdn);
        return commissionRepository
            .findByCommissionPaymentStatusAndSenderMsisdn(CommissionPaymentStatus.TO_BE_PAID_TO_PARTNER, msisdn)
            .stream()
            .map(commissionMapper::toDto)
            .collect(Collectors.toList());
    }

    @Override
    public List<CommissionDTO> findByIdIn(List<Long> ids) {
        log.info("Request to get all Commissions with id in : {}", ids);
        return commissionRepository.findByIdIn(ids).stream().map(commissionMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public void changeTopProfileNetworkCommissionsStatus(Commission commission, CommissionPaymentStatus status) {
        List<String> lowAndTopProfileCodes = new ArrayList<>();
        lowAndTopProfileCodes.add("DP");
        lowAndTopProfileCodes.add("PDV");
        lowAndTopProfileCodes.add("PDV/PC");
        lowAndTopProfileCodes.add("PDV_SA");
        List<Commission> networkCommissions = commissionRepository.findByCommissionPaymentStatusAndSenderProfileNotInAndConfigurationPlan_CommissioningPlan_Id(
            CommissionPaymentStatus.TO_BE_PAID,
            lowAndTopProfileCodes,
            commission.getConfigurationPlan().getCommissioningPlan().getId()
        );
        log.info("======================= Updating commissions : {}", networkCommissions);
        if (!networkCommissions.isEmpty()) {
            networkCommissions.forEach(networkCommission -> {
                networkCommission.setCommissionPaymentStatus(status);
            });
            commissionRepository.saveAll(networkCommissions);
        }
    }

    @Override
    public List<CommissionDTO> findTopProfileCommissionsByPlan(Long planId) {
        log.info("Request to find top profile Commissions for plan : {}", planId);
        List<Long> configPlans = getPlanConfigurations(planId);
        return commissionMapper.toDto(
            commissionRepository.findByConfigurationPlan_IdInAndSenderProfileAndCommissionPaymentStatusIn(
                configPlans,
                "DP",
                statusesToFetch()
            )
        );
    }

    private TimeInterval buildPlanPeriod(Long planId) {
        TimeInterval result = new TimeInterval();
        int today = LocalDate.now().getDayOfMonth();
        int month = LocalDate.now().getMonthValue();
        int year = LocalDate.now().getYear();
        Optional<CommissioningPlan> planOptional = commissioningPlanRepository.findById(planId);
        if (
            planOptional.isPresent() &&
            planOptional.get().getPaymentFrequency() != null &&
            planOptional.get().getPaymentFrequency().getDatesOfOccurrence() != null
        ) {
            String[] days = planOptional.get().getPaymentFrequency().getDatesOfOccurrence().split(",");
            for (int i = 0; i < days.length; i++) {
                if (i == days.length - 1) {
                    if (today > Integer.parseInt(days[i])) {
                        result.setStartDate(
                            Instant.parse(
                                year + "-" + toTwoDigitsString(String.valueOf(month)) + "-" + toTwoDigitsString(days[i]) + "T00:00:00Z"
                            )
                        );
                        result.setEndDate(
                            Instant
                                .parse(
                                    month < 12
                                        ? year +
                                        "-" +
                                        toTwoDigitsString(String.valueOf(month + 1)) +
                                        "-" +
                                        toTwoDigitsString(days[0]) +
                                        "T23:59:59Z"
                                        : (year + 1) + "-" + "01" + "-" + toTwoDigitsString(days[0]) + "T23:59:59Z"
                                )
                                .truncatedTo(ChronoUnit.SECONDS)
                                .minusSeconds(86400)
                        );
                    } else {
                        result.setStartDate(
                            Instant.parse(
                                year + "-" + toTwoDigitsString(String.valueOf(month)) + "-" + toTwoDigitsString(days[i - 1]) + "T00:00:00Z"
                            )
                        );
                        result.setEndDate(
                            Instant
                                .parse(
                                    year + "-" + toTwoDigitsString(String.valueOf(month)) + "-" + toTwoDigitsString(days[i]) + "T23:59:59Z"
                                )
                                .truncatedTo(ChronoUnit.SECONDS)
                                .minusSeconds(86400)
                        );
                    }
                } else if (Integer.parseInt(days[i + 1]) >= today && Integer.parseInt(days[i]) < today) {
                    result.setStartDate(
                        Instant.parse(
                            year + "-" + toTwoDigitsString(String.valueOf(month)) + "-" + toTwoDigitsString(days[i]) + "T00:00:00Z"
                        )
                    );
                    result.setEndDate(
                        Instant.parse(
                            year + "-" + toTwoDigitsString(String.valueOf(month)) + "-" + toTwoDigitsString(days[i + 1]) + "T23:59:59Z"
                        )
                    );
                }
            }
            return result;
        } else {
            return null;
        }
    }

    private String toTwoDigitsString(String number) {
        return Integer.parseInt(number) > 9 ? number : "0" + number;
    }

    @Override
    public Page<CommissionBulkPaymentDTO> findAllProfilesCommissionsByPlan(Long planId, Pageable pageable) {
        HashMap<String, CommissionBulkPaymentDTO> result = new HashMap<>();
        List<Long> configPlans = getPlanConfigurations(planId);
        TimeInterval planPeriod = buildPlanPeriod(planId);
        if (planPeriod == null) {
            log.info("======= No payment frequency for plan : {}", planId);
            return Page.empty();
        }
        log.info(
            "======= Request to find all profiles Commissions for plan : {}, between: {} and {}",
            planId,
            planPeriod.getStartDate(),
            planPeriod.getEndDate()
        );
        List<CommissionDTO> commissions = commissionMapper.toDto(
            commissionRepository.findByConfigurationPlan_IdInAndCommissionPaymentStatusInAndCalculationDateBetween(
                configPlans,
                statusesToFetch(),
                planPeriod.getStartDate(),
                planPeriod.getEndDate()
            )
        );
        commissions
            .stream()
            .sorted(Comparator.comparing(CommissionDTO::getCalculationDate))
            .forEach(commission -> {
                CommissionBulkPaymentDTO newValue;
                if (result.containsKey(commission.getSenderMsisdn())) {
                    newValue = result.get(commission.getSenderMsisdn());
                    newValue.getCommissions().add(commission);
                    newValue.setTotalAmount(newValue.getCommissions().stream().map(CommissionDTO::getAmount).reduce(0.0, Double::sum));
                    result.replace(commission.getSenderMsisdn(), newValue);
                } else {
                    newValue = new CommissionBulkPaymentDTO();
                    newValue.getCommissions().add(commission);
                    newValue.setCommissioningPlanType(commission.getCommissioningPlanType());
                    newValue.setPartnerProfile(commission.getSenderProfile());
                    newValue.setPartnerMsisdn(commission.getSenderMsisdn());
                    newValue.setTotalAmount(newValue.getCommissions().stream().map(CommissionDTO::getAmount).reduce(0.0, Double::sum));
                    result.put(newValue.getPartnerMsisdn(), newValue);
                }
            });
        log.info("All profiles Commissions : {}", commissions.size());
        log.info("All profiles CommissionsBulks : {}", result.values().size());

        final int start = (int) pageable.getOffset();
        final int end = Math.min((start + pageable.getPageSize()), result.values().size());
        return new PageImpl<>(new ArrayList<>(result.values()).subList(start, end), pageable, result.values().size());
    }

    private List<CommissionPaymentStatus> statusesToFetch() {
        List<CommissionPaymentStatus> statuses = new ArrayList<>();
        statuses.add(CommissionPaymentStatus.TO_BE_PAID);
        //        statuses.add(CommissionPaymentStatus.FAILED);
        return statuses;
    }

    private List<Long> getPlanConfigurations(Long planId) {
        CommissioningPlan plan = commissioningPlanRepository.findById(planId).get();
        return plan.getConfigurationPlans().stream().map(ConfigurationPlan::getId).collect(Collectors.toList());
    }
}
