//package sn.free.commissioning.service.impl;
//
//import org.springframework.amqp.rabbit.annotation.RabbitHandler;
//import org.springframework.amqp.rabbit.annotation.RabbitListener;
//import org.springframework.stereotype.Component;
//import sn.free.commissioning.service.TransactionConsumerService;
//
//@Component
//public class TransactionConsumerImpl implements TransactionConsumerService {
//
//    // @RabbitListener(queues = { "${free.broker.rabbitmq.queueName}" }
//
//    private static final String QUEUE = "transaction_queue";
//
//    @Override
//    @RabbitHandler
//    @RabbitListener(queues = QUEUE)
//    public void listen(String transaction) {
//        System.out.println("================================================");
//        System.out.println(transaction);
//
//        System.out.println("Enregistrement de la transaction");
//        System.out.println("Récuperation des plans");
//        System.out.println("Récuperation de l'aborescente");
//        System.out.println("Calcul de la commission");
//        System.out.println("Pusblish le resultat");
//    }
//}
