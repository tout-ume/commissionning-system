package sn.free.commissioning.service.impl;

import java.util.*;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.repository.PartnerProfileRepository;
import sn.free.commissioning.repository.PartnerRepository;
import sn.free.commissioning.service.CommissionService;
import sn.free.commissioning.service.PartnerProfileService;
import sn.free.commissioning.service.PartnerService;
import sn.free.commissioning.service.dto.*;
import sn.free.commissioning.service.mapper.CommissionForPaymentMapper;
import sn.free.commissioning.service.mapper.PartnerForPaymentMapper;
import sn.free.commissioning.service.mapper.PartnerMapper;

/**
 * Service Implementation for managing {@link Partner}.
 */
@Service
@Transactional
public class PartnerServiceImpl implements PartnerService {

    private final Logger log = LoggerFactory.getLogger(PartnerServiceImpl.class);

    private final PartnerRepository partnerRepository;

    private final PartnerMapper partnerMapper;

    private final CommissionForPaymentMapper commissionForPaymentMapper;

    private final PartnerForPaymentMapper partnerForPaymentMapper;

    private PartnerProfileRepository partnerProfileRepository;

    private final CommissionService commissionService;
    private final PartnerProfileService partnerProfileService;

    public PartnerServiceImpl(
        PartnerRepository partnerRepository,
        CommissionService commissionService,
        PartnerMapper partnerMapper,
        CommissionForPaymentMapper commissionForPaymentMapper,
        PartnerForPaymentMapper partnerForPaymentMapper,
        PartnerProfileRepository partnerProfileRepository,
        PartnerProfileService partnerProfileService
    ) {
        this.partnerRepository = partnerRepository;
        this.partnerMapper = partnerMapper;
        this.commissionService = commissionService;
        this.commissionForPaymentMapper = commissionForPaymentMapper;
        this.partnerForPaymentMapper = partnerForPaymentMapper;
        this.partnerProfileRepository = partnerProfileRepository;
        this.partnerProfileService = partnerProfileService;
    }

    @Override
    public PartnerDTO save(PartnerDTO partnerDTO) {
        log.debug("Request to save Partner : {}", partnerDTO);
        Partner partner = partnerMapper.toEntity(partnerDTO);
        partner = partnerRepository.save(partner);
        return partnerMapper.toDto(partner);
    }

    @Override
    public PartnerDTO update(PartnerDTO partnerDTO) {
        log.debug("Request to save Partner : {}", partnerDTO);
        Partner partner = partnerMapper.toEntity(partnerDTO);
        partner = partnerRepository.save(partner);
        return partnerMapper.toDto(partner);
    }

    @Override
    public Optional<PartnerDTO> partialUpdate(PartnerDTO partnerDTO) {
        log.debug("Request to partially update Partner : {}", partnerDTO);

        return partnerRepository
            .findById(partnerDTO.getId())
            .map(existingPartner -> {
                partnerMapper.partialUpdate(existingPartner, partnerDTO);

                return existingPartner;
            })
            .map(partnerRepository::save)
            .map(partnerMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PartnerDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Partners");
        return partnerRepository.findAll(pageable).map(partnerMapper::toDto);
    }

    @Override
    public Page<PartnerDTO> findAllWithEagerRelationships(Pageable pageable) {
        return partnerRepository.findAllWithEagerRelationships(pageable).map(partnerMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PartnerDTO> findOne(Long id) {
        log.debug("Request to get Partner : {}", id);
        return partnerRepository.findOneWithEagerRelationships(id).map(partnerMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Partner : {}", id);
        partnerRepository.deleteById(id);
    }

    @Override
    public List<Partner> getPartnerTree(String msisdn, List<Partner> partners) {
        log.debug("====================== Getting Partner tree");
        Partner partner = partnerRepository.findByMsisdn(msisdn);
        if (partner != null) {
            partners.add(partner);
            if (partner.getParent() != null) {
                // Partner parent = partnerRepository.findById(partner.getParentId()).get();
                Partner parent = partner.getParent();
                return getPartnerTree(parent.getMsisdn(), partners);
            }
        } else {
            log.debug("====================== No Partner found with msisdn : {}", msisdn);
        }
        return partners;
    }

    @Override
    public List<PartnerForPaymentDTO> getDPNetwork(String msisdn) {
        Partner partner = partnerRepository.findByMsisdn(msisdn);
        if (partner != null) {
            if (
                partnerProfileService.findOne(partner.getPartnerProfileId()).isPresent() &&
                !Objects.equals(partnerProfileService.findOne(partner.getPartnerProfileId()).get().getCode(), "DP")
            ) {
                return null;
            }
            List<PartnerForPaymentDTO> nodes = new ArrayList<>();
            partner
                .getChildren()
                .stream()
                .forEach(dg -> {
                    nodes.add(getNode(dg));
                    dg
                        .getChildren()
                        .stream()
                        .forEach(rev -> {
                            nodes.add(getNode(rev));
                        });
                });
            return nodes;
        }
        return new ArrayList<>();
    }

    public PartnerForPaymentDTO getNode(Partner partner) {
        List<CommissionDTO> commissions = commissionService.findToBePaidToPartnerBySenderMsisdn(partner.getMsisdn());
        Set<CommissionForPaymentDTO> commissionForPaymentDTOS = commissions
            .stream()
            .map(commissionForPaymentMapper::toDto)
            .collect(Collectors.toSet());
        PartnerForPaymentDTO partnerForPaymentDTO = new PartnerForPaymentDTO();
        partnerForPaymentDTO.setCommissionForPaymentDTOS(commissionForPaymentDTOS);
        partnerForPaymentDTO.setId(partner.getId());
        partnerForPaymentDTO.setMsisdn(partner.getMsisdn());
        partnerForPaymentDTO.setName(partner.getName());
        partnerForPaymentDTO.setSurname(partner.getSurname());
        Long profileId = partner.getPartnerProfileId();
        String code = partnerProfileRepository.findById(profileId).get().getCode();
        partnerForPaymentDTO.setCodeProfile(code);
        return partnerForPaymentDTO;
    }

    @Override
    public List<PartnerForPaymentDTO> getDPNetworkFilter(List<PartnerForPaymentDTO> partnerForPaymentDTOList, String codeProfile) {
        log.info("-------------> sended profile {}", codeProfile);
        log.info("-------------> sended profile {}", codeProfile);

        if (!partnerForPaymentDTOList.isEmpty()) {
            return partnerForPaymentDTOList
                .stream()
                .filter(partnerBisDTO -> {
                    log.info("-------------> DTO profile {}", partnerBisDTO.getCodeProfile());
                    return partnerBisDTO.getCodeProfile().equals(codeProfile);
                })
                .collect(Collectors.toList());
        }
        return partnerForPaymentDTOList;
    }

    @Override
    public boolean checkIfPartnerProfileISInCommissioningPlan(PartnerDTO partnerDTO, CommissioningPlanDTO commissioningPlanDTO) {
        String code = partnerProfileService.findOne(partnerDTO.getPartnerProfileId()).get().getCode();
        List<String> codesCommissioning = new ArrayList<>();
        commissioningPlanDTO
            .getConfigurationPlans()
            .stream()
            .forEach(configurationPlan -> {
                codesCommissioning.add(configurationPlan.getPartnerProfile().getCode());
            });
        if (codesCommissioning.contains(code)) return true;
        return false;
    }

    @Override
    public Partner findByMsisdn(String msisdn) {
        return partnerRepository.findByMsisdn(msisdn);
    }
}
