package sn.free.commissioning.service.impl;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.calculus_engine.domain.TimeInterval;
import sn.free.commissioning.calculus_engine.utils.CalculusUtilities;
import sn.free.commissioning.domain.*;
import sn.free.commissioning.repository.TransactionOperationRepository;
import sn.free.commissioning.service.TransactionOperationService;
import sn.free.commissioning.service.dto.TransactionOperationDTO;
import sn.free.commissioning.service.mapper.TransactionOperationMapper;

/**
 * Service Implementation for managing {@link TransactionOperation}.
 */
@Service
@Transactional
public class TransactionOperationServiceImpl implements TransactionOperationService {

    private final Logger log = LoggerFactory.getLogger(TransactionOperationServiceImpl.class);

    private final TransactionOperationRepository transactionOperationRepository;

    private final TransactionOperationMapper transactionOperationMapper;

    public TransactionOperationServiceImpl(
        TransactionOperationRepository transactionOperationRepository,
        TransactionOperationMapper transactionOperationMapper
    ) {
        this.transactionOperationRepository = transactionOperationRepository;
        this.transactionOperationMapper = transactionOperationMapper;
    }

    @Override
    public TransactionOperationDTO save(TransactionOperationDTO transactionOperationDTO) {
        log.debug("Request to save TransactionOperation : {}", transactionOperationDTO);
        TransactionOperation transactionOperation = transactionOperationMapper.toEntity(transactionOperationDTO);
        transactionOperation = transactionOperationRepository.save(transactionOperation);
        return transactionOperationMapper.toDto(transactionOperation);
    }

    @Override
    public TransactionOperationDTO update(TransactionOperationDTO transactionOperationDTO) {
        log.debug("Request to save TransactionOperation : {}", transactionOperationDTO);
        TransactionOperation transactionOperation = transactionOperationMapper.toEntity(transactionOperationDTO);
        transactionOperation = transactionOperationRepository.save(transactionOperation);
        return transactionOperationMapper.toDto(transactionOperation);
    }

    @Override
    public Optional<TransactionOperationDTO> partialUpdate(TransactionOperationDTO transactionOperationDTO) {
        log.debug("Request to partially update TransactionOperation : {}", transactionOperationDTO);

        return transactionOperationRepository
            .findById(transactionOperationDTO.getId())
            .map(existingTransactionOperation -> {
                transactionOperationMapper.partialUpdate(existingTransactionOperation, transactionOperationDTO);

                return existingTransactionOperation;
            })
            .map(transactionOperationRepository::save)
            .map(transactionOperationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TransactionOperationDTO> findAll(Pageable pageable) {
        log.debug("Request to get all TransactionOperations");
        return transactionOperationRepository.findAll(pageable).map(transactionOperationMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<TransactionOperationDTO> findOne(Long id) {
        log.debug("Request to get TransactionOperation : {}", id);
        return transactionOperationRepository.findById(id).map(transactionOperationMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete TransactionOperation : {}", id);
        transactionOperationRepository.deleteById(id);
    }

    @Override
    public List<TransactionOperation> getPartnerTransactions(
        Partner partner,
        List<String> serviceTypes,
        List<String> zones,
        TimeInterval timeInterval
    ) {
        List<TransactionOperation> result;
        result =
            transactionOperationRepository.findByAgentMsisdnIgnoreCaseAndOperationDateIsBetweenAndSubTypeIsInAndSenderZoneIsInAndIsFraud(
                partner.getMsisdn(),
                timeInterval.getStartDate(),
                timeInterval.getEndDate(),
                serviceTypes,
                zones,
                false
            );
        return result;
    }

    public Page<TransactionOperationDTO> findAllWithEagerRelationships(Pageable pageable) {
        return transactionOperationRepository.findAllWithEagerRelationships(pageable).map(transactionOperationMapper::toDto);
    }

    @Override
    public List<TransactionOperationDTO> findBySenderMsisdnIgnoreCase(String senderMsisdn) {
        return transactionOperationRepository
            .findByAgentMsisdnIgnoreCase(senderMsisdn)
            .stream()
            .map(transactionOperationMapper::toDto)
            .collect(Collectors.toList());
    }

    @Override
    public List<TransactionOperationDTO> getPreviousDayTaggedTransactions() {
        Instant previousDay = Instant.now().minusSeconds(86340);
        log.info("===================== Searching transactions tagged after {}", previousDay);
        return transactionOperationMapper.toDto(
            transactionOperationRepository.findByIsFraudAndTaggedAtIsGreaterThanEqual(true, previousDay)
        );
    }
}
