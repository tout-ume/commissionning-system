package sn.free.commissioning.service.impl;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.Territory;
import sn.free.commissioning.repository.TerritoryRepository;
import sn.free.commissioning.service.TerritoryService;
import sn.free.commissioning.service.dto.TerritoryDTO;
import sn.free.commissioning.service.mapper.TerritoryMapper;

/**
 * Service Implementation for managing {@link Territory}.
 */
@Service
@Transactional
public class TerritoryServiceImpl implements TerritoryService {

    private final Logger log = LoggerFactory.getLogger(TerritoryServiceImpl.class);

    private final TerritoryRepository territoryRepository;

    private final TerritoryMapper territoryMapper;

    public TerritoryServiceImpl(TerritoryRepository territoryRepository, TerritoryMapper territoryMapper) {
        this.territoryRepository = territoryRepository;
        this.territoryMapper = territoryMapper;
    }

    @Override
    public TerritoryDTO save(TerritoryDTO territoryDTO) {
        log.debug("Request to save Territory : {}", territoryDTO);
        Territory territory = territoryMapper.toEntity(territoryDTO);
        territory = territoryRepository.save(territory);
        return territoryMapper.toDto(territory);
    }

    @Override
    public TerritoryDTO update(TerritoryDTO territoryDTO) {
        log.debug("Request to save Territory : {}", territoryDTO);
        Territory territory = territoryMapper.toEntity(territoryDTO);
        territory = territoryRepository.save(territory);
        return territoryMapper.toDto(territory);
    }

    @Override
    public Optional<TerritoryDTO> partialUpdate(TerritoryDTO territoryDTO) {
        log.debug("Request to partially update Territory : {}", territoryDTO);

        return territoryRepository
            .findById(territoryDTO.getId())
            .map(existingTerritory -> {
                territoryMapper.partialUpdate(existingTerritory, territoryDTO);

                return existingTerritory;
            })
            .map(territoryRepository::save)
            .map(territoryMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TerritoryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Territories");
        return territoryRepository.findAll(pageable).map(territoryMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<TerritoryDTO> findOne(Long id) {
        log.debug("Request to get Territory : {}", id);
        return territoryRepository.findById(id).map(territoryMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Territory : {}", id);
        territoryRepository.deleteById(id);
    }
}
