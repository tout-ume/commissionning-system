package sn.free.commissioning.service.impl;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.CalculationExecutionTrace;
import sn.free.commissioning.repository.CalculationExecutionTraceRepository;
import sn.free.commissioning.service.CalculationExecutionTraceService;
import sn.free.commissioning.service.dto.CalculationExecutionTraceDTO;
import sn.free.commissioning.service.mapper.CalculationExecutionTraceMapper;

/**
 * Service Implementation for managing {@link CalculationExecutionTrace}.
 */
@Service
@Transactional
public class CalculationExecutionTraceServiceImpl implements CalculationExecutionTraceService {

    private final Logger log = LoggerFactory.getLogger(CalculationExecutionTraceServiceImpl.class);

    private final CalculationExecutionTraceRepository calculationExecutionTraceRepository;

    private final CalculationExecutionTraceMapper calculationExecutionTraceMapper;

    public CalculationExecutionTraceServiceImpl(
        CalculationExecutionTraceRepository calculationExecutionTraceRepository,
        CalculationExecutionTraceMapper calculationExecutionTraceMapper
    ) {
        this.calculationExecutionTraceRepository = calculationExecutionTraceRepository;
        this.calculationExecutionTraceMapper = calculationExecutionTraceMapper;
    }

    @Override
    public CalculationExecutionTraceDTO save(CalculationExecutionTraceDTO calculationExecutionTraceDTO) {
        log.debug("Request to save CalculationExecutionTrace : {}", calculationExecutionTraceDTO);
        CalculationExecutionTrace calculationExecutionTrace = calculationExecutionTraceMapper.toEntity(calculationExecutionTraceDTO);
        calculationExecutionTrace = calculationExecutionTraceRepository.save(calculationExecutionTrace);
        return calculationExecutionTraceMapper.toDto(calculationExecutionTrace);
    }

    @Override
    public CalculationExecutionTraceDTO update(CalculationExecutionTraceDTO calculationExecutionTraceDTO) {
        log.debug("Request to save CalculationExecutionTrace : {}", calculationExecutionTraceDTO);
        CalculationExecutionTrace calculationExecutionTrace = calculationExecutionTraceMapper.toEntity(calculationExecutionTraceDTO);
        calculationExecutionTrace = calculationExecutionTraceRepository.save(calculationExecutionTrace);
        return calculationExecutionTraceMapper.toDto(calculationExecutionTrace);
    }

    @Override
    public Optional<CalculationExecutionTraceDTO> partialUpdate(CalculationExecutionTraceDTO calculationExecutionTraceDTO) {
        log.debug("Request to partially update CalculationExecutionTrace : {}", calculationExecutionTraceDTO);

        return calculationExecutionTraceRepository
            .findById(calculationExecutionTraceDTO.getId())
            .map(existingCalculationExecutionTrace -> {
                calculationExecutionTraceMapper.partialUpdate(existingCalculationExecutionTrace, calculationExecutionTraceDTO);

                return existingCalculationExecutionTrace;
            })
            .map(calculationExecutionTraceRepository::save)
            .map(calculationExecutionTraceMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<CalculationExecutionTraceDTO> findAll(Pageable pageable) {
        log.debug("Request to get all CalculationExecutionTraces");
        return calculationExecutionTraceRepository.findAll(pageable).map(calculationExecutionTraceMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<CalculationExecutionTraceDTO> findOne(Long id) {
        log.debug("Request to get CalculationExecutionTrace : {}", id);
        return calculationExecutionTraceRepository.findById(id).map(calculationExecutionTraceMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete CalculationExecutionTrace : {}", id);
        calculationExecutionTraceRepository.deleteById(id);
    }
}
