package sn.free.commissioning.service.impl;

import java.util.*;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.CommissionType;
import sn.free.commissioning.domain.ConfigurationPlan;
import sn.free.commissioning.repository.CommissionTypeRepository;
import sn.free.commissioning.repository.ConfigurationPlanRepository;
import sn.free.commissioning.service.ConfigurationPlanService;
import sn.free.commissioning.service.dto.ConfigurationPlanDTO;
import sn.free.commissioning.service.mapper.ConfigurationPlanMapper;

/**
 * Service Implementation for managing {@link ConfigurationPlan}.
 */
@Service
@Transactional
public class ConfigurationPlanServiceImpl implements ConfigurationPlanService {

    private final Logger log = LoggerFactory.getLogger(ConfigurationPlanServiceImpl.class);

    private final ConfigurationPlanRepository configurationPlanRepository;

    private final ConfigurationPlanMapper configurationPlanMapper;

    private final CommissionTypeRepository commissionTypeRepository;

    List<ConfigurationPlan> configurationPlansByCommissionPlan = new ArrayList<ConfigurationPlan>();

    List<ConfigurationPlan> configurationPlansByPartenerProfile = new ArrayList<ConfigurationPlan>();

    public ConfigurationPlanServiceImpl(
        ConfigurationPlanRepository configurationPlanRepository,
        CommissionTypeRepository commissionTypeRepository,
        ConfigurationPlanMapper configurationPlanMapper
    ) {
        this.configurationPlanRepository = configurationPlanRepository;
        this.commissionTypeRepository = commissionTypeRepository;
        this.configurationPlanMapper = configurationPlanMapper;
    }

    @Override
    public ConfigurationPlanDTO save(ConfigurationPlanDTO configurationPlanDTO) {
        log.info("Request to save ConfigurationPlan : {}", configurationPlanDTO);
        ConfigurationPlan configurationPlan = configurationPlanMapper.toEntity(configurationPlanDTO);
        configurationPlan = configurationPlanRepository.save(configurationPlan);
        List<CommissionType> addedCommissionTypes = new ArrayList<>(
            commissionTypeRepository.saveAll(configurationPlan.getCommissionTypes())
        );
        //        Set<CommissionType> commissionTypes = configurationPlan.getCommissionTypes();
        //        commissionTypes.forEach(item -> {
        //            CommissionType tempCommissionType = commissionTypeRepository.save(item);
        //            addedCommissionTypes.add(tempCommissionType);
        //        });
        log.info("Saved commissionTypes : {}", addedCommissionTypes);
        return configurationPlanMapper.toDto(configurationPlan);
    }

    @Override
    public List<ConfigurationPlan> saveAll(Set<ConfigurationPlan> configurationPlans) {
        log.info("Request to save ConfigurationPlans : {}", configurationPlans);
        List<CommissionType> commissionTypesToAdd = new ArrayList<>();
        List<ConfigurationPlan> result = configurationPlanRepository.saveAll(configurationPlans);
        result.forEach(configurationPlan -> {
            Set<CommissionType> commissionTypes = configurationPlan.getCommissionTypes();
            commissionTypes.forEach(item -> {
                item.setConfigurationPlan(configurationPlan);
                commissionTypesToAdd.add(item);
            });
        });
        commissionTypeRepository.saveAll(commissionTypesToAdd);
        return result;
    }

    @Override
    public ConfigurationPlanDTO update(ConfigurationPlanDTO configurationPlanDTO) {
        log.info("Request to save ConfigurationPlan : {}", configurationPlanDTO);
        ConfigurationPlan configurationPlan = configurationPlanMapper.toEntity(configurationPlanDTO);
        configurationPlan = configurationPlanRepository.save(configurationPlan);
        return configurationPlanMapper.toDto(configurationPlan);
    }

    @Override
    public Optional<ConfigurationPlanDTO> partialUpdate(ConfigurationPlanDTO configurationPlanDTO) {
        log.info("Request to partially update ConfigurationPlan : {}", configurationPlanDTO);

        return configurationPlanRepository
            .findById(configurationPlanDTO.getId())
            .map(existingConfigurationPlan -> {
                configurationPlanMapper.partialUpdate(existingConfigurationPlan, configurationPlanDTO);

                return existingConfigurationPlan;
            })
            .map(configurationPlanRepository::save)
            .map(configurationPlanMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ConfigurationPlanDTO> findAll(Pageable pageable) {
        log.info("Request to get all ConfigurationPlans");
        return configurationPlanRepository.findAll(pageable).map(configurationPlanMapper::toDto);
    }

    public Page<ConfigurationPlanDTO> findAllWithEagerRelationships(Pageable pageable) {
        return configurationPlanRepository.findAllWithEagerRelationships(pageable).map(configurationPlanMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ConfigurationPlanDTO> findOne(Long id) {
        log.info("Request to get ConfigurationPlan : {}", id);
        return configurationPlanRepository.findOneWithEagerRelationships(id).map(configurationPlanMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.info("Request to delete ConfigurationPlan : {}", id);
        configurationPlanRepository.deleteById(id);
    }

    @Override
    public List<ConfigurationPlan> findByCommissioningPlan(Long id) {
        log.info("Request to get ConfigurationPlans by ommissioningPlanId : {}", id);
        return configurationPlanRepository.findByCommissioningPlan_Id(id);
    }

    @Override
    public List<ConfigurationPlanDTO> findByPartnerProfile_Code(String code) {
        return configurationPlanRepository
            .findByPartnerProfile_Code(code)
            .stream()
            .map(configurationPlanMapper::toDto)
            .collect(Collectors.toList());
    }
}
