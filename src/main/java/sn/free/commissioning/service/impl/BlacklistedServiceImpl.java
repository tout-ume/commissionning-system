package sn.free.commissioning.service.impl;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.Blacklisted;
import sn.free.commissioning.domain.CommissioningPlan;
import sn.free.commissioning.domain.Partner;
import sn.free.commissioning.repository.BlacklistedRepository;
import sn.free.commissioning.repository.CommissioningPlanRepository;
import sn.free.commissioning.repository.PartnerRepository;
import sn.free.commissioning.security.SecurityUtils;
import sn.free.commissioning.service.BlacklistedService;
import sn.free.commissioning.service.dto.BlacklistedDTO;
import sn.free.commissioning.service.dto.CommissioningPlanDTO;
import sn.free.commissioning.service.mapper.BlacklistedMapper;

/**
 * Service Implementation for managing {@link Blacklisted}.
 */
@Service
@Transactional
public class BlacklistedServiceImpl implements BlacklistedService {

    private final Logger log = LoggerFactory.getLogger(BlacklistedServiceImpl.class);

    private final BlacklistedRepository blacklistedRepository;

    private final BlacklistedMapper blacklistedMapper;

    private final PartnerRepository partnerRepository;

    private final CommissioningPlanRepository commissioningPlanRepository;

    public BlacklistedServiceImpl(
        BlacklistedRepository blacklistedRepository,
        BlacklistedMapper blacklistedMapper,
        PartnerRepository partnerRepository,
        CommissioningPlanRepository commissioningPlanRepository
    ) {
        this.blacklistedRepository = blacklistedRepository;
        this.blacklistedMapper = blacklistedMapper;
        this.partnerRepository = partnerRepository;
        this.commissioningPlanRepository = commissioningPlanRepository;
    }

    @Override
    public BlacklistedDTO save(BlacklistedDTO blacklistedDTO) {
        log.debug("Request to save Blacklisted : {}", blacklistedDTO);
        Blacklisted blacklisted = blacklistedMapper.toEntity(blacklistedDTO);
        blacklisted = blacklistedRepository.save(blacklisted);
        return blacklistedMapper.toDto(blacklisted);
    }

    @Override
    public BlacklistedDTO update(BlacklistedDTO blacklistedDTO) {
        log.debug("Request to save Blacklisted : {}", blacklistedDTO);
        Blacklisted blacklisted = blacklistedMapper.toEntity(blacklistedDTO);
        blacklisted = blacklistedRepository.save(blacklisted);
        return blacklistedMapper.toDto(blacklisted);
    }

    @Override
    public Optional<BlacklistedDTO> partialUpdate(BlacklistedDTO blacklistedDTO) {
        log.debug("Request to partially update Blacklisted : {}", blacklistedDTO);

        return blacklistedRepository
            .findById(blacklistedDTO.getId())
            .map(existingBlacklisted -> {
                blacklistedMapper.partialUpdate(existingBlacklisted, blacklistedDTO);

                return existingBlacklisted;
            })
            .map(blacklistedRepository::save)
            .map(blacklistedMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<BlacklistedDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Blacklisteds");
        return blacklistedRepository.findAll(pageable).map(blacklistedMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<BlacklistedDTO> findOne(Long id) {
        log.debug("Request to get Blacklisted : {}", id);
        return blacklistedRepository.findById(id).map(blacklistedMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Blacklisted : {}", id);
        blacklistedRepository.deleteById(id);
    }

    @Override
    public List<BlacklistedDTO> create(HashMap<String, String> partnerReason, List<Long> commissioningPlanIDList) {
        HashMap<Long, String> partnersAlreadyBlacklisted = getPartnersAlreadyBlacklisted(partnerReason.keySet(), commissioningPlanIDList);
        List<Partner> partners = partnerRepository.findByMsisdnIn(partnerReason.keySet());
        List<CommissioningPlan> commissioningPlans = commissioningPlanRepository.findByIdIn(commissioningPlanIDList);
        List<Blacklisted> blacklistedList = new ArrayList<>();

        if (!partners.isEmpty()) {
            partners.forEach(partner -> {
                commissioningPlans.forEach(commissioningPlan -> {
                    if (partnersAlreadyBlacklisted.get(commissioningPlan.getId()) == null) {
                        Blacklisted blacklisted = new Blacklisted();
                        blacklisted.setPartner(partner);
                        blacklisted.setCommissioningPlan(commissioningPlan);
                        blacklisted.setReason(partnerReason.get(partner.getMsisdn()));
                        blacklisted.setBlocked(true);
                        blacklisted.setCreatedBy(SecurityUtils.getCurrentUserLogin().get());
                        blacklisted.setCreatedDate(Instant.now());
                        blacklisted.setLastModifiedBy(null);
                        blacklisted.setLastModifiedDate(Instant.now());
                        blacklisted = blacklistedRepository.save(blacklisted);
                        log.info("****************** {}", blacklisted);
                        blacklistedList.add(blacklisted);
                    }
                });
            });
            return blacklistedMapper.toDto(blacklistedList);
        } else {
            return null;
        }
    }

    public HashMap<Long, String> getPartnersAlreadyBlacklisted(Collection<String> partners, List<Long> plans) {
        HashMap<Long, String> result = new HashMap<>();
        List<Blacklisted> existingBlacklist = blacklistedRepository.findByCommissioningPlan_IdInAndPartner_MsisdnInAndBlockedIs(
            plans,
            partners,
            true
        );
        if (!existingBlacklist.isEmpty()) {
            for (Blacklisted blacklisted : existingBlacklist) {
                // Add partner and the plan he has been blacklisted
                result.put(blacklisted.getCommissioningPlan().getId(), blacklisted.getPartner().getMsisdn());
            }
        }
        return result;
    }

    @Override
    public void unblockAPartner(String msisdn) {
        Partner partner = partnerRepository.findByMsisdn(msisdn);
        if (partner != null) {
            List<Blacklisted> blacklisteds = blacklistedRepository.findByPartner_Id(partner.getId());
            if (blacklisteds.size() != 0) {
                blacklisteds
                    .stream()
                    .forEach(blacklisted -> {
                        blacklisted.setBlocked(false);
                        blacklisted.setLastModifiedBy(SecurityUtils.getCurrentUserLogin().get());
                        blacklisted.setLastModifiedDate(Instant.now());
                        blacklistedRepository.save(blacklisted);
                    });
            }
        }
    }

    @Override
    public Long unblockOneCommissioningPlan(String msisdn, Long commissioningPlanID) {
        Long result = null;
        Partner partner = partnerRepository.findByMsisdn(msisdn);
        if (partner != null) {
            Blacklisted blacklisted = blacklistedRepository.findByPartner_IdAndCommissioningPlan_IdAndBlockedIsTrue(
                partner.getId(),
                commissioningPlanID
            );
            if (blacklisted != null) {
                blacklisted.setBlocked(false);
                blacklisted.setLastModifiedBy(SecurityUtils.getCurrentUserLogin().get());
                blacklisted.setLastModifiedDate(Instant.now());
                blacklistedRepository.save(blacklisted);
                result = commissioningPlanID;
            }
        }
        return result;
    }

    @Override
    public List<Long> unblockAListOfCommissioningPlansForPartner(String msisdn, List<CommissioningPlanDTO> commissioningPlanDTOS) {
        Partner partner = partnerRepository.findByMsisdn(msisdn);
        List<Long> result = new ArrayList<>();
        if (partner != null) {
            if (commissioningPlanDTOS.size() != 0) {
                commissioningPlanDTOS
                    .stream()
                    .forEach(commissioningPlanDTO -> {
                        Long theId = unblockOneCommissioningPlan(partner.getMsisdn(), commissioningPlanDTO.getId());
                        if (theId != null) {
                            result.add(theId);
                        }
                    });
            }
        }
        return result;
    }

    @Override
    public List<BlacklistedDTO> findByPartner_MsisdnAndBlockedIsTrue(String msisdn) {
        return blacklistedRepository
            .findByPartner_MsisdnAndBlockedIsTrue(msisdn)
            .stream()
            .map(blacklistedMapper::toDto)
            .collect(Collectors.toList());
    }
}
