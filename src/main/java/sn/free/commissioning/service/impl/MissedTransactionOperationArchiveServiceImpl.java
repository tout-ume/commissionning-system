package sn.free.commissioning.service.impl;

import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.MissedTransactionOperationArchive;
import sn.free.commissioning.repository.MissedTransactionOperationArchiveRepository;
import sn.free.commissioning.service.MissedTransactionOperationArchiveService;
import sn.free.commissioning.service.dto.MissedTransactionOperationArchiveDTO;
import sn.free.commissioning.service.mapper.MissedTransactionOperationArchiveMapper;

/**
 * Service Implementation for managing {@link MissedTransactionOperationArchive}.
 */
@Service
@Transactional
public class MissedTransactionOperationArchiveServiceImpl implements MissedTransactionOperationArchiveService {

    private final Logger log = LoggerFactory.getLogger(MissedTransactionOperationArchiveServiceImpl.class);

    private final MissedTransactionOperationArchiveRepository missedTransactionOperationArchiveRepository;

    private final MissedTransactionOperationArchiveMapper missedTransactionOperationArchiveMapper;

    public MissedTransactionOperationArchiveServiceImpl(
        MissedTransactionOperationArchiveRepository missedTransactionOperationArchiveRepository,
        MissedTransactionOperationArchiveMapper missedTransactionOperationArchiveMapper
    ) {
        this.missedTransactionOperationArchiveRepository = missedTransactionOperationArchiveRepository;
        this.missedTransactionOperationArchiveMapper = missedTransactionOperationArchiveMapper;
    }

    @Override
    public MissedTransactionOperationArchiveDTO save(MissedTransactionOperationArchiveDTO missedTransactionOperationArchiveDTO) {
        log.debug("Request to save MissedTransactionOperationArchive : {}", missedTransactionOperationArchiveDTO);
        MissedTransactionOperationArchive missedTransactionOperationArchive = missedTransactionOperationArchiveMapper.toEntity(
            missedTransactionOperationArchiveDTO
        );
        missedTransactionOperationArchive = missedTransactionOperationArchiveRepository.save(missedTransactionOperationArchive);
        return missedTransactionOperationArchiveMapper.toDto(missedTransactionOperationArchive);
    }

    @Override
    public MissedTransactionOperationArchiveDTO update(MissedTransactionOperationArchiveDTO missedTransactionOperationArchiveDTO) {
        log.debug("Request to save MissedTransactionOperationArchive : {}", missedTransactionOperationArchiveDTO);
        MissedTransactionOperationArchive missedTransactionOperationArchive = missedTransactionOperationArchiveMapper.toEntity(
            missedTransactionOperationArchiveDTO
        );
        missedTransactionOperationArchive = missedTransactionOperationArchiveRepository.save(missedTransactionOperationArchive);
        return missedTransactionOperationArchiveMapper.toDto(missedTransactionOperationArchive);
    }

    @Override
    public Optional<MissedTransactionOperationArchiveDTO> partialUpdate(
        MissedTransactionOperationArchiveDTO missedTransactionOperationArchiveDTO
    ) {
        log.debug("Request to partially update MissedTransactionOperationArchive : {}", missedTransactionOperationArchiveDTO);

        return missedTransactionOperationArchiveRepository
            .findById(missedTransactionOperationArchiveDTO.getId())
            .map(existingMissedTransactionOperationArchive -> {
                missedTransactionOperationArchiveMapper.partialUpdate(
                    existingMissedTransactionOperationArchive,
                    missedTransactionOperationArchiveDTO
                );

                return existingMissedTransactionOperationArchive;
            })
            .map(missedTransactionOperationArchiveRepository::save)
            .map(missedTransactionOperationArchiveMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<MissedTransactionOperationArchiveDTO> findAll(Pageable pageable) {
        log.debug("Request to get all MissedTransactionOperationArchives");
        return missedTransactionOperationArchiveRepository.findAll(pageable).map(missedTransactionOperationArchiveMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<MissedTransactionOperationArchiveDTO> findOne(Long id) {
        log.debug("Request to get MissedTransactionOperationArchive : {}", id);
        return missedTransactionOperationArchiveRepository.findById(id).map(missedTransactionOperationArchiveMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete MissedTransactionOperationArchive : {}", id);
        missedTransactionOperationArchiveRepository.deleteById(id);
    }
}
