package sn.free.commissioning.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.*; // for static metamodels
import sn.free.commissioning.domain.MissedTransactionOperationArchive;
import sn.free.commissioning.repository.MissedTransactionOperationArchiveRepository;
import sn.free.commissioning.service.criteria.MissedTransactionOperationArchiveCriteria;
import sn.free.commissioning.service.dto.MissedTransactionOperationArchiveDTO;
import sn.free.commissioning.service.mapper.MissedTransactionOperationArchiveMapper;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link MissedTransactionOperationArchive} entities in the database.
 * The main input is a {@link MissedTransactionOperationArchiveCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link MissedTransactionOperationArchiveDTO} or a {@link Page} of {@link MissedTransactionOperationArchiveDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class MissedTransactionOperationArchiveQueryService extends QueryService<MissedTransactionOperationArchive> {

    private final Logger log = LoggerFactory.getLogger(MissedTransactionOperationArchiveQueryService.class);

    private final MissedTransactionOperationArchiveRepository missedTransactionOperationArchiveRepository;

    private final MissedTransactionOperationArchiveMapper missedTransactionOperationArchiveMapper;

    public MissedTransactionOperationArchiveQueryService(
        MissedTransactionOperationArchiveRepository missedTransactionOperationArchiveRepository,
        MissedTransactionOperationArchiveMapper missedTransactionOperationArchiveMapper
    ) {
        this.missedTransactionOperationArchiveRepository = missedTransactionOperationArchiveRepository;
        this.missedTransactionOperationArchiveMapper = missedTransactionOperationArchiveMapper;
    }

    /**
     * Return a {@link List} of {@link MissedTransactionOperationArchiveDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<MissedTransactionOperationArchiveDTO> findByCriteria(MissedTransactionOperationArchiveCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<MissedTransactionOperationArchive> specification = createSpecification(criteria);
        return missedTransactionOperationArchiveMapper.toDto(missedTransactionOperationArchiveRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link MissedTransactionOperationArchiveDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<MissedTransactionOperationArchiveDTO> findByCriteria(MissedTransactionOperationArchiveCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<MissedTransactionOperationArchive> specification = createSpecification(criteria);
        return missedTransactionOperationArchiveRepository.findAll(specification, page).map(missedTransactionOperationArchiveMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(MissedTransactionOperationArchiveCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<MissedTransactionOperationArchive> specification = createSpecification(criteria);
        return missedTransactionOperationArchiveRepository.count(specification);
    }

    /**
     * Function to convert {@link MissedTransactionOperationArchiveCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<MissedTransactionOperationArchive> createSpecification(MissedTransactionOperationArchiveCriteria criteria) {
        Specification<MissedTransactionOperationArchive> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), MissedTransactionOperationArchive_.id));
            }
            if (criteria.getAmount() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getAmount(), MissedTransactionOperationArchive_.amount));
            }
            if (criteria.getSubsMsisdn() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getSubsMsisdn(), MissedTransactionOperationArchive_.subsMsisdn));
            }
            if (criteria.getAgentMsisdn() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getAgentMsisdn(), MissedTransactionOperationArchive_.agentMsisdn));
            }
            if (criteria.getTypeTransaction() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getTypeTransaction(), MissedTransactionOperationArchive_.typeTransaction)
                    );
            }
            if (criteria.getCreatedAt() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getCreatedAt(), MissedTransactionOperationArchive_.createdAt));
            }
            if (criteria.getTransactionStatus() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getTransactionStatus(), MissedTransactionOperationArchive_.transactionStatus)
                    );
            }
            if (criteria.getSenderZone() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getSenderZone(), MissedTransactionOperationArchive_.senderZone));
            }
            if (criteria.getSenderProfile() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getSenderProfile(), MissedTransactionOperationArchive_.senderProfile)
                    );
            }
            if (criteria.getCodeTerritory() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getCodeTerritory(), MissedTransactionOperationArchive_.codeTerritory)
                    );
            }
            if (criteria.getSubType() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getSubType(), MissedTransactionOperationArchive_.subType));
            }
            if (criteria.getOperationDate() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(criteria.getOperationDate(), MissedTransactionOperationArchive_.operationDate)
                    );
            }
            if (criteria.getIsFraud() != null) {
                specification = specification.and(buildSpecification(criteria.getIsFraud(), MissedTransactionOperationArchive_.isFraud));
            }
            if (criteria.getTaggedAt() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getTaggedAt(), MissedTransactionOperationArchive_.taggedAt));
            }
            if (criteria.getFraudSource() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getFraudSource(), MissedTransactionOperationArchive_.fraudSource));
            }
            if (criteria.getComment() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getComment(), MissedTransactionOperationArchive_.comment));
            }
            if (criteria.getFalsePositiveDetectedAt() != null) {
                specification =
                    specification.and(
                        buildRangeSpecification(
                            criteria.getFalsePositiveDetectedAt(),
                            MissedTransactionOperationArchive_.falsePositiveDetectedAt
                        )
                    );
            }
            if (criteria.getTid() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTid(), MissedTransactionOperationArchive_.tid));
            }
            if (criteria.getParentMsisdn() != null) {
                specification =
                    specification.and(
                        buildStringSpecification(criteria.getParentMsisdn(), MissedTransactionOperationArchive_.parentMsisdn)
                    );
            }
            if (criteria.getFctDt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getFctDt(), MissedTransactionOperationArchive_.fctDt));
            }
            if (criteria.getParentId() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getParentId(), MissedTransactionOperationArchive_.parentId));
            }
            if (criteria.getCanceledAt() != null) {
                specification =
                    specification.and(buildRangeSpecification(criteria.getCanceledAt(), MissedTransactionOperationArchive_.canceledAt));
            }
            if (criteria.getCanceledId() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getCanceledId(), MissedTransactionOperationArchive_.canceledId));
            }
            if (criteria.getProductId() != null) {
                specification =
                    specification.and(buildStringSpecification(criteria.getProductId(), MissedTransactionOperationArchive_.productId));
            }
        }
        return specification;
    }
}
