package sn.free.commissioning.service;

import java.util.List;
import javax.persistence.criteria.JoinType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sn.free.commissioning.domain.*; // for static metamodels
import sn.free.commissioning.domain.ServiceType;
import sn.free.commissioning.repository.ServiceTypeRepository;
import sn.free.commissioning.service.criteria.ServiceTypeCriteria;
import sn.free.commissioning.service.dto.ServiceTypeDTO;
import sn.free.commissioning.service.mapper.ServiceTypeMapper;
import tech.jhipster.service.QueryService;

/**
 * Service for executing complex queries for {@link ServiceType} entities in the database.
 * The main input is a {@link ServiceTypeCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ServiceTypeDTO} or a {@link Page} of {@link ServiceTypeDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ServiceTypeQueryService extends QueryService<ServiceType> {

    private final Logger log = LoggerFactory.getLogger(ServiceTypeQueryService.class);

    private final ServiceTypeRepository serviceTypeRepository;

    private final ServiceTypeMapper serviceTypeMapper;

    public ServiceTypeQueryService(ServiceTypeRepository serviceTypeRepository, ServiceTypeMapper serviceTypeMapper) {
        this.serviceTypeRepository = serviceTypeRepository;
        this.serviceTypeMapper = serviceTypeMapper;
    }

    /**
     * Return a {@link List} of {@link ServiceTypeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ServiceTypeDTO> findByCriteria(ServiceTypeCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ServiceType> specification = createSpecification(criteria);
        return serviceTypeMapper.toDto(serviceTypeRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ServiceTypeDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ServiceTypeDTO> findByCriteria(ServiceTypeCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ServiceType> specification = createSpecification(criteria);
        return serviceTypeRepository.findAll(specification, page).map(serviceTypeMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ServiceTypeCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ServiceType> specification = createSpecification(criteria);
        return serviceTypeRepository.count(specification);
    }

    /**
     * Function to convert {@link ServiceTypeCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ServiceType> createSpecification(ServiceTypeCriteria criteria) {
        Specification<ServiceType> specification = Specification.where(null);
        if (criteria != null) {
            // This has to be called first, because the distinct method returns null
            if (criteria.getDistinct() != null) {
                specification = specification.and(distinct(criteria.getDistinct()));
            }
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ServiceType_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), ServiceType_.name));
            }
            if (criteria.getCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCode(), ServiceType_.code));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), ServiceType_.description));
            }
            if (criteria.getState() != null) {
                specification = specification.and(buildSpecification(criteria.getState(), ServiceType_.state));
            }
            if (criteria.getIsCosRelated() != null) {
                specification = specification.and(buildSpecification(criteria.getIsCosRelated(), ServiceType_.isCosRelated));
            }
            if (criteria.getCommissioningPlanType() != null) {
                specification =
                    specification.and(buildSpecification(criteria.getCommissioningPlanType(), ServiceType_.commissioningPlanType));
            }
            if (criteria.getCreatedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCreatedBy(), ServiceType_.createdBy));
            }
            if (criteria.getCreatedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreatedDate(), ServiceType_.createdDate));
            }
            if (criteria.getLastModifiedBy() != null) {
                specification = specification.and(buildStringSpecification(criteria.getLastModifiedBy(), ServiceType_.lastModifiedBy));
            }
            if (criteria.getLastModifiedDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastModifiedDate(), ServiceType_.lastModifiedDate));
            }
            if (criteria.getCommissionPlanId() != null) {
                specification =
                    specification.and(
                        buildSpecification(
                            criteria.getCommissionPlanId(),
                            root -> root.join(ServiceType_.commissionPlans, JoinType.LEFT).get(CommissioningPlan_.id)
                        )
                    );
            }
        }
        return specification;
    }
}
